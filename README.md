# Examples

This is an area to create examples and experiments.

# Phaser 3 - Logsdon

Docs
https://docs.google.com/document/d/1QaC8i9pOGnVew9L6JniPz50L4dnqvs681AikXx749j4/edit#heading=h.xjct3j1d0ywx
https://docs.google.com/document/d/144DYCFhC-Qbuumtj7HePEs_7prTFopzINt3KCPGrqbM/edit#heading=h.a89hxf4oxi87

Paths to check
C:\xampp\htdocs\phaser-logsdon_examples\templates\_tmp3
C:\xampp\htdocs\phaser-logsdon_examples\templates\_tmp3\scripts\fbc-vbs-2021
C:\xampp\htdocs\phaser-logsdon_examples\templates\_tmp3\scripts\mystery-files\scenes
C:\xampp\htdocs\phaser-logsdon_examples\apps

## Common game structure

* Boot
* Preload Screen
* Promotional screens
* Start Screen
  * Menu/Options Screen
  * Instructions/How to Play
  * High Scores/Level Time Screen
* Game
  * Levels
    * Preload
    * Intro
    * Level Start
    * Gameplay
      * Scenes like Driving or Tapping
    * Level Over
    * Outtro
    * Continue to Next Level
  * Pause Screen
  * Game Over Screen
    * Lose
    * Win
    * Continue
* Credits

## Example of states and the order they run (Phaser 2)

* gameStates.Boot.init
* gameStates.Boot.preload
* gameStates.Boot.create
* gameStates.Boot.update
* gameStates.Boot.render
* gameStates.Boot.shutdown

* gameStates.Preloader.preload
* gameStates.Preloader.create

* gameStates.MainMenu.create
* gameStates.MainMenu.update (loop)
* gameStates.MainMenu.start
* gameStates.MainMenu.shutdown

* gameStates.Game.create
* gameStates.Game.update (loop)
* gameStates.Game.render (loop)

## Base HTML

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Game</title>
	
	<!-- Meta tags. -->
	<meta http-equiv="X-UA-Compatible" content="chrome=1, IE=9">
	<meta http-equiv="cleartype" content="on">
	<meta name="format-detection" content="telephone=no">
	<meta name="HandheldFriendly" content="true">
	<meta name="robots" content="noindex,nofollow">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Game">
	<meta name="viewport" content="initial-scale=1.0 maximum-scale=1.0 user-scalable=0 minimal-ui shrink-to-fit=no">

	<!-- Use common styles. -->
	<link href="styles/style.css" media="all" rel="stylesheet" type="text/css">
	<style type="text/css"></style>
	
    <!-- Use the latest production build. -->
    <script src="https://cdn.jsdelivr.net/npm/phaser@3.19.0/dist/phaser-arcade-physics.min.js"></script>
	<!-- Include your own scripts. -->
	<!-- Use custom scripts. -->
	<script type="text/javascript"></script>

<body>

</body>
</html>
```

# Resources

* https://www.emanueleferonato.com/2019/03/08/fling-unity-game-built-in-html5-with-phaser-and-matter-js-updated-with-the-latest-constraints-tweaks-fly-through-the-screen-with-your-ninja-rope/
* http://phaser.io/news/2019/07/save-and-load-progress-with-local-storage
* http://phaser.io/news/2019/07/building-maintainable-games-part-1
* http://phaser.io/news/2019/02/build-a-space-shooter-in-phaser-3
* https://github.com/rexrainbow/phaser3-rex-notes/tree/master/plugins/behaviors
* Plugins
  * https://phaserplugins.com/

## Documentation

* https://photonstorm.github.io/phaser3-docs/
* https://rexrainbow.github.io/phaser3-rex-notes/docs/site/tween/

## Video

* http://phaser.io/news/category/video
* https://phasertutorials.com/creating-a-phaser-3-template-part-1/

### Resources

* https://www.dl-sounds.com/royalty-free/category/game-film/video-game/
* https://www.sounds-resource.com/
* https://bassgorilla.com/video-game-sound-effects/
* https://www.freepik.com/free-vectors/background
* https://clipart.me/download/abstract-blue-cubist-vector-ice-background-28615

* https://forums.cocoon.io/t/solved-phaser-audio-looping/1283/6
* http://www.html5gamedevs.com/topic/10976-seamless-loop/

* http://fontawesome.io/cheatsheet/

* http://www.html5gamedevs.com/topic/9345-text-shadow-problem/

* https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb

* https://stackoverflow.com/questions/122102/what-is-the-most-efficient-way-to-deep-clone-an-object-in-javascript

* https://gamedevacademy.org/how-to-debug-phaser-games/

* http://www.html5gamedevs.com/topic/4670-gradient-background/

* https://stackoverflow.com/questions/11832914/round-to-at-most-2-decimal-places-only-if-necessary

* http://www.html5gamedevs.com/topic/7095-best-audio-format-to-use-mp3-wav-or-ogg/
* http://www.html5gamedevs.com/topic/29518-sound-problem-ios-ipad/
* http://www.html5gamedevs.com/topic/29518-sound-problem-ios-ipad/
* http://www.html5gamedevs.com/topic/16356-audio-on-safari/

* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
* https://stackoverflow.com/questions/4116608/pass-unknown-number-of-arguments-into-javascript-function

#### QR Code

* https://github.com/davidshimjs/qrcodejs (create)
  * https://medium.com/geekculture/few-ways-to-generate-qr-code-using-javascript-54b6b5220c4f
* https://github.com/mebjas/html5-qrcode (read)
  * https://minhazav.medium.com/qr-and-barcode-scanner-using-html-and-javascript-2cdc937f793d
* https://github.com/zxing-js/library
  * https://github.com/zxing-js/browser
  * https://github.com/zxing-js/library/blob/master/docs/examples/qr-camera/index.html

#### Design & UI

* [Bootstrap](https://getbootstrap.com/)
* [FontAwesome](https://fontawesome.com/)
* [Compromise](https://github.com/spencermountain/compromise/) for JavaScript and the English language

## jQuery

[Download](https://jquery.com/download/)

## CSS Animation Libraries

* https://devdojo.com/souptikdn/12-best-css-animation-libraries
* https://css-tricks.com/css-animation-libraries/
* [Animate](https://animate.style/)

## Debugging and Testing

* [Chrome Remote](https://developer.chrome.com/docs/devtools/remote-debugging/)
