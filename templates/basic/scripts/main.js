
// Game data object for reference across states
// Game settings persistent during session
var gameSettingsDefault = {
	// Debug options
	debug : {
		gameClock : true,
		nocache : true,
		methodCalls : true,
		physicsBodies : true,
		preloading : true
	},
	responsiveSettings : {
		'480' : {
			fontSizeScale : 0.8
		}
	},
	version : '1.0',
	'' : ''// Only here so everything before can end with a comma
};
var gameSettings = window.REDLOVE_PHASER_UTILITY.deepCloneValues(gameSettingsDefault);

// Game data persistent during a session
var gameDataDefault = {
	// Mode options
	difficulty : 'low',
	paused : false,
	// Stats
	stats : {
		accuracy : 0,
		attempts : 0,
		completions : 0,
		time : 0
	},
	gameTimer : {
		last : 0,
		paused : false,
		start : 0,
		total : 0
	},
	'' : ''// Only here so everything before can end with a comma
};
var gameData = window.REDLOVE_PHASER_UTILITY.deepCloneValues(gameDataDefault);

// Can be used to hold data, like scores or achievements, across sessions
var gamePersistentData = {};

// Game objects or resources created and used during a session
var gameObjects = {};

// Create an object for game states
var gameStates = {};

// Create a property to hold custom functions
gameStates.fn = {};

var game;

window.onload = function ()
{
	
	// Gather device information
	var dimensions = window.REDLOVE_PHASER_UTILITY.calculateDimensions();
	
	// Create game element
	var config = {
		width: dimensions.clientWidth,
		height: dimensions.clientHeight,
		renderer: Phaser.CANVAS,
		parent: 'game',
		transparent: false,
		antialias: true,
		forceSetTimeOut: false
	};
	game = new Phaser.Game(config);
	
	// Add game states
	game.state.add('Boot', gameStates.Boot);
	game.state.add('Preloader', gameStates.Preloader);
	game.state.add('MainMenu', gameStates.MainMenu);
	game.state.add('Game', gameStates.Game);
	
	// Boot the game
	game.state.start('Boot');

};
