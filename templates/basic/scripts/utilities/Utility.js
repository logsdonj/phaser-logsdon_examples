class Utility
{
	constructor() {
	}

	static isMobile() {
		return (navigator.userAgent.indexOf('Mobile') !== -1 || navigator.userAgent.indexOf('Tablet') !== -1);
	}

	static gameWidth(width) {
		width = width || 800;

        // Potentially use device dimensions for mobile games
		if (this.isMobile()) {
			width = window.innerWidth;
		}

		return width;
	}

	static gameHeight(height) {
		height = height || 600;

        // Potentially use device dimensions for mobile games
		if (this.isMobile()) {
			height = window.innerHeight;
		}

		return height;
	}
}


window.REDLOVE_PHASER_UTILITY = window.REDLOVE_PHASER_UTILITY || {};

window.REDLOVE_PHASER_UTILITY.newline = '\n';

//https://medium.com/slow-cooked-games-blog/how-to-create-a-mobile-game-on-the-cheap-38a7b75999a7#.7ho2ywm5h
window.REDLOVE_PHASER_UTILITY.calculateDimensions = function ()
{
	var data = {
		ratio : window.innerWidth / window.innerHeight,
		innerWidth : window.innerWidth,
		innerHeight : window.innerHeight,
		clientWidth : document.documentElement.clientWidth,
		clientHeight : document.documentElement.clientHeight,
		devicePixelRatio : window.devicePixelRatio || 1
	};
	
	data.screenWidthMax = Math.max(window.innerWidth * data.devicePixelRatio, data.clientWidth);
	data.screenHeightMax = Math.max(window.innerHeight * data.devicePixelRatio, data.clientHeight);
	data.scaleRatio3rds = data.devicePixelRatio / 3;//http://www.joshmorony.com/how-to-scale-a-game-for-all-device-sizes-in-phaser/
	data.scaleRatio = data.devicePixelRatio;//myAsset.scale.setTo(scaleRatio, scaleRatio)

	return data;
};

/*
// Try to hide mobile browser address bar
function hideAddressBar ()
{
	if ( ! window.location.hash )
  {
	  if ( document.documentElement.scrollHeight < window.outerHeight / window.devicePixelRatio )
		{
			window.scrollTo(0,0);// reset in case prev not scrolled
			document.documentElement.style.height = (window.outerHeight / window.devicePixelRatio) + 'px';
			setTimeout(function ()
			{
				window.scrollTo(1, 1);
			}, 50);
		}
	}
}
window.addEventListener('load', function ()
{
	if ( ! window.pageYOffset )
	{
		hideAddressBar();
	}
});
window.addEventListener('orientationchange', function ()
{
	hideAddressBar();
});
*/

window.REDLOVE_PHASER_UTILITY.valueAtIndex = function ( obj, index )
{
	return obj[ Object.keys(obj)[index] ];
};

window.REDLOVE_PHASER_UTILITY.objLength = function ( obj )
{
	return Object.keys(obj).length;
};

window.REDLOVE_PHASER_UTILITY.randomColorFromHex = function ( colors )
{
	var color = window.REDLOVE_PHASER_UTILITY.randomFromArray(colors);
	return '#' + window.REDLOVE_PHASER_UTILITY.hexString(color);
};

// https://stackoverflow.com/questions/57803/how-to-convert-decimal-to-hex-in-javascript
// https://stackoverflow.com/questions/11023144/working-with-hex-strings-and-hex-values-more-easily-in-javascript
window.REDLOVE_PHASER_UTILITY.hexString = function ( hex )
{
	if ( typeof hex === 'undefined' )
	{
		hex = 0x000000;
	}
	
	var hexString = hex.toString(16);
	return window.REDLOVE_PHASER_UTILITY.pad('000000', hexString, true);
};

window.REDLOVE_PHASER_UTILITY.randomFromArray = function ( arr )
{
	return arr[Math.floor(Math.random() * arr.length)];
};

// Pad values: https://stackoverflow.com/questions/2686855/is-there-a-javascript-function-that-can-pad-a-string-to-get-to-a-determined-leng/24398129#24398129
window.REDLOVE_PHASER_UTILITY.pad = function ( pad, str, padLeft )
{
	if ( typeof str === 'undefined' )
	{
		return pad;
	}
	
	if ( padLeft )
	{
		return (pad + str).slice(-pad.length);
	}
	else
	{
		return (str + pad).substring(0, pad.length);
	}
};

window.REDLOVE_PHASER_UTILITY.timeExpanded = function ( time_milliseconds )
{
	var time = ( typeof time_milliseconds === 'undefined' ) ? 0 : time_milliseconds;
	var milliseconds = 0;
	var seconds = 0;
	var minutes = 0;
	var hours = 0;
	
	milliseconds = time;
	seconds = time / (1000);
	minutes = time / (1000 * 60);
	hours = time / (1000 * 60 * 60);
	days = time / (1000 * 60 * 60 * 24);
	years = time / (1000 * 60 * 60 * 24 * 364.25);
	
	var tmp_time = time;
	clock_milliseconds = tmp_time % 1000;
	tmp_time = (tmp_time - clock_milliseconds) / 1000;
	clock_seconds = tmp_time % 60;
	tmp_time = (tmp_time - clock_seconds) / 60;
	clock_minutes = tmp_time % 60;
	clock_hours = (tmp_time - clock_minutes) / 60;
	
	return {
		milliseconds : milliseconds,
		seconds : seconds,
		minutes : minutes,
		hours : hours,
		days : days,
		years : years,
		
		clock_milliseconds : clock_milliseconds,
		clock_seconds : clock_seconds,
		clock_minutes : clock_minutes,
		clock_hours : clock_hours,
		
		clock_pad_milliseconds : window.REDLOVE_PHASER_UTILITY.pad('000', clock_milliseconds, true),
		clock_pad_seconds : window.REDLOVE_PHASER_UTILITY.pad('00', clock_seconds, true),
		clock_pad_minutes : window.REDLOVE_PHASER_UTILITY.pad('00', clock_minutes, true),
		clock_pad_hours : window.REDLOVE_PHASER_UTILITY.pad('00', clock_hours, true)
	};
};

window.REDLOVE_PHASER_UTILITY.scaleToWorld = function ( objWidth, objHeight, worldWidth, worldHeight, type )
{
	if ( typeof objWidth === 'object' )
	{
		var tmpObj = objWidth;
		var tmpWorldObj = objHeight;
		type = worldWidth;
		
		objWidth = tmpObj.width;
		objHeight = tmpObj.height;
		
		worldWidth = tmpWorldObj.width;
		worldHeight = tmpWorldObj.height;
	}
	
	if ( typeof type === 'undefined' )
	{
		type = 'fill';
	}
	
	var width = objWidth;
	var height = objHeight;
	
	// If ratio > 1, the width is greater than the height
	var worldRatio = worldWidth / worldHeight;
	var objRatio = objWidth / objHeight;
	
	// Scale proportionately to fill the world
	if ( type == 'fill' )
	{
		// Obj compard to the world, obj width is smallest and should max
		if ( objRatio < worldRatio )
		{
			width = worldWidth;
			height = width / objRatio;
		}
		else
		{
			height = worldHeight;
			width = height * objRatio;
		}
	}
	
	return {
		width : width,
		height : height
	};
};

window.REDLOVE_PHASER_UTILITY.deepCloneValues = function ( obj )
{
	return JSON.parse(JSON.stringify(obj));
};

// https://stackoverflow.com/questions/1255512/how-to-draw-a-rounded-rectangle-on-html-canvas
window.REDLOVE_PHASER_UTILITY.roundedRect = function ( ctx, x, y, w, h, r )
{
	if (w < 2 * r) r = w / 2;
	if (h < 2 * r) r = h / 2;
	ctx.beginPath();
	ctx.moveTo(x+r, y);
	ctx.arcTo(x+w, y, x+w, y+h, r);
	ctx.arcTo(x+w, y+h, x, y+h, r);
	ctx.arcTo(x, y+h, x, y, r);
	ctx.arcTo(x, y, x+w, y, r);
	ctx.closePath();
	return ctx;
};

window.REDLOVE_PHASER_UTILITY.childPosition = function ( group, index )
{
	if ( typeof index === 'undefined' )
	{
		index = group.length - 1;
	}
	
	var child = group.children[index];
	
	return {
		height : child.height,
		width : child.width,
		x : child.x,
		y : child.y
	};
};