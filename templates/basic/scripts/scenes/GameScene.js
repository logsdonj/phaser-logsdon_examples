class GameScene extends Phaser.Scene
{
	// Properties
	myProperty = 'myProperty value';

	// Constructor
	constructor(sceneName) {
		super(sceneName);// Pass the scene identifier
		this.sceneName = sceneName;
	}

	// Methods

	// Prepare data
	init() {
		console.log('here ' + this.myProperty);
		if (config.debug) console.log(this.sceneName + '.init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log(this.sceneName + '.preload');
		// Load images
		this.load.image('star', 'assets/images/star.png');
	}

	// Add objects to app
	create() {
		//this.add.text(20, 20, 'Loading game...');
		//this.scene.start('playGame');
		
		if (config.debug) console.log(this.sceneName + '.create');
		// 1) Create sprite and then add to the physics system
		this.droppingStar = this.add.image(400, 300, 'star');
		this.physics.add.existing(this.droppingStar);
	}

	// App loop
	update(time, delta) {
		//this.scene.start('PreloadScene');
	}

	// Getters and setter methods

	get sceneName() {
		return this._sceneName;
	}
	set sceneName(value) {
		this._sceneName = value;
	}
}
