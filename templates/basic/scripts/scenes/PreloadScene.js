class PreloadScene extends Phaser.Scene
{
	constructor() {
		super('PreloadScene');// Pass the scene identifier
	}

	// Prepare data
	init() {
	}

	// Load assets
	preload() {
	}

	// Add objects to app
	create() {
	}

	// App loop
	update(time, delta) {
		this.scene.start('GameScene');
	}
}
