/**
 * RedLove Utilities
 * 
 * Namespace
 * https://stackoverflow.com/questions/11651527/how-to-set-up-javascript-namespace-and-classes-properly
 * Classes
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
 */
window.REDLOVE = window.REDLOVE || {};
window.RL = window.REDLOVE;

// Values
window.REDLOVE.newline = '\n';