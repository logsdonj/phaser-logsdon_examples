/**
 * Facilitate event handling and listening for classes and objects without native events
 * 
 * References:

// Classes
// https://developer.mozilla.org/en-US/docs/Web/Java#_listenerKeysScript/Reference/Classes
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
// https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Classes_in_JavaScript
// https://stackoverflow.com/questions/29879267/es6-class-multiple-inheritance

// Mixins
// https://stackoverflow.com/questions/42247434/how-to-add-mixins-to-es6-javascript-classes
// https://stackoverflow.com/a/42250080
// https://javascript.info/mixins
// Add in a mixin for custom even listener handling
// In constructor: Object.assign(this, mixin);

// Event listener vs handler
// https://medium.com/geekculture/event-handlers-vs-event-listeners-in-javascript-b4086b8040b0
// https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Events

// .apply()
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/apply

	Usage in a class:
	// Public properties
	events;
	eventNames = {
		reset: 'reset'
	};
	// OR
	// Turn an array into an object of key => values later
	eventNames = [
		'reset'
	];

	// Methods

    constructor () {
		this.events = new EventHandler(this.eventNames);
    }

	reset () {
		// Trigger listening events
		this.events.trigger(this.events.names.reset);
	}

	Usage for class using EventHandler:
	// Listen for reset
	MyGame.events.on(MyGame.events.names.reset, handleReset);

 */
class EventHandler
{
	// Properties
	
	// Static

	// Private

	// Public

	// Option 1: Regular array - [{callback: fn, eventName: 'name'}, {callback: fn2, eventName: 'name'}]
	// Option 2: Associative array (object) - {eventName: [fn,fn2]}
	// Option 1 is used right now so that duplicate callbacks can exist for an eventName
	listeners;
	names;

	// Methods

	/**
	 * Add an event listener
	 * @param {object | array} names 
	 */
    constructor ( names ) {
		// If passing an array, turn it into a key => value object using values
		if ( Array.isArray(names) )
		{
			// https://stackoverflow.com/a/50985915
			// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce
			names = names.reduce((obj, value, index) => ({
				...obj, [value]: value
			}), {});
		}

		this.names = names;
		this.reset();
    }
	
	reset () {
		this.listeners = [];
	}

	// Getters and Setters

	get listeners () {
		return this.listeners;
	}

	set listeners ( value) {
		this.listeners = value;
		return this.listeners;
	}

	// Handle events for listeners

	/**
	 * Add an event listener
	 * @param {string} eventName 
	 * @param {object} callback 
	 */
	on ( eventName, callback ) {
		this.listeners.push({
			callback: callback,
			//callbackScope: callbackScope,// Future consideration
			//args: [],// Future consideration
			eventName: eventName
		});
	}

	/**
	 * Alias to on
	 * @param {mixed} args 
	 */
	add ( ...args ) {
		this.on.apply(this, args);
	}

	/**
	 * Remove an event listener
	 * @param {string|bool} eventName 
	 * @param {object} callback 
	 */
	off ( eventName, callback ) {
		// If removing listener from all event names
		if ( eventName === true )
		{
			this.listeners.forEach((listener, listenerIndex) => {
				if ( listener.callback == callback )
				{
					this.listeners.splice(listenerIndex, 1);
				}
			});

			return;
		}

		// Find matching listener
		const index = this.listeners.indexOf({
			callback: callback,
			eventName: eventName
		});
		
		// If item found, splice it from array
		if ( index > -1 )
		{
			// 2nd parameter means remove one item only
			this.listeners.splice(index, 1);
		}
	}

	/**
	 * Alias to off
	 * @param {mixed} args 
	 */
	remove ( ...args ) {
		this.on.apply(this, args);
	}

	/**
	 * Do an event listener
	 * @param {string} eventName 
	 * @param {mixed} args 
	 */
	trigger ( eventName, ...args ) {
		// https://stackoverflow.com/a/3010848
		for ( const listener of this.listeners )
		{
			if ( listener.eventName != eventName )
			{
				continue;
			}

			listener.callback.apply(this, args);
		}
	}

	/**
	 * Alias to trigger
	 * @param {mixed} args 
	 */
	do ( ...args ) {
		this.trigger.apply(this, args);
	}

	/**
	 * Alias to trigger
	 * @param {mixed} args 
	 */
	emit ( ...args ) {
		this.trigger.apply(this, args);
	}
	
}