class SlingshotInteraction {
    constructor(scene, obj, config) {
		// Scene to add global events, graphics, and resources to
		this.scene = scene;
		// Game object to use for interaction
		this.obj = obj;
		
		var defaultConfig = {
			debug: false, // Show debug information

			// Anchor pull to object center
			anchorPullFromOrigin: true,
			minPullDistance: 0,
			maxPullDistance: -1,
		};

        if (config === undefined) {
        	config = {};
        }

		this.config = Object.assign({}, defaultConfig, config);

		this.data = {};

		// Create an event emitter for this class if not using the game object to emit
		this.events = new Phaser.Events.EventEmitter();

		// Make the game object interactive
		this.obj.setInteractive({cursor: 'pointer'})
		.on('pointerdown', this.pointerDown, this);
	}

	pointerDown(pointer) {
		// If debugging
		if ( this.config.debug )
		{
			// If debug resources do not exist
			if ( typeof(this.debugGraphics) === 'undefined' )
			{
				// Create debug graphics
				this.debugGraphics = this.scene.add.graphics({ x: 0, y: 0 })
				.setVisible(true);

				// Create debug text
				this.debugText = this.scene.add.text(
					// Normalize the top left of the object
					this.obj.x - (this.obj.originX * this.obj.width), 
					this.obj.y - (this.obj.originY * this.obj.height),
					'',
					{ 
						backgroundColor: 'rgba(0,0,100,0.4)',
						font: '10px sans-serif', 
						fill: 'rgb(255,255,255)',
						/*stroke: 'rgb(47,92,31)',
						strokeThickness: 5,
						shadow: {
							offsetX: 4,
							offsetY: 4,
							color: 'rgba(0,0,0,1)',
							blur: 0,
							stroke: true,
							fill: true
						},*/
						padding: { // Adjust to work with shadown offset and blur to avoid clipping
							x: 5,
							y: 5
						},
						align: 'left'
					}
				)
				.setOrigin(0, 0)
				//.setFixedSize(this.game.scale.width, this.game.scale.height)
				//.setDepth(10)
				//.setInteractive({ cursor: 'pointer' })
				.setVisible(true);
				/*
				.once('pointerdown', function()
				{
					this.leaderboardText.destroy();
					this.formElement.setVisible(true);
				}, this);
				*/
			}
			// Reset existing resources
			else
			{
				this.debugGraphics.clear().setVisible(true);
				this.debugText.setText('').setVisible(true);
			}
		}

		// Add scene event listeners for events outside of the game object
		this.scene.input.on('pointermove', this.pointerMove, this);
		this.scene.input.once('pointerup', this.pointerUp, this);
		this.scene.input.once('pointerupoutside', this.pointerUp, this);

		this.updateInteractive(pointer);

		// Emit event
		this.events.emit('slingshot.down', this);
		this.obj.emit('slingshot.down', this);
	}

	pointerMove(pointer) {
		// Stop if pointer not down
		if ( ! pointer.isDown )
		{
			return;
		}

		if ( this.updateInteractive(pointer) === false )
		{
			return false;
		}
		
		// Emit event
		this.events.emit('slingshot.move', this);
		this.obj.emit('slingshot.move', this);
	}

	pointerUp(pointer) {
		// Stop watching movement
		this.scene.input.off('pointermove', this.pointerMove, this);

		this.updateInteractive(pointer);
		
		// If debugging
		if ( this.config.debug )
		{
			this.debugGraphics.clear().setVisible(false);
			this.debugText.setText('').setVisible(false);
		}

		// Emit event
		this.events.emit('slingshot.up', this);
		this.obj.emit('slingshot.up', this);
	}

	updateInteractive(pointer) {
		// Set points

		// Default to using the pointer down as the origin
		var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
		
		// If option is for alternate anchor point, use it for the origin
		if ( 
			this.config.anchorPullFromOrigin != false
		)
		{
			// If pulling from center
			if ( this.config.anchorPullFromOrigin === 'center' )
			{
				// Make adjustments after normalizing the origin
				var halfWidth = this.obj.width / 2;
				var originWidth = this.obj.originX * this.obj.width;
				var normalizedLeftX = this.obj.x - originWidth;
				var centerX = normalizedLeftX + halfWidth;
				
				var halfHeight = this.obj.height / 2;
				var originHeight = this.obj.originY * this.obj.height;
				var normalizedTopY = this.obj.y - originHeight;
				var centerY = normalizedTopY + halfHeight;

				originPoint = new Phaser.Geom.Point(centerX, centerY);
			}
			// If manually setting the origin for the pull, e.g. {x:0.5,y:0.5} would be center of the object
			else if ( typeof(this.config.anchorPullFromOrigin) === 'object' )
			{
				// Make adjustments after normalizing the origin
				var offsetWidth = this.config.anchorPullFromOrigin.x * this.obj.width;
				var originWidth = this.obj.originX * this.obj.width;
				var normalizedLeftX = this.obj.x - originWidth;
				var originX = normalizedLeftX + offsetWidth;
				
				var offsetHeight = this.config.anchorPullFromOrigin.y * this.obj.height;
				var originHeight = this.obj.originY * this.obj.height;
				var normalizedTopY = this.obj.y - originHeight;
				var originY = normalizedTopY + offsetHeight;

				originPoint = new Phaser.Geom.Point(originX, originY);
			}
			// Otherwise, use the origin
			else
			{
				originPoint = new Phaser.Geom.Point(this.obj.x, this.obj.y);
			}
		}

		// Target point is the current pointer position
		var targetPoint = new Phaser.Geom.Point(pointer.x, pointer.y);

		// Data calculations
		var distance = Phaser.Math.Distance.BetweenPoints(originPoint, targetPoint);
		var angleRadians = Phaser.Math.Angle.BetweenPoints(originPoint, targetPoint);
		var angleDegrees = Phaser.Math.RadToDeg(angleRadians);
		// Use the reverse angle with object rotation for the pull effect
		var angleRadiansReverse = Phaser.Math.Angle.Wrap(Phaser.Math.Angle.Reverse(angleRadians));//angleRadians + Math.PI
		var angleDegreesReverse = Phaser.Math.RadToDeg(angleRadiansReverse);
		// Normalize from -pi-pi and -180-180 to 0-2pi and 0-360
		var angleRadiansNormalized = Phaser.Math.Angle.Normalize(angleRadians);
		var angleDegreesNormalized = Phaser.Math.RadToDeg(angleRadiansNormalized);

		// Check minimum distance to move before action
		if ( distance < this.config.minPullDistance )
		{
			return false;
		}

		var maxPullDistance = this.config.maxPullDistance;

		// Line from first click to current drag pull back
		var pullLine = new Phaser.Geom.Line(originPoint.x, originPoint.y, targetPoint.x, targetPoint.y);

		// Use gameplay area if max distance not manually defined
		if ( maxPullDistance < 0 )
		{
			// Line that will eventually intersect gampeplay if continued
			var intersectingLine = Phaser.Geom.Line.Clone(pullLine);
			// Create a rect the size of gameplay area
			var gameplayRect = new Phaser.Geom.Rectangle(0, 0, this.scene.game.scale.width, this.scene.game.scale.height);
			// Find the longest length in the game area
			var gameplayHypotenuse = Math.hypot( gameplayRect.width, gameplayRect.height);
			// To ensure the line will intersect the gamplay area, extend the line with the largest length
			// Sometimes the intersect is Nan if same coordinations for original points
			intersectingLine = Phaser.Geom.Line.Extend(intersectingLine, 0, Math.ceil(gameplayHypotenuse) + 1);
			// Find where the intersect into gameplay would be
			var intersectPoints = Phaser.Geom.Intersects.GetLineToRectangle(intersectingLine, gameplayRect);
			// This gives the farthest point on the gameplay permiter that could be pulled back to
			var intersectPoint = ( intersectPoints.length > 0 ) ? intersectPoints[0] : Phaser.Geom.Point.Clone(originPoint);
			// Get the distance from the pull origin to the farthest point
			var gameplayPullMaxDistance = Phaser.Math.Distance.BetweenPoints(originPoint, intersectPoint);

			// Update max distance to gameplay area
			maxPullDistance = gameplayPullMaxDistance;
		}

		// Line for the max pull distance
		var maxPullLine = new Phaser.Geom.Line(originPoint.x, originPoint.y, originPoint.x, originPoint.y);
		maxPullLine = Phaser.Geom.Line.Extend(maxPullLine, 0, maxPullDistance);

		// Get final distances and strength
		var distanceConstrained = Math.min(distance, maxPullDistance);
		var rawStrength = distance / maxPullDistance;
		var strengthConstrained = distanceConstrained / maxPullDistance;
		strengthConstrained = RL.u.roundToDecimals(strengthConstrained, 2);

		// Get axis and rotation information
		//var axisInfo = RL.p.getAxisDirection(originPoint, targetPoint);
		var rotationDirection = RL.p.orientationTest(originPoint, targetPoint, this.prevTargetPoint);
		// Use angles from RL.p.getAxisDirection or recreate them
		// Track angle changes to add up total rotation amount in the same direction
		var angleRadians = Phaser.Math.Angle.BetweenPoints(originPoint, targetPoint);
		var anglePrevRadians = Phaser.Math.Angle.BetweenPoints(originPoint, this.prevTargetPoint || targetPoint);
		var angleRadiansDiff = Math.abs(Math.abs(anglePrevRadians) - Math.abs(angleRadians));
		var angleDegreesDiff = Phaser.Math.RadToDeg(angleRadiansDiff);
		// Multiplying by the rotation direction will give it a sign +/- to handle addition or subtraction on its own
		// Clockwise is negative
		this.data.rotatedDegreesTotal = (this.data.rotatedDegreesTotal || 0) + (angleDegreesDiff * rotationDirection);
		this.data.rotations = this.data.rotatedDegreesTotal / 360;
		// After all calculations, update the previous target point for use next time
		this.prevTargetPoint = targetPoint;

		// Save data through copy over objects instead of directly assigning values
		this.data = Object.assign({}, this.data, {
			rawDistance: distance,
			distance: distanceConstrained,
			maxPullDistance: maxPullDistance,
			rawStrength: rawStrength,
			strength: strengthConstrained,
			angleRadians: angleRadians,
			angleRadiansReverse: angleRadiansReverse,
			angleDegrees: angleDegrees,
			angleDegreesReverse: angleDegreesReverse,
			angleRadiansNormalized: angleRadiansNormalized,
			angleDegreesNormalized: angleDegreesNormalized,
		});

		// If debugging
		if ( this.config.debug )
		{
			// Update text
			this.debugText.setText(
				'distance: ' + RL.u.roundToDecimals(this.data.distance, 2) + '\n' +
				'strength: ' + this.data.strength + '\n' +
				'angle: ' + RL.u.roundToDecimals(this.data.angleDegrees, 2) + '\n' + 
				'angle: ' + RL.u.roundToDecimals(this.data.angleDegreesNormalized, 2) + '\n' +
				''
			);

			// Move text with object bounds
			var objBounds = this.obj.getBounds()
			this.debugText.x = objBounds.x;
			this.debugText.y = objBounds.y;

			// Draw pull line
			this.debugGraphics
			.clear()
			.lineStyle(10, 0x00ff00, 0.4)
			.beginPath()
			.moveTo(pullLine.x1, pullLine.y1)
			.lineTo(pullLine.x2, pullLine.y2)
			.closePath()
			.strokePath();

			// Draw max pull line
			var maxPullLine = Phaser.Geom.Line.Clone(pullLine);
			var maxPullDistanceDifference = maxPullDistance - distance;
			maxPullLine = Phaser.Geom.Line.Extend(maxPullLine, 0, maxPullDistanceDifference);

			this.debugGraphics
			.lineStyle(6, 0xff0000, 0.4)
			.beginPath()
			.moveTo(maxPullLine.x1, maxPullLine.y1)
			.lineTo(maxPullLine.x2, maxPullLine.y2)
			.closePath()
			.strokePath();

			// If intersect used
			if ( typeof(intersectingLine) !== 'undefined' )
			{
				// Draw intersect line
				this.debugGraphics
				.lineStyle(1, 0xffff00, 0.4)
				.beginPath()
				.moveTo(intersectingLine.x1, intersectingLine.y1)
				.lineTo(intersectingLine.x2, intersectingLine.y2)
				.closePath()
				.strokePath();
	
				// Draw intersect point
				var size = 10;
				var sizeHalf = size / 2;
	
				this.debugGraphics
				.lineStyle(1, 0xffff00, 0.4)
				.fillStyle(0xffff00, 0.4)
				.beginPath()
				.fillRect(intersectPoint.x - sizeHalf, intersectPoint.y - sizeHalf, size, size)
				.strokeRect(intersectPoint.x - sizeHalf, intersectPoint.y - sizeHalf, size, size)
				.closePath();
			}
		}
	}
}