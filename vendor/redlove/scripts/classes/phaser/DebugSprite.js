class DebugSprite {
    constructor(scene, config) {
		// Scene to add global events, graphics, and resources to
		this.scene = scene;
		
		var defaultConfig = {
			debug: true, // Show debug information

			width: 100,
			height: 100,
			fillColor: 0x565656,
			fillOpacity: 0.5,
			strokeWidth: 1,
			strokeColor: 0xffffff,
			strokeOpacity: 0.5
		};

        if (config === undefined) {
        	config = {};
        }

		this.config = Object.assign({}, defaultConfig, config);

		this.data = {};

		/*
		// Create player rectangle
		this.player = this.add.rectangle(this.game.scale.width / 2, this.game.scale.height / 3, 100, 100, 0xff0000, 0.5)
		.setStrokeStyle(2, 0x1a65ac)
		.setOrigin(0.5, 0.5);
		*/

		// Create graphics
		var objGraphics = this.scene.add.graphics({ x: 0, y: 0 });

		// Create shape for graphics
		var objRect = new Phaser.Geom.Rectangle(0, 0, this.config.width, this.config.height);
		objGraphics.lineStyle(this.config.strokeWidth, this.config.strokeColor, this.config.strokeOpacity);
		objGraphics.strokeRectShape(objRect);
		objGraphics.fillStyle(this.config.fillColor, this.config.fillOpacity);
		objGraphics.fillRectShape(objRect);

		var objRectCenter = Phaser.Geom.Rectangle.GetCenter(objRect);
		
		// Create shape for graphics
		var objCircle = new Phaser.Geom.Circle(objRectCenter.x, objRectCenter.y, objRect.width / 2);
		objGraphics.strokeCircleShape(objCircle);
		objGraphics.fillCircleShape(objCircle);
		
		// Create triangle on graphics starting at and pointing to 0 degrees
		var objTriangle = new Phaser.Geom.Triangle.BuildEquilateral(objRect.width, objRect.height / 2, objRect.width / 2);
		objTriangle = Phaser.Geom.Triangle.RotateAroundXY(objTriangle, objRect.width, objRect.height / 2, Phaser.Math.DegToRad(90));
		objGraphics.strokeTriangleShape(objTriangle);
		objGraphics.fillTriangleShape(objTriangle);
		
		// Create texture from graphics and destroy graphic
		// Create a unique texture name, otherwise the texture will be added to every time another object is created with the same texture name
		var randomTextureNum = Date.now();//Phaser.Math.Between(0, 1000);
		var objTexture = objGraphics.generateTexture('objTexture' + randomTextureNum, objRect.width, objRect.height);
		objGraphics.destroy();
		// Create player from texture
		return this.scene.add.sprite(this.scene.game.scale.width / 2, (this.scene.game.scale.height / 2) - objRect.height, 'objTexture' + randomTextureNum);
		//.setAngle(-90)
		//.setOrigin(0.5);
	}
}