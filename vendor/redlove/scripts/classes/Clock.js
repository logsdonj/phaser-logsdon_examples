/**
 * 
 * References:
 * https://rexrainbow.github.io/phaser3-rex-notes/docs/site/timer/
 * https://codepen.io/awkay/pen/ExzGea
 * https://rexrainbow.github.io/phaser3-rex-notes/docs/site/clock/
 * https://rexrainbow.github.io/phaser3-rex-notes/docs/site/date/
 * 
 * Usage:

let MyClock = new Clock().start();

 */
class Clock
{
	// Properties
	
	// Static

	// Private

	#_version = '0.1';
	#_runningTime;
	#_elapsedTime;

	// Public

	events;
	eventNames = {
		reset: 'resetClock',
		seek: 'seekClock',
		start: 'startClock',
		stop: 'stopClock'
	};
	
	started;
	running;
	startTime;
	stopTime;
	timeScale = 1;

	// Methods

	/**
	 * 
	 * @returns object
	 */
    constructor () {
		this.events = new EventHandler(this.eventNames);
		this.reset();
		return this; // For method chaining
    }
	
	/**
	 * 
	 * @returns object
	 */
	reset () {
		this.started = false;
		this.running = false;
		this.startTime = 0;
		this.stopTime = 0;
		this.runningTime = 0;
		this.elapsedTime = 0;
		// Trigger event
		this.events.trigger(this.events.names.reset);
		return this; // For method chaining
	}

	/**
	 * 
	 * @returns object
	 */
	start ( offsetMs ) {
		this.started = true;
		this.running = true;
		// Add on offset time
		this.startTime = Date.now() + (offsetMs ?? 0);
		// Trigger event
		this.events.trigger(this.events.names.start);
		return this; // For method chaining
	}

	/**
	 * 
	 * @returns object
	 */
	seek ( offsetMs ) {
		// Add on offset time
		this.startTime += (offsetMs ?? 0);
		// Trigger event
		this.events.trigger(this.events.names.seek);
		return this; // For method chaining
	}

	/**
	 * 
	 * @returns object
	 */
	stop () {
		// If running, perform these actions
		if ( this.running )
		{
			this.running = false;
			this.stopTime = Date.now();
			this.runningTime = this.stopTime - this.startTime;
			this.elapsedTime += this.runningTime;
		}

		// Trigger event
		this.events.trigger(this.events.names.stop);
		return this; // For method chaining
	}

	// Getters and Setters

	get version () {
		return this.#_version;
	}

	get elapsedTime () {
		// If still running, calculate from current time
		if ( this.running )
		{
			console.log(this.running);
			return (this.#_elapsedTime + this.runningTime) * this.timeScale;
		}

		return this.#_elapsedTime * this.timeScale;
	}

	set elapsedTime ( value ) {
		this.#_elapsedTime = value;
	}

	get elapsedSeconds () {
		return this.elapsedTime / 1000;
	}

	get runningTime () {
		// If still running, calculate from current time
		if ( this.running )
		{
			return (Date.now() - this.startTime) * this.timeScale;
		}

		return this.#_runningTime * this.timeScale;
	}

	set runningTime ( value ) {
		this.#_runningTime = value;
	}

}