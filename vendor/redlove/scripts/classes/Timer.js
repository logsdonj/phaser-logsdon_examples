/**
 * 
 * References:
 * https://developer.mozilla.org/en-US/docs/Web/API/setTimeout
 * https://javascript.info/settimeout-setinterval
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_OR_assignment
 * https://rexrainbow.github.io/phaser3-rex-notes/docs/site/timer/
 * 
 * Usage:

let MyTimer = new Timer({goalTime: 3000, delay: false});
MyTimer.events.on('update', function(timer) { console.log('Timer tick: ', timer.getProgress());});
MyTimer.events.on('complete', function(timer) { console.log('Timer complete! ', timer.getProgress());});
MyTimer.events.on('stop', function(timer) { console.log('Timer stopped.', timer.running);});
MyTimer.start();

 */
class Timer
{
	// Properties
	
	// Static

	// Private

	#_version = '0.1';
	#_runningTime;
	#_elapsedTime;
	#_remainingTime;

	// Public

	events;
	eventNames = {
		reset: 'resetTimer',
		seek: 'seekTimer',
		start: 'startTimer',
		stop: 'stopTimer',
		update: 'updateTimer',
		pause: 'pauseTimer',
		resume: 'resumeTimer',
		complete: 'completeTimer'
	};

	defaultConfig = {
		startAt: 0,
		intervalMs: 1000,
		repeat: 0,
		loop: true,
		delay: true,
		goalTime: 0,
		timeScale: 1,
		clockOnly: false
	};

	startAt;
	intervalMs;
	repeat;
	loop;
	goalTime;
	timeScale;

	timerId;
	started;
	running;
	paused;
	repeatCount;
	startTime;
	stopTime;

	// Methods

	/**
	 * 
	 * @param {object} config 
	 * @returns object
	 */
    constructor ( config ) {
		// Filter the passed config
		const filteredConfig = Utilities.mergeWhitelistObject(this.defaultConfig, config);
		// Set config values in this
		Object.assign(this, filteredConfig);

		this.events = new EventHandler(this.eventNames);
		this.reset();
		return this; // For method chaining
    }
	
	/**
	 * 
	 * @returns object
	 */
	reset () {
		this.started = false;
		this.running = false;
		this.paused = false;
		this.repeatCount = 0;
		this.startTime = 0;
		this.stopTime = 0;
		this.runningTime = 0;
		this.elapsedTime = 0;
		// Trigger event
		this.events.trigger(this.events.names.reset, this);
		return this; // For method chaining
	}

	/**
	 * 
	 * @param {int} offsetMs 
	 * @returns object
	 */
	start ( offsetMs ) {
		this.#_startTimer(offsetMs);
		// Trigger event
		this.events.trigger(this.events.names.start, this);
		return this; // For method chaining
	}

	/**
	 * 
	 * @returns object
	 */
	resume () {
		this.#_startTimer(offsetMs);
		// Trigger event
		this.events.trigger(this.events.names.resume, this);
		return this; // For method chaining
	}

	/**
	 * 
	 * @param {int} offsetMs 
	 * @returns object
	 */
	seek ( offsetMs ) {
		// Add on offset time
		this.startTime += (offsetMs || 0);
		// Trigger event
		this.events.trigger(this.events.names.seek, this);
		return this; // For method chaining
	}

	/**
	 * Run every timer tick
	 * @param {object} instanceRef 
	 */
	update ( instanceRef ) {
		clearTimeout(instanceRef.timerId);
		// Trigger event
		instanceRef.events.trigger(instanceRef.events.names.update, instanceRef);

		//instanceRef.update();
		// If a goal was set, check it
		if ( instanceRef.remainingTime <= 0 )
		{
			// Trigger event
			instanceRef.events.trigger(instanceRef.events.names.complete, instanceRef);

			instanceRef.stop();
			return;
		}

		// If not looping or repeat count met, stop
		if ( 
			( instanceRef.loop === false ) || 
			( instanceRef.repeat > 0 && instanceRef.repeatCount >= instanceRef.repeat )
		)
		{
			instanceRef.stop();
			return;
		}

		// Nest the timeout and repeat
		if ( instanceRef.running )
		{
			instanceRef.repeatCount++;
			instanceRef.timerId = setTimeout(instanceRef.update, instanceRef.intervalMs, instanceRef);
		}
	}

	/**
	 * 
	 * @returns object
	 */
	pause () {
		this.#_stopTimer(true);
		// Trigger event
		this.events.trigger(this.events.names.paused, this);
		return this; // For method chaining
	}

	/**
	 * 
	 * @returns object
	 */
	stop () {
		this.#_stopTimer();
		// Trigger event
		this.events.trigger(this.events.names.stop, this);
		return this; // For method chaining
	}

	/**
	 * Get progress towards goalTime if set.
	 * 
	 * @param {boolean} overall 
	 * @returns {float}
	 */
	getProgress ( overall ) {
		// If there is no goal time, stop
		if ( ! this.goalTime )
		{
			return;
		}

		let targetTime = this.goalTime;

		// Check if wanting overall progress and if repeating, adjust target time
		if ( overall && this.repeat > 0 )
		{
			targetTime = this.goalTime * this.repeat;
		}

		// Remaining time is normalized, so use it to avoid > 1
		// this.elapsedTime / targetTime
		return 1 - (this.remainingTime / targetTime);
	}

	// Private methods

	#_startTimer ( offsetMs ) {
		this.started = true;
		this.running = true;
		this.paused = false;

		// If first time starting, check if starting at a different time or default
		let startAt = ( ! this.started ) ? this.startAt ?? 0 : 0;
		// Add on offset time
		this.startTime = Date.now() + startAt + (offsetMs ?? 0);

		// If no delay in the timeout, trigger a first update immediately
		if ( this.delay === false )
		{
			this.update(this);
		}
		
		// Clear any previous timeouts
		clearTimeout(this.timerId);
		// If using timers
		if ( this.clockOnly !== true )
		{
			// Use nested timeouts instead of intervals
			this.timerId = setTimeout(this.update, this.intervalMs, this);
		}
	}

	#_stopTimer ( pause ) {
		clearTimeout(this.timerId);

		if ( pause )
		{
			this.paused = true;
		}
		
		// If running, perform these actions
		if ( this.running )
		{
			this.running = false;
			this.stopTime = Date.now();
			this.runningTime = this.stopTime - this.startTime;
			this.elapsedTime += this.runningTime;
		}
	}

	// Getters and Setters

	get version () {
		return this.#_version;
	}

	get interval () {
		return this.intervalMs;
	}

	set interval ( value) {
		this.intervalMs = value;
		return this.intervalMs;
	}

	get elapsedTime () {
		// If still running, calculate from current time
		if ( this.running )
		{
			return (this.#_elapsedTime + this.runningTime) * this.timeScale;
		}

		return this.#_elapsedTime * this.timeScale;
	}

	set elapsedTime ( value ) {
		this.#_elapsedTime = value;
	}

	get elapsedSeconds () {
		return this.elapsedTime / 1000;
	}

	get runningTime () {
		// If still running, calculate from current time
		if ( this.running )
		{
			return (Date.now() - this.startTime) * this.timeScale;
		}

		return this.#_runningTime * this.timeScale;
	}

	set runningTime ( value ) {
		this.#_runningTime = value;
	}

	get remainingTime () {
		// If there is no goal time, stop
		if ( ! this.goalTime )
		{
			return;
		}

		this.remainingTime = this.goalTime - this.elapsedTime;
		return this.#_remainingTime;
	}

	set remainingTime ( value ) {
		// Normalize the value
		if ( value <= 0 )
		{
			value = 0;
		}

		this.#_remainingTime = value;
		return this.#_remainingTime;
	}

	get progress () {
		let progress = this.getProgress();
		return progress;
	}

	get progressPercent () {
		return (this.progress * 100).toFixed(2);
	}

}