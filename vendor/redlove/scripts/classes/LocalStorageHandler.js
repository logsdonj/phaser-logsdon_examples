// https://github.com/joaocunha/javascript-localstorage-handler/blob/master/LocalStorageHandler.js

/*
// https://blog.logrocket.com/localstorage-javascript-complete-guide/
window.localStorage.setItem('user', JSON.stringify(person));
setItem(): Add key and value to localStorage
getItem(): This is how you get items from localStorage
removeItem(): Remove an item by key from localStorage
clear(): Clear all localStorage
key(): Passed a number to retrieve the key of a localStorage

if (typeof(Storage) !== "undefined") {
	// Code for localStorage
} else {
	// No web storage Support.
}
*/

class LocalStorageHandler
{
	/**
	 * @property _ls
	 * @private
	 * @type Object
	 */
	#_ls = window.localStorage;

	/**
	 * @property length
	 * @type Number
	 */
	static length;
	
    constructor() {
        this.length = this.#_ls.length;
    }

	/**
	 * @method get
	 * @param key {String} Item key
	 * @return {String|Object|Null}
	 */
	static get (key) {
		try {
			return JSON.parse(this.#_ls.getItem(key));
		} catch(e) {
			return this.#_ls.getItem(key);
		}
	}

	/**
	 * @method set
	 * @param key {String} Item key
	 * @param val {String|Object} Item value
	 * @return {String|Object} The value of the item just set
	 */
	static set (key, val) {
		this.#_ls.setItem(key,JSON.stringify(val));
		return this.get(key);
	};

	/**
	 * @method key
	 * @param index {Number} Item index
	 * @return {String|Null} The item key if found, null if not
	 */
	static key (index) {
		if (typeof index === 'number') {
			return this.#_ls.key(index);
		}
	}


	/**
	 * @method data
	 * @return {Array|Null} An array containing all items in localStorage through key{string}-value{String|Object} pairs
	 */
	static data () {
		let i = 0;
		let data = [];

		while (this.#_ls.key(i)) {
			data[i] = [this.#_ls.key(i), this.get(this.#_ls.key(i))];
			i++;
		}

		return data.length ? data : null;
	}

	/**
	 * @method remove
	 * @param keyOrIndex {String|Number} Item key or index (which will be converted to key anyway)
	 * @return {Boolean} True if the key was found before deletion, false if not
	 */
	static remove (keyOrIndex) {
		let result = false;
		let key = (typeof keyOrIndex === 'number') ? this.key(keyOrIndex) : keyOrIndex;

		if (key in this.#_ls) {
			result = true;
			this.#_ls.removeItem(key);
		}

		return result;
	}

	/**
	 * @method clear
	 * @return {Number} The total of items removed
	 */
	static clear () {
		let len = this.#_ls.length;
		this.#_ls.clear();
		return len;
	}

}
