// https://github.com/joaocunha/javascript-localstorage-handler/blob/master/LocalStorageHandler.js

/*
// https://blog.logrocket.com/localstorage-javascript-complete-guide/
window.localStorage.setItem('user', JSON.stringify(person));
setItem(): Add key and value to localStorage
getItem(): This is how you get items from localStorage
removeItem(): Remove an item by key from localStorage
clear(): Clear all localStorage
key(): Passed a number to retrieve the key of a localStorage

if (typeof(Storage) !== "undefined") {
	// Code for localStorage
} else {
	// No web storage Support.
}

Usage:

let MySs = new Storage('session');
MySs.set('introTitle', true);
console.log(MySs.get('introTitle'));
MySs.remove('introTitle');
console.log(MySs.get('introTitle'));

*/

class Storage
{
	/**
	 * @property #_s
	 * @private
	 * @type Object
	 */
	#_s;

	type;
	
    constructor ( type = 'local' ) {
		this.type = type;
		this.#_s = window[`${type}Storage`];
        this.length = this.#_s.length;
    }

	get type () {
		return this.type;
	};

	/**
	 * @method type
	 * @param type {String} Storage type
	 * @return {Object} Self for method chaining
	 */
	set type ( type = 'local' ) {
		this.type = type;
		this.#_s = window[`${type}Storage`];
		return this;
	};

	/**
	 * @method get
	 * @param key {String} Item key
	 * @return {String|Object|Null}
	 */
	get (key) {
		try {
			return JSON.parse(this.#_s.getItem(key));
		} catch(e) {
			return this.#_s.getItem(key);
		}
	}

	/**
	 * @method set
	 * @param key {String} Item key
	 * @param val {String|Object} Item value
	 * @return {String|Object} The value of the item just set
	 */
	set (key, val) {
		this.#_s.setItem(key, JSON.stringify(val));
		return this.get(key);
	};

	/**
	 * @method key
	 * @param index {Number} Item index
	 * @return {String|Null} The item key if found, null if not
	 */
	key (index) {
		if (typeof index === 'number') {
			return this.#_s.key(index);
		}
	}


	/**
	 * @method data
	 * @return {Array|Null} An array containing all items in localStorage through key{string}-value{String|Object} pairs
	 */
	data () {
		let i = 0;
		let data = [];

		while (this.#_s.key(i)) {
			data[i] = [this.#_s.key(i), this.get(this.#_s.key(i))];
			i++;
		}

		return data.length ? data : null;
	}

	/**
	 * @method remove
	 * @param keyOrIndex {String|Number} Item key or index (which will be converted to key anyway)
	 * @return {Boolean} True if the key was found before deletion, false if not
	 */
	remove (keyOrIndex) {
		let result = false;
		let key = (typeof keyOrIndex === 'number') ? this.key(keyOrIndex) : keyOrIndex;

		if (key in this.#_s) {
			result = true;
			this.#_s.removeItem(key);
		}

		return result;
	}

	/**
	 * @method clear
	 * @return {Number} The total of items removed
	 */
	clear () {
		let len = this.#_s.length;
		this.#_s.clear();
		return len;
	}

}
