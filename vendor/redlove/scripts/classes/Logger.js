/**
 * 
 * Reference:
 * https://www.npmjs.com/package/js-logger
 * 
 * Usage:

let MyLogger = new Logger();

 */
class Logger
{
	// Properties

	levels = {
		DEBUG: 'DEBUG',
		INFO: 'INFO',
		WARN: 'WARN',
		ERROR: 'ERROR',
		TRACE: 'TRACE'
	};

	level;
	type;

	handler;

	// Public

	// Methods

	/**
	 * 
	 * @returns object
	 */
    constructor () {
		this.level = this.levels.debug;
		this.type = 'console';
		this.handler = this.handleMessages;
		return this; // For method chaining
    }
	
	/**
	 * 
	 * @returns object
	 */
	log ( message, context ) {
		this.handler(message, context);
		return this; // For method chaining
	}

	/**
	 * 
	 * @returns object
	 */
	debug ( message ) {
		this.handler(message, {level: this.levels.DEBUG});
		return this; // For method chaining
	}

	/**
	 * 
	 * @returns object
	 */
	handleMessages ( messages, context ) {
		// Prefix each log message with a timestamp.
		//messages.unshift(new Date().toUTCString());

		let level = context?.level ?? this.level;
		let type = context?.type ?? this.type;

		if ( type == 'console' )
		{
			console.log(`${new Date().toUTCString()}: `, messages);
		}
	}

}