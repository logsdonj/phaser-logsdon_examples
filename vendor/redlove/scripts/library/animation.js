
// Animation
window.REDLOVE.animation = window.REDLOVE.animation || {};
window.REDLOVE.a = window.REDLOVE.animation;

/* https://animate.style/

Usage:
animateCSS('.my-element', 'bounce');

// or
animateCSS('.my-element', 'bounce').then((message) => {
// Do something after the animation
});
*/
window.REDLOVE.a.animateCSS = function ( element, animation, animationUtilities, prefix = 'animate__' )
{
	// If a jQuery object passed, get DOM element
	if ( typeof(jQuery) === 'function' && element instanceof jQuery )
	{
		element = element[0]; // or .get(0)
	}

	// We create a Promise and return it
	return new Promise((resolve, reject) => 
	{
		const animationName = `${prefix}${animation}`;
		// If a string is passed, assume a selector else already an element
		const node = ( typeof(element) == 'string' ) ? document.querySelector(element) : element;

		// If no node, stop
		if ( ! node ) reject('animateCSS: Node does not exist.');

		// If utilities not an array, start one and map with prefix
		if ( ! Array.isArray(animationUtilities) )
		{
			animationUtilities = animationUtilities ? [animationUtilities] : [];
			animationUtilities = animationUtilities.map(val => `${prefix}${val}`);
		}

		// Create and add to class array
		let classArray = animationUtilities;
		classArray.unshift(animationName);
		classArray.unshift(`${prefix}animated`);
		
		//node.classList.add(`${prefix}animated`, animationName);
		// Use array for adding classes
		node.classList.add(...classArray);

		// When the animation ends, we clean the classes and resolve the Promise
		function handleAnimationEnd(event) {
			event.stopPropagation();
			node.classList.remove(...classArray);
			resolve('animateCSS: Animation ended');
		}

		node.addEventListener('animationend', handleAnimationEnd, {once: true});
	});
};