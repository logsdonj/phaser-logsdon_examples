
/**
 * Helper to get dimensions of an image
 * // https://stackoverflow.com/a/55934241
 * // https://stackoverflow.com/questions/61302149/why-use-url-createobjecturlblob-instead-of-image-src
 * 
 * @param {*} file 
 * @returns 
 * 
 * Usage:
 * 
	// here's how to use the helper
	const getInfo = async ( file ) =>
	{//{ target: { files } }
		//const [file] = files;
	
		try
		{
			const dimensions = await imageDimensions(file);
			console.info(dimensions);
		}
		catch ( error )
		{
			console.error(error);
		}
	}

	const image = 'images/my-image.jpg';
	getInfo(image);

	// Another raw example with onload
	function useImage ( url )
	{
		console.log(`useImage ${url}`);
		// https://stackoverflow.com/a/55934241
		const img = new Image();
		img.onload = function() {
			console.log(this.width, this.height);
		}
		img.src = url;
	}
	useImage(image);
*/
window.REDLOVE.u.imageDimensions = function ( file )
{
	return new Promise(( resolve, reject ) =>
	{
		const img = new Image();

		// the following handler will fire after a successful loading of the image
		img.onload = () => 
		{
			const { naturalWidth: width, naturalHeight: height } = img
			resolve({ width, height })
		}

		// and this handler will fire if there was an error with the image (like if it's not really an image or a corrupted one)
		img.onerror = () =>
		{
			reject('There was some problem with the image.')
		}

		img.src = ( typeof(file) === 'string' ) ? file : URL.createObjectURL(file);
	});
};