
// https://webdesign.tutsplus.com/tutorials/javascript-debounce-and-throttle--cms-36783
// https://blog.webdevsimplified.com/2022-03/debounce-vs-throttle/
// https://medium.com/nerd-for-tech/debouncing-throttling-in-javascript-d36ace200cea
// https://www.youtube.com/watch?v=cjIswDCKgu0

// https://www.freecodecamp.org/news/javascript-debounce-example/
/*
static debounce ( func, timeout = 300 )
{
	let timer;
	return (...args) => {
		clearTimeout(timer);
		timer = setTimeout(() => { 
			func.apply(this, args); 
		}, timeout);
	};
}

static debounce_leading ( func, timeout = 300 )
{
	let timer;
	return (...args) => {
		if (! timer) {
			func.apply(this, args);
		}
		clearTimeout(timer);
		timer = setTimeout(() => {
			timer = undefined;
		}, timeout);
	};
}
*/

// https://davidwalsh.name/javascript-debounce-function
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
/*
static debounce ( func, wait, immediate )
{
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}
*/


// https://davidwalsh.name/javascript-debounce-function
// https://dev.to/monaye/refactor-davidwalsh-s-debounce-function-5afc
/*
const updateModalComponentInfoDebounced = window.REDLOVE.u.debounce(updateModalComponentInfo, 100);

// IIFE version for inline functions
window.addEventListener('scroll', debounce( () => {
	// All the taxing stuff you do
	}, 250)
);

// Creating the debounced function directly
var myEfficientFn = debounce(function() {
	// All the taxing stuff you do
}, 250);
window.addEventListener('resize', myEfficientFn);

// Creating a function separately and then a debounced version for use
var myfunc = function ( str ) {
	console.log(str);
};
var myEfficientFn = debounce(myfunc, 250);
myfunc('Hello1');
myfunc('Hello1');
myfunc('Hello1');
myEfficientFn('Hello2');
myEfficientFn('Hello2');
myEfficientFn('Hello2');
*/
window.REDLOVE.u.debounce = ( func, delay, immediate ) =>
{
	let timerId;
	return (...args) => {
		const boundFunc = func.bind(this, ...args);
		clearTimeout(timerId);
		if (immediate && !timerId) {
			boundFunc();
		}
		const calleeFunc = immediate ? () => { timerId = null } : boundFunc;
		timerId = setTimeout(calleeFunc, delay);
	}
};

window.REDLOVE.u.throttle = ( func, delay, immediate ) =>
{
	let timerId;
	return (...args) => {
		const boundFunc = func.bind(this, ...args);
		if (timerId) {
			return;
		}
		if (immediate && !timerId) {
			boundFunc();
		}
		timerId = setTimeout(() => {
			if(!immediate) {
			boundFunc(); 
			}
			timerId = null; // reset the timer so next call will be excuted
		}, delay);
	}
};
