
// Pad values: https://stackoverflow.com/questions/2686855/is-there-a-javascript-function-that-can-pad-a-string-to-get-to-a-determined-leng/24398129#24398129
window.REDLOVE.u.pad = function ( pad, str, padLeft )
{
	if ( typeof str === 'undefined' )
	{
		return pad;
	}
	
	if ( padLeft )
	{
		return (pad + str).slice(-pad.length);
	}
	else
	{
		return (str + pad).substring(0, pad.length);
	}
};

// Hex

window.REDLOVE.u.randomColorFromHex = function ( colors )
{
	var color = window.REDLOVE.u.randomFromArray(colors);
	return '#' + window.REDLOVE.u.hexString(color);
};

// https://stackoverflow.com/questions/57803/how-to-convert-decimal-to-hex-in-javascript
// https://stackoverflow.com/questions/11023144/working-with-hex-strings-and-hex-values-more-easily-in-javascript
window.REDLOVE.u.hexString = function ( hex )
{
	if ( typeof hex === 'undefined' )
	{
		hex = 0x000000;
	}
	
	var hexString = hex.toString(16);
	return window.REDLOVE.u.pad('000000', hexString, true);
};

// https://stackoverflow.com/questions/1026069/how-do-i-make-the-first-letter-of-a-string-uppercase-in-javascript
window.REDLOVE.u.capitalize = function ( str = '' )
{
	//str[0].toUpperCase() + str.substring(1)
	return str.charAt(0).toUpperCase() + str.slice(1);
};

/**
 * https://stackoverflow.com/questions/5488028/how-do-i-check-for-vowels-in-javascript
 */
window.REDLOVE.u.isVowel = function ( char )
{
	return (['a', 'e', 'i', 'o', 'u'].indexOf(char.toLowerCase()) !== -1);
	//return (/^[aeiou]$/i).test(char);
};

// https://stackoverflow.com/questions/1705057/regex-remove-articles-from-title-the-an-a
window.REDLOVE.u.removeArticles = function ( str )
{
	let regex = /(?:(the|a|an) +)/gi;
	str = str.replace(regex, '');
	return str;
};

/**
 * 
 */
window.REDLOVE.u.isSingular = function ( obj )
{
	if ( typeof(obj) === 'number' )
	{
		return (obj === 1);
	}
	else if ( Array.isArray(obj) )
	{
		return (obj.length === 1);
	}
	else if ( typeof(obj) === 'object' )
	{
		return (Object.keys(obj).length === 1);
	}
};

/**
 * 
 */
window.REDLOVE.u.numberPluralizeVerb = function ( amount, verb )
{
	// If array or object, get its length for the amount
	if ( Array.isArray(amount) )
	{
		amount = amount.length;
	}
	else if ( typeof(amount) === 'object' )
	{
		amount = Object.keys(amount).length;
	}

	let verbLowercase = verb.toLowerCase();

	if ( verb == 'is' || verb == 'are' )
	{
		return (amount === 1) ? 'is' : 'are';
	}

	return verb;
};

/**
 * https://www.grammarly.com/blog/spelling-plurals-with-s-es/
 */
window.REDLOVE.u.wordPluralizeSuffix = function ( word, amount, suffixOnly )
{
	// Don't pluralize if singular
	if ( window.REDLOVE.u.isSingular(amount) )
	{
		return (suffixOnly) ? '' : word;
	}

	// If ending in ch, sh, s, x, or z then assume "es"
	let lastChar = word.slice(-1);
	let last2Chars = word.slice(-2);
	let lastCharMatch = (['s', 'x', 'z'].indexOf(lastChar.toLowerCase()) !== -1);
	let last2CharMatch = (['ch', 'sh'].indexOf(last2Chars.toLowerCase()) !== -1);

	let suffix = (lastCharMatch || last2CharMatch) ? 'es' : 's';
	return (suffixOnly) ? suffix : word + suffix;
};

/**
 * https://www.grammarly.com/blog/spelling-plurals-with-s-es/
 */
window.REDLOVE.u.wordArticlePrefix = function ( word, type )
{
	let firstLetter = word.charAt(0).toLowerCase();
	
	if ( type === 'a' || type === 'an' )
	{
		return window.REDLOVE.u.isVowel(firstLetter) ? 'an' : 'a';
	}

	return 'the';
};

/**
 * 
 */
window.REDLOVE.u.createConjunctionSentence = function ( strings, stringBegin, stringEnd, conjuction )
{
	// Default conjunction
	if ( ! conjuction )
	{
		conjuction = 'and';
	}

	let compiledString = '';

	// If no string, stop
	if ( strings.length == 0 )
	{
		return compiledString;
	}

	// Add passed beginning
	compiledString += stringBegin ?? '';

	for ( let index = 0; index < strings.length; index++ )
	{
		// Append conjunction before item
		// If 2 items and on the last one, append 'and'
		if ( strings.length == 2 && index == 1 )
		{
			compiledString += ` ${conjuction} `;
		}
		// If more than 2 items
		else if ( strings.length > 2 )
		{
			// If on the last, append ', and'
			if ( index == strings.length - 1 )
			{
				compiledString += `, ${conjuction} `;
			}
			// Else if not on the first, append comma
			else if ( index > 0 )
			{
				compiledString += ', ';
			}
		}

		// Add onto string
		compiledString += strings[index];
	}

	// Add passed ending
	compiledString += stringEnd ?? '';
	
	return this.capitalize(compiledString);
};

/**
 * https://gist.github.com/letsgetrandy/1e05a68ea74ba6736eb5
 */
window.REDLOVE.u.verbPastTense = function ( verb )
{
	// language exceptions
	let exceptions = {
		'are': 'were',
		'eat': 'ate',
		'go': 'went',
		'have': 'had',
		'inherit': 'inherited',
		'is': 'was',
		'run': 'ran',
		'sit': 'sat',
		'visit': 'visited'
	}

	if (exceptions[verb]) {
		return exceptions[verb];
	}

	if ((/e$/i).test(verb)) {
		return verb + 'd';
	}

	if ((/[aeiou]c/i).test(verb)) {
		return verb + 'ked';
	}

	// for american english only
	if ((/el$/i).test(verb)) {
		return verb + 'ed';
	}

	if ((/[aeio][aeiou][dlmnprst]$/).test(verb)) {
		return verb + 'ed';
	}
	
	if ((/[aeiou][bdglmnprst]$/i).test(verb)) {
		return verb.replace(/(.+[aeiou])([bdglmnprst])/, '$1$2$2ed');
	}

	return verb + 'ed';
};

/**
 * https://stackoverflow.com/questions/105034/how-do-i-create-a-guid-uuid/62359248
 */
window.REDLOVE.u.uuid = function ()
{
	const url = URL.createObjectURL(new Blob())
	const [id] = url.toString().split('/').reverse()
	URL.revokeObjectURL(url)
	return id
};

window.REDLOVE.u.generateQuickGuid = function ()
{
    return Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15);
	
	// let uniqueId = Date.now().toString(36) + Math.random().toString(36).substring(2);

	// let u = Date.now().toString(16) + Math.random().toString(16) + '0'.repeat(16);
	// let guid = [u.substr(0,8), u.substr(8,4), '4000-8' + u.substr(13,3), u.substr(16,12)].join('-');
}

window.REDLOVE.u.generateUUID = function () // Public Domain/MIT
{ 
    var d = new Date().getTime();//Timestamp
    var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}