// https://stackoverflow.com/a/10717422
// https://stackoverflow.com/a/19033868
// https://stackoverflow.com/a/38515050
window.REDLOVE.u.swapElements = function ( obj1, obj2 )
{
	// If a jQuery object passed, get DOM element
	if ( typeof(jQuery) === 'function' )
	{
		if ( obj1 instanceof jQuery )
		{
			obj1 = obj1[0]; // or .get(0)
		}

		if ( obj2 instanceof jQuery )
		{
			obj2 = obj2[0]; // or .get(0)
		}
	}

	// create marker element and insert it where obj1 is
	var temp = document.createElement('div');
	obj1.parentNode.insertBefore(temp, obj1);

	// move obj1 to right before obj2
	obj2.parentNode.insertBefore(obj1, obj2);

	// move obj2 to right before where obj1 used to be
	temp.parentNode.insertBefore(obj2, temp);

	// remove temporary marker node
	temp.parentNode.removeChild(temp);
};

//https://medium.com/slow-cooked-games-blog/how-to-create-a-mobile-game-on-the-cheap-38a7b75999a7#.7ho2ywm5h
window.REDLOVE.u.calculateDimensions = function ()
{
	var data = {
		ratio : window.innerWidth / window.innerHeight,
		innerWidth : window.innerWidth,
		innerHeight : window.innerHeight,
		clientWidth : document.documentElement.clientWidth,
		clientHeight : document.documentElement.clientHeight,
		devicePixelRatio : window.devicePixelRatio || 1
	};
	
	data.screenWidthMax = Math.max(window.innerWidth * data.devicePixelRatio, data.clientWidth);
	data.screenHeightMax = Math.max(window.innerHeight * data.devicePixelRatio, data.clientHeight);
	data.scaleRatio3rds = data.devicePixelRatio / 3;//http://www.joshmorony.com/how-to-scale-a-game-for-all-device-sizes-in-phaser/
	data.scaleRatio = data.devicePixelRatio;//myAsset.scale.setTo(scaleRatio, scaleRatio)

	return data;
};
