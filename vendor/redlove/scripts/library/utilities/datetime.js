
window.REDLOVE.u.timeExpanded = function ( time_milliseconds )
{
	var time = ( typeof time_milliseconds === 'undefined' ) ? 0 : time_milliseconds;
	var milliseconds = 0;
	var seconds = 0;
	var minutes = 0;
	var hours = 0;
	
	milliseconds = time;
	seconds = time / (1000);
	minutes = time / (1000 * 60);
	hours = time / (1000 * 60 * 60);
	days = time / (1000 * 60 * 60 * 24);
	years = time / (1000 * 60 * 60 * 24 * 364.25);
	
	var tmp_time = time;
	clock_milliseconds = tmp_time % 1000;
	tmp_time = (tmp_time - clock_milliseconds) / 1000;
	clock_seconds = tmp_time % 60;
	tmp_time = (tmp_time - clock_seconds) / 60;
	clock_minutes = tmp_time % 60;
	clock_hours = (tmp_time - clock_minutes) / 60;
	
	return {
		milliseconds : milliseconds,
		seconds : seconds,
		minutes : minutes,
		hours : hours,
		days : days,
		years : years,
		
		clock_milliseconds : clock_milliseconds,
		clock_seconds : clock_seconds,
		clock_minutes : clock_minutes,
		clock_hours : clock_hours,
		
		clock_pad_milliseconds : window.REDLOVE_PHASER_UTILITY.pad('000', clock_milliseconds, true),
		clock_pad_seconds : window.REDLOVE_PHASER_UTILITY.pad('00', clock_seconds, true),
		clock_pad_minutes : window.REDLOVE_PHASER_UTILITY.pad('00', clock_minutes, true),
		clock_pad_hours : window.REDLOVE_PHASER_UTILITY.pad('00', clock_hours, true)
	};
};


window.REDLOVE.u.timeBreakdown = function ( time )
{
	let minuteS = 60;
	let hourS = 60 * 60;

	let timeMs = time;
	let timeS = timeMs / 1000;
	let timeSMod = timeS % 60;
	let timeM = (timeS - timeSMod) / 60;
	let timeMMod = timeM % 60;
	let timeH = (timeM - timeMMod) / 60;

	/*
	console.log(timeS, timeSMod);
	console.log(timeM, timeMMod);
	console.log(timeH);
	*/

	return {
		mToS: minuteS,
		hToS: hourS,
		ms: timeMs,
		s: timeS,
		sMod: timeSMod,
		m: timeM,
		mMod: timeMMod,
		h: timeH
	};
};

window.REDLOVE.u.timeAgo = function ( timeMs )
{
	let timeBd = window.REDLOVE.u.timeBreakdown(timeMs);
	let timeText = '';

	// Tolerance of now
	if ( timeBd.s < 1 )
	{
		// 0 seconds ago, just now
		timeText = 'just now';
	}
	// Tolerance of a minute
	else if ( timeBd.s > (timeBd.mToS - 5) && timeBd.s < (timeBd.mToS + 5) )
	{
		timeText = 'about a minute ago';
	}
	// Tolerance of an hour
	else if ( timeBd.s > (timeBd.hToS - 60) && longAgoS < (timeBd.hToS + 60) )
	{
		timeText = 'about an hour ago';
	}
	// Regular time reporting
	else
	{
		let timeTexts = [];

		if ( timeBd.h > 0 )
		{
			let displayLongAgoH = Math.round(timeBd.h);
			timeTexts.push(`${displayLongAgoH} hour${displayLongAgoH != 1 ? 's' : ''}`);
		}

		if ( timeBd.m > 0 )
		{
			if ( timeBd.mMod > 0 )
			{
				let displayLongAgoMMod = Math.round(timeBd.mMod);
				timeTexts.push(`${displayLongAgoMMod} minute${displayLongAgoMMod != 1 ? 's' : ''}`);
			}
		}

		if ( timeBd.sMod > 0 )
		{
			let displayLongAgoSMod = Math.round(timeBd.sMod);
			timeTexts.push(`${displayLongAgoSMod} second${displayLongAgoSMod != 1 ? 's' : ''}`);
		}

		timeText = timeTexts.join(', ');
		timeText += ` ago`;
	}


	return timeText;
};

window.REDLOVE.u.timeDurationToText = function ( timeMs )
{
	let timeBd = window.REDLOVE.u.timeBreakdown(timeMs);
	let timeText = '';
	let timeTexts = [];

	if ( timeBd.h > 0 )
	{
		let displayTimeH = Math.round(timeBd.h);
		timeTexts.push(`${displayTimeH} hour${displayTimeH != 1 ? 's' : ''}`);
	}

	if ( timeBd.m > 0 )
	{
		if ( timeBd.mMod > 0 )
		{
			let displayTimeMMod = Math.round(timeBd.mMod);
			timeTexts.push(`${displayTimeMMod} minute${displayTimeMMod != 1 ? 's' : ''}`);
		}
	}

	if ( timeBd.sMod > 0 )
	{
		let displayTimeSMod = Math.round(timeBd.sMod);
		timeTexts.push(`${displayTimeSMod} second${displayTimeSMod != 1 ? 's' : ''}`);
	}

	timeText = timeTexts.join(', ');


	return timeText;
	
	/*
	// https://stackoverflow.com/questions/3552461/how-do-i-format-a-date-in-javascript
	let addDate = new Date(challenge.addTime);
	let dateOptions = {month: 'numeric', day: 'numeric'};//year: 'numeric', 
	let localDate = addDate.toLocaleDateString('en', dateOptions);
	let timeOptions = {hour12: true, hour: 'numeric', minute: '2-digit', second: '2-digit'};//year: 'numeric', 
	let localTime = addDate.toLocaleTimeString('en', timeOptions);

	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/RelativeTimeFormat/format
	// Create a relative time formatter in your locale
	let rtf = new Intl.RelativeTimeFormat('en', { numeric: "auto" });

	let rtf = new Intl.RelativeTimeFormat("en", {
		localeMatcher: "best fit", // other values: "lookup"
		numeric: "always", // other values: "auto"
		style: "long", // other values: "short" or "narrow"
	});

	"quarter", "month", "week", "day", "hour", "minute", "second"
	*/
};
