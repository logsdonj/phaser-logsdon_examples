
window.REDLOVE.u.valueAtIndex = function ( obj, index )
{
	return obj[ Object.keys(obj)[index] ];
};

window.REDLOVE.u.objLength = function ( obj )
{
	return Object.keys(obj).length;
};

window.REDLOVE.u.isArray = function ( object )
{
	return Array.isArray(object);
};

// https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
window.REDLOVE.u.shuffleArray = function ( array )
{
	for ( let i = array.length - 1; i > 0; i-- )
	{
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
};

// https://stackoverflow.com/questions/5915096/get-a-random-item-from-a-javascript-array
window.REDLOVE.u.randomFromArray = function ( arr )
{
	return arr[arr.length * Math.random() | 0];
	//return arr[Math.floor(Math.random() * arr.length)];
};

// https://stackoverflow.com/questions/14453670/remove-an-empty-string-from-array-of-strings-jquery
// https://stackoverflow.com/questions/55685037/how-to-remove-empty-array-values-from-an-array
window.REDLOVE.u.filterArrayEmpty = function ( arr )
{
	return arr.filter(el => el != null && el.trim() != '');
	/*
	return arr.filter(function (el) {
		return ( el != null && el.trim() != '' );
	});
	*/
};

window.REDLOVE.u.isObject = function ( object )
{
	return ( 
		object && 
		typeof object === 'object' && 
		object !== null && 
		! Array.isArray(object) && 
		object.constructor === Object 
	);
};

//https://stackoverflow.com/questions/679915/how-do-i-test-for-an-empty-javascript-object
window.REDLOVE.u.isObjectEmpty = function ( object )
{
	for (var prop in object)
	{
		if (object.hasOwnProperty(prop))
		{
			return false;
		}
	}

	return ( JSON.stringify(object) === JSON.stringify({}) );
};

// https://stackoverflow.com/a/1527820

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
window.REDLOVE.u.getRandomArbitrary = function ( min, max )
{
	return Math.random() * (max - min) + min;
};

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
window.REDLOVE.u.getRandomInt = function ( min, max )
{
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

window.REDLOVE.u.deepCloneValues = function ( obj )
{
	// Init if no data or an empty object: if ( typeof(data) === 'undefined' || JSON.stringify(data) === JSON.stringify({}) )
	// Deep clone: JSON.parse(JSON.stringify(defaultData))
	// Return a new object, but only shallow copies property values: Object.assign({}, defaultData, data);
	// Merge data with object spread: data = {...defaultData, ...data};
	//data = { ...defaultData, ...data };

	return JSON.parse(JSON.stringify(obj));
};

window.REDLOVE.u.deepClone = function ( obj )
{
	// Create a deep copy
	// https://blog.logrocket.com/methods-for-deep-cloning-objects-in-javascript/
	// https://web.dev/structured-clone/
	// Shallow: Object.assign({}, dbItem);
	// Deep, but limited: JSON.parse(JSON.stringify(myOriginal));
	// Deep: structuredClone(myOriginal)
	
	// Init if no data or an empty object: if ( typeof(data) === 'undefined' || JSON.stringify(data) === JSON.stringify({}) )
	// Deep clone: JSON.parse(JSON.stringify(defaultData))
	// Return a new object, but only shallow copies property values: Object.assign({}, defaultData, data);
	// Merge data with object spread: data = {...defaultData, ...data};
	//data = { ...defaultData, ...data };

	//return JSON.parse(JSON.stringify(obj));
	return structuredClone(obj);
};

/**
 * 
 * Example:
 * let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = mergeWhitelistObject(foo, bar);
 * fooBar == {a: 1, b: 3}
 * 
 * @param {*} whitelistObject 
 * @param {*} passedObj 
 */
window.REDLOVE.u.mergeWhitelistObject = function ( whitelistObject, passedObj )
{
	// Ensure passedObj is an object
	if ( typeof(passedObj) !== 'object' )
	{
		passedObj = {};
	}

	// Stop if parameters are not objects
	if ( typeof(whitelistObject) !== 'object' || typeof(passedObj) !== 'object' )
	{
		return;
	}
	
	// Get allowed keys
	const allowedKeys = Object.keys(whitelistObject);

	// Filter object using allowed keys
	// https://stackoverflow.com/questions/38750705/filter-object-properties-by-key-in-es6

	/* New object will have all whitelist entries and passed ones overwritten
	It should be unnecessary to merge the object later:
	
	let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = mergeWhitelistObject(foo, bar);
	fooBar == {a: 1, b: 3}
	
	newObj = Object.assign({}, this.defaultObj, filteredObj);
	*/
	// https://stackoverflow.com/a/60379733
	// For allowed keys
	const filteredObj = allowedKeys.reduce((obj, key) => {
		// If it exists in the passedObj, use it or default
		obj[key] = passedObj[key] ?? whitelistObject[key];
		return obj;
	}, {});

	return filteredObj;
};

/**
 * 
 * Example:
 * let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = whitelistObjectByKeys(foo, bar);
 * fooBar == {b: 3}
 * 
 * @param {*} allowedKeys 
 * @param {*} passedObj 
 */
window.REDLOVE.u.whitelistObjectByKeys = function ( allowedKeys, passedObj )
{
	// Ensure passedObj is an object
	if ( typeof(passedObj) !== 'object' )
	{
		passedObj = {};
	}

	// Stop if parameters are not objects
	if ( typeof(allowedKeys) !== 'object' || typeof(passedObj) !== 'object' )
	{
		return;
	}

	// Ensure working with an array of keys
	if ( Array.isArray(allowedKeys) === false )
	{
		// Get allowed keys
		allowedKeys = Object.keys(allowedKeys);
	}

	// Filter object using allowed keys
	// https://stackoverflow.com/questions/38750705/filter-object-properties-by-key-in-es6

	/* Option 1 - New object will only have passed whitelist keys and values
	This means you may still need to merge the object later: 

	let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = whitelistObjectByKeys(foo, bar);
	fooBar == {b: 3}

	newObj = Object.assign({}, this.defaultObj, filteredObj);
	*/
	// https://stackoverflow.com/a/38750895
	// For passedObj keys
	const filteredObj = Object.keys(passedObj)
	// Use conditional in reduce for speed
	//.filter(key => allowedKeys.includes(key))
	.reduce((obj, key) => {
		// If the key is found, use value
		if ( allowedKeys.indexOf(key) !== -1 )
		{
			obj[key] = passedObj[key];
		}
		return obj;
	}, {});
	
	/* Option 2 - New object will have all whitelist keys but only passed values defined
	
	let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = whitelistObjectByKeys(foo, bar);
	fooBar == {a: undefined, b: 3}

	// https://stackoverflow.com/a/60379733
	// For allowed keys
	const filteredObj = allowedKeys.reduce((obj, key) => {
		// Use existing value or undefined
		obj[key] = passedObj[key];
		return obj;
	}, {});
	*/

	return filteredObj;
};

/**
 * 
 * Example:
 * let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = whitelistObjectByKeys(foo, bar);
 * fooBar == {b: 3}
 * 
 * @param {*} allowedKeys 
 * @param {*} passedObj 
 */
window.REDLOVE.u.blacklistObjectByKeys = function ( disallowedKeys, passedObj )
{
	// Ensure passedObj is an object
	if ( typeof(passedObj) !== 'object' )
	{
		passedObj = {};
	}

	// Stop if parameters are not objects
	if ( typeof(disallowedKeys) !== 'object' || typeof(passedObj) !== 'object' )
	{
		return;
	}

	// Ensure working with an array of keys
	if ( Array.isArray(disallowedKeys) === false )
	{
		// Get allowed keys
		disallowedKeys = Object.keys(disallowedKeys);
	}

	// Filter object using disallowed keys

	// For passedObj keys
	const filteredObj = Object.keys(passedObj)
	// Use conditional in reduce for speed
	//.filter(key => disallowedKeys.includes(key))
	.reduce((obj, key) => {
		// If the key is not found, use value
		if ( disallowedKeys.indexOf(key) === -1 )
		{
			obj[key] = passedObj[key];
		}
		return obj;
	}, {});
	
	return filteredObj;
};

window.REDLOVE.u.sortByTypeName = function ( recordsObject, turnIntoArrayOfIds )
{
	// If an object not passed, return empty array
	if ( typeof(recordsObject) != 'object' )
	{
		return [];
	}

	// Turn object into an array for sorting
	let param;
	let recordsArray = [];
	const recordKeys = Object.keys(recordsObject);
	recordKeys.forEach(key => {
		const record = {
			id: key,
			type: recordsObject[key].type ?? '',
			name: recordsObject[key].name ?? ''
		};
		recordsArray.push(record);
	});

	// Sort by name
	param = 'name';
	recordsArray.sort((a, b) => {
		a = a[param] ?? '';
		b = b[param] ?? '';
		if ( a == b ) return 0;
		return ( a > b ) ? 1 : -1;
	});

	// Sort by type
	param = 'type';
	recordsArray.sort((a, b) => {
		a = a[param] ?? '';
		b = b[param] ?? '';
		if ( a == b ) return 0;
		return ( a > b ) ? 1 : -1;
	});

	// If turning into array of ids
	if ( turnIntoArrayOfIds )
	{
		// Put back into an array of keys/ids as if they were that way from the object
		let recordIds = [];
		recordsArray.forEach(arrayItem => {
			recordIds.push(arrayItem.id);
		});
		return recordIds;
	}

	return recordsArray;
};

/**
 * 
 * @param {*} recordsObject 
 * @param {*} orderKeys 
 * @param {*} turnIntoArrayOfIds 
 * @returns 
 * 
 * recordsObject example:
 * const records = {
 * 	"one": {'type': "primary", "name": "Item B"},
 * 	"two": {'type': "primary", "name": "Item A"},
 * }
 * 
 * const sortedIds = window.REDLOVE.u.sortObjectBy(records, ['type', 'name'], true)
 * 
 */
window.REDLOVE.u.sortObjectBy = function ( recordsObject, orderKeys, turnIntoArrayOfIds )
{
	// If an object not passed, return empty array
	if ( typeof(recordsObject) != 'object' )
	{
		return [];
	}

	// Turn object into an array for sorting
	let param;
	let recordsArray = [];
	const recordKeys = Object.keys(recordsObject);
	recordKeys.forEach(key => {
		const record = {
			id: key
		};

		orderKeys.forEach(orderKey => {
			record[orderKey] = recordsObject[key][orderKey] ?? ''
		});

		recordsArray.push(record);
	});

	// Sort by order key, do reverse to maintain proper order
	const orderKeysReversed = orderKeys.reverse();
	orderKeysReversed.forEach(orderKey => {
		recordsArray.sort((a, b) => {
			a = a[orderKey] ?? '';
			b = b[orderKey] ?? '';
			if ( a == b ) return 0;
			return ( a > b ) ? 1 : -1;
		});
	});

	// If turning into array of ids
	if ( turnIntoArrayOfIds )
	{
		// Put back into an array of keys/ids as if they were that way from the object
		let recordIds = [];
		recordsArray.forEach(arrayItem => {
			recordIds.push(arrayItem.id);
		});
		return recordIds;
	}

	return recordsArray;
};

/*
// Examples of JSON handling
let text = '{ "employees" : [' +
'{ "firstName":"John" , "lastName":"Doe" },' +
'{ "firstName":"Anna" , "lastName":"Smith" },' +
'{ "firstName":"Peter" , "lastName":"Jones" } ]}';
const obj = JSON.parse(text);
//console.log(obj);
//console.log(JSON.stringify(obj));
*/
// https://stackoverflow.com/questions/29797946/handling-bad-json-parse-in-node-safely
window.REDLOVE.u.safelyParseJSON = function ( json )
{
	// This function cannot be optimised, it's best to keep it small!
	let parsed;

	try {
		parsed = JSON.parse(json)
	} catch (e) {
		// Oh well, but whatever...
	}

	return parsed;// Could be undefined!
};
