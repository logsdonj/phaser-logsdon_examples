
window.REDLOVE.u.roundToDecimals = function ( value, decimals )
{
	// https://www.jacklmoore.com/notes/rounding-in-javascript/
	//return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);

	// https://stackoverflow.com/a/11832950
	return Math.round((value + Number.EPSILON) * (10 * decimals)) / (10 * decimals);
};
/**
 * Get a random floating point number between `min` and `max`.
 * 
 * https://greensock.com/forums/topic/21306-creating-and-maintaining-an-animation-queue/
 * https://bobbyhadz.com/blog/javascript-get-random-float-in-range
 * 
 * @param {number} min - min number
 * @param {number} max - max number
 * @return {number} a random floating point number
 */
window.REDLOVE.u.getRandomFloat = function ( min, max )
{
	if ( max == null )
	{
		max = min;
		min = 0;
	}

	if ( min > max )
	{
		const tmp = min;
		min = max;
		max = tmp;
	}

	return Math.random() * (max - min) + min;
}

// https://stackoverflow.com/a/1527820

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
window.REDLOVE.u.getRandomArbitrary = function ( min, max )
{
	return Math.random() * (max - min) + min;
};

/**
 * Get a random integer between `min` and `max`.
 * 
 * @param {number} min - min number
 * @param {number} max - max number
 * @return {number} a random integer
 * 
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 * 
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 * getRandomIntInclusive
 */
window.REDLOVE.u.getRandomInt = function ( min, max )
{
	min = Math.ceil(min);
	max = Math.floor(max);
	// The maximum is inclusive and the minimum is inclusive
	return Math.floor(Math.random() * (max - min + 1) + min);
};

/**
 * Get a random boolean value.
 * https://gist.github.com/kerimdzhanov/7529623
 * 
 * @return {boolean} a random true/false
 */
window.REDLOVE.u.getRandomBool = function ()
{
	return Math.random() >= 0.5;
};