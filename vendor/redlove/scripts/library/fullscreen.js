
// Fullscreen
window.REDLOVE.fullscreen = window.REDLOVE.fullscreen || {};

window.REDLOVE.fullscreen = {

	// https://blog.bitsrc.io/building-fullscreen-web-apps-dfc2286b7049
	toggle: ( toggle, element ) =>
	{
		if ( window.REDLOVE.fullscreen.isEnabled() )
		{
			// Exit fullscreen
			if ( window.REDLOVE.fullscreen.getElement() && toggle !== true )
			{
				return window.REDLOVE.fullscreen.exit();
			}
			// Request fullscreen
			else
			{
				return window.REDLOVE.fullscreen.enter(element);
			}
		}
		else
		{
			console.log('Fullscreen is not supported.');
			return;
		}
	},

	enter: ( element ) =>
	{
		// Default to document
		element = element ?? document.documentElement;
		// Available vendor prefix functions
		const functionNames = ['requestFullscreen', 'webkitRequestFullScreen', 'mozRequestFullScreen', 'msRequestFullscreen'];

		// Try each available function
		for ( const functionName of functionNames )
		{
			if ( element[functionName] )
			{
				element[functionName]()
				.catch(function(error) {
					console.log(`${error.name}: ${error.message}`, error);
					return false;
				});

				break;
			}
		}

		return true;
	},

	exit: ( element ) =>
	{
		// Available vendor prefix functions
		const functionNames = ['exitFullscreen', 'webkitExitFullscreen', 'mozCancelFullScreen', 'msExitFullscreen'];
		
		// Try each available function
		for ( const functionName of functionNames )
		{
			if ( document[functionName] )
			{
				document[functionName]();
				break;
			}
		}

		return true;
	},

	isEnabled: () =>
	{
		return Boolean( document.fullscreenEnabled ?? webkitFullscreenEnabled ?? document.mozFullscreenEnabled ?? document.msFullscreenEnabled );
	},

	getElement: () =>
	{
		const fullScreenElement = document.fullscreenElement ?? document.webkitFullscreenElement ?? document.mozFullScreenElement ?? document.msFullscreenElement;
		return ( fullScreenElement && fullScreenElement !== null ? fullScreenElement : false );
	},

	is: () =>
	{
		return Boolean( 
			window.REDLOVE.fullscreen.isEnabled() && 
			window.REDLOVE.fullscreen.getElement() !== false 
		);
	}

};
