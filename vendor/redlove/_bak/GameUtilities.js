
class GameUtilities
{
	static roundToDecimals = function round(value, decimals)
	{
		// https://www.jacklmoore.com/notes/rounding-in-javascript/
		//return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);

		// https://stackoverflow.com/a/11832950
		return Math.round((value + Number.EPSILON) * (10 * decimals)) / (10 * decimals);
	};

	static checkGestures(pointer)
	{
		var data = {};

		var pointerDownDuration = pointer.upTime - pointer.downTime;

		data.pointerDownDuration = pointerDownDuration;

		//var angle = Phaser.Geom.Line.Angle(line);
		var startPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
		var stopPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
		var angle = Phaser.Math.Angle.BetweenPoints(startPoint, stopPoint);
		angle = Phaser.Math.Angle.Normalize(Phaser.Math.Angle.Wrap(angle));

		data.startPoint = startPoint;
		data.stopPoint = stopPoint;
		data.angle = angle;
		data.angleRadians = angle;
		data.angleDegrees = Phaser.Math.RadToDeg(angle);

		// Check for swipe
		var swipeMinTime = gameOptions.swipeMinTime || 0;
		var swipeMaxTime = gameOptions.swipeMaxTime || 1000;
		var isSwipeDuration = pointerDownDuration >= swipeMinTime && pointerDownDuration <= swipeMaxTime;
		var swipeDistancePoint = new Phaser.Geom.Point(pointer.upX - pointer.downX, pointer.upY - pointer.downY);
		var swipeMagnitude = Phaser.Geom.Point.GetMagnitude(swipeDistancePoint);
		var isSwipeDistance = swipeMagnitude > gameOptions.swipeMinDistance;
		var isSwipe = isSwipeDuration && isSwipeDistance;
		
		data.swipeMinTime = swipeMinTime;
		data.swipeMaxTime = swipeMaxTime;
		data.isSwipeDuration = isSwipeDuration;
		data.swipeDistancePoint = swipeDistancePoint;
		data.swipeMagnitude = swipeMagnitude;
		data.isSwipeDistance = isSwipeDistance;
		data.isSwipe = isSwipe;

		if (isSwipe)
		{
			console.log('swipe');
			console.log('pointerDownDuration: ' + pointerDownDuration + ' ms; isSwipeDuration: ' + isSwipeDuration);
			console.log('swipeDistancePoint: ' + swipeDistancePoint.x + ', '+ swipeDistancePoint.y + '; swipeMagnitude: ' + swipeMagnitude + '; isSwipeDistance: ' + isSwipeDistance);
			console.log('swipe angle Radians: ' + angle + '; Degrees: ' + Phaser.Math.RadToDeg(angle));
			
			// Normalize magnitude
			var normalizedSwipePoint = new Phaser.Geom.Point(swipeDistancePoint.x, swipeDistancePoint.y);
			Phaser.Geom.Point.SetMagnitude(normalizedSwipePoint, 1);
			var minNormalRadians = gameOptions.swipeMinNormal || 0.85;

			if (swipeDistancePoint.x > minNormalRadians)
			{
				data.swipeDirection = 'right';
			}
			else if (swipeDistancePoint.x < -minNormalRadians)
			{
				data.swipeDirection = 'left';
			}
			else if (swipeDistancePoint.y > minNormalRadians)
			{
				data.swipeDirection = 'down';
			}
			else if (swipeDistancePoint.y < -minNormalRadians)
			{
				data.swipeDirection = 'up';
			}

			data.swipeLine = new Phaser.Geom.Line(pointer.downX, pointer.downY, pointer.upX, pointer.upY);
			data.swipeLineLength = Phaser.Geom.Line.Length(data.swipeLine);
		}

		return data;
	}

	static checkSwipeIntersect(gestureData, obj)
	{
		var rect = obj;

		if (typeof(obj.type) !== 'undefined')
		{
			if (obj.type == 'Image')
			{
				rect = obj.getBounds();
			}
		}

		// Check swipe line intersection
		if (Phaser.Geom.Intersects.LineToRectangle(gestureData.swipeLine, rect))
		{
			// Return intersection points
			return Phaser.Geom.Intersects.GetLineToRectangle(gestureData.swipeLine, rect);
		}

		return false;
	}
	
	static checkIntersectionSide(rect, point)
	{
		var leftX = rect.left;
		var rightX = rect.right;
		var topY = rect.top;
		var bottomY = rect.bottom;
		
		// Top intersect
		if (point.y == topY)
		{
			return 'top';
		}
		// Right intersect
		else if (point.x == rightX)
		{
			return 'right';
		}
		// Bottom intersect
		else if (point.y == bottomY)
		{
			return 'bottom';
		}
		// Left intersect
		else if (point.x == leftX)
		{
			return 'left';
		}
	}

	static getPolygonsFromIntersectPoints(obj, intersectPoints)
	{
		var objRect = obj;

		// Start collecting polygons
		var curPoly = 'A';
		var polyObjs = {'A': [], 'B': []};
		// When hit an intersection, start collecting for B and continue switching back and forth on intersects
		var rectPoints = objRect.getPoints(4);

		// Loop over rectangle points
		for (var rectPointIndex = 0; rectPointIndex < rectPoints.length; rectPointIndex++)
		{
			var rectPoint = rectPoints[rectPointIndex];
			var intersectingPoint = null;

			// Loop over intersecting points
			for (var intersectPointIndex = 0; intersectPointIndex < intersectPoints.length; intersectPointIndex++)
			{
				var intersectPoint = intersectPoints[intersectPointIndex];
				// Check for intersection on rectangle line
				if (intersectPoint.x == rectPoint.x || intersectPoint.y == rectPoint.y)
				{
					intersectingPoint = intersectPoint;
					// Remove the intersect point now that it has been handled
					intersectPoints.splice(intersectPointIndex, 1);
					break;
				}
			}
			
			if (intersectingPoint)
			{
				// Determine placement of the intersection before or after the rectangle point
				var rectBeforeIntersection = true;

				if (
					(rectPointIndex == 0 && intersectingPoint.intersectSide == 'left') || 
					(rectPointIndex == 1 && intersectingPoint.intersectSide == 'top') || 
					(rectPointIndex == 2 && intersectingPoint.intersectSide == 'right') || 
					(rectPointIndex == 3 && intersectingPoint.intersectSide == 'bottom')
				)
				{
					rectBeforeIntersection = false;
				}

				// Add the rectangle point to the polygon
				if (rectBeforeIntersection)
				{
					polyObjs[curPoly].push(rectPoint);
				}

				// Add the polygon, switch to the other polygon, and continue main iteration
				polyObjs[curPoly].push(intersectingPoint);
				curPoly = (curPoly == 'A') ? 'B' : 'A';
				polyObjs[curPoly].push(intersectingPoint);

				// Add the rectangle point to the polygon
				if (! rectBeforeIntersection)
				{
					polyObjs[curPoly].push(rectPoint);
				}
			}
			else
			{
				// Add the rectangle point to the polygon
				polyObjs[curPoly].push(rectPoint);
			}
		}

		return polyObjs;
	}
	
	/*
	var rotationDirection = this.orientationTest(rectangleCenterPoint, this.polygonPoints[this.polygonPoints.length - 2], this.polygonPoints[this.polygonPoints.length - 1]);//isLeft
	*/
	static orientationTest(point1, point2, point3)
	{
		if (typeof(point1) === 'undefined' || typeof(point2) === 'undefined' || typeof(point3) === 'undefined')
		{
			return 0;
		}

		// Orientation test
		// https://www.geeksforgeeks.org/orientation-3-ordered-points/
		// https://math.stackexchange.com/questions/128061/check-if-point-is-on-or-below-line-when-either-x-or-y-0
		// http://www.cs.tufts.edu/comp/163/OrientationTests.pdf

		// The signed area from the points tells us the area of the triangle, and the sign gives the orientation (left-turning or right-turning)
		var val = ((point2.y - point1.y) * (point3.x - point2.x)) - ((point2.x - point1.x) * (point3.y - point2.y));
		
		// clock or counterclock wise
		return (val > 0) ? -1: 1;

		// Colinear
		//if (val == 0) return 0;
	}
	/*
	isLeft(vectorA, vectorB, vectorC)
	{
		if (typeof(vectorA) === 'undefined' || typeof(vectorB) === 'undefined' || typeof(vectorC) === 'undefined')
		{
			return 0;
		}
		return ((vectorB.x - vectorA.x) * (vectorC.y - vectorA.y) - (vectorB.y - vectorA.y) * (vectorC.x - vectorA.x)) > 0;
	}
	*/

	/*
	// Use the middle of the screen and the pointer to test dpad ability
	//var originPoint = new Phaser.Geom.Point(this.anchorPoint.x, this.anchorPoint.y);
	var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
	var targetPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
	console.log(this.getAxisDirection(originPoint, targetPoint));
	*/
	static getAxisDirection(originPoint, targetPoint, targetDimension)
	{
		var translatedTargetPoint = window.REDLOVE_PHASER_UTILITY.deepCloneValues(targetPoint);

		// Check if translating the point that could be in a rectangle to a square so we can do circle instead of ellipse calculations
		if ( 
			typeof(targetDimension) === 'object' && 
			typeof(targetDimension.width) === 'number' && 
			typeof(targetDimension.height) === 'number' 
		)
		{
			// Imagine a square |_| made from the minium touchpad area (which could be a rectangle |___|) dimension.
			// We can find size ratios and translate the points made in the rectangle to equivalent points made in the square.
			// We do this because circle math works with uniform sides while ellipse math would be needed with rectangles.
			// We can then find the angles between points in the square/circle instead of trying to find the stretched angles in the rectangle/ellipse.

			// Get the minimum touchpad dimension to use as the imagined square side
			var minDimension = Math.min(targetDimension.width, targetDimension.height);
			// Get a square ratio against touchpad dimensions
			var ratioX = minDimension / targetDimension.width;
			var ratioY = minDimension / targetDimension.height;
			var diffX = translatedTargetPoint.x - originPoint.x;
			var diffY = translatedTargetPoint.y - originPoint.y;
			// Translate the touchpad point to the square
			translatedTargetPoint.x = originPoint.x + (diffX * ratioX);
			translatedTargetPoint.y = originPoint.y + (diffY * ratioY);
			// Now the point in the square can be used with circle math for things like finding the angle between points
		}

		// Get the angle between the points
		var angle = Phaser.Math.Angle.BetweenPoints(originPoint, translatedTargetPoint);
		// Wrap and normalize the angle to 0-360 instead of -180-180
		var angleWrapped = Phaser.Math.Angle.Wrap(angle);
		var angleNormalized = Phaser.Math.Angle.Normalize(angleWrapped);
		var angleDegrees = Phaser.Math.RadToDeg(angleNormalized);
		// Get the axis direction lookup info
		var axisLookup = GameUtilities.getAxisLookup(angleDegrees);
		// Add angle information
		axisLookup.angle = angle;
		axisLookup.angleWrapped = angleWrapped;
		axisLookup.angleNormalized = angleNormalized;
		axisLookup.angleRadians = angleNormalized;
		axisLookup.angleDegrees = angleDegrees;
		axisLookup.originPoint = originPoint;
		axisLookup.targetPoint = targetPoint;
		axisLookup.translatedTargetPoint = translatedTargetPoint;
		return axisLookup;
	}

	static getAxisLookup(normalizedDegrees, isRadians, isNormalized)
	{
		// If raw radians are being used instead of normalized degrees, perform operations
		if (typeof(isRadians) !== 'undefined')
		{
			normalizedDegrees = Phaser.Math.Angle.Wrap(normalizedDegrees);
			
			if (typeof(isNormalized) === 'undefined')
			{
				normalizedDegreesPhaser.Math.Angle.Normalize(normalizedDegrees);
			}

			normalizedDegrees = Phaser.Math.RadToDeg(angleNormalized);
		}

		// Lookup arrays broken down into 16 circle segments of 22.5 degrees in a 360 degree circle
		// Starting at 0 to the right at 3 o'clock and going around clockwise

		// 2-way directional pad
		// Lookup for left and right along the X axis
		var segmentDirectionLookup2X = [
			'right',
			'right',
			'right',
			'right',
			'left',
			'left',
			'left',
			'left',
			'left',
			'left',
			'left',
			'left',
			'right',
			'right',
			'right',
			'right'
		];

		// 2-way directional pad
		// Lookup for up and down along the Y axis
		var segmentDirectionLookup2Y = [
			'down',
			'down',
			'down',
			'down',
			'down',
			'down',
			'down',
			'down',
			'up',
			'up',
			'up',
			'up',
			'up',
			'up',
			'up',
			'up'
		];

		// 4-way directional pad
		// These segments are like taking a circle and making an X across it
		// Lookup for left, right, up, and down along the X and Y axis
		var segmentDirectionLookup4 = [
			'right',
			'right',
			'down',
			'down',
			'down',
			'down',
			'left',
			'left',
			'left',
			'left',
			'up',
			'up',
			'up',
			'up',
			'right',
			'right'
		];

		// 8-way directional pad
		// These segments are putting 2 narrow perpendicular Xs across a circle
		// Lookup for left, right, up, and down along the X and Y axis and diaganals
		var segmentDirectionLookup8 = [
			'right',
			'down-right',
			'down-right',
			'down',
			'down',
			'down-left',
			'down-left',
			'left',
			'left',
			'up-left',
			'up-left',
			'up',
			'up',
			'up-right',
			'up-right',
			'right'
		];

		// Break a circle up into 16 segments of 22.5 degrees
		var degreeIncrement = 22.5;
		// Determene which segment the angle is in by how many whole increments go into it
		// Use the floor to get an array index starting at 0
		var segmentIndex = Math.floor(normalizedDegrees / degreeIncrement);

		var directionDegreesLookup = {
			right: 0,
			'down-right': 45,
			down: 90,
			'down-left': 135,
			left: 180,
			'up-left': 225,
			up: 270,
			'up-right': 315
		};
		
		// Determine axis directions from segment from angle
		var axisLookup = {
			degrees: normalizedDegrees,
			degreeIncrement: degreeIncrement,
			segmentIndex: segmentIndex,
			twoX: {
				directionDegrees: directionDegreesLookup[segmentDirectionLookup2X[segmentIndex]],
				down: false,
				left: (segmentDirectionLookup2X[segmentIndex] == 'left'),
				raw: segmentDirectionLookup2X[segmentIndex],
				right: (segmentDirectionLookup2X[segmentIndex] == 'right'),
				up: false
			},
			twoY: {
				directionDegrees: directionDegreesLookup[segmentDirectionLookup2Y[segmentIndex]],
				down: (segmentDirectionLookup2Y[segmentIndex] == 'down'),
				left: false,
				raw: segmentDirectionLookup2Y[segmentIndex],
				right: false,
				up: (segmentDirectionLookup2Y[segmentIndex] == 'up')
			},
			four: {
				directionDegrees: directionDegreesLookup[segmentDirectionLookup4[segmentIndex]],
				down: (segmentDirectionLookup4[segmentIndex] == 'down'),
				left: (segmentDirectionLookup4[segmentIndex] == 'left'),
				raw: segmentDirectionLookup4[segmentIndex],
				right: (segmentDirectionLookup4[segmentIndex] == 'right'),
				up: (segmentDirectionLookup4[segmentIndex] == 'up')
			},
			eight: {
				directionDegrees: directionDegreesLookup[segmentDirectionLookup8[segmentIndex]],
				down: (segmentDirectionLookup8[segmentIndex].indexOf('down') >= 0),
				left: (segmentDirectionLookup8[segmentIndex].indexOf('left') >= 0),
				raw: segmentDirectionLookup8[segmentIndex],
				right: (segmentDirectionLookup8[segmentIndex].indexOf('right') >= 0),
				up: (segmentDirectionLookup8[segmentIndex].indexOf('up') >= 0)
			},
		};

		// 4-way diagonal-only
		axisLookup.fourD = {
			upLeft: axisLookup.twoY.up && axisLookup.twoX.left,
			upRight: axisLookup.twoY.up && axisLookup.twoX.right,
			downLeft: axisLookup.twoY.down && axisLookup.twoX.left,
			downRight: axisLookup.twoY.down && axisLookup.twoX.right
		};

		/*
		console.log('Degrees: ' + angleDegrees + ' | Index: ' + segmentIndex);
		console.log(axisLookup);
		*/

		return axisLookup;
	}

	static drawAxisSegments(scene, originPoint, segmentLineLength)
	{
		// Create a line and reuse it to rotate around a circle like a clock hand
		var segmentLineLength = segmentLineLength || 100;
		var segmentLine = new Phaser.Geom.Line();

		// Create graphics to reset and display
		scene.segmentDivisionGraphics = scene.segmentDivisionGraphics || scene.add.graphics();
		var segmentDivisionGraphics = scene.segmentDivisionGraphics;
		// Clear segment lines
		segmentDivisionGraphics.clear();
		// Break a 360 degree circle up into 16 segments
		var degreeIncrement = 22.5;
		var degreeIteration = 0;
		for (var tmpDegrees = 0; tmpDegrees < 360; tmpDegrees += degreeIncrement)
		{
			degreeIteration++;

			// Determine the lookup index
			var segmentIndex = tmpDegrees / degreeIncrement;
			//console.log('Draw - Degrees: ' + tmpDegrees + ' - ' + (tmpDegrees + degreeIncrement) + ' | Index: ' + segmentIndex);

			// Degrees start to the right at 0 (3 o'clock) and go counterclockwise to 360
			
			// Draw segment lines, changing the style when in the middle of a segment
			if (degreeIteration % 2 > 0)
			{
				segmentDivisionGraphics.lineStyle(1, 0x00ff00, 0.15);
			}
			else
			{
				segmentDivisionGraphics.lineStyle(1, 0x00ff00, 1);
			}

			// Use radians for the angle
			Phaser.Geom.Line.SetToAngle(segmentLine, originPoint.x, originPoint.y, Phaser.Math.DegToRad(tmpDegrees), segmentLineLength);
			segmentDivisionGraphics.strokeLineShape(segmentLine);
		}
	}


	

    static constrainObjectPosition(object, bounds)
    {
		// If a Phaser Game
		if (bounds && typeof(bounds) === 'object' && bounds.constructor.name === 'Game')
		{
			bounds = {
				minX: 0,
				maxX: bounds.scale.width,
				minY: 0,
				maxY: bounds.scale.height,
			};
		}
	
		// Constrain x
		if (object.x < bounds.minX)
		{
			object.x = bounds.minX;
		}
		else if (object.x > bounds.maxX)
		{
			object.x = bounds.maxX;
		}
		
		// Constrain y
		if (object.y < bounds.minY)
		{
			object.y = bounds.minY;
		}
		else if (object.y > bounds.maxY)
		{
			object.y = bounds.maxY;
		}
    }

    static wrapObjectPosition(object, bounds)
    {
		// If a Phaser Game
		if (bounds && typeof(bounds) === 'object' && bounds.constructor.name === 'Game')
		{
			bounds = {
				minX: 0,
				maxX: bounds.scale.width,
				minY: 0,
				maxY: bounds.scale.height,
			};
		}

		// Wrap x
		if (object.x < bounds.minX)
		{
			object.x = bounds.maxX + object.x;
		}
		else if (object.x > bounds.maxX)
		{
			object.x = object.x - bounds.maxX;
		}
		
		// Wrap y
		if (object.y < bounds.minY)
		{
			object.y = bounds.maxY + object.y;
		}
		else if (object.y > bounds.maxY)
		{
			object.y = object.y - bounds.maxY;
		}
    }

	static checkBounds(bounds)
	{
		// If a Phaser Game, use its bounds
		if ( this.isGame(bounds) )
		{
			bounds = {
				x: 0,
				y: 0,
				width: bounds.scale.width,
				height: bounds.scale.height,
			};
		}
		// Else if only a number passed, set bounds
		else if ( typeof(bounds) === 'number' )
		{
			bounds = {
				x: 0,
				y: 0,
				width: bounds,
				height: bounds,
			};
		}

		return bounds;
	}

	/*
	* Distribute space between objects
	space evenly // (space to fill - total width) / (n + 1)
	space between // Just get a starting x/y and the space to put between them all?
	justify // (space to fill - total width) / (n - 1)

	left, right, up, down
	staggerOffset
	*/
	static distributeSpace(objectArray, bounds, type, direction)
	{
		// Stop if array of objects not passed or does not have multiple objects
		if ( ! this.isArray(objectArray) || objectArray.length < 2 )
		{
			return;
		}

		// Validate bounds
		bounds = this.checkBounds(bounds);

		// Set default values
		type = type || 'space-evenly';
		direction = direction || 'right';

		// Set parameters for reference
		var numObj = objectArray.length;
		var totalWidth = 0;
		var totalHeight = 0;

		// For each object, add dimensions to totals
		for ( var objectIteration = 0; objectIteration < numObj; objectIteration++ )
		{
			var curObj = objectArray[objectIteration];
			totalWidth += curObj.width;
			totalHeight += curObj.height;
		}

		// Calculate space for distribution
		var emptySpace = 0;

		if ( type == 'space-evenly' || type == 'justify' )
		{
			if ( direction == 'up' || direction == 'down' )
			{
				emptySpace = bounds.height - bounds.y - totalHeight;
			}
			else
			{
				emptySpace = bounds.width - bounds.x - totalWidth;
			}
		}

		var evenSpace = emptySpace / (numObj + 1);
		var justifySpace = emptySpace / (numObj - 1) || 0;
		
		// For each object
		for ( var objectIteration = 0; objectIteration < numObj; objectIteration++ )
		{
			// Set object references
			var curObj = objectArray[objectIteration];
			var prevObj = (objectIteration > 0) ? objectArray[objectIteration - 1] : undefined;
			var nextObj = (numObj > objectIteration + 1) ? objectArray[objectIteration + 1] : undefined;

			// If distributing space
			if ( type == 'space-evenly' || type == 'justify' )
			{
				// If vertically
				if ( direction == 'up' || direction == 'down' || direction == 'vertical' )
				{
					// If the first iteration
					if (objectIteration == 0)
					{
						curObj.y = ( type == 'space-evenly' ) ? bounds.y + evenSpace : bounds.y;
						continue;
					}

					curObj.y = prevObj.y + prevObj.height;
					curObj.y += ( type == 'space-evenly' ) ? evenSpace : justifySpace;
				}
				// Else horizontally
				else
				{
					// If the first iteration
					if (objectIteration == 0)
					{
						curObj.x = ( type == 'space-evenly' ) ? bounds.x + evenSpace : bounds.x;
						continue;
					}

					curObj.x = prevObj.x + prevObj.width;
					curObj.x += ( type == 'space-evenly' ) ? evenSpace : justifySpace;
				}
			}
		}
	}
	
	/*
	* Spread objects out in a direction
	*/
	static distributeObjects(objectArray, direction, offset, staggerOffset)
	{
		// Stop if array of objects not passed or does not have multiple objects
		if ( ! this.isArray(objectArray) || objectArray.length < 2 )
		{
			return;
		}

		// Set default values
		if ( offset === undefined )
		{
			offset = 0;
		}

		// Validate staggering for a diagonal effect
		var defaultOffset = {x: 0, y: 0};
		if ( staggerOffset === undefined )
		{
			staggerOffset = defaultOffset;
		}
		else if ( typeof(staggerOffset) === 'number' )
		{
			staggerOffset = {x: staggerOffset, y: staggerOffset};
		}
		staggerOffset = Object.assign({}, defaultOffset, staggerOffset);

		// For more readable reference and use
		var numObj = objectArray.length;

		// For each object
		for ( var objectIteration = 1; objectIteration < numObj; objectIteration++ )
		{
			// Set reference objects
			var curObj = objectArray[objectIteration];
			var prevObj = (objectIteration > 0) ? objectArray[objectIteration - 1] : undefined;
			//var nextObj = (numObj > objectIteration + 1) ? objectArray[objectIteration + 1] : undefined;

			// Move object based on direction
			if ( direction == 'left' || direction == 'right' )
			{
				curObj.x = prevObj.x + offset + staggerOffset.x;
				curObj.x += ( direction == 'left' ) ? - curObj.width : prevObj.width;
				curObj.y += staggerOffset.y * objectIteration; //= prevObj.y + staggerOffset.y;
				
			}
			else if ( direction == 'up' || direction == 'down' )
			{
				curObj.y = prevObj.y + offset + staggerOffset.y;
				curObj.y += ( direction == 'up' ) ? - curObj.height : prevObj.height;
				curObj.x += staggerOffset.x * objectIteration; //= prevObj.x + staggerOffset.x
			}
		}
	}

	static lookupAlign(align)
	{
		// Default parameters
		if ( align === undefined )
		{
			align = 'tl';
		}
		
		// Shorthand
		var shorthandDictionary = {
			// Horizontal positions
			l: 'left',
			c: 'center',
			r: 'right',
			// Vertical positions
			t: 'top',
			m: 'middle',
			b: 'bottom'
		}
		
		// If using shorthand for alignment, create and reference pieces
		if ( align.length <= 2 )
		{
			var alignPieces = align.split('');
			// Reset and default align
			align = 'top-left';

			if ( shorthandDictionary[alignPieces[0]] !== undefined )
			{
				align = shorthandDictionary[alignPieces[0]];
			}

			if ( alignPieces.length > 1 && shorthandDictionary[alignPieces[1]] !== undefined )
			{
				align += '-' + shorthandDictionary[alignPieces[1]];
			}
		}

		return align;
	}

	static getAlignData(align)
	{
		var data = {
			original: align
		};

		align = this.lookupAlign(align);

		data.position = align;
		
		// Align X
		if ( align.indexOf('left') > -1 )
		{
			data.left = true;
		}
		else if ( align.indexOf('right') > -1 )
		{
			data.right = true;
		}
		else if ( align.indexOf('center') > -1 )
		{
			data.center = true;
		}

		// Align Y
		if ( align.indexOf('top') > -1 )
		{
			data.top = true;
		}
		else if ( align.indexOf('bottom') > -1 )
		{
			data.bottom = true;
		}
		else if ( align.indexOf('middle') > -1 )
		{
			data.middle = true;
		}

		return data;
	}

	static alignObject(object, bounds, align, type, offset, originOffset)
	{
		// Default parameters
		if ( type === undefined )
		{
			type = 'inside';
		}

		// Make sure a default offset is set
		var defaultOffset = {x: 0, y: 0};
		if ( offset === undefined )
		{
			offset = defaultOffset;
		}
		else if ( typeof(offset) === 'number' )
		{
			offset = {x: offset, y: offset};
		}
		offset = Object.assign({}, defaultOffset, offset);

		// Validate default origin
		if ( originOffset === undefined )
		{
			originOffset = defaultOffset;
		}
		else if ( typeof(originOffset) === 'number' )
		{
			originOffset = {x: originOffset, y: originOffset};
		}
		originOffset = Object.assign({}, defaultOffset, originOffset);

		// Validate bounds
		bounds = this.checkBounds(bounds);

		// If passing more than one object to align, not that we've normalized parameters, pass them recursively
		if ( this.isArray(object) )
		{
			for ( var objectIteration = 0; objectIteration < object.length; objectIteration++ )
			{
				this.alignObject(object[objectIteration], bounds, align, type, offset, originOffset);
			}
		}

		// Check if an object passed and use as percent of bounds for alignment
		if ( typeof(align) === 'object' )
		{
			if ( typeof(align.x) === 'number' )
			{
				object.x = align.x * bounds.width;
			}

			if ( typeof(align.y) === 'number' )
			{
				object.y = align.y * bounds.height;
			}

			return;
		}

		// Get alignment data
		align = this.getAlignData(align);

		// Align X
		if ( align.left )
		{
			if ( type == 'middle' )
			{
				object.x = bounds.x - (object.width / 2) - offset.x;
			}
			else if ( type == 'outside' )
			{
				object.x = bounds.x - object.width - offset.x;
			}
			else
			{
				object.x = bounds.x + offset.x;
			}
		}
		else if ( align.right )
		{
			if ( type == 'middle' )
			{
				object.x = (bounds.x + bounds.width) - (object.width / 2) + offset.x;
			}
			else if ( type == 'outside' )
			{
				object.x = (bounds.x + bounds.width) + offset.x;
			}
			else
			{
				object.x = (bounds.x + bounds.width) - object.width - offset.x;
			}
		}
		else if ( align.center )
		{
			object.x = ((bounds.x + bounds.width) / 2) - (object.width / 2) + offset.x;
		}

		// Align Y
		if ( align.top )
		{
			if ( type == 'middle' )
			{
				object.y = bounds.y - (object.height / 2) - offset.y;
			}
			else if ( type == 'outside' )
			{
				object.y = bounds.y - object.height - offset.y;
			}
			else
			{
				object.y = bounds.y + offset.y;
			}
		}
		else if ( align.bottom )
		{
			if ( type == 'middle' )
			{
				object.y = (bounds.y + bounds.height) - (object.height / 2) + offset.y;
			}
			else if ( type == 'outside' )
			{
				object.y = (bounds.y + bounds.height) + offset.y;
			}
			else
			{
				object.y = (bounds.y + bounds.height) - object.height - offset.y;
			}
		}
		else if ( align.middle )
		{
			object.y = ((bounds.y + bounds.height) / 2) - (object.height / 2) + offset.y;
		}

		// Keep things to whole pixels
		object.x = Math.round(object.x);
		object.y = Math.round(object.y);
	}

	static normalizeRadiansToDegrees(angleRadians)
	{
		var angleRadiansNormalized = Phaser.Math.Angle.Normalize(angleRadians);
		var angleDegreesNormalized = Phaser.Math.RadToDeg(angleRadiansNormalized);
		return angleDegreesNormalized;
	}

	static normalizeDegrees(angleDegrees)
	{
		if ( angleDegrees < 0 )
		{
			angleDegrees = 360 + angleDegrees;
		}
		return angleDegrees;
		
		var angleRadians = Phaser.Math.DegToRad(angleDegrees);
		var angleRadiansNormalized = Phaser.Math.Angle.Normalize(angleRadians);
		var angleDegreesNormalized = Phaser.Math.RadToDeg(angleRadiansNormalized);
		return angleDegreesNormalized;
	}

	static isGame(object)
	{
		// If a Phaser Game
		return ( typeof(object) === 'object' && object.constructor.name === 'Game' );
	}

	static isArray(object)
	{
		return Array.isArray(object);
	}

	static isObject(object)
	{
		return ( object && typeof object === 'object' && object !== null && ! Array.isArray(object) && object.constructor === Object );
	}

	//https://stackoverflow.com/questions/679915/how-do-i-test-for-an-empty-javascript-object
	static isObjectEmpty(object)
	{
		for (var prop in object)
		{
			if (object.hasOwnProperty(prop))
			{
				return false;
			}
		}

		return ( JSON.stringify(object) === JSON.stringify({}) );
	}

	// https://stackoverflow.com/a/1527820
	
	/**
	 * Returns a random number between min (inclusive) and max (exclusive)
	 */
	 static getRandomArbitrary(min, max) {
		return Math.random() * (max - min) + min;
	}

	/**
	 * Returns a random integer between min (inclusive) and max (inclusive).
	 * The value is no lower than min (or the next integer greater than min
	 * if min isn't an integer) and no greater than max (or the next integer
	 * lower than max if max isn't an integer).
	 * Using Math.round() will give you a non-uniform distribution!
	 */
	 static getRandomInt(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
}

window.REDLOVE_PHASER_UTILITY = window.REDLOVE_PHASER_UTILITY || {};

window.REDLOVE_PHASER_UTILITY.newline = '\n';

//https://medium.com/slow-cooked-games-blog/how-to-create-a-mobile-game-on-the-cheap-38a7b75999a7#.7ho2ywm5h
window.REDLOVE_PHASER_UTILITY.calculateDimensions = function ()
{
	var data = {
		ratio : window.innerWidth / window.innerHeight,
		innerWidth : window.innerWidth,
		innerHeight : window.innerHeight,
		clientWidth : document.documentElement.clientWidth,
		clientHeight : document.documentElement.clientHeight,
		devicePixelRatio : window.devicePixelRatio || 1
	};
	
	data.screenWidthMax = Math.max(window.innerWidth * data.devicePixelRatio, data.clientWidth);
	data.screenHeightMax = Math.max(window.innerHeight * data.devicePixelRatio, data.clientHeight);
	data.scaleRatio3rds = data.devicePixelRatio / 3;//http://www.joshmorony.com/how-to-scale-a-game-for-all-device-sizes-in-phaser/
	data.scaleRatio = data.devicePixelRatio;//myAsset.scale.setTo(scaleRatio, scaleRatio)

	return data;
};

/*
// Try to hide mobile browser address bar
function hideAddressBar ()
{
	if ( ! window.location.hash )
  {
	  if ( document.documentElement.scrollHeight < window.outerHeight / window.devicePixelRatio )
		{
			window.scrollTo(0,0);// reset in case prev not scrolled
			document.documentElement.style.height = (window.outerHeight / window.devicePixelRatio) + 'px';
			setTimeout(function ()
			{
				window.scrollTo(1, 1);
			}, 50);
		}
	}
}
window.addEventListener('load', function ()
{
	if ( ! window.pageYOffset )
	{
		hideAddressBar();
	}
});
window.addEventListener('orientationchange', function ()
{
	hideAddressBar();
});
*/

window.REDLOVE_PHASER_UTILITY.valueAtIndex = function ( obj, index )
{
	return obj[ Object.keys(obj)[index] ];
};

window.REDLOVE_PHASER_UTILITY.objLength = function ( obj )
{
	return Object.keys(obj).length;
};

window.REDLOVE_PHASER_UTILITY.randomColorFromHex = function ( colors )
{
	var color = window.REDLOVE_PHASER_UTILITY.randomFromArray(colors);
	return '#' + window.REDLOVE_PHASER_UTILITY.hexString(color);
};

// https://stackoverflow.com/questions/57803/how-to-convert-decimal-to-hex-in-javascript
// https://stackoverflow.com/questions/11023144/working-with-hex-strings-and-hex-values-more-easily-in-javascript
window.REDLOVE_PHASER_UTILITY.hexString = function ( hex )
{
	if ( typeof hex === 'undefined' )
	{
		hex = 0x000000;
	}
	
	var hexString = hex.toString(16);
	return window.REDLOVE_PHASER_UTILITY.pad('000000', hexString, true);
};

window.REDLOVE_PHASER_UTILITY.randomFromArray = function ( arr )
{
	return arr[Math.floor(Math.random() * arr.length)];
};

// Pad values: https://stackoverflow.com/questions/2686855/is-there-a-javascript-function-that-can-pad-a-string-to-get-to-a-determined-leng/24398129#24398129
window.REDLOVE_PHASER_UTILITY.pad = function ( pad, str, padLeft )
{
	if ( typeof str === 'undefined' )
	{
		return pad;
	}
	
	if ( padLeft )
	{
		return (pad + str).slice(-pad.length);
	}
	else
	{
		return (str + pad).substring(0, pad.length);
	}
};

window.REDLOVE_PHASER_UTILITY.timeExpanded = function ( time_milliseconds )
{
	var time = ( typeof time_milliseconds === 'undefined' ) ? 0 : time_milliseconds;
	var milliseconds = 0;
	var seconds = 0;
	var minutes = 0;
	var hours = 0;
	
	milliseconds = time;
	seconds = time / (1000);
	minutes = time / (1000 * 60);
	hours = time / (1000 * 60 * 60);
	days = time / (1000 * 60 * 60 * 24);
	years = time / (1000 * 60 * 60 * 24 * 364.25);
	
	var tmp_time = time;
	clock_milliseconds = tmp_time % 1000;
	tmp_time = (tmp_time - clock_milliseconds) / 1000;
	clock_seconds = tmp_time % 60;
	tmp_time = (tmp_time - clock_seconds) / 60;
	clock_minutes = tmp_time % 60;
	clock_hours = (tmp_time - clock_minutes) / 60;
	
	return {
		milliseconds : milliseconds,
		seconds : seconds,
		minutes : minutes,
		hours : hours,
		days : days,
		years : years,
		
		clock_milliseconds : clock_milliseconds,
		clock_seconds : clock_seconds,
		clock_minutes : clock_minutes,
		clock_hours : clock_hours,
		
		clock_pad_milliseconds : window.REDLOVE_PHASER_UTILITY.pad('000', clock_milliseconds, true),
		clock_pad_seconds : window.REDLOVE_PHASER_UTILITY.pad('00', clock_seconds, true),
		clock_pad_minutes : window.REDLOVE_PHASER_UTILITY.pad('00', clock_minutes, true),
		clock_pad_hours : window.REDLOVE_PHASER_UTILITY.pad('00', clock_hours, true)
	};
};

window.REDLOVE_PHASER_UTILITY.scaleToWorld = function ( objWidth, objHeight, worldWidth, worldHeight, type )
{
	if ( typeof objWidth === 'object' )
	{
		var tmpObj = objWidth;
		var tmpWorldObj = objHeight;
		type = worldWidth;
		
		objWidth = tmpObj.width;
		objHeight = tmpObj.height;
		
		worldWidth = tmpWorldObj.width;
		worldHeight = tmpWorldObj.height;
	}
	
	if ( typeof type === 'undefined' )
	{
		type = 'fill';
	}
	
	var width = objWidth;
	var height = objHeight;
	
	// If ratio > 1, the width is greater than the height
	var worldRatio = worldWidth / worldHeight;
	var objRatio = objWidth / objHeight;
	
	// Scale proportionately to fill the world
	if ( type == 'fill' )
	{
		// Obj compard to the world, obj width is smallest and should max
		if ( objRatio < worldRatio )
		{
			width = worldWidth;
			height = width / objRatio;
		}
		else
		{
			height = worldHeight;
			width = height * objRatio;
		}
	}
	
	return {
		width : width,
		height : height
	};
};

window.REDLOVE_PHASER_UTILITY.deepCloneValues = function ( obj )
{
	// Init if no data or an empty object: if ( typeof(data) === 'undefined' || JSON.stringify(data) === JSON.stringify({}) )
	// Deep clone: JSON.parse(JSON.stringify(defaultData))
	// Return a new object, but only shallow copies property values: Object.assign({}, defaultData, data);
	// Merge data with object spread: data = {...defaultData, ...data};
	//data = { ...defaultData, ...data };

	return JSON.parse(JSON.stringify(obj));
};

// https://stackoverflow.com/questions/1255512/how-to-draw-a-rounded-rectangle-on-html-canvas
window.REDLOVE_PHASER_UTILITY.roundedRect = function ( ctx, x, y, w, h, r )
{
	if (w < 2 * r) r = w / 2;
	if (h < 2 * r) r = h / 2;
	ctx.beginPath();
	ctx.moveTo(x+r, y);
	ctx.arcTo(x+w, y, x+w, y+h, r);
	ctx.arcTo(x+w, y+h, x, y+h, r);
	ctx.arcTo(x, y+h, x, y, r);
	ctx.arcTo(x, y, x+w, y, r);
	ctx.closePath();
	return ctx;
};

window.REDLOVE_PHASER_UTILITY.childPosition = function ( group, index )
{
	if ( typeof index === 'undefined' )
	{
		index = group.length - 1;
	}
	
	var child = group.children[index];
	
	return {
		height : child.height,
		width : child.width,
		x : child.x,
		y : child.y
	};
};


window.REDLOVE_PHASER_UTILITY.roundToDecimals = function round(value, decimals)
{
	// https://www.jacklmoore.com/notes/rounding-in-javascript/
	//return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);

	// https://stackoverflow.com/a/11832950
	return Math.round((value + Number.EPSILON) * (10 * decimals)) / (10 * decimals);
};