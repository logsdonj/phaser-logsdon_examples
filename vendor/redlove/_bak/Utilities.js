// https://www.freecodecamp.org/news/var-let-and-const-whats-the-difference/
class Utilities
{
	static animate = {
		
		/* https://animate.style/

		Usage:
		animateCSS('.my-element', 'bounce');

		// or
		animateCSS('.my-element', 'bounce').then((message) => {
		// Do something after the animation
		});
		*/
		animateCSS: (element, animation, animationUtilities, prefix = 'animate__') =>
		{
			// We create a Promise and return it
			return new Promise((resolve, reject) => 
			{
				const animationName = `${prefix}${animation}`;
				// If a string is passed, assume a selector else already an element
				const node = ( typeof(element) == 'string' ) ? document.querySelector(element) : element;
	
				// If no node, stop
				if ( ! node ) reject('animateCSS: Node does not exist.');
	
				// If utilities not an array, start one and map with prefix
				if ( ! Array.isArray(animationUtilities) )
				{
					animationUtilities = animationUtilities ? [animationUtilities] : [];
					animationUtilities = animationUtilities.map(val => `${prefix}${val}`);
				}
	
				// Create and add to class array
				let classArray = animationUtilities;
				classArray.unshift(animationName);
				classArray.unshift(`${prefix}animated`);
				
				//node.classList.add(`${prefix}animated`, animationName);
				// Use array for adding classes
				node.classList.add(...classArray);
	
				// When the animation ends, we clean the classes and resolve the Promise
				function handleAnimationEnd(event) {
					event.stopPropagation();
					node.classList.remove(...classArray);
					resolve('animateCSS: Animation ended');
				}
	
				node.addEventListener('animationend', handleAnimationEnd, {once: true});
			});
		}

	};

	static fullscreen = {

		// https://blog.bitsrc.io/building-fullscreen-web-apps-dfc2286b7049
		toggle: ( toggle, element ) =>
		{
			if ( Utilities.fullscreen.isEnabled() )
			{
				// Exit fullscreen
				if ( Utilities.fullscreen.getElement() && toggle !== true )
				{
					return Utilities.fullscreen.exit();
				}
				// Request fullscreen
				else
				{
					return Utilities.fullscreen.enter(element);
				}
			}
			else
			{
				console.log('Fullscreen is not supported.');
				return;
			}
		},

		enter: ( element ) =>
		{
			// Default to document
			element = element ?? document.documentElement;
			// Available vendor prefix functions
			const functionNames = ['requestFullscreen', 'webkitRequestFullScreen', 'mozRequestFullScreen', 'msRequestFullscreen'];
	
			// Try each available function
			for ( const functionName of functionNames )
			{
				if ( element[functionName] )
				{
					element[functionName]()
					.catch(function(error) {
						console.log(`${error.name}: ${error.message}`, error);
						return false;
					});
	
					break;
				}
			}
	
			return true;
		},

		exit: ( element ) =>
		{
			// Available vendor prefix functions
			const functionNames = ['exitFullscreen', 'webkitExitFullscreen', 'mozCancelFullScreen', 'msExitFullscreen'];
			
			// Try each available function
			for ( const functionName of functionNames )
			{
				if ( document[functionName] )
				{
					document[functionName]();
					break;
				}
			}
	
			return true;
		},

		isEnabled: () =>
		{
			return Boolean( document.fullscreenEnabled ?? webkitFullscreenEnabled ?? document.mozFullscreenEnabled ?? document.msFullscreenEnabled );
		},
	
		getElement: () =>
		{
			const fullScreenElement = document.fullscreenElement ?? document.webkitFullscreenElement ?? document.mozFullScreenElement ?? document.msFullscreenElement;
			return ( fullScreenElement && fullScreenElement !== null ? fullScreenElement : false );
		},
	
		is: () =>
		{
			return Boolean( Utilities.fullscreen.isEnabled() && Utilities.fullscreen.getElement() !== false );
		}

	};

	/**
	 * 
	 * Example:
	 * let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = mergeWhitelistObject(foo, bar);
	 * fooBar == {a: 1, b: 3}
	 * 
	 * @param {*} whitelistObject 
	 * @param {*} passedObj 
	 */
	static mergeWhitelistObject ( whitelistObject, passedObj )
	{
		// Ensure passedObj is an object
		if ( typeof(passedObj) !== 'object' )
		{
			passedObj = {};
		}

		// Stop if parameters are not objects
		if ( typeof(whitelistObject) !== 'object' || typeof(passedObj) !== 'object' )
		{
			return;
		}
		
		// Get allowed keys
		const allowedKeys = Object.keys(whitelistObject);

		// Filter object using allowed keys
		// https://stackoverflow.com/questions/38750705/filter-object-properties-by-key-in-es6

		/* New object will have all whitelist entries and passed ones overwritten
		It should be unnecessary to merge the object later:
		
		let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = mergeWhitelistObject(foo, bar);
		fooBar == {a: 1, b: 3}
		
		newObj = Object.assign({}, this.defaultObj, filteredObj);
		*/
		// https://stackoverflow.com/a/60379733
		// For allowed keys
		const filteredObj = allowedKeys.reduce((obj, key) => {
			// If it exists in the passedObj, use it or default
			obj[key] = passedObj[key] ?? whitelistObject[key];
			return obj;
		}, {});

		return filteredObj;
	}

	/**
	 * 
	 * Example:
	 * let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = whitelistObjectByKeys(foo, bar);
	 * fooBar == {b: 3}
	 * 
	 * @param {*} allowedKeys 
	 * @param {*} passedObj 
	 */
	static whitelistObjectByKeys ( allowedKeys, passedObj )
	{
		// Ensure passedObj is an object
		if ( typeof(passedObj) !== 'object' )
		{
			passedObj = {};
		}

		// Stop if parameters are not objects
		if ( typeof(allowedKeys) !== 'object' || typeof(passedObj) !== 'object' )
		{
			return;
		}

		// Ensure working with an array of keys
		if ( Array.isArray() === false )
		{
			// Get allowed keys
			allowedKeys = Object.keys(allowedKeys);
		}

		// Filter object using allowed keys
		// https://stackoverflow.com/questions/38750705/filter-object-properties-by-key-in-es6

		/* Option 1 - New object will only have passed whitelist keys and values
		This means you may still need to merge the object later: 

		let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = whitelistObjectByKeys(foo, bar);
		fooBar == {b: 3}

		newObj = Object.assign({}, this.defaultObj, filteredObj);
		*/
		// https://stackoverflow.com/a/38750895
		// For passedObj keys
		const filteredObj = Object.keys(passedObj)
		// Use conditional in reduce for speed
		//.filter(key => allowedKeys.includes(key))
		.reduce((obj, key) => {
			// If the key is found, use value
			if ( allowedKeys.indexOf(key) !== -1 )
			{
				obj[key] = passedObj[key];
			}
			return obj;
		}, {});
		
		/* Option 2 - New object will have all whitelist keys but only passed values defined
		
		let foo = {a: 1, b: 2}; let bar = {b: 3, c: 4}; let fooBar = whitelistObjectByKeys(foo, bar);
		fooBar == {a: undefined, b: 3}

		// https://stackoverflow.com/a/60379733
		// For allowed keys
		const filteredObj = allowedKeys.reduce((obj, key) => {
			// Use existing value or undefined
			obj[key] = passedObj[key];
			return obj;
		}, {});
		*/

		return filteredObj;
	}

	static sortByTypeName ( recordsObject, turnIntoArrayOfIds )
	{
		// If an object not passed, return empty array
		if ( typeof(recordsObject) != 'object' )
		{
			return [];
		}

		// Turn object into an array for sorting
		let param;
		let recordsArray = [];
		const recordKeys = Object.keys(recordsObject);
		recordKeys.forEach(key => {
			const record = {
				id: key,
				type: recordsObject[key].type ?? '',
				name: recordsObject[key].name ?? ''
			};
			recordsArray.push(record);
		});

		// Sort by name
		param = 'name';
		recordsArray.sort((a, b) => {
			a = a[param] ?? '';
			b = b[param] ?? '';
			if ( a == b ) return 0;
			return ( a > b ) ? 1 : -1;
		});

		// Sort by type
		param = 'type';
		recordsArray.sort((a, b) => {
			a = a[param] ?? '';
			b = b[param] ?? '';
			if ( a == b ) return 0;
			return ( a > b ) ? 1 : -1;
		});

		// If turning into array of ids
		if ( turnIntoArrayOfIds )
		{
			// Put back into an array of keys/ids as if they were that way from the object
			let recordIds = [];
			recordsArray.forEach(arrayItem => {
				recordIds.push(arrayItem.id);
			});
			return recordIds;
		}

		return recordsArray;
	}

	/**
	 * 
	 * @param {*} recordsObject 
	 * @param {*} orderKeys 
	 * @param {*} turnIntoArrayOfIds 
	 * @returns 
	 * 
	 * recordsObject example:
	 * const records = {
	 * 	"one": {'type': "primary", "name": "Item B"},
	 * 	"two": {'type': "primary", "name": "Item A"},
	 * }
	 * 
	 * const sortedIds = Utilities.sortObjectBy(records, ['type', 'name'], true)
	 * 
	 */
	static sortObjectBy ( recordsObject, orderKeys, turnIntoArrayOfIds )
	{
		// If an object not passed, return empty array
		if ( typeof(recordsObject) != 'object' )
		{
			return [];
		}

		// Turn object into an array for sorting
		let param;
		let recordsArray = [];
		const recordKeys = Object.keys(recordsObject);
		recordKeys.forEach(key => {
			const record = {
				id: key
			};

			orderKeys.forEach(orderKey => {
				record[orderKey] = recordsObject[key][orderKey] ?? ''
			});

			recordsArray.push(record);
		});

		// Sort by order key, do reverse to maintain proper order
		const orderKeysReversed = orderKeys.reverse();
		orderKeysReversed.forEach(orderKey => {
			recordsArray.sort((a, b) => {
				a = a[orderKey] ?? '';
				b = b[orderKey] ?? '';
				if ( a == b ) return 0;
				return ( a > b ) ? 1 : -1;
			});
		});

		// If turning into array of ids
		if ( turnIntoArrayOfIds )
		{
			// Put back into an array of keys/ids as if they were that way from the object
			let recordIds = [];
			recordsArray.forEach(arrayItem => {
				recordIds.push(arrayItem.id);
			});
			return recordIds;
		}

		return recordsArray;
	}

	/**
	 * https://stackoverflow.com/questions/5488028/how-do-i-check-for-vowels-in-javascript
	 */
	static isVowel ( char )
	{
		return (['a', 'e', 'i', 'o', 'u'].indexOf(char.toLowerCase()) !== -1);
		//return (/^[aeiou]$/i).test(char);
	}

	/**
	 * 
	 */
	static isSingular ( obj )
	{
		if ( typeof(obj) === 'number' )
		{
			return (obj === 1);
		}
		else if ( Array.isArray(obj) )
		{
			return (obj.length === 1);
		}
		else if ( typeof(obj) === 'object' )
		{
			return (Object.keys(obj).length === 1);
		}
	}

	/**
	 * https://gist.github.com/letsgetrandy/1e05a68ea74ba6736eb5
	 */
	static verbPastTense ( verb )
	{
		// language exceptions
		let exceptions = {
			'are': 'were',
			'eat': 'ate',
			'go': 'went',
			'have': 'had',
			'inherit': 'inherited',
			'is': 'was',
			'run': 'ran',
			'sit': 'sat',
			'visit': 'visited'
		}

		if (exceptions[verb]) {
			return exceptions[verb];
		}

		if ((/e$/i).test(verb)) {
			return verb + 'd';
		}

		if ((/[aeiou]c/i).test(verb)) {
			return verb + 'ked';
		}

		// for american english only
		if ((/el$/i).test(verb)) {
			return verb + 'ed';
		}

		if ((/[aeio][aeiou][dlmnprst]$/).test(verb)) {
			return verb + 'ed';
		}
		
		if ((/[aeiou][bdglmnprst]$/i).test(verb)) {
			return verb.replace(/(.+[aeiou])([bdglmnprst])/, '$1$2$2ed');
		}

		return verb + 'ed';
	}

	/**
	 * 
	 */
	static numberPluralizeVerb ( amount, verb )
	{
		// If array or object, get its length for the amount
		if ( Array.isArray(amount) )
		{
			amount = amount.length;
		}
		else if ( typeof(amount) === 'object' )
		{
			amount = Object.keys(amount).length;
		}

		let verbLowercase = verb.toLowerCase();

		if ( verb == 'is' || verb == 'are' )
		{
			return (amount === 1) ? 'is' : 'are';
		}

		return verb;
	}

	/**
	 * https://www.grammarly.com/blog/spelling-plurals-with-s-es/
	 */
	static wordPluralizeSuffix ( word, amount, suffixOnly )
	{
		// Don't pluralize if singular
		if ( Utilities.isSingular(amount) )
		{
			return (suffixOnly) ? '' : word;
		}

		// If ending in ch, sh, s, x, or z then assume "es"
		let lastChar = word.slice(-1);
		let last2Chars = word.slice(-2);
		let lastCharMatch = (['s', 'x', 'z'].indexOf(lastChar.toLowerCase()) !== -1);
		let last2CharMatch = (['ch', 'sh'].indexOf(last2Chars.toLowerCase()) !== -1);

		let suffix = (lastCharMatch || last2CharMatch) ? 'es' : 's';
		return (suffixOnly) ? suffix : word + suffix;
	}

	/**
	 * https://www.grammarly.com/blog/spelling-plurals-with-s-es/
	 */
	static wordArticlePrefix ( word, type )
	{
		let firstLetter = word.charAt(0).toLowerCase();
		
		if ( type === 'a' || type === 'an' )
		{
			return Utilities.isVowel(firstLetter) ? 'an' : 'a';
		}

		return 'the';
	}
	
	/**
	 * 
	 */
	static createConjunctionSentence ( strings, stringBegin, stringEnd, conjuction )
	{
		// Default conjunction
		if ( ! conjuction )
		{
			conjuction = 'and';
		}

		let compiledString = '';

		// If no string, stop
		if ( strings.length == 0 )
		{
			return compiledString;
		}

		// Add passed beginning
		compiledString += stringBegin ?? '';

		for ( let index = 0; index < strings.length; index++ )
		{
			// Append conjunction before item
			// If 2 items and on the last one, append 'and'
			if ( strings.length == 2 && index == 1 )
			{
				compiledString += ` ${conjuction} `;
			}
			// If more than 2 items
			else if ( strings.length > 2 )
			{
				// If on the last, append ', and'
				if ( index == strings.length - 1 )
				{
					compiledString += `, ${conjuction} `;
				}
				// Else if not on the first, append comma
				else if ( index > 0 )
				{
					compiledString += ', ';
				}
			}

			// Add onto string
			compiledString += strings[index];
		}

		// Add passed ending
		compiledString += stringEnd ?? '';
		
		return this.capitalize(compiledString);
	}

	// https://stackoverflow.com/questions/1026069/how-do-i-make-the-first-letter-of-a-string-uppercase-in-javascript
	static capitalize ( str = '' )
	{
		//str[0].toUpperCase() + str.substring(1)
		return str.charAt(0).toUpperCase() + str.slice(1);
	}

	// https://stackoverflow.com/questions/1705057/regex-remove-articles-from-title-the-an-a
	static removeArticles ( str )
	{
		let regex = /(?:(the|a|an) +)/gi;
		str = str.replace(regex, '');
		return str;
	}
	
	/*
	// Examples of JSON handling
	let text = '{ "employees" : [' +
	'{ "firstName":"John" , "lastName":"Doe" },' +
	'{ "firstName":"Anna" , "lastName":"Smith" },' +
	'{ "firstName":"Peter" , "lastName":"Jones" } ]}';
	const obj = JSON.parse(text);
	//console.log(obj);
	//console.log(JSON.stringify(obj));
	*/
	// https://stackoverflow.com/questions/29797946/handling-bad-json-parse-in-node-safely
	static safelyParseJSON ( json )
	{
		// This function cannot be optimised, it's best to keep it small!
		let parsed;
	
		try {
			parsed = JSON.parse(json)
		} catch (e) {
			// Oh well, but whatever...
		}
	
		return parsed;// Could be undefined!
	};

	
	static roundToDecimals (value, decimals)
	{
		// https://www.jacklmoore.com/notes/rounding-in-javascript/
		//return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);

		// https://stackoverflow.com/a/11832950
		return Math.round((value + Number.EPSILON) * (10 * decimals)) / (10 * decimals);
	}

	// Pad values: https://stackoverflow.com/questions/2686855/is-there-a-javascript-function-that-can-pad-a-string-to-get-to-a-determined-leng/24398129#24398129
	static pad ( pad, str, padLeft )
	{
		if ( typeof(str) === 'undefined' )
		{
			return pad;
		}
		
		if ( padLeft )
		{
			return (pad + str).slice(- pad.length);
		}
		
		return (str + pad).substring(0, pad.length);
	}

	static deepClone ( obj )
	{
		// Create a deep copy
		// https://blog.logrocket.com/methods-for-deep-cloning-objects-in-javascript/
		// https://web.dev/structured-clone/
		// Shallow: Object.assign({}, dbItem);
		// Deep, but limited: JSON.parse(JSON.stringify(myOriginal));
		// Deep: structuredClone(myOriginal)
		
		// Init if no data or an empty object: if ( typeof(data) === 'undefined' || JSON.stringify(data) === JSON.stringify({}) )
		// Deep clone: JSON.parse(JSON.stringify(defaultData))
		// Return a new object, but only shallow copies property values: Object.assign({}, defaultData, data);
		// Merge data with object spread: data = {...defaultData, ...data};
		//data = { ...defaultData, ...data };

		//return JSON.parse(JSON.stringify(obj));
		return structuredClone(obj);
	}

	// https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
	static shuffleArray ( array )
	{
		for ( let i = array.length - 1; i > 0; i-- )
		{
			const j = Math.floor(Math.random() * (i + 1));
			[array[i], array[j]] = [array[j], array[i]];
		}
	}

	// https://stackoverflow.com/questions/5915096/get-a-random-item-from-a-javascript-array
	static randomFromArray ( arr )
	{
		return arr[arr.length * Math.random() | 0];
		//return arr[Math.floor(Math.random() * arr.length)];
	}

	// https://stackoverflow.com/a/1527820
	
	/**
	 * Returns a random number between min (inclusive) and max (exclusive)
	 */
	static getRandomArbitrary ( min, max )
	{
		return Math.random() * (max - min) + min;
	}

	/**
	 * Returns a random integer between min (inclusive) and max (inclusive).
	 * The value is no lower than min (or the next integer greater than min
	 * if min isn't an integer) and no greater than max (or the next integer
	 * lower than max if max isn't an integer).
	 * Using Math.round() will give you a non-uniform distribution!
	 */
	static getRandomInt ( min, max )
	{
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	
	// https://stackoverflow.com/questions/14453670/remove-an-empty-string-from-array-of-strings-jquery
	// https://stackoverflow.com/questions/55685037/how-to-remove-empty-array-values-from-an-array
	static filterArrayEmpty ( arr )
	{
		return arr.filter(el => el != null && el.trim() != '');
		/*
		return arr.filter(function (el) {
			return ( el != null && el.trim() != '' );
		});
		*/
	}

	static timeBreakdown ( time )
	{
		let minuteS = 60;
		let hourS = 60 * 60;

		let timeMs = time;
		let timeS = timeMs / 1000;
		let timeSMod = timeS % 60;
		let timeM = (timeS - timeSMod) / 60;
		let timeMMod = timeM % 60;
		let timeH = (timeM - timeMMod) / 60;

		/*
		console.log(timeS, timeSMod);
		console.log(timeM, timeMMod);
		console.log(timeH);
		*/

		return {
			mToS: minuteS,
			hToS: hourS,
			ms: timeMs,
			s: timeS,
			sMod: timeSMod,
			m: timeM,
			mMod: timeMMod,
			h: timeH
		};
	}

	static timeAgo ( timeMs )
	{
		let timeBd = Utilities.timeBreakdown(timeMs);
		let timeText = '';

		// Tolerance of now
		if ( timeBd.s < 1 )
		{
			// 0 seconds ago, just now
			timeText = 'just now';
		}
		// Tolerance of a minute
		else if ( timeBd.s > (timeBd.mToS - 5) && timeBd.s < (timeBd.mToS + 5) )
		{
			timeText = 'about a minute ago';
		}
		// Tolerance of an hour
		else if ( timeBd.s > (timeBd.hToS - 60) && longAgoS < (timeBd.hToS + 60) )
		{
			timeText = 'about an hour ago';
		}
		// Regular time reporting
		else
		{
			let timeTexts = [];

			if ( timeBd.h > 0 )
			{
				let displayLongAgoH = Math.round(timeBd.h);
				timeTexts.push(`${displayLongAgoH} hour${displayLongAgoH != 1 ? 's' : ''}`);
			}

			if ( timeBd.m > 0 )
			{
				if ( timeBd.mMod > 0 )
				{
					let displayLongAgoMMod = Math.round(timeBd.mMod);
					timeTexts.push(`${displayLongAgoMMod} minute${displayLongAgoMMod != 1 ? 's' : ''}`);
				}
			}

			if ( timeBd.sMod > 0 )
			{
				let displayLongAgoSMod = Math.round(timeBd.sMod);
				timeTexts.push(`${displayLongAgoSMod} second${displayLongAgoSMod != 1 ? 's' : ''}`);
			}

			timeText = timeTexts.join(', ');
			timeText += ` ago`;
		}

	
		return timeText;
	}

	static timeDurationToText ( timeMs )
	{
		let timeBd = Utilities.timeBreakdown(timeMs);
		let timeText = '';
		let timeTexts = [];

		if ( timeBd.h > 0 )
		{
			let displayTimeH = Math.round(timeBd.h);
			timeTexts.push(`${displayTimeH} hour${displayTimeH != 1 ? 's' : ''}`);
		}

		if ( timeBd.m > 0 )
		{
			if ( timeBd.mMod > 0 )
			{
				let displayTimeMMod = Math.round(timeBd.mMod);
				timeTexts.push(`${displayTimeMMod} minute${displayTimeMMod != 1 ? 's' : ''}`);
			}
		}

		if ( timeBd.sMod > 0 )
		{
			let displayTimeSMod = Math.round(timeBd.sMod);
			timeTexts.push(`${displayTimeSMod} second${displayTimeSMod != 1 ? 's' : ''}`);
		}

		timeText = timeTexts.join(', ');

	
		return timeText;
		
		/*
		// https://stackoverflow.com/questions/3552461/how-do-i-format-a-date-in-javascript
		let addDate = new Date(challenge.addTime);
		let dateOptions = {month: 'numeric', day: 'numeric'};//year: 'numeric', 
		let localDate = addDate.toLocaleDateString('en', dateOptions);
		let timeOptions = {hour12: true, hour: 'numeric', minute: '2-digit', second: '2-digit'};//year: 'numeric', 
		let localTime = addDate.toLocaleTimeString('en', timeOptions);

		// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/RelativeTimeFormat/format
		// Create a relative time formatter in your locale
		let rtf = new Intl.RelativeTimeFormat('en', { numeric: "auto" });

		let rtf = new Intl.RelativeTimeFormat("en", {
			localeMatcher: "best fit", // other values: "lookup"
			numeric: "always", // other values: "auto"
			style: "long", // other values: "short" or "narrow"
		});

		"quarter", "month", "week", "day", "hour", "minute", "second"
		*/
	}

	// https://webdesign.tutsplus.com/tutorials/javascript-debounce-and-throttle--cms-36783
	// https://blog.webdevsimplified.com/2022-03/debounce-vs-throttle/
	// https://medium.com/nerd-for-tech/debouncing-throttling-in-javascript-d36ace200cea
	// https://www.youtube.com/watch?v=cjIswDCKgu0

	// https://www.freecodecamp.org/news/javascript-debounce-example/
	/*
	static debounce ( func, timeout = 300 )
	{
		let timer;
		return (...args) => {
			clearTimeout(timer);
			timer = setTimeout(() => { 
				func.apply(this, args); 
			}, timeout);
		};
	}

	static debounce_leading ( func, timeout = 300 )
	{
		let timer;
		return (...args) => {
			if (! timer) {
				func.apply(this, args);
			}
			clearTimeout(timer);
			timer = setTimeout(() => {
				timer = undefined;
			}, timeout);
		};
	}
	*/

	// https://davidwalsh.name/javascript-debounce-function
	// Returns a function, that, as long as it continues to be invoked, will not
	// be triggered. The function will be called after it stops being called for
	// N milliseconds. If `immediate` is passed, trigger the function on the
	// leading edge, instead of the trailing.
	/*
	static debounce ( func, wait, immediate )
	{
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	}
	*/

	
	// https://davidwalsh.name/javascript-debounce-function
	// https://dev.to/monaye/refactor-davidwalsh-s-debounce-function-5afc
	/*
	const updateModalComponentInfoDebounced = Utilities.debounce(updateModalComponentInfo, 100);

	// IIFE version for inline functions
	window.addEventListener('scroll', debounce( () => {
		// All the taxing stuff you do
		}, 250)
	);

	// Creating the debounced function directly
	var myEfficientFn = debounce(function() {
		// All the taxing stuff you do
	}, 250);
	window.addEventListener('resize', myEfficientFn);

	// Creating a function separately and then a debounced version for use
	var myfunc = function ( str ) {
		console.log(str);
	};
	var myEfficientFn = debounce(myfunc, 250);
	myfunc('Hello1');
	myfunc('Hello1');
	myfunc('Hello1');
	myEfficientFn('Hello2');
	myEfficientFn('Hello2');
	myEfficientFn('Hello2');
	*/
	static debounce = (func, delay, immediate) => {
		let timerId;
		return (...args) => {
			const boundFunc = func.bind(this, ...args);
			clearTimeout(timerId);
			if (immediate && !timerId) {
				boundFunc();
			}
			const calleeFunc = immediate ? () => { timerId = null } : boundFunc;
			timerId = setTimeout(calleeFunc, delay);
		}
	}
	
	static throttle = (func, delay, immediate) => {
		let timerId;
		return (...args) => {
			const boundFunc = func.bind(this, ...args);
			if (timerId) {
				return;
			}
			if (immediate && !timerId) {
				boundFunc();
			}
			timerId = setTimeout(() => {
				if(!immediate) {
				boundFunc(); 
				}
				timerId = null; // reset the timer so next call will be excuted
			}, delay);
		}
	}

}