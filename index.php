<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

	<title></title>
</head>
<body>

	<div class="container mb-4 mt-4">
		<div class="row">
			<div class="col-sm">
				<h1>Phaser Dashboard</h1>
			</div>
			<div class="col-sm text-end">
				<a class="btn btn-outline-warning" href="?">Reset</a>
			</div>
		</div>
	</div>
	

	<div class="container mb-4 mt-4">
		<div class="row">
			<div class="col-sm">

<?php
/*
http://www.sitepoint.com/list-files-and-directories-with-php/
http://stackoverflow.com/questions/24783862/list-all-the-files-and-folders-in-a-directory-with-php-recursive-function
*/
$realpath = realpath(dirname(__FILE__));

$manifestJsonString = '{
	"directoryWhitelist": ["apps", "examples", "templates"],
	"directoryBlacklist": [".git"]
}';
$manifestJson = json_decode($manifestJsonString);


$examplesRealpath = $realpath . '/examples/examples-by-config/scripts/configs';
$examplesData = gatherFilesandDirectories($examplesRealpath, array(
	'maxDepth' => 1,
));

?>

<h2>Examples</h2>
<ul>
	<li><a href="examples/">Example Switcher via Configs</a>
		<ul>
			<?php
			if ( ! empty($examplesData) && ! empty($examplesData['files']) )
			{
				foreach ( $examplesData['files'] as $file )
				{
					$pathParts = pathinfo($file);
				?>
			<li><a href="examples/examples-by-config/?config=<?php echo $pathParts['filename']; ?>"><?php echo $pathParts['filename']; ?></a></li>
				<?php
				}
			}
			?>
		</ul>
	</li>
</ul>

<h2>Templates</h2>
<ul>
	<li></li>
</ul>

<h2>Apps</h2>
<ul>
	<li></li>
</ul>

<hr>

<?php

$appsRealpath = $realpath . '/apps';
$appsData = gatherFilesandDirectories($appsRealpath, array(
	'maxDepth' => 1, 
	'blacklistDirectoryNames' => $manifestJson->directoryBlacklist,
));

$examplesRealpath = $realpath . '/examples';
$examplesData = gatherFilesandDirectories($examplesRealpath, array(
	'maxDepth' => 1, 
	'blacklistDirectoryNames' => $manifestJson->directoryBlacklist,
));

$templatesRealpath = $realpath . '/templates';
$templatesData = gatherFilesandDirectories($templatesRealpath, array(
	'maxDepth' => 1, 
	'blacklistDirectoryNames' => $manifestJson->directoryBlacklist,
));

echo '<pre>';
print_r($appsData);
print_r($examplesData);
print_r($templatesData);
echo '</pre>';






// https://hotexamples.com/examples/-/RecursiveIteratorIterator/getSubPath/php-recursiveiteratoriterator-getsubpath-method-examples.html
function gatherFilesandDirectories( $path, $params = array() )
{
	$defaultParams = array(
		'maxDepth' => -1,
		'whitelistDirectoryNames' => null, // Array of names
		'blacklistDirectoryNames' => null, // Array of names
		'filterCallback' => null,
		'directoryCallback' => null,
		'fileCallback' => null,
	);
	$params = array_merge($defaultParams, $params);

	// Setting FilesystemIterator will include depth in file spl info
	$directory = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS | FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_SELF | FilesystemIterator::FOLLOW_SYMLINKS);

	//https://www.php.net/manual/en/class.recursivedirectoryiterator.php#114504
	if ( isset($params['filterCallback']) )
	{
		$filter = new RecursiveCallbackFilterIterator($directory, $params['filterCallback']);
		/*
		$filter = new RecursiveCallbackFilterIterator($directory, function ($current, $key, $iterator)
		{
			// Skip hidden files and directories.
			if ($current->getFilename()[0] === '.')
			{
				return FALSE;
			}

			if ($current->isDir())
			{
				// Only recurse into intended subdirectories.
				return $current->getFilename() === 'examples';
			}
			else
			{
				// Only consume files of interest.
				return strpos($current->getFilename(), 'index.php') === 0;
			}
		});
		*/
		$iterator = new RecursiveIteratorIterator($filter, RecursiveIteratorIterator::SELF_FIRST);
	}
	else
	{
		$iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
	}

	$iterator->setMaxDepth($params['maxDepth']);

	$data = array(
		'directories' => array(),
		'files' => array(),
	);

	foreach ( $iterator as $file_pathname => $spl_file_info )
	{
		// Normalize directory path
		$file_pathname = str_ireplace('\\', '/', $file_pathname);

		if ( $spl_file_info->isDir() )
		{
			/*
			// If whitelisting names
			if ( isset($params['whitelistDirectoryNames']) )
			{
				// If name not in whitelist, skip
				if ( ! in_array($spl_file_info->getFilename(), $params['whitelistDirectoryNames']) )
				{
					continue;
				}
			}

			// If blacklisting names
			if ( isset($params['blacklistDirectoryNames']) )
			{
				// If name in blacklist, skip
				if ( in_array($spl_file_info->getFilename(), $params['blacklistDirectoryNames']) )
				{
					continue;
				}
			}
			*/
			
			if ( isset($params['directoryCallback']) )
			{
				if ( ! $params['directoryCallback']($file_pathname, $spl_file_info, $iterator) )
				{
					continue;
				}
			}
			
			$data['directories'][] = $file_pathname;
		}
		elseif ( $spl_file_info->isFile() )
		{
			if ( isset($params['fileCallback']) )
			{
				if ( ! $params['fileCallback']($file_pathname, $spl_file_info, $iterator) )
				{
					continue;
				}
			}

			$data['files'][] = $file_pathname;
		}
	}

	return $data;
}
?>

<h2>gatherFilesandDirectories</h2>
<?php
$data = gatherFilesandDirectories($realpath, array(
	'maxDepth' => 1, 
	'blacklistDirectoryNames' => $manifestJson->directoryBlacklist,
	'whitelistDirectoryNames' => $manifestJson->directoryWhitelist,
));

echo '<pre>';
print_r($data);
echo '</pre>';


?>

<h2>gatherFilesandDirectories with filter</h2>
<?php
$data = gatherFilesandDirectories($realpath, array(
	'maxDepth' => 3, 
	'filterCallback' => function ($current, $key, $iterator) use ( & $manifestJson )
	{
		// Skip hidden files and directories.
		if ( $current->getFilename()[0] === '.' )
		{
			return FALSE;
		}

		if ( $current->isDir() )
		{
			// Only recurse into intended subdirectories.
			echo $current->getBaseName() . ' | ' . $current->getRealPath() . ' | ' . $current->getSubPath() . ' | ' . $current->getSubPathName() . ' | ' . $current->getPathName() . ' | ' . $current->getFilename() . '<br>';
			

			if ( isset($manifestJson->directoryWhitelist) && ! in_array($current->getFilename(), $manifestJson->directoryWhitelist) )
			{
				return false;
			}

			if ( isset($manifestJson->directoryBlacklist) && in_array($current->getFilename(), $manifestJson->directoryBlacklist) )
			{
				return false;
			}

			return true;
		}
		else
		{
			// Only recurse into intended subdirectories.
			echo $current->getSubPathName() . ' | ' . $current->getPathName() . ' | ' . $current->getFilename() . '<br>';
			
			// Only consume files of interest.
			return strpos($current->getFilename(), 'index') === 0;
		}
	},
	'directoryCallback' => function ($file_pathname, $spl_file_info, $iterator) use ( & $manifestJson )
	{
		return true;
	},
	'fileCallback' => function ($file_pathname, $spl_file_info, $iterator) use ( & $manifestJson )
	{
		if ( $iterator->getDepth() <= 0 )
		{
			//return false;
		}

		return true;
	},
));

echo '<pre>';
print_r($data);
echo '</pre>';
?>


<h2>FilesystemIterator</h2>
<?php
$iterator = new FilesystemIterator($realpath);
$data = array(
	'directories' => array(),
	'files' => array(),
);
foreach ( $iterator as $entry )
{
	if ( $entry->isDir() )
	{
		$data['directories'][] = $entry->getFilename();
	}
	elseif ( $entry->isFile() )
	{
		$data['files'][] = $entry->getFilename();
	}
}
echo '<pre>';
print_r($data);
echo '</pre>';
?>


<h2>RecursiveDirectoryIterator</h2>
<?php
// Setting FilesystemIterator will include depth in file spl info
$directory = new RecursiveDirectoryIterator($realpath, RecursiveDirectoryIterator::SKIP_DOTS | FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_SELF);
$iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
//$iterator->setMaxDepth(1);
$data = array(
	'directories' => array(),
	'files' => array(),
);
foreach ( $iterator as $file_pathname => $spl_file_info )
{
	// Normalize directory path
	$file_pathname = str_ireplace('\\', '/', $file_pathname);
	
	if ( $spl_file_info->isDir() )
	{
		$data['directories'][] = $file_pathname;
	}
	elseif ( $spl_file_info->isFile() )
	{
		$data['files'][] = $file_pathname;
	}
}
echo '<pre>';
print_r($data);
echo '</pre>';
?>

			</div>
		</div>
	</div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

</body>
</html>