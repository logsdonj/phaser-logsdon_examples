// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				// Dialog manifest

				this.dialogManifest = {
					entryTemplate: {
						leftAvatar: '',
						rightAvatar: '',
						background: '',
						audio: '',
						text: "Well hey there!\nLet's go!\nLet's go!",
						next: 'entry1'
					},
					// Separate entries
					entry1: {
						text: "One ring to rule them all...",
						next: 'entry2'
					},
					entry2: {
						text: "one ring to find them...",
						next: 'entry3'
					},
					entry3: {
						text: "one ring to bring them all...",
						next: 'entry4'
					},
					entry4: {
						text: "and in the darkness, bind them."
					},
					// A nested entry instead of several separated
					nestedEntry1: {
						text: "One ring to rule them all...",
						next: {
							text: "one ring to find them...",
							next: {
								text: "one ring to bring them all...",
								next: {
									text: "and in the darkness, bind them."
								}
							}
						}
					},
					// A set of dialog entries grouped in an array with no next value needed
					arrayEntry: [
						{
							text: "One ring to rule them all..."
						},
						{
							text: "one ring to find them..."
						},
						{
							text: "one ring to bring them all..."
						},
						{
							text: "and in the darkness, bind them."
						}
					],
					// An entry with options nested
					optionsEntry: {
						text: "What do you think?",
						choices: {
							choice1: {
								text: "Choice 1",
								next: {
									text: "Ok, 1 it is!",
									callback: 'myCallback',
									callbackParams: {param1: 'choice1'}
								}
							},
							choice2: {
								text: "Choice 2",
								next: {
									text: "Fine, be that way with 2.",
									callback: 'myCallback',
									callbackParams: {param1: 'choice2'}
								}
							},
							choice3: {
								text: "Choice 3",
								next: {
									text: "Hmmm, I'll have to think 3 over...",
									callback: 'myCallback',
									callbackParams: {param1: 'choice3'}
								}
							}
						}
					}
				};

				//this.dialog = new Dialog(this, {}, this.dialogManifest.entry1);
				//this.dialog = new Dialog(this, {}, this.dialogManifest.nestedEntry1);
				//this.dialog = new Dialog(this, {}, this.dialogManifest.arrayEntry);
				//this.dialog = new Dialog(this, {}, this.dialogManifest.optionsEntry);

				// Test different dialog entry types
				//this.dialog = new Dialog(this);

				var textOptions = { 
					backgroundColor: 'rgba(0,0,0,0.8)',
					font: '12px Arial, sans-serif', 
					fill: 'rgb(255,255,255)',
					padding: { // Adjust to work with shadown offset and blur to avoid clipping
						x: 10,
						y: 10
					}
				};

				var dialogTest1 = this.add.text(0, 0, 
					'Linear path dialog entires', 
					textOptions
				)
				.setOrigin(0).setInteractive({ cursor: 'pointer' })
				.on('pointerdown', function() {
					this.dialog = new Dialog(this, {}, this.dialogManifest.entry1);
				}, this);

				var dialogTest2 = this.add.text(0, 0, 
					'Array of dialog entries', 
					textOptions
				)
				.setOrigin(0).setInteractive({ cursor: 'pointer' })
				.on('pointerdown', function() {
					this.dialog = new Dialog(this, {}, this.dialogManifest.nestedEntry1);
				}, this);

				var dialogTest3 = this.add.text(0, 0, 
					'Nested dialog entries', 
					textOptions
				)
				.setOrigin(0).setInteractive({ cursor: 'pointer' })
				.on('pointerdown', function() {
					this.dialog = new Dialog(this, {}, this.dialogManifest.nestedEntry1);
				}, this);

				var dialogTest4 = this.add.text(0, 0, 
					'Dialog entry with options and callback with parameters', 
					textOptions
				)
				.setOrigin(0).setInteractive({ cursor: 'pointer' })
				.on('pointerdown', function() {
					this.dialog = new Dialog(this, {}, this.dialogManifest.optionsEntry);
				}, this);

				window.REDLOVE.p.distributeObjects([dialogTest1, dialogTest2, dialogTest3, dialogTest4], 'down', 20);
			},

			// App loop
			update: function(time, delta) {
			}
		}
	]
};

class Dialog {
    constructor(scene, config, dialogEntry) {
		this.scene = scene;
		
		var defaultConfig = {
			debug: true, // Show debug information
		};

        if (config === undefined) {
        	config = {};
        }

		this.config = Object.assign({}, defaultConfig, config);
		
		// Create an event emitter to emit events from this object
		this.events = new Phaser.Events.EventEmitter();

		// Start data collection
		this.data = {};

		if ( dialogEntry !== undefined )
		{
			this.create(dialogEntry);
		}
	}

	myCallback(callbackParams)
	{
		console.log('myCallback', callbackParams);
	}

	create(dialogEntry)
	{
		console.log('Dialog create', dialogEntry);

		// Check if there is a callback to do
		if ( dialogEntry.callback !== undefined )
		{
			console.log('do callback:', dialogEntry.callback, dialogEntry.callbackParams);
			this[dialogEntry.callback](dialogEntry.callbackParams);
		}

		this.hasOptions = window.REDLOVE.u.isObject(dialogEntry.choices);
		// If the entry has choices
		if ( this.hasOptions )
		{
			console.log('Dialog has choices.');
		}

		this.hasNested = window.REDLOVE.u.isObject(dialogEntry.next);
		// If the next entry is nested object
		if ( this.hasNested )
		{
			console.log('Dialog has nested next.');
		}

		// If the entry is an array, it should be a set of entries, so make sure the first in the set is not another array or this will endless loop
		if ( window.REDLOVE.u.isArray(dialogEntry) && ! window.REDLOVE.u.isArray(dialogEntry[0]) )
		{
			console.log('Dialog is array of entries.');

			// Start set mode
			this.dialogSet = dialogEntry;
			this.dialogSetIndex = 0;
			this.create(this.dialogSet[this.dialogSetIndex]);
			return;
		}

		this.currentEntry = dialogEntry;

		// Create graphics for dialog box
		
		// Background overlay
		this.createBackground();

		// Create avatars
		this.createAvatars();

		// Text
		this.createText();

		// Buttons
		this.createButtons();

		// Emit event
		this.events.emit('down', 'test');
	}

	createBackground()
	{
		if ( this.bgGraphics )
		{
			return;
		}
		
		// Background overlay
		this.overlayOffset = 20;
		this.overlayRect = new Phaser.Geom.Rectangle(this.overlayOffset, this.overlayOffset, this.scene.game.scale.width - (this.overlayOffset * 2), this.scene.game.scale.height - (this.overlayOffset * 2));
		this.bgGraphics = this.scene.add.graphics({ x: 0, y: 0 });
		this.bgGraphics.lineStyle(2, 0x565656, 0.5);
		this.bgGraphics.strokeRectShape(this.overlayRect);
		this.bgGraphics.fillStyle(0xff0000, 0.2);
		this.bgGraphics.fillRectShape(this.overlayRect);
		
		// Add interaction to prevent clicks to objects behind
		this.gameRect = new Phaser.Geom.Rectangle(0, 0, this.scene.game.scale.width, this.scene.game.scale.height);
		this.bgGraphicsInteractiveConfig = {
			hitArea: this.gameRect,//this.overlayRect,
			hitAreaCallback: Phaser.Geom.Rectangle.Contains,
			useHandCursor: false
		};
		this.bgGraphics.setInteractive(this.bgGraphicsInteractiveConfig)
		.on('pointerdown', function(pointer)
		{
			console.log('Dialog background rect, prevent clicks under dialog.');
		}, this);
	}

	destroyBackground()
	{
		this.bgGraphics.destroy();
	}

	createAvatars()
	{
		if ( this.playerAvatar )
		{
			return;
		}

		this.playerAvatar = this.scene.add.rectangle(0, 0, 100, 100, 0xff0000, 0.5)
		.setStrokeStyle(2, 0x1a65ac)
		.setOrigin(0, 0);
		
		window.REDLOVE.p.alignObject([this.playerAvatar], this.overlayRect, 'tl', 'inside', 20);
	}

	destroyAvatars()
	{
		this.playerAvatar.destroy();
	}

	createText()
	{
		if ( this.dialogText )
		{
			this.dialogText.destroy();
		}

		// Text
		this.dialogText = this.scene.add.text(
			this.overlayRect.x + this.overlayOffset, 
			this.overlayRect.y + this.overlayOffset, 
			this.currentEntry.text, 
			{ 
				backgroundColor: 'rgba(0,0,0,0.8)',
				font: '12px Arial, sans-serif', 
				fill: 'rgb(255,255,255)',
				stroke: 'rgb(47,92,31)',
				strokeThickness: 0,
				shadow: {
					offsetX: 4,
					offsetY: 4,
					color: 'rgba(0,0,0,1)',
					blur: 0,
					stroke: true,
					fill: true
				},
				padding: { // Adjust to work with shadown offset and blur to avoid clipping
					x: 10,
					y: 10
				},
				align: 'left'
			}
		)
		.setOrigin(0, 0)
		//.setFixedSize(this.game.scale.width, this.game.scale.height)
		.setDepth(10)
		.setInteractive({ cursor: 'pointer' })
		.once('pointerdown', function()
		{
		}, this);

		window.REDLOVE.p.alignObject(this.dialogText, this.overlayRect, 'mc', 'inside', 0);

		this.createTextOptions();
	}

	createTextOptions()
	{
		// Text Options
		// Reset pre-existing options
		this.destroyOptionTexts();
		if ( this.hasOptions )
		{
			/*
			console.log(dialogEntry.choices);
			for ( var prop in dialogEntry.choices )
			{
				console.log(prop, dialogEntry.choices[prop])
			}
			*/

			this.optionTexts = [];

			var choiceIteration = 0;
			
			for ( var [key, value] of Object.entries(this.currentEntry.choices) )
			{
				choiceIteration++;

				//console.log(`${key}: ${value}`);
				console.log(key, value);

				var dialogTextBounds = this.dialogText.getBounds();

				var choiceOffset = 10;
				var choiceX = dialogTextBounds.x;
				var choiceY = dialogTextBounds.y + dialogTextBounds.height;

				if ( choiceIteration > 1 )
				{
					var prevText = this.optionTexts[choiceIteration - 2];
					var buttonTextBounds = prevText.getBounds();
					choiceX = buttonTextBounds.x;
					choiceY = buttonTextBounds.y + buttonTextBounds.height + choiceOffset;
				}
				
				// Text
				var optionText = this.scene.add.text(
					choiceX, 
					choiceY, 
					value.text, 
					{ 
						backgroundColor: 'rgba(0,0,0,0.8)',
						font: '12px Arial, sans-serif', 
						fill: 'rgb(255,255,255)',
						stroke: 'rgb(47,92,31)',
						strokeThickness: 0,
						shadow: {
							offsetX: 4,
							offsetY: 4,
							color: 'rgba(0,0,0,1)',
							blur: 0,
							stroke: true,
							fill: true
						},
						padding: { // Adjust to work with shadown offset and blur to avoid clipping
							x: 10,
							y: 10
						},
						align: 'center'
					}
				)
				.setOrigin(0)
				.setInteractive({ cursor: 'pointer' });
				optionText.dialog = this;
				optionText.dialogEntry = value;
				// Set event after object instantiation
				optionText.on('pointerdown', function()
				{
					console.log('optionText pointerdown')
					this.dialog.create(this.dialogEntry.next);
				}, optionText);

				this.optionTexts.push(optionText);
			}
		}
	}

	destroyText()
	{
		this.dialogText.destroy();
		this.destroyOptionTexts();
	}

	destroyOptionTexts()
	{
		console.log('destroyOptionTexts', this.optionTexts);

		if ( this.optionTexts && this.optionTexts.length > 0 )
		{
			for ( var optionTextIteration = 0; optionTextIteration < this.optionTexts.length; optionTextIteration++ )
			{
				this.optionTexts[optionTextIteration].destroy();
			}
		}
	}

	createButtons()
	{
		if ( this.continueButton )
		{
			return;
		}

		this.skipButton = this.createButton("Skip");
		this.continueButton = this.createButton("Continue");

		this.skipButton.on('pointerdown', function(pointer)
		{
			console.log('clicked');
			this.destroy();
		}, this);

		this.continueButton.on('pointerdown', function(pointer)
		{
			console.log('clicked', this.currentEntry.next);
			if ( this.currentEntry.next !== undefined )
			{
				// Check for nested object
				if ( window.REDLOVE.u.isObject(this.currentEntry.next) )
				{
					console.log('Next is an object.');
					this.create(this.currentEntry.next);
				}
				else
				{
					console.log('Next is manifest entry.');
					this.create(this.scene.dialogManifest[this.currentEntry.next]);
				}
			}
			// Check for dialog set mode
			else if ( window.REDLOVE.u.isArray(this.dialogSet) && this.dialogSetIndex + 1 < this.dialogSet.length)
			{
				this.dialogSetIndex++;
				this.create(this.dialogSet[this.dialogSetIndex]);
			}
			else
			{
				// TODO: destroy and exit dialog
				this.destroy();
			}
		}, this);

		window.REDLOVE.p.alignObject(this.continueButton, this.overlayRect, 'br', 'inside', 20);
		window.REDLOVE.p.alignObject(this.skipButton, this.overlayRect, 'br', 'inside', {x: 20, y: this.continueButton.height + 40});
	}

	destroyButtons()
	{
		
		this.scene.input.removeDebug(this.skipButton);
		this.skipButton.destroy();
		
		this.scene.input.removeDebug(this.continueButton);
		this.continueButton.destroy();
	}

	createButton(text)
	{
		// Button
		
		var buttonText = this.scene.add.text(
			0, 
			0, 
			text, 
			{ 
				backgroundColor: 'rgba(0,0,0,0.2)',
				font: '12px Arial, sans-serif', 
				fill: 'rgb(255,255,255)',
				stroke: 'rgb(47,92,31)',
				strokeThickness: 0,
				shadow: {
					offsetX: 2,
					offsetY: 2,
					color: 'rgba(0,0,0,1)',
					blur: 0,
					stroke: true,
					fill: true
				},
				padding: { // Adjust to work with shadown offset and blur to avoid clipping
					x: 10,
					y: 10
				},
				align: 'left'
			}
		)
		.setOrigin(0, 0)
		//.setFixedSize(this.game.scale.width, this.game.scale.height)
		.setDepth(10);
		
		var buttonTextBounds = buttonText.getBounds();

		var buttonX = buttonTextBounds.x;
		var buttonY = buttonTextBounds.y;
		var buttonWidth = buttonTextBounds.width;
		var buttonHeight = buttonTextBounds.height;

		// Button graphics
		var buttonGraphics = this.scene.add.graphics({ x: buttonX, y: buttonY });
		//  Button shadow
		buttonGraphics.fillStyle(0x222222, 0.5);
		buttonGraphics.fillRoundedRect(6, 6, buttonWidth, buttonHeight, 16);
		//  Button color
		buttonGraphics.fillStyle(0x0000ff, 1);
		//  Button outline line style
		buttonGraphics.lineStyle(4, 0x565656, 1);
		//  Button shape and outline
		buttonGraphics.strokeRoundedRect(0, 0, buttonWidth, buttonHeight, 16);
		buttonGraphics.fillRoundedRect(0, 0, buttonWidth, buttonHeight, 16);

		var buttonContainer = this.scene.add.container();
		buttonContainer.add([ buttonGraphics, buttonText ]);

		var buttonContainerBounds = buttonContainer.getBounds();
		buttonContainer.width = buttonContainerBounds.width;
		buttonContainer.height = buttonContainerBounds.height;

		var buttonContainerInteractiveConfig = {
			hitArea: new Phaser.Geom.Rectangle(buttonContainerBounds.width / 2, buttonContainerBounds.height / 2, buttonContainerBounds.width, buttonContainerBounds.height),
			hitAreaCallback: Phaser.Geom.Rectangle.Contains,
			useHandCursor: true
		};
		buttonContainer.setInteractive(buttonContainerInteractiveConfig);
		this.scene.input.enableDebug(buttonContainer, 0xff00ff);

		return buttonContainer;
	}

	destroy()
	{
		this.destroyBackground();
		this.destroyAvatars();
		this.destroyText();
		this.destroyButtons();
	}
}
