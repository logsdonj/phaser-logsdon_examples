
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}


class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	pointerMove(pointer)
	{
		if (pointer.isDown)
		{
			// Analyze the angle of the pointer changes
			var angle = pointer.getAngle();
			var angle = Phaser.Math.Angle.BetweenPoints(this.star, pointer);
			this.star.rotation = angle;

			
			var line = new Phaser.Geom.Line(this.anchorPoint.x, this.anchorPoint.y, pointer.x, pointer.y);
			var line2 = new Phaser.Geom.Line(this.anchorPoint.x, this.anchorPoint.y, this.anchorPoint.x + 50, this.anchorPoint.y + 50);
			this.lineGraphics2 = this.lineGraphics2 || this.add.graphics({x: 0, y: 0});
			this.lineGraphics2.clear();
			this.lineGraphics2.lineStyle(1, 0xff0000, 0.5);
			this.lineGraphics2.strokeLineShape(line);
			
			var lineAngle = Phaser.Math.Angle.BetweenPoints(this.anchorPoint, pointer);
        	Phaser.Geom.Line.SetToAngle(line2, this.anchorPoint.x, this.anchorPoint.y, lineAngle, 50);
			this.lineGraphics2.lineStyle(5, 0x00ff00, 0.5);
			this.lineGraphics2.strokeLineShape(line2);
			
		}
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');

		
		
		this.star = this.add.image(400, 300, 'star');
		this.physics.add.existing(this.star);
		this.star
		.setInteractive({
			useHandCursor: true
		})
		.on('pointerdown', function(pointer) {
			console.log('pointerdown star');

			this.star.body.moves = false;//this.star.body.setEnable(false);
			this.anchorPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);

			this.input.on('pointermove', this.pointerMove, this);

			this.input.once('pointerup', function() {
				console.log('pointerup');
				this.input.off('pointermove', this.pointerMove, this);
				
				var pointer = this.input.activePointer;
				var velocityX = pointer.velocity.x;
				var velocityY = pointer.velocity.y;
				var duration = pointer.getDuration();
				// If not using the pointer angle, the pointer release should come after target or else opposite directions happen
				var angle = pointer.getAngle();
				var angleToTarget = Phaser.Math.Angle.BetweenPoints(this.star, pointer);
				var distanceBetweenPointer = pointer.getDistance();
				//var distanceToTarget = Phaser.Math.Distance.Between(this.star.x, this.star.y, pointer.x, pointer.y);
				console.log('Distance: ' + distanceBetweenPointer);
				console.log('Angle: ' + angleToTarget);

				//this.physicsGroup.setVelocityX(velocityX * distance, 10);
				this.star.body.moves = true;//this.star.body.setEnable(true);
				//this.star.body.setVelocity(velocityX, velocityY);
				//this.star.body.setDrag(-velocityX, -velocityY);

				// https://www.html5gamedevs.com/topic/27901-angular-acceleration-based-movement-is-inconsistent/
				// https://www.html5gamedevs.com/topic/10401-is-there-a-way-to-set-maximum-speed-for-an-object/
				var maxVelocity = distanceBetweenPointer;
				this.physics.velocityFromRotation(Phaser.Math.Angle.Reverse(angleToTarget), maxVelocity, this.star.body.velocity);
				// https://photonstorm.github.io/phaser3-docs/Phaser.Math.Vector2.html
				// https://rexrainbow.github.io/phaser3-rex-notes/docs/site/geom-point/#point-as-vector
				// https://rexrainbow.github.io/phaser3-rex-notes/docs/site/arcade-body/#get-physics-body
				// Normalize to get 0-1 number to use as directional magnitude
				var tmpVector = this.star.body.velocity.clone();
				tmpVector.normalize();
				var drag = 100;
				var dragX = drag * Math.abs(tmpVector.x);
				var dragY = drag * Math.abs(tmpVector.y);
				/*
				// These ratios would be the same as vector ratios; the numbers need normalized
				var adjacent = Math.cos(angle) * distance;
				var opposite = Math.sin(angle) * distance;
				var ratio = adjacent/opposite;
				*/

				this.star.body.setDrag(dragX, dragY);
				//this.star.body.setDrag(100 * tmpVector.x, 100 * tmpVector.y);
				//this.star.body.velocity.setMagnitude(Math.min(100, this.star.body.velocity.getMagnitude()));//this.star.body.setDrag(100, 100);

				//this.star.body.velocity.scale(0.5);

				// Rotation
	
				var rotationDirection = this.pointsToRotationDirection(
					new Phaser.Geom.Point(pointer.downX, pointer.downY), 
					new Phaser.Geom.Point(pointer.x, pointer.y), 
					this.star.getBounds()
				);

				this.star.body.setAngularDrag(45);
				//this.star.body.angularVelocity = 0.5 * (this.star.body.velocity.x + this.star.body.velocity.y);
				this.star.body.setAngularVelocity(-rotationDirection * 0.5 * (Math.abs(velocityX) + Math.abs(velocityY)));
			}, this);
		}, this)
		.on('pointerup', function(){
			console.log('pointerup star');
		}, this);

		//this.physicsGroup.add(this.star);
		this.star.body
		.setVelocity(100, 100)
		//.setMaxVelocity(20, 20)// Set with speed instead since working with angles
		//.setDrag(5, 5)// Set with angle ratio instead since working with angles
		.setBounce(1, 1)
		.setCollideWorldBounds(true)
		.setMaxSpeed(300)
		.setAllowGravity(false);

		// Add text for display
		text = this.add.text(10, 10, 'Move the mouse', { font: '16px Courier', fill: '#00ff00' });

		var circleGameObject = this.add.circle(100, 400, 80, 0x6666ff);
		this.physics.add.existing(circleGameObject);
		circleGameObject.body.setAllowGravity(false);

		// Create a Circle
		var circle = new Phaser.Geom.Circle(50, 50, 50);
		// And display our circle on the top
		this.circleGraphics = this.scene.scene.add.graphics(0, 0);//game.make.graphics();
		this.circleGraphics.fillStyle(0x00ff00, 0.5);
		this.circleGraphics.fillCircleShape(circle);
		//this.circleGraphics.x = this.game.scale.width / 2;
		//this.circleGraphics.y = this.game.scale.height / 2;
		
		this.physics.add.existing(this.circleGraphics);
		this.circleGraphics.body.setAllowGravity(false);
		this.circleGraphics.body.setCircle(50, 0, 0);//this.circleGraphics.body.setSize(100, 100);
		//this.physics.accelerateToObject(this.circleGraphics, this.star, 60, 300, 300);

		var collider = this.physics.add.overlap(this.circleGraphics, this.star, function (collisionElment)
		{
			console.log('collision');
			collisionElment.body.stop();
			this.physics.world.removeCollider(collider);
		}, null, this);
	}

	// App loop
	update(time, delta) {
		this.physics.moveToObject(this.circleGraphics, this.star, 60, 1000);
		//this.scene.start('PreloadScene');

		var pointer = this.input.activePointer;

		text.setText([
			'x: ' + pointer.x,
			'y: ' + pointer.y,
			'mid x: ' + pointer.midPoint.x,
			'mid y: ' + pointer.midPoint.y,
			'velocity x: ' + Math.round(pointer.velocity.x),
			'velocity y: ' + pointer.velocity.y,
			'movementX: ' + pointer.movementX,
			'movementY: ' + pointer.movementY,
			'duration: ' + pointer.getDuration(),
			'angle: ' + pointer.getAngle(),
			'distance: ' + pointer.getDistance()
		]);
	}

	pointsToRotationDirection(point1, point2, targetBounds)
	{
		var targetBounds = targetBounds;//this.star.getBounds();
		var beginPoint = point1;//new Phaser.Geom.Point(pointer.downX, pointer.downY);
		var endPoint = point2;//new Phaser.Geom.Point(pointer.x, pointer.y);
		var targetMidX = targetBounds.x + (targetBounds.width / 2);
		var targetMidY = targetBounds.y + (targetBounds.height / 2);
		var targetMidPoint = new Phaser.Geom.Point(targetMidX, targetMidY);
		var beginLeftQuadrant = (beginPoint.x <= targetMidX) ? true : false;
		var beginTopQuadrant = (beginPoint.y <= targetMidY) ? true : false;
		var endLeftQuadrant = (endPoint.x <= targetMidX) ? true : false;
		var endTopQuadrant = (endPoint.y <= targetMidY) ? true : false;
		
		// Try to determine which direction moving around the middle of the object
		var angleToTargetMidPoint = Phaser.Math.Angle.BetweenPoints(beginPoint, targetMidPoint);
		var angleToEndPoint = Phaser.Math.Angle.BetweenPoints(beginPoint, endPoint);
		var distanceToTargetMidPoint = Phaser.Math.Distance.Between(beginPoint.x, beginPoint.y, targetMidPoint.x, targetMidPoint.y);
		var distanceToEndPoint = Phaser.Math.Distance.Between(beginPoint.x, beginPoint.y, endPoint.x, endPoint.y);

		var adjacent = Math.cos(angleToTargetMidPoint) * distanceToTargetMidPoint;
		var opposite = Math.sin(angleToTargetMidPoint) * distanceToTargetMidPoint;
		var angleToTargetMidPointRatio = adjacent/opposite;
		
		var adjacent = Math.cos(angleToEndPoint) * distanceToEndPoint;
		var opposite = Math.sin(angleToEndPoint) * distanceToEndPoint;
		var angleToEndPointRatio = adjacent/opposite;

		var rotationDirection = this.orientationTest(targetMidPoint, beginPoint, endPoint);
		console.log(rotationDirection);
		var line = new Phaser.Geom.Line(beginPoint.x, beginPoint.y, endPoint.x, endPoint.y);
		var line2 = new Phaser.Geom.Line(beginPoint.x, beginPoint.y, targetMidPoint.x, targetMidPoint.y);
		this.lineGraphics2 = this.lineGraphics2 || this.add.graphics({x: 0, y: 0});
		this.lineGraphics2.clear();
		this.lineGraphics2.lineStyle(1, 0xff0000, 0.5);
		this.lineGraphics2.strokeLineShape(line);
		this.lineGraphics2.lineStyle(1, 0x0000ff, 0.5);
		this.lineGraphics2.strokeLineShape(line2);
		
		return rotationDirection;

		/*
		// If ending in the top half of the object
		if (endTopQuadrant)
		{
			if (beginPoint.x <= endPoint.x)
			{
				rotationDirection = 'CW';
			}
			else if (beginPoint.x >= endPoint.x)
			{
				rotationDirection = 'CCW';
			}
		}

		if (beginPoint.x <= endPoint.x)
		{
			rotationDirection = 'CW';
		}
		else if (beginPoint.x >= endPoint.x)
		{
			rotationDirection = 'CCW';
		}
		
		// If ending in the right half of the object
		if (beginPoint.y <= endPoint.y)
		{
			rotationDirection = 'CW';
		}
		else if (beginPoint.y >= endPoint.y)
		{
			rotationDirection = 'CCW';
		}
		*/
	}

	orientationTest(point1, point2, point3)
	{
		// Orientation test
		// https://www.geeksforgeeks.org/orientation-3-ordered-points/
		// https://math.stackexchange.com/questions/128061/check-if-point-is-on-or-below-line-when-either-x-or-y-0
		var val = ((point2.y - point1.y) * (point3.x - point2.x)) - ((point2.x - point1.x) * (point3.y - point2.y));
		
		// clock or counterclock wise
		return (val > 0) ? -1: 1;

		// Colinear
		if (val == 0) return 0;
	}

	isLeft(vectorA, vectorB, vectorC)
	{
		return ((vectorB.x - vectorA.x) * (vectorC.y - vectorA.y) - (vectorB.y - vectorA.y) * (vectorC.x - vectorA.x)) > 0;
	}

	angleToDirection(angle, usingDegrees)
	{
		var direction = {
			down: false,
			downLeft: false,
			downRight: false,
			left: false,
			right: false,
			up: false,
			upLeft: false,
			upRight: false,
			wedgeDown: false,
			wedgeLeft: false,
			wedgeRight: false,
			wedgeUp: false
		};

		if (typeof(usingDegrees) !== 'undefined' && usingDegrees)
		{
			angle = Phaser.Math.DegToRad(angle);
		}
		
		// Look at wedge sections to determine single swipe directions
		if (angle >= -Math.PI/4 && angle <= Math.PI/4)
		{
			direction.wedgeRight = true;
		}
		else if (angle >= -Math.PI*(3/4) && angle <= -Math.PI/4)
		{
			direction.wedgeUp = true;
		}
		else if (angle >= Math.PI*(3/4) || angle <= -Math.PI*(3/4))
		{
			direction.wedgeLeft = true;
		}
		else if (angle >= Math.PI/4 && angle <= Math.PI*(3/4))
		{
			direction.wedgeDown = true;
		}

		// Look in quadrants to determine directions
		if (angle >= -Math.PI/2 && angle <= Math.PI/2)
		{
			direction.right = true;
		}
		else if (angle <= -Math.PI/2 || angle >= Math.PI/2)
		{
			direction.left = true;
		}

		if (angle <= 0)
		{
			direction.up = true;
			direction.upLeft = direction.left ? true : false;
			direction.upRight = direction.right ? true : false;
		}
		else
		{
			direction.down = true;
			direction.downLeft = direction.left ? true : false;
			direction.downRight = direction.right ? true : false;
		}

		return direction;
	}
	
	handleSwipe(pointer) {
        if (this.canMove) {
            var swipeTime = pointer.upTime - pointer.downTime;
            var fastEnough = swipeTime < gameOptions.swipeMaxTime;
            var swipe = new Phaser.Geom.Point(pointer.upX - pointer.downX, pointer.upY - pointer.downY);
            var swipeMagnitude = Phaser.Geom.Point.GetMagnitude(swipe);
			var longEnough = swipeMagnitude > gameOptions.swipeMinDistance;
			
			console.log('Movement time: ' + swipeTime + ' ms; fastEnough: ' + fastEnough);
			console.log('Distance: ' + swipe.x + ' px, '+ swipe.y + ' px; swipeMagnitude: ' + swipeMagnitude + '; longEnough' + longEnough);

			//var angle = Phaser.Geom.Line.Angle(line);
            var startPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
            var stopPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
			var angle = Phaser.Math.Angle.BetweenPoints(startPoint, stopPoint);
			angle = Phaser.Math.Angle.Normalize(Phaser.Math.Angle.Wrap(angle));
			console.log('angle: ' + angle + ' ' + Phaser.Math.RadToDeg(angle));

            if (longEnough && fastEnough) {
				// Normalize magnitude
                Phaser.Geom.Point.SetMagnitude(swipe, 1);
                if (swipe.x > gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(RIGHT);');
                }
                if (swipe.x < -gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(LEFT);');
                }
                if (swipe.y > gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(DOWN);');
                }
                if (swipe.y < -gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(UP);');
				}
				
				// Draw swipe
				this.previousSwipeGraphics.strokeRect(pointer.downX, pointer.downY, pointer.upX - pointer.downX, pointer.upY - pointer.downY);

				this.previousSwipeGraphics.strokeLineShape(this.swipeLine);
				
				this.swipeLine = new Phaser.Geom.Line(pointer.downX, pointer.downY, pointer.upX, pointer.upY);
				this.swipeLineGraphics.clear();
				this.swipeLineGraphics.lineStyle(3, 0xffff00, 0.75);
				this.swipeLineGraphics.strokeLineShape(this.swipeLine);
				var swipeLineLength = Phaser.Geom.Line.Length(this.swipeLine);
				this.swipeLineGraphics.lineStyle(2, 0x00aa00, 1);
				this.swipeLineGraphics.strokeCircle(pointer.downX, pointer.downY, swipeLineLength);
				console.log('swipeLineLength: ' + swipeLineLength);

				// Arrow at the end of the swipe line
				this.triangleGraphics = this.add.graphics({ lineStyle: { width: 2, color: 0x00ff00 }, fillStyle: { color: 0xff0000 } });

				this.arrowTriangle = new Phaser.Geom.Triangle(0, 0, 10, 50, -10, 50);
				// graphics.strokeTriangleShape(triangle);
				this.triangleGraphics.fillTriangleShape(this.arrowTriangle);
				this.triangleGraphics.x = pointer.x;
				this.triangleGraphics.y = pointer.y;
				//this.triangleGraphics.angle = Phaser.Math.RadToDeg(angle);
				this.triangleGraphics.rotation = angle + (Math.PI / 2);

				// Check for swipe intersections
				if (Phaser.Geom.Intersects.LineToRectangle(this.swipeLine, this.enemyRect))
				{
					console.log('intersection');
					var intersectPoints = Phaser.Geom.Intersects.GetLineToRectangle(this.swipeLine, this.enemyRect);
					// Make an associative array of intersected sides
					for (var point of intersectPoints)
					{
						var intersectSide = this.checkIntersectionSide(this.enemyRect, point);
						point.intersectSide = intersectSide;
					}

					this.intersectedGraphics.clear();

					if (intersectPoints.length == 1)
					{
						console.log('intersection on 1 side');
					}
					else if (intersectPoints.length == 2)
					{
						console.log('intersection on 2 sides');

						console.log('Build polygons from intersection...');
						// Start collecting polygons
						var curPoly = 'A';
						var polyObjs = {'A': [], 'B': []};
						// When hit an intersection, start collecting for B and continue switching back and forth on intersects
						var rectPoints = this.enemyRect.getPoints(4);
						// Loop over rectangle points
						for (var rectPointIndex = 0; rectPointIndex < rectPoints.length; rectPointIndex++)
						{
							var rectPoint = rectPoints[rectPointIndex];
							var intersectingPoint = null;

							// Loop over intersecting points
							for (var intersectPointIndex = 0; intersectPointIndex < intersectPoints.length; intersectPointIndex++)
							{
								var intersectPoint = intersectPoints[intersectPointIndex];
								// Check for intersection on rectangle line
								if (intersectPoint.x == rectPoint.x || intersectPoint.y == rectPoint.y)
								{
									intersectingPoint = intersectPoint;
									// Remove the intersect point now that it has been handled
									intersectPoints.splice(intersectPointIndex, 1);
									break;
								}
							}
							
							if (intersectingPoint)
							{
								// Determine placement of the intersection before or after the rectangle point
								var rectBeforeIntersection = true;

								if (
									(rectPointIndex == 0 && intersectingPoint.intersectSide == 'left') || 
									(rectPointIndex == 1 && intersectingPoint.intersectSide == 'top') || 
									(rectPointIndex == 2 && intersectingPoint.intersectSide == 'right') || 
									(rectPointIndex == 3 && intersectingPoint.intersectSide == 'bottom')
								)
								{
									rectBeforeIntersection = false;
								}

								// Add the rectangle point to the polygon
								if (rectBeforeIntersection)
								{
									polyObjs[curPoly].push(rectPoint);
								}

								// Add the polygon, switch to the other polygon, and continue main iteration
								polyObjs[curPoly].push(intersectingPoint);
								curPoly = (curPoly == 'A') ? 'B' : 'A';
								polyObjs[curPoly].push(intersectingPoint);

								// Add the rectangle point to the polygon
								if (! rectBeforeIntersection)
								{
									polyObjs[curPoly].push(rectPoint);
								}
							}
							else
							{
								// Add the rectangle point to the polygon
								polyObjs[curPoly].push(rectPoint);
							}
						}

						this.intersectedGraphics.strokePoints(polyObjs.A, true);
						this.intersectedGraphics.fillPoints(polyObjs.A, true);
						
						this.intersectedGraphics.lineStyle(1, 0xffff00, 0.5);
						this.intersectedGraphics.fillStyle(0xffff00, 0.5);
						this.intersectedGraphics.strokePoints(polyObjs.B, true);
						this.intersectedGraphics.fillPoints(polyObjs.B, true);

						/*
						*/
						var maskShape = this.make.graphics();
						//  Create a hash shape Graphics object
						maskShape.fillStyle(0xffffff);
						//  You have to begin a path for a Geometry mask to work
						maskShape.beginPath();
						maskShape.fillRect(50, 0, 50, 300);
						maskShape.fillRect(175, 0, 50, 300);
						maskShape.fillRect(0, 75, 275, 50);
						maskShape.fillRect(0, 200, 275, 50);
						maskShape.fillPoints(polyObjs.A, true);
						var mask = maskShape.createGeometryMask();
						this.star.setMask(mask);

						var intersectPointsTemp = Phaser.Geom.Intersects.GetLineToRectangle(this.swipeLine, this.enemyRect);
						this.burstSplitLine = new Phaser.Geom.Line(intersectPointsTemp[0].x, intersectPointsTemp[0].y, intersectPointsTemp[1].x, intersectPointsTemp[1].y);
						var burstSplitLineLength = Phaser.Geom.Line.Length(this.burstSplitLine);
						var midPoint = Phaser.Geom.Line.GetMidPoint(this.burstSplitLine);
						this.burstTriangleGraphics = this.add.graphics({ lineStyle: { width: 2, color: 0x00ff00, alpha: 0.5 }, fillStyle: { color: 0xff0000, alpha: 0.5 } });

						this.burstTriangle = new Phaser.Geom.Triangle(0, 0, 10, 20, -10, 20);
						this.burstTriangle2 = new Phaser.Geom.Triangle(0, 0, 10, -20, -10, -20);
						// graphics.strokeTriangleShape(triangle);
						this.burstTriangleGraphics.fillTriangleShape(this.burstTriangle);
						this.burstTriangleGraphics.fillTriangleShape(this.burstTriangle2);
						this.burstTriangleGraphics.x = midPoint.x;
						this.burstTriangleGraphics.y = midPoint.y;
						//this.burstTriangleGraphics.angle = Phaser.Math.RadToDeg(angle);
						this.burstTriangleGraphics.rotation = angle + (Math.PI / 2);
						

						this.burstCircleGraphics = this.add.graphics({ lineStyle: { width: 2, color: 0x00ff00, alpha: 0.5 }, fillStyle: { color: 0xff0000, alpha: 0.5 } });
						this.burstCircleGraphics.lineStyle(2, 0x00aa00, 1);
						this.burstCircleGraphics.strokeCircle(midPoint.x, midPoint.y, burstSplitLineLength / 2);
					}
				}
			}
        }
	}
	
	checkIntersectionSide(rect, point) {
		var leftX = rect.left;
		var rightX = rect.right;
		var topY = rect.top;
		var bottomY = rect.bottom;
		
		// Top intersect
		if (point.y == topY)
		{
			return 'top';
		}
		// Right intersect
		else if (point.x == rightX)
		{
			return 'right';
		}
		// Bottom intersect
		else if (point.y == bottomY)
		{
			return 'bottom';
		}
		// Left intersect
		else if (point.x == leftX)
		{
			return 'left';
		}
	}

}


var gameOptions = {
    tweenSpeed: 50,
    swipeMaxTime: 1000,
    swipeMinDistance: 20,
    swipeMinNormal: 0.85,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			debug: true,
			gravity: {
				y: 200
			}
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

