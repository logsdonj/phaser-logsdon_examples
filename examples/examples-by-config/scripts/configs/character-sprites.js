// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');

				// Load images
				//this.load.image('../../assets/spritesheets/link_sprite_sheet_by_tiozacdasgalaxias-dayh0s6.png')
				// Make sure frame dimensions are correct for accurate animation references
				this.load.spritesheet('characterSpritesheet', 'assets/spritesheets/link_sprite_sheet_by_tiozacdasgalaxias-dayh0s6.png', { frameWidth: 96, frameHeight: 104 });
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				// Add character from spritesheet resource
				this.characterSprite = this.add.sprite(this.game.scale.width / 2, this.game.scale.height / 2, 'characterSpritesheet').setScale(1);

				// Set up character animations from spritesheet
				var characterSpritesheetNumCols = 10;
				var characterSpritesheetNumRows = 8;

				var idleAnimation = this.anims.create({
					key: 'idle',
					frames: this.anims.generateFrameNumbers('characterSpritesheet', {
						frames: [ 0, 1, 2 ]
						//start: 0, end: 2//getFrameRange(characterSpritesheetNumCols, 1, 1, 3)
					}),
					frameRate: 8
				});

				var idleUpRange = getFrameRange(characterSpritesheetNumCols, 3, 1);
				var idleUpAnimation = this.anims.create({
					key: 'idleUp',
					frames: this.anims.generateFrameNumbers('characterSpritesheet', {
						frames: [ idleUpRange.start ]
						//start: 0, end: 2
					}),
					frameRate: 8
				});

				var idleDownAnimation = this.anims.create({
					key: 'idleDown',
					frames: this.anims.generateFrameNumbers('characterSpritesheet', getFrameRange(characterSpritesheetNumCols, 1, 1, 3)),
					frameRate: 8
				});

				var idleLeftAnimation = this.anims.create({
					key: 'idleLeft',
					frames: this.anims.generateFrameNumbers('characterSpritesheet', getFrameRange(characterSpritesheetNumCols, 2, 1, 3)),
					frameRate: 8
				});

				var idleRightAnimation = this.anims.create({
					key: 'idleRight',
					frames: this.anims.generateFrameNumbers('characterSpritesheet', getFrameRange(characterSpritesheetNumCols, 4, 1, 3)),
					frameRate: 8
				});
				
				var walkDownAnimation = this.anims.create({
					key: 'walkDown',
					frames: this.anims.generateFrameNumbers('characterSpritesheet', getFrameRange(characterSpritesheetNumCols, 5)),
					frameRate: 16
				});

				var walkUpAnimation = this.anims.create({
					key: 'walkUp',
					frames: this.anims.generateFrameNumbers('characterSpritesheet', getFrameRange(characterSpritesheetNumCols, 7)),
					frameRate: 16
				});

				var walkLeftAnimation = this.anims.create({
					key: 'walkLeft',
					frames: this.anims.generateFrameNumbers('characterSpritesheet', getFrameRange(characterSpritesheetNumCols, 6)),
					frameRate: 16
				});

				var walkRightAnimation = this.anims.create({
					key: 'walkRight',
					frames: this.anims.generateFrameNumbers('characterSpritesheet', getFrameRange(characterSpritesheetNumCols, 8)),
					frameRate: 16
				});

				// Use animations

				// TODO: Work out common configuration sharing
				//sprite.play({ key: 'idle', repeat: -1, repeatDelay: 2000, yoyo: true });
				this.characterSprite.play({ key: 'idle', repeat: -1, repeatDelay: 2000, yoyo: true });

				/*
				this.time.addEvent({
					delay: 3000,
					callback: () => {
						plane.play("explode");

						plane.once(Phaser.Animations.Events.SPRITE_ANIMATION_COMPLETE, () => {
							plane.destroy();
						});
					}
				});
				*/

				// https://rexrainbow.github.io/phaser3-rex-notes/docs/site/keyboardevents/

				/*
				var keyObj = this.input.keyboard.addKey('UP');  // Get key object
				keyObj.on('down', function(event) {
					sprite.play({ key: 'walkUp', repeat: -1 });
				}, this);
				*/


				this.direction = {up: false, down: false, left: false, right: false};
				this.numMovementKeysPressed = 0;
				this.movementKeysDown = [];
				this.handleControls = function(event) {
					// Handle tracking key presses

					var lastIndexRemoved = false;

					// If key down
					if ( event.isDown ) {
						// Add to keys down
						this.movementKeysDown.push(event.keyCode);
					}
					// Else if key up
					else if ( event.isUp ) {
						// Find and remove it from keys down
						var index = this.movementKeysDown.indexOf(event.keyCode);
						var lastIndexRemoved = (index == this.movementKeysDown.length - 1);
						if ( index > -1 )
						{
							this.movementKeysDown.splice(index, 1);
						}
					}

					this.noMovementKeysPressed = (this.movementKeysDown.length == 0);

					// Handle animations and directions

					// Handle up
					if ( event.keyCode == this.keys.up.keyCode ) {
						this.direction.up = event.isDown;

						if ( event.isDown ) {
							this.characterSprite.play({ key: 'walkUp', repeat: -1 });
						}
						else if ( event.isUp && this.noMovementKeysPressed ) {
							this.characterSprite.play({ key: 'idleUp' });
						}
					}

					// Handle down
					if ( event.keyCode == this.keys.down.keyCode ) {
						this.direction.down = event.isDown;

						if ( event.isDown ) {
							this.characterSprite.play({ key: 'walkDown', repeat: -1 });
						}
						else if ( event.isUp && this.noMovementKeysPressed ) {
							this.characterSprite.play({ key: 'idleDown', repeat: -1, repeatDelay: 2000, yoyo: true });
						}
					}

					// Handle left
					if ( event.keyCode == this.keys.left.keyCode ) {
						this.direction.left = event.isDown;

						if ( event.isDown ) {
							this.characterSprite.play({ key: 'walkLeft', repeat: -1 });
						}
						else if ( event.isUp && this.noMovementKeysPressed ) {
							this.characterSprite.play({ key: 'idleLeft', repeat: -1, repeatDelay: 2000, yoyo: true });
						}
					}

					// Handle right
					if ( event.keyCode == this.keys.right.keyCode ) {
						this.direction.right = event.isDown;

						if ( event.isDown ) {
							this.characterSprite.play({ key: 'walkRight', repeat: -1 });
						}
						else if ( event.isUp && this.noMovementKeysPressed ) {
							this.characterSprite.play({ key: 'idleRight', repeat: -1, repeatDelay: 2000, yoyo: true });
						}
					}

					// Normalize direction values, not allowing in opposite directions
					if ( this.direction.up && this.direction.down )
					{
						//this.direction.up = this.direction.down = false;
					}

					if ( this.direction.left && this.direction.right )
					{
						//this.direction.left = this.direction.right = false;
					}


					// If the index removed was the last item and there are more items
					if ( lastIndexRemoved && this.movementKeysDown.length > 0 )
					{
						// Send an event through with the last active movement key
						var lastMovementKeyPressed = this.movementKeysDown.pop();
						return this.handleControls({keyCode: lastMovementKeyPressed, isDown: true, isUp: false});
					}
				}

				// TODO: Work out better key handling, especially when multiple keys pressed
				// TODO: Work out joystick handling
				// TODO: Review more examples of chaining and other animation abilities
				//var keys = this.input.keyboard.addKeys('W,S,A,D');keys.W.isDown;keys.W.on('down');keys.W.destroy();scene.input.keyboard.on('keydown-' + 'W';Phaser.Input.Keyboard.KeyCodes.SPACE
				//https://photonstorm.github.io/phaser3-docs/Phaser.Input.Keyboard.KeyboardPlugin.html
				//https://docs.idew.org/video-game/project-references/phaser-coding/input
				this.keys = this.input.keyboard.addKeys({
					up: 'W',
					down: 'S',
					left: 'A',
					right: 'D'
				});
				this.keys.up.on('down', this.handleControls, this);
				this.keys.up.on('up', this.handleControls, this);
				this.keys.down.on('down', this.handleControls, this);
				this.keys.down.on('up', this.handleControls, this);
				this.keys.left.on('down', this.handleControls, this);
				this.keys.left.on('up', this.handleControls, this);
				this.keys.right.on('down', this.handleControls, this);
				this.keys.right.on('up', this.handleControls, this);

				this.characterSprite.on('animationrepeat', function () {
				}, this);
			},

			// App loop
			update: function(time, delta) {
				// Use vector normalization to normalize diagonal movement to be equal to horizontal and vertical movement
				var diagonalFactor = ((this.direction.up || this.direction.down) && (this.direction.left || this.direction.right)) ? 0.7071 : 1; // 1 / sqrt(2)

				// Move character
				if ( this.direction.up )
				{
					this.characterSprite.y -= 5 * diagonalFactor;
				}

				if ( this.direction.down )
				{
					this.characterSprite.y += 5 * diagonalFactor;
				}

				if ( this.direction.left )
				{
					this.characterSprite.x -= 5 * diagonalFactor;
				}
				
				if ( this.direction.right )
				{
					this.characterSprite.x += 5 * diagonalFactor;
				}
			}
		}
	]
};

function getFrameRange(spritesheetNumCols, startRow, startCol, numFrames)
{
	// If not set, default to first row
	if ( startRow === undefined )
	{
		startRow = 1;
	}

	// If not set, default to start of row
	if ( startCol === undefined )
	{
		startCol = 1;
	}

	// If not set, default to use a row of frames
	if ( numFrames === undefined )
	{
		numFrames = spritesheetNumCols;
	}
	
	// Adjust for 0 starting indexes
	var startFrame = ((startRow - 1) * spritesheetNumCols) + (startCol - 1);
	var stopFrame = startFrame + (numFrames - 1);

	return {
		start: startFrame,
		end: stopFrame
	};
}

class Character {
    constructor(scene, config) {
		this.scene = scene;
		
		var defaultConfig = {
			debug: true, // Show debug information
		};

		this.config = Object.assign({}, defaultConfig, config);
		
		// Create an event emitter to emit events from this object
		this.events = new Phaser.Events.EventEmitter();

		// Start data collection
		this.data = {};
	}

	destroy() {
	}
}
