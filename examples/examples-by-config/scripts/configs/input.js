
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}


class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');
		
		this.star = this.add.image(400, 300, 'star');

		// Add text for display
		text = this.add.text(10, 10, 'Move the mouse', { font: '16px Courier', fill: '#00ff00' });

		// On pointerdown, add a ball
		this.input.on('pointerdown', function (pointer) {
			console.log(this.game.loop.frame, 'down B');
			var ball = this.add.image(pointer.x, pointer.y, 'balls', Phaser.Math.Between(0, 5));
			ball.alpha = 0.5;
		}, this);

		// On pointerup, check swipe
		this.input.on('pointerup', this.handleSwipe, this);

		// On pointerup, check swipe
		this.swipeLine = new Phaser.Geom.Line();
		this.swipeLineGraphics = this.add.graphics({ lineStyle: { width: 4, color: 0xaa00aa } });
	
		this.previousSwipeGraphics = this.add.graphics({x: 0, y: 0});
		this.previousSwipeGraphics.lineStyle(1, 0xff0000, 0.5);
		this.previousSwipeGraphics.fillStyle(0xff00ff, 0.5);

		this.enemyGraphics = this.add.graphics({x: 0, y: 0, lineStyle: { width: 4, color: 0x0000aa } });
		this.enemyRect = new Phaser.Geom.Rectangle(this.star.getTopLeft().x, this.star.getTopLeft().y, this.star.width, this.star.height);
		this.enemyGraphics.strokeRectShape(this.enemyRect);

		this.intersectedGraphics = this.add.graphics({x: 0, y: 0});
		this.intersectedGraphics.lineStyle(1, 0xffffff, 0.5);
		this.intersectedGraphics.fillStyle(0xffffff, 0.5);

		/*
		// Drag rectangle
		var graphics = this.add.graphics();

		var color = 0xffff00;
		var thickness = 2;
		var alpha = 1;
	
		//  Events
	
		var draw = false;
	
		this.input.on('pointerdown', function (pointer) {
			draw = true;
		}, this);
	
		this.input.on('pointerup', function () {
			draw = false;
		}, this);
	
		this.input.on('pointermove', function (pointer) {
			//if (pointer.isDown)
			if (draw)
			{
				graphics.clear();
				graphics.lineStyle(thickness, color, alpha);
				graphics.strokeRect(pointer.downX, pointer.downY, pointer.x - pointer.downX, pointer.y - pointer.downY);
			}
		}, this);
		*/

		// 1) Create graphics and then add to the physics
		this.myRect = this.add.graphics({x: 0, y: 0});
		this.myRect.lineStyle(1, 0xffff00, 0.5);
		this.myRect.fillStyle(0xff00ff, 0.5);
		this.myRect.strokeRect(0, 0, 100, 100);
		this.physics.add.existing(this.myRect);
		
		// Create a Circle
		var circle = new Phaser.Geom.Circle(this.game.scale.width / 2, this.game.scale.height / 2, 20);
		// And display our circle on the top
		this.circleGraphics = this.scene.scene.add.graphics(0, 0);//game.make.graphics();
		this.circleGraphics.beginPath();
		this.circleGraphics.lineStyle(1, 0x00ff00, 1);
		this.circleGraphics.fillStyle(0x00ff00, 0.5);
		this.circleGraphics.fillCircle(circle.x, circle.y, circle.diameter);
		this.circleGraphics.closePath();
		
		// Interactive graphics need a shape and callback - https://phaser.discourse.group/t/how-to-make-graphics-interactive/487, https://snowbillr.github.io/blog/2018-07-03-buttons-in-phaser-3/
		this.circleGraphics.setInteractive({
			hitArea: circle,
			hitAreaCallback: Phaser.Geom.Circle.Contains,
			draggable: false,
			dropZone: false,
			useHandCursor: true,
			//cursor: CSSString,
			pixelPerfect: false,
			alphaTolerance: 1
		})
		.on("pointerup", function(){
            console.log('test');
		}, this);
		//  Specify a different debug outline color
		this.input.enableDebug(this.circleGraphics, 0xff00ff);

		// Game object shapes do not
		var circleGameObject = this.add.circle(100, 400, 80, 0x6666ff);
		circleGameObject.setInteractive({
			hitArea: new Phaser.Geom.Circle(circleGameObject.width / 2, circleGameObject.height / 2, 80),
			hitAreaCallback: Phaser.Geom.Circle.Contains,
			useHandCursor: true
		})
		.on('pointerover', function() {
            console.log('circleGameObject pointerover');
		}, this)
		.on('pointerdown', function() {
            console.log('circleGameObject pointerdown');
		}, this)
		.on('pointermove', function() {
			if (this.input.activePointer.isDown)
			{
				console.log('circleGameObject pointermove while pointer is down');
			}
			else
			{
				console.log('circleGameObject pointermove');
			}
		}, this)
		.on('pointerup', function() {
            console.log('circleGameObject pointerup');
		}, this)
		.on('pointerout', function() {
            console.log('circleGameObject pointerout');
		}, this);
		//  Specify a different debug outline color
		this.input.enableDebug(circleGameObject, 0xff00ff);

	}

	// App loop
	update(time, delta) {
		//this.scene.start('PreloadScene');

		var pointer = this.input.activePointer;

		text.setText([
			'x: ' + pointer.x,
			'y: ' + pointer.y,
			'mid x: ' + pointer.midPoint.x,
			'mid y: ' + pointer.midPoint.y,
			'velocity x: ' + Math.round(pointer.velocity.x),
			'velocity y: ' + pointer.velocity.y,
			'movementX: ' + pointer.movementX,
			'movementY: ' + pointer.movementY,
			'duration: ' + pointer.getDuration(),
			'angle: ' + pointer.getAngle(),
			'distance: ' + pointer.getDistance()
		]);
	}
	
	handleSwipe(pointer) {
        if (this.canMove) {
            var swipeTime = pointer.upTime - pointer.downTime;
            var fastEnough = swipeTime < gameOptions.swipeMaxTime;
            var swipe = new Phaser.Geom.Point(pointer.upX - pointer.downX, pointer.upY - pointer.downY);
            var swipeMagnitude = Phaser.Geom.Point.GetMagnitude(swipe);
			var longEnough = swipeMagnitude > gameOptions.swipeMinDistance;
			
			console.log('Movement time: ' + swipeTime + ' ms; fastEnough: ' + fastEnough);
			console.log('Distance: ' + swipe.x + ' px, '+ swipe.y + ' px; swipeMagnitude: ' + swipeMagnitude + '; longEnough' + longEnough);

			//var angle = Phaser.Geom.Line.Angle(line);
            var startPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
            var stopPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
			var angle = Phaser.Math.Angle.BetweenPoints(startPoint, stopPoint);
			angle = Phaser.Math.Angle.Normalize(Phaser.Math.Angle.Wrap(angle));
			console.log('angle: ' + angle + ' ' + Phaser.Math.RadToDeg(angle));

            if (longEnough && fastEnough) {
				// Normalize magnitude
                Phaser.Geom.Point.SetMagnitude(swipe, 1);
                if (swipe.x > gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(RIGHT);');
                }
                if (swipe.x < -gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(LEFT);');
                }
                if (swipe.y > gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(DOWN);');
                }
                if (swipe.y < -gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(UP);');
				}
				
				// Draw swipe
				this.previousSwipeGraphics.strokeRect(pointer.downX, pointer.downY, pointer.upX - pointer.downX, pointer.upY - pointer.downY);

				this.previousSwipeGraphics.strokeLineShape(this.swipeLine);
				
				this.swipeLine = new Phaser.Geom.Line(pointer.downX, pointer.downY, pointer.upX, pointer.upY);
				this.swipeLineGraphics.clear();
				this.swipeLineGraphics.lineStyle(3, 0xffff00, 0.75);
				this.swipeLineGraphics.strokeLineShape(this.swipeLine);
				var swipeLineLength = Phaser.Geom.Line.Length(this.swipeLine);
				this.swipeLineGraphics.lineStyle(2, 0x00aa00, 1);
				this.swipeLineGraphics.strokeCircle(pointer.downX, pointer.downY, swipeLineLength);
				console.log('swipeLineLength: ' + swipeLineLength);

				this.triangleGraphics = this.add.graphics({ lineStyle: { width: 2, color: 0x00ff00 }, fillStyle: { color: 0xff0000 } });

				this.arrowTriangle = new Phaser.Geom.Triangle(0, 0, 10, 50, -10, 50);
				// graphics.strokeTriangleShape(triangle);
				this.triangleGraphics.fillTriangleShape(this.arrowTriangle);
				this.triangleGraphics.x = pointer.x;
				this.triangleGraphics.y = pointer.y;
				//this.triangleGraphics.angle = Phaser.Math.RadToDeg(angle);
				this.triangleGraphics.rotation = angle + (Math.PI / 2);
			}
        }
	}
	
	toggleFullscreen()
	{
		if (! this.scale.isFullscreen){
			this.scale.startFullscreen();
		}
		else
		{
			this.scale.stopFullscreen();
		}
	}
}


var gameOptions = {
    tweenSpeed: 50,
    swipeMaxTime: 1000,
    swipeMinDistance: 20,
    swipeMinNormal: 0.85,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 200
			},
			debug: false
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

