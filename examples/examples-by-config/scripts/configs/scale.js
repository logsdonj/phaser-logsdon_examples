
class MainScene extends Phaser.Scene{
    constructor() {
		super('MainScene');
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');
		
		// Add text for display
		this.toggleFullscreenText = this.add.text(10, 10, 'Toggle Fullscreen', { font: '16px Courier', fill: '#00ff00' });
		this.toggleFullscreenText
		.setInteractive({
			useHandCursor: true
		})
		.on('pointerup', function(){
			console.log('toggleFullscreen');
			this.toggleFullscreen();
		}, this);

		this.star = this.add.image(400, 300, 'star');

		// Drag rectangle
		var graphics = this.add.graphics();
		var color = 0xffff00;
		var thickness = 2;
		var alpha = 1;
	
		//  Events
		var draw = false;
	
		this.input.on('pointerdown', function (pointer) {
			draw = true;
		}, this);
	
		this.input.on('pointerup', function () {
			draw = false;
		}, this);
	
		this.input.on('pointermove', function (pointer) {
			//if (pointer.isDown)
			if (draw)
			{
				graphics.clear();
				graphics.lineStyle(thickness, color, alpha);
				graphics.strokeRect(pointer.downX, pointer.downY, pointer.x - pointer.downX, pointer.y - pointer.downY);
			}
		}, this);

		// Create a Circle
		var circle = new Phaser.Geom.Circle(this.game.scale.width / 2, this.game.scale.height / 2, 20);
		// And display our circle on the top
		this.circleGraphics = this.scene.scene.add.graphics(0, 0);//game.make.graphics();
		this.circleGraphics.beginPath();
		this.circleGraphics.lineStyle(1, 0x00ff00, 1);
		this.circleGraphics.fillStyle(0x00ff00, 0.5);
		this.circleGraphics.fillCircle(circle.x, circle.y, circle.diameter);
		this.circleGraphics.closePath();
		
		// Interactive graphics need a shape and callback - https://phaser.discourse.group/t/how-to-make-graphics-interactive/487, https://snowbillr.github.io/blog/2018-07-03-buttons-in-phaser-3/
		this.circleGraphics.setInteractive({
			hitArea: circle,
			hitAreaCallback: Phaser.Geom.Circle.Contains,
			draggable: false,
			dropZone: false,
			useHandCursor: true,
			//cursor: CSSString,
			pixelPerfect: false,
			alphaTolerance: 1
		})
		.on('pointerup', function(){
			console.log('toggleFullscreen');
			this.toggleFullscreen();
		}, this);
		//  Specify a different debug outline color
		this.input.enableDebug(this.circleGraphics, 0xff00ff);

		// Game object shapes do not
		var circleGameObject = this.add.circle(100, 400, 80, 0x6666ff);
		circleGameObject.setInteractive({
			hitArea: new Phaser.Geom.Circle(circleGameObject.width / 2, circleGameObject.height / 2, 80),
			hitAreaCallback: Phaser.Geom.Circle.Contains,
			useHandCursor: true
		})
		.on('pointerover', function() {
            console.log('circleGameObject pointerover');
		}, this)
		.on('pointerdown', function() {
            console.log('circleGameObject pointerdown');
		}, this)
		.on('pointermove', function() {
			if (this.input.activePointer.isDown)
			{
				console.log('circleGameObject pointermove while pointer is down');
			}
			else
			{
				console.log('circleGameObject pointermove');
			}
		}, this)
		.on('pointerup', function() {
            console.log('circleGameObject pointerup');
		}, this)
		.on('pointerout', function() {
            console.log('circleGameObject pointerout');
		}, this);
		//  Specify a different debug outline color
		this.input.enableDebug(circleGameObject, 0xff00ff);

		this.checkOriention(this.scale.orientation);
		this.scale.on('orientationchange', this.checkOriention, this);

		this.scale.on('resize', this.resize, this);
	}

	// App loop
	update(time, delta) {
		//this.scene.start('PreloadScene');

	}
	
	checkOriention(orientation)
	{
		if (orientation === Phaser.Scale.PORTRAIT)
		{
		}
		else if (orientation === Phaser.Scale.LANDSCAPE)
		{
		}
		
		console.log('Orientation: ' + orientation);
		return orientation;
	}

	resize (gameSize, baseSize, displaySize, resolution)
	{
		console.log('Resize: ' + displaySize);

		var width = gameSize.width;
		var height = gameSize.height;
	
		this.cameras.resize(width, height);
		/*
		this.bg.setSize(width, height);
		this.logo.setPosition(width / 2, height / 2);
		*/
	}

	toggleFullscreen()
	{
		if (! this.scale.isFullscreen)
		{
			this.scale.startFullscreen();
		}
		else
		{
			this.scale.stopFullscreen();
		}
	}
}


var gameOptions = {
    tweenSpeed: 50,
    swipeMaxTime: 1000,
    swipeMinDistance: 20,
    swipeMinNormal: 0.85,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
    backgroundColor: '#2dab2d',
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	pixelArt: false,
    //  Open the Dev Tools
	//  The version of your game appears after the title in the banner
	banner: true,
	banner: {
        hidePhaser: true
    },
    title: 'Test',
    version: '0.0b',
    scale: {
        mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: 600,
        height: 800,
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 200
			},
			debug: false
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('MainScene');
				console.log(this.scale);
			}
		},
		// Use scene class
		MainScene
	]
};

