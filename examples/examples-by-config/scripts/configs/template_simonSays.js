
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}

class MyButtonGraphics extends Phaser.GameObjects.Graphics {
    constructor(scene, options) {
		super(scene, options);
		scene.add.existing(this);

		var buttonHeight = 100;
		if (options && typeof(options.height) !== 'undefined')
		{
			buttonHeight = options.height;
		}
		
		var buttonWidth = 100;
		if (options && typeof(options.width) !== 'undefined')
		{
			buttonWidth = options.width;
		}
		
		var buttonColor = 0xffffff;
		if (options && typeof(options.color) !== 'undefined')
		{
			buttonColor = options.color;
		}

		var lineStyle = {
			width: 1,
			color: buttonColor,
			alpha: 1
		};
		//this.lineStyle(1, 0x0000ff, 1.0);

		var fillStyle = {
			color: buttonColor,
			alpha: 1
		};
		//this.fillStyle(0x0000ff, 1.0);

		this.setDefaultStyles({
			lineStyle: lineStyle,
			fillStyle: fillStyle
		});

		this.fillRect(0, 0, buttonWidth, buttonHeight);
		this.setAlpha(0.5);

		this.setDataEnabled();

		this.setInteractive({
			useHandCursor: true,
			hitArea: new Phaser.Geom.Rectangle(0, 0, buttonWidth, buttonHeight),
			hitAreaCallback: Phaser.Geom.Rectangle.Contains
		});
		this
		.on('pointerdown', function() {
			console.log('pointerdown');
		}, this)
		.on('pointerover', function() {
			console.log('pointerover');
			this.setAlpha(0.8);
		}, this)
		.on('pointerout', function() {
			console.log('pointerout');
			this.setAlpha(0.5);
		}, this)
		.on('gameout', function() {
			console.log('gameout');
			this.setAlpha(0.5);
		}, this);
		//this.input.enableDebug(this.myRect2, 0xff00ff);
		
		scene.input.on('gameout', function() {
			this.emit('gameout');
		}, this);
    }
}

class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
		this.checkSwipe = false;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');
		
		// Add text for display
		this.cpuText = this.add.text(10, 10, 'Testing', { font: '16px Courier', fill: '#00ff00' });
		this.promptText = this.add.text((this.game.scale.width / 2), 10, 'Prompt', { font: '16px Courier', fill: '#00ff00' });

		var buttonWidth = 100;

		this.myRectA = new MyButtonGraphics(this);

		this.myRectB = new MyButtonGraphics(this);
		this.myRectB.x = this.game.scale.width - buttonWidth;

		this.myRectC = new MyButtonGraphics(this);
		this.myRectC.y = this.game.scale.height - buttonWidth;

		this.myRectD = new MyButtonGraphics(this);
		this.myRectD.x = this.game.scale.width - buttonWidth;
		this.myRectD.y = this.game.scale.height - buttonWidth;

		this.turn = 'cpu';

		this.choices = ['A', 'B'];
		this.choiceArray = ['A', 'B', 'C', 'D'];
		this.numChoices = this.choices.length;
		this.numAnswers = 5;
		this.answers = [];
		/*
		for (var i = 0; i < this.numAnswers; i++)
		{
			this.answers.push(Phaser.Utils.Array.GetRandom(this.choiceArray));
			//answers.push(choices[0]);
		}
		console.log(this.answers);
		*/

		
		this.input.on('pointerdown', function() {
			console.log('pointerdown');

			this.cpuTurn();
		}, this);

		this.promptText.setText(['Click on the game...']);
		
		this.cpuTurn();
	}

	cpuTurn ()
	{
		this.promptText.setText(['Replay all previous actions, add and play additional action']);
		
		this.promptText.setText(this.promptText.text + "\n" + 'Adding additional action');

		this.answers.push(Phaser.Utils.Array.GetRandom(this.choiceArray));

		this.promptText.setText(this.promptText.text + "\n" + 'Loop over actions');

		for (var i = 0; i < this.answers.length; i++)
		{
			var name = 'myRect' + this.answers[i];
			var item = this[name];
			item.name = name;
			var curAlpha = item.alpha;
			
			// Create tween
			var turnTween = this.tweens.addCounter({
				from: curAlpha,
				to: 1.0,
				duration: 500,
				yoyo: true,
				target: item,
				onUpdate: function (tween)
				{
					console.log(tween);
					this.setAlpha(tween.getValue());
				},
				onUpdateScope: item,
				onComplete: function (tween)
				{
					console.log(this.name);
				},
				onCompleteScope: item
			});
			item.data.set('turnTween', turnTween);
		}

		this.cpuText.setText(this.answers);
	}

	replayAnswer()
	{
		var name = 'myRect' + this.answers[i];
		var item = this[name];
		item.name = name;
		var curAlpha = item.alpha;
		
		// Create tween
		var turnTween = this.tweens.addCounter({
			from: curAlpha,
			to: 1.0,
			duration: 500,
			yoyo: true,
			target: item,
			onUpdate: function (tween)
			{
				console.log(tween);
				this.setAlpha(tween.getValue());
			},
			onUpdateScope: item,
			onComplete: function (tween)
			{
				console.log(this.name);
			},
			onCompleteScope: item
		});
		item.data.set('turnTween', turnTween);
	}

	// App loop
	update(time, delta) {
		//this.scene.start('PreloadScene');

		var pointer = this.input.activePointer;
		var thisScope = this;

		/*
		this.cpuText.setText([
			'x: ' + pointer.x,
			'y: ' + pointer.y
		]);*/
		//this.cpuText.setText('Replay all previous actions, add and play additional action');
	}
	
	handleSwipe(pointer)
	{
		var gestureData = this.canMove ? window.REDLOVE.p.checkGestures(pointer) : null;
		console.log(gestureData);

		if (gestureData && gestureData.isSwipe)
		{
			// Clear mask
			this.star.setMask();

			var thisScope = this;
			var sliceableObj = this.star;
			if (sliceableObj.data.get('dead'))
			{
				return;
			}

			// Check for swipe intersections
			var intersectPoints = window.REDLOVE.p.checkSwipeIntersect(gestureData, sliceableObj);
			if (intersectPoints)
			{
				console.log('intersection');
				
				// Update hits
				var hits = sliceableObj.data.get('hits') || 0;
				hits++;
				sliceableObj.data.set('hits', hits);

				// Make an associative array of intersected sides
				for (var point of intersectPoints)
				{
					var intersectSide = window.REDLOVE.p.checkIntersectionSide(sliceableObj, point);
					point.intersectSide = intersectSide;
				}

				// If intersected one side, it's a hit and not a slice
				if (intersectPoints.length < 2)
				{
					// Impact the object with the hit
					thisScope.addHitTween(sliceableObj);
					
					//var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
					var originPoint = new Phaser.Geom.Point(sliceableObj.x, sliceableObj.y);
					var targetPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
					var pointerDownPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
					var pointerUpPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
					var pointerAxisInfo = window.REDLOVE.p.getAxisDirection(pointerDownPoint, pointerUpPoint);
					var axisInfo = window.REDLOVE.p.getAxisDirection(originPoint, pointerUpPoint);

					var rotationDirection = window.REDLOVE.p.orientationTest(originPoint, pointerDownPoint, pointerUpPoint);
					var angularDirection = rotationDirection;

					var angularVelocity = 50;
					console.log(angularVelocity);

					console.log(pointerAxisInfo.four);
					console.log(pointerAxisInfo.eight);
					console.log(pointerAxisInfo.four.raw == pointerAxisInfo.eight.raw);
					console.log(axisInfo.four);
					console.log(axisInfo.eight);
					
					// If a straight on hit in a cardinal direction (up, down, left, or right), no rotation
					// TODO: Check axis to make sure up/down and left/right and not an up/left, etc. hit that should involve rotation
					if (pointerAxisInfo.four.raw == pointerAxisInfo.eight.raw && axisInfo.four.raw == axisInfo.eight.raw)
					{
						console.log('Not a straight on hit');
					}

					// If a straight on hit in a cardinal direction (up, down, left, or right), no rotation
					if (pointerAxisInfo.four.raw != pointerAxisInfo.eight.raw || axisInfo.four.raw != axisInfo.eight.raw)
					{
						console.log('Not a straight on hit');
					}
				}
				else if (intersectPoints.length == 2)
				{
					var health = sliceableObj.data.get('health') || 1;
					health--;
					sliceableObj.data.set('health', health);

					// Check health
					if (health > 0)
					{
						thisScope.addHitTween(sliceableObj);
					}
					else
					{
						// Stop any existing tweens
						thisScope.stopHitTween(sliceableObj);
						
						// Set to dead
						sliceableObj.data.set('dead', true);

						// Set it to a dead state
						sliceableObj.setTint(Phaser.Display.Color.GetColor(255, 0, 0));
						// Stop physics
					}
				}
			}

        }
	}
	
	stopHitTween(sliceableObj)
	{
		// Stop any existing tweens
		var hitTween = sliceableObj.data.get('hitTween');
		if (hitTween)
		{
			hitTween.stop();
		}
	}

	addHitTween(sliceableObj)
	{
		// Stop any existing tweens
		this.stopHitTween(sliceableObj);

		// Create tween
		var hitTween = this.tweens.addCounter({
			from: 0,
			to: 255,
			duration: 500,
			onUpdate: function (tween)
			{
				var value = Math.floor(tween.getValue());
				sliceableObj.setTint(Phaser.Display.Color.GetColor(255, value, value));
			},
			onComplete: function (tween)
			{
				sliceableObj.setTint();
			}
		});

		sliceableObj.data.set('hitTween', hitTween);
	}


}


var gameOptions = {
    tweenSpeed: 50,
    swipeMaxTime: 1000,
    swipeMinDistance: 20,
    swipeMinNormal: 0.85,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 2
			},
            debug: true,
            debugShowBody: true,
            debugShowStaticBody: true,
            debugShowVelocity: true,
            debugVelocityColor: 0xffff00,
            debugBodyColor: 0x0000ff,
            debugStaticBodyColor: 0xffffff
        }
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

