//https://medium.com/@michaelwesthadley/modular-game-worlds-in-phaser-3-tilemaps-1-958fc7e6bbd6
//https://phaser.io/examples/v3/category/tilemap

// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');

				this.load.image('mario-tiles', 'assets/tilesets/super-mario-tiles.png');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				// Load a map from a 2D array of tile indices
				const level = [
					[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0 ],
					[  0,   1,   2,   3,   0,   0,   0,   1,   2,   3,   0 ],
					[  0,   5,   6,   7,   0,   0,   0,   5,   6,   7,   0 ],
					[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0 ],
					[  0,   0,   0,  14,  13,  14,   0,   0,   0,   0,   0 ],
					[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0 ],
					[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0 ],
					[  0,   0,  14,  14,  14,  14,  14,   0,   0,   0,  15 ],
					[  0,   0,   0,   0,   0,   0,   0,   0,   0,  15,  15 ],
					[ 35,  36,  37,   0,   0,   0,   0,   0,  15,  15,  15 ],
					[ 39,  39,  39,  39,  39,  39,  39,  39,  39,  39,  39 ]
				];

				// When loading from an array, make sure to specify the tileWidth and tileHeight
				const map = this.make.tilemap({ data: level, tileWidth: 16, tileHeight: 16 });
				const tiles = map.addTilesetImage('mario-tiles');
				const layer = map.createLayer(0, tiles, 0, 0);

				
				// Phaser supports multiple cameras, but you can access the default camera like this:
				const camera = this.cameras.main;

				// Set up the arrows to control the camera
				const cursors = this.input.keyboard.createCursorKeys();
				controls = new Phaser.Cameras.Controls.FixedKeyControl({
					camera: camera,
					left: cursors.left,
					right: cursors.right,
					up: cursors.up,
					down: cursors.down,
					speed: 0.5
				});

				// Constrain the camera so that it isn't allowed to move outside the width/height of tilemap
				camera.setBounds(0, 0, map.widthInPixels, map.heightInPixels);

				// Help text that has a "fixed" position on the screen
				this.add
					.text(16, 16, "Arrow keys to scroll", {
					font: "18px monospace",
					fill: "#ffffff",
					padding: { x: 20, y: 10 },
					backgroundColor: "#000000"
					})
					.setScrollFactor(0);
			},

			// App loop
			update: function(time, delta) {
				// Apply the controls to the camera each update tick of the game
				controls.update(delta);
			}
		}
	]
};
