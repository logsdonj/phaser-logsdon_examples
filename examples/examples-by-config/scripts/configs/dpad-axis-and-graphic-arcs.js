
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}


class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	pointerMove(pointer)
	{
		if (pointer.isDown)
		{
			// Analyze the angle of the pointer changes
			var angle = pointer.getAngle();
			var angle = Phaser.Math.Angle.BetweenPoints(this.star, pointer);
			this.star.rotation = angle;

			
			var line = new Phaser.Geom.Line(this.anchorPoint.x, this.anchorPoint.y, pointer.x, pointer.y);
			var line2 = new Phaser.Geom.Line(this.anchorPoint.x, this.anchorPoint.y, this.anchorPoint.x + 50, this.anchorPoint.y + 50);
			this.lineGraphics2 = this.lineGraphics2 || this.add.graphics({x: 0, y: 0});
			this.lineGraphics2.clear();
			this.lineGraphics2.lineStyle(1, 0xff0000, 0.5);
			this.lineGraphics2.strokeLineShape(line);
			
			var lineAngle = Phaser.Math.Angle.BetweenPoints(this.anchorPoint, pointer);
        	Phaser.Geom.Line.SetToAngle(line2, this.anchorPoint.x, this.anchorPoint.y, lineAngle, 50);
			this.lineGraphics2.lineStyle(5, 0x00ff00, 0.5);
			this.lineGraphics2.strokeLineShape(line2);
			
		}
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');

		// Use the center of the screen as an anchor point
		this.anchorPoint = new Phaser.Geom.Point(this.game.scale.width / 2, this.game.scale.height / 2);

		// Add text for display
		text = this.add.text(10, 10, 'Move the mouse', { font: '16px Courier', fill: '#00ff00' });

		// Test axis directional pad
		this.input.on('pointerdown', function(pointer)
		{
			// Use the middle of the screen and the pointer to test dpad ability
			//var originPoint = new Phaser.Geom.Point(this.anchorPoint.x, this.anchorPoint.y);
			var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
			this.drawAxisSegments(originPoint);
		}, this)
		.on('pointermove', function(pointer)
		{
			if (pointer.isDown)
			{
				// Use the middle of the screen and the pointer to test dpad ability
				//var originPoint = new Phaser.Geom.Point(this.anchorPoint.x, this.anchorPoint.y);
				var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
				var targetPoint = new Phaser.Geom.Point(pointer.x, pointer.y);
				var line = new Phaser.Geom.Line(originPoint.x, originPoint.y, targetPoint.x, targetPoint.y);

				this.segmentDivisionMouseMoveGraphics = this.segmentDivisionMouseMoveGraphics || this.add.graphics();
				this.segmentDivisionMouseMoveGraphics.clear();
				this.segmentDivisionMouseMoveGraphics.lineStyle(1, 0xff0000, 1);
				this.segmentDivisionMouseMoveGraphics.strokeLineShape(line);
			}
		}, this)
		.on('pointerup', function(pointer)
		{
			// Use the middle of the screen and the pointer to test dpad ability
			//var originPoint = new Phaser.Geom.Point(this.anchorPoint.x, this.anchorPoint.y);
			var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
			var targetPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
			console.log(this.getAxisDirection(originPoint, targetPoint));
		}, this);
		
		// Draw circle slices
		var graphics = this.add.graphics();

		graphics.fillStyle(0xffff00, 1);
		graphics.slice(this.anchorPoint.x, this.anchorPoint.y, 100, Phaser.Math.DegToRad(337.5), Phaser.Math.DegToRad(22.5), true);
		graphics.fillPath();

		graphics.fillStyle(0xff00ff, 1);
		graphics.slice(this.anchorPoint.x, this.anchorPoint.y, 100, Phaser.Math.DegToRad(67.5), Phaser.Math.DegToRad(22.5), true);
		graphics.fillPath();
		
		// Draw circle arcs
		graphics.lineStyle(4, 0xff00ff, 1);
		//  Without this the arc will appear closed when stroked
		graphics.beginPath();
		// arc (x, y, radius, startAngle, endAngle, anticlockwise)
		graphics.arc(this.anchorPoint.x, this.anchorPoint.y, 200, Phaser.Math.DegToRad(90), Phaser.Math.DegToRad(180), true);
		//  Uncomment this to close the path before stroking
		// graphics.closePath();
		graphics.strokePath();
		//graphics.fillPath();

		// Draw complete and stroke arcs that work like donuts, either clockwise or counter-clockwise
		graphics.beginPath();
		graphics.lineStyle(50, 0xffffff);
		graphics.arc(200, 200, 100, Phaser.Math.DegToRad(0), Phaser.Math.DegToRad(360), false, 0.02);
		graphics.strokePath();
		graphics.closePath();
	
		graphics.beginPath();
		graphics.lineStyle(40, 0xff00ff);
		graphics.arc(200, 200, 100, Phaser.Math.DegToRad(0), Phaser.Math.DegToRad(360), true, 0.02);
		graphics.strokePath();
		graphics.closePath();
	}

	// App loop
	update(time, delta) {
		var pointer = this.input.activePointer;

		text.setText([
			'x: ' + pointer.x,
			'y: ' + pointer.y,
			'mid x: ' + pointer.midPoint.x,
			'mid y: ' + pointer.midPoint.y,
			'velocity x: ' + Math.round(pointer.velocity.x),
			'velocity y: ' + pointer.velocity.y,
			'movementX: ' + pointer.movementX,
			'movementY: ' + pointer.movementY,
			'duration: ' + pointer.getDuration(),
			'angle: ' + pointer.getAngle(),
			'distance: ' + pointer.getDistance()
		]);
	}

	drawAxisSegments(originPoint, segmentLineLength)
	{
		// Create a line and reuse it to rotate around a circle like a clock hand
		var segmentLineLength = segmentLineLength || 100;
		var segmentLine = new Phaser.Geom.Line();

		// Create graphics to reset and display
		this.segmentDivisionGraphics = this.segmentDivisionGraphics || this.add.graphics();
		var segmentDivisionGraphics = this.segmentDivisionGraphics;
		// Clear segment lines
		segmentDivisionGraphics.clear();
		// Break a 360 degree circle up into 16 segments
		var degreeIncrement = 22.5;
		var degreeIteration = 0;
		for (var tmpDegrees = 0; tmpDegrees < 360; tmpDegrees += degreeIncrement)
		{
			degreeIteration++;

			// Determine the lookup index
			var segmentIndex = tmpDegrees / degreeIncrement;
			//console.log('Draw - Degrees: ' + tmpDegrees + ' - ' + (tmpDegrees + degreeIncrement) + ' | Index: ' + segmentIndex);

			// Degrees start to the right at 0 (3 o'clock) and go counterclockwise to 360
			
			// Draw segment lines, changing the style when in the middle of a segment
			if (degreeIteration % 2 > 0)
			{
				segmentDivisionGraphics.lineStyle(1, 0x00ff00, 0.15);
			}
			else
			{
				segmentDivisionGraphics.lineStyle(1, 0x00ff00, 1);
			}

			// Use radians for the angle
			Phaser.Geom.Line.SetToAngle(segmentLine, originPoint.x, originPoint.y, Phaser.Math.DegToRad(tmpDegrees), segmentLineLength);
			segmentDivisionGraphics.strokeLineShape(segmentLine);
		}
	}

	getAxisDirection(originPoint, targetPoint)
	{
		// Get the angle between the points
		var angle = Phaser.Math.Angle.BetweenPoints(originPoint, targetPoint);
		// Wrap and normalize the angle to 0-360 instead of -180-180
		var angleWrapped = Phaser.Math.Angle.Wrap(angle);
		var angleNormalized = Phaser.Math.Angle.Normalize(angleWrapped);
		var angleDegrees = Phaser.Math.RadToDeg(angleNormalized);
		// Get the axis direction lookup info
		var axisLookup = this.getAxisLookup(angleDegrees);
		// Add angle information
		axisLookup.angle = angle;
		axisLookup.angleWrapped = angleWrapped;
		axisLookup.angleNormalized = angleNormalized;
		axisLookup.angleRadians = angleNormalized;
		axisLookup.angleDegrees = angleDegrees;
		return axisLookup;
	}

	getAxisLookup(normalizedDegrees, isRadians, isNormalized)
	{
		// If raw radians are being used instead of normalized degrees, perform operations
		if (typeof(isRadians) !== 'undefined')
		{
			normalizedDegrees = Phaser.Math.Angle.Wrap(normalizedDegrees);
			
			if (typeof(isNormalized) === 'undefined')
			{
				normalizedDegreesPhaser.Math.Angle.Normalize(normalizedDegrees);
			}

			normalizedDegrees = Phaser.Math.RadToDeg(angleNormalized);
		}

		// Lookup arrays broken down into 16 circle segments of 22.5 degrees in a 360 degree circle
		// Starting at 0 to the right at 3 o'clock and going around clockwise

		// 2-way directional pad
		// Lookup for left and right along the X axis
		var segmentDirectionLookup2X = [
			'right',
			'right',
			'right',
			'right',
			'left',
			'left',
			'left',
			'left',
			'left',
			'left',
			'left',
			'left',
			'right',
			'right',
			'right',
			'right'
		];

		// 2-way directional pad
		// Lookup for up and down along the Y axis
		var segmentDirectionLookup2Y = [
			'down',
			'down',
			'down',
			'down',
			'down',
			'down',
			'down',
			'down',
			'up',
			'up',
			'up',
			'up',
			'up',
			'up',
			'up',
			'up'
		];

		// 4-way directional pad
		// These segments are like taking a circle and making an X across it
		// Lookup for left, right, up, and down along the X and Y axis
		var segmentDirectionLookup4 = [
			'right',
			'right',
			'down',
			'down',
			'down',
			'down',
			'left',
			'left',
			'left',
			'left',
			'up',
			'up',
			'up',
			'up',
			'right',
			'right'
		];

		// 8-way directional pad
		// These segments are putting 2 narrow perpendicular Xs across a circle
		// Lookup for left, right, up, and down along the X and Y axis and diaganals
		var segmentDirectionLookup8 = [
			'right',
			'down-right',
			'down-right',
			'down',
			'down',
			'down-left',
			'down-left',
			'left',
			'left',
			'up-left',
			'up-left',
			'up',
			'up',
			'up-right',
			'up-right',
			'right'
		];

		// Break a circle up into 16 segments of 22.5 degrees
		var degreeIncrement = 22.5;
		// Determene which segment the angle is in by how many whole increments go into it
		// Use the floor to get an array index starting at 0
		var segmentIndex = Math.floor(normalizedDegrees / degreeIncrement);
		
		// Determine axis directions from segment from angle
		var axisLookup = {
			degrees: normalizedDegrees,
			degreeIncrement: degreeIncrement,
			segmentIndex: segmentIndex,
			twoX: {
				down: false,
				left: (segmentDirectionLookup2X[segmentIndex] == 'left'),
				raw: segmentDirectionLookup2X[segmentIndex],
				right: (segmentDirectionLookup2X[segmentIndex] == 'right'),
				up: false
			},
			twoY: {
				down: (segmentDirectionLookup2Y[segmentIndex] == 'down'),
				left: false,
				raw: segmentDirectionLookup2Y[segmentIndex],
				right: false,
				up: (segmentDirectionLookup2Y[segmentIndex] == 'up')
			},
			four: {
				down: (segmentDirectionLookup4[segmentIndex] == 'down'),
				left: (segmentDirectionLookup4[segmentIndex] == 'left'),
				raw: segmentDirectionLookup4[segmentIndex],
				right: (segmentDirectionLookup4[segmentIndex] == 'right'),
				up: (segmentDirectionLookup4[segmentIndex] == 'up')
			},
			eight: {
				down: (segmentDirectionLookup8[segmentIndex].indexOf('down') >= 0),
				left: (segmentDirectionLookup8[segmentIndex].indexOf('left') >= 0),
				raw: segmentDirectionLookup8[segmentIndex],
				right: (segmentDirectionLookup8[segmentIndex].indexOf('right') >= 0),
				up: (segmentDirectionLookup8[segmentIndex].indexOf('up') >= 0)
			},
		};

		// 4-way diagonal-only
		axisLookup.fourD = {
			upLeft: axisLookup.twoY.up && axisLookup.twoX.left,
			upRight: axisLookup.twoY.up && axisLookup.twoX.right,
			downLeft: axisLookup.twoY.down && axisLookup.twoX.left,
			downRight: axisLookup.twoY.down && axisLookup.twoX.right
		};

		/*
		console.log('Degrees: ' + angleDegrees + ' | Index: ' + segmentIndex);
		console.log(axisLookup);
		*/

		return axisLookup;
	}
}


var gameOptions = {
    tweenSpeed: 50,
    swipeMaxTime: 1000,
    swipeMinDistance: 20,
    swipeMinNormal: 0.85,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			debug: true,
			gravity: {
				y: 200
			}
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

