// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 200
			},
			debug: false
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');

				// Create loading graphics
				this.progress = this.add.graphics();

				// Set up loading events
				this.load.on('progress', function(progress)
				{
					console.log('Loading... ' + Math.round(progress * 100) + '%');
				}, this);
				
				this.load.on('filecomplete', function(key, type, data)
				{
					console.log('Loading file complete... ' + type + ' ' + key);
				}, this);

				this.load.on('complete', function(loader, totalComplete, totalFailed)
				{
					console.log('Loading complete... ' + totalComplete + ' completed ' + totalFailed + ' failed');
					this.preloadComplete();
				}, this);

				// Load files
				this.load.image('star', 'assets/images/star.png');
				
				this.load.setBaseURL('http://labs.phaser.io');

				this.load.image('sky', 'assets/skies/space3.png');
				this.load.image('logo', 'assets/sprites/phaser3-logo.png');
				this.load.image('red', 'assets/particles/red.png');
				
				// If nothing to load, jump to complete
				//this.preloadComplete();
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				//this.scene.start('PreloadScene');
			},

			// Move on to next scene
			preloadComplete: function()
			{
				if (config.debug) console.log(this.sceneKey + ' preloadComplete');
				
				this.progress.destroy();
				this.scene.start('MainScene');
			}
		}
	]
};