// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 200
			},
			debug: false
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
				
				this.test7 = this.add.text(400, 550, 'Phaser 3', { fontSize: 32 }).setOrigin(0.5).setAlpha(0.5).setBlendMode(1).setScale(2);
				
				/*
				// Generate texture for image from graphics
				this.myRect = this.make.graphics({x: 0, y: 0, add: false});
				this.myRect.generateTexture('starGraphics', 210, 210);
				image = this.add.image(400, 300, 'starGraphics');
				*/
				this.myRect = this.add.graphics({x: 0, y: 0});
				var color = 0xffff00;
				var thickness = 2;
				var alpha = 1;
				this.myRect.lineStyle(thickness, color, alpha);
				this.myRect.fillStyle(0xff00ff, 0.5);
				this.myRect.strokeRect(32, 32, 256, 256);
				this.physics.add.existing(this.myRect);
				
				var r2 = this.add.rectangle(400, 150, 148, 148, 0x9966ff).setStrokeStyle(4, 0xefc53f);
				this.physics.add.existing(r2);
				r2.body.velocity.x = -100;
				r2.body.velocity.y = 100;
				r2.body.bounce.x = 1;
				r2.body.bounce.y = 1;
				r2.body.collideWorldBounds = true;
				
				var startPoint = new Phaser.Math.Vector2(0, 300);
				var controlPoint1 = new Phaser.Math.Vector2(100, 100);
				var controlPoint2 = new Phaser.Math.Vector2(200, 100);
				var endPoint = new Phaser.Math.Vector2(300, 300);
				var curve = new Phaser.Curves.CubicBezier(startPoint, controlPoint1, controlPoint2, endPoint);
				var r = this.add.curve(400, 300, curve, 0x00aa00);
				r.setStrokeStyle(4, 0xff0000);
				// r.setAlpha(0.5);
				// r.setAngle(20);
				// r.setOrigin(1);


				
				// Create a Circle
				var circle = new Phaser.Geom.Circle(this.game.scale.width / 2, this.game.scale.height / 2, 64);
				// And display our circle on the top
				var graphics = this.scene.scene.add.graphics(0, 0);//game.make.graphics();
				console.log(graphics);
				graphics.beginPath();
				graphics.fillStyle(0x00ff00, 0.5);
				graphics.lineStyle(1, 0x00ff00, 1);
				graphics.fillCircle(circle.x, circle.y, circle.diameter);
				graphics.closePath();
				// Set events
				//graphics.inputEnabled = true;
				//graphics.input.useHandCursor = true;
				//graphics.events.onInputDown.addOnce(this.start, this);
			},
			// App loop
			update: function(time, delta) {
				this.test7.rotation += 0.01;
			}
		}
	]
};