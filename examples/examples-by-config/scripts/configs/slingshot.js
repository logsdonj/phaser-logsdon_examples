// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
    physics: {
		// https://github.com/photonstorm/phaser/issues/3570
        default: 'arcade',
        arcade: {
            debug: true,
            gravity: { y: 200 }
        },
		matter: {
			debug: true,
			gravity: { y: 0.5 }
		}
    },
	scene: [
		// Create a scene as an object
		{
			// If using multiple physics engines, do it by scene
			// https://phaser.io/examples/v3/view/physics/multi/arcade-and-matter
			physics: {
				// https://github.com/photonstorm/phaser/issues/3570
				//default: 'arcade',
				arcade: {
					debug: true,
					gravity: { y: 200 }
				},
				matter: {
					debug: true,
					gravity: { y: 0.5 }
				}
			},

			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				// Make something active to be clicked and active for slingshot
				this.slingshotObjImage = new DebugSprite(this, {width: 70, height: 70});
				this.slingshotObjImage.setX(200);
				this.slingshotObjImage.setY(200);
				// Pull from default, origin
				var ssInteraction = new SlingshotInteraction(this, this.slingshotObjImage, {
					debug: true
				});

				// Rotate object in slingshot direction
				this.slingshotObjImageHandleMove = function(ssInteractive)
				{
					// Set to reverse angle for pull effect
					this.setAngle(ssInteractive.data.angleDegreesReverse);
				};
				this.slingshotObjImage.on('slingshot.down', this.slingshotObjImageHandleMove);
				this.slingshotObjImage.on('slingshot.move', this.slingshotObjImageHandleMove);

				// Make something active to be clicked and active for slingshot
				this.slingshotObjImage3 = new DebugSprite(this, {width: 70, height: 70});
				this.slingshotObjImage3.setX(200);
				this.slingshotObjImage3.setY(400);
				// Pull from wherever clicked
				var ssInteraction3 = new SlingshotInteraction(this, this.slingshotObjImage3, {
					debug: true,
					anchorPullFromOrigin: false
				});
				this.slingshotObjImage3.on('slingshot.down', this.slingshotObjImageHandleMove);
				this.slingshotObjImage3.on('slingshot.move', this.slingshotObjImageHandleMove);

				// Make something active to be clicked and active for slingshot
				this.slingshotObjImage4 = new DebugSprite(this, {width: 70, height: 70});
				this.slingshotObjImage4.setX(400);
				this.slingshotObjImage4.setY(400);
				// Pull from an alternate origin
				var ssInteraction4 = new SlingshotInteraction(this, this.slingshotObjImage4, {
					debug: true,
					anchorPullFromOrigin: {x: 0.8, y: 0.8}
				});
				this.slingshotObjImage4.on('slingshot.down', this.slingshotObjImageHandleMove);
				this.slingshotObjImage4.on('slingshot.move', this.slingshotObjImageHandleMove);
				
				// Make something active to be clicked and active for slingshot
				this.slingshotObjImage2 = new DebugSprite(this, {width: 70, height: 70});
				this.slingshotObjImage2.setOrigin(1.0, 0.5);
				this.slingshotObjImage2.setX(400);
				this.slingshotObjImage2.setY(200);
				// Pull from origin with restrictions
				var ssInteraction2 = new SlingshotInteraction(this, this.slingshotObjImage2, {
					debug: true,
					minPullDistance: 0,
					maxPullDistance: 200
				});

				// Create scene variable for reference in scope
				var scene = this;

				// Rotate object in slingshot direction
				this.slingshotObjImage2HandleMove = function(ssInteractive)
				{
					// Stretch object when pulled
					
					this.displayWidth = ssInteractive.data.distance;//rawDistance

					// Constrain width
					if ( this.scaleX < 0.5 )
					{
						this.scaleX = 0.5;
					}
					else if ( this.scaleX > 8.0 )
					{
						this.scaleX = 8.0;
					}

					// Inversely stretch height
					this.scaleY = 1 / this.scaleX;

					// Rotate object

					// Simple follow pointer
					//this.setAngle(ssInteractive.data.angleDegreesReverse);
					//this.angle = ssInteractive.data.angleDegreesReverse;

					// Tween the angle
					// https://phaser.discourse.group/t/smooth-player-rotation/8001/7
					// https://www.youtube.com/watch?v=3WloQupH_PQ
					// https://github.com/ourcade/phaser3-smart-rotation
					var targetAngle = ssInteractive.data.angleDegreesReverse;
					var currentAngle = ssInteractive.obj.angle;
					var angleDiff = targetAngle - currentAngle;
					// Wrap so closest angle is used
					var angleDiffWrapped = Phaser.Math.Angle.WrapDegrees(angleDiff);
					/*// Manually wrap degrees or us Phaser.Math.Angle.WrapDegrees
					var angleDiffWrapped = angleDiff;
					if ( angleDiffWrapped < -180 )
					{
						angleDiffWrapped += 360;
					}
					else if ( angleDiffWrapped > 180 )
					{
						angleDiffWrapped -= 360;
					}
					*/
					var newAngle = currentAngle + angleDiffWrapped;

					/*
					*/
					// Simple tween
					// Only go if angle difference is low to help prevent blips at the -180/180 or 0/360 crossovers
					if ( Math.abs(angleDiff) < 120 )
					{
						//console.log(targetAngle.toFixed(2), currentAngle.toFixed(2), angleDiff.toFixed(2));

						this.slingshotObjImage2AngleTween = scene.tweens.add({
							targets: ssInteractive.obj,
							duration: 1200,
							ease: Phaser.Math.Easing.Elastic.Out,
							angle: targetAngle
						}, scene);
					}
					
					/*
					// More involved tween
					// Works better when just a tween without updates while moving
					// Update tween if currently active
					if ( this.slingshotObjImage2AngleTween && this.slingshotObjImage2AngleTween.isPlaying() )
					{
						console.log(ssInteractive.obj.angle);
						// Creates blips at the -180/180 or 0/360 crossovers
						this.slingshotObjImage2AngleTween.updateTo('angle', newAngle, true);
					}
					// Create tween
					else
					{
						this.slingshotObjImage2AngleTween = scene.tweens.add({
							targets: ssInteractive.obj,
							duration: 100,
							ease: Phaser.Math.Easing.Linear,//Cubic.Out
							angle: newAngle,
							onComplete: function(tween) {
								console.log('completed');

								var targetAngle = ssInteractive.data.angleDegreesReverse;
								var currentAngle = ssInteractive.obj.angle;
								var angleDiff = targetAngle - currentAngle;
								// Wrap so closest angle is used
								var angleDiffWrapped = Phaser.Math.Angle.WrapDegrees(angleDiff);
								var newAngle = currentAngle + angleDiffWrapped;
								
								//ssInteractive.obj.angle = newAngle;
							},
							onCompleteScope: this,
							onUpdate: function(tween, target) {
								var value = tween.getValue();
								console.log('Tween value: ', value.toFixed(2))
							}
						}, scene);
					}
					*/
					
					/*
					// Tween by counter
					// https://phaser.discourse.group/t/smooth-player-rotation/8001/7
					
					// Update tween if currently active
					if ( this.slingshotObjImage2AngleTween && this.slingshotObjImage2AngleTween.isPlaying() )
					{
						this.slingshotObjImage2AngleTween.updateTo('to', newAngle, true);
					}
					// Create tween
					else
					{
						this.slingshotObjImage2AngleTween = scene.tweens.addCounter({
							from: currentAngle,
							to: newAngle,
							duration: 100,
							ease: Phaser.Math.Easing.Linear,
							onUpdate: function(tween) {
								//  tween.getValue = range between 0 and 360
								ssInteractive.obj.setAngle(tween.getValue());
								//var value = tween.getValue();
								//console.log('Tween value: ', value.toFixed(2))
							}
						}, scene);
					}
					*/
				};
				this.slingshotObjImage2.on('slingshot.down', this.slingshotObjImage2HandleMove);
				this.slingshotObjImage2.on('slingshot.move', this.slingshotObjImage2HandleMove);
				this.slingshotObjImage2.on('slingshot.up', function(ssInteractive)
				{
					// Tween the object stretch back into original shape
					scene.tweens.add({
						targets: this,
						duration: 300,
						ease: Phaser.Math.Easing.Bounce.Out,
						scaleX: 1.0,//displayWidth: this.width,
						scaleY: 1.0//displayHeight: this.height,
					}, this);
				});
				
				// Make something active to be clicked and active for slingshot
				this.slingshotObjImage6 = new DebugSprite(this, {width: 70, height: 70});
				this.matter.add.gameObject(this.slingshotObjImage6);//this.matter.add.sprite(400, 600, 'slingshot');//
				
				this.matter.world.setBounds();//.disableGravity();
				this.ground = this.matter.add.sprite(0, 0, 'ground', null, { restitution: 0.4, isStatic: true });
				this.ground.setOrigin(0.5, 0.5);
				this.ground.displayWidth = this.game.scale.width;
				this.ground.displayHeight = 20;
				this.ground.y = this.game.scale.height - 20;
				this.ground.restitution = 0.4;
				this.ground.isStatic = true;
				this.slingshotObjImage6.setOrigin(0.5, 0.5)
				.setPosition(400, 0);
				// Pull from origin with restrictions
				var ssInteraction6 = new SlingshotInteraction(this, this.slingshotObjImage6, {
					debug: true,
				});
				// Rotate object in slingshot direction
				this.slingshotObjImage6HandleDown = function(ssInteractive)
				{
					// Rotate the game object every scene update with the physics body angle from velocity
					var slingshotObjImage5 = this;
					scene.events.off('update', scene.slingshotObjImage6HandleMove);
					scene.events.on('update', scene.slingshotObjImage6HandleMove);
				};
				this.slingshotObjImage6HandleUp = function(ssInteractive)
				{
					// Rotate the game object every scene update with the physics body angle from velocity
					scene.events.off('update', scene.slingshotObjImage6HandleMove);

					// Tween the object stretch back into original shape
					scene.tweens.add({
						targets: this.obj,
						duration: 300,
						ease: Phaser.Math.Easing.Bounce.Out,
						scaleX: 1.0,//displayWidth: this.width,
						scaleY: 1.0//displayHeight: this.height,
					}, this);

					// https://github.com/photonstorm/phaser/issues/3456
					//this.obj.setVelocity(10, 10);angleDegreesReverse, 0.001);
					this.obj.thrust(0.25 * this.data.strength);

				};
				// Rotate object in slingshot direction
				this.slingshotObjImage6HandleMove = function(time, delta)
				{

					// Rotate object

					// Set to reverse angle for pull effect
					//ssInteraction6.obj.setAngle(ssInteraction6.data.angleDegreesReverse);
					// OR
					// Give some friction to the rotation
					var angleDiff = ssInteraction6.obj.angle - ssInteraction6.data.angleDegreesReverse;
					angleDiff = Phaser.Math.Angle.WrapDegrees(angleDiff);
					var newAngle = ssInteraction6.obj.angle - (angleDiff * 0.1);
					newAngle = Phaser.Math.Angle.WrapDegrees(newAngle);
					ssInteraction6.obj.angle = newAngle;
				};
				this.slingshotObjImage6.on('slingshot.down', this.slingshotObjImage6HandleDown, ssInteraction6);
				this.slingshotObjImage6.on('slingshot.up', this.slingshotObjImage6HandleUp, ssInteraction6);
				
				// Make something active to be clicked and active for slingshot
				this.slingshotObjImage5 = new DebugSprite(this, {width: 70, height: 70});
				this.slingshotObjImage5.setOrigin(1.0, 0.5)
				.setPosition(200,600)
				.setDataEnabled();
				this.slingshotObjImage5.data.set('juggles', 0);
				// Pull from origin with restrictions
				var ssInteraction5 = new SlingshotInteraction(this, this.slingshotObjImage5, {
					debug: true,
				});

				// Rotate object in slingshot direction
				this.slingshotObjImage5HandleMove = function(ssInteractive)
				{

					// Stretch object when pulled
					
					this.displayWidth = ssInteractive.data.distance;//rawDistance

					// Constrain width
					if ( this.scaleX < 0.5 )
					{
						this.scaleX = 0.5;
					}
					else if ( this.scaleX > 8.0 )
					{
						this.scaleX = 8.0;
					}

					// Inversely stretch height
					this.scaleY = 1 / this.scaleX;

					// Rotate object

					// Set to reverse angle for pull effect
					this.setAngle(ssInteractive.data.angleDegreesReverse);
				};
				this.slingshotObjImage5.on('slingshot.down', this.slingshotObjImage5HandleMove);
				this.slingshotObjImage5.on('slingshot.move', this.slingshotObjImage5HandleMove);

				this.physics.add.existing(this.slingshotObjImage5, 0);
				this.slingshotObjImage5.body
				.setCollideWorldBounds(true)
				.setAllowRotation(true)
				.setEnable(true)
				.setBounce(0.5, 0.5)
				.setGravity(0, 0)
				.setMass(2)
				//.setMaxVelocity(1000)
				.setAllowGravity(false);
				this.slingshotObjImage5.body.debugShowVelocity = true;
				this.slingshotObjImage5.body.moveable = true;
				this.slingshotObjImage5SceneUpdate = function(time, delta)
				{
					this.obj.rotation = this.obj.body.angle;
				};
				this.slingshotObjImage5.on('slingshot.up', function(ssInteractive)
				{
					var numJuggles = this.data.get('juggles');
					numJuggles++;
					console.log('Juggles: ', numJuggles);
					this.data.set('juggles', numJuggles);

					// Rotate the game object every scene update with the physics body angle from velocity
					var slingshotObjImage5 = this;
					scene.events.off('update', scene.slingshotObjImage5SceneUpdate);
					scene.events.on('update', scene.slingshotObjImage5SceneUpdate, ssInteractive);

					// Tween the object stretch back into original shape
					scene.tweens.add({
						targets: this,
						duration: 300,
						ease: Phaser.Math.Easing.Bounce.Out,
						scaleX: 1.0,//displayWidth: this.width,
						scaleY: 1.0//displayHeight: this.height,
					}, this);

					this.body.setEnable(true);
					// Add velocity to the existing direction
					//scene.physics.velocityFromRotation(this.rotation, 200 * ssInteractive.data.strength, this.body.velocity);
					// OR
					// Add velocity in the new direction
					scene.physics.velocityFromRotation(ssInteractive.data.angleRadiansReverse, 1000 * ssInteractive.data.strength, this.body.velocity);
				});

				this.crates = this.physics.add.group({
					key: 'crate',
					quantity: 10,
					bounceX: 1,
					bounceY: 1,
					collideWorldBounds: true,
					velocityX: 300,
					velocityY: 150
				});
				Phaser.Actions.RandomRectangle(this.crates.getChildren(), this.physics.world.bounds);
				
				// Create wall to bounce off of
				this.wall = this.add.sprite(100, 100, 'wall');
				this.wall.displayHeight = 600;
				this.physics.add.existing(this.wall);this.wall.body
				.setImmovable(true)
				.setCollideWorldBounds(true);
				//this.physics.add.sprite(200, 300, 'wall').setImmovable();

				// Set up collisions
				this.physics.add.collider(this.slingshotObjImage5, [this.wall, this.crates]);

				// Matter

				// Create wall to bounce off of
				this.crate = this.matter.add.sprite(100, 100, 'crate');
				this.crate.displayWidth = 200;
				this.crate.displayHeight = 200;

				this.crate.setFriction(0.05);
				this.crate.setFrictionAir(0.0005);
				this.crate.setBounce(0.9);

				var ground = this.matter.add.sprite(100, 550, 'platform', null, { restitution: 0.4, isStatic: true });
				/*
				*/
			},

			// App loop
			update: function(time, delta) {

			}
		}
	]
};


