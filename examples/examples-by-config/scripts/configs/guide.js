// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
    physics: {
		// https://github.com/photonstorm/phaser/issues/3570
        default: 'arcade',
        arcade: {
            debug: true,
            gravity: { y: 200 }
        }
    },
	scene: [
		// Create a scene as an object
		{

			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				var scene = this;

				// Make something active to be clicked and active for slingshot
				this.guidedObj = new DebugSprite(this, {width: 70, height: 70})
				.setInteractive({cursor: 'pointer'})
				.setDataEnabled();
				this.guidedObj.data.set('juggles', 0);

				this.physics.add.existing(this.guidedObj);
				// https://photonstorm.github.io/phaser3-docs/Phaser.Physics.Arcade.Sprite.html
				this.guidedObj.body
				.setCollideWorldBounds(true)
				.setAllowRotation(true)
				.setEnable(true)
				.setBounce(0.5, 0.5)
				.setGravity(0, 0).setAllowGravity(false)
				.setMass(2)
				.setDrag(0.2).setDamping(true)
				.setMaxVelocity(300);

				this.guidedObjHandleMove = function(time, delta)
				{
					var pointer = scene.input.activePointer;
					
					// Adjust object angle
					
					// Directly with no drag
					//scene.guidedObj.rotation = Phaser.Math.Angle.Between(scene.guidedObj.x, scene.guidedObj.y, pointer.x, pointer.y);

					// Tween the angle
					// https://phaser.discourse.group/t/smooth-player-rotation/8001/7
					// https://www.youtube.com/watch?v=3WloQupH_PQ
					// https://github.com/ourcade/phaser3-smart-rotation
					var targetAngle = Phaser.Math.Angle.Between(scene.guidedObj.x, scene.guidedObj.y, pointer.x, pointer.y);
					var currentAngle = scene.guidedObj.rotation;
					var angleDiff = targetAngle - currentAngle;
					// Wrap so closest angle is used
					var angleDiffWrapped = Phaser.Math.Angle.Wrap(angleDiff);
					/*// Manually wrap degrees or us Phaser.Math.Angle.WrapDegrees
					var angleDiffWrapped = angleDiff;
					if ( angleDiffWrapped < -180 )
					{
						angleDiffWrapped += 360;
					}
					else if ( angleDiffWrapped > 180 )
					{
						angleDiffWrapped -= 360;
					}
					*/
					var newAngle = currentAngle + angleDiffWrapped;
					var angularDrag = 0.1;
					scene.guidedObj.rotation += angleDiffWrapped * angularDrag;


					// Adjust object position

					// Using basic math
					// Bound to left/right movement
					//scene.guidedObj.x -= ((scene.guidedObj.x - scene.input.activePointer.x) * 0.03);
					// Using Arcade Physics
					// Set velocity similar to basic math approach
					// Bound to left/right movement
					scene.guidedObj.body.velocity.x -= ((scene.guidedObj.x - pointer.x) * 0.05);
					// All axis movement
					// https://phaser.io/examples/v3/view/physics/arcade/asteroids-movement
					//this.physics.velocityFromRotation(scene.guidedObj.rotation, 200, scene.guidedObj.body.acceleration);
					// https://phaser.io/examples/v3/view/physics/arcade/accelerate-to
					//this.physics.accelerateToObject(scene.guidedObj, pointer, 600, 300, 300);
					// https://phaser.io/examples/v3/view/physics/arcade/move-to
					// https://phaser.io/examples/v3/view/physics/arcade/move-to-pointer
					//this.physics.moveToObject(scene.guidedObj, pointer, 200);
				};

				this.guidedObjHandleDown = function(pointer)
				{
					console.log(pointer);
					scene.events.off('update', scene.guidedObjHandleMove);
					scene.events.on('update', scene.guidedObjHandleMove, this);
					
					scene.input.once('pointerup', scene.guidedObjHandleUp, this);
					scene.input.once('pointerupoutside', scene.guidedObjHandleUp, this);
				};
				this.guidedObj.on('pointerdown', this.guidedObjHandleDown, this);

				this.guidedObjHandleUp = function(pointer)
				{
					scene.events.off('update', scene.guidedObjHandleMove);
					scene.events.off('pointerup', scene.guidedObjHandleUp);
					scene.events.off('pointerupoutside', scene.guidedObjHandleUp);
					
					// Rotate towards pointer at the end
					var targetAngle = Phaser.Math.Angle.Between(scene.guidedObj.x, scene.guidedObj.y, pointer.x, pointer.y);
					var currentAngle = scene.guidedObj.rotation;
					var angleDiff = targetAngle - currentAngle;
					// Wrap so closest angle is used
					var angleDiffWrapped = Phaser.Math.Angle.Wrap(angleDiff);
					var newAngle = currentAngle + angleDiffWrapped;
					this.guidedObjAngleTween = scene.tweens.add({
						targets: scene.guidedObj,
						duration: 800,
						ease: Phaser.Math.Easing.Cubic.Out,
						rotation: newAngle
					}, scene);

					// If using Arcade Physics for movement
					scene.guidedObj.body.setAcceleration(0);
				};

			},

			// App loop
			update: function(time, delta) {

			}
		}
	]
};


