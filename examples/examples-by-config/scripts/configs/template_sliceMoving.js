
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}


class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
		this.checkSwipe = false;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');
		
		// Create a physics group
		//this.physics.world.setBoundsCollision(true, true, true, true);
		this.sliceableObjectsGroup = this.physics.add.group({
			//bounceX: 1,
			//bounceY: 1,
			collideWorldBounds: false,
			//velocityX: 100
		});

		this.star = this.add.image(this.game.scale.width / 2, this.game.scale.height / 2, 'star');
		this.star.setDataEnabled();
		this.star.data.set('health', 1);
		
		this.star2 = this.add.image(0, 0, 'star');
		this.star2.setDataEnabled();
		this.star2.data.set('health', 1);
		this.star2.x += this.star2.width / 2;
		this.star2.y += this.star2.height / 2;

		this.star3 = this.add.image(this.game.scale.width, this.game.scale.height, 'star');
		this.star3.setDataEnabled();
		this.star3.data.set('health', 2);
		this.star3.x -= this.star3.width / 2;
		this.star3.y -= this.star3.height / 2;
		
		this.sliceableObjectsGroup.add(this.star);
		this.sliceableObjectsGroup.add(this.star2);
		this.sliceableObjectsGroup.add(this.star3);
		this.star.body.setMass(2);
		this.star2.body.setMass(2);
		this.star3.body.setMass(2);
		this.star.body.setMaxVelocity(500);
		this.star2.body.setMaxVelocity(500);
		this.star3.body.setMaxVelocity(500);

		/*
		this.physics.add.collider(this.sliceableObjectsGroup, this.sliceableObjectsGroup, function (ball, block)
		{
			console.log('block velocity', block.body.velocity.x);
		});
		*/

		// Add text for display
		text = this.add.text(10, 10, 'Move the mouse', { font: '16px Courier', fill: '#00ff00' });

		// Reference: https://rexrainbow.github.io/phaser3-rex-notes/docs/site/gesture-overview/
		
		// On pointerup, check swipe
		//this.input.on('pointerdown', function(pointer){console.log('pointerdown');console.log(pointer);}, this);
		//this.input.on('pointerup', function(pointer){console.log('pointerup');}, this);

		this.input.on('pointerdown', function(){this.checkSwipe = true;}, this);
		this.input.on('pointerup', this.handleSwipe, this);
		this.input.on('pointerupoutside', this.handleSwipe, this);

		// On pointerup, check swipe
		this.swipeLine = new Phaser.Geom.Line();
		this.swipeLineGraphics = this.add.graphics({ lineStyle: { width: 4, color: 0xaa00aa } });
	
		this.previousSwipeGraphics = this.add.graphics({x: 0, y: 0});
		this.previousSwipeGraphics.lineStyle(1, 0xff0000, 0.5);
		this.previousSwipeGraphics.fillStyle(0xff00ff, 0.5);

		this.enemyGraphics = this.add.graphics({x: 0, y: 0, lineStyle: { width: 4, color: 0x0000aa } });
		this.enemyRect = new Phaser.Geom.Rectangle(this.star.getTopLeft().x, this.star.getTopLeft().y, this.star.width, this.star.height);
		this.enemyGraphics.strokeRectShape(this.enemyRect);

		this.intersectedGraphics = this.add.graphics({x: 0, y: 0});
		this.intersectedGraphics.lineStyle(1, 0xffffff, 0.5);
		this.intersectedGraphics.fillStyle(0xffffff, 0.5);

		// 1) Create graphics and then add to the physics
		this.myRect = this.add.graphics({x: 0, y: 0});
		this.myRect.lineStyle(1, 0xffff00, 0.5);
		this.myRect.fillStyle(0xff00ff, 0.5);
		this.myRect.strokeRect(0, 0, 100, 100);
		this.physics.add.existing(this.myRect);
	}

	// App loop
	update(time, delta) {
		//this.scene.start('PreloadScene');

		var pointer = this.input.activePointer;

		text.setText([
			'x: ' + pointer.x,
			'y: ' + pointer.y,
			'mid x: ' + pointer.midPoint.x,
			'mid y: ' + pointer.midPoint.y,
			'velocity x: ' + Math.round(pointer.velocity.x),
			'velocity y: ' + pointer.velocity.y,
			'movementX: ' + pointer.movementX,
			'movementY: ' + pointer.movementY,
			'duration: ' + pointer.getDuration(),
			'angle: ' + pointer.getAngle(),
			'distance: ' + pointer.getDistance()
		]);

		var thisScope = this;
		this.sliceableObjectsGroup.children.iterate(function (sliceableObj)
		{
			var overlaps = Phaser.Geom.Rectangle.Overlaps(thisScope.physics.world.bounds, sliceableObj.getBounds());
			if (! overlaps)
			{
				if (sliceableObj.y > thisScope.physics.world.bounds.bottom)
				{
					sliceableObj.y = thisScope.physics.world.bounds.top;
				}
				else if (sliceableObj.y < thisScope.physics.world.bounds.top)
				{
					sliceableObj.y = thisScope.physics.world.bounds.bottom;
				}

				if (sliceableObj.x > thisScope.physics.world.bounds.right)
				{
					sliceableObj.x = thisScope.physics.world.bounds.left;
				}
				else if (sliceableObj.x < thisScope.physics.world.bounds.left)
				{
					sliceableObj.x = thisScope.physics.world.bounds.right;
				}
				
				thisScope.sliceableObjectsGroup.kill(sliceableObj);
			}
		});
	}
	
	stopHitTween(sliceableObj)
	{
		// Stop any existing tweens
		var hitTween = sliceableObj.data.get('hitTween');
		if (hitTween)
		{
			hitTween.stop();
		}
	}

	addHitTween(sliceableObj)
	{
		// Stop any existing tweens
		this.stopHitTween(sliceableObj);

		/*
		var tintColorInteger = sliceableObj.isTinted ? sliceableObj.tintTopLeft : Phaser.Display.Color.GetColor(10, 0, 0);
		console.log(tintColorInteger);
		var tintColor = Phaser.Display.Color.IntegerToColor(tintColorInteger);
		console.log(tintColor);
		
		tintColorInteger = Phaser.Display.Color.GetColor(tintColor.r + (sliceableObj.hits * 10), 0, 0);
		console.log(tintColorInteger);
		tintColor = Phaser.Display.Color.IntegerToColor(tintColorInteger);
		console.log(tintColor);

		var color = tintColorInteger;//Phaser.Display.Color.RandomRGB().color;
		sliceableObj.setTint(color);
		*/
		
		//sliceableObj.setScale(Math.pow(sliceableObj.body.mass, 1 / 3));
		//sliceableObj.setTint(0xff00ff);
		/*
		var color = Phaser.Display.Color.GetColor(sliceableObj.slices * 20, 0, 0);//Phaser.Display.Color.RandomRGB().color;
		sliceableObj.setTintFill(color);
		*/

		// Create tween
		var hitTween = this.tweens.addCounter({
			from: 0,
			to: 255,
			duration: 500,
			onUpdate: function (tween)
			{
				var value = Math.floor(tween.getValue());
				sliceableObj.setTint(Phaser.Display.Color.GetColor(255, value, value));
			},
			onComplete: function (tween)
			{
				sliceableObj.setTint();
			}
		});

		sliceableObj.data.set('hitTween', hitTween);
	}

	handleSwipe(pointer)
	{
		var gestureData = this.canMove ? window.REDLOVE.p.checkGestures(pointer) : null;
		console.log(gestureData)

		if (gestureData && gestureData.isSwipe)
		{
			// Clear mask
			this.star.setMask();

			var thisScope = this;
			this.sliceableObjectsGroup.children.iterate(function (sliceableObj)
			{
				if (sliceableObj.data.get('dead'))
				{
					return;
				}

				// Check for swipe intersections
				var intersectPoints = window.REDLOVE.p.checkSwipeIntersect(gestureData, sliceableObj);
				if (intersectPoints)
				{
					console.log('intersection');
					
					// Update hits
					var hits = sliceableObj.data.get('hits') || 0;
					hits++;
					sliceableObj.data.set('hits', hits);

					// Make an associative array of intersected sides
					for (var point of intersectPoints)
					{
						var intersectSide = window.REDLOVE.p.checkIntersectionSide(sliceableObj, point);
						point.intersectSide = intersectSide;
					}

					// If intersected one side, it's a hit and not a slice
					if (intersectPoints.length < 2)
					{
						// Impact the object with the hit
						thisScope.physics.velocityFromRotation(gestureData.angle, 5, sliceableObj.body.velocity);
						thisScope.addHitTween(sliceableObj);
						
						//var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
						var originPoint = new Phaser.Geom.Point(sliceableObj.x, sliceableObj.y);
						var targetPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
						var pointerDownPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
						var pointerUpPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
						var pointerAxisInfo = window.REDLOVE.p.getAxisDirection(pointerDownPoint, pointerUpPoint);
						var axisInfo = window.REDLOVE.p.getAxisDirection(originPoint, pointerUpPoint);

						var rotationDirection = window.REDLOVE.p.orientationTest(originPoint, pointerDownPoint, pointerUpPoint);
						var angularDirection = rotationDirection;

						var angularVelocity = 50;
						console.log(angularVelocity);

						console.log(pointerAxisInfo.four);
						console.log(pointerAxisInfo.eight);
						console.log(pointerAxisInfo.four.raw == pointerAxisInfo.eight.raw);
						console.log(axisInfo.four);
						console.log(axisInfo.eight);
						
						// If a straight on hit in a cardinal direction (up, down, left, or right), no rotation
						// TODO: Check axis to make sure up/down and left/right and not an up/left, etc. hit that should involve rotation
						if (pointerAxisInfo.four.raw == pointerAxisInfo.eight.raw && axisInfo.four.raw == axisInfo.eight.raw)
						{
							console.log('Not a straight on hit');
						}
						sliceableObj.body.setAngularVelocity(angularVelocity * angularDirection);

						// If a straight on hit in a cardinal direction (up, down, left, or right), no rotation
						if (pointerAxisInfo.four.raw != pointerAxisInfo.eight.raw || axisInfo.four.raw != axisInfo.eight.raw)
						{
							console.log('Not a straight on hit');
							sliceableObj.body.setAngularVelocity(angularVelocity * angularDirection);
						}
					}
					else if (intersectPoints.length == 2)
					{
						var health = sliceableObj.data.get('health') || 1;
						health--;
						sliceableObj.data.set('health', health);

						// Check health
						if (health > 0)
						{
							thisScope.physics.velocityFromRotation(gestureData.angle, 300, sliceableObj.body.velocity);
							thisScope.addHitTween(sliceableObj);
						}
						else
						{
							// Stop any existing tweens
							thisScope.stopHitTween(sliceableObj);
							
							// Set to dead
							sliceableObj.data.set('dead', true);

							// Set it to a dead state
							sliceableObj.setTint(Phaser.Display.Color.GetColor(255, 0, 0));
							// Stop physics
							sliceableObj.body.setAllowGravity(false);
							sliceableObj.body.setImmovable(true);
							sliceableObj.body.setVelocity(0);
							
							/*
							console.log('Build polygons from intersection...');
							var polyObjs = window.REDLOVE.p.getPolygonsFromIntersectPoints(this.enemyRect, intersectPoints);

							this.intersectedGraphics.strokePoints(polyObjs.A, true);
							this.intersectedGraphics.fillPoints(polyObjs.A, true);
							
							this.intersectedGraphics.lineStyle(1, 0xffff00, 0.5);
							this.intersectedGraphics.fillStyle(0xffff00, 0.5);
							this.intersectedGraphics.strokePoints(polyObjs.B, true);
							this.intersectedGraphics.fillPoints(polyObjs.B, true);

							var maskShape = this.make.graphics();
							//  Create a hash shape Graphics object
							maskShape.fillStyle(0xffffff);
							//  You have to begin a path for a Geometry mask to work
							maskShape.beginPath();
							maskShape.fillRect(50, 0, 50, 300);
							maskShape.fillRect(175, 0, 50, 300);
							maskShape.fillRect(0, 75, 275, 50);
							maskShape.fillRect(0, 200, 275, 50);
							maskShape.fillPoints(polyObjs.A, true);
							var mask = maskShape.createGeometryMask();
							this.star.setMask(mask);

							var intersectPointsTemp = Phaser.Geom.Intersects.GetLineToRectangle(gestureData.swipeLine, this.enemyRect);
							this.burstSplitLine = new Phaser.Geom.Line(intersectPointsTemp[0].x, intersectPointsTemp[0].y, intersectPointsTemp[1].x, intersectPointsTemp[1].y);
							var burstSplitLineLength = Phaser.Geom.Line.Length(this.burstSplitLine);
							var midPoint = Phaser.Geom.Line.GetMidPoint(this.burstSplitLine);
							*/
						}
					}
				}
			});

        }
	}
	

}


var gameOptions = {
    tweenSpeed: 50,
    swipeMaxTime: 1000,
    swipeMinDistance: 20,
    swipeMinNormal: 0.85,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 2
			},
            debug: true,
            debugShowBody: true,
            debugShowStaticBody: true,
            debugShowVelocity: true,
            debugVelocityColor: 0xffff00,
            debugBodyColor: 0x0000ff,
            debugStaticBodyColor: 0xffffff
        }
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

