/*
// Create a scene
var bootScene = new Phaser.Scene('BootScene');
var preloadScene = new Phaser.Scene('PreloadScene');
var menuScene = new Phaser.Scene('MenuScene');
var gameScene = new Phaser.Scene('GameScene');

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	parent: 'game',
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {y: 200},
			debug: false
		}
	},
	scene: [
		bootScene,
		preloadScene,
		menuScene,
		gameScene
	]
};
// Create the game and pass it the configuration
var game = new Phaser.Game(config);

bootScene.init = function() {
	if (config.debug) console.log('bootScene.init');
};
bootScene.preload = function() {
	if (config.debug) console.log('bootScene.preload');
	// Load images
	this.load.image('star', 'assets/images/star.png');
};
bootScene.create = function() {
	if (config.debug) console.log('bootScene.create');
	// 1) Create sprite and then add to the physics system
	this.droppingStar = this.add.image(400, 300, 'star');
	this.physics.add.existing(this.droppingStar);
	
};
bootScene.update = function(time, delta) {
	//this.scene.start('PreloadScene');
};
*/
/*

// Create a scene
var gameScene = new Phaser.Scene('Game');
gameScene.init = function() {};
gameScene.preload = function() {
	// Load images
	this.load.image('resourceName', 'resourceURL');
	this.load.image('star', 'assets/images/star.png');
};
gameScene.create = function() {
	this.image = this.add.image(400, 300, 'star');
	
	// 1) Create sprite and then add to the physics system
	this.droppingStar = this.add.image(400, 300, 'star');
	this.physics.add.existing(this.droppingStar);
	// 2) Create and add sprite to physics system
	this.droppingStar2 = this.physics.add.sprite(200, 200, 'star'); 
	
	// Run for input once each event
	this.input.keyboard.on('keyup_D', function(event) {
		this.image.x += 10;
	}, this);
	
	// Poll for input during update
	this.key_A = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
	
	// Run for input once each event
	this.input.on('pointerdown', function(event) {
		this.image.x = event.x;
		this.image.y = event.y;
	}, this);
	
	// Run for input once each event
	this.input.keyboard.on('keyup_P', function(event) {
		var physicsImage = this.physics.add.image(this.image.x, this.image.y, 'star');physicsImage.setVelocity(Phaser.Math.RND.integerInRange(-100, 100), -300);
	}, this);
	
	// Run for input once each event
	// Generic all key up handler
	this.input.keyboard.on('keyup', function(event) {
		// Go to another scene
		if (event.key == '1') {
			this.scene.start('Scene1');
		} else if (event.key == '2') {
			this.scene.start('Scene2');
		} else if (event.key == '3') {
			this.scene.start('Scene3');
		}
	}, this);
};
gameScene.update = function(delta) {
	// Poll for input during update
	if (this.key_A.isDown) {
		this.image.x--;
	}
};

// Set game configuration
var config = {
	type: Phaser.AUTO,
	parent: 'game',
	target: 15,
	pixelArt: false,
	width: 800,
	height: 600,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {y: 200},
			debug: false
		}
	},
	scene: gameScene
}
// Create the game and pass it the configuration
var game = new Phaser.Game(config);
*/

/*
var BootScene = new Phaser.Class({
    Extends: Phaser.Scene,

    initialize: function BootScene ()
    {
        Phaser.Scene.call(this, { key: 'BootScene', active: true });
    },

    preload: function ()
    {
    },

    create: function ()
    {
        this.scene.start('PreloadScene');

        this.input.once('pointerdown', function () {
            this.scene.bringToTop();
        }, this);
    }
});
*/
class BootScene extends Phaser.Scene
{
	// Prepare data
	constructor()
	{
        super('BootScene');
		this.sceneKey = 'BootScene';
		console.log(this.sceneKey);
	}
	
	// Prepare data
	init(data)
	{
		if (config.debug) console.log(this.sceneKey + ' init');
	}

	// Load assets
	preload()
	{
		if (config.debug) console.log(this.sceneKey + ' preload');
	}
	
	// Add objects to app
	create()
	{
		if (config.debug) console.log(this.sceneKey + ' create');

		this.scene.start('PreloadScene');
		
		/*
		// Call method in another scene
		var sceneB = this.scene.get('sceneB');
		var frame = sceneB.getImage();

		// Multiple scenes at the same time
        this.scene.launch('SceneA');
        this.scene.launch('SceneB');
        this.scene.launch('SceneC');
		this.currentScene = this.scene.get('SceneA');
		
		// Example methods
		this.cameras.main.setViewport(0, 136, 1024, 465);
		this.scene.setVisible(false, this.currentScene);
		this.scene.setActive(false, this.currentScene);
		this.scene.isActive(this.currentScene)
		this.scene.bringToTop(this.currentScene);
		this.scene.moveAbove('Controller', this.currentScene);
		this.scene.moveUp(this.currentScene);
		this.scene.getIndex(this.currentScene);
		*/
    }
	
	// App loop
	update(time, delta)
	{
	}
}

class PreloadScene extends Phaser.Scene
{
	// Prepare data
	constructor()
	{
		super('PreloadScene');
		this.sceneKey = 'PreloadScene';
		console.log(this.sceneKey);
	}
	
	// Prepare data
	init(data)
	{
		if (config.debug) console.log(this.sceneKey + ' init');
	}

	// Load assets
	preload()
	{
		if (config.debug) console.log(this.sceneKey + ' preload');
	}
	
	// Add objects to app
	create()
	{
		if (config.debug) console.log(this.sceneKey + ' create');
	}
	
	// App loop
	update(time, delta)
	{
		this.scene.start('MainScene');
	}
}

class MenuScene extends Phaser.Scene
{
	constructor()
	{
		super('MenuScene');
		this.sceneKey = 'MenuScene';
		console.log(this.sceneKey);
	}
	
	// Prepare data
	init(data)
	{
		if (config.debug) console.log(this.sceneKey + ' init');
	}

	// Load assets
	preload()
	{
		if (config.debug) console.log(this.sceneKey + ' preload');
		
		// Load images
	}

	// Add objects to app
	create()
	{
		if (config.debug) console.log(this.sceneKey + ' create');
		
		var bubble = this.add.graphics({ x: 0, y: 0 });
		bubble.fillStyle(0x222222, 0.5);
		bubble.fillRoundedRect(6, 6, 200, 200, 10);
		
		var circleGameObject = this.add.circle(100, 100, 80, 0x0066ff);
		circleGameObject.setInteractive({
			useHandCursor: true
		})
		.on('pointerover', function() {
            console.log(this.sceneKey + 'circleGameObject pointerover');
		}, this)
		.on('pointerdown', function() {
            console.log('circleGameObject pointerdown');
			this.scene.stop();
			this.scene.resume('MainScene');
			
			// Other examples

			// Multiple scenes at the same time
			//this.scene.pause();
			//this.scene.launch('MenuScene');
			//this.scene.bringToTop(this.scene.get('MenuScene'));
			
			// Toggling scenes; sleep/wake(run), pause/resume(run), launch, start
			if (this.scene.isActive('MenuScene'))
			{
				/*
				this.scene.switch('TitleScene');
				*/
				this.scene.stop('MenuScene', 'test');
				//this.scene.run('TitleScene', 'test');
			}
			else
			{
				/*
				this.scene.transition({
					target: 'MenuScene',
					duration: 1000,
					//moveBelow: true,
					//onUpdate: this.transitionOut,
					data: { x: 400, y: 300 }
				});

				this.scene.switch('MenuScene');
				*/
				
				RLGDATA.previousSceneKey = 'TitleScene';
				//this.scene.pause('TitleScene', 'test');
				this.scene.run('MenuScene', 'test');
			}
		}, this);
	}

	// App loop
	update(time, delta)
	{
	}
}

class MainScene extends Phaser.Scene
{
	constructor(config)
	{
		super({ key: 'MainScene', active: true });
		this.sceneKey = 'MainScene';
		console.log(this.sceneKey);
	}
	
	// Prepare data
	init(data)
	{
		if (config.debug) console.log(this.sceneKey + ' init');
	}

	// Load assets
	preload()
	{
		if (config.debug) console.log(this.sceneKey + ' preload');
		
		// Load images
	}

	// Add objects to app
	create()
	{
		if (config.debug) console.log(this.sceneKey + ' create');
		
		var bubble = this.add.graphics({ x: 0, y: 0 });
		bubble.fillStyle(0x222222, 0.5);
		bubble.fillRoundedRect(6, 6, 100, 100, 16);

		
		var circleGameObject = this.add.circle(150, 150, 80, 0x6666ff);
		circleGameObject.setInteractive({
			useHandCursor: true
		})
		.on('pointerover', function() {
            console.log(this.sceneKey + 'circleGameObject pointerover');
		}, this);
		
		
		// Scene events
		
		this.events.on('launch', function(data)
		{
			console.log('launch');
		}, this);
		this.events.on('run', function(data)
		{
			console.log('run');
		}, this);
		this.events.on('sleep', function(data)
		{
			console.log('sleep');
		}, this);
		this.events.on('wake', function(data)
		{
			console.log('wake');
		}, this);
		this.events.on('pause', function(data)
		{
			console.log('pause');
		}, this);
		this.events.on('resume', function(data)
		{
			console.log('resume');
		}, this);
		
		// Multiple scenes at the same time
		this.scene.pause();
		this.scene.launch('MenuScene');
		this.scene.bringToTop(this.scene.get('MenuScene'));
	}

	// App loop
	update(time, delta)
	{
		//this.scene.start('PreloadScene');
	}
}


// Set game configuration
var config = {
    backgroundColor: '#2dab2d',
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	pixelArt: false,
    //  Open the Dev Tools
	//  The version of your game appears after the title in the banner
	banner: true,
	banner: {
        hidePhaser: true
    },
    title: 'Test',
    version: '0.0b',
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function(data) {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		PreloadScene,
		MenuScene,
		MainScene
	]
};

