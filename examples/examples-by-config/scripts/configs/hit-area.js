// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				var x2Start = this.game.scale.width * 0.5;
				var y1Start = this.game.scale.height * 0.1;
				var width = this.game.scale.width * 0.5;
				var height = this.game.scale.height * 0.9;

				var hitArea = new Phaser.Geom.Rectangle(0, 0, width, height);

				var graphicsInteractiveConfig = {
					hitArea: hitArea, // shape
					hitAreaCallback: Phaser.Geom.Rectangle.Contains,
					useHandCursor: true
				};

				var leftRectGraphics = this.add.rectangle(0, y1Start, width, height, 0xff0000, 0.6)
				.setStrokeStyle(2, 0xff0000, 0.8)
				.setOrigin(0, 0)
				.setInteractive(graphicsInteractiveConfig);

				leftRectGraphics.on('pointerdown', function(pointer) {
					console.log('Boop left');
					this.dialogText.setText('Boop left');
				}, this);

				var rightRectGraphics = this.add.rectangle(x2Start, y1Start, width, height, 0xff00ff, 0.6)
				.setStrokeStyle(2, 0xff0000, 0.8)
				.setOrigin(0, 0)
				.setInteractive(graphicsInteractiveConfig);

				rightRectGraphics.on('pointerdown', function(pointer) {
					console.log('Boop right');
					this.dialogText.setText('Boop right');
				}, this);
				
				//  Just a visual display of the drop zone
				var graphics = this.add.graphics();
				graphics.lineStyle(2, 0xffff00);
				graphics.strokeRect(leftRectGraphics.x, leftRectGraphics.y, leftRectGraphics.input.hitArea.width, leftRectGraphics.input.hitArea.height);
				
				// Text
				this.dialogText = this.add.text(
					this.game.scale.width * 0.5, 
					this.game.scale.height * 0.5, 
					"Click on a side", 
					{ 
						backgroundColor: 'rgba(0,0,0,0.8)',
						font: '12px Arial, sans-serif', 
						fill: 'rgb(255,255,255)',
						stroke: 'rgb(47,92,31)',
						strokeThickness: 0,
						shadow: {
							offsetX: 4,
							offsetY: 4,
							color: 'rgba(0,0,0,1)',
							blur: 0,
							stroke: true,
							fill: true
						},
						padding: { // Adjust to work with shadown offset and blur to avoid clipping
							x: 10,
							y: 10
						},
						align: 'center'
					}
				)
				.setOrigin(0.5)
				//.setFixedSize(this.game.scale.width, this.game.scale.height)
				.setDepth(10);
			},

			// App loop
			update: function(time, delta) {
			}
		}
	]
};
