
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}

class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
		this.checkSwipe = false;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');
		
		// Add text for display
		this.cpuText = this.add.text(10, 10, 'Testing', { font: '16px Courier', fill: '#00ff00' });
		this.promptText = this.add.text((this.game.scale.width / 2), 10, 'Prompt', { font: '16px Courier', fill: '#00ff00' });

		this.input.on('pointerdown', function() {
			console.log('pointerdown');

			//this.cpuTurn();
		}, this);

		this.promptText.setText(['Click on the game...']);
		
		//this.cpuTurn();

		this.choices = ['a', 'b', 'c'];
		this.cpuTurn = true;

		this.manifest = [
			{
				message: 'test',
				time: 1000
			},
			{
				message: 'test2',
				time: 1000
			},
			{
				message: 'test3',
				time: 1000
			}
		];
		this.manifestIndex = 0;

		
		this.processManifest(this.manifestIndex);
	}

	replayChoices ()
	{
		if (this.choices.length == 0)
		{
			return;
		}

		var manifestEntry = this.manifest[mIndex];
		
		console.log(manifestEntry.message);

		// Create tween
		var choiceTween = this.tweens.addCounter({
			from: 0,
			to: 1,
			duration: manifestEntry.time,
			target: this,
			onUpdate: function (tween)
			{
				//console.log(tween.getValue());
			},
			onUpdateScope: this,
			onComplete: function (tween)
			{
				console.log(this.manifestIndex + ' ' + manifestEntry.message);
				this.manifestIndex++;
				this.processManifest(this.manifestIndex);
			},
			onCompleteScope: this
		});
	}

	processManifest (mIndex)
	{
		if (mIndex >= this.manifest.length)
		{
			return;
		}

		var manifestEntry = this.manifest[mIndex];
		
		console.log(manifestEntry.message);

		// Create tween
		var turnTween = this.tweens.addCounter({
			from: 0,
			to: 1,
			duration: manifestEntry.time,
			target: this,
			onUpdate: function (tween)
			{
				//console.log(tween.getValue());
			},
			onUpdateScope: this,
			onComplete: function (tween)
			{
				console.log(this.manifestIndex + ' ' + manifestEntry.message);
				this.manifestIndex++;
				this.processManifest(this.manifestIndex);
			},
			onCompleteScope: this
		});
	}

	cpuTurn ()
	{
		this.promptText.setText(['Replay all previous actions, add and play additional action']);
		
		this.promptText.setText(this.promptText.text + "\n" + 'Adding additional action');

		this.answers.push(Phaser.Utils.Array.GetRandom(this.choiceArray));

		this.promptText.setText(this.promptText.text + "\n" + 'Loop over actions');

		for (var i = 0; i < this.answers.length; i++)
		{
			var name = 'myRect' + this.answers[i];
			var item = this[name];
			item.name = name;
			var curAlpha = item.alpha;
			
			// Create tween
			var turnTween = this.tweens.addCounter({
				from: curAlpha,
				to: 1.0,
				duration: 500,
				yoyo: true,
				target: item,
				onUpdate: function (tween)
				{
					console.log(tween);
					this.setAlpha(tween.getValue());
				},
				onUpdateScope: item,
				onComplete: function (tween)
				{
					console.log(this.name);
				},
				onCompleteScope: item
			});
			item.data.set('turnTween', turnTween);
		}

		this.cpuText.setText(this.answers);
	}

	replayAnswer()
	{
		var name = 'myRect' + this.answers[i];
		var item = this[name];
		item.name = name;
		var curAlpha = item.alpha;
		
		// Create tween
		var turnTween = this.tweens.addCounter({
			from: curAlpha,
			to: 1.0,
			duration: 500,
			yoyo: true,
			target: item,
			onUpdate: function (tween)
			{
				console.log(tween);
				this.setAlpha(tween.getValue());
			},
			onUpdateScope: item,
			onComplete: function (tween)
			{
				console.log(this.name);
			},
			onCompleteScope: item
		});
		item.data.set('turnTween', turnTween);
	}

	// App loop
	update(time, delta) {
		//this.scene.start('PreloadScene');

		var pointer = this.input.activePointer;
		var thisScope = this;

		/*
		this.cpuText.setText([
			'x: ' + pointer.x,
			'y: ' + pointer.y
		]);*/
		//this.cpuText.setText('Replay all previous actions, add and play additional action');
	}
	
}


var gameOptions = {
    tweenSpeed: 50,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 2
			},
            debug: true,
            debugShowBody: true,
            debugShowStaticBody: true,
            debugShowVelocity: true,
            debugVelocityColor: 0xffff00,
            debugBodyColor: 0x0000ff,
            debugStaticBodyColor: 0xffffff
        }
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

