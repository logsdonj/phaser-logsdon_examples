// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 200
			},
			debug: false
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				// 1) Create graphics and then add to the physics
				this.myRect = this.add.graphics({x: 0, y: 0});
				var color = 0xffff00;
				var thickness = 2;
				var alpha = 1;
				this.myRect.lineStyle(thickness, color, alpha);
				this.myRect.fillStyle(0xff00ff, 0.5);
				this.myRect.strokeRect(0, 0, 100, 100);
				this.physics.add.existing(this.myRect);
				
				// Create a Circle
				var circle = new Phaser.Geom.Circle(this.game.scale.width / 2, this.game.scale.height / 2, 20);
				// And display our circle on the top
				var graphics = this.scene.scene.add.graphics(0, 0);//game.make.graphics();
				console.log(graphics);
				graphics.beginPath();
				graphics.fillStyle(0x00ff00, 0.5);
				graphics.lineStyle(1, 0x00ff00, 1);
				graphics.fillCircle(circle.x, circle.y, circle.diameter);
				graphics.closePath();
				// Set events
				//graphics.inputEnabled = true;
				//graphics.input.useHandCursor = true;
				//graphics.events.onInputDown.addOnce(this.start, this);
			},
			// App loop
			update: function(time, delta) {
				//this.scene.start('PreloadScene');
			}
		}
	]
};