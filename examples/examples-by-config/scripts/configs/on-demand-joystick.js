// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				// TODO: Joystick/Action Hitarea before activation

				// Create graphics for player texture
				this.playerGraphics = this.add.graphics({ x: 0, y: 0 });

				// Create player rectangle
				var playerRect = new Phaser.Geom.Rectangle(0, 0, 100, 100);
				this.playerGraphics.lineStyle(2, 0x565656, 0.5);
				this.playerGraphics.strokeRectShape(playerRect, 0xff0000, 0.5);
				this.playerGraphics.fillStyle(0x565656, 0.5);
				this.playerGraphics.fillRectShape(playerRect, 0xff0000, 0.5);

				// Create triangle on graphics starting at and pointing to 0 degrees
				var playerTriangle = new Phaser.Geom.Triangle.BuildEquilateral(playerRect.width, playerRect.height / 2, playerRect.width / 2);
				playerTriangle = Phaser.Geom.Triangle.RotateAroundXY(playerTriangle, playerRect.width, playerRect.height / 2, Phaser.Math.DegToRad(90));
				this.playerGraphics.strokeTriangleShape(playerTriangle);
				this.playerGraphics.fillTriangleShape(playerTriangle);

				// Create texture from graphics and destroy graphic
				this.playerTexture = this.playerGraphics.generateTexture('playerTexture', playerRect.width, playerRect.height);
				this.playerGraphics.destroy();

				// Create player from texture
				this.player = this.add.image(this.game.scale.width / 2, (this.game.scale.height / 2) - playerRect.height, 'playerTexture')
				.setAngle(-90)
				.setOrigin(0.5);

				/*
				// Create player rectangle
				this.player = this.add.rectangle(this.game.scale.width / 2, this.game.scale.height / 3, 100, 100, 0xff0000, 0.5)
				.setStrokeStyle(2, 0x1a65ac)
				.setOrigin(0.5, 0.5);
				*/
				
				// Create joystick
				var testJoystickConfig = {
					debug: false,
					centerPoint: {
						x: 100,
						y: 100
					}
				};
				this.testJoystick = new VirtualJoyStick(this, testJoystickConfig);

				// Create joystick
				this.OnDemandJoystick = new VirtualJoyStick(this);
				this.OnDemandJoystick.events
				.on('down', function()
				{
					this.joystickDown = true;
				}, this)
				.on('up', function()
				{
					this.joystickDown = false;
				}, this)
				.on('move', function()
				{
				}, this);
			},

			// App loop
			update: function(time, delta) {
				// If joystick being used
				if ( this.joystickDown )
				{
					// Create a minimum distance moved before use
					if ( this.OnDemandJoystick.data.distanceConstrained > 3 )
					{
						var maxMovement = 10;
						var movementType = 'snap';
						var maxMovementX = maxMovement;
						var maxMovementY = maxMovement;
						var signX = 1;
						var signY = 1;

						// TODO: Look into combining similar movement functionality

						if ( movementType == 'snap' )
						{
							// Set to player angle to movement, snapping to direction angles
							this.player.angle = this.OnDemandJoystick.data.direction.eight.directionDegrees;
		
							// If moving diagonally
							if ( this.OnDemandJoystick.data.direction.eight.raw.indexOf('-') !== -1 )
							{
								// Normalize diagonal movement to be equal to horizontal and vertical movement
								// https://www.reddit.com/r/gamemaker/comments/6ph578/simple_way_to_limit_diagonal_speed/
								// https://docs.yoyogames.com/source/dadiospice/002_reference/maths/vector%20functions/normalised%20vectors.html
								//diagFactor = Math.cos(45);
								var diagonalNormalization = 0.7071; // 1 / sqrt(2)
								maxMovementX = maxMovement * diagonalNormalization;
								maxMovementY = maxMovement * diagonalNormalization;
							}

							if ( this.OnDemandJoystick.data.direction.eight.up )
							{
								signY = -1;
								this.player.y -= this.OnDemandJoystick.data.strength * maxMovementY;
							}
							else if ( this.OnDemandJoystick.data.direction.eight.down )
							{
								this.player.y += this.OnDemandJoystick.data.strength * maxMovementY;
							}
							
							if ( this.OnDemandJoystick.data.direction.eight.left )
							{
								signX = -1;
								this.player.x -= this.OnDemandJoystick.data.strength * maxMovementX;
							}
							else if ( this.OnDemandJoystick.data.direction.eight.right )
							{
								this.player.x += this.OnDemandJoystick.data.strength * maxMovementX;
							}
						}
						else
						{
							// Set the player angle to movement, openly with no snapping to direction angles
							this.player.angle = this.OnDemandJoystick.data.angleDegrees;

							// Normalize diagonal movement to be equal to horizontal and vertical movement
							// https://www.reddit.com/r/gamemaker/comments/6ph578/simple_way_to_limit_diagonal_speed/
							// https://docs.yoyogames.com/source/dadiospice/002_reference/maths/vector%20functions/normalised%20vectors.html

							// Vector components for free range movement and diagonal vs horizontal/vertical movement
							var vX = this.OnDemandJoystick.data.targetPoint.x - this.OnDemandJoystick.data.originPoint.x; // x2 - x1
							var vY = this.OnDemandJoystick.data.targetPoint.y - this.OnDemandJoystick.data.originPoint.y; // y2 - y1
							var vLen = Math.sqrt(Math.pow(vX, 2) + Math.pow(vY, 2)); // sqr(vx^2 + vy^2)
							var normalizedVX = vX / vLen; // vx / len
							var normalizedVY = vY / vLen; // vy / len

							signX = Math.sign(vX);
							signY = Math.sign(vY);

							maxMovementX = signX * maxMovement * normalizedVX;
							maxMovementY = signY * maxMovement * normalizedVY;
							
							this.player.x += signX * this.OnDemandJoystick.data.strength * maxMovementX;
							this.player.y += signY * this.OnDemandJoystick.data.strength * maxMovementY;
						}
	
						RL.p.constrainObjectPosition(this.player, this.game);
					}
				}

			}
		}
	]
};

class VirtualJoyStick {
    constructor(scene, config) {
		this.scene = scene;
		
		var defaultConfig = {
			debug: true, // Show debug information

			maxDistance: 60, // Max knob movement distance from center

			shape: 'circle', // Knob shap
			size: 40, // Knob size
			base: true, // Show knob base

			static: true, // Don't move joystick with pointer

			centerPoint: {
				x: this.scene.game.scale.width / 2,
				y: this.scene.game.scale.height / 2
			}
		};

        if (config === undefined) {
        	config = {};
        }

		this.config = Object.assign({}, defaultConfig, config);
		
		this.data = {};
		this.data.knobShape = this.config.shape;
		this.data.maxMoveDistance = this.config.maxDistance;
		this.data.knobDiameter = this.config.size;

		this.data.knobRadius = this.data.knobDiameter / 2;

		//var joystickCircle = new Phaser.Geom.Circle((this.game.scale.width - touchpadWidth) / 2, this.game.scale.height - touchpadHeight, touchpadWidth, touchpadHeight);
		this.data.knobHitArea = new Phaser.Geom.Circle(0, 0, this.data.knobRadius);
		this.data.knobInteractiveConfig = {
			hitArea: this.data.knobHitArea, // shape
			hitAreaCallback: Phaser.Geom.Circle.Contains,
			useHandCursor: true
		};
		
		// If showing a knob base
		if ( this.config.base )
		{
			// Create it
			this.data.knobBase = this.scene.add.graphics({ x: 0, y: 0 })
			.setVisible(false);
			this.data.knobBase.lineStyle(2, 0x565656, 0.5);
			this.data.knobBase.strokeCircle(0, 0, this.data.knobRadius);
		}

		// Constrain knob to an area
		this.data.knobMaxCircle;
		this.data.knobMaxSquare;

		// If debugging
		if ( this.config.debug )
		{
			// Show visible constraint areas

			// Draw a line
			this.data.knobLine = this.scene.add.line(0, 0, 0, 0, 0, 0, 0xff0000, 0.8)
			.setOrigin(0, 0)
			.setVisible(false);
		
			// Draw a circle
			this.data.knobMaxCircleAreaCircle = this.scene.add.circle(0, 0, this.data.maxMoveDistance, 0xff0000, 0.2)
			.setVisible(false);
			
			// Draw a square
			this.data.knobMaxAreaSquare = this.scene.add.rectangle(0, 0, this.data.maxMoveDistance * 2, this.data.maxMoveDistance * 2, 0xff0000, 0.2)
			.setVisible(false);
		}
		
		this.data.knob = this.scene.add.graphics(this.config.centerPoint);
		//.setOrigin(0.5, 0.5);
		this.data.knob.fillStyle(0xffffff, 0.6);
		this.data.knob.lineStyle(4, 0x565656, 0.6);
		this.data.knob.strokeCircle(0, 0, this.data.knobRadius);
		this.data.knob.fillCircle(0, 0, this.data.knobRadius);

		this.data.knobStaticPoint = {
			downX: this.data.knob.x,
			downY: this.data.knob.y,
			upX: this.data.knob.x,
			upY: this.data.knob.y,
			x: this.data.knob.x,
			y: this.data.knob.y,
		};
		
		this.data.knob.setInteractive(this.data.knobInteractiveConfig)
		.on('pointerdown', this.pointerDown, this)
		.on('pointerup', this.pointerUp, this);
		
		// Create an event emitter to emit events from this object
		this.events = new Phaser.Events.EventEmitter();
	}

	pointerDown(pointer)
	{
		console.log('pointerdown');

		// Use the middle of the screen and the pointer to test dpad ability
		var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
		if ( this.config.static )
		{
			originPoint = this.data.knobStaticPoint;
		}
		
		// If showing a knob base
		if ( this.config.base )
		{
			// Update and show it
			//joystickGhost.strokeCircle(originPoint.x, originPoint.y, joystickRadius);
			this.data.knobBase.setPosition(originPoint.x, originPoint.y)
			.setVisible(true);
		}
		
		// Update constraint areas
		this.data.knobMaxCircle = new Phaser.Geom.Circle(originPoint.x, originPoint.y, this.data.maxMoveDistance);
		this.data.knobMaxSquare = new Phaser.Geom.Rectangle(originPoint.x, originPoint.y, this.data.maxMoveDistance * 2, this.data.maxMoveDistance * 2);

		// If debugging
		if ( this.config.debug )
		{
			// Update visible constraint areas
			this.data.knobMaxCircleAreaCircle.setPosition(originPoint.x, originPoint.y)
			.setVisible(true);
			this.data.knobMaxAreaSquare.setPosition(originPoint.x, originPoint.y)
			.setVisible(true);
		}

		this.updateJoystick(pointer);

		this.scene.input.on('pointermove', this.pointerMove, this);
		this.scene.input.once('pointerup', this.pointerUp, this);

		// Emit event
		this.events.emit('down', 'test');
	}

	pointerUp(pointer)
	{
		console.log('pointerup');

		this.scene.input.off('pointermove', this.pointerMove, this);

		// If static
		if ( this.config.static )
		{
			// Return the joystick to its starting point
			this.updateJoystick(this.data.knobStaticPoint);
		}

		// If showing a knob base
		if ( this.config.base )
		{
			// Hide
			this.data.knobBase.setVisible(false);
		}
		
		// If debugging
		if ( this.config.debug )
		{
			// Hide
			this.data.knobLine.setVisible(false);
			this.data.knobMaxCircleAreaCircle.setVisible(false);
			this.data.knobMaxAreaSquare.setVisible(false);
		}
		
		// Emit event
		this.events.emit('up', 'test');
	}

	pointerMove(pointer)
	{
		console.log('pointerMove');

		if ( pointer.isDown )
		{
			this.updateJoystick(pointer);
		}
	}

	updateJoystick(pointer)
	{
		console.log('updateJoystick');

		// Use the middle of the screen and the pointer to test dpad ability
		var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
		if ( this.config.static )
		{
			originPoint = this.data.knobStaticPoint;
		}
		var targetPoint = new Phaser.Geom.Point(pointer.x, pointer.y);

		// Check distance in a square container versus a circle
		var distanceContainerType = 'circle';

		// Square check
		var distance = Phaser.Math.Distance.BetweenPoints(originPoint, targetPoint);
		this.data.angleRadians = Phaser.Math.Angle.BetweenPoints(originPoint, targetPoint);
		this.data.angleDegrees = Phaser.Math.RadToDeg(this.data.angleRadians);
		var angleRadiansNormalized = Phaser.Math.Angle.Normalize(this.data.angleRadians);
		var diffX = originPoint.x - targetPoint.x;
		var diffY = originPoint.y - targetPoint.y;

		//console.log('distance:', distance, 'this.data.maxMoveDistance:', this.data.maxMoveDistance, 'angle:', angle, 'angleNormalized:', angleNormalized, 'diffX:', diffX, diffX / Math.abs(diffX), 'diffY:', diffY);

		if ( distanceContainerType == 'circle' )
		{
			var circlePoint = Phaser.Geom.Circle.CircumferencePoint(this.data.knobMaxCircle, this.data.angleRadians);

			// If the joystick movement is past the max allowed, constrain it
			if ( distance > this.data.maxMoveDistance )
			{
				targetPoint.x = circlePoint.x;
				targetPoint.y = circlePoint.y;
			}
		}
		// Constrain by x and y like within a square vs hypotenuse, which is longer than the sides
		else
		{
			// If the joystick movement is past the max in either direction, constrain it
			if ( Math.abs(diffX) > this.data.maxMoveDistance ) {
				targetPoint.x = originPoint.x - (Math.sign(diffX) * this.data.maxMoveDistance);
			}

			if ( Math.abs(diffY) > this.data.maxMoveDistance ) {
				targetPoint.y = originPoint.y - (Math.sign(diffY) * this.data.maxMoveDistance);
			}
		}

		var distanceConstrained = Math.min(Phaser.Math.Distance.BetweenPoints(originPoint, targetPoint), this.data.maxMoveDistance);
		this.data.distanceConstrained = distanceConstrained;
		this.data.strength = distanceConstrained / this.data.maxMoveDistance;
		this.data.strength = Math.round((this.data.strength + Number.EPSILON) * 100) / 100; // Round to 2 decimal places

		//console.log('distanceConstrained:', distanceConstrained, 'this.data.maxMoveDistance:', this.data.maxMoveDistance, 'strength:', strength);

		this.data.knob.setPosition(targetPoint.x, targetPoint.y);
		
		// Get joystick direction data
		this.data.direction = RL.p.getAxisDirection(originPoint, targetPoint);
		this.data.originPoint = originPoint;
		this.data.targetPoint = targetPoint;
		
		// If debugging
		if ( this.config.debug )
		{
			// Update line
			this.data.knobLine.setTo(originPoint.x, originPoint.y, targetPoint.x, targetPoint.y).setVisible(true);
		}

		// Emit event
		this.events.emit('move', 'test');//this.scene.events.emit('move', 'test');
	}

	destroy()
	{
		this.events.destroy();
		this.knob.destroy();
		this.knobBase.destroy();
	}
}
