
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}


class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
		this.checkSwipe = false;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');
		
		// Add text for display
		text = this.add.text(10, 10, 'Move the mouse', { font: '16px Courier', fill: '#00ff00' });

		// Add an object to slice
		this.star = this.add.image(this.game.scale.width / 2, this.game.scale.height / 2, 'star');
		// Add data to slice object
		this.star.setDataEnabled();
		this.star.data.set('health', 1);
		
		// Reference: https://rexrainbow.github.io/phaser3-rex-notes/docs/site/gesture-overview/
		
		// Don't check swipe until pointer is down
		this.input.on('pointerdown', function(){
			this.checkSwipe = true;
		}, this);
		// Check swipe when pointer is up or up outside of game
		this.input.on('pointerup', this.handleSwipe, this);
		this.input.on('pointerupoutside', this.handleSwipe, this);
	}

	// App loop
	update(time, delta) {
		//this.scene.start('PreloadScene');

		var pointer = this.input.activePointer;
		var thisScope = this;

		text.setText([
			'x: ' + pointer.x,
			'y: ' + pointer.y,
			'mid x: ' + pointer.midPoint.x,
			'mid y: ' + pointer.midPoint.y,
			'velocity x: ' + Math.round(pointer.velocity.x),
			'velocity y: ' + pointer.velocity.y,
			'movementX: ' + pointer.movementX,
			'movementY: ' + pointer.movementY,
			'duration: ' + pointer.getDuration(),
			'angle: ' + pointer.getAngle(),
			'distance: ' + pointer.getDistance()
		]);
	}
	
	handleSwipe(pointer)
	{
		var gestureData = this.canMove ? window.REDLOVE.p.checkGestures(pointer) : null;
		console.log(gestureData);

		if (gestureData && gestureData.isSwipe)
		{
			// Clear mask
			this.star.setMask();

			var thisScope = this;
			var sliceableObj = this.star;
			if (sliceableObj.data.get('dead'))
			{
				return;
			}

			// Check for swipe intersections
			var intersectPoints = window.REDLOVE.p.checkSwipeIntersect(gestureData, sliceableObj);
			if (intersectPoints)
			{
				console.log('intersection');
				
				// Update hits
				var hits = sliceableObj.data.get('hits') || 0;
				hits++;
				sliceableObj.data.set('hits', hits);

				// Make an associative array of intersected sides
				for (var point of intersectPoints)
				{
					var intersectSide = window.REDLOVE.p.checkIntersectionSide(sliceableObj, point);
					point.intersectSide = intersectSide;
				}

				// If intersected one side, it's a hit and not a slice
				if (intersectPoints.length < 2)
				{
					// Impact the object with the hit
					thisScope.addHitTween(sliceableObj);
					
					//var originPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
					var originPoint = new Phaser.Geom.Point(sliceableObj.x, sliceableObj.y);
					var targetPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
					var pointerDownPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
					var pointerUpPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
					var pointerAxisInfo = window.REDLOVE.p.getAxisDirection(pointerDownPoint, pointerUpPoint);
					var axisInfo = window.REDLOVE.p.getAxisDirection(originPoint, pointerUpPoint);

					var rotationDirection = window.REDLOVE.p.orientationTest(originPoint, pointerDownPoint, pointerUpPoint);
					var angularDirection = rotationDirection;

					var angularVelocity = 50;
					console.log(angularVelocity);

					console.log(pointerAxisInfo.four);
					console.log(pointerAxisInfo.eight);
					console.log(pointerAxisInfo.four.raw == pointerAxisInfo.eight.raw);
					console.log(axisInfo.four);
					console.log(axisInfo.eight);
					
					// If a straight on hit in a cardinal direction (up, down, left, or right), no rotation
					// TODO: Check axis to make sure up/down and left/right and not an up/left, etc. hit that should involve rotation
					if (pointerAxisInfo.four.raw == pointerAxisInfo.eight.raw && axisInfo.four.raw == axisInfo.eight.raw)
					{
						console.log('Not a straight on hit');
					}

					// If a straight on hit in a cardinal direction (up, down, left, or right), no rotation
					if (pointerAxisInfo.four.raw != pointerAxisInfo.eight.raw || axisInfo.four.raw != axisInfo.eight.raw)
					{
						console.log('Not a straight on hit');
					}
				}
				else if (intersectPoints.length == 2)
				{
					var health = sliceableObj.data.get('health') || 1;
					health--;
					sliceableObj.data.set('health', health);

					// Check health
					if (health > 0)
					{
						thisScope.addHitTween(sliceableObj);
					}
					else
					{
						// Stop any existing tweens
						thisScope.stopHitTween(sliceableObj);
						
						// Set to dead
						sliceableObj.data.set('dead', true);

						// Set it to a dead state
						sliceableObj.setTint(Phaser.Display.Color.GetColor(255, 0, 0));
						// Stop physics
					}
				}
			}

        }
	}
	
	stopHitTween(sliceableObj)
	{
		// Stop any existing tweens
		var hitTween = sliceableObj.data.get('hitTween');
		if (hitTween)
		{
			hitTween.stop();
		}
	}

	addHitTween(sliceableObj)
	{
		// Stop any existing tweens
		this.stopHitTween(sliceableObj);

		// Create tween
		var hitTween = this.tweens.addCounter({
			from: 0,
			to: 255,
			duration: 500,
			onUpdate: function (tween)
			{
				var value = Math.floor(tween.getValue());
				sliceableObj.setTint(Phaser.Display.Color.GetColor(255, value, value));
			},
			onComplete: function (tween)
			{
				sliceableObj.setTint();
			}
		});

		sliceableObj.data.set('hitTween', hitTween);
	}


}


var gameOptions = {
    tweenSpeed: 50,
    swipeMaxTime: 1000,
    swipeMinDistance: 20,
    swipeMinNormal: 0.85,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 2
			},
            debug: true,
            debugShowBody: true,
            debugShowStaticBody: true,
            debugShowVelocity: true,
            debugVelocityColor: 0xffff00,
            debugBodyColor: 0x0000ff,
            debugStaticBodyColor: 0xffffff
        }
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

