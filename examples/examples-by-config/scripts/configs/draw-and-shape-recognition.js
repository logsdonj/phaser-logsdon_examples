
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}


class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');

		// Use the center of the screen as an anchor point
		this.anchorPoint = new Phaser.Geom.Point(this.game.scale.width / 2, this.game.scale.height / 2);

		// Add text for display
		text = this.add.text(10, 10, 'Move the mouse', { font: '16px Courier', fill: '#00ff00' });

		var textStyle = {
			fontSize: '16px',
			fontFamily: 'Courier, Verdana, "Times New Roman", Tahoma, serif',
			color: '#ffffff',
			backgroundColor: '#009900',
			shadow: {
				color: '#000000',
				fill: true,
				offsetX: 2,
				offsetY: 2,
				blur: 8
			},
			wordWrap: {
				width: 300,
				useAdvancedWrap: true
			}
		};
		var textConfig = {
			x: 0,
			y: 0,
			padding: {
				x: 32,
				y: 32,
			},
			style: textStyle,
			align: 'right',
			origin: { x: 0.5, y: 0.5 },
			text: 'Draw a shape!',
			testString: ',|"\'Xy|'
		};
		this.shapeText = this.make.text(textConfig)
		.setOrigin(1, 1)
		.setPosition();
		this.shapeText.setPosition(this.game.scale.width, this.game.scale.height);
		console.log(this.shapeText.getTextMetrics());
		console.log(this.shapeText.toJSON());

		// Create drawing polygon
		// https://rexrainbow.github.io/phaser3-rex-notes/docs/site/geom-polygon/#get-properties
		// https://photonstorm.github.io/phaser3-docs/Phaser.Geom.Polygon.html
		// https://photonstorm.github.io/phaser3-docs/Phaser.Geom.Rectangle.html
		this.polygonPoints = [];
		this.polygon = new Phaser.Geom.Polygon();
		// DEBUG: Draw polygon
		this.polygonGraphics = this.polygonGraphics || this.add.graphics();

		// Test axis directional pad
		this.input.on('pointerdown', function(pointer)
		{
			// Reset points and graphics
			this.polygonPoints = [];
			// DEBUG: Draw polygon
			this.polygonGraphics.clear();
		}, this)
		.on('pointermove', function(pointer)
		{
			if (pointer.isDown)
			{
				if (this.polygonPoints.length > 0)
				{
					// Do not draw if the movement tolerance is below a certain length
					var movementLengthTolerance = 3;

					if (
						(Math.abs(this.polygonPoints[this.polygonPoints.length - 1].x - pointer.x) < movementLengthTolerance) &&
						(Math.abs(this.polygonPoints[this.polygonPoints.length - 1].y - pointer.y) < movementLengthTolerance)
					)
					{
						return;
					}
				}
				// Add point
				this.polygonPoints.push(new Phaser.Geom.Point(pointer.x, pointer.y));
				this.polygon.setTo(this.polygonPoints);
				//Phaser.Geom.Polygon.Smooth(this.polygon);

				// DEBUG: Draw polygon
				this.polygonGraphics.clear();
				this.polygonGraphics.lineStyle(2, 0xff00ff, 0.8);
				this.polygonGraphics.strokePoints(this.polygon.points, false);
				this.polygonGraphics.fillStyle(0xff0000, 0.8);
				this.polygonGraphics.fillPoints(this.polygon.points, false);

				// Check area
				var rectangle = Phaser.Geom.Polygon.GetAABB(this.polygon);//Phaser.Geom.Rectangle.FromPoints(this.polygon.points);
				var rectangleCenterPoint = Phaser.Geom.Rectangle.GetCenter(rectangle);
				var drawnClockwise = (this.polygon.area >= 0);
				var polygonArea = Math.abs(this.polygon.area);
				var rectangleArea = Phaser.Geom.Rectangle.Area(rectangle);
				// If no width or height, it's a line
				var isLine = (rectangle.width + rectangle.height == 0);
				// Square == 1, Wide > 1, Tall < 1
				var sizeRatio = (! isLine) ? rectangle.width / rectangle.height : 0;
				// Square or rectangle == 1
				var areaCoverage = (! isLine) ? polygonArea / rectangleArea : 0;
				console.log('Coverage: ' + areaCoverage + '; Direction: ' + drawnClockwise + '; sizeRatio: ' + sizeRatio);

				// Check points toward the corners intersecting with the shape
				var cornerPointPrecision = {};
				var corners = ['tl', 'tr', 'br', 'bl'];
				var precisions = {
					half: 2,
					fourth: 4,
					eighth: 8,
					sixteenth: 16,
					thirtysecond: 32
				};

				// Align the testing shapes with the drawing shapes
				var offsetX = rectangle.x;
				var offsetY = rectangle.y;

				// DEBUG: Corner graphics
				this.cornerGraphics = this.cornerGraphics || this.add.graphics();
				this.cornerGraphics.clear();

				for (var corner of corners)
				{
					cornerPointPrecision[corner] = {};

					for (var precision in precisions)
					{
						if (! precisions.hasOwnProperty(precision)) continue;

						cornerPointPrecision[corner][precision] = {};

						var x1 = 0;
						var x2 = 0;
						var y1 = 0;
						var y2 = 0;
						
						// If on the right, bring point in from width
						if (corner.indexOf('r') >= 0)
						{
							x1 = rectangle.width - (rectangle.width / precisions[precision]);
							x2 = rectangle.width;
							
							// If on the top, move point out from the origin
							if (corner.indexOf('t') >= 0)
							{
								y1 = 0;
								y2 = 0 + (rectangle.height / precisions[precision]);
							}
							
							// If on the bottom, bring point in from height
							if (corner.indexOf('b') >= 0)
							{
								y1 = rectangle.height;
								y2 = rectangle.height - (rectangle.height / precisions[precision]);
							}
						}
						
						// If on the left, move point out from the origin
						if (corner.indexOf('l') >= 0)
						{
							x1 = 0;
							x2 = 0 + (rectangle.width / precisions[precision]);
							
							// If on the top, move point out from the origin
							if (corner.indexOf('t') >= 0)
							{

								y1 = 0 + (rectangle.height / precisions[precision]);
								y2 = 0;
							}
							
							// If on the bottom, bring point in from height
							if (corner.indexOf('b') >= 0)
							{
								y1 = rectangle.height - (rectangle.height / precisions[precision]);
								y2 = rectangle.height;
							}
						}

						// Check of the corner line midpoint is in the shape
						var cornerLine = new Phaser.Geom.Line(x1 + offsetX, y1 + offsetY, x2 + offsetX, y2 + offsetY);
						// https://photonstorm.github.io/phaser3-docs/Phaser.Geom.Line.html
						var cornerPoint = Phaser.Geom.Line.GetMidPoint(cornerLine);
						var contains = Phaser.Geom.Polygon.ContainsPoint(this.polygon, cornerPoint);
						cornerPointPrecision[corner][precision] = contains;

						if (typeof(cornerPointPrecision[precision + 'Hits']) === 'undefined')
						{
							cornerPointPrecision[precision + 'Hits'] = 0;
						}

						cornerPointPrecision[precision + 'Hits'] += (contains) ? 1 : 0;
						
						// DEBUG: Corner graphics
						this.cornerGraphics.fillStyle(0x00ff00, 0.8);
						this.cornerGraphics.fillPointShape(cornerPoint, 4);
						
						this.cornerGraphics = this.cornerGraphics || this.add.graphics();
						this.cornerGraphics.lineStyle(1, 0x00ff00, 0.5);
						this.cornerGraphics.strokeLineShape(cornerLine);
					}
				}

				// DEBUG: Corner graphics
				// Draw rectangle cross lines
				this.cornerGraphics = this.cornerGraphics || this.add.graphics();
				var xLine = new Phaser.Geom.Line(rectangle.x, rectangle.y, rectangle.x + rectangle.width, rectangle.y + rectangle.height);
				this.cornerGraphics.lineStyle(1, 0x00ff00, 0.2);
				this.cornerGraphics.strokeLineShape(xLine);
				xLine = new Phaser.Geom.Line(rectangle.x + rectangle.width, rectangle.y, rectangle.x, rectangle.y + rectangle.height);
				this.cornerGraphics.strokeLineShape(xLine);
				
				// Create center point for shape
				this.cornerGraphics.fillStyle(0x2266aa, 0.5);
				var centroid = Phaser.Geom.Point.GetCentroid(this.polygon.points);
				this.cornerGraphics.fillPointShape(centroid, 10);

				var rotationDirection = this.orientationTest(rectangleCenterPoint, this.polygonPoints[this.polygonPoints.length - 2], this.polygonPoints[this.polygonPoints.length - 1]);//isLeft

				//console.log(rotationDirection);
				console.log(cornerPointPrecision);

				var shape = '';
				// A square or rectangle has points near the corners
				// A square has a width to height ratio near 1
				if (areaCoverage >= 0.70 && cornerPointPrecision.halfHits >= 3 && cornerPointPrecision.fourthHits >= 2)
				{
					shape = 'rectangle';

					if (sizeRatio >= 0.75 && sizeRatio <= 1.25)
					{
						shape = 'square';
					}
				}
				// A circle has a square-like ratio without points in/near the corners
				if (areaCoverage < 0.85 && cornerPointPrecision.halfHits >= 3 && cornerPointPrecision.eighthHits == 0)
				{
					shape = 'circle';
				}
				// A diamond has square-like ratio and 0.5 coverage
				if (
					areaCoverage >= 0.35 && areaCoverage <= 0.60 && 
					cornerPointPrecision.halfHits >= 1 && 
					cornerPointPrecision.fourthHits == 0
				)
				{
					if (sizeRatio >= 0.35 && sizeRatio <= 1.60)
					{
						shape = 'diamond';
					}
				}
				// A triangle has points at/near 2 corners and near 0.5 coverage
				// A triangle has square-like ratio and 0.5 coverage
				if (
					(areaCoverage >= 0.35 && areaCoverage <= 0.60) && 
					(cornerPointPrecision.halfHits >= 1 && cornerPointPrecision.halfHits <= 3) && 
					(cornerPointPrecision.fourthHits == 1 || cornerPointPrecision.fourthHits == 2) && 
					(cornerPointPrecision.eighthHits == 1 || cornerPointPrecision.eighthHits == 2)
				)
				{
					if (sizeRatio >= 0.35 && sizeRatio <= 1.60)
					{
						shape = 'triangle';
					}
				}
				// A line has points near corners and a low coverage or no height or width
				if (isLine || areaCoverage < 0.10 || sizeRatio < 0.2 || sizeRatio > 5)
				{
					shape = 'line';
				}
				//console.log('Shape: ' + shape);
				this.shapeText.text = shape;
				
				// To check custom shapes matching, loop over the drawn points overlapping/contained in the custom shape (a stroked outline or area check would be more challenging) and the percentage
			}
		}, this)
		.on('pointerup', function(pointer)
		{
		}, this);
		
	}

	orientationTest(point1, point2, point3)
	{
		if (typeof(point1) === 'undefined' || typeof(point2) === 'undefined' || typeof(point3) === 'undefined')
		{
			return 0;
		}

		// Orientation test
		// https://www.geeksforgeeks.org/orientation-3-ordered-points/
		// https://math.stackexchange.com/questions/128061/check-if-point-is-on-or-below-line-when-either-x-or-y-0
		// http://www.cs.tufts.edu/comp/163/OrientationTests.pdf

		// The signed area from the points tells us the area of the triangle, and the sign gives the orientation (left-turning or right-turning)
		var val = ((point2.y - point1.y) * (point3.x - point2.x)) - ((point2.x - point1.x) * (point3.y - point2.y));
		
		// clock or counterclock wise
		return (val > 0) ? -1: 1;

		// Colinear
		if (val == 0) return 0;
	}

	isLeft(vectorA, vectorB, vectorC)
	{
		if (typeof(vectorA) === 'undefined' || typeof(vectorB) === 'undefined' || typeof(vectorC) === 'undefined')
		{
			return 0;
		}
		return ((vectorB.x - vectorA.x) * (vectorC.y - vectorA.y) - (vectorB.y - vectorA.y) * (vectorC.x - vectorA.x)) > 0;
	}

	createSpeechBubble (x, y, width, height, quote)
	{
		var bubbleWidth = width;
		var bubbleHeight = height;
		var bubblePadding = 10;
		var arrowHeight = bubbleHeight / 4;
	
		var bubble = this.add.graphics({ x: x, y: y });
	
		//  Bubble shadow
		bubble.fillStyle(0x222222, 0.5);
		bubble.fillRoundedRect(6, 6, bubbleWidth, bubbleHeight, 16);
	
		//  Bubble color
		bubble.fillStyle(0xffffff, 1);
	
		//  Bubble outline line style
		bubble.lineStyle(4, 0x565656, 1);
	
		//  Bubble shape and outline
		bubble.strokeRoundedRect(0, 0, bubbleWidth, bubbleHeight, 16);
		bubble.fillRoundedRect(0, 0, bubbleWidth, bubbleHeight, 16);
	
		//  Calculate arrow coordinates
		var point1X = Math.floor(bubbleWidth / 7);
		var point1Y = bubbleHeight;
		var point2X = Math.floor((bubbleWidth / 7) * 2);
		var point2Y = bubbleHeight;
		var point3X = Math.floor(bubbleWidth / 7);
		var point3Y = Math.floor(bubbleHeight + arrowHeight);
	
		//  Bubble arrow shadow
		bubble.lineStyle(4, 0x222222, 0.5);
		bubble.lineBetween(point2X - 1, point2Y + 6, point3X + 2, point3Y);
	
		//  Bubble arrow fill
		bubble.fillTriangle(point1X, point1Y, point2X, point2Y, point3X, point3Y);
		bubble.lineStyle(2, 0x565656, 1);
		bubble.lineBetween(point2X, point2Y, point3X, point3Y);
		bubble.lineBetween(point1X, point1Y, point3X, point3Y);
	
		var content = this.add.text(0, 0, quote, { fontFamily: 'Arial', fontSize: 20, color: '#000000', align: 'center', wordWrap: { width: bubbleWidth - (bubblePadding * 2) } });
	
		var b = content.getBounds();
	
		content.setPosition(bubble.x + (bubbleWidth / 2) - (b.width / 2), bubble.y + (bubbleHeight / 2) - (b.height / 2));
	}

	// App loop
	update(time, delta) {
		var pointer = this.input.activePointer;

		text.setText([
			'x: ' + pointer.x,
			'y: ' + pointer.y,
			'mid x: ' + pointer.midPoint.x,
			'mid y: ' + pointer.midPoint.y,
			'velocity x: ' + Math.round(pointer.velocity.x),
			'velocity y: ' + pointer.velocity.y,
			'movementX: ' + pointer.movementX,
			'movementY: ' + pointer.movementY,
			'duration: ' + pointer.getDuration(),
			'angle: ' + pointer.getAngle(),
			'distance: ' + pointer.getDistance()
		]);
	}

}


var gameOptions = {
    tweenSpeed: 50,
    swipeMaxTime: 1000,
    swipeMinDistance: 20,
    swipeMinNormal: 0.85,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			debug: true,
			gravity: {
				y: 200
			}
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

