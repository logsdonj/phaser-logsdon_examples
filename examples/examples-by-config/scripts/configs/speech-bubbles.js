// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	// Creating DOM elements requires special configuration
	// https://photonstorm.github.io/phaser3-docs/Phaser.GameObjects.DOMElement.html
	parent: 'container',
    dom: {
        createContainer: true
    },
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 0
			},
            debug: true,
            debugShowBody: true,
            debugShowStaticBody: true,
            debugShowVelocity: true,
            debugVelocityColor: 0xffff00,
            debugBodyColor: 0x0000ff,
            debugStaticBodyColor: 0xffffff
        }
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
        
				this.testText = this.add.text(
					0, 
					0, 
					'BootScene', 
					{ 
					backgroundColor: 'rgba(0,0,0,0.8)',
					font: '12px Arial, sans-serif', 
					fill: 'rgb(255,255,255)',
					stroke: 'rgb(47,92,31)',
					strokeThickness: 0,
					shadow: {
						offsetX: 4,
						offsetY: 4,
						color: 'rgba(0,0,0,1)',
						blur: 0,
						stroke: true,
						fill: true
					},
					padding: { // Adjust to work with shadown offset and blur to avoid clipping
						x: 10,
						y: 10
					},
					align: 'left'
					}
				)
				.setOrigin(0, 0)
				//.setFixedSize(this.game.scale.width, this.game.scale.height)
				.setDepth(10)
				.setInteractive({ cursor: 'pointer' })
				.once('pointerdown', function()
				{
				}, this);
		
				var bubble1 = createSpeechBubble(this, 100, 200, 220, 80, "Well hey there!\nLet's go!\nLet's go!").setVisible(true);
		
				this.game.scale.on('resize', function(gameSize, baseSize, displaySize, previousWidth, previousHeight) {
					console.log('game scale resized');
					bubble1.x = this.game.scale.width / 2;
				});
		
				var sceneRef = this;
				console.log(sceneRef);
				
				window.addEventListener('resize', function(event) {
					console.log('resize');
					sceneRef.testText.text = sceneRef.game.scale.width + ' x ' + sceneRef.game.scale.height + '\n' + sceneRef.testText.text;
				}, true);
			},
			// App loop
			update: function(time, delta) {
			}
		}
	]
};

function createSpeechBubble (scene, x, y, width, height, quote)
{
    var bubbleWidth = width;
    var bubbleHeight = height;
    var bubblePadding = 10;
    var arrowHeight = bubbleHeight / 3;

    var bubble = scene.add.graphics({ x: x, y: y });

    //  Bubble shadow
    bubble.fillStyle(0x222222, 0.5);
    bubble.fillRoundedRect(6, 6, bubbleWidth, bubbleHeight, 16);

    //  Bubble color
    bubble.fillStyle(0xffffff, 1);

    //  Bubble outline line style
    bubble.lineStyle(4, 0x565656, 1);

    //  Bubble shape and outline
    bubble.strokeRoundedRect(0, 0, bubbleWidth, bubbleHeight, 16);
    bubble.fillRoundedRect(0, 0, bubbleWidth, bubbleHeight, 16);

    //  Calculate arrow coordinates
    var point1X = Math.floor(bubbleWidth / 4);
    var point1Y = bubbleHeight;
    var point2X = Math.floor((bubbleWidth / 4) * 1.4);
    var point2Y = bubbleHeight;
    var point3X = Math.floor(bubbleWidth / 4);
    var point3Y = Math.floor(bubbleHeight + arrowHeight);

    //  Bubble arrow shadow
    bubble.lineStyle(4, 0x222222, 0.5);
    bubble.lineBetween(point2X - 1, point2Y + 6, point3X + 2, point3Y);

    //  Bubble arrow fill
    bubble.fillTriangle(point1X, point1Y, point2X, point2Y, point3X, point3Y);
    bubble.lineStyle(2, 0x565656, 1);
    bubble.lineBetween(point2X, point2Y, point3X, point3Y);
    bubble.lineBetween(point1X, point1Y, point3X, point3Y);

    var content = scene.add.text(0, 0, quote, { fontFamily: 'Arial', fontSize: 20, color: '#000000', align: 'center', wordWrap: { width: bubbleWidth - (bubblePadding * 2) } });

    var b = content.getBounds();

    content.setPosition(bubble.x + (bubbleWidth / 2) - (b.width / 2), bubble.y + (bubbleHeight / 2) - (b.height / 2));

    var container = scene.add.container();

    container.add([ bubble, content ]);

    return container;
}
