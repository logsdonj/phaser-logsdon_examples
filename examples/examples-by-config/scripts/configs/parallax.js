const parallaxLayers = {
	desert1e: [
		{
			key: 'layer0',
			file: 'assets/parallax/desert-1_extended/layer-0.jpg',
			moveFactor: 0.1,
			fixedToCamera: true
		},
		{
			key: 'layer1',
			file: 'assets/parallax/desert-1_extended/layer-1.png',
			moveFactor: 0.3
		},
		{
			key: 'layer2',
			file: 'assets/parallax/desert-1_extended/layer-2.png',
			moveFactor: 0.6
		},
		{
			key: 'layer3',
			file: 'assets/parallax/desert-1_extended/layer-3.png',
			moveFactor: 0.9
		}
	]
};

// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
    physics: {
		// https://github.com/photonstorm/phaser/issues/3570
        default: 'arcade',
        arcade: {
            debug: true,
            gravity: { y: 200 }
        }
    },
	scene: [
		// Create a scene as an object
		{

			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
				
				// Load images
				//this.load.image('layer0', 'assets/parallax/desert-1_extended/layer-0.jpg');
				// https://hacks.mozilla.org/2015/04/es6-in-depth-iterators-and-the-for-of-loop/
				for ( let layerSetKey of Object.keys(parallaxLayers) )
				{
					for ( let layer of parallaxLayers[layerSetKey] )
					{
						this.load.image(layer.key, layer.file);
					}
				}
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				// https://ansimuz.itch.io/parallax-scrolling-in-phaser
				// https://www.youtube.com/watch?v=GwGzFczdpkg
				// https://docs.idew.org/video-game/project-references/phaser-coding/tilesprite-scrolling
				// https://www.joshmorony.com/how-to-create-a-parallax-background-in-phaser/
				// https://www.freepik.com/free-photos-vectors/parallax-background
				// https://www.youtube.com/watch?v=7UlNsN0RyEE

				// https://phaser.io/news/2018/04/mobile-games-tutorial
				// https://gamedevacademy.org/creating-mobile-games-with-phaser-3-and-cordova/?a=13
				// https://github.com/orgs/ourcade/repositories

				// https://phasergames.com/how-to-flip-a-sprite-in-phaser-3/

				var scene = this;

				console.log(this.textures.get('layer0').getSourceImage().width);

				for ( let layer of parallaxLayers.desert1e )
				{
					const layerKey = layer.key;
					const srcImage = this.textures.get(layerKey).getSourceImage();
					const imageWidth = srcImage.width;
					const imageHeight = srcImage.height;
					
					// Fill the screen with the tile sprite
					this[layerKey] = this.add.tileSprite(
						0,
						0,
						this.scale.width,
						this.scale.height,
						layerKey
					);
					this[layerKey].setOrigin(0, 0);
					this[layerKey].setScrollFactor(0);
					// Scale the tile sprite image to fil
					const tileScaleFitX = this[layerKey].width / imageWidth;
					const tileScaleFitY = this[layerKey].height / imageHeight;
					this[layerKey].setTileScale(tileScaleFitX, tileScaleFitY);

					if ( Object.hasOwn(layer, 'fixedToCamera') )
					{
						this[layerKey].fixedToCamera = true;
					}
				}

				// Make something active to be clicked and active for slingshot
				this.mySprite = new DebugSprite(this, {width: 70, height: 70})
				.setInteractive({cursor: 'pointer'})
				.setDataEnabled();

				this.physics.add.existing(this.mySprite);
				// https://photonstorm.github.io/phaser3-docs/Phaser.Physics.Arcade.Sprite.html
				this.mySprite.body
				//.setCollideWorldBounds(true)
				.setAllowRotation(true)
				.setEnable(true)
				.setBounce(0.5, 0.5)
				.setGravity(0, 0).setAllowGravity(false)
				.setMass(2)
				.setDrag(0.2).setDamping(true)
				.setMaxVelocity(300);

				// Camera follow
				this.myCam = this.cameras.main;
				this.cameras.main.startFollow(this.mySprite);




				this.direction = {up: false, down: false, left: false, right: false};
				this.numMovementKeysPressed = 0;
				this.movementKeysDown = [];
				this.handleControls = function(event) {
					// Handle tracking key presses

					var lastIndexRemoved = false;

					// If key down
					if ( event.isDown ) {
						// Add to keys down
						this.movementKeysDown.push(event.keyCode);
					}
					// Else if key up
					else if ( event.isUp ) {
						// Find and remove it from keys down
						var index = this.movementKeysDown.indexOf(event.keyCode);
						var lastIndexRemoved = (index == this.movementKeysDown.length - 1);
						if ( index > -1 )
						{
							this.movementKeysDown.splice(index, 1);
						}
					}

					this.noMovementKeysPressed = (this.movementKeysDown.length == 0);

					// Handle animations and directions

					// Handle up
					if ( event.keyCode == this.keys.up.keyCode ) {
						this.direction.up = event.isDown;

						if ( event.isDown ) {
							//this.mySprite.play({ key: 'walkUp', repeat: -1 });
						}
						else if ( event.isUp && this.noMovementKeysPressed ) {
							//this.mySprite.play({ key: 'idleUp' });
						}
					}

					// Handle down
					if ( event.keyCode == this.keys.down.keyCode ) {
						this.direction.down = event.isDown;

						if ( event.isDown ) {
							//this.mySprite.play({ key: 'walkDown', repeat: -1 });
						}
						else if ( event.isUp && this.noMovementKeysPressed ) {
							//this.mySprite.play({ key: 'idleDown', repeat: -1, repeatDelay: 2000, yoyo: true });
						}
					}

					// Handle left
					if ( event.keyCode == this.keys.left.keyCode ) {
						this.direction.left = event.isDown;

						if ( event.isDown ) {
							//this.mySprite.play({ key: 'walkLeft', repeat: -1 });
						}
						else if ( event.isUp && this.noMovementKeysPressed ) {
							//this.mySprite.play({ key: 'idleLeft', repeat: -1, repeatDelay: 2000, yoyo: true });
						}
					}

					// Handle right
					if ( event.keyCode == this.keys.right.keyCode ) {
						this.direction.right = event.isDown;

						if ( event.isDown ) {
							//this.mySprite.play({ key: 'walkRight', repeat: -1 });
						}
						else if ( event.isUp && this.noMovementKeysPressed ) {
							//this.mySprite.play({ key: 'idleRight', repeat: -1, repeatDelay: 2000, yoyo: true });
						}
					}

					// Normalize direction values, not allowing in opposite directions
					if ( this.direction.up && this.direction.down )
					{
						//this.direction.up = this.direction.down = false;
					}

					if ( this.direction.left && this.direction.right )
					{
						//this.direction.left = this.direction.right = false;
					}


					// If the index removed was the last item and there are more items
					if ( lastIndexRemoved && this.movementKeysDown.length > 0 )
					{
						// Send an event through with the last active movement key
						var lastMovementKeyPressed = this.movementKeysDown.pop();
						return this.handleControls({keyCode: lastMovementKeyPressed, isDown: true, isUp: false});
					}
				};

				// TODO: Work out better key handling, especially when multiple keys pressed
				// TODO: Work out joystick handling
				// TODO: Review more examples of chaining and other animation abilities
				//var keys = this.input.keyboard.addKeys('W,S,A,D');keys.W.isDown;keys.W.on('down');keys.W.destroy();scene.input.keyboard.on('keydown-' + 'W';Phaser.Input.Keyboard.KeyCodes.SPACE
				//https://photonstorm.github.io/phaser3-docs/Phaser.Input.Keyboard.KeyboardPlugin.html
				//https://docs.idew.org/video-game/project-references/phaser-coding/input
				this.keys = this.input.keyboard.addKeys({
					up: 'W',
					down: 'S',
					left: 'A',
					right: 'D'
				});
				this.keys.up.on('down', this.handleControls, this);
				this.keys.up.on('up', this.handleControls, this);
				this.keys.down.on('down', this.handleControls, this);
				this.keys.down.on('up', this.handleControls, this);
				this.keys.left.on('down', this.handleControls, this);
				this.keys.left.on('up', this.handleControls, this);
				this.keys.right.on('down', this.handleControls, this);
				this.keys.right.on('up', this.handleControls, this);

			},

			// App loop
			update: function(time, delta) {

				// Move parallax layers
				//this.layer1.tilePositionX += 10;
				for ( let layer of parallaxLayers.desert1e )
				{
					this[layer.key].tilePositionX = this.myCam.scrollX * ( Object.hasOwn(this[layer.key], 'moveFactor') ? this[layer.key].moveFactor : 1 );
					//this[layer.key].tilePositionY = this.myCam.scrollY * ( Object.hasOwn(this[layer.key], 'moveFactor') ? this[layer.key].moveFactor : 1 );
				}

				// Use vector normalization to normalize diagonal movement to be equal to horizontal and vertical movement
				var diagonalFactor = ((this.direction.up || this.direction.down) && (this.direction.left || this.direction.right)) ? 0.7071 : 1; // 1 / sqrt(2)

				// Move character
				if ( this.direction.up )
				{
					this.mySprite.y -= 5 * diagonalFactor;
				}

				if ( this.direction.down )
				{
					this.mySprite.y += 5 * diagonalFactor;
				}

				if ( this.direction.left )
				{
					this.mySprite.x -= 5 * diagonalFactor;
				}
				
				if ( this.direction.right )
				{
					this.mySprite.x += 5 * diagonalFactor;
				}
				
				var pointer = this.input.activePointer;
					
				// Tween the angle
				// https://phaser.discourse.group/t/smooth-player-rotation/8001/7
				// https://www.youtube.com/watch?v=3WloQupH_PQ
				// https://github.com/ourcade/phaser3-smart-rotation
				var targetAngle = Phaser.Math.Angle.Between(this.mySprite.x, this.mySprite.y, pointer.x, pointer.y);
				var currentAngle = this.mySprite.rotation;
				var angleDiff = targetAngle - currentAngle;
				// Wrap so closest angle is used
				var angleDiffWrapped = Phaser.Math.Angle.Wrap(angleDiff);
				var newAngle = currentAngle + angleDiffWrapped;
				var angularDrag = 0.1;
				this.mySprite.rotation += angleDiffWrapped * angularDrag;

			}
		}
	]
};


