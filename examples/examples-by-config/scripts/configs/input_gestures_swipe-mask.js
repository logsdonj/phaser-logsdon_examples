
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}


class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Load images
		this.load.image('star', '../assets/images/star.png');
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');
		
		this.star = this.add.image(this.game.scale.width / 2, this.game.scale.height / 2, 'star');

		// Add text for display
		text = this.add.text(10, 10, 'Move the mouse', { font: '16px Courier', fill: '#00ff00' });

		// On pointerdown, add a ball
		this.input.on('pointerdown', function (pointer) {
			console.log(this.game.loop.frame, 'down B');
			var ball = this.add.image(pointer.x, pointer.y, 'balls', Phaser.Math.Between(0, 5));
			ball.alpha = 0.5;
		}, this);

		// Reference: https://rexrainbow.github.io/phaser3-rex-notes/docs/site/gesture-overview/
		
		// On pointerup, check swipe
		this.input.on('pointerup', this.handleSwipe, this);

		// On pointerup, check swipe
		this.swipeLine = new Phaser.Geom.Line();
		this.swipeLineGraphics = this.add.graphics({ lineStyle: { width: 4, color: 0xaa00aa } });
	
		this.previousSwipeGraphics = this.add.graphics({x: 0, y: 0});
		this.previousSwipeGraphics.lineStyle(1, 0xff0000, 0.5);
		this.previousSwipeGraphics.fillStyle(0xff00ff, 0.5);

		this.enemyGraphics = this.add.graphics({x: 0, y: 0, lineStyle: { width: 4, color: 0x0000aa } });
		this.enemyRect = new Phaser.Geom.Rectangle(this.star.getTopLeft().x, this.star.getTopLeft().y, this.star.width, this.star.height);
		this.enemyGraphics.strokeRectShape(this.enemyRect);

		this.intersectedGraphics = this.add.graphics({x: 0, y: 0});
		this.intersectedGraphics.lineStyle(1, 0xffffff, 0.5);
		this.intersectedGraphics.fillStyle(0xffffff, 0.5);

		/*
		// Drag rectangle
		var graphics = this.add.graphics();

		var color = 0xffff00;
		var thickness = 2;
		var alpha = 1;
	
		//  Events
	
		var draw = false;
	
		this.input.on('pointerdown', function (pointer) {
			draw = true;
		}, this);
	
		this.input.on('pointerup', function () {
			draw = false;
		}, this);
	
		this.input.on('pointermove', function (pointer) {
			//if (pointer.isDown)
			if (draw)
			{
				graphics.clear();
				graphics.lineStyle(thickness, color, alpha);
				graphics.strokeRect(pointer.downX, pointer.downY, pointer.x - pointer.downX, pointer.y - pointer.downY);
			}
		}, this);
		*/

		// 1) Create graphics and then add to the physics
		this.myRect = this.add.graphics({x: 0, y: 0});
		this.myRect.lineStyle(1, 0xffff00, 0.5);
		this.myRect.fillStyle(0xff00ff, 0.5);
		this.myRect.strokeRect(0, 0, 100, 100);
		this.physics.add.existing(this.myRect);
		
		// Create a Circle
		var circle = new Phaser.Geom.Circle(this.game.scale.width / 2, this.game.scale.height / 2, 20);
		// And display our circle on the top
		this.circleGraphics = this.scene.scene.add.graphics(0, 0);//game.make.graphics();
		this.circleGraphics.beginPath();
		this.circleGraphics.lineStyle(1, 0x00ff00, 1);
		this.circleGraphics.fillStyle(0x00ff00, 0.5);
		this.circleGraphics.fillCircle(circle.x, circle.y, circle.diameter);
		this.circleGraphics.closePath();
		// Set events
		//this.circleGraphics.inputEnabled = true;
		//this.circleGraphics.input.useHandCursor = true;
		//this.circleGraphics.events.onInputDown.addOnce(this.start, this);
		
		/*
        this.circleGraphics.setInteractive();
        this.circleGraphics.on("pointerup", function(){
            if(!this.scale.isFullscreen){
                this.scale.startFullscreen();
            }
            else{
                this.scale.stopFullscreen();
            }
		}, this);
		
        this.circleGraphics.setInteractive();
        this.circleGraphics.on("pointerdown", function() {
            this.scene.start("PlayGame");
		}, this);
		*/
	}

	// App loop
	update(time, delta) {
		//this.scene.start('PreloadScene');

		var pointer = this.input.activePointer;

		text.setText([
			'x: ' + pointer.x,
			'y: ' + pointer.y,
			'mid x: ' + pointer.midPoint.x,
			'mid y: ' + pointer.midPoint.y,
			'velocity x: ' + Math.round(pointer.velocity.x),
			'velocity y: ' + pointer.velocity.y,
			'movementX: ' + pointer.movementX,
			'movementY: ' + pointer.movementY,
			'duration: ' + pointer.getDuration(),
			'angle: ' + pointer.getAngle(),
			'distance: ' + pointer.getDistance()
		]);
	}
	
	handleSwipe(pointer) {
        if (this.canMove) {
            var swipeTime = pointer.upTime - pointer.downTime;
            var fastEnough = swipeTime < gameOptions.swipeMaxTime;
            var swipe = new Phaser.Geom.Point(pointer.upX - pointer.downX, pointer.upY - pointer.downY);
            var swipeMagnitude = Phaser.Geom.Point.GetMagnitude(swipe);
			var longEnough = swipeMagnitude > gameOptions.swipeMinDistance;
			
			console.log('Movement time: ' + swipeTime + ' ms; fastEnough: ' + fastEnough);
			console.log('Distance: ' + swipe.x + ' px, '+ swipe.y + ' px; swipeMagnitude: ' + swipeMagnitude + '; longEnough' + longEnough);

			//var angle = Phaser.Geom.Line.Angle(line);
            var startPoint = new Phaser.Geom.Point(pointer.downX, pointer.downY);
            var stopPoint = new Phaser.Geom.Point(pointer.upX, pointer.upY);
			var angle = Phaser.Math.Angle.BetweenPoints(startPoint, stopPoint);
			angle = Phaser.Math.Angle.Normalize(Phaser.Math.Angle.Wrap(angle));
			console.log('angle: ' + angle + ' ' + Phaser.Math.RadToDeg(angle));

            if (longEnough && fastEnough) {
				// Normalize magnitude
                Phaser.Geom.Point.SetMagnitude(swipe, 1);
                if (swipe.x > gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(RIGHT);');
                }
                if (swipe.x < -gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(LEFT);');
                }
                if (swipe.y > gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(DOWN);');
                }
                if (swipe.y < -gameOptions.swipeMinNormal) {
                    console.log('this.makeMove(UP);');
				}
				
				// Draw swipe
				this.previousSwipeGraphics.strokeRect(pointer.downX, pointer.downY, pointer.upX - pointer.downX, pointer.upY - pointer.downY);

				this.previousSwipeGraphics.strokeLineShape(this.swipeLine);
				
				this.swipeLine = new Phaser.Geom.Line(pointer.downX, pointer.downY, pointer.upX, pointer.upY);
				this.swipeLineGraphics.clear();
				this.swipeLineGraphics.lineStyle(3, 0xffff00, 0.75);
				this.swipeLineGraphics.strokeLineShape(this.swipeLine);
				var swipeLineLength = Phaser.Geom.Line.Length(this.swipeLine);
				this.swipeLineGraphics.lineStyle(2, 0x00aa00, 1);
				this.swipeLineGraphics.strokeCircle(pointer.downX, pointer.downY, swipeLineLength);
				console.log('swipeLineLength: ' + swipeLineLength);

				// Arrow at the end of the swipe line
				this.triangleGraphics = this.add.graphics({ lineStyle: { width: 2, color: 0x00ff00 }, fillStyle: { color: 0xff0000 } });

				this.arrowTriangle = new Phaser.Geom.Triangle(0, 0, 10, 50, -10, 50);
				// graphics.strokeTriangleShape(triangle);
				this.triangleGraphics.fillTriangleShape(this.arrowTriangle);
				this.triangleGraphics.x = pointer.x;
				this.triangleGraphics.y = pointer.y;
				//this.triangleGraphics.angle = Phaser.Math.RadToDeg(angle);
				this.triangleGraphics.rotation = angle + (Math.PI / 2);

				// Check for swipe intersections
				if (Phaser.Geom.Intersects.LineToRectangle(this.swipeLine, this.enemyRect))
				{
					console.log('intersection');
					var intersectPoints = Phaser.Geom.Intersects.GetLineToRectangle(this.swipeLine, this.enemyRect);
					// Make an associative array of intersected sides
					for (var point of intersectPoints)
					{
						var intersectSide = this.checkIntersectionSide(this.enemyRect, point);
						point.intersectSide = intersectSide;
					}

					this.intersectedGraphics.clear();

					if (intersectPoints.length == 1)
					{
						console.log('intersection on 1 side');
					}
					else if (intersectPoints.length == 2)
					{
						console.log('intersection on 2 sides');

						console.log('Build polygons from intersection...');
						// Start collecting polygons
						var curPoly = 'A';
						var polyObjs = {'A': [], 'B': []};
						// When hit an intersection, start collecting for B and continue switching back and forth on intersects
						var rectPoints = this.enemyRect.getPoints(4);
						// Loop over rectangle points
						for (var rectPointIndex = 0; rectPointIndex < rectPoints.length; rectPointIndex++)
						{
							var rectPoint = rectPoints[rectPointIndex];
							var intersectingPoint = null;

							// Loop over intersecting points
							for (var intersectPointIndex = 0; intersectPointIndex < intersectPoints.length; intersectPointIndex++)
							{
								var intersectPoint = intersectPoints[intersectPointIndex];
								// Check for intersection on rectangle line
								if (intersectPoint.x == rectPoint.x || intersectPoint.y == rectPoint.y)
								{
									intersectingPoint = intersectPoint;
									// Remove the intersect point now that it has been handled
									intersectPoints.splice(intersectPointIndex, 1);
									break;
								}
							}
							
							if (intersectingPoint)
							{
								// Determine placement of the intersection before or after the rectangle point
								var rectBeforeIntersection = true;

								if (
									(rectPointIndex == 0 && intersectingPoint.intersectSide == 'left') || 
									(rectPointIndex == 1 && intersectingPoint.intersectSide == 'top') || 
									(rectPointIndex == 2 && intersectingPoint.intersectSide == 'right') || 
									(rectPointIndex == 3 && intersectingPoint.intersectSide == 'bottom')
								)
								{
									rectBeforeIntersection = false;
								}

								// Add the rectangle point to the polygon
								if (rectBeforeIntersection)
								{
									polyObjs[curPoly].push(rectPoint);
								}

								// Add the polygon, switch to the other polygon, and continue main iteration
								polyObjs[curPoly].push(intersectingPoint);
								curPoly = (curPoly == 'A') ? 'B' : 'A';
								polyObjs[curPoly].push(intersectingPoint);

								// Add the rectangle point to the polygon
								if (! rectBeforeIntersection)
								{
									polyObjs[curPoly].push(rectPoint);
								}
							}
							else
							{
								// Add the rectangle point to the polygon
								polyObjs[curPoly].push(rectPoint);
							}
						}

						this.intersectedGraphics.strokePoints(polyObjs.A, true);
						this.intersectedGraphics.fillPoints(polyObjs.A, true);
						
						this.intersectedGraphics.lineStyle(1, 0xffff00, 0.5);
						this.intersectedGraphics.fillStyle(0xffff00, 0.5);
						this.intersectedGraphics.strokePoints(polyObjs.B, true);
						this.intersectedGraphics.fillPoints(polyObjs.B, true);

						/*
						*/
						var maskShape = this.make.graphics();
						//  Create a hash shape Graphics object
						maskShape.fillStyle(0xffffff);
						//  You have to begin a path for a Geometry mask to work
						maskShape.beginPath();
						maskShape.fillRect(50, 0, 50, 300);
						maskShape.fillRect(175, 0, 50, 300);
						maskShape.fillRect(0, 75, 275, 50);
						maskShape.fillRect(0, 200, 275, 50);
						maskShape.fillPoints(polyObjs.A, true);
						var mask = maskShape.createGeometryMask();
						this.star.setMask(mask);

						var intersectPointsTemp = Phaser.Geom.Intersects.GetLineToRectangle(this.swipeLine, this.enemyRect);
						this.burstSplitLine = new Phaser.Geom.Line(intersectPointsTemp[0].x, intersectPointsTemp[0].y, intersectPointsTemp[1].x, intersectPointsTemp[1].y);
						var burstSplitLineLength = Phaser.Geom.Line.Length(this.burstSplitLine);
						var midPoint = Phaser.Geom.Line.GetMidPoint(this.burstSplitLine);
						this.burstTriangleGraphics = this.add.graphics({ lineStyle: { width: 2, color: 0x00ff00, alpha: 0.5 }, fillStyle: { color: 0xff0000, alpha: 0.5 } });

						this.burstTriangle = new Phaser.Geom.Triangle(0, 0, 10, 20, -10, 20);
						this.burstTriangle2 = new Phaser.Geom.Triangle(0, 0, 10, -20, -10, -20);
						// graphics.strokeTriangleShape(triangle);
						this.burstTriangleGraphics.fillTriangleShape(this.burstTriangle);
						this.burstTriangleGraphics.fillTriangleShape(this.burstTriangle2);
						this.burstTriangleGraphics.x = midPoint.x;
						this.burstTriangleGraphics.y = midPoint.y;
						//this.burstTriangleGraphics.angle = Phaser.Math.RadToDeg(angle);
						this.burstTriangleGraphics.rotation = angle + (Math.PI / 2);
						

						this.burstCircleGraphics = this.add.graphics({ lineStyle: { width: 2, color: 0x00ff00, alpha: 0.5 }, fillStyle: { color: 0xff0000, alpha: 0.5 } });
						this.burstCircleGraphics.lineStyle(2, 0x00aa00, 1);
						this.burstCircleGraphics.strokeCircle(midPoint.x, midPoint.y, burstSplitLineLength / 2);
					}
				}
			}
        }
	}
	
	checkIntersectionSide(rect, point) {
		var leftX = rect.left;
		var rightX = rect.right;
		var topY = rect.top;
		var bottomY = rect.bottom;
		
		// Top intersect
		if (point.y == topY)
		{
			return 'top';
		}
		// Right intersect
		else if (point.x == rightX)
		{
			return 'right';
		}
		// Bottom intersect
		else if (point.y == bottomY)
		{
			return 'bottom';
		}
		// Left intersect
		else if (point.x == leftX)
		{
			return 'left';
		}
	}

}


var gameOptions = {
    tweenSpeed: 50,
    swipeMaxTime: 1000,
    swipeMinDistance: 20,
    swipeMinNormal: 0.85,
    aspectRatio: 16/9,
    localStorageName: 'standalone_input'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 1200,
            height: 1600
        }
    },
	/*
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'container',
		width: width,
		height: width * gameOptions.aspectRatio
	},
	*/
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 200
			},
			debug: false
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

