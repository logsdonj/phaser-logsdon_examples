// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 200
			},
			debug: false
		}
	},
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
				
				// Load images
				this.load.image('star', 'assets/images/star.png');
				
				this.load.setBaseURL('http://labs.phaser.io');

				this.load.image('sky', 'assets/skies/space3.png');
				this.load.image('logo', 'assets/sprites/phaser3-logo.png');
				this.load.image('red', 'assets/particles/red.png');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
				// 1) Create sprite and then add to the physics system
				this.droppingStar = this.add.image(400, 300, 'star');
				this.physics.add.existing(this.droppingStar);
				
				this.add.image(400, 300, 'sky');

				var particles = this.add.particles('red');

				var emitter = particles.createEmitter({
					speed: 100,
					scale: { start: 1, end: 0 },
					blendMode: 'ADD'
				});

				var logo = this.physics.add.image(400, 100, 'logo');

				logo.setVelocity(100, 200);
				logo.setBounce(1, 1);
				logo.setCollideWorldBounds(true);

				emitter.startFollow(logo);
			},
			// App loop
			update: function(time, delta) {
				//this.scene.start('PreloadScene');
			}
		}
	]
};