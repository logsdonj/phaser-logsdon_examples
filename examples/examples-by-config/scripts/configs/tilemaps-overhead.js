//https://medium.com/@michaelwesthadley/modular-game-worlds-in-phaser-3-tilemaps-1-958fc7e6bbd6
//https://blog.ourcade.co/posts/2020/phaser-3-noob-guide-loading-tiled-tilemaps/
//https://www.youtube.com/playlist?list=PLumYWZ2t7CRtgjbZK0JMoXHjebeYmT85-
//https://stackabuse.com/phaser-3-and-tiled-building-a-platformer/
//https://phasergames.com/downloads/load-a-tilemap-using-json-in-phaser-3/
//https://phaser.io/examples/v3/category/tilemap
//https://photonstorm.github.io/phaser3-docs/Phaser.Tilemaps.Tilemap.html
//https://phaser.discourse.group/t/phaser-3-arcade-physics-tilemap-collision/6091

// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');

				// Load the tileset image used to create a tilemap
				this.load.image('tilesImage', 'assets/tilesets/overhead-tiles.png');
				// Load the JSON file exported from Tiled that has tilemap and associated tileset image data
				this.load.tilemapTiledJSON('tilemapJSON', 'assets/tilesets/overhead-map.json')
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				// When loading from an array, make sure to specify the tileWidth and tileHeight
				const tilemap = this.make.tilemap({ key: 'tilemapJSON', tileWidth: 64, tileHeight: 64 });
				// Create tileset using Tiled tileset name and Phaser tileset image key
				const tileset = tilemap.addTilesetImage('overheadTiles', 'tilesImage');
				// Create layers in stacked order using Tile layer names
				const groundLayer = tilemap.createStaticLayer('ground', tileset, 0, 0);
				const overGroundLayer = tilemap.createLayer('over-ground', tileset, 0, 0);

				// Set tiles for any collisions
				overGroundLayer.setCollisionBetween(12, 44);//setCollisionByProperty({ collides: true });
				const debugGraphics = this.add.graphics().setAlpha(0.75);
				overGroundLayer.renderDebug(debugGraphics, {
					tileColor: null, // Color of non-colliding tiles
					collidingTileColor: new Phaser.Display.Color(243, 134, 48, 255), // Color of colliding tiles
					faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Color of colliding face edges
				});

				// Move around the tilemap

				// Phaser supports multiple cameras, but you can access the default camera like this:
				const camera = this.cameras.main;

				// Constrain the camera so that it isn't allowed to move outside the width/height of tilemap
				camera.setBounds(0, 0, tilemap.widthInPixels, tilemap.heightInPixels);

				// Set up the arrows to control the camera
				const cursors = this.input.keyboard.createCursorKeys();
				controls = new Phaser.Cameras.Controls.FixedKeyControl({
					camera: camera,
					left: cursors.left,
					right: cursors.right,
					up: cursors.up,
					down: cursors.down,
					speed: 0.5
				});

				// Help text that has a "fixed" position on the screen
				const text = this.add
					.text(16, 16, "Arrow keys to scroll", {
					font: "18px monospace",
					fill: "#ffffff",
					padding: { x: 20, y: 10 },
					backgroundColor: "#000000"
				})
				.setScrollFactor(0);

				//  The miniCam, so can display the whole world at a zoom of 0.2
				this.minimap = this.cameras.add(this.game.scale.width - 100, 10, 100, 0).setZoom(0.2).setName('mini');
				this.minimap.setBackgroundColor(0x002244);
				this.minimap.ignore(text);
			},

			// App loop
			update: function(time, delta) {
				// Apply the controls to the camera each update tick of the game
				controls.update(delta);

				// And this camera is 400px wide, so -200
				//this.minimap.scrollX = Phaser.Math.Clamp(this.cameras.main.scrollX, 800, 2000);
				this.minimap.scrollX = this.cameras.main.scrollX;
				this.minimap.scrollY = this.cameras.main.scrollY;
			}
		}
	]
};
