// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				// Rectangle settings
				var rectSize = 50;
				var numRects = 5;
				
				// Red rectangles
				var redRects = createRectangles(this, numRects, rectSize, 0xff0000, {x: 0, y: rectSize}, true);
				window.REDLOVE.p.alignObject(redRects, this.game, 'r', 'inside', 0);
				window.REDLOVE.p.distributeSpace(redRects, this.game, 'space-evenly', 'down');

				// Green rectangles
				var greenRects = createRectangles(this, numRects, rectSize, 0x00ff00);
				window.REDLOVE.p.alignObject(greenRects, this.game, 'l', 'inside', 0);
				window.REDLOVE.p.distributeObjects(greenRects, 'down', 10);

				// Light yellow rectangles
				var lightYellowRects = createRectangles(this, numRects, rectSize, 0xffff99, '', true);
				window.REDLOVE.p.alignObject(lightYellowRects, this.game, 'c', 'inside');
				window.REDLOVE.p.distributeSpace(lightYellowRects, this.game, 'justify', 'down');

				// Blue rectangles
				var blueRects = createRectangles(this, numRects, rectSize, 0x0000ff, '', true);
				window.REDLOVE.p.alignObject(blueRects, this.game, 'b', 'inside', 0);
				window.REDLOVE.p.distributeSpace(blueRects, this.game.scale.width, 'space-evenly', 'right');

				// Purple rectangles
				var purpleRects = createRectangles(this, numRects, rectSize, 0xff00ff, {x: rectSize, y: 0}, true);
				window.REDLOVE.p.alignObject(purpleRects, this.game, 'mc', 'inside', rectSize);
				window.REDLOVE.p.distributeSpace(purpleRects, this.game.scale.width, 'justify', 'right');

				// Yellow rectangles
				var yellowRects = createRectangles(this, numRects, rectSize, 0xffff00, {x: 0, y: -rectSize});
				window.REDLOVE.p.alignObject(yellowRects, this.game, 'br', 'inside', 0);
				window.REDLOVE.p.distributeObjects(yellowRects, 'up', 0, {x: -rectSize - 10, y: -10});
				
				// White rectangles
				var whiteRects = createRectangles(this, numRects, rectSize, 0xffffff);
				//window.REDLOVE.p.alignObject(whiteRects, this.game, 'mr', 'inside', 0);
				window.REDLOVE.p.alignObject(whiteRects, this.game, {x: 3/4, y: 1/4});
				window.REDLOVE.p.distributeObjects(whiteRects, 'left', 0, {x: -50, y: -25});
			},

			// App loop
			update: function(time, delta) {
			}
		}
	]
};

function createRectangles(scene, numRects, rectSize, color, stagger, randomize)
{
	// Set defaults
	numRects = numRects || 5;
	rectSize = rectSize || 50;
	color = color || 0xff00ff;
	stagger = stagger || {x: 0, y: 0};

	var rectsArray = [];

	for ( var i = 0; i < numRects; i++ )
	{
		var percentCompleteIterations = i / (numRects - 1);
		var reversePercentCompleteIterations = 1 - percentCompleteIterations;
		var reducedAlpha = 0.8 * percentCompleteIterations;
		var reversedReducedAlpha = 0.8 * reversePercentCompleteIterations;

		var newRectSize = rectSize;
		if ( randomize )
		{
			newRectSize = rectSize + Phaser.Math.Between(0, 200);
		}

		var rectGraphics = scene.add.rectangle(0 + (stagger.x * i), 0 + (stagger.y * i), newRectSize, newRectSize, color, 1.0 - reducedAlpha)
		.setStrokeStyle(2, color, 1.0 - reversedReducedAlpha)
		.setOrigin(0, 0);
		/*
		var rectShape = new Phaser.Geom.Rectangle(0, 0, rectSize, rectSize);
		var rectGraphics = this.add.graphics({ x: 0, y: i * rectSize });
		rectGraphics.lineStyle(2, 0x00ff00, 1.0 - reversedReducedAlpha);
		rectGraphics.strokeRectShape(rectShape);
		rectGraphics.fillStyle(0x00ff00, 1.0 - reducedAlpha);
		rectGraphics.fillRectShape(rectShape);
		rectGraphics.width = rectSize;
		rectGraphics.height = rectSize;
		*/
		rectsArray.push(rectGraphics);
	}

	return rectsArray
}