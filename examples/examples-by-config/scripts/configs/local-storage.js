
class BootScene extends Phaser.Scene{
    constructor(){
        super('BootScene');
    }
    preload(){
    }
    create(){
        this.scene.start('MainScene');
    }
}


class MainScene extends Phaser.Scene{
    constructor(){
		super('MainScene');
		
		this.canMove = true;
	}
	
	// Prepare data
	init() {
		if (config.debug) console.log('init');
	}

	// Load assets
	preload() {
		if (config.debug) console.log('preload');
		
		// Get localStorage object
		this.storageObj = LocalStorageHelper.safeGet(gameOptions.localStorageName, {});
		console.log(this.storageObj);
	}

	// Add objects to app
	create() {
		if (config.debug) console.log('create');
		
		// Get localStorage object
		this.storageObj = LocalStorageHelper.safeGet(gameOptions.localStorageName, {});
		console.log(this.storageObj);

		// On pointerdown, add a ball
		this.input.on('pointerdown', function (pointer) {
			this.storageObj = {downFrame: this.game.loop.frame};
			LocalStorageHelper.safeSet(gameOptions.localStorageName, this.storageObj);
		}, this);
	}

	// App loop
	update(time, delta) {
		//this.scene.start('PreloadScene');
	}
	

}


class LocalStorageHelper {
    constructor() {
    }
    
	static safeGet(itemKey, fallback) {
		//fallback = typeof(fallback) !== 'undefined' ? fallback : undefined;
		// Catch localStorage and JSON parse errors
		try {
			return JSON.parse(localStorage.getItem(itemKey));
		} catch(error) {
			return fallback;
		}
	}

	static safeSet(itemKey, value) {
		// Catch localStorage memory or JSON errors
		try {
			localStorage.setItem(itemKey, JSON.stringify(value));
		} catch(error) {
			return false;
		}
	}
}

var gameOptions = {
    localStorageName: 'standalone_local-storage'
};
var gameData = {};

var text;

// Set game configuration
var config = {
	debug: true,
	type: Phaser.AUTO,
	fps: {
		target: 15
	},
	render: {
		pixelArt: false
	},
	width: 800,
	height: 600,
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
				
				// Get localStorage object
				this.storageObj = LocalStorageHelper.safeGet(gameOptions.localStorageName, {});
				console.log(this.storageObj);
				
				this.storageObj = {frame: this.game.loop.frame};
				LocalStorageHelper.safeSet(gameOptions.localStorageName, this.storageObj);
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');
			},
			// App loop
			update: function(time, delta) {
				this.scene.start('BootScene');
			}
		},
		// Use scene class
		BootScene,
		MainScene
	]
};

