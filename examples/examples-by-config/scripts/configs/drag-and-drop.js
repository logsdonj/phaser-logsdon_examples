// Set game configuration
var config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
    scale: {
        mode: Phaser.Scale.RESIZE,
		autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'container',
        width: '100%',
        height: '100%',
        min: {
			width: 300,
			height: 500
        },
        max: {
            width: 2400,
            height: 2000
        }
    },
	scene: [
		// Create a scene as an object
		{
			// Prepare data
			init: function() {
				if (config.debug) console.log('init');
			},
			// Load assets
			preload: function() {
				if (config.debug) console.log('preload');
			},
			// Add objects to app
			create: function() {
				if (config.debug) console.log('create');

				//https://phaser.io/examples/v3/search?search=drag
				//https://phaser.io/examples/v3/view/game-objects/container/draggable-container
				//https://phaser.io/examples/v3/search?search=drop
				//https://phaser.io/examples/v3/view/input/zones/drop-zone

				var rectSize = 50;

				var graphicsHitArea = new Phaser.Geom.Rectangle(0, 0, rectSize, rectSize);
				var graphicsInteractiveConfig = {
					hitArea: graphicsHitArea, // shape
					hitAreaCallback: Phaser.Geom.Rectangle.Contains,
					useHandCursor: true
				};

				var rectGraphics = this.add.rectangle(0, 0, rectSize, rectSize, 0xff0000, 0.6)
				.setStrokeStyle(2, 0xff0000, 0.8)
				.setOrigin(0, 0)
				.setInteractive(graphicsInteractiveConfig);
				
				this.input.setDraggable(rectGraphics);

				//  A drop zone
				var zone = this.add.zone(500, 300, 300, 300).setRectangleDropZone(300, 300);

				//  Just a visual display of the drop zone
				var graphics = this.add.graphics();
				graphics.lineStyle(2, 0xffff00);
				graphics.strokeRect(zone.x - zone.input.hitArea.width / 2, zone.y - zone.input.hitArea.height / 2, zone.input.hitArea.width, zone.input.hitArea.height);
			
				this.input.on('dragstart', function (pointer, gameObject) {
			
					this.children.bringToTop(gameObject);
			
				}, this);
			
				this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
			
					gameObject.x = dragX;
					gameObject.y = dragY;
			
				});
			
				this.input.on('dragenter', function (pointer, gameObject, dropZone) {
			
					graphics.clear();
					graphics.lineStyle(2, 0x00ffff);
					graphics.strokeRect(zone.x - zone.input.hitArea.width / 2, zone.y - zone.input.hitArea.height / 2, zone.input.hitArea.width, zone.input.hitArea.height);
			
				});
			
				this.input.on('dragleave', function (pointer, gameObject, dropZone) {
			
					graphics.clear();
					graphics.lineStyle(2, 0xffff00);
					graphics.strokeRect(zone.x - zone.input.hitArea.width / 2, zone.y - zone.input.hitArea.height / 2, zone.input.hitArea.width, zone.input.hitArea.height);
			
				});
			
				this.input.on('drop', function (pointer, gameObject, dropZone) {
			
					gameObject.x = dropZone.x;
					gameObject.y = dropZone.y;
			
					gameObject.input.enabled = false;
			
				});
			},

			// App loop
			update: function(time, delta) {
			}
		}
	]
};
