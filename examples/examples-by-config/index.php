<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Game</title>
	
	<meta http-equiv="X-UA-Compatible" content="chrome=1, IE=9">
	<meta http-equiv="cleartype" content="on">
	<meta name="format-detection" content="telephone=no">
	<meta name="HandheldFriendly" content="true">
	<meta name="robots" content="noindex,nofollow">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Game">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 minimum-scale=1.0 user-scalable=0 minimal-ui shrink-to-fit=no">

	<!-- Use common styles. -->
	<style type="text/css">
		body {
			background-color: #000000;
			height: 100%;
			margin: 0;
			overflow: hidden;/**//*margin-bottom: 1px; an extra margin-bottom: 1px allows the iOS inner/outer check to work */
			padding: 0;
			width: 100%;
			
			image-rendering: optimizeSpeed; /* Older versions of FF */
			image-rendering: -moz-crisp-edges; /* FF 6.0+ */
			image-rendering: -webkit-optimize-contrast; /* Webkit (non-standard naming) */
			image-rendering: -o-crisp-edges; /* OSX & Windows Opera (12.02+) */
			image-rendering: crisp-edges; /* Older versions of FF */
			image-rendering: optimizeSpeed; /* Possible future browsers */
			-ms-interpolation-mode: nearest-neighbor; /* IE (non-standard naming) */
			image-rendering: pixelated; /* Chrome 41 */
		}

		/* Better allow game canvas and DOM interactions */
		#container > div {
			pointer-events: none;
		}
		#container > div > div {
			pointer-events: all;
		}
	</style>

	<!-- Fonts -->
	<!--
	<link rel="stylesheet" href="./assets/fonts/stylesheet.css">
	-->

    <!-- Use the latest production build. -->
    <script type="text/javascript" src="../../vendor/phaser/phaser.js"></script>
	<!-- Use custom scripts. -->
	<!--<script type="text/javascript" src="../../vendor/redlove/GameUtilities.js"></script>-->
	<?php
	// Gather files to load in order
	$scriptsToLoad = [
		'../../vendor/redlove/scripts/RedLove.js',
		'../../vendor/redlove/scripts/library/animation.js',
		'../../vendor/redlove/scripts/library/fullscreen.js',
		'../../vendor/redlove/scripts/library/phaser.js',
		'../../vendor/redlove/scripts/library/utilities.js',
		'../../vendor/redlove/scripts/library/utilities/datetime.js',
		'../../vendor/redlove/scripts/library/utilities/dom.js',
		'../../vendor/redlove/scripts/library/utilities/function.js',
		'../../vendor/redlove/scripts/library/utilities/image.js',
		'../../vendor/redlove/scripts/library/utilities/number.js',
		'../../vendor/redlove/scripts/library/utilities/object.js',
		'../../vendor/redlove/scripts/library/utilities/string.js'
	];

	// For each file
	foreach ( $scriptsToLoad as $script )
	{
		// Add modified time onto file to break cache
		$modifiedTime = filemtime($script);
	?>
	<script src="<?php echo $script; ?>?<?php echo $modifiedTime; ?>" type="text/javascript"></script>
	<?php
	}
	?>

	<!-- Load all plugins -->
	<?php
	$realpath = realpath(dirname(__FILE__));
	$relativePath = '../../vendor/redlove/scripts/classes';
	$directoryRealpath = realpath(dirname(__FILE__) . '/' . $relativePath);
	// Normalize directory path
	$directoryRealpath = str_ireplace('\\', '/', $directoryRealpath);


	$directory = new RecursiveDirectoryIterator($directoryRealpath, RecursiveDirectoryIterator::SKIP_DOTS | FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_SELF | FilesystemIterator::FOLLOW_SYMLINKS);

	$iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
	
	$data = array(
		'files' => array(),
	);

	foreach ( $iterator as $file_pathname => $spl_file_info )
	{
		// Normalize directory path
		$file_pathname = str_ireplace('\\', '/', $file_pathname);

		if ( $spl_file_info->isFile() )
		{
			$data['files'][] = $file_pathname;
		}
	}

	foreach ( $data['files'] as $file )
	{
		$jsSrc = $relativePath . str_ireplace($directoryRealpath, '', $file);
		?>
		<script type="text/javascript" src="<?php echo $jsSrc; ?>"></script>
		<?php
	}
	?>
	<!-- Custom config loading -->
	<script type="text/javascript">
		<?php
		// Get the name of the file to check for associated resources
		$filename = pathinfo(__FILE__, PATHINFO_FILENAME);//basename(__FILE__, '.php')

		/*
		// Scenes
		$sceneFilenames = [
			'DebugScene',
			'BootScene',
			'PreloadScene',
			'TitleScene',
			'IntroScene',
			'HudScene',
			'LevelsScene',
			'GameOverScene',
		];
		foreach ( $sceneFilenames as $sceneFilename )
		{
			$sceneFile = './scripts/' . $filename . '/scenes/' . $sceneFilename . '.js';
			if (is_file($sceneFile))
			{
				include($sceneFile);
			}
			else
			{
			?>
				console.log('Scene file "<?php echo $sceneFile; ?>" did not exist.');
			<?php
			}
		}
		*/

		// Set default config file
		$configFilename = 'default';
		// Check for overriding config file in querystring
		$configFilenameKey = 'config';
		if (! empty($_GET[$configFilenameKey]))
		{
			$sanitizedGet = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
			$configFilename = str_ireplace(str_split('./\\'), '', $sanitizedGet[$configFilenameKey]);
		}
		
		// Set config file and if it exists, include it
		$scriptsDirectory = './scripts/';
		// Option 1: Pooled configs directory
		$configsDirectory = $scriptsDirectory . 'configs/';
		$configsFile = $configsDirectory . $configFilename . '.js';
		// Option 2: Separate folder for config
		$configFileInDirectory = $scriptsDirectory . $configFilename . '/config.js';

		// Check for config options
		if (is_file($configsFile))
		{
			include($configsFile);
		}
		elseif (is_file($configFileInDirectory))
		{
			include($configFileInDirectory);
		}
		else
		{
		?>
			console.log('Config file did not exist.');
		<?php
		}
		?>

        // Set game configuration
		var config = config || {};

        // Create the game and pass it the configuration
        var game = new Phaser.Game(config);

		// Waiting until window load
        window.onload = function() {
			window.focus();
		};
    </script>
</head>
<body>
	<div id="container"></div>
</body>
</html>
