<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	
	<meta http-equiv="X-UA-Compatible" content="chrome=1, IE=9">
	<meta http-equiv="cleartype" content="on">
	<meta name="format-detection" content="telephone=no">
	<meta name="HandheldFriendly" content="true">
	<meta name="robots" content="noindex,nofollow">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Game">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 minimum-scale=1.0 user-scalable=0 minimal-ui shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

	<!-- Use common styles. -->
	<style type="text/css">
	</style>

	<!-- Fonts -->
	<!--
	<link rel="stylesheet" href="./assets/fonts/stylesheet.css">
	-->

	<!-- Custom config loading -->
	<script type="text/javascript">
    </script>
</head>
<body>




<style type="text/css">
    /* Always set the map height explicitly to define the size of the div element that contains the map. */
    #map {
        height: 60vh;
        /* margin-bottom: 2.0rem; */
        max-height: 300px;
        border-radius: 8px;
        -moz-border-radius: 8px;
        -webkit-border-radius: 8px;
    }
</style>

<div class="map map--basic" id="{{ $id }}"></div>

<script type="text/javascript">
    let basicMapLocations = {!! json_encode($locations) !!};
    let routeParameters = {!! json_encode($routeParameters) !!};

    // https://developers.google.com/maps/documentation/
    var map;
    var lastState = "";
    // https://mapstyle.withgoogle.com/
    var mapStyle = [
        {
            "featureType": "all",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#bdbdbd"
                }
            ]
        },
        {
            "featureType": "landscape.natural",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#cdeae9"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 35
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dadada"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#c9c9c9"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        }
    ];
    let basicMap;
    function initMap()
    {
        basicMap = new google.maps.Map(document.getElementById('{{ $id }}'), {
            // There are two required options for every map: center and zoom.
            center: {lat: -34.397, lng: 150.644},
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            // Styling
            styles: mapStyle,
            // Controls
            disableDefaultUI: true,
            fullscreenControl: false,
            mapTypeControl: false,
            rotateControl: false,
            scaleControl: true,
            streetViewControl: false,
            zoomControl: false,
            '': ''
        });

        // Handle map click
        basicMap.addListener('click', function(e)
        {
            console.log(e.latLng);
        });

        // Display the area between the location southWest and northEast.
        var bounds = new google.maps.LatLngBounds();
        // Iterate over locations
        for (i in basicMapLocations)
        {
            // Place marker
            var location = basicMapLocations[i];
            var myLatlng = new google.maps.LatLng(location.lat, location.lng);
            var opportunity = location.opportunity;
            var state = location.itemRouteParameters.state;
	        var marker = placeMarker(myLatlng, basicMap, opportunity, state);
            marker.addListener('click', function () {
                app = this;
                selectMarker(app);
            });

            // Add location to bounds
            bounds.extend(myLatlng);
        }
        // Adjust map to bounds
        if (basicMapLocations.length > 0) basicMap.fitBounds(bounds);

        function placeMarker(latLng, map, op, state)
        {
            // console.log(op);
            //if (op == 0 ) op = 5000;
            var wscale;
            if (window.screen.availWidth >= 992) {
                wscale = 60;
            } else {
                wscale = 100;
            }
            var marker = new google.maps.Marker({
                position: latLng,
                title: (state != null) ? state : 'State was not defined' ,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    fillColor: '#007899',
                    fillOpacity: 0.6,
                    // scale: Math.pow(2, 6) / 2,
                    scale: Math.sqrt(op) / wscale,
                    strokeColor: '#007899',
                    strokeWeight: 0.5
                },
                map: map
            });
            marker.state = state;

            return marker;
        }

        //google.maps.event.trigger(map, 'resize'); map.panTo(new google.maps.LatLng(x, y));
        jQuery(window).on('resize', function (event)
        {
            if (basicMap)
            {
                google.maps.event.trigger(basicMap, 'resize');
                // Adjust map to bounds
                basicMap.fitBounds(bounds);
            }
        });

        function selectMarker(app) {
            let lastState = ('{!! $lastState !!}');
            if ((app.state == null) || (lastState == app.state))return;
            
            app.get('map').panTo(app.getPosition());
            var url = '{!! route($routeRoot ,$routeParameters) !!}';
            url = url.replace(':state', app.state);
            window.location.href = url;
            
        }

    }
</script>

<!--
Load the API from the specified URL
The async attribute allows the browser to render the page while the API loads
The key parameter will contain your own API key (which is not needed for this tutorial)
The callback parameter executes the initMap() function
-->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGfc-yzyzslgrAK5ZesfhH0_kT9Cfo9LE&callback=initMap"></script>
	
	<!-- Optional JavaScript; choose one of the two! -->

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>

	<!-- Option 2: Separate Popper and Bootstrap JS -->
	<!--
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
	-->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

</body>
</html>
