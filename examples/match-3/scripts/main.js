// Hide loading screen
document.addEventListener('readystatechange', (event) => {
	// If complete
	if ( event.target.readyState === 'complete' )
	{
		const loadingScreen = document.querySelector('#loading-screen');
		// Fade and then hide
		RL.a.animateCSS(loadingScreen, 'fadeOut').then((message) => {
			loadingScreen.classList.add('d-none');
		});
	}
});

let myMatch3;

// Poll with interval until available
(function() {
	let nTimer = setInterval(function() {
		// If available, stop polling
		if (window.jQuery) {
			clearInterval(nTimer);
			//====================


			
			// DOM ready
			jQuery(document).ready(function($)
			{
				// Event listeners
				jQuery(document).on('click', '.cta--fullscreen-toggle', {}, RL.fullscreen.toggle);
				jQuery(document).on('click', '.cta--start', {}, startGame);

				document.addEventListener('fullscreenchange', (event) => {
					console.log(`Fullscreen: ${RL.fullscreen.is()}`);
				});

				document.addEventListener('Match3__buildBoard', (event) => {
					buildBoard(myMatch3, event.detail.animate);
				});
				
				// TODO: Adjust game board on window resize or fullscreen
				// Make sure puzzle fits inside of browser
				const windowHeight = $(window).height();
				const windowWidth = $(window).width();
				const gameContainerWidth = $('.game-board').width();
				const targetWidth = ( windowHeight < windowWidth ) ? windowHeight : windowWidth;
				
				// Start game
				myMatch3 = new Match3({
					autoFullscreen: false,
					boardHeight: targetWidth,
					boardWidth: targetWidth,
					debug: true,
					numCols: 8,
					numRows: 8
				});

				// Start swipe
				$(document).on('mousedown touchstart', `.${myMatch3.tileClassName}`, startSwipe.bind(myMatch3));
				jQuery(document).on('click', '.cta--board-reset', {}, (event) => {
					myMatch3.resetBoard(true);
				});
				jQuery(document).on('click', '.cta--hint-create', {}, (event) => {
					myMatch3.createHint();
				});
				jQuery(document).on('click', '.cta--move-default', {}, (event) => {
					myMatch3.doMove();
				});

				buildBoard(myMatch3);

				// Test game array
				const test = false;
				if ( test )
				{
					//console.log(JSON.stringify(myMatch3.gameArray));
					myMatch3.gameArray = JSON.parse(`[
						[{"color":"#5630D8","row":0,"col":0,"isEmpty":false},{"color":"#5630D8","row":0,"col":1,"isEmpty":false},{"color":"#3192F1","row":0,"col":2,"isEmpty":false},{"color":"#5630D8","row":0,"col":3,"isEmpty":false},{"color":"#FFD449","row":0,"col":4,"isEmpty":false},{"color":"#666666","row":0,"col":5,"isEmpty":false},{"color":"#666666","row":0,"col":6,"isEmpty":false},{"color":"#5630D8","row":0,"col":7,"isEmpty":false}],
						[{"color":"#E23822","row":1,"col":0,"isEmpty":false},{"color":"#FFD449","row":1,"col":1,"isEmpty":false},{"color":"#5630D8","row":1,"col":2,"isEmpty":false},{"color":"#F9A620","row":1,"col":3,"isEmpty":false},{"color":"#F9A620","row":1,"col":4,"isEmpty":false},{"color":"#FFD449","row":1,"col":5,"isEmpty":false},{"color":"#3192F1","row":1,"col":6,"isEmpty":false},{"color":"#144B14","row":1,"col":7,"isEmpty":false}],
						[{"color":"#5630D8","row":2,"col":0,"isEmpty":false},{"color":"#E23822","row":2,"col":1,"isEmpty":false},{"color":"#5630D8","row":2,"col":2,"isEmpty":false},{"color":"#FFD449","row":2,"col":3,"isEmpty":false},{"color":"#FFD449","row":2,"col":4,"isEmpty":false},{"color":"#3192F1","row":2,"col":5,"isEmpty":false},{"color":"#E23822","row":2,"col":6,"isEmpty":false},{"color":"#FFD449","row":2,"col":7,"isEmpty":false}],
						[{"color":"#F9A620","row":3,"col":0,"isEmpty":false},{"color":"#F9A620","row":3,"col":1,"isEmpty":false},{"color":"#144B14","row":3,"col":2,"isEmpty":false},{"color":"#5630D8","row":3,"col":3,"isEmpty":false},{"color":"#144B14","row":3,"col":4,"isEmpty":false},{"color":"#666666","row":3,"col":5,"isEmpty":false},{"color":"#E23822","row":3,"col":6,"isEmpty":false},{"color":"#3192F1","row":3,"col":7,"isEmpty":false}],
						[{"color":"#FFD449","row":4,"col":0,"isEmpty":false},{"color":"#FFD449","row":4,"col":1,"isEmpty":false},{"color":"#5630D8","row":4,"col":2,"isEmpty":false},{"color":"#E23822","row":4,"col":3,"isEmpty":false},{"color":"#666666","row":4,"col":4,"isEmpty":false},{"color":"#144B14","row":4,"col":5,"isEmpty":false},{"color":"#3192F1","row":4,"col":6,"isEmpty":false},{"color":"#F9A620","row":4,"col":7,"isEmpty":false}],
						[{"color":"#5630D8","row":5,"col":0,"isEmpty":false},{"color":"#5630D8","row":5,"col":1,"isEmpty":false},{"color":"#FFD449","row":5,"col":2,"isEmpty":false},{"color":"#5630D8","row":5,"col":3,"isEmpty":false},{"color":"#5630D8","row":5,"col":4,"isEmpty":false},{"color":"#FFD449","row":5,"col":5,"isEmpty":false},{"color":"#FFD449","row":5,"col":6,"isEmpty":false},{"color":"#144B14","row":5,"col":7,"isEmpty":false}],
						[{"color":"#FFD449","row":6,"col":0,"isEmpty":false},{"color":"#3192F1","row":6,"col":1,"isEmpty":false},{"color":"#5630D8","row":6,"col":2,"isEmpty":false},{"color":"#5630D8","row":6,"col":3,"isEmpty":false},{"color":"#666666","row":6,"col":4,"isEmpty":false},{"color":"#666666","row":6,"col":5,"isEmpty":false},{"color":"#5630D8","row":6,"col":6,"isEmpty":false},{"color":"#FFD449","row":6,"col":7,"isEmpty":false}],
						[{"color":"#3192F1","row":7,"col":0,"isEmpty":false},{"color":"#3192F1","row":7,"col":1,"isEmpty":false},{"color":"#5630D8","row":7,"col":2,"isEmpty":false},{"color":"#666666","row":7,"col":3,"isEmpty":false},{"color":"#144B14","row":7,"col":4,"isEmpty":false},{"color":"#FFD449","row":7,"col":5,"isEmpty":false},{"color":"#FFD449","row":7,"col":6,"isEmpty":false},{"color":"#144B14","row":7,"col":7,"isEmpty":false}]
					]`);
				}

				function startGame ()
				{
					if ( myMatch3.autoFullscreen )
					{
						// Attempt fullscreen
						RL.fullscreen.enter();
					}
					
					// Fade out start screen
					const $startScreen = $('#start-screen');
					const ease = 'circ.out';
					gsap.to($startScreen, {
						autoRound: false,
						duration: 1,
						ease: ease,
						autoAlpha: 0,
						onComplete: () => {
							myMatch3.startTurn();
						}
					});
				}

				function buildBoard ( match3Instance, animate )
				{
					const $gameBoard = $(`.${match3Instance.gameBoardClassName}`);
					$gameBoard.empty();
					$gameBoard.css({
						width: match3Instance.boardWidth,
						height: match3Instance.boardHeight
					});
	
					// Save tiles to animate
					let $tilesArray = [];

					// For each column
					for ( let colIndex = match3Instance.numCols - 1; colIndex >= 0; colIndex -- )
					{
						// For each row
						for ( let rowIndex = match3Instance.numRows - 1; rowIndex >= 0; rowIndex -- )
						{
							const tile = match3Instance.getTileAt(rowIndex, colIndex);
							const $colEl = match3Instance.createDisplayTile(tile);
							
							// If animating
							if ( animate )
							{
								$tilesArray.push($colEl);
							}
						}
					}

					// If animating
					if ( animate )
					{
						// Move tile in
						let ease = 'circ.out';
						
						gsap.from($tilesArray, {
							autoRound: false,
							duration: (match3Instance.transitionMs / 1000) * 0.125,
							stagger: 0.01,
							ease: ease,
							//x: this.tileWidth * this.getTileCol(tile1),
							//y: this.tileHeight * this.getTileRow(tile1),
							top: `-=${match3Instance.boardHeight}`,
							onComplete: () => {
								$tilesArray = null;
								match3Instance.buildBoardAfter();
							}
						});
					}
					else
					{
						match3Instance.buildBoardAfter();
					}
				}

				function startSwipe ( event )
				{
					// If debug
					if ( myMatch3.debug )
					{
						console.log('startSwipe');
					}
					
					event.preventDefault();

					// If can't pick, stop
					if ( ! this.canInteract )
					{
						// If debug
						if ( myMatch3.debug )
						{
							console.log(`startSwipe: Can't pick yet`);
						}
						
						return;
					}

					// If dragging, stop
					if ( this.dragging )
					{
						// If debug
						if ( myMatch3.debug )
						{
							console.log(`startSwipe: Already dragging`);
						}
						
						return;
					}

					this.handleStartInteraction();

					this.dragging = true;

					// Set pointer start position
					this.pointerStartPos = {x: event.clientX, y: event.clientY, time: Date.now()};
					// Update with mobile touches
					// https://stackoverflow.com/questions/9585487/cant-get-coordinates-of-touchevents-in-javascript-on-android-devices
					const touches = event.originalEvent.touches;
					if ( touches )
					{
						this.pointerStartPos.x = touches[0].clientX;
						this.pointerStartPos.y = touches[0].clientY;
					}

					const $tile = $(event.currentTarget);
					const tileRow = +$tile.attr(this.tileRowAttributeName);
					const tileCol = +$tile.attr(this.tileColumnAttributeName);
					const tile = this.getTileAt(tileRow, tileCol);
					// Set selected tile and when stopping, determine swipe or click handling with from/to tiles
					this.selectedTile = tile;

					// If debug
					if ( myMatch3.debug )
					{
						console.log(`Selected tile: ${tileRow},${tileCol}`);
					}
					
					$(document).one('mouseup touchend', stopSwipe.bind(this));
				}
				
				function stopSwipe ( event )
				{
					// If debug
					if ( myMatch3.debug )
					{
						console.log('stopSwipe');
					}
					
					event.preventDefault();

					this.dragging = false;
					let tile;
					
					// Use pointer positions for swipe direction

					// Set pointer start position
					this.pointerStopPos = {x: event.clientX, y: event.clientY, time: Date.now()};
					// Update with mobile touches
					// If using touch events, check for change in touched element if swipe
					// https://stackoverflow.com/questions/11523496/find-element-finger-is-on-during-a-touchend-event
					const changedTouches = event.changedTouches;
					if ( changedTouches )
					{
						this.pointerStopPos.x = changedTouches[0].clientX;
						this.pointerStopPos.y = changedTouches[0].clientY;
					}

					// Gather movement details to help check swipe
					const selectedTile = this.selectedTile;
					const selectedTileRow = this.getTileRow(selectedTile);
					const selectedTileCol = this.getTileCol(selectedTile);

					let targetTileRow = selectedTileRow;
					let targetTileCol = selectedTileCol;
					const swipeMovementDeltaX = this.pointerStopPos.x - this.pointerStartPos.x;
					const swipeMovementDeltaY = this.pointerStopPos.y - this.pointerStartPos.y;
					const swipeMovementDelta = Math.max(Math.abs(swipeMovementDeltaX), Math.abs(swipeMovementDeltaY));
					const swipeMovementDeltaMin = 10;
					const swipeTimeDelta = this.pointerStopPos.time - this.pointerStartPos.time;
					const swipeTimeDeltaMin = 50;
					const isSwipe = (swipeMovementDelta >= swipeMovementDeltaMin && swipeTimeDelta >= swipeTimeDeltaMin);

					// Check if ending on the same tile to handle clicks instead of swipes
					let isClick = false;

					let el = event.target;
					// If using touch events, check for change in touched element if swipe
					// https://stackoverflow.com/questions/11523496/find-element-finger-is-on-during-a-touchend-event
					if ( changedTouches )
					{
						el = document.elementFromPoint(changedTouches[0].clientX, changedTouches[0].clientY);
					}

					const $touchedEl = $(el);

					// If target is a tile
					if ( $touchedEl.hasClass(this.tileWrapperClassName) )
					{
						// Get tile
						const $tile = $touchedEl.parent();
						const touchedTileRow = +$tile.attr(this.tileRowAttributeName);
						const touchedTileCol = +$tile.attr(this.tileColumnAttributeName);
						tile = this.getTileAt(touchedTileRow, touchedTileCol);

						// If tiles are the same, set up for click
						if ( this.areTilesSame(this.selectedTile, tile) )
						{
							isClick = true;
						}
						
						// If debug
						if ( myMatch3.debug )
						{
							console.log(`stopSwipe: On tile ${this.getTileRow(tile)},${this.getTileCol(tile)}`);
						}
					}

					// If debug
					if ( myMatch3.debug )
					{
						console.log(`stopSwipe: isClick ${isClick}, isSwipe ${isSwipe}`);
					}

					// If no interaction detected, stop
					if ( ! isClick && ! isSwipe )
					{
						// If debug
						if ( myMatch3.debug )
						{
							console.log(`stopSwipe: No interaction detected`);
						}
						
						return;
					}
					// Else if click and not swipe
					else if ( isClick && ! isSwipe )
					{
						// If debug
						if ( myMatch3.debug )
						{
							console.log(`stopSwipe: Handling click`);
						}

						// If there is no fromTile, set it and stop
						if ( ! this.fromTile )
						{
							this.fromTile = tile;
							return;
						}
						// Else, set the toTile and swap
						else
						{
							this.toTile = tile;
						}
					}
					// Else, handle swipe
					else
					{
						// If debug
						if ( myMatch3.debug )
						{
							console.log(`stopSwipe: Handling swipe`);
						}

						// Determine which direction has the greater movement

						// If left/right movement greater than up/down movement
						if ( Math.abs(swipeMovementDeltaX) > Math.abs(swipeMovementDeltaY) )
						{
							// Right
							targetTileCol += ( swipeMovementDeltaX > 0 ) ? 1 : -1;
						}
						// Else up/down
						else
						{
							// Down
							targetTileRow += ( swipeMovementDeltaY > 0 ) ? 1 : -1;
						}

						const targetTile = this.getTileAt(targetTileRow, targetTileCol);
						const $targetTile = this.getDisplayTile(targetTile);
						tile = targetTile;
						
						this.fromTile = selectedTile;
						this.toTile = tile;
					}

					// Attempt tile swap
					this.swapTiles(this.fromTile, this.toTile, true);
				}

			});

			// Window load
			jQuery(window).on('load', function ()
			{
			});

			

			//====================
			// Continue polling
		}
	}, 100);
})();
