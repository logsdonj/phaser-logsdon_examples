class Match3
{
	defaultParams = {
		autoFullscreen: true,
		autoHint: true,
		boardHeight: 800,
		boardWidth: 800,
		debug: false,
		numCols: 8,
		numRows: 8,
		transitionMs: 500,
		inactivityTimeoutMs: 5000,
		gameBoardClassName: 'game-board',
		eventNamePrefix: 'Match3__',
		tileClassName: 'tile',
		tileWrapperClassName: 'tile-wrapper',
		tileColumnAttributeName: 'data-tile-col',
		tileRowAttributeName: 'data-tile-row',
		tileHintClassName: 'tile--hint'
	};
	params;
	
	// Constants
	HORIZONTAL = 'horizontal';
	VERTICAL = 'vertical';
	UP = 'up';
	DOWN = 'down';
	LEFT = 'left';
	RIGHT = 'right';

	// https://medium.com/@dwang.loves.ai/javascript-how-to-generate-candy-crush-or-bejeweled-grid-with-no-matches-138295ebefbf
	// https://www.emanueleferonato.com/2017/10/24/complete-bejeweled-prototype-made-with-jquery-updated-to-jquery-3-2-1-improving-swipe-controls-and-working-with-any-board-size/

	// https://rexrainbow.github.io/phaser3-rex-notes/docs/site/board-bejeweled/

	// https://www.redblobgames.com/grids/edges/
	// https://news.ycombinator.com/item?id=32216436
	// https://www.youtube.com/watch?v=TXlIfjAoeWk
	// https://github.com/gereleth/hexapipes
	// https://hexapipes.vercel.app/hexagonal/5/1

	tileReference = {
		red: {
			color: '#E23822',
			image: '',
			points: 1,
			description: 'Red'
		},
		blue: {
			color: '#3192F1',
			image: '',
			points: 1,
			description: 'Blue'
		},
		purple: {
			color: '#5630D8',
			image: '',
			points: 1,
			description: 'Purple'
		},
		yellow: {
			color: '#FFD449',
			image: '',
			points: 1,
			description: 'Yellow'
		},
		green: {
			color: '#144B14',
			image: '',
			points: 1,
			description: 'Green'
		},
		silk: {
			color: '#666666',
			image: '',
			points: 1,
			description: 'Silk'
		},
		gold: {
			color: '#F9A620',
			image: '',
			points: 1,
			description: 'Gold'
		},
		bomb: {
			color: '#000000',
			image: '',
			points: 1,
			description: 'Bomb',
			type: 'bomb'
		},
		laser: {
			color: '#FC46AA',
			image: '',
			points: 1,
			description: 'Laser',
			type: 'laser'
		},
		wildcard: {
			color: '#00FF66',
			image: '',
			points: 1,
			description: 'Wildcard',
			type: 'wildcard'
		}
	};
	tileKeys = Object.keys(this.tileReference);

	// Game configuration
	numRows = 8;
	numCols = 8;
	gameArray;
	
	// Track interaction states
	gameState = 'playing';
	turn = 'player';
	_canInteract = false;
	dragging = false;
	selectedTile = null;
	fromTile = null;
	toTile = null;

	// Track number of tiles processing
	numTilesToSwap = 0;
	numTilesToDestroy = 0;
	numTilesToFall = 0;
	numTilesToReplinish = 0;
	numTilesToReset = 0;

	// TODO: Work on scoring tracking, scores per color/tile type, combo, score multiplier, penalties
	ledger;
	scoreTotals = {
		match3: [],
		match4: [],
		match5: [],
		match6: [],
		match7: [],
		matchMax: 0,
		vMatch3: [],
		vMatch4: [],
		vMatch5: [],
		vMatchMax: 0,
		hMatch3: [],
		hMatch4: [],
		hMatch5: [],
		hMatchMax: 0,
		axisMatchMax: 0,
		numBoardsReset: 0,
		numHintsGiven: 0,
		numMatchSessions: 0, // Matches in 1 fall and replenish cycle
		maxMatchSessionScore: 0,
		numConsecutiveMatches: 0, // Fall and replenishes cycles
		maxConsecutiveMatches: 0,
		numConsecutiveMatchChecks: 0,
		maxScoreMultiplier: 0,
		score: 0,
		tiles: {
			total: 0,
			keys: {}
		}
	};
	score = 0;
	comboCount = 0;
	numTilesDestroyed = 0;

	// TODO: Work on special tile replacement, like all X to Y
	// TODO: Work on special tiles, like bomb tiles
	// TODO: Work on puzzles where moves that take out all tiles on board; blank spaces and no replenishing during time
	// TODO: Work on modes where you have to get so many X matches of Y of a color to complete challenges
	// TODO: Work on computer opponent taking turns

	constructor ( params )
	{
		// Whitelist using defaultParams for safety
		this.params = RL.u.mergeWhitelistObject(this.defaultParams, params);//this.params = Object.assign({}, this.defaultParams, params);

		const paramKeys = Object.keys(this.params);
		for ( let key of paramKeys )
		{
			this[key] = this.params[key];
		}
		
		// If debug
		if ( this.debug )
		{
			console.log('Match3');
		}

		// Create grid dimensions
		this.numCols = ( this.numCols >= 1 ) ? this.numCols : 1;
		this.numRows = ( this.numRows >= 1 ) ? this.numRows : 1;
		this.numTiles = this.numCols * this.numRows;
		this.boardRatio = this.boardWidth / this.boardHeight;
		this.tileWidth = this.boardWidth / this.numCols;
		this.tileHeight = this.boardHeight / this.numRows;
		this.tileRatio = this.tileWidth / this.tileHeight;

		// Set reference key and type inside of tiles
		this.normalTileKeys = [];
		for ( const tileKey of this.tileKeys )
		{
			const tile = this.tileReference[tileKey];
			tile.key = tileKey;

			if ( ! Object.hasOwn(tile, 'type') || tile.type === 'normal' )
			{
				tile.type = 'normal';
				this.normalTileKeys.push(tileKey);
			}
		}
		
		this.generateGrid();
	}

	generateGrid ( tileKeys, numCols, numRows )
	{
		if ( typeof(tileKeys) === 'undefined' )
		{
			tileKeys = this.tileKeys;
		}

		if ( ! RL.u.isArray(tileKeys) )
		{
			return [];
		}

		if ( typeof(numCols) === 'undefined' )
		{
			numCols = this.numCols;
		}

		if ( typeof(numRows) === 'undefined' )
		{
			numRows = this.numRows;
		}

		numCols = parseInt(numCols);
		numRows = parseInt(numRows);

		if ( numCols <= 0 || numRows <= 0 )
		{
			throw Error('Invalid props');
		}

		this.tileKeys = tileKeys;
		this.gameArray = [];

		// For each row
		for ( let i = 0; i < numRows; i++ )
		{
			// Start row
			this.gameArray[i] = [];

			// For each column
			for ( let j = 0; j < numCols; j++ )
			{
				// Try up to 100 times make a random non-matching entry
				// Instead of do/while
				for ( let k = 0; k < 100; k++ )
				{
					// Pick a random entry
					this.gameArray[i][j] = this.createTile(i, j);

					// If a non-match, stop
					if ( this.findTileMatches(i, j).num == 0 )
					{
						break;
					}
					// Else try again
				}
			}
		}

		return this.gameArray;
	}

	buildBoard ( animate )
	{
		// If debug
		if ( this.debug )
		{
			console.log(`buildBoard`);
		}

		// Event for building game board
		this.dispatchEvent('buildBoard', {animate: animate});
	}

	buildBoardAfter ()
	{
		this.numTilesToReset = 0;
		this.allowInteraction();
	}

	resetBoard ( animate )
	{
		// If debug
		if ( this.debug )
		{
			console.log(`resetBoard`);
		}

		// Clear out anything related to the previous game board
		this.preventInteraction();
		this.stopInactivity();
		
		// Update scoreTotals
		this.scoreTotals.numBoardsReset ++;

		// Save tiles to animate
		let $tilesArray = [];

		// For each column
		for ( let j = this.numCols - 1; j >= 0; j -- )
		{
			// For each row
			for ( let i = this.numRows - 1; i >= 0; i -- )
			{
				this.numTilesToReset ++;

				// Get tile
				const tile = this.getTileAt(i, j);
				const $tile = this.getDisplayTile(tile);
				
				// If animating
				if ( animate )
				{
					$tilesArray.push($tile);
				}
			}
		}

		// If animating
		if ( animate )
		{
			// Move tile away
			let ease = 'circ.in';
			
			gsap.to($tilesArray, {
				autoRound: false,
				duration: (this.transitionMs / 1000) * 0.5,
				stagger: 0.01,
				ease: ease,
				//x: this.tileWidth * this.getTileCol(tile1),
				//y: this.tileHeight * this.getTileRow(tile1),
				top: `+=${this.boardHeight}`,
				onComplete: () => {
					// Remove display tiles
					for ( const $tile of $tilesArray )
					{
						$tile.remove();
					}

					// Regenerate grid and board
					this.resetBoardAfter(animate);
				}
			});
		}
		else
		{
			// Regenerate grid and board
			this.resetBoardAfter(animate);
		}
	}

	resetBoardAfter (animate)
	{
		// Regenerate grid and board
		this.generateGrid();
		this.buildBoard(animate);
		this.startTurn();
	}

	createTile ( row, col )
	{
		const randInt = RL.u.getRandomInt(0, this.tileKeys.length - 1);
		// Copy the tile reference with new data
		const tile = Object.assign(
			{}, 
			this.tileReference[ this.tileKeys[randInt] ],
			{
				row: row,
				col: col,
				isEmpty: false
			}
		);
		return tile;
	}

	createDisplayTile ( tile )
	{
		const tileRow = this.getTileRow(tile);
		const tileCol = this.getTileCol(tile);

		return $(`<div class="${this.tileClassName}"><div class="${this.tileWrapperClassName}"></div></div>`)
		.css({
			top: this.tileHeight * tileRow,
			left: this.tileWidth * tileCol,
			width: this.tileWidth,
			height: this.tileHeight,
			backgroundColor: tile.color
		})
		.attr({
			[this.tileRowAttributeName]: tileRow,
			[this.tileColumnAttributeName]: tileCol
		})
		.appendTo(`.${this.gameBoardClassName}`);
	}

	replaceTileAt ( row, col, withTile )
	{
		if ( typeof(withTile) === 'undefined' )
		{
			const randInt = RL.u.getRandomInt(0, this.tileKeys.length - 1);
			withTile = this.tileReference[ this.tileKeys[randInt] ];
		}
		else if ( typeof(withTile) === 'number' )
		{
			withTile = this.tileReference[ this.tileKeys[withTile] ];
		}
		else if ( typeof(withTile) === 'string' )
		{
			withTile = this.tileReference[withTile];
		}

		// TODO: Maybe some animation in the switch

		// Copy the tile reference with new data
		const tile = Object.assign(
			{}, 
			withTile,
			{
				row: row,
				col: col,
				isEmpty: false
			}
		);
		this.gameArray[row][col] = tile;

		this.updateDisplayTile(tile);

		return tile;
	}

	updateDisplayTile ( tileOrRow, col )
	{
		// If a col was passed, tile is the row
		const tile = ( typeof(col) !== 'undefined' ) ? this.getTileAt(tile, col) : tileOrRow;
		const $tile = this.getDisplayTile(tile);
		$tile.css({
			backgroundColor: tile.color
		});

		return $tile;
	}

	getDisplayTile ( tileOrRow, col )
	{
		// If a col was passed, tile is the row
		const tile = ( typeof(col) !== 'undefined' ) ? this.getTileAt(tile, col) : tileOrRow;

		return $(`.${this.tileClassName}[${this.tileRowAttributeName}="${this.getTileRow(tile)}"][${this.tileColumnAttributeName}="${this.getTileCol(tile)}"]`);
	}

	cleanCopyTileCustomProps ( fromTile, toTile )
	{
		const gameTileKeys = ['col', 'isEmpty', 'row'];
		const toTileGameProps = RL.u.whitelistObjectByKeys(gameTileKeys, toTile);
		const fromTileCustomProps = RL.u.blacklistObjectByKeys(gameTileKeys, fromTile);
		return Object.assign({}, fromTileCustomProps, toTileGameProps);
	}

	getTile ( key, col )
	{
		// If more than a key passed, assume row + col and forward on
		if ( typeof(col) !== 'undefined' )
		{
			return this.getTileAt(key, col);
		}

		return this.tileReference[key];
	}

	getTileAt ( row, col )
	{
		// If a col passed, assume key and forward on
		if ( typeof(col) === 'undefined' )
		{
			return this.getTile(row);
		}

		// If out of bounds, stop
		if ( row < 0 || row >= this.numRows || col < 0 || col >= this.numCols )
		{
			return -1;
		}

		// If board entry doesn't exist, stop
		if ( typeof(this.gameArray[row]) === 'undefined' || typeof(this.gameArray[row][col]) === 'undefined' )
		{
			return -1;
		}

		return this.gameArray[row][col];
	}
	
	/**
	 * Check if the same tile
	 * @param {*} tile1 
	 * @param {*} tile2 
	 * @returns 
	 */
    areTilesSame ( tile1, tile2 )
	{
        return this.getTileRow(tile1) == this.getTileRow(tile2) && this.getTileCol(tile1) == this.getTileCol(tile2);
    }

	/**
	 * Check if tiles are right adjacent or next to each other
	 * @param {*} tile1 
	 * @param {*} tile2 
	 * @returns 
	 */
    areTilesBeside ( tile1, tile2 )
	{
        return (
			Math.abs(this.getTileRow(tile1) - this.getTileRow(tile2)) + 
			Math.abs(this.getTileCol(tile1) - this.getTileCol(tile2)) == 1
		);
    }

	/**
	 * Check if tiles are diagonal from each other
	 * @param {*} tile1 
	 * @param {*} tile2 
	 * @returns 
	 */
    areTilesDiagonal ( tile1, tile2 )
	{
        return (
			Math.abs(this.getTileRow(tile1) - this.getTileRow(tile2)) == 1 && 
			Math.abs(this.getTileCol(tile1) - this.getTileCol(tile2)) == 1
		);
    }

    getTileRow ( tile )
	{
		return tile ? tile.row : -1;
        return Math.floor(tile.tileSprite.y / gameOptions.tileSize);
    }

    getTileCol ( tile )
	{
		return tile ? tile.col : -1;
        return Math.floor(tile.tileSprite.x / gameOptions.tileSize);
    }
	
    swapTiles ( tile1, tile2, allowSwapBack )
	{
		// If the same tile or not adjacent, stop
		if ( this.areTilesSame(tile1, tile2) || ! this.areTilesBeside(tile1, tile2) )
		{
			// If debug
			if ( this.debug )
			{
				console.log('swapTiles: Illegal move');
			}
			
			this.selectedTile = null;
			this.fromTile = null;
			this.toTile = null;
			return;
		}

		// Stop inactivity
		this.stopInactivity();

        this.numTilesToSwap += 2;// -1 after each tile swap animation complete
        this.preventInteraction();

		// TODO: Revisit swapping data and whether it happens here or in tween
        let tile1Row = this.getTileRow(tile1);
        let tile1Col = this.getTileCol(tile1);
        let tile2Row = this.getTileRow(tile2);
        let tile2Col = this.getTileCol(tile2);

		// Swap tile data
		tile1.row = tile2Row;
		tile1.col = tile2Col;
		tile2.row = tile1Row;
		tile2.col = tile1Col;

        this.gameArray[tile1Row][tile1Col] = tile2;
        this.gameArray[tile2Row][tile2Col] = tile1;

		// Update element attributes
		const $tile1 = this.getDisplayTile(tile2);
		const $tile2 = this.getDisplayTile(tile1);
		$tile1.attr({
			[this.tileRowAttributeName]: tile2Row,
			[this.tileColumnAttributeName]: tile2Col
		});
		$tile2.attr({
			[this.tileRowAttributeName]: tile1Row,
			[this.tileColumnAttributeName]: tile1Col
		});

        this.tweenSwap(tile1, tile2, allowSwapBack);
    }

	tweenSwap ( tile1, tile2, allowSwapBack )
	{
		// Tween
		
		const $tile1 = this.getDisplayTile(tile1);
		const $tile2 = this.getDisplayTile(tile2);
		let ease = 'circ.out';

		// If not swapping back, change easing
		if ( ! allowSwapBack )
		{
			ease = 'bounce.out';
		}
		
		gsap.to($tile1, {
			autoRound: false,
			duration: this.transitionMs / 1000,
			ease: ease,
			//x: this.tileWidth * this.getTileCol(tile1),
			//y: this.tileHeight * this.getTileRow(tile1),
			top: this.tileHeight * this.getTileRow(tile1),
			left: this.tileWidth * this.getTileCol(tile1),
			onComplete: () => {
				this.tweenSwapEnd(tile1, tile2, allowSwapBack);
			}
		});

		gsap.to($tile2, {
			autoRound: false,
			duration: this.transitionMs / 1000,
			ease: ease,
			top: this.tileHeight * this.getTileRow(tile2),
			left: this.tileWidth * this.getTileCol(tile2),
			onComplete: (tween) => {
				this.tweenSwapEnd(tile1, tile2, allowSwapBack);
			}
		});
	}

	tweenSwapEnd ( tile1, tile2, allowSwapBack )
	{
		this.numTilesToSwap --;
		
		// If debug
		if ( this.debug )
		{
			console.log(`tweenSwapEnd: ${this.numTilesToSwap} more to swap`);
		}
		
		// If still swapping tiles, stop
		if ( this.numTilesToSwap > 0 )
		{
			return;
		}

		// If a bomb used
		if ( tile1.type === 'bomb' || tile2.type === 'bomb' )
		{
			// Check tiles being blasted and bombs blasted continue to check and add to blast radius, with a slight delay in destroying

			const boomTile = ( tile1.type === 'bomb' ) ? tile1 : tile2;
			console.log('Do boom!');
			// Go around tile
			const blastRadius = 1;
			const destroyTilesMap = this.getTileMapFromRadiusAroundTile(boomTile, blastRadius);

			// Handle scoring of tiles to destroy
			this.handleDestroyTileScoring(destroyTilesMap);
			return;
		}

		// If a laser used
		if ( tile1.type === 'laser' || tile2.type === 'laser' )
		{
			const laserTile = ( tile1.type === 'laser' ) ? tile1 : tile2;
			console.log('Do laser!');
			// Go in direction
			const laserDistance = 2;
			const destroyTilesMap = this.getTileMapInDirection(laserTile, '+', laserDistance); // Laser

			// Handle scoring of tiles to destroy
			this.handleDestroyTileScoring(destroyTilesMap);
			return;
		}
		
		// If there are matches, handle them and stop
		const boardMatches = this.findBoardMatches();
		if ( boardMatches.length > 0 )
		{
			// If debug
			if ( this.debug )
			{
				console.log(`tweenSwapEnd: Match in board`);
			}
			
			// Start tracking times consecutively checking for matches
			this.numConsecutiveMatchChecks = 0;
			this.numConsecutiveMatches = 0;
			this.handleMatches(boardMatches);
			return;
		}

		// If we swapping tiles back to original places, do it and stop
		if ( allowSwapBack )
		{
			// If debug
			if ( this.debug )
			{
				console.log('tweenSwapEnd: No match, swapping back');
			}
			
			this.swapTiles(tile1, tile2, false);
			return;
		}
		
		this.resetInteraction();
	}

    handleMatches ( matchSets )
	{
		// Start score multiplier checks
		let scoreMultiplier = 1;

		// Increment the number of consecutive match checks (swipe check > fall and replenish check, repeat...)
		this.numConsecutiveMatchChecks ++;

		// If matches not passed from checks for matches, find them
		if ( typeof(matchSets) === 'undefined' )
		{
			matchSets = this.findBoardMatches();
		}

		// If debug
		if ( this.debug )
		{
			console.log(`handleMatches: matchSets`, matchSets);
		}

		// Start totalling the score for this round
		let matchSessionScore = 0;

		// For each match set, calculate score
		for ( let matchSet of matchSets )
		{
			// Increment the number of consecutive matches
			this.numConsecutiveMatches ++;
			
			// If debug
			if ( this.debug )
			{
				console.log(`handleMatches: matchSet.num ${matchSet.num}`);
			}

			// Update scoreTotals
			this.scoreTotals.matchMax = Math.max(this.scoreTotals.matchMax, matchSet.num);
			this.scoreTotals.vMatchMax = Math.max(this.scoreTotals.vMatchMax, matchSet.vNum);
			this.scoreTotals.hMatchMax = Math.max(this.scoreTotals.hMatchMax, matchSet.hNum);

			if ( matchSet.vNum > 0 )
			{
				this.scoreTotals[`vMatch${matchSet.vNum}`].push(matchSet);
			}

			if ( matchSet.hNum > 0 )
			{
				this.scoreTotals[`hMatch${matchSet.hNum}`].push(matchSet);
			}

			this.scoreTotals[`match${matchSet.num}`].push(matchSet);
			this.scoreTotals.tiles.total += matchSet.num;
			// For each tile in the set
			for ( const matchSetTile of matchSet.tiles )
			{
				// If not initialized, do it
				if ( typeof(this.scoreTotals.tiles.keys[matchSetTile.key]) === 'undefined' )
				{
					this.scoreTotals.tiles.keys[matchSetTile.key] = 0;
				}

				// Increase the count for the key
				this.scoreTotals.tiles.keys[matchSetTile.key] ++;

				// TODO: For tile multipliers where 2x the tiles/wildcards and such, do more fine-detailed checking... if a 2x tile was used then increase the color, etc.
			}
			
			// Increase score multiplier for matches with more than 3 tiles
			if ( matchSet.num > 3 )
			{
				scoreMultiplier += matchSet.num - 3;
			}

			// Increase score multiplier for each consecutive match
			if ( this.numConsecutiveMatches > 1 )
			{
				scoreMultiplier += this.numConsecutiveMatches;

				// If hint given, penalty: lower the multiplier
				if ( this.hintGiven )
				{
					scoreMultiplier --;
				}
			}

			matchSessionScore += matchSet.num * scoreMultiplier;
		}
		
		// If debug
		if ( this.debug )
		{
			console.log(`handleMatches: matchSessionScore ${matchSessionScore}, scoreMultiplier ${scoreMultiplier}, numConsecutiveMatches ${this.numConsecutiveMatches}, numConsecutiveMatchChecks ${this.numConsecutiveMatchChecks}, hint ${this.hintGiven}`);
		}

		// Score with the number of tiles matched or change it to a number like 10 for each match base amount
		this.score += matchSessionScore;

		// Update scoreTotals
		this.scoreTotals.score += matchSessionScore;
		this.scoreTotals.axisMatchMax = Math.max(this.scoreTotals.vMatchMax, this.scoreTotals.hMatchMax);
		this.scoreTotals.maxMatchSessionScore = Math.max(this.scoreTotals.maxMatchSessionScore, matchSessionScore);
		this.scoreTotals.maxScoreMultiplier = Math.max(this.scoreTotals.maxScoreMultiplier, scoreMultiplier);
		this.scoreTotals.numMatchSessions ++;

		// Update score
		this.updateScore();

        this.destroyTiles();
    }

	handleDestroyTileScoring ( destroyTilesMap )
	{
		// Get tiles to destroy from map
		let tilesToDestroy = [];
		for ( let row = 0; row < this.numRows; row ++ )
		{
			for ( let col = 0; col < this.numCols; col ++ )
			{
				// If not destroying, skip
				if ( destroyTilesMap[row][col] == 0 )
				{
					continue;
				}

				console.log(`destroyTilesMap ${destroyTilesMap[row][col]}`)

				// Add tile to destroy
				tilesToDestroy.push(this.getTileAt(row, col));
			}
		}

		// Handle scoring of tiles to destroy

		// Start totalling the score for this round
		let sessionScore = 0;

		// Calculate scores
		for ( let tile of tilesToDestroy )
		{
			this.scoreTotals.tiles.total ++;

			// If not initialized, do it
			if ( typeof(this.scoreTotals.tiles.keys[tile.key]) === 'undefined' )
			{
				this.scoreTotals.tiles.keys[tile.key] = 0;
			}

			// Increase the count for the key
			this.scoreTotals.tiles.keys[tile.key] ++;

			// TODO: For tile multipliers where 2x the tiles/wildcards and such, do more fine-detailed checking... if a 2x tile was used then increase the color, etc.

			// Start score multiplier checks, use iteration set in map
			let scoreMultiplier = destroyTilesMap[this.getTileRow(tile)][this.getTileCol(tile)];

			sessionScore += 1 * scoreMultiplier;
		}
		
		// Score with the number of tiles matched or change it to a number like 10 for each match base amount
		this.score += sessionScore;

		// Update scoreTotals
		this.scoreTotals.score += sessionScore;

		// Update score
		this.updateScore();

		this.destroyTiles(destroyTilesMap);
	}

	updateScore ()
	{
		// Update score
		$('.score').html(this.score);
	}

    destroyTiles ( tilesMap )
	{
		if ( typeof(tilesMap) === 'undefined' )
		{
			tilesMap = this.matchedTilesMap;
		}

        this.numTilesToDestroy = 0;

        for ( let i = 0; i < this.numRows; i ++ )
		{
            for ( let j = 0; j < this.numCols; j ++ )
			{
				// If tile not matched, skip
                if ( tilesMap[i][j] == 0 )
				{
					continue;
				}

				this.numTilesToDestroy ++;

				// If debug
				if ( this.debug )
				{
					console.log(`destroyTiles: Destroying ${i},${j} ${tilesMap[i][j]}`);
				}

				// Mark as empty
				this.gameArray[i][j].isEmpty = true;

				const tile = this.getTileAt(i, j);
				this.tweenDestroy(tile, {recursiveIteration: tilesMap[i][j]});
            }
        }
    }

	tweenDestroy ( tile, params )
	{
		// https://greensock.com/forums/topic/21306-creating-and-maintaining-an-animation-queue/
		const $tile = this.getDisplayTile(tile);
		const ease = 'circ.out';
		const hue = 360;
		gsap.to($tile, {
			autoRound: false,
			// Offset for things like explosions in sequence that were found over recursive checks
			delay: (params.recursiveIteration - 1) * 0.075,
			// Do some randomizing to make it more interesting
			duration: (this.transitionMs / 1000) * (RL.u.getRandomInt(80, 100) / 100),
			ease: ease,
			//scale: 0.1,
			// Do some randomizing to make it more interesting
			//rotation: RL.u.getRandomInt(-360, 360),
			//opacity: 0.0,

			
			autoAlpha: 0,
			force3D: true,
			backgroundColor: "hsl(" + hue + ",80%," + RL.u.getRandomFloat(50, 70) + "%)",
			scale: Math.random() < 0.8 ? 0 : RL.u.getRandomFloat(1.1, 1.3),
			rotationX: RL.u.getRandomFloat(-180, 180),
			rotationY: RL.u.getRandomFloat(-180, 180),
			rotationZ: RL.u.getRandomFloat(-180, 180),
			xPercent: RL.u.getRandomFloat(-100, 100),
			yPercent: RL.u.getRandomFloat(-100, 100),
			
			//top: (this.tileHeight * this.numRows)
			onComplete: () => {
				this.tweenDestroyEnd(tile);
			}
		});
	}

	tweenDestroyEnd ( tile )
	{
		this.numTilesToDestroy --;
		
		// If debug
		if ( this.debug )
		{
			console.log(`tweenDestroyEnd: ${this.numTilesToDestroy} more to destroy`);
		}

		this.numTilesDestroyed ++;

		const $tile = this.getDisplayTile(tile);
		$tile.remove();

		// If still destroying, stop
		if ( this.numTilesToDestroy > 0 )
		{
			return;
		}

		this.makeTilesFall();
		this.replenishField();
	}

    makeTilesFall ()
	{
		// If debug
		if ( this.debug )
		{
			console.log(`makeTilesFall`);
		}
		
		this.numTilesToFall = 0;

		// Go from bottom to top and move tiles down as necessary

		// For each row, starting from next to last
        for ( let i = this.numRows - 2; i >= 0; i -- )
		{
			// For each col
            for ( let j = 0; j < this.numCols; j ++ )
			{
				// If empty, skip
                if ( this.gameArray[i][j].isEmpty )
				{
					continue;
				}

				let fallTiles = this.holesBelow(i, j);

				// If no empty tiles below, skip
				if ( fallTiles == 0 )
				{
					continue;
				}

				this.numTilesToFall ++;

				// From is the tile that needs to fall
				let fallTile = this.gameArray[i][j];
				// To is the empty tile
				const emptyRow = i + fallTiles;
				let emptyTile = this.gameArray[emptyRow][j];
				
				// Swap tile properties
				// Popuplate the tile below with this one
				const newTile = this.cleanCopyTileCustomProps(fallTile, emptyTile);
				newTile.isEmpty = false;
				this.gameArray[emptyRow][j] = newTile;

				// Make the above tile empty now
				fallTile.isEmpty = true;

				this.tweenFall(fallTile, emptyRow, j);
            }
        }
    }

	tweenFall ( fallTile, toRow, toCol )
	{
		const ease = 'circ.out';
		// Turn the from tile into the to tile
		const $fallTile = this.getDisplayTile(fallTile)
		.attr({
			[this.tileRowAttributeName]: toRow,
			[this.tileColumnAttributeName]: toCol
		});
		
		gsap.to($fallTile, {
			autoRound: false,
			duration: (this.transitionMs / 1000) * 0.5,
			ease: ease,
			opacity: 1.0,
			top: this.tileHeight * toRow,
			onComplete: () => {
				this.tweenFallEnd(fallTile);
			}
		});
	}

	tweenFallEnd ( tile )
	{
		this.numTilesToFall --;

		// If debug
		if ( this.debug )
		{
			console.log(`tweenFallEnd: ${this.numTilesToFall} more to fall`);
		}
	}
	
    replenishField()
	{
		// If debug
		if ( this.debug )
		{
			console.log(`replenishField`);
		}

		this.numTilesToReplinish = 0;

		// For every column num
        for ( let j = 0; j < this.numCols; j ++)
		{
			// Go through all rows and find how many holes are in the column
            let emptySpots = this.holesInCol(j);

			// If no empty spots, skip
            if ( emptySpots == 0 )
			{
				continue;
			}

			for ( let i = 0; i < emptySpots; i ++ )
			{
				this.numTilesToReplinish ++;

				const tile = this.createTile(i, j);
				this.gameArray[i][j] = tile;

				this.tweenReplenish(tile, emptySpots - i);
			}
        }
    }

	tweenReplenish ( tile, replenishIteration )
	{
		// Create a new display tile
		const $tile = this.createDisplayTile(tile)
		.css({
			top: - this.tileHeight * replenishIteration,
			left: this.tileWidth * this.getTileCol(tile)
		});

		const ease = 'circ.out';
		
		gsap.to($tile, {
			autoRound: false,
			duration: (this.transitionMs / 1000) * 0.5,
			ease: ease,
			top: this.tileHeight * this.getTileRow(tile),
			backgroundColor: tile.color,
			opacity: 1.0,
			onComplete: () => {
				this.tweenReplenishEnd(tile);
			}
		});
	}

	tweenReplenishEnd ( tile )
	{
		this.numTilesToReplinish --;

		// If debug
		if ( this.debug )
		{
			console.log(`tweenReplenishEnd: ${this.numTilesToReplinish} more to replinish`);
		}

		// If still replenising, stop
		if ( this.numTilesToReplinish > 0 )
		{
			return;
		}

		// If matches exist, handle them and stop
		const boardMatches = this.findBoardMatches();
		if ( boardMatches.length > 0 )
		{
			// If debug
			if ( this.debug )
			{
				console.log(`Matches found after fall and replenish`);
			}
			
			// Delay match check
			setTimeout(() => {
				this.handleMatches(boardMatches);
			}, 250);
			return;
		}

		this.endTurn();
	}

	findBoardMatches ()
	{
		// Initialize a map of the board for matched tiles
        this.matchedTilesMap = this.createEmptyTileMap();

		// Find all matching tiles on the board
		let boardMatches = [];
		
		// For every row
        for ( let row = 0; row < this.numRows; row ++ )
		{
			// For every column
            for ( let col = 0; col < this.numCols; col ++ )
			{
				// If the tile has already been processed, skip
				if ( this.matchedTilesMap[row][col] > 0 )
				{
					continue;
				}

				// Find matches
				const tileMatches = this.findTileMatches(row, col);
				// If no matches found, skip
				if ( tileMatches.num == 0 )
				{
					continue;
				}

				// Add matches
				boardMatches = boardMatches.concat(tileMatches);

				// Update matched tiles map
				for ( const matchTile of tileMatches.tiles )
				{
					const matchTileRow = this.getTileRow(matchTile);
					const matchTileCol = this.getTileCol(matchTile);
					this.matchedTilesMap[matchTileRow][matchTileCol] = 1;
				}
			}
		}

		return boardMatches;
	}

	/**
	 * 
	 * Existing

		00Y00
		00X00
		YX1XY
		00X00
		00Y00

		3 matching in a direction (middle, top and bottom or left and right ends potential fills)
		6 potential fills, middle vertical/horizontal, top/bottom vertical, left/right horizontal)

		Check for extended reach and Ts

		Record tiles as a set
	 * 
	 * @param {*} row 
	 * @param {*} col 
	 */
	findTileMatches ( row, col )
	{
		const matchSet = {
			axis: {h: false, v: false},
			// Intersecting matches will have an hNum and vNum total greater than total num tiles
			axisMaxNum: 0,
			hNum: 0,
			num: 0,
			tiles: [],
			vNum: 0
		};

		// Initial matches are based off of the passed tile
		const vMatches = this.findVerticalTileMatches(row, col);
		const hMatches = this.findHorizontalTileMatches(row, col);
		matchSet.num = vMatches.num + hMatches.num;

		// If no tiles matched, stop
		if ( matchSet.num == 0 )
		{
			return matchSet;
		}

		// Continue updating initial match data
		matchSet.axis = Object.assign(matchSet.axis, vMatches.axis, hMatches.axis);
		matchSet.vNum = vMatches.num;
		matchSet.hNum = hMatches.num;
		// Before returning results, filter out duplicates from intersecting matches
		matchSet.tiles = vMatches.tiles.concat(hMatches.tiles);

		// If vertical and horizontal matches were both found, this was the intersecting tile, so no more searching is necessary
		// Else, check every tile for any branching match
		if ( vMatches.num == 0 || hMatches.num == 0 )
		{
			// If vertical matches
			if ( vMatches.tiles.length > 0 )
			{
				// Find any horizontal matches from vertical matches
				for ( let matchTile of vMatches.tiles )
				{
					// If debug
					if ( this.debug )
					{
						//console.log('Checking vMatches.tiles for additional matches');
					}
					
					const matchTileRow = this.getTileRow(matchTile);
					const matchTileCol = this.getTileCol(matchTile);
					// If initially passed tile, skip
					if ( matchTileRow == row && matchTileCol == col )
					{
						continue;
					}
	
					const additionalMatches = this.findHorizontalTileMatches(matchTileRow, matchTileCol);
					// If additional matches found
					if ( additionalMatches.num > 0 )
					{
						matchSet.axis = Object.assign(matchSet.axis, additionalMatches.axis);
						// Do not include matchTile in additional results
						matchSet.num += additionalMatches.num - 1;
						matchSet.hNum += additionalMatches.num;
						matchSet.tiles = matchSet.tiles.concat(additionalMatches.tiles);
					}
				}
			}

			// If horizontal matches
			if ( hMatches.tiles.length > 0 )
			{
				// Find any vertical matches from horizontal matches
				for ( let matchTile of hMatches.tiles )
				{
					// If debug
					if ( this.debug )
					{
						//console.log('Checking hMatches.tiles for additional matches');
					}
					
					const matchTileRow = this.getTileRow(matchTile);
					const matchTileCol = this.getTileCol(matchTile);
					// If initially passed tile, skip
					if ( matchTileRow == row && matchTileCol == col )
					{
						continue;
					}

					const additionalMatches = this.findVerticalTileMatches(matchTileRow, matchTileCol);
					// If additional matches found
					if ( additionalMatches.num > 0 )
					{
						matchSet.axis = Object.assign(matchSet.axis, additionalMatches.axis);
						// Do not include matchTile in additional results
						matchSet.num += additionalMatches.num - 1;
						matchSet.vNum += additionalMatches.num;
						matchSet.tiles = matchSet.tiles.concat(additionalMatches.tiles);
					}
				}
			}
		}

		// Set the max num tiles matched on an axis
		matchSet.axisMaxNum = Math.max(matchSet.hNum, matchSet.vNum);

		// Remove duplicate tiles included from intersecting matches
		// https://stackoverflow.com/questions/2218999/how-to-remove-all-duplicates-from-an-array-of-objects
		matchSet.tiles = matchSet.tiles.filter((value, index, self) =>
			index === self.findIndex((t) => 
				(t.row === value.row && t.col === value.col)
			)
		);

		return matchSet;
	}

	/**
	 * Check axis match tiles
	 * 
	 * @param {*} row 
	 * @param {*} col 
	 * @returns 
	 */
	findVerticalTileMatches ( row, col )
	{
		return this.findAxisTileMatches(row, col, this.VERTICAL);
	}

	/**
	 * Check axis match tiles
	 * 
	 * @param {*} row 
	 * @param {*} col 
	 * @returns 
	 */
	findHorizontalTileMatches ( row, col )
	{
		return this.findAxisTileMatches(row, col, this.HORIZONTAL);
	}

	/**
	 * Check axis match tiles
	 * 
	 * @param {*} row 
	 * @param {*} col 
	 * @returns 
	 */
	findAxisTileMatches ( row, col, axis )
	{
		const isVertical = (axis === this.VERTICAL);
		const tile = this.getTileAt(row, col);
		let tilesMatched = [];
		let numTilesMatched = 0;

		// If debug
		if ( this.debug )
		{
			//console.log(`findAxisTileMatches: ${isVertical ? 'Vertical' : 'Horizontal'}`);
		}

		const stationaryValue = isVertical ? col : row;
		const dynamicValue = isVertical ? row : col;

		// Check up or left
		//for ( let checkRow = row - 1; checkRow >= 0; checkRow -- )
		//for ( let checkCol = col - 1; checkCol >= 0; checkCol -- )
		for ( let checkValue = dynamicValue - 1; checkValue >= 0; checkValue -- )
		{
			//this.getTileAt(checkRow, col);
			//this.getTileAt(row, checkCol);
			// If no match, stop
			const checkRow = isVertical ? checkValue : stationaryValue;
			const checkCol = isVertical ? stationaryValue : checkValue;
			
			// If debug
			if ( this.debug )
			{
				//console.log(`findAxisTileMatches: Checking ${isVertical ? 'up' : 'left'} at ${checkRow},${checkCol}`);
			}

			const tmpTile = this.getTileAt(checkRow, checkCol);
			
			// If debug
			if ( this.debug )
			{
				//console.log(`findAxisTileMatches: Comparing ${tmpTile.key} to ${tile.key}`);
			}

			// If colors don't match, stop
			if ( tmpTile.key !== tile.key )
			{
				break;
			}

			numTilesMatched ++;
			tilesMatched.push(tmpTile);
		}
		
		// Check down or right
		//for ( let checkRow = row + 1; checkRow < this.numRows; checkRow -- )
		//for ( let checkCol = col + 1; checkCol < this.numCols; checkCol -- )
		const maxCheckValue = isVertical ? this.numRows : this.numCols;
		for ( let checkValue = dynamicValue + 1; checkValue < maxCheckValue; checkValue ++ )
		{
			// If no match, stop
			const checkRow = isVertical ? checkValue : stationaryValue;
			const checkCol = isVertical ? stationaryValue : checkValue;
			
			// If debug
			if ( this.debug )
			{
				//console.log(`findAxisTileMatches: Checking ${isVertical ? 'down' : 'right'} at ${checkRow},${checkCol}`);
			}

			const tmpTile = this.getTileAt(checkRow, checkCol);
			
			// If debug
			if ( this.debug )
			{
				//console.log(`findAxisTileMatches: Comparing ${tmpTile.key} to ${tile.key}`);
			}

			// If keys don't match, stop
			if ( tmpTile.key !== tile.key )
			{
				break;
			}

			numTilesMatched ++;
			tilesMatched.push(tmpTile);
		}
		
		const matches = {
			axis: {},
			hNum: 0,
			num: 0,
			tiles: [],
			vNum: 0
		};

		// If min matching tiles found
		if ( numTilesMatched >= 2 )
		{
			matches.axis = (isVertical ? {v: true} : {h: true});
			// Add self to tiles matched
			matches.num = numTilesMatched + 1;
			matches.hNum = (isVertical ? 0 : matches.num);
			matches.vNum = (isVertical ? matches.num : 0);
			tilesMatched.unshift(tile);
			console.log(tilesMatched);
			matches.tiles = tilesMatched;
		}

		return matches;
	}

	findPotentialBoardMoves ()
	{
		let boardMoves = [];
		
		// For every row
        for ( let row = 0; row < this.numRows; row ++ )
		{
			// For every column
            for ( let col = 0; col < this.numCols; col ++ )
			{
				// Find potential moves for matches
				const tileMoves = this.findPotentialTileMoves(row, col);
				boardMoves = boardMoves.concat(tileMoves);
			}
		}

		return boardMoves;
	}

	/**
	 * 
	 * Potential

		Check total

		000Y000
		00YXY00
		0YX0XY0
		YX010XY
		0YX0XY0
		00YXY00
		000Y000

		16 patterns, 4 in each direction (middle, top and bottom and/or left and right combinations like bottom, left, and right potential fills if tile moves up)

	 * 
	 * @param {*} row 
	 * @param {*} col 
	 */
	findPotentialTileMoves ( row, col )
	{
		const moveTemplate = {
			axis: {h: false, v: false},
			direction: '',
			num: 0,
			tile: '',
			toCol: '',
			toRow: ''
		};
		const potentialMoves = [];
		let move;

		// If can move up, check new position
		move = this.findPotentialAxisTileMoves(row, col, this.VERTICAL, this.UP);
		// If there was a match
		if ( move.num > 0 )
		{
			potentialMoves.push(move);
		}

		// If can move down, check new position
		move = this.findPotentialAxisTileMoves(row, col, this.VERTICAL, this.DOWN);
		// If there was a match
		if ( move.num > 0 )
		{
			potentialMoves.push(move);
		}

		// If can move left, check new position
		move = this.findPotentialAxisTileMoves(row, col, this.HORIZONTAL, this.LEFT);
		// If there was a match
		if ( move.num > 0 )
		{
			potentialMoves.push(move);
		}
		
		// If can move right, check new position
		move = this.findPotentialAxisTileMoves(row, col, this.HORIZONTAL, this.RIGHT);
		// If there was a match
		if ( move.num > 0 )
		{
			potentialMoves.push(move);
		}

		return potentialMoves;
	}

	findPotentialAxisTileMoves ( row, col, axis, swapDirection )
	{
		const moveTemplate = {
			axis: {h: false, v: false},
			swapDirection: '',
			num: 0,
			tile: '',
			toCol: '',
			toRow: ''
		};
		const move = RL.u.deepClone(moveTemplate);

		const isVertical = (axis === this.VERTICAL);
		const isUpOrLeft = (swapDirection === this.UP || swapDirection === this.LEFT);
		const stationaryValue = isVertical ? col : row;
		const dynamicValue = isVertical ? row : col;
		const minDynamicValue = 0;
		const maxDynamicValue = isVertical ? this.numRows : this.numCols;
		const minStationaryValue = 0;
		const maxStationaryValue = isVertical ? this.numCols : this.numRows;

		// If moving in the swapDirection goes out of bounds, stop
		if ( 
			( isUpOrLeft && dynamicValue - 1 < minDynamicValue ) ||
			( ! isUpOrLeft && dynamicValue + 1 >= maxDynamicValue )
		)
		{
			return move;
		}

		const tile = this.getTileAt(row, col);

		// If min spaces for end match available
		if ( dynamicValue + (isUpOrLeft ? -3 : 3) < maxDynamicValue )
		{
			// Check axis end match
			let numTilesMatched = 1; // Start with self
			let startCheckValue = dynamicValue + (isUpOrLeft ? -2 : 2);

			if ( isUpOrLeft )
			{
				// Check axis end match tiles
				for ( let checkValue = startCheckValue; checkValue >= minDynamicValue; checkValue -- )
				{
					const checkRow = isVertical ? checkValue : stationaryValue;
					const checkCol = isVertical ? stationaryValue : checkValue;
					
					// If no match, stop
					if ( this.getTileAt(checkRow, checkCol).key !== tile.key )
					{
						break;
					}

					numTilesMatched ++;
				}
			}
			else
			{
				// Check axis end match tiles
				for ( let checkValue = startCheckValue; checkValue < maxDynamicValue; checkValue ++ )
				{
					const checkRow = isVertical ? checkValue : stationaryValue;
					const checkCol = isVertical ? stationaryValue : checkValue;
					
					// If no match, stop
					if ( this.getTileAt(checkRow, checkCol).key !== tile.key )
					{
						break;
					}

					numTilesMatched ++;
				}
			}

			// If min matching tiles found
			if ( numTilesMatched >= 3 )
			{
				// If debug
				if ( this.debug )
				{
					console.log(`If move ${swapDirection}, axis match: ${row},${col}`);
				}

				move.axis[(isVertical ? 'v' : 'h')] = true;
				move.num += numTilesMatched;
			}
		}
		
		// Check perpendicular axis middle or end match
		let numTilesMatched = 1; // Start with self
		let startCheckValue = dynamicValue + (isUpOrLeft ? -1 : 1);

		// For each row above or column to the left
		for ( let checkValue = stationaryValue - 1; checkValue >= minStationaryValue; checkValue -- )
		{
			const checkRow = isVertical ? startCheckValue : checkValue;
			const checkCol = isVertical ? checkValue : startCheckValue;
			
			// If no match, stop
			if ( this.getTileAt(checkRow, checkCol).key !== tile.key )
			{
				break;
			}

			numTilesMatched ++;
		}

		// For each row below or column to the right
		for ( let checkValue = stationaryValue + 1; checkValue < maxStationaryValue; checkValue ++ )
		{
			const checkRow = isVertical ? startCheckValue : checkValue;
			const checkCol = isVertical ? checkValue : startCheckValue;
			
			// If no match, stop
			if ( this.getTileAt(checkRow, checkCol).key !== tile.key )
			{
				break;
			}

			numTilesMatched ++;
		}

		// If min matching tiles found
		if ( numTilesMatched >= 3 )
		{
			// If debug
			if ( this.debug )
			{
				console.log(`If move ${swapDirection}, perpendicular axis match: ${row},${col}`);
			}
			
			move.axis[(isVertical ? 'h' : 'v')] = true;
			move.num += numTilesMatched;
		}

		// If there was a match
		if ( move.num > 0 )
		{
			move.swapDirection = swapDirection;
			move.tile = tile;
			move.toRow = (isVertical ? dynamicValue + (isUpOrLeft ? -1 : 1) : stationaryValue);
			move.toCol = (isVertical ? stationaryValue : dynamicValue + (isUpOrLeft ? -1 : 1));
		}
		
		return move;
	}

	refreshInteraction ()
	{
		// If debug
		if ( this.debug )
		{
			console.log('refreshInteraction');
		}

		if ( this.isInteractionAllowed() )
		{
			this.allowInteraction();
		}
		else
		{
			this.preventInteraction();
		}
	}

	// Instead of 1 flag set, check a series of conditions
	isInteractionAllowed ()
	{
		// If debug
		if ( this.debug )
		{
			console.log('isInteractionAllowed');
		}

		let interactionAllowed = true;

		// If game is active
		if ( this.gameState !== 'playing' )
		{
			// If debug
			if ( this.debug )
			{
				console.log(`isInteractionAllowed: Game not being played ${this.gameState}`);
			}
	
			interactionAllowed = false;
		}

		// If player's turn
		if ( this.turn !== 'player' )
		{
			// If debug
			if ( this.debug )
			{
				console.log(`isInteractionAllowed: Not player's turn ${this.turn}`);
			}
	
			interactionAllowed = false;
		}

		// If tiles still processing, stop
		if ( this.numTilesProcessing > 0 )
		{
			// If debug
			if ( this.debug )
			{
				console.log(`isInteractionAllowed: Tiles still processing`);
			}
	
			interactionAllowed = false;
		}

		return interactionAllowed;
	}

	get numTilesProcessing ()
	{
		return this.numTilesToSwap + this.numTilesToDestroy + this.numTilesToFall + this.numTilesToReplinish + this.numTilesToReset;
	}

	allowInteraction ( force )
	{
		// If debug
		if ( this.debug )
		{
			console.log('allowInteraction');
		}

		// Safely set the interaction flag
		if ( force || this.isInteractionAllowed() )
		{
			this.canInteract = true;
		}
	}

	preventInteraction ()
	{
		// If debug
		if ( this.debug )
		{
			console.log('preventInteraction');
		}

		this.canInteract = false;
	}

	resetInteraction ( force )
	{
		// If debug
		if ( this.debug )
		{
			console.log('resetInteraction');
		}

		this.allowInteraction(force);
		this.startInactivityTimer();
		// Clear selections
		this.selectedTile = null;
		this.fromTile = null;
		this.toTile = null;
	}

	// https://www.javascripttutorial.net/es6/javascript-getters-and-setters/
	get canInteract ()
	{
		return this._canInteract;
	}

	set canInteract ( value )
	{
		this._canInteract = value;
		return value;
	}

	startTurn ()
	{
		// If debug
		if ( this.debug )
		{
			console.log(`startTurn`);
		}
		
		// Event for start of turn
		this.dispatchEvent('startTurn');
		
		this.resetInteraction();

		const potentialBoardMoves = this.findPotentialBoardMoves();

		// If no more moves available, refresh board
		if ( potentialBoardMoves.length == 0 )
		{
			console.log('No more moves available. Reset board or end game.');
			this.resetBoard(true);
			return;
		}

		// Sort ascending by number of tiles in the match
		// https://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value
		//Strings//objs.sort((a, b) => a.last_nom.localeCompare(b.last_nom));
		//Numeric//objs.sort((a, b) => a.value - b.value);
		potentialBoardMoves.sort((a, b) => a.num - b.num);

		const lastMoveOptionIndex = potentialBoardMoves.length - 1;

		// If the first and last options have the same # of tiles, pick random
		if ( potentialBoardMoves[0].num == potentialBoardMoves[lastMoveOptionIndex].num )
		{
			this.suggestedMove = RL.u.randomFromArray(potentialBoardMoves);
		}
		// Else, the best move has the most tiles and is the last sorted option
		else
		{
			this.suggestedMove = potentialBoardMoves[lastMoveOptionIndex];
		}

		// Start inactivity timer
		this.startInactivityTimer();
		
		// Make the game play itself
		//this.doSuggestedMove();
	}

	doMove ( type = 'suggested' )
	{
		this.handleStartInteraction();

		if ( type === 'suggested' )
		{
			this.swapTiles(this.suggestedMove.tile, this.getTileAt(this.suggestedMove.toRow, this.suggestedMove.toCol), true);
		}
	}

	handleStartInteraction ()
	{
		this.stopInactivity();
	}

	handleStopInteraction ()
	{
	}

	startInactivityTimer ()
	{
		// If debug
		if ( this.debug )
		{
			console.log(`startInactivityTimer`);
		}

		this.stopInactivityTimer();

		this.inactivityTimer = setTimeout(() => {
			this.handleInactivity();
		}, this.inactivityTimeoutMs);
	}

	stopInactivityTimer ()
	{
		// If debug
		if ( this.debug )
		{
			console.log(`stopInactivityTimer`);
		}

		// If an inactivity timer exists, stop it
		if ( this.inactivityTimer )
		{
			clearTimeout(this.inactivityTimer);
			this.inactivityTimer = null;
			return true;
		}
	}

	stopInactivity ()
	{
		// If debug
		if ( this.debug )
		{
			console.log(`stopInactivity`);
		}

		this.stopInactivityTimer();
		// Stop and remove any hints
		this.destroyTileHint();
	}

	handleInactivity ()
	{
		// If debug
		if ( this.debug )
		{
			console.log(`handleInactivity`);
		}

		// If a hint doesn't already exist, create one
		if ( this.autoHint && ! this.hintTile )
		{
			this.createHint();
		}
	}

	createHint ( move )
	{
		// Stop and remove any hints
		this.destroyTileHint();
		// Track if a hint has been created or not, possibly to change scoring
		this.hintTile = ( typeof(move) === 'undefined' ) ? this.suggestedMove : move;
		// Create tile hint
		this.createTileHint(this.hintTile);
		this.hintGiven = true;
		
		// Update scoreTotals
		this.scoreTotals.numHintsGiven ++;
	}

	createTileHint ( move )
	{
		// If debug
		if ( this.debug )
		{
			console.log(`createTileHint`, move);
		}

		// Create tile hint
		const $randomMatchTile = this.createDisplayTile(move.tile)
		.addClass(`${this.tileHintClassName} pe-none`);

		const ease = 'circ.inout';
		gsap.to($randomMatchTile, {
			autoRound: false,
			duration: (this.transitionMs / 1000) * 3,
			ease: ease,
			top: this.tileHeight * move.toRow,
			left: this.tileWidth * move.toCol,
			backgroundColor: '#FFFFFF',//move.color,
			opacity: 0.5,
			repeat: -1,
			onComplete: () => {
			}
		});
	}

	destroyTileHint ()
	{
		// If debug
		if ( this.debug )
		{
			console.log(`destroyTileHint`);
		}

		// Remove any displayed hints
		const $hintTile = $(`.${this.tileHintClassName}`);

		if ( ! $hintTile.length )
		{
			return;
		}

		const ease = 'circ.out';
		gsap.to($hintTile, {
			autoRound: false,
			duration: (this.transitionMs / 1000) * 0.5,
			ease: ease,
			autoAlpha: 0,
			onComplete: () => {
				$hintTile.remove();
				this.hintTile = null;
			}
		});
	}

	endTurn ()
	{
		// If debug
		if ( this.debug )
		{
			console.log(`endTurn`);
		}

		// If tiles still processing, stop
		if ( this.numTilesProcessing > 0 )
		{
			// If debug
			if ( this.debug )
			{
				console.log(`endTurn: Tiles still processing, don't end turn`);
			}
	
			return;
		}

		// Clear hints
		this.hintGiven = false;
		// Update scoreTotals
		this.scoreTotals.numConsecutiveMatches += this.numConsecutiveMatches;
		this.scoreTotals.numConsecutiveMatchChecks += this.numConsecutiveMatchChecks;
		this.scoreTotals.maxConsecutiveMatches = Math.max(this.scoreTotals.maxConsecutiveMatches, this.numConsecutiveMatchChecks);

		// Event for end of turn
		this.dispatchEvent('endTurn');

		this.startTurn();
	}

	/**
	 * 
	 * 
		document.addEventListener('MyPrefix__myEventName', (event) => {
			// Actions
		});
	 * 
	 * @param {*} eventName 
	 * @param {*} eventData 
	 */
	dispatchEvent ( eventName, eventData )
	{
		// If debug
		if ( this.debug )
		{
			console.log(`dispatchEvent: ${eventName}`, eventData);
		}

		// Dispatch custom event
		const event = new CustomEvent(this.eventNamePrefix + eventName, {
			bubbles: true,
			cancelable: true,
			detail: eventData
		});
		document.dispatchEvent(event);
	}

    holesBelow ( row, col )
	{
        let result = 0;

		// For each row, starting below the passed row argument
        for ( let i = row + 1; i < this.numRows; i ++ )
		{
			// If empty, increase count
            if ( this.gameArray[i][col].isEmpty )
			{
                result ++;
            }
        }

        return result;
    }

    holesInCol ( col )
	{
        var result = 0;

		// For each row
        for ( let i = 0; i < this.numRows; i ++ )
		{
			// If the passed col argument of the row is empty, increase counts
            if ( this.gameArray[i][col].isEmpty )
			{
                result ++;
            }
        }

        return result;
    }

	createEmptyTileMap ( defaultValue = 0 )
	{
		// Initialize a map of the board
		let tileMap = [];

		// Generate the map of items to remove
		for ( let row = 0; row < this.numRows; row ++ )
		{
			tileMap[row] = [];

			for ( let col = 0; col < this.numCols; col ++ )
			{
				tileMap[row][col] = defaultValue;
			}
		}

		return tileMap;
	}

	getTileMapFromRadiusAroundTile ( tile, radius = 1, tileMap, recursiveIteration = 1 )
	{
		// If a tileMap was not passed recursively, start a new one
		if ( typeof(tileMap) === 'undefined' )
		{
			// Initialize a map of the board for matched tiles
			tileMap = this.createEmptyTileMap();
		}

		const tileRow = this.getTileRow(tile);
		const tileCol = this.getTileCol(tile);

		// Add initial tile
		tileMap[tileRow][this.getTileCol(tile)] = recursiveIteration;
		// Track more tile found in radius that need added to expand selection
		let compoundingTiles = [];

		// Go around tile
		
		// Start with the row at the edge of the blast radius
		for ( let row = tileRow - radius; row <= tileRow + radius; row ++ )
		{
			// If outside the game baoundary, skip
			if ( row < 0 || row > this.numRows - 1 )
			{
				continue;
			}

			// Start with the col at the edge of the blast radius
			for ( let col = this.getTileCol(tile) - radius; col <= this.getTileCol(tile) + radius; col ++ )
			{
				// If outside the game baoundary, skip
				if ( col < 0 || col > this.numCols - 1 )
				{
					continue;
				}

				// If already process recursively, skip
				if ( tileMap[row][col] )
				{
					continue;
				}
			
				tileMap[row][col] = recursiveIteration;

				// For bombs, expand the tiles if other bombs triggered
				const checkTile = this.getTileAt(row, col);
				if ( checkTile.type === 'bomb')
				{
					compoundingTiles.push(checkTile);
					//this.getTileMapFromRadiusAroundTile(tile, radius, tileMap, recursiveIteration + 1);
				}
			}
		}

		// Now check additional tiles
		for ( let compoundingTile of compoundingTiles )
		{
			this.getTileMapFromRadiusAroundTile(compoundingTile, radius, tileMap, recursiveIteration + 1);
		}

		return tileMap;
	}

	getTileMapInDirection ( tile, direction = '+', distance = this.numRows, tileMap, recursiveIteration = 1 )
	{
		// If a tileMap was not passed recursively, start a new one
		if ( typeof(tileMap) === 'undefined' )
		{
			// Initialize a map of the board for matched tiles
			tileMap = this.createEmptyTileMap();
		}

		const tileRow = this.getTileRow(tile);
		const tileCol = this.getTileCol(tile);
		// Add initial tile
		tileMap[tileRow][tileCol] = recursiveIteration;
		// Track more tiles found that need added to expand selection
		let compoundingTiles = [];

		// TODO: Check if running into stone tiles and stop in that direction?
		// TODO: Only go a certain distance

		if ( direction == 'x'  )
		{
			let col;

			// Down-right
			col = tileCol;
			for ( let row = tileRow; row < this.numRows && col < this.numCols; row ++, col ++ )
			{
				const curDistance = Math.max(row - tileRow, col - tileCol);
				if ( curDistance > distance )
				{
					break;
				}
				
				const recursiveIterationDelay = recursiveIteration + curDistance;
				this.getTileMapInDirectionCheck(tileMap, row, col, recursiveIterationDelay, compoundingTiles);
			}

			// Up-right
			col = tileCol;
			for ( let row = tileRow; row >= 0 && col < this.numCols; row --, col ++ )
			{
				const curDistance = Math.max(tileRow - row, col - tileCol);
				if ( curDistance > distance )
				{
					break;
				}
				
				const recursiveIterationDelay = recursiveIteration + curDistance;
				this.getTileMapInDirectionCheck(tileMap, row, col, recursiveIterationDelay, compoundingTiles);
			}
			
			// Down-left
			col = tileCol;
			for ( let row = tileRow; row < this.numRows && col >= 0; row ++, col -- )
			{
				const curDistance = Math.max(row - tileRow, tileCol - col);
				if ( curDistance > distance )
				{
					break;
				}
				
				const recursiveIterationDelay = recursiveIteration + curDistance;
				this.getTileMapInDirectionCheck(tileMap, row, col, recursiveIterationDelay, compoundingTiles);
			}
			
			// Up-left
			col = tileCol;
			for ( let row = tileRow; row >= 0 && col >= 0; row --, col -- )
			{
				const curDistance = Math.max(tileRow - row, tileCol - col);
				if ( curDistance > distance )
				{
					break;
				}
				
				const recursiveIterationDelay = recursiveIteration + curDistance;
				this.getTileMapInDirectionCheck(tileMap, row, col, recursiveIterationDelay, compoundingTiles);
			}
		}

		if ( direction == '+' || direction == '|' )
		{
			// Keep the col the same
			let col = tileCol;

			// Up
			for ( let row = tileRow; row >= 0; row -- )
			{
				const curDistance = tileRow - row;
				if ( curDistance > distance )
				{
					break;
				}
				
				const recursiveIterationDelay = recursiveIteration + curDistance;
				this.getTileMapInDirectionCheck(tileMap, row, col, recursiveIterationDelay, compoundingTiles);
			}

			// Down
			for ( let row = tileRow; row < this.numRows; row ++ )
			{
				const curDistance = row - tileRow;
				if ( curDistance > distance )
				{
					break;
				}
				
				const recursiveIterationDelay = recursiveIteration + curDistance;
				this.getTileMapInDirectionCheck(tileMap, row, col, recursiveIterationDelay, compoundingTiles);
			}
		}
			
		if ( direction == '+' || direction == '-'  )
		{
			// Keep the row the same
			let row = tileRow;

			// Left
			for ( let col = tileCol; col >= 0; col -- )
			{
				const curDistance = tileCol - col;
				if ( curDistance > distance )
				{
					break;
				}
				
				const recursiveIterationDelay = recursiveIteration + curDistance;
				this.getTileMapInDirectionCheck(tileMap, row, col, recursiveIterationDelay, compoundingTiles);
			}
			
			// Right
			for ( let col = tileCol; col < this.numCols; col ++ )
			{
				const curDistance = col - tileCol;
				if ( curDistance > distance )
				{
					break;
				}

				const recursiveIterationDelay = recursiveIteration + curDistance;
				this.getTileMapInDirectionCheck(tileMap, row, col, recursiveIterationDelay, compoundingTiles);
			}
		}

		// Now check additional tiles
		for ( let compoundingTile of compoundingTiles )
		{
			this.getTileMapInDirection(compoundingTile, direction, distance, tileMap, recursiveIteration + 1);
		}

		return tileMap;
	}

	getTileMapInDirectionCheck ( tileMap, row, col, recursiveIteration, compoundingTiles )
	{
		// If already process recursively, skip
		if ( tileMap[row][col] )
		{
			return;
		}

		tileMap[row][col] = recursiveIteration;

		// For bombs, expand the tiles if other bombs triggered
		const checkTile = this.getTileAt(row, col);
		if ( checkTile.type === 'laser')
		{
			compoundingTiles.push(checkTile);
		}

		return true;
	}

}
