<!-- Primary game screen -->
<div class="container-fluid game-screen game-screen--main d-none" id="main-screen">
	<div class="game-screen__wrapper">
		
		<div class="row mb-3 game-screen__title-row">
			<div class="col game-screen__back-col">
				<a data-screen="title" href="#title-screen"><i class="icon-home icon--fa icon--fw"></i></a>
			</div>
			<div class="col-8 game-screen__title-col">
				<h2 class="game-screen__title-heading">Game Title</h2>
			</div>
			<div class="col game-screen__close-col">
				<a class="cta--start-tour" data-tour="main-screen" href=""><i class="fa-solid fa-compass fa-fw"></i></a>
				<!--
				<a class="cta--hud-refresh" href="#title-screen"><i class="icon-refresh icon--fa icon--fw"></i></a>
				-->
			</div>
		</div>
		<div class="row game-screen__player-stats-row">
			<div class="col-sm">
				
				<div class="mb-3 text-center player-stats">
					<span class="player-stat">
						<span class="badge rounded-pill bg-secondary">
							<span class="player-stat__label"><i class="icon-health icon--fa icon--fw"></i></span>
							<span class="player-stat__value"><span class="player-stat-health"></span></span>
						</span>
					</span>
					<span class="player-stat">
						<span class="badge rounded-pill bg-secondary">
							<span class="player-stat__label"><i class="icon-exp icon--fa icon--fw"></i></span>
							<span class="player-stat__value"><span class="player-stat-exp"></span></span>
						</span>
					</span>
					<span class="player-stat">
						<span class="badge rounded-pill bg-secondary">
							<span class="player-stat__label"><i class="icon-special icon--fa icon--fw"></i></span>
							<span class="player-stat__value"><span class="player-stat-special"></span></span>
						</span>
					</span>
					<span class="player-stat">
						<span class="badge rounded-pill bg-secondary">
							<span class="player-stat__label"><i class="icon-currency icon--fa icon--fw"></i></span>
							<span class="player-stat__value"><span class="player-stat-currency"></span></span>
						</span>
					</span>
					<span class="player-stat">
						<span class="badge rounded-pill bg-secondary">
							<span class="player-stat__label"><i class="icon-points icon--fa icon--fw"></i></span>
							<span class="player-stat__value"><span class="player-stat-points"></span></span>
						</span>
					</span>
				</div>

			</div>
		</div>
		<div class="row game-screen__inventory-row">
			<div class="col-sm inventory-col">
				
				<div class="status-container status-container--activated-items d-none">
					<a data-screen="items" href="#items-screen"><i class="icon-activated-item icon--fa icon--fw"></i></a>
					<span class="activated-items-status"></span>
				</div>

			</div>
		</div>
		<div class="game-screen__status-container">
			<div class="row game-screen__status-row">
				<div class="col-sm status-col">

					<div class="status-container status-container--items d-none">
						<a data-screen="items" href="#items-screen"><i class="icon-items icon--fa icon--fw"></i></a>
						<span class="items-status"></span>
					</div>

					<div class="status-container status-container--locations d-none">
						<a data-screen="locations" href="#locations-screen"><i class="icon-map-pin icon--fa icon--fw"></i></a>
						<span class="locations-status"></span>
					</div>

					<div class="status-container status-container--characters d-none">
						<a data-screen="characters" href="#characters-screen"><i class="icon-character icon--fa icon--fw"></i></a>
						<span class="characters-status"></span>
					</div>

					<div class="status-container status-container--challenges d-none">
						<a data-screen="challenges" href="#challenges-screen"><i class="icon-challenges icon--fa icon--fw"></i></a>
						<span class="challenges-status"></span>
					</div>

					<div class="status-container status-container--active-challenges d-none">
						<a data-screen="challenges" href="#challenges-screen"><i class="icon-challenge icon--fa icon--fw"></i></a>
						<ul class="list-group list-group-flush active-challenges-status"></ul>
					</div>

					<div class="status-container status-container--game d-none">
						<i class="icon-game icon--fa icon--fw"></i>
						<span class="game-status"></span>
					</div>

				</div>
			</div>
		</div>

		<div class="game-screen__bottom-row">
			<div class="row">
				<div class="col-sm">
				
					<!-- QRCode Scanning -->
					<div class="qrcode-reader__container">
							
						<div class="overflow-hidden position-relative w-100 video-area">
							<div class="d-flex justify-content-center align-items-center position-absolute w-100 h-100 d-none video-spinner">
								<div class="spinner-border" role="status">
									<span class="visually-hidden">Loading...</span>
								</div>
							</div>

							<video class="d-none"></video>
							<div class="video-area-overlay d-none"></div>
						</div>

						<div class="video-options">
							<div class="d-none video-controls">
								<button class="btn btn-primary d-none video-control cta--toggle-torch" type="button">Light</button>
								<div class="d-none video-control video-control-zoom">
									<label class="form-label">Zoom <span class="setting-value"></span></label>
									<input class="form-range" min="0" max="5" step="0.5" type="range">
								</div>
								<div class="d-none video-control video-control-brightness">
									<label class="form-label">Brightness <span class="setting-value"></span></label>
									<input class="form-range" max="5" min="0" step="1" type="range">
								</div>
							</div>
						</div>
						
						<a class="icon-stop-scan icon--fa game-screen__nav-icon game-screen__nav-icon--stop-scan cta--stop-scan" href="">
							<span class="visually-hidden">Stop Scanning</span>
						</a>

					</div>

					<a class="icon-scan icon--fa game-screen__nav-icon game-screen__nav-icon--start-scan cta--start-scan" href="">
						<span class="visually-hidden">Start Scanning</span>
					</a>

					<!-- QRCode File Uploading -->
					<div class="form-group">
						<label>
							<span class="btn btn-lg text-muted icon-upload icon--fa game-screen__nav-icon--upload-scan"><span class="visually-hidden">Upload Scan</span></span>
							<input accept="image/*" capture class="form-control form-control-file d-none cta--file-decode" type="file">
						</label>
					</div>

				</div>
			</div>
			<div class="row setting--debug-mode">
				<div class="col-sm">
					
					<div class="form-group text-start mb-3">
						<div class="input-group dropup">
							<label class="input-group-text btn btn-primary btn-sm" for="injectPayloadValue"><i class="fa-solid fa-qrcode fa-fw"></i></label>
							<button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-toggle-split rounded-0" data-bs-toggle="dropdown" aria-expanded="false">
								<span class="visually-hidden">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu">
								<li class="payload-dropdown-option payload-dropdown-option--items"><hr class="dropdown-divider"></li>
								<li class="payload-dropdown-option payload-dropdown-option--challenges"><hr class="dropdown-divider"></li>
								<li class="payload-dropdown-option payload-dropdown-option--locations"><hr class="dropdown-divider"></li>
								<li class="payload-dropdown-option payload-dropdown-option--characters"><hr class="dropdown-divider"></li>
							</ul>
							<input aria-describedby="injectPayloadHelp" aria-label="Payload" class="form-control form-control-sm" id="injectPayloadValue" onkeydown="if(event.keyCode==13){document.getElementById('injectPayloadValueButton').click();}" placeholder="Enter JSON object" type="text" value="">
							<button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-toggle-split rounded-0" data-bs-toggle="dropdown" aria-expanded="false">
								<span class="visually-hidden">Toggle Dropdown</span>
							</button>
							<button class="btn btn-primary btn-sm" type="button" id="injectPayloadValueButton" onclick="MyGame.processPayload(document.getElementById('injectPayloadValue').value);return false;">Inject</button>
						</div>
						<small class="form-text text-muted d-none" id="injectPayloadHelp">Payload object to inject.</small>
					</div>


					<div id="itemPayloadButtons"></div>
					<div id="challengePayloadButtons"></div>
					<div id="locationPayloadButtons"></div>
					<div id="characterPayloadButtons"></div>

				</div>
			</div>
			<?php echo $main__componentNav; ?>
		</div>

	</div>
</div>
<!-- Alternate sticky navigation outsite and above main game screen layers -->
<div class="main__sticky-bottom-nav pe-none">
	<?php echo $main__componentNav; ?>
</div>