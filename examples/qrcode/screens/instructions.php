<div class="container-fluid game-screen game-screen--nested game-screen--instructions d-none" id="instructions-screen">
	<div class="game-screen__wrapper">
		
		<div class="row game-screen__title-row">
			<div class="col game-screen__back-col">
				<a class="icon-screen-back icon--fa game-screen__back game-screen__nav-icon cta--prev-screen" href="#main-screen"><span class="visually-hidden">">&lt;</span></a>
			</div>
			<div class="col game-screen__title-col">
				<h2 class="game-screen__title-heading">Instructions</h2>
			</div>
			<div class="col game-screen__close-col">
				<a class="icon-screen-close icon--fa game-screen__close game-screen__nav-icon cta--close-screen" href="#main-screen"><span class="visually-hidden">">&times;</span></a>
			</div>
		</div>
		<div class="row game-screen__content-row">
			<div class="col">

				<!--
				<div id="carouselExampleCaptions" class="carousel carousel-dark slide mb-4" data-bs-ride="false">
					<div class="carousel-indicators">
						<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
						<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
						<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
					</div>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img src="images/backgrounds/nordwood-themes-KcsKWw77Ovw-unsplash.jpg" class="d-block w-100 img-fluid" style="max-height: 30vh;" alt="...">
							<div class="carousel-caption d-none d-md-block">
								<h5>First slide label</h5>
								<p>Some representative placeholder content for the first slide.</p>
							</div>
						</div>
						<div class="carousel-item">
							<img src="images/backgrounds/nordwood-themes-KcsKWw77Ovw-unsplash.jpg" class="d-block w-100 img-fluid" style="max-height: 30vh;" alt="...">
							<div class="carousel-caption d-none d-md-block">
								<h5>Second slide label</h5>
								<p>Some representative placeholder content for the second slide.</p>
							</div>
						</div>
						<div class="carousel-item">
							<img src="images/backgrounds/nordwood-themes-KcsKWw77Ovw-unsplash.jpg" class="d-block w-100 img-fluid" style="max-height: 30vh;" alt="...">
							<div class="carousel-caption d-none d-md-block">
								<h5>Third slide label</h5>
								<p>Some representative placeholder content for the third slide.</p>
							</div>
						</div>
					</div>
					<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="visually-hidden">Previous</span>
					</button>
					<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="visually-hidden">Next</span>
					</button>
				</div>
				-->
				
				<nav class="mt-3">
					<div class="nav nav-tabs nav-pills nav-fill" id="nav-tab" role="tablist">
						<button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Goals</button>
						<button class="nav-link" id="nav-how-tab" data-bs-toggle="tab" data-bs-target="#nav-how" type="button" role="tab" aria-controls="nav-how" aria-selected="true">Playing</button>
						<button class="nav-link" id="nav-tips-tab" data-bs-toggle="tab" data-bs-target="#nav-tips" type="button" role="tab" aria-controls="nav-tips" aria-selected="false">Tips</button>
					</div>
				</nav>
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab" tabindex="0">

						<div class="pt-3">
							<p>Your objective is to start challenges and complete them. Complete all challenges and complete the game!</p>
							<p>You interact through prompts on your device and in the real world.</p>
							<p>Some challenges will have you finding items, finding them in a correct order, and finding them in a time limit. You may need to activate or use items, visit different locations, or even play minigames to complete some challenges.</p>
							<p>You may need to strategize for the best path to take and think your way through riddles.</p>
						</div>
						
					</div>
					<div class="tab-pane fade" id="nav-how" role="tabpanel" aria-labelledby="nav-how-tab" tabindex="0">
						
						<div class="pt-3">
							<p>Return to the title screen with <i class="icon-home icon--fa icon--fw"></i> at the device top corner.</p>
							<p>Keep track of your <i class="icon-health icon--fa icon--fw"></i> Health, <i class="icon-exp icon--fa icon--fw"></i> Experience, <i class="icon-special icon--fa icon--fw"></i> Special, <i class="icon-currency icon--fa icon--fw"></i> Currency, and <i class="icon-points icon--fa icon--fw"></i> Points stats at the top of the device.</p>
							<p>Below stats are status updates and helpful information to progress. Your status will update after actions or you can manually refresh your status with <i class="icon-refresh icon--fa icon--fw"></i> at the device top corner.</p>
							<p>Below your status, capture codes as you find them using <i class="icon-scan icon--fa icon--fw"></i>. If they are stored on your device, upload them using <i class="icon-upload icon--fa icon--fw"></i>.</p>
							<p>At the bottom of the device, you can access your <i class="icon-challenges icon--fa icon--fw"></i> Challenges, <i class="icon-items icon--fa icon--fw"></i> Items, <i class="icon-locations icon--fa icon--fw"></i> Locations, and <i class="icon-characters icon--fa icon--fw"></i> Characters. Numbers <span class="badge bg-secondary rounded-pill">0</span> next to the icons show availability.</p>
							<p>In these screens you can manage and view more detailed information about what you have found. Exit them using back <i class="icon-screen-back icon--fa icon--fw"></i> or close <i class="icon-screen-close icon--fa icon--fw"></i> buttons.</p>
							<p>That's it. Good luck!</p>
						</div>

					</div>
					<div class="tab-pane fade" id="nav-tips" role="tabpanel" aria-labelledby="nav-tips-tab" tabindex="0">
						
						<div class="pt-3">
							
							<ol class="list-group list-group-numbered list-group-flush">
								<li class="list-group-item d-flex justify-content-between align-items-start">
									<div class="ms-2 me-auto">
										<div class="fw-bold">Timed and Ordered Challenges</div>
										You can restart a Challenge by deactivating and reactivating it. This will reset an ordered or timed Challenge.
									</div>
									<span class="badge bg-secondary rounded-pill align-self-center">2</span>
								</li>
							</ol>
							
						</div>

					</div>
				</div>

			</div>
		</div>

	</div>
</div>