<div class="container-fluid game-screen game-screen--nested game-screen--characters d-none" id="characters-screen">
	<div class="game-screen__wrapper">

		<div class="row game-screen__title-row">
			<div class="col game-screen__back-col">
				<a class="icon-screen-back icon--fa game-screen__back game-screen__nav-icon cta--prev-screen" href="#main-screen"><span class="visually-hidden">">&lt;</span></a>
			</div>
			<div class="col game-screen__title-col">
				<h2 class="game-screen__title-heading">Characters</h2>
			</div>
			<div class="col game-screen__close-col">
				<a class="icon-screen-close icon--fa game-screen__close game-screen__nav-icon cta--close-screen" href="#main-screen"><span class="visually-hidden">">&times;</span></a>
			</div>
		</div>
		<div class="row game-screen__content-row">
			<div class="col">
				<ul class="list-group list-group-flush characters-list"></ul>
			</div>
		</div>
		
	</div>
</div>