<div class="container-fluid game-screen game-screen--title active" id="title-screen">
	<div class="game-screen__wrapper">

		<div class="row game-screen__title-row">
			<div class="col game-screen__title-col">
				<h2 class="game-screen__title-heading">Welcome</h2>
			</div>
		</div>
		<div class="row h-100 align-content-center">
			<div class="col">
				
				<div class="d-flex flex-column m-auto" style="width: fit-content;">
					<div class="d-flex flex-column m-auto tour__title-screen-buttons">
						<button class="btn btn-primary btn-lg mb-3 cta--game-start" type="button"><i class="icon-start icon--fa me-2"></i><span>Start</span></button>

						<button class="btn btn-outline-secondary mb-3" data-screen="instructions" href="#instructions-screen" id="instructions-button" type="button"><i class="icon-info icon--fa me-2"></i><span>Instructions</span></button>

						<button class="btn btn-outline-secondary" data-screen="settings" href="#settings-screen" type="button"><i class="icon-cog icon--fa me-2"></i><span>Settings</span></button>
						<button class="btn btn-light btn-sm mt-3 cta--start-tour" data-tour="title-screen" type="button"><i class="fa-solid fa-compass fa-fw me-1"></i>Need a tour?</button>
					</div>
					<hr>
					<div class="d-flex flex-column m-auto tour__enhanced-experience">
						<div class="text-muted fw-bold mb-2 text-center">Enhanced Experience</div>
						<button class="btn btn-outline-secondary btn-sm mb-2 toggle__fullScreen" href="#settings-screen" type="button"><i class="fa-solid fa-maximize fa-fw"></i> Toggle Fullscreen</button>
						<button class="btn btn-outline-secondary btn-sm toggle__checkCameraPermission" href="#settings-screen" type="button"><i class="fa-solid fa-camera fa-fw"></i> Camera Access</button>
						<div class="small text-muted mt-2 motionless__banner"><i class="fa-solid fa-triangle-exclamation fa-fw"></i> Animation disabled by device</div>
					</div>
				</div>

			</div>
		</div>
		<div class="row">
			<div class="col text-end">
				<small class="fst-italic text-muted stat--game-version"></small>
			</div>
		</div>

	</div>
</div>