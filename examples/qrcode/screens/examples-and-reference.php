
<hr>

<div class="container mb-4 mt-4">
	<div class="row">
		<div class="col-sm">
			<h1>Player</h1>
		</div>
	</div>
</div>
<div class="container mb-4 mt-4">
	<div class="row">
		<div class="col-sm">

			<ul>
				<li>Screens
					<ul>
						<li>Welcome/Title</li>
						<li>Instructions</li>
						<li>Settings</li>
						<li>Main</li>
						<li>Challenges</li>
						<li>Inventory/Items</li>
						<li>Locations</li>
						<li>Characters</li>
					</ul>
				</li>
				<li>Player
					<ul>
						<li>Stats, check at a status station
							<ul>
								<li>Health</li>
								<li>Experience</li>
								<li>Special/Magic</li>
								<li>Currency/Gold/Gems</li>
								<li>Points/Score</li>
								<li>Inventory/Backpack
									<ul>
										<li>Found Object</li>
										<li>Equip Object</li>
										<li>Build Object</li>
									</ul>
								</li>
								<li>Rank</li>
							</ul>
						</li>
						<li>Challenges/Quests/Adventures/Tasks
							<ul>
								<li>Choose from available at stations, abondon</li>
								<li>Challenge/Quest/Adventure
									<ul>
										<li>Prerequisites
											<ul>
												<li>Items</li>
												<li>Challenges</li>
											</ul>
										</li>
										<li>Type
											<ul>
												<li>Collect/Search/Find/Hunt/Talk to Item/Location/Character</li>
												<li>... certain # of I/L/C</li>
												<li>... in time limit</li>
												<li>... in order</li>
												<li>... in order timed</li>
												<li>Activate I/L/C</li>
												<li>... with an I/L/C</li>
												<li>Use I/L/C</li>
												<li>Interact with I/L/C</li>
												<li>Solve riddle/puzzle</li>
												<li>Transport/deliver item(s) to location, maybe have to create/build something when there</li>
												<li>Visit/interact with character to trade for or buy an item</li>
												<li>Create/build using items</li>
												<li>Stats levels to grind/farm to</li>
												<li>Combinations of the above</li>
												<li>Mini-game</li>
											</ul>
										</li>
										<li>Description/Riddle/Puzzle</li>
										<li>Clues/Hints</li>
										<li>Objectives</li>
										<li>Progress</li>
										<li>Success or failure
											<ul>
												<li>Reward</li>
											</ul>
										</li>

									</ul>
								</li>
							</ul>
						</li>
						<li>Items/Objects
							<ul>
								<li>Consumables
									<ul>
										<li>Health, etc. for stats</li>
									</ul>
								</li>
								<li>Inventoriable
									<ul>
										<li>Tracked stock, activation, use, etc.</li>
										<li>Types
											<ul>
												<li>Weapon</li>
												<li>Armor</li>
												<li>Clothing</li>
												<li>Spell</li>
												<li>Potion</li>
												<li>Artifact</li>
												<li>Animal</li>
												<li>Food</li>
												<li>Medicine/Health Pack/Healing</li>
												<li>Key</li>
												<li>Gemstone</li>
												<li>Shield</li>
												<li>Helmet</li>
												<li>Shoe</li>
											</ul>
										</li>
									</ul>
								</li>
								<li>Health</li>
								<li>Currency/Gold/Gems</li>
								<li>Weapons/Armor/Clothing</li>
								<li>Clues</li>
								<li>Runes/Artifacts/Crystals</li>
								<li>Combine items to strengthen them or create unique items</li>
								<li>Medals</li>
								<li>Achievements</li>
								<li>Spells, maybe made from different items</li>
								<li>Keys</li>
							</ul>
						</li>
						<li>Actions</li>
						<li>Locations/Destinations</li>
						<li>People/Character/Interactions/Talk/Discussions/Dialogs</li>
						<li>Game Experience
							<ul>
								<li>Video intros and explanations</li>
								<li>Audio</li>
								<li>Art, illustrations, and animations</li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>

		</div>
	</div>
</div>

<div class="container mb-4 mt-4">
	<div class="row">
		<div class="col-sm">
			<h1>Markup Examples</h1>
		</div>
	</div>
</div>

<div class="container-fluid game-screen position-relative">
	<div class="game-screen__wrapper">

		<div class="row game-screen__title-row">
			<div class="col game-screen__back-col">
				<a class="icon-screen-back icon--fa game-screen__back game-screen__nav-icon cta--prev-screen" href="#main-screen"><span class="visually-hidden">">&lt;</span></a>
			</div>
			<div class="col game-screen__title-col">
				<h2 class="game-screen__title-heading">Title</h2>
			</div>
			<div class="col game-screen__close-col">
				<a class="icon-screen-close icon--fa game-screen__close game-screen__nav-icon cta--close-screen" href="#main-screen"><span class="visually-hidden">">&times;</span></a>
			</div>
		</div>

		<div class="row">
			<div class="col">
				
				<ul class="list-group list-group-flush">
					<?php echo $component__listGroupItemTemplate; ?>
					<?php echo $component__listGroupItemActiveTemplate; ?>
				</ul>

				<?php echo $component__withSelectedContainer; ?>
				
			</div>
		</div>

	</div>
</div>

<?php echo $component__modalShowcaseTemplate; ?>

<?php echo $component__modalInfoTemplate; ?>

<div class="container mb-4 mt-4">
	<div class="row">
		<div class="col-sm">

			<h2>Components</h2>

			<h3>component__listGroupItemTemplate</h3>
			<ul class="list-group list-group-flush">
				<?php echo $component__listGroupItemTemplate; ?>
				<?php echo $component__listGroupItemActiveTemplate; ?>
			</ul>

			<h2>Items</h2>

			<h3>item__activatedInventoryItemButtonTemplate</h3>
			<?php echo $item__activatedInventoryItemButtonTemplate; ?>

			<h2>Functionality</h2>

			<div class="container-fluid">
				<div class="row">

					<div class="col">
						<div class="camera-area" id="frontCamera">
							<div class="video-area d-none">
								<div class="ratio ratio-16x9" style="width: 200px;">
									<video></video>
									<canvas class="d-none"></canvas>
								</div>
								<img alt="" class="screenshot-image d-none" style="width: 200px;">
							</div>
							<div class="video-options">
								<button class="btn btn-sm btn-primary cta--get-cameras" type="button">Get Cameras</button>

								<div class="input-group d-none video-camera-select-group">
									<select aria-label="" class="form-select form-select-sm cta--camera-select">
										<option value="">Select camera...</option>
										<option value="1">One</option>
										<option value="2">Two</option>
										<option value="3">Three</option>
									</select>
									<button class="btn btn-sm btn-primary cta--start-stream" type="button">Use Camera</button>
								</div>

								<div class="d-none video-controls">
									<button class="btn btn-secondary d-none cta--pause-video" type="button">Pause</button>
									<button class="btn btn-secondary cta--stop-video" type="button">Stop</button>
									<button class="btn btn-secondary d-none cta--take-screenshot" type="button">Screenshot</button>
									<button class="btn btn-primary d-none cta--toggle-torch" type="button">Torch</button>
									<div class="d-none video-control-zoom">
										<label class="form-label">Zoom <span class="setting-value"></span></label>
										<input class="form-range" min="0" max="5" step="0.5" type="range">
									</div>
									<div class="d-none video-control-brightness">
										<label class="form-label">Brightness <span class="setting-value"></span></label>
										<input class="form-range" max="5" min="0" step="1" type="range">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col">
						<div class="camera-area" id="backCamera">
							<div class="video-area d-none">
								<div class="ratio ratio-16x9" style="width: 200px;">
									<video></video>
									<canvas class="d-none"></canvas>
								</div>
								<img alt="" class="screenshot-image d-none" style="width: 200px;">
							</div>
							<div class="video-options">
								<button class="btn btn-sm btn-primary cta--get-cameras" type="button">Get Cameras</button>

								<div class="input-group d-none video-camera-select-group">
									<select aria-label="" class="form-select form-select-sm cta--camera-select">
										<option value="">Select camera...</option>
										<option value="1">One</option>
										<option value="2">Two</option>
										<option value="3">Three</option>
									</select>
									<button class="btn btn-sm btn-primary cta--start-stream" type="button">Use Camera</button>
								</div>

								<div class="d-none video-controls">
									<button class="btn btn-secondary d-none cta--pause-video" type="button">Pause</button>
									<button class="btn btn-secondary cta--stop-video" type="button">Stop</button>
									<button class="btn btn-secondary d-none cta--take-screenshot" type="button">Screenshot</button>
									<button class="btn btn-primary d-none cta--toggle-torch" type="button">Torch</button>
									<div class="d-none video-control-zoom">
										<label class="form-label">Zoom <span class="setting-value"></span></label>
										<input class="form-range" min="0" max="5" step="0.5" type="range">
									</div>
									<div class="d-none video-control-brightness">
										<label class="form-label">Brightness <span class="setting-value"></span></label>
										<input class="form-range" max="5" min="0" step="1" type="range">
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col">
						<hr>
						<div class="d-flex flex-column align-items-center justify-content-center scanner-area" id="scannerCamera">
							<div class="overflow-hidden position-relative w-100 video-area">
								<div class="d-flex justify-content-center align-items-center position-absolute w-100 h-100 d-none video-spinner">
									<div class="spinner-border" role="status">
										<span class="visually-hidden">Loading...</span>
									</div>
								</div>
								<video class="d-none"></video>
								<div class="video-area-overlay d-none"></div>
							</div>
							
							<div class="d-flex align-items-center">
								<!-- QRCode File Uploading -->
								<div class="form-group ms-2">
									<label>
										<span class="btn btn-lg text-muted"><i class="fa-solid fa-upload fa-fw"></i></span>
										<input accept="image/*" capture class="form-control form-control-file d-none cta--file-decode-example" type="file">
									</label>
								</div>

								<button class="btn btn-lg cta--start-decode-example" type="button"><i class="fa-solid fa-eye fa-fw"></i></button>
								<button class="btn btn-lg cta--stop-decode-example d-none" type="button"><i class="fa-solid fa-eye-slash fa-fw"></i></button>
							</div>

							<div class="video-options">
								<div class="d-none video-controls">
									<button class="btn btn-primary d-none cta--toggle-torch" type="button">Torch</button>
									<div class="d-none video-control-zoom">
										<label class="form-label">Zoom <span class="setting-value"></span></label>
										<input class="form-range" min="0" max="5" step="0.5" type="range">
									</div>
									<div class="d-none video-control-brightness">
										<label class="form-label">Brightness <span class="setting-value"></span></label>
										<input class="form-range" max="5" min="0" step="1" type="range">
									</div>
								</div>
							</div>
						</div>

						
					</div>
				</div>
			</div>

			<script>
				
// Poll with interval until available
(function() {
	let nTimer = setInterval(function() {
		// If available, stop polling
		if (window.jQuery) {
			clearInterval(nTimer);
			//====================


			jQuery(document).ready(function($) {
				$('.cta--get-cameras').on('click', handleGetCamerasClick);
				$('.cta--start-stream').on('click', handleStartStreamClick);
				$('.cta--stop-video').on('click', handleStopVideoClick);
				$('.cta--pause-video').on('click', handlePauseVideoClick);
				$('.cta--take-screenshot').on('click', handleScreenshotClick);
				$('.cta--start-decode-example').on('click', handleStartDecodeClickExample);
				$('.cta--stop-decode-example').on('click', handleStopDecodeClickExample);
				$('.cta--file-decode-example').on('change', handleFileDecodeChangeExample);
				//updateCameraSelection(false, '#frontCamera');
				//updateCameraSelection(false, '#backCamera');
			});


			//====================
			// Continue polling
		}
	}, 100);
})();

const handleFileDecodeChangeExample = async ( event ) =>
{
	if ( event.target.files.length == 0 )
	{
		// No file selected, ignore 
		return;
	}

	const fileInput = event.target;

	const $cameraArea = $('#scannerCamera');
	const cameraAreaId = $cameraArea[0].id;
	const cameraAreaSelector = `#${cameraAreaId}`;
	
	const videoArea = document.querySelector(`${cameraAreaSelector} .video-area`);

	// https://github.com/zxing-js/library/issues/545
	// https://developer.mozilla.org/en-US/docs/Web/API/Barcode_Detection_API
	// https://medium.com/js-now/creating-a-real-time-qr-code-scanner-with-vanilla-javascript-part-1-2-creating-the-scanner-a8934ee8f614
	const imageFile = event.target.files[0];
	//const img = document.createElement('img');img.src = imageUrl;
	const imageUrl = URL.createObjectURL(imageFile);
	//document.querySelector('#qrcodeReaderInput').append(img);

	// Scan QR Code
	// https://github.com/zxing-js/library/issues/64
	/*
	const hints = new Map();
	const formats = [
		ZXing.BarcodeFormat.AZTEC,
		ZXing.BarcodeFormat.CODABAR,
		ZXing.BarcodeFormat.CODE_39,
		ZXing.BarcodeFormat.CODE_93,
		ZXing.BarcodeFormat.CODE_128,
		ZXing.BarcodeFormat.DATA_MATRIX,
		ZXing.BarcodeFormat.EAN_8,
		ZXing.BarcodeFormat.EAN_13,
		ZXing.BarcodeFormat.ITF,
		ZXing.BarcodeFormat.MAXICODE,
		ZXing.BarcodeFormat.PDF_417,
		ZXing.BarcodeFormat.QR_CODE,
		ZXing.BarcodeFormat.RSS_14,
		ZXing.BarcodeFormat.RSS_EXPANDED,
		ZXing.BarcodeFormat.UPC_A,
		ZXing.BarcodeFormat.UPC_E,
		ZXing.BarcodeFormat.UPC_EAN_EXTENSION
	];
	hints.set(ZXing.DecodeHintType.POSSIBLE_FORMATS, formats);
	*/
	codeReader = codeReader ?? new ZXing.BrowserQRCodeReader();//new ZXing.BrowserMultiFormatReader(hints);
	const result = await codeReader.decodeFromImageUrl(imageUrl)
	.then((result) => {
		console.log('Success: ', result);
		fileInput.value = null;
		
		// Show scan success
		videoArea.classList.add('bg-success');
		RL.a.animateCSS(videoArea, 'pulse');
		
		// Process the value
		//MyGame.processPayload(decodedText);
	}).catch((error) => {
		console.log(`Error scanning file. Reason: ${error}`);

		// Show scan error
		videoArea.classList.add('bg-danger');
		RL.a.animateCSS(videoArea, 'fadeIn');
	});

	// Wait, then remove class
	setTimeout(() => {
		RL.a.animateCSS(videoArea, 'fadeOut')
		.then((message) => {
			videoArea.classList.remove('bg-success', 'bg-danger');
		});
	}, 1 * 750);
};

// File based scanning

/*
const fileInputs = document.querySelectorAll('.qrcodeInputFile');
for ( const fileInput of fileInputs )
{
	fileInput.addEventListener('change', handleFileDecodeChange);
}
*/

// https://github.com/zxing-js/library/blob/master/docs/examples/qr-camera/index.html
const decodeOnce = async ( codeReader, previewElement, selectedDeviceId ) =>
{
	return codeReader.decodeOnceFromVideoDevice(selectedDeviceId, previewElement)
	.then((result) => {
		//console.log(result);
		//codeReader.reset();
		return result;
	}).catch((error) => {
		console.log(error);
		return false;
	});
};

const decodeOnceFromConstraints = async ( codeReader, constraints, previewElement ) =>
{
	return codeReader.decodeOnceFromConstraints(constraints, previewElement)
	.then((result) => {
		//console.log(result);
		//codeReader.reset();
		return result;
	}).catch((error) => {
		console.log(error);
		return false;
	});
};

const decodeContinuously = async ( codeReader, previewElement, selectedDeviceId ) =>
{
	return codeReader.decodeFromVideoDevice(selectedDeviceId, previewElement, ( result, error ) => {
		if ( result )
		{
			// properly decoded qr code
			//console.log('Found QR code!', result);
			//codeReader.reset();
			return result;
		}

		if ( error )
		{
			// As long as this error belongs into one of the following categories
			// the code reader is going to continue as excepted. Any other error
			// will stop the decoding loop.
			//
			// Excepted Exceptions:
			//
			//  - NotFoundException
			//  - ChecksumException
			//  - FormatException

			if ( error instanceof ZXing.NotFoundException ) {
				console.log('No QR code found.')
			}

			if ( error instanceof ZXing.ChecksumException ) {
				console.log('A code was found, but it\'s read value was not valid.')
			}

			if ( error instanceof ZXing.FormatException ) {
				console.log('A code was found, but it was in a invalid format.')
			}

			return false;
		}
	});
};

const getCameras = async ( constraints ) =>
{
	let result = await requestCameraPermission(constraints, false);

	if ( ! result )
	{
		return;
	}
	
	// If no enumerateDevices, stop
	if ( typeof(navigator) == 'undefined' )
	{
		console.log('Browser not capable of navigator');
		return;
	}
	
	// If no enumerateDevices, stop
	if ( ! navigator?.mediaDevices )
	{
		console.log('Browser not capable of media devices');
		return;
	}

	// If no enumerateDevices, stop
	if ( ! navigator.mediaDevices?.enumerateDevices )
	{
		console.log('Browser not capable of media devices enumerateDevices');
		return;
	}

	//const supportedConstraints = navigator.mediaDevices.getSupportedConstraints();
	//console.log('supportedConstraints', supportedConstraints);
	
	// List cameras and microphones
	return navigator.mediaDevices.enumerateDevices()
	.then((devices) => {
		//console.log('devices', devices);
		return {
			devices: devices,
			stream: result.stream,
			deviceId: result.deviceId
		};
	})
	.catch((error) => {
		console.log(`${error.name}: ${error.message}`, error);
	});
};

const handleGetCamerasClick = ( event ) =>
{
	if ( event )
	{
		event.preventDefault();
		RL.a.animateCSS(event.currentTarget, 'heartBeat');
	}

	const $this = $( event ? event.currentTarget : this );
	const $cameraArea = $this.closest('.camera-area');
	const cameraAreaId = $cameraArea[0].id;
	const cameraAreaSelector = `#${cameraAreaId}`;

	updateCameraSelection(true, cameraAreaSelector);
};

const updateCameraSelection = async ( requestPermissions, cameraAreaSelector ) =>
{
	const cameraPermission = await hasCameraPermission();
	// If no camera permissions, stop
	if ( ! cameraPermission && ! requestPermissions )
	{
		return;
	}

	// Update camera options
	// https://www.digitalocean.com/community/tutorials/front-and-rear-camera-access-with-javascripts-getusermedia
	// Get Step 2 camera selections ready
	const $cameraArea = $(cameraAreaSelector);
	const $cameraSelect = $('.cta--camera-select', $cameraArea);
	// Clear any existing options
	$cameraSelect.find('option:not([value=""])').remove();
	// Set up event handlers
	$cameraSelect.off()
	.on('change', function (event) {
		const value = $(this).val();
		$cameraArea.data('deviceId', value);
	});

	const result = await getCameras({
		video: {
			facingMode: ( cameraAreaSelector == '#frontCamera' ? 'user' : 'environment' ),
			zoom: true,
			torch: true
		}
	});

	if ( ! result )
	{
		return;
	}

	// Filter devices
	const videoDevices = result.devices.filter((device) => device.kind === 'videoinput');

	if ( videoDevices.length === 0 )
	{
		throw 'No camera found on this device.';
	}

	// Last camera may be primary with most features
	let lastCamera = videoDevices[videoDevices.length - 1].deviceId;
	const cameraDeviceId = result.deviceId;
	$cameraArea.data('deviceId', cameraDeviceId);

	// Iterate over devices
	videoDevices.forEach((camera) => {
		//console.log(`${camera.kind}: ${camera.label} id = ${camera.deviceId}`, camera.getCapabilities());

		// If label blank, most likely permissions not given

		// Add option
		$cameraSelect.append(`<option value="${camera.deviceId}"${( cameraDeviceId == camera.deviceId ) ? ' selected' : ''}>${camera.label}</option>`);
	});

	// Hide Step 1
	$('.cta--get-cameras', $cameraArea).addClass('d-none');
	// Show camera selection
	$('.video-camera-select-group', $cameraArea).removeClass('d-none');

	// If a stream passed, use it
	if ( result.stream )
	{
		handleStream(result.stream, cameraAreaSelector);
	}
};

const resetVideoControls = ( querySelector ) =>
{
	$('.video-area, .video-controls, .video-controls btn, .video-controls [class*="video-control-"]', querySelector).addClass('d-none');
};

const handleStartDecodeClickExample = async ( event ) =>
{
	document.querySelectorAll('.cta--start-decode-example, .cta--stop-decode-example').forEach((element) => {
		element.classList.toggle('d-none');
	});

	//const $this = $( event ? event.currentTarget : this );
	//const $cameraArea = $this.closest('.camera-area');
	const $cameraArea = $('#scannerCamera');
	const cameraAreaId = $cameraArea[0].id;
	const cameraAreaSelector = `#${cameraAreaId}`;
	const deviceId = $cameraArea.data('deviceId');
	
	const video = document.querySelector(`${cameraAreaSelector} video`);
	const videoArea = document.querySelector(`${cameraAreaSelector} .video-area`);
	const videoAreaOverlay = document.querySelector(`${cameraAreaSelector} .video-area-overlay`);
	const videoSpinner = document.querySelector(`${cameraAreaSelector} .video-spinner`);
	videoSpinner.classList.remove('d-none');
	RL.a.animateCSS(videoSpinner, 'fadeIn');

	$('.video-area, .video-controls, .cta--take-screenshot', $cameraArea).removeClass('d-none');

	// https://github.com/zxing-js/library/issues/64
	/*
	const hints = new Map();
	hints.set(ZXing.DecodeHintType.POSSIBLE_FORMATS, [
		ZXing.BarcodeFormat.QR_CODE
	]);
	*/
	codeReader = codeReader ?? new ZXing.BrowserQRCodeReader();//new ZXing.BrowserMultiFormatReader(hints);

	video.classList.add('d-none');
	// https://www.w3schools.com/tags/ref_av_dom.asp
	video.oncanplay = () => {
		// Fade in video elements and hide loading
		video.classList.remove('d-none');
		RL.a.animateCSS(video, 'fadeIn')
		.then((message) => {
			videoSpinner.classList.add('d-none');
			videoAreaOverlay.classList.remove('d-none');
			RL.a.animateCSS(videoAreaOverlay, 'fadeIn');
		});
		
		// Use video stream and set up controls
		const stream = video.srcObject;
		const track = stream.getVideoTracks()[0];
		const capabilities = track.getCapabilities();
		const settings = track.getSettings();
		
		let settingKey;
		
		// If camera light, use it
		settingKey = 'torch';
		$(`.cta--toggle-${settingKey}`, $cameraArea)
		.toggleClass('d-none', ! Boolean(capabilities[settingKey]));
		if ( capabilities[settingKey] )
		{
			$(`.cta--toggle-${settingKey}`, $cameraArea).off()
			.on('click', function ( event ) {
				track.applyConstraints({
					advanced: [{[settingKey]: ! settings[settingKey]}]
				})
				.catch(e => console.log(e));
			});
		}

		const ranges = ['zoom', 'brightness'];
		for ( const settingKey of ranges )
		{
			let $videoControl = $(`.video-control-${settingKey}`, $cameraArea);
			$videoControl.toggleClass('d-none', ! Boolean(capabilities[settingKey]));
			if ( capabilities[settingKey] )
			{
				const optionCapabilities = capabilities[settingKey];
				const optionSetting = settings[settingKey];
				const $range = $(`input[type="range"]`, $videoControl);
				$videoControl.find('.setting-value').html(`(${optionSetting})`);
				$range.attr({
					max: optionCapabilities.max,
					min: optionCapabilities.min,
					step: optionCapabilities.step
				}).val(optionSetting);
				$range.off().on('change', function ( event ) {//change,input
					$videoControl.find('.setting-value').html(`(${this.value})`);
					track.applyConstraints({
						advanced: [{[settingKey]: this.value}]
					})
					.catch(e => console.log(e));
				});

				// If zoom, default start to 4
				if ( settingKey == 'zoom' )
				{
					if ( optionCapabilities.min <= 4 && optionCapabilities.max >= 4 )
					{
						$range.val(4).trigger('change');
					}
				}
			}
		}
	};

	const constraints = {
		video: {
			facingMode: 'environment',
			torch: true,
			zoom: true
		}
	};
	const result = await decodeOnceFromConstraints(codeReader, constraints, document.querySelector(`${cameraAreaSelector} video`));
	// TODO: Possibly check and use preferred or previous device
	//result = await decodeOnce(codeReader, document.querySelector(`${cameraAreaSelector} video`), deviceId);
	$cameraArea.data('deviceId', getStreamDeviceId(video.srcObject));
	
	// If no result, error
	if ( ! result )
	{
		console.log('Error with permissions or scan.');

		// Show scan error
		videoArea.classList.add('bg-danger');
		RL.a.animateCSS(videoArea, 'fadeIn');
	}
	else
	{
		console.log(result);

		// Show scan success
		videoArea.classList.add('bg-success');
		RL.a.animateCSS(videoArea, 'pulse');

		// Stop decoding
		stopDecodeExample();
	}

	// Wait, then remove class
	setTimeout(() => {
		RL.a.animateCSS(videoArea, 'fadeOut')
		.then((message) => {
			videoArea.classList.remove('bg-success', 'bg-danger');
		});
	}, 0.75 * 1000);
};

const handleStopDecodeClickExample = async ( event ) =>
{
	stopDecodeExample();
};

const stopDecodeExample = () =>
{
	document.querySelectorAll('.cta--start-decode-example, .cta--stop-decode-example').forEach((element) => {
		element.classList.toggle('d-none');
	});

	const $cameraArea = $('#scannerCamera');
	const cameraAreaId = $cameraArea[0].id;
	const cameraAreaSelector = `#${cameraAreaId}`;
	const video = document.querySelector(`${cameraAreaSelector} video`);
	const videoAreaOverlay = document.querySelector(`${cameraAreaSelector} .video-area-overlay`);
	const videoSpinner = document.querySelector(`${cameraAreaSelector} .video-spinner`);
	// Reset video controls
	$('.video-controls, .video-controls btn, .video-controls [class*="video-control-"]', cameraAreaSelector).addClass('d-none');

	// Fade out video elements
	RL.a.animateCSS(videoAreaOverlay, 'fadeOut')
	.then((message) => {
		videoAreaOverlay.classList.add('d-none');
	});

	RL.a.animateCSS(videoSpinner, 'fadeOut')
	.then((message) => {
		videoSpinner.classList.add('d-none');
	});

	RL.a.animateCSS(video, 'fadeOut')
	.then((message) => {
		video.classList.add('d-none');

		// If code reader available, stop it and clean video
		if ( codeReader )
		{
			codeReader.reset();
			// Remove all event listeners
			// https://bobbyhadz.com/blog/javascript-remove-all-event-listeners-from-element
			video.replaceWith(video.cloneNode(true));
		}
	});
};

const handleStartStreamClick = ( event ) =>
{
	if ( event )
	{
		event.preventDefault();
		RL.a.animateCSS(event.currentTarget, 'heartBeat');
	}

	const $this = $( event ? event.currentTarget : this );
	const $cameraArea = $this.closest('.camera-area');
	const cameraAreaId = $cameraArea[0].id;
	const cameraAreaSelector = `#${cameraAreaId}`;
	const deviceId = $cameraArea.data('deviceId');

	resetVideo(document.querySelector(`${cameraAreaSelector} video`));
	resetVideoControls(cameraAreaSelector);
	resetScreenshot(`${cameraAreaSelector} .screenshot-image`);

	startStream({
		video: {
			deviceId: { exact: deviceId },
			//facingMode: 'environment',
			zoom: true,
			torch: true
		}
	}, cameraAreaSelector)
	.then((stream) => {
		handleStream(stream, cameraAreaSelector);
	});
};


const videoIsPlaying = ( video ) =>
{
	return ( video.currentTime > 0 && ! video.paused && ! video.ended && video.readyState > 2 );
};

// https://www.w3schools.com/jsref/prop_video_readystate.asp
const videoHasMetadata = ( video ) =>
{
	return ( video.readyState > 0 );
};

const handleStream = ( stream, cameraAreaSelector ) =>
{
	console.log('handleStream', stream, cameraAreaSelector);
	const $cameraArea = $(cameraAreaSelector);
	
	const track = stream.getVideoTracks()[0];
	const capabilities = track.getCapabilities();
	const settings = track.getSettings();
	//console.log('capabilities', capabilities, 'settings', settings);

	const video = document.querySelector(`${cameraAreaSelector} video`);
	video.autoplay = true;
	video.playsInline = true;
	video.volume = 0;
	video.muted = true;
	video.srcObject = stream;
	video.onloadedmetadata = () => { // readyState 1
		video.play();
	};
	
	$('.video-area, .video-controls, .cta--pause-video, .cta--stop-video, .cta--take-screenshot', $cameraArea).removeClass('d-none');
	
	let settingKey;
	
	// If camera light, use it
	settingKey = 'torch';
	$(`.cta--toggle-${settingKey}`, $cameraArea)
	.toggleClass('d-none', ! Boolean(capabilities[settingKey]));
	if ( capabilities[settingKey] )
	{
		$(`.cta--toggle-${settingKey}`, $cameraArea).off()
		.on('click', function ( event ) {
			track.applyConstraints({
				advanced: [{[settingKey]: ! settings[settingKey]}]
			})
			.catch(e => console.log(e));
		});
	}

	const ranges = ['zoom', 'brightness'];
	for ( const settingKey of ranges )
	{
		let $videoControl = $(`.video-control-${settingKey}`, $cameraArea);
		$videoControl.toggleClass('d-none', ! Boolean(capabilities[settingKey]));
		if ( capabilities[settingKey] )
		{
			const optionCapabilities = capabilities[settingKey];
			const optionSetting = settings[settingKey];
			const $range = $(`input[type="range"]`, $videoControl);
			$videoControl.find('.setting-value').html(`(${optionSetting})`);
			$range.attr({
				max: optionCapabilities.max,
				min: optionCapabilities.min,
				step: optionCapabilities.step
			}).val(optionSetting);
			$range.off().on('change', function ( event ) {//change,input
				$videoControl.find('.setting-value').html(`(${this.value})`);
				track.applyConstraints({
					advanced: [{[settingKey]: this.value}]
				})
				.catch(e => console.log(e));
			});
		}
	}

	$('.cta--pause-video, .cta--take-screenshot', $cameraArea).removeClass('d-none');
};

const handlePauseVideoClick = ( event ) =>
{
	if ( event )
	{
		event.preventDefault();
		RL.a.animateCSS(event.currentTarget, 'heartBeat');
	}

	const $this = $( event ? event.currentTarget : this );
	const $cameraArea = $this.closest('.camera-area');
	const cameraAreaId = $cameraArea[0].id;
	const cameraAreaSelector = `#${cameraAreaId}`;

	pauseVideo(document.querySelector(`${cameraAreaSelector} video`));
};

const handleStopVideoClick = ( event ) =>
{
	if ( event )
	{
		event.preventDefault();
		RL.a.animateCSS(event.currentTarget, 'heartBeat');
	}

	const $this = $( event ? event.currentTarget : this );
	const $cameraArea = $this.closest('.camera-area');
	const cameraAreaId = $cameraArea[0].id;
	const cameraAreaSelector = `#${cameraAreaId}`;

	resetVideo(document.querySelector(`${cameraAreaSelector} video`));
	resetVideoControls(cameraAreaSelector);
	resetScreenshot(`${cameraAreaSelector} .screenshot-image`);
};

const handleScreenshotClick = ( event ) =>
{
	const $this = $( event ? event.currentTarget : this );
	const $cameraArea = $this.closest('.camera-area');
	const cameraAreaId = $cameraArea[0].id;
	const cameraAreaSelector = `#${cameraAreaId}`;

	const video = document.querySelector(`${cameraAreaSelector} video`);
	const canvas = document.querySelector(`${cameraAreaSelector} canvas`);
	const screenshotImage = document.querySelector(`${cameraAreaSelector} .screenshot-image`);
	canvas.width = video.videoWidth;
	canvas.height = video.videoHeight;
	canvas.getContext('2d').drawImage(video, 0, 0);
	screenshotImage.src = canvas.toDataURL('image/webp');
	screenshotImage.classList.remove('d-none');
};

const pauseVideo = ( video ) =>
{
	// If no video or metadata
	if ( ! video || ! videoHasMetadata(video) )
	{
		return;
	}


	// Toggle pause/play
	if ( ! videoIsPlaying(video) )
	{
		video.play();
	}
	else
	{
		video.pause();
	}
};

const resetVideo = ( video ) =>
{
	if ( video && video.srcObject )
	{
		stopStream(video.srcObject);
	}
};

const resetScreenshot = ( querySelector ) =>
{
	// Reset screenshot
	const screenshotImage = document.querySelector(querySelector);
	screenshotImage.src = '';
	delete screenshotImage.src;
};

const startStream = async ( constraints ) =>
{
	if ( typeof(constraints) == 'undefined' )
	{
		constraints = {
			audio: true,
			video: true// {deviceId: cameraId, facingMode: 'environment', zoom: true, torch: true }
		};
	}
	
	return navigator.mediaDevices.getUserMedia(constraints)
	/*
	.then((stream) => {
		const track = stream.getVideoTracks()[0];
		const capabilities = track.getCapabilities();
		const settings = track.getSettings();
		handleStream(stream);
	})
	*/
	.catch((error) => {
		console.log(`${error.name}: ${error.message}`, error);
	});
};

const stopStream = ( stream ) =>
{
	if ( ! stream )
	{
		return;
	}

	// https://stackoverflow.com/questions/11642926/stop-close-webcam-stream-which-is-opened-by-navigator-mediadevices-getusermedia
	// Stop each stream track
	stream.getTracks().forEach((track) => 
	{
		if ( track.readyState == 'live' )
		{
			track.stop();
		}
	});
};

// https://stackoverflow.com/questions/46926479/how-to-get-media-device-ids-that-user-selected-in-request-permission-dialog
const getDevice = ( id ) =>
{
	navigator.mediaDevices.enumerateDevices()
	.then((devices) =>
	{
		devices.forEach((device) =>
		{
			if ( device.deviceId == id )
			{
				console.log(`${device.kind}: ${( device.label ) ? device.label : 'Default'}`);
				return device;
			}
		});
	})
	.catch((error) => {
		console.log(`${error.name}: ${error.message}`, error);
	});
};
</script>

			<a href="#" class="btn btn-primary" role="button" onclick="MyGame.toast.create(MyGame.toast.toastTemplate, 'Hello, world! This is a toast message.');return false;">Test Toast</a>

			<div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
				<div class="toast-header">
					<svg class="bd-placeholder-img rounded me-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
					<strong class="me-auto">Bootstrap</strong>
					<small class="text-muted">just now</small>
					<button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
				</div>
				<div class="toast-body">
					<div class="toast-body__content">Hello, world! This is a toast message.</div>
					<div class="mt-2 pt-2 border-top">
						<button type="button" class="btn btn-primary btn-sm">Take action</button>
						<button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="toast">Close</button>
					</div>
				</div>
			</div>

			<div class="toast show align-items-center" role="alert" aria-live="assertive" aria-atomic="true">
				<div class="d-flex">
					<div class="toast-body">
						<div class="toast-body__content">Hello, world! This is a toast message.</div>
					</div>
					<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
				</div>
			</div>
			
			<div class="toast show align-items-center text-bg-primary border-0" role="alert" aria-live="assertive" aria-atomic="true">
				<div class="d-flex">
					<div class="toast-body">
						<div class="toast-body__content">Hello, world! This is a toast message.</div>
					</div>
					<button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
				</div>
			</div>
			

			<hr>

			<h2>Icons</h2>
			<svg class="rounded" width="20" height="20" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
			
			<span class="fa-stack">
				<i class="fa-solid fa-maximize fa-fw fa-stack-1x"></i>
				<i class="fa-solid fa-ban fa-fw fa-stack-2x" style="color:Tomato"></i>
			</span>
			<i class="fa-solid fa-reply-all"></i>
			<i class="fa-solid fa-puzzle-piece"></i> 
			<i class="fa-solid fa-skull-crossbones"></i> 
			<i class="fa-solid fa-shield"></i>
			<i class="fa-solid fa-coins"></i>
			<i class="fa-solid fa-cube"></i> 
			<i class="fa-solid fa-cubes"></i> 
			<i class="fa-solid fa-square-full"></i> 
			<i class="fa-solid fa-diamond"></i> 
			<i class="fa-solid fa-heart"></i> 
			<i class="fa-solid fa-splotch"></i> 
			<i class="fa-solid fa-dice-d6"></i> 
			<i class="fa-solid fa-scroll"></i> 
			<i class="fa-solid fa-qrcode"></i> 
			<i class="fa-solid fa-circle-info"></i>
			<i class="fa-solid fa-circle-dot"></i>
			<i class="fa-solid fa-circle"></i>
			<i class="fa-regular fa-circle"></i>
			<i class="fa-solid fa-plus"></i> 
			<i class="fa-solid fa-plus fa-2xs"></i>
			<i class="fa-solid fa-circle-plus"></i> 
			<i class="fa-solid fa-circle-minus"></i>
			<i class="fa-regular fa-square-plus"></i> 
			<i class="fa-solid fa-square-plus"></i> 
			<i class="fa-solid fa-up-right-from-square"></i> 
			<i class="fa-solid fa-barcode"></i>
			<i class="fa-solid fa-wand-magic-sparkles"></i>
			<i class="fa-regular fa-star"></i>
			<i class="fa-solid fa-magnifying-glass"></i>
			<i class="fa-solid fa-sitemap"></i>
			<i class="fa-solid fa-user"></i>
			<i class="fa-solid fa-image"></i>
			<i class="fa-solid fa-music"></i>
			<i class="fa-solid fa-envelope"></i>
			<i class="fa-solid fa-xmark"></i>
			<i class="fa-solid fa-check"></i>
			<i class="fa-solid fa-info"></i>
			<i class="fa-solid fa-bolt"></i>
			<i class="fa-solid fa-paperclip"></i>
			<i class="fa-solid fa-pen"></i>
			<i class="fa-solid fa-gift"></i>
			<i class="fa-solid fa-gear"></i>
			<i class="fa-solid fa-tag"></i>
			<i class="fa-solid fa-lock"></i>
			<i class="fa-solid fa-bookmark"></i>
			<i class="fa-solid fa-book"></i>
			<i class="fa-solid fa-pen-to-square"></i>
			<i class="fa-solid fa-share-from-square"></i>
			<i class="fa-solid fa-fire"></i>
			<i class="fa-solid fa-circle-exclamation"></i>
			<i class="fa-solid fa-comments"></i>
			<i class="fa-solid fa-key"></i>
			<i class="fa-solid fa-camera"></i>
			<i class="fa-solid fa-eye"></i>
			<i class="fa-solid fa-eye-slash"></i>
			<i class="fa-solid fa-upload"></i>
			<i class="fa-solid fa-rectangle-list"></i>
			<i class="fa-solid fa-briefcase"></i>
			<i class="fa-solid fa-gears"></i>
			<i class="fa-solid fa-code"></i>
			<i class="fa-solid fa-compass"></i>
			<i class="fa-solid fa-earth-americas"></i>
			<i class="fa-solid fa-paper-plane"></i>
			<i class="fa-solid fa-ticket"></i>
			<i class="fa-solid fa-address-book"></i>
			<i class="fa-solid fa-wrench"></i>
			<i class="fa-solid fa-hammer"></i>
			<i class="fa-solid fa-lightbulb"></i>
			<i class="fa-solid fa-unlock"></i>
			<i class="fa-solid fa-unlock-keyhole"></i>
			<i class="fa-solid fa-wand-sparkles"></i>
			<i class="fa-solid fa-wand-magic"></i>
			<i class="fa-solid fa-right-from-bracket"></i>
			<i class="fa-solid fa-right-to-bracket"></i>
			<i class="fa-solid fa-arrow-up-from-bracket"></i>
			<i class="fa-solid fa-clock"></i>
			<i class="fa-solid fa-clock-rotate-left"></i>
			<i class="fa-solid fa-stopwatch"></i>
			<i class="fa-solid fa-user-clock"></i>
			<i class="fa-solid fa-fingerprint"></i>
			<i class="fa-solid fa-handcuffs"></i>
			<i class="fa-solid fa-door-closed"></i>
			<i class="fa-solid fa-ban"></i>
			<i class="fa-solid fa-layer-group"></i>
			<i class="fa-solid fa-signs-post"></i>
			<i class="fa-solid fa-location-dot"></i>
			<i class="fa-solid fa-location-pin"></i>
			<i class="fa-solid fa-location-pin-lock"></i>
			<i class="fa-solid fa-arrow-rotate-right"></i>
			<i class="fa-solid fa-rotate-right"></i>
			<i class="fa-solid fa-arrows-rotate"></i>
			<i class="fa-solid fa-arrows-spin"></i>
			<i class="fa-solid fa-repeat"></i>
			<i class="fa-solid fa-hand"></i>
			<i class="fa-solid fa-hand-sparkles"></i>
			<i class="fa-solid fa-hand-pointer"></i>
			<i class="fa-regular fa-hand-pointer"></i>
			<i class="fa-solid fa-file-arrow-up"></i>
			<i class="fa-solid fa-arrow-up"></i>
			<i class="fa-solid fa-up-long"></i>
			<i class="fa-solid fa-arrow-up-long"></i>
			<i class="fa-solid fa-arrow-pointer"></i>
			<i class="fa-solid fa-location-arrow"></i>
			<i class="fa-solid fa-share"></i>
			<i class="fa-solid fa-reply"></i>
			<i class="fa-solid fa-caret-up"></i>
			<i class="fa-solid fa-square-caret-right"></i>
			<i class="fa-solid fa-play"></i>
			<i class="fa-solid fa-chevron-up"></i>
			<i class="fa-solid fa-circle-chevron-up"></i>
			<i class="fa-solid fa-circle-chevron-left"></i>
			<i class="fa-solid fa-angle-up"></i>
			<i class="fa-regular fa-circle-up"></i>
			<i class="fa-solid fa-circle-up"></i>
			<i class="fa-solid fa-turn-up"></i>
			<i class="fa-solid fa-angles-up"></i>
			<i class="fa-solid fa-circle-xmark"></i>
			<i class="fa-solid fa-circle-check"></i>
			<i class="fa-solid fa-circle-info"></i>
			<i class="fa-solid fa-map-location-dot"></i>
			<i class="fa-solid fa-map"></i>
			<i class="fa-regular fa-map"></i>
			<i class="fa-solid fa-users"></i>
			<i class="fa-solid fa-house-user"></i>
			<i class="fa-solid fa-house"></i>
			<i class="fa-solid fa-house-chimney"></i>
			<i class="fa-solid fa-house-chimney-window"></i>
			<i class="fa-solid fa-house-chimney-user"></i>
			<i class="fa-solid fa-trophy"></i>
			<i class="fa-solid fa-clipboard-list"></i>
			<i class="fa-solid fa-flag-checkered"></i>
			<i class="fa-solid fa-hand-holding"></i>
			<i class="fa-solid fa-store"></i>
			<i class="fa-solid fa-screwdriver-wrench"></i>
			<i class="fa-solid fa-shop"></i>
			<i class="fa-solid fa-toolbox"></i>
			<i class="fa-solid fa-box"></i>
			<i class="fa-solid fa-box-open"></i>
			<i class="fa-solid fa-gift"></i>
			<i class="fa-solid fa-medal"></i>
			<i class="fa-solid fa-award"></i>
			<i class="fa-solid fa-trophy"></i>
			<i class="fa-solid fa-infinity"></i>
			<i class="fa-solid fa-skull-crossbones"></i>
			<i class="fa-solid fa-skull"></i>

			<hr>

			<h2>Bootstrap</h2>

			<h3>Slider</h3>

			<label for="customRange3" class="form-label">Example range</label>
			<input type="range" class="form-range" min="0" max="5" step="0.5" id="customRange3"

			<h3>Media Object</h3>
			<div class="d-flex align-items-center mt-4 mb-4">
				<div class="flex-shrink-0">
					<img alt="...">
					<svg class="rounded" width="100" height="100" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
				</div>
				<div class="flex-grow-1 ms-3">
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
					This is some content from a media component. You can replace this with any content and adjust it as needed.
				</div>
				<div class="flex-shrink-0">
					<img alt="...">
					<svg class="rounded" width="100" height="100" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
				</div>
			</div>
			
			<div class="mt-4 mb-4">
				<div class="float-start me-4">
					<img alt="...">
					<svg class="rounded" width="100" height="100" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
				</div>
				<div class="float-end ms-4">
					<img alt="...">
					<svg class="rounded" width="100" height="100" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
				</div>
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
				This is some content from a media component. You can replace this with any content and adjust it as needed.
			</div>

			<a href="#" class="btn btn-primary" role="button" data-bs-toggle="button">Toggle link</a>
			<a href="#" class="btn btn-primary active" role="button" data-bs-toggle="button" aria-pressed="true">Active toggle link</a>
			<a class="btn btn-primary disabled" aria-disabled="true" role="button" data-bs-toggle="button">Disabled toggle link</a>

			<nav>
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
					<button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Home</button>
					<button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</button>
					<button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</button>
					<button class="nav-link" id="nav-disabled-tab" data-bs-toggle="tab" data-bs-target="#nav-disabled" type="button" role="tab" aria-controls="nav-disabled" aria-selected="false" disabled>Disabled</button>
				</div>
			</nav>
			<div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab" tabindex="0">...</div>
				<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab" tabindex="0">...</div>
				<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab" tabindex="0">...</div>
				<div class="tab-pane fade" id="nav-disabled" role="tabpanel" aria-labelledby="nav-disabled-tab" tabindex="0">...</div>
			</div>

			<button type="button" class="btn btn-primary">
			Notifications <span class="badge text-bg-secondary">4</span>
			</button>

			<button type="button" class="btn btn-primary position-relative">
			Inbox
			<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
				99+
				<span class="visually-hidden">unread messages</span>
			</span>
			</button>

			<button type="button" class="btn btn-primary position-relative">
			Profile
			<span class="position-absolute top-0 start-100 translate-middle p-2 bg-danger border border-light rounded-circle">
				<span class="visually-hidden">New alerts</span>
			</span>
			</button>

			<div class="card" style="width: 18rem;">
			<!--<img src="..." class="card-img-top" alt="...">-->
			<div class="card-body">
				<h5 class="card-title">Card title</h5>
				<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
				<a href="#" class="btn btn-primary">Go somewhere</a>
			</div>
			</div>

			<div class="btn-group" role="group" aria-label="Basic example">
				<button type="button" class="btn btn-secondary">Left</button>
				<button type="button" class="btn btn-secondary">Middle</button>
				<button type="button" class="btn btn-secondary">Right</button>
			</div>


			<div class="alert alert-primary" role="alert">
				This is a primary alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
			</div>
			
			<div class="modal" tabindex="-1" role="dialog" aria-modal="true" style="display: block; position: relative; height: auto;">
				<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Modal title</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal" tabindex="-1" role="dialog" aria-modal="true" style="display: block; position: relative; height: auto;">
				<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Quiz</h5>
							<button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer justify-content-center">
							<button type="button" class="btn btn-success">Choice</button>
							<button type="button" class="btn btn-primary">Choice</button>
							<button type="button" class="btn btn-danger">Choice</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal" tabindex="-1" role="dialog" aria-modal="true" style="display: block; position: relative; height: auto;">
				<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Fill-in</h5>
							<button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer justify-content-center">
							<div class="input-group mb-3">
								<input type="text" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
								<button class="btn btn-primary" type="button" id="button-addon1">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal modal--showcase" tabindex="-1" role="dialog" aria-labelledby="showcase-title" aria-modal="true" style="display: block; position: relative; height: auto;">
				<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
					<div class="modal-content">
						<div class="modal-header justify-content-center">
							<h5 class="modal-title text-muted" id="showcase-title">Item Name</h5>
							<button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body text-center">
							<svg class="rounded" width="100" height="100" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
							<div>Item description.</div>
						</div>
						<div class="modal-footer justify-content-center">
							<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">OK</button>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
				Launch demo modal
			</button>

			<!-- Modal -->
			<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="input-group">
				<input type="text" class="form-control rounded-left" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
				<div class="input-group-append">
					<button class="btn btn-outline-secondary rounded-right" type="button">Button</button>
				</div>
			</div>

			
			<ul class="list-group">
				<li class="list-group-item active">An item <span class="badge bg-secondary">New</span></li>
				<li class="list-group-item">A second item</li>
				<li class="list-group-item">A third item</li>
				<li class="list-group-item">A fourth item</li>
				<li class="list-group-item disabled" aria-disabled="true">And a fifth one</li>
			</ul>
			<div class="list-group">
				<button type="button" class="list-group-item list-group-item-action active" aria-current="true">
					The current button
				</button>
				<button type="button" class="list-group-item list-group-item-action">A second button item</button>
				<button type="button" class="list-group-item list-group-item-action">A third button item</button>
				<button type="button" class="list-group-item list-group-item-action">A fourth button item</button>
				<button type="button" class="list-group-item list-group-item-action" disabled>A disabled button item</button>
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">An item</li>
				<li class="list-group-item">A second item</li>
				<li class="list-group-item">A third item</li>
				<li class="list-group-item">A fourth item</li>
				<li class="list-group-item">And a fifth one</li>
			</ul>
			<ol class="list-group list-group-numbered">
				<li class="list-group-item">A list item</li>
				<li class="list-group-item">A list item</li>
				<li class="list-group-item">A list item</li>
			</ol>
			<ol class="list-group list-group-numbered">
				<li class="list-group-item d-flex justify-content-between align-items-start">
					<div class="ms-2 me-auto">
					<div class="fw-bold">Subheading</div>
					Content for list item
					</div>
					<span class="badge bg-primary rounded-pill">14</span>
				</li>
				<li class="list-group-item d-flex justify-content-between align-items-start">
					<div class="ms-2 me-auto">
					<div class="fw-bold">Subheading</div>
					Content for list item
					</div>
					<span class="badge bg-primary rounded-pill">14</span>
				</li>
				<li class="list-group-item d-flex justify-content-between align-items-start">
					<div class="ms-2 me-auto">
					<div class="fw-bold">Subheading</div>
					Content for list item
					</div>
					<span class="badge bg-primary rounded-pill">14</span>
				</li>
			</ol>
			<ul class="list-group">
				<li class="list-group-item d-flex justify-content-between align-items-center">
					A list item
					<span class="badge bg-primary rounded-pill">14</span>
				</li>
				<li class="list-group-item d-flex justify-content-between align-items-center">
					A second list item
					<span class="badge bg-primary rounded-pill">2</span>
				</li>
				<li class="list-group-item d-flex justify-content-between align-items-center">
					A third list item
					<span class="badge bg-primary rounded-pill">1</span>
				</li>
			</ul>

			<div class="list-group list-group-flush">
				<li class="list-group-item" aria-current="true">
					
					<div class="d-flex w-100 justify-content-between">
						<span class="badge bg-secondary rounded-pill align-self-center">1</span>
						<div class="d-flex align-items-center">
							<small class="text-muted">3 days ago</small>
						</div>
					</div>
					<div class="d-flex w-100 justify-content-between">
						<h5 class="mb-1">List group item heading</h5>
					</div>
					<p class="mb-1">Some placeholder content in a paragraph.</p>
					<small class="text-muted">And some muted small print.</small>
					<div class="d-flex flex-wrap mt-2 pt-2">
						<button type="button" class="btn btn-primary btn-sm mb-2 me-2">Take action</button>
						<button type="button" class="btn btn-secondary btn-sm mb-2 me-2" data-bs-dismiss="toast">Close</button>

						<div class="input-group w-auto mb-2 ms-auto">
							<select class="form-select form-select-sm" aria-label="Default select example">
								<option value="1" selected>1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
							<label class="form-check-label input-group-text">
								<input class="form-check-input m-0" type="checkbox" value="" aria-label="Checkbox for following text input">
							</label>
						</div>
					</div>

				</li>
			</div>
			<hr>
			<div class="d-flex justify-content-end p-2">
				<button type="button" class="btn btn-primary btn-sm cta--items-combine">Combine Selected</button>
			</div>

			<div class="list-group">
				<li class="list-group-item" aria-current="true">
					<div class="d-flex w-100 justify-content-between">
						<h5 class="mb-1">List group item heading</h5>
						<small class="text-muted">3 days ago</small>
					</div>
					<p class="mb-1">Some placeholder content in a paragraph.</p>
					<small class="text-muted">And some muted small print.</small>
					<div class="mt-2 pt-2">
						<button type="button" class="btn btn-primary btn-sm">Take action</button>
						<button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="toast">Close</button>
					</div>
				</li>
				<a href="#" class="list-group-item list-group-item-action active" aria-current="true">
					<div class="d-flex w-100 justify-content-between">
					<h5 class="mb-1">List group item heading</h5>
					<small>3 days ago</small>
					</div>
					<p class="mb-1">Some placeholder content in a paragraph.</p>
					<small>And some small print.</small>
				</a>
				<a href="#" class="list-group-item list-group-item-action">
					<div class="d-flex w-100 justify-content-between">
					<h5 class="mb-1">List group item heading</h5>
					<small class="text-muted">3 days ago</small>
					</div>
					<p class="mb-1">Some placeholder content in a paragraph.</p>
					<small class="text-muted">And some muted small print.</small>
				</a>
				<a href="#" class="list-group-item list-group-item-action">
					<div class="d-flex w-100 justify-content-between">
					<h5 class="mb-1">List group item heading</h5>
					<small class="text-muted">3 days ago</small>
					</div>
					<p class="mb-1">Some placeholder content in a paragraph.</p>
					<small class="text-muted">And some muted small print.</small>
				</a>
			</div>

			<ul class="list-group">
				<li class="list-group-item">
					<input class="form-check-input me-1" type="checkbox" value="" id="firstCheckboxStretched">
					<label class="form-check-label stretched-link" for="firstCheckboxStretched">First checkbox</label>
				</li>
				<li class="list-group-item">
					<input class="form-check-input me-1" type="checkbox" value="" id="secondCheckboxStretched">
					<label class="form-check-label stretched-link" for="secondCheckboxStretched">Second checkbox</label>
				</li>
				<li class="list-group-item">
					<input class="form-check-input me-1" type="checkbox" value="" id="thirdCheckboxStretched">
					<label class="form-check-label stretched-link" for="thirdCheckboxStretched">Third checkbox</label>
				</li>
			</ul>

			<div class="input-group mb-3">
				<div class="input-group-text">
					<input class="form-check-input mt-0" type="checkbox" value="" aria-label="Checkbox for following text input">
				</div>
				<input type="text" class="form-control" aria-label="Text input with checkbox">
			</div>
			<div class="input-group">
				<div class="input-group-text">
					<input class="form-check-input mt-0" type="radio" value="" aria-label="Radio button for following text input">
				</div>
				<input type="text" class="form-control" aria-label="Text input with radio button">
			</div>

			<div class="input-group mb-3">
				<button class="btn btn-outline-secondary" type="button" id="button-addon1">Button</button>
				<input type="text" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
			</div>
			<div class="input-group mb-3">
				<input type="text" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
				<button class="btn btn-outline-secondary" type="button" id="button-addon1">Button</button>
			</div>

			<div class="input-group mb-3">
				<button type="button" class="btn btn-outline-secondary">Action</button>
				<button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
					<span class="visually-hidden">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu">
					<li><a class="dropdown-item" href="#">Action</a></li>
					<li><a class="dropdown-item" href="#">Another action</a></li>
					<li><a class="dropdown-item" href="#">Something else here</a></li>
					<li><hr class="dropdown-divider"></li>
					<li><a class="dropdown-item" href="#">Separated link</a></li>
				</ul>
				<input type="text" class="form-control" aria-label="Text input with segmented dropdown button">
			</div>

			<div class="input-group">
				<input type="text" class="form-control" aria-label="Text input with segmented dropdown button">
				<button type="button" class="btn btn-outline-secondary">Action</button>
				<button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
					<span class="visually-hidden">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu dropdown-menu-end">
					<li><a class="dropdown-item" href="#">Action</a></li>
					<li><a class="dropdown-item" href="#">Another action</a></li>
					<li><a class="dropdown-item" href="#">Something else here</a></li>
					<li><hr class="dropdown-divider"></li>
					<li><a class="dropdown-item" href="#">Separated link</a></li>
				</ul>
			</div>

			<ul class="nav justify-content-center">
				<li class="nav-item">
					<a class="nav-link active" aria-current="page" href="#">Active</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Link</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Link</a>
				</li>
				<li class="nav-item">
					<a class="nav-link disabled">Disabled</a>
				</li>
			</ul>

		</div>
	</div>
</div>

<div class="container mb-4 mt-4">
	<div class="row">
		<div class="col-sm">
			<h1>Generated QRCode</h1>
		</div>
	</div>
</div>
<div class="container mb-4 mt-4">
	<div class="row">
		<div class="col-sm">

			<div class="form-group">
				<label for="qrcodeValue">QRCode Value</label>
				<input aria-describedby="qrCodeHelp" class="form-control" id="qrcodeValue" type="text" value="http://jindo.dev.naver.com/collie" placeholder="Enter value">
				<small id="qrCodeHelp" class="form-text text-muted">Information stored in the QRCode.</small>
			</div>

			<div id="qrcode" style="width:100px; height:100px; margin-top:15px;"></div>
			
			<!-- Create QRCode -->
			<script type="text/javascript" src="../../vendor/qrcode/davidshimjs/qrcodejs/qrcode.min.js"></script>
			<script type="text/javascript">
			var qrcode = new QRCode(document.getElementById("qrcode"), {
				width : 100,
				height : 100
			});

			function makeCode () {		
				var elText = document.getElementById("qrcodeValue");
				
				if (!elText.value) {
					alert("Input a text");
					elText.focus();
					return;
				}
				
				qrcode.makeCode(elText.value);
			}

			makeCode();

			(function() {
				var nTimer = setInterval(function() {
					if (window.jQuery) {
						// Do something with jQuery
						clearInterval(nTimer);
						
						jQuery(document).ready(function($)
						{
							
							// When text changed, update code
							$("#qrcodeValue").
							on("blur", function () {
								makeCode();
							}).
							on("keydown", function (e) {
								if (e.keyCode == 13) {
									makeCode();
								}
							});
							
						});
					}
				}, 100);
			})();
			</script>
			
		</div>
	</div>
</div>

