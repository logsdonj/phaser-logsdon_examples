<div class="container-fluid game-screen game-screen--nested game-screen--settings d-none" id="settings-screen">
	<div class="game-screen__wrapper">

		<div class="row game-screen__title-row">
			<div class="col game-screen__back-col">
				<a class="icon-screen-back icon--fa game-screen__back game-screen__nav-icon cta--prev-screen" href="#main-screen"><span class="visually-hidden">">&lt;</span></a>
			</div>
			<div class="col game-screen__title-col">
				<h2 class="game-screen__title-heading">Settings</h2>
			</div>
			<div class="col game-screen__close-col">
				<a class="icon-screen-close icon--fa game-screen__close game-screen__nav-icon cta--close-screen" href="#main-screen"><span class="visually-hidden">">&times;</span></a>
			</div>
		</div>
		<div class="row game-screen__content-row">
			<div class="col">
				
				<ul class="list-group list-group-flush">
					<li class="list-group-item">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">Gameplay</h5>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input toggle__fullScreen" type="checkbox" role="switch" id="setting__fullScreen">
							<label class="form-check-label" for="setting__fullScreen">Fullscreen</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" type="checkbox" role="switch" id="setting__autoLight">
							<label class="form-check-label" for="setting__autoLight">Auto Light <small class="text-muted"> &ndash; Turns on camera light when scanning if possible</small>
							</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" type="checkbox" role="switch" id="setting__autoZoom">
							<label class="form-check-label" for="setting__autoZoom">Auto Zoom <small class="text-muted"> &ndash; Zooms camera when scanning if possible</small>
							</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" type="checkbox" role="switch" id="setting__leftHanded">
							<label class="form-check-label" for="setting__leftHanded">Left-handed</label>
						</div>
					</li>
					<li class="list-group-item">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">Theme</h5>
						</div>
						<p class="mb-1"><small class="text-muted">Adjust the look of the game interface.</small></p>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="setting__theme" id="setting__theme1" value="" checked>
							<label class="form-check-label" for="setting__theme1">
								Light
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="setting__theme" id="setting__theme2" value="theme-dark">
							<label class="form-check-label" for="setting__theme2">
								Dark
							</label>
						</div>
					</li>
					<li class="list-group-item">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">Modes</h5>
						</div>
						<p class="mb-1"><small class="text-muted">Change up how the game is played.</small></p>
						<div class="form-check form-switch">
							<input class="form-check-input" type="checkbox" role="switch" id="setting__debugMode">
							<label class="form-check-label" for="setting__debugMode">Debug mode <small class="text-muted"> &ndash; For game development</small>
							</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" type="checkbox" role="switch" id="setting__collectorMode">
							<label class="form-check-label" for="setting__collectorMode">Collector mode <small class="text-muted"> &ndash; See all components to find and unlock</small>
							</label>
						</div>
						<div class="form-check form-switch">
							<input class="form-check-input" type="checkbox" role="switch" id="setting__actionMode">
							<label class="form-check-label" for="setting__actionMode">Action mode <small class="text-muted"> &ndash; All nonlinear challenges are available</small>
							</label>
						</div>
					</li>
					<li class="list-group-item">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">Credits</h5>
						</div>
						<small class="text-muted">
							<ul>
								<li>https://getbootstrap.com/</li>
								<li>https://fontawesome.com/</li>
								<li>https://unsplash.com/</li>
								<li>https://www.artbreeder.com/</li>
								<li>https://introjs.com/</li>
								<li>https://animate.style/</li>
								<li>https://www.google.com/</li>
							</ul>
						</small>
					</li>
				</ul>

			</div>
		</div>
		
	</div>
</div>