<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	
	<meta http-equiv="X-UA-Compatible" content="chrome=1, IE=9">
	<meta http-equiv="cleartype" content="on">
	<meta name="format-detection" content="telephone=no">
	<meta name="HandheldFriendly" content="true">
	<meta name="robots" content="noindex,nofollow">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Game">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 minimum-scale=1.0 user-scalable=0 minimal-ui shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<!--
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
	-->
	<link href="../../vendor/bootstrap/5.2.2/bootstrap.min.css" rel="stylesheet">

	<!-- Use common styles. -->
	<link href="../../vendor/fontawesome/fontawesome-free-6.2.0-web/css/all.min.css" rel="stylesheet">

	<!-- Animate.css -->
	<link href="../../vendor/animate.css/4.1.1/animate.min.css" rel="stylesheet">
	
	<?php
	// Gather files to load in order
	$stylesToLoad = [
		'styles/style.css',
	];
	// For each file
	foreach ( $stylesToLoad as $style )
	{
		// Add modified time onto file to break cache
		$modifiedTime = filemtime($style);
	?>
	<link href="<?php echo $style; ?>?<?php echo $modifiedTime; ?>" rel="stylesheet">
	<?php
	}
	?>

	<style type="text/css">
	</style>

	<!-- Fonts -->
	<!--
	<link rel="stylesheet" href="./assets/fonts/stylesheet.css">
	-->

	<!-- Custom config loading -->
	<script type="text/javascript">
    </script>
</head>
<body class="-body--debug setting--gameplay-left-handed-toggle">

<!-- Loading screen -->
<div class="container-fluid d-flex vw-100 justify-content-center align-items-center flex-column min-vh-100 fixed-top" id="loading-screen" style="background-color: #ffffff;">
	<div class="d-flex justify-content-center">
		<div class="spinner-border" role="status">
			<span class="visually-hidden">Loading...</span>
		</div>
	</div>
</div>

<!-- Modal container -->
<div class="modal-container"></div>
<!-- Toast container -->
<div class="toast-container position-fixed top-0 end-0 p-3"></div>

<?php
// Include PHP templates for example and JS use
foreach ( ['game', 'components', 'challenges', 'items', 'locations', 'characters'] as $filename )
{
	require_once('includes/templates/' . $filename . '.php');
}

// If showing references
if ( isset($_GET['reference']) )
{
	require_once('screens/examples-and-reference.php');
}
// If playing a game
elseif ( isset($_GET['database']) )
{
	require_once('screens/main.php');

	require_once('screens/challenges.php');
	require_once('screens/items.php');
	require_once('screens/locations.php');
	require_once('screens/characters.php');
	
	require_once('screens/title.php');
	require_once('screens/instructions.php');
	require_once('screens/settings.php');
}
// Show game selection
else
{
	?>

	<div class="container d-flex vw-100 justify-content-center align-items-center flex-column min-vh-100">

		<div class="row w-100">
			<div class="col-md mt-3">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item active" aria-current="page">Home</li>
					</ol>
				</nav>
				<hr>
			</div>
		</div>

		<div class="row w-100 justify-content-center align-items-stretch">

		<?php
		// Gather all database files, parse their data, and present a game select screen
		$scanDir = './scripts/game/databases/';
		// https://www.php.net/manual/en/function.scandir.php
		$scan = scandir($scanDir, SCANDIR_SORT_NONE);
		$scanCount = count($scan);
		$files = [];
		// For each file
		foreach ( $scan as $file )
		{
			$fileWithPath = $scanDir . $file;
			// If dots or not a file, skip
			if ( $file == '.' || $file == '..' || ! is_file($fileWithPath) )
			{
				continue;
			}

			// Add file for usage
			$files[] = $file;
			$fileName = basename($file, '.js');

			// Parse file JSON for data
			$rawJson = file_get_contents($fileWithPath);
			// https://jsonformatter.org/
			$testRawJson = 'const qrcodeDatabase = {
				"game": { // Test comment
					"name": "Test Name",
					"items": {
						"1": [1,"1"],
						"2": "https:\/\/www.example.com\/",
						"3": ""/*,
						"4": ""*/
					},
					"challenges": {
						"flag": true,
						"html": "<a href=\"\">Test<\/a>"
					}
				}
			};';
			$fileJson = $rawJson;

			// Clean up JSON, stripping comments and removing beginning and ending JavaScript

			// https://stackoverflow.com/questions/17776942/remove-fake-comments-from-json-file
			// https://stackoverflow.com/questions/3984380/regular-expression-to-remove-css-comments
			// https://stackoverflow.com/questions/643113/regex-to-strip-comments-and-multi-line-comments-and-empty-lines
			// https://stackoverflow.com/questions/10210338/json-encode-escaping-forward-slashes
			// Removes multi-line comments and does not create a blank line, also treats white spaces/tabs 
			//$fileJson = preg_replace('!^[ \t]*/\*.*?\*/[ \t]*[\r\n]!s', '', $fileJson);
			$fileJson = preg_replace('/\n\s*\n/', "\n", $fileJson);
			$fileJson = preg_replace('!/\*.*?\*/!s', '', $fileJson);
			
			// Removes single line '//' comments, treats blank characters
			$fileJson = preg_replace('![ \t]*//.*[ \t]*[\r\n]!', '', $fileJson);
			// Strip blank lines
			$fileJson = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $fileJson);

			// Remove from beginning
			$jsonBeginningRemoval = 'const qrcodeDatabase = ';
			$needle = $jsonBeginningRemoval;
			$needleLength = strlen($jsonBeginningRemoval);
			if ( substr($fileJson, 0, $needleLength) == $needle )
			{
				$fileJson = substr($fileJson, $needleLength);
			}

			// Remove from ending
			$jsonEndingRemoval = ';';
			$needle = $jsonEndingRemoval;
			$needleLength = strlen($needle);
			if ( substr($fileJson, - $needleLength) == $needle )
			{
				$fileJson = substr($fileJson, 0, - $needleLength);
			}

			// Parse
			$fileJson = json_decode($fileJson, false);
			$gameName = $fileJson ? $fileJson->game->name : $fileName;
			$gameDescription = $fileJson ? $fileJson->game->description : 'No description available.';
			$gameVersion = $fileJson ? $fileJson->game->version : '';
			?>

			<div class="col-md-4 mb-4 align-items-stretch">
				<div class="card h-100">
					<!--
					<div class="ratio ratio-16x9">
						<svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image cap" preserveAspectRatio="xMidYMid slice" focusable="false">
							<title>Placeholder</title>
							<rect width="100%" height="100%" fill="#868e96"></rect>
						</svg>
					</div>
					-->
					<div class="card-body d-flex flex-column">
						<h5 class="card-title"><?php echo $gameName; ?></h5>
						<h6 class="card-subtitle mb-2 text-muted">
							<?php echo $file; ?>
							<?php if ( $gameVersion ) { ?>
							<span class="badge bg-secondary">v<?php echo $gameVersion; ?></span>
							<?php } ?>
							<span class="badge bg-<?php echo $fileJson ? 'success' : 'danger'; ?>">JSON<i class="fa-regular fa-<?php echo $fileJson ? 'circle-check' : 'circle-xmark'; ?> fa-fw"></i></span>
						</h6>
						<p class="card-text"><?php echo $gameDescription; ?></p>
						<?php
						// If valid JSON, allow play
						if ( $fileJson )
						{
							?>
						<a href="?database=<?php echo $fileName; ?>" class="btn btn-primary d-block mt-auto"><i class="fa-solid fa-circle-play fa-fw me-1"></i>Play</a>
						<!--
						<a class="card-link" href="?database=<?php echo $fileName; ?>">Play</a>
						-->
							<?php
						}
						?>
					</div>
				</div>
			</div>

		<?php
		}
		?>

		</div>

		<div class="row w-100">
			<div class="col-md mb-3 text-center">
				<hr>
				<a href="?reference">Examples and Reference</a>
			</div>
		</div>
		
	</div>

	<?php
	
}
?>



	<!-- Optional JavaScript; choose one of the two! -->

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<!--
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
	-->
	<script type="text/javascript" src="../../vendor/bootstrap/5.2.2/bootstrap.bundle.min.js"></script>

	<!-- Option 2: Separate Popper and Bootstrap JS -->
	<!--
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
	-->

	<!--
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	-->
	<script type="text/javascript" src="../../vendor/jquery/jquery-3.6.1.min.js"></script>

	<!-- Intro.js onboarding -->
	<!--
	// https://hospodarets.com/highlight_element_with_page_fading
	// https://www.jqueryscript.net/blog/best-guided-tour.html
	-->
	<link rel="stylesheet" href="../../vendor/intro.js/6.0.0/introjs.min.css">
	<script src="../../vendor/intro.js/6.0.0/intro.min.js"></script>
	<!-- Compromise - Language data and especially verb tense handling -->
	<script src="../../vendor/compromise/compromise.js"></script>
	<!-- zxing-js bar code reader -->
	<!-- <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script> -->
	<!-- <script type="text/javascript" src="https://unpkg.com/@zxing/browser@latest"></script> -->
	<script src="../../vendor/qrcode/zxing/library/0.18.6/index.min.js"></script>

	<!-- Read QRCode -->
	<script type="text/javascript" src="../../vendor/qrcode/mebjas/html5-qrcode/minified/html5-qrcode.min.js"></script>

	<?php
	// Gather files to load in order
	$scriptsToLoad = [
		// Game
		'../../vendor/redlove/scripts/RedLove.js',
		'../../vendor/redlove/scripts/library/animation.js',
		'../../vendor/redlove/scripts/library/fullscreen.js',
		'../../vendor/redlove/scripts/library/phaser.js',
		'../../vendor/redlove/scripts/library/utilities.js',
		'../../vendor/redlove/scripts/library/utilities/datetime.js',
		'../../vendor/redlove/scripts/library/utilities/dom.js',
		'../../vendor/redlove/scripts/library/utilities/function.js',
		'../../vendor/redlove/scripts/library/utilities/image.js',
		'../../vendor/redlove/scripts/library/utilities/number.js',
		'../../vendor/redlove/scripts/library/utilities/object.js',
		'../../vendor/redlove/scripts/library/utilities/string.js',
		'../../vendor/redlove/scripts/classes/Clock.js',
		'../../vendor/redlove/scripts/classes/EventHandler.js',
		'../../vendor/redlove/scripts/classes/Timer.js',
		'../../vendor/redlove/scripts/classes/Logger.js',
		'scripts/game/utilities/Ledger.js',
		'scripts/game/utilities/Modal.js',
		'scripts/game/utilities/Toast.js',
		'scripts/game/components/ui/DebugUi.js',
		'scripts/game/components/ui/GameUi.js',
		'scripts/game/components/ui/CameraUi.js',
		'scripts/game/components/ui/NavUi.js',
		'scripts/game/components/ui/CtaUi.js',
		'scripts/game/components/ui/TourUi.js',
		'scripts/game/components/ui/ComponentInfoUi.js',
		'scripts/game/components/Challenge.js',
		'scripts/game/components/ui/ChallengeUi.js',
		'scripts/game/components/Character.js',
		'scripts/game/components/ui/CharacterUi.js',
		'scripts/game/components/Game.js',
		'scripts/game/components/Item.js',
		'scripts/game/components/ui/ItemUi.js',
		'scripts/game/components/Location.js',
		'scripts/game/components/ui/LocationUi.js',
		'scripts/game/components/Player.js',
		'scripts/game/components/ui/PlayerUi.js',
		'includes/templatesToJS.php',
	];

	$database = ( isset($_GET['database']) ) ? $_GET['database'] : 'test';
	$scriptsToLoad[] = 'scripts/game/databases/' . $database . '.js';
	$scriptsToLoad[] = 'scripts/game/main.js';

	// For each file
	foreach ( $scriptsToLoad as $script )
	{
		// Add modified time onto file to break cache
		$modifiedTime = filemtime($script);
	?>
	<script src="<?php echo $script; ?>?<?php echo $modifiedTime; ?>" type="text/javascript"></script>
	<?php
	}
	?>

</body>
</html>
