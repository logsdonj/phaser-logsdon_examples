
// File based scanning
const fileinput = document.getElementById('qrcodeInputFile');
if ( fileinput )
{
	fileinput.addEventListener('change', e => {
		if (e.target.files.length == 0) {
			// No file selected, ignore 
			return;
		}
	
		const html5QrCodeInput = new Html5Qrcode("qrcodeReaderInput");
		const imageFile = e.target.files[0];
	
		// Scan QR Code
		html5QrCodeInput.scanFile(imageFile, false)
		.then(decodedText => {
			// success, use decodedText
			console.log('Success: ', decodedText);
			fileinput.value = null;
	
			// Process the value
			MyGame.processPayload(decodedText);
		})
		.catch(err => {
			// failure, handle it.
			console.log(`Error scanning file. Reason: ${err}`)
		});
	});
}