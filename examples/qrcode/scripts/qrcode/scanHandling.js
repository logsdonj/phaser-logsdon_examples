
function docReady(fn) {
	// see if DOM is already available
	if (document.readyState === "complete"
		|| document.readyState === "interactive") {
		// call on next available tick
		setTimeout(fn, 1);
	} else {
		document.addEventListener("DOMContentLoaded", fn);
	}
}


let html5QrCode;
let config = {
	formatsToSupport: [Html5QrcodeSupportedFormats.QR_CODE],
	fps: 10,
	qrbox: { width: 250, height: 250 },
	showTorchButtonIfSupported: true,
	showZoomSliderIfSupported: true
};

function onScanSuccess(decodedText, decodedResult) {
	// handle the scanned code as you like, for example:
	console.log(`Code matched = ${decodedText}`, decodedResult);
	
	stopScanning();
	// Process the value
	MyGame.processPayload(decodedText);
}

function onScanFailure(error) {
	// handle scan failure, usually better to ignore and keep scanning.
	// for example:
	//console.warn(`Code scan error = ${error}`);
}

function startScanning (event)
{
	if ( event )
	{
		event.preventDefault();
		animateCSS(this, 'heartBeat');
	}

	// Show scanner
	jQuery('#qrcodeReaderContainer').addClass('scanner-overlay');
	animateCSS('#qrcodeReaderContainer', 'fadeIn').then((message) => {});

	// Easy Mode
	//html5QrCode = new Html5QrcodeScanner("qrcodeReader", config);
	// Pro Mode
	html5QrCode = new Html5Qrcode("qrcodeReader", false);
	
	try
	{
		// Easy Mode
		//html5QrCode.render(onScanSuccess, onScanFailure);
		// Pro Mode
		// Prefer back camera and simple setup
		//html5QrCode.start({facingMode: "environment"}, config, onScanSuccess, onScanFailure);

		const autoLight = MyGame.getSetting('autoLight', '', '', 'game');
		const autoZoom = MyGame.getSetting('autoZoom', '', '', 'game');
		const advancedScanSetup = false;//( autoLight || autoZoom );

		// If simple scan setup
		if ( ! advancedScanSetup )
		{
			html5QrCode.start({facingMode: "environment"}, config, onScanSuccess, onScanFailure)
			// Use promise to adjust stream settings
			.then(() => {
				// https://medium.com/qr-code/using-flash-or-torch-with-html5-qr-code-library-ba931965fe78
				// let settings = html5Qrcode.getRunningTrackSettings();return "torch" in settings;
				//console.log(html5QrCode.getRunningTrackCameraCapabilities());
				const stream = html5QrCode.renderedCamera.mediaStream;
				const track = stream.getVideoTracks()[0];
				const capabilities = track.getCapabilities();
				//const settings = track.getSettings();

				// If camera light, use it
				if ( autoLight && capabilities.torch )
				{
					track.applyConstraints({
						advanced: [{torch: true}]
					})
					.catch(e => console.log(e));
				}

				// If zoom, use it
				if ( autoZoom && capabilities.zoom )
				{
					track.applyConstraints({
						advanced: [{zoom: 2}]
					})
					.catch(e => console.log(e));
				}
			})
			.catch((err) => {
				// Start failed, handle it

				// Keep it simple
				html5QrCode.start({facingMode: "environment"}, config, onScanSuccess, onScanFailure);
			});
		}
		else
		{
			// Use the last camera, which may be primary with more features
			Html5Qrcode.getCameras()
			.then((devices) => {
				if ( ! devices || ! devices.length )
				{
					return;
				}

				let cameraId = devices[devices.length - 1].id;

				// Use specific camera
				html5QrCode.start(cameraId, config, onScanSuccess, onScanFailure)
				// Use promise to adjust stream settings
				.then(() => {
					// https://medium.com/qr-code/using-flash-or-torch-with-html5-qr-code-library-ba931965fe78
					// let settings = html5Qrcode.getRunningTrackSettings();return "torch" in settings;
					//console.log(html5QrCode.getRunningTrackCameraCapabilities());
					const stream = html5QrCode.renderedCamera.mediaStream;
					const track = stream.getVideoTracks()[0];
					const capabilities = track.getCapabilities();

					// If camera light, use it
					if ( autoLight && capabilities.torch )
					{
						track.applyConstraints({
							advanced: [{torch: true}]
						})
						.catch(e => console.log(e));
					}

					// If zoom, use it
					if ( autoZoom && capabilities.zoom )
					{
						track.applyConstraints({
							advanced: [{zoom: autoZoom}]
						})
						.catch(e => console.log(e));
					}
				})
				.catch((err) => {
					// Start failed, handle it

					// Keep it simple
					html5QrCode.start({facingMode: "environment"}, config, onScanSuccess, onScanFailure);
				});
			}).catch(err => {
				// handle err
			});
		}
		
		// Example using Html5Qrcode to iterate over devices
		/*
		// This method will trigger user permissions
		Html5Qrcode.getCameras().then(devices => {
			// devices would be an array of objects of type: { id: "id", label: "label" }
			if (devices && devices.length)
			{
				console.log(devices);
				var cameraId = devices[0].id;
				// .. use this to start scanning.
				console.log(cameraId);
			}
		}).catch(err => {
			// handle err
		});
		*/

		// Example using the last camera, which is most likely primary with torch
		/*
		// Work on checking for camera with torch and using it
		if ( navigator.mediaDevices?.enumerateDevices )
		{
			let cameraId;
			// List cameras and microphones.
			navigator.mediaDevices.enumerateDevices()
			.then((devices) => {
				// Filter devices
				const cameras = devices.filter((device) => device.kind === 'videoinput');
				// Last camera
				cameraId = cameras[cameras.length - 1].deviceId;
			})
			.then(() => {
				html5QrCode.start(cameraId, config, onScanSuccess, onScanFailure)
				// https://medium.com/qr-code/using-flash-or-torch-with-html5-qr-code-library-ba931965fe78
				.then(() => {
					const stream = html5QrCode.renderedCamera.mediaStream;
					const track = stream.getVideoTracks()[0];
					const capabilities = track.getCapabilities();
					
					if (capabilities.torch) {
						track.applyConstraints({
							advanced: [{torch: true}]
						})
						.catch(e => console.log(e));
					}
					if (capabilities.zoom) {
						track.applyConstraints({
							advanced: [{zoom: 2}]
						})
						.catch(e => console.log(e));
					}
				})
				.catch((err) => {
					console.error(`${err.name}: ${err.message}`);
				});
			})
			.catch((err) => {
				// Start failed, handle it
			});
		}
		*/
		
		// Example using constraints to get a camera
		/*
		console.log(navigator.mediaDevices.getSupportedConstraints());
		let constraints = {
			video: {// {deviceId: cameraId, facingMode: "environment", zoom: true, torch: true }
				facingMode: "environment"
			}
		};
		navigator.mediaDevices.getUserMedia(constraints)
		.then((stream) => {
			// use the stream
			console.log(stream.getVideoTracks()[0].getCapabilities());
		})
		.catch((err) => {
			// handle the error
			console.log(err);
		});
		*/

		// Example iterating over video devices and activating each to check capabilities, crashes mobile
		/*
		let cameraId;
		// https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/enumerateDevices
		// https://webrtc.org/getting-started/media-devices
		if ( navigator.mediaDevices?.enumerateDevices )
		{
			const supportedConstraints = navigator.mediaDevices.getSupportedConstraints();
			console.log(supportedConstraints);
			
			// List cameras and microphones.
			navigator.mediaDevices.enumerateDevices()
			.then((devices) => {
				console.log(devices);
				// Filter devices
				const cameras = devices.filter((device) => device.kind === 'videoinput');

				if (cameras.length === 0) {
					throw 'No camera found on this device.';
				}

				// Last camera may be primary with most features
				let lastCamera = cameras[cameras.length - 1].deviceId;
				let cameraIdEnvironment;
				let cameraIdEnvironmentWithTorch;

				// Iterate over devices
				cameras.forEach((camera) => {
					console.log(`${camera.kind}: ${camera.label} id = ${camera.deviceId}`);

					// Create stream and get video track
					navigator.mediaDevices.getUserMedia({
						video: {
							deviceId: camera.deviceId,
							facingMode: "environment"
						}
					}).then(stream => {
						// Check stream capabilities and set variables
						const track = stream.getVideoTracks()[0];
						const capabilities = track.getCapabilities();
						console.log(capabilities);
						if ( capabilities.torch )
						{
							cameraIdEnvironmentWithTorch = camera.deviceId;
							
							// https://stackoverflow.com/questions/66724191/how-to-light-mobile-phone-flash-from-a-website
							// https://stackoverflow.com/questions/37848494/is-it-possible-to-control-the-camera-light-on-a-phone-via-a-website
							// https://oberhofer.co/mediastreamtrack-and-its-capabilities/
						}
						else
						{
							cameraIdEnvironment = camera.deviceId;
						}
						cameraId = cameraIdEnvironmentWithTorch ?? cameraIdEnvironment;

						// Stop stream
						stream.getTracks()
						.forEach(function(track) {
							track.stop();
						});
					});
				});

			})
			.then(() => {

				// Create stream and get video track
				navigator.mediaDevices.getUserMedia({
					video: {
						deviceId: cameraId,
						facingMode: "environment",
						zoom: true,
						torch: true
					}
				}).then(stream => {
					// Check stream capabilities and set variables
					const track = stream.getVideoTracks()[0];
					const capabilities = track.getCapabilities();
					const settings = track.getSettings();
					console.log('capabilities', capabilities);
					console.log('settings', settings);

					// https://googlechrome.github.io/samples/image-capture/update-camera-zoom.html
					// Stop stream
					stream.getTracks()
					.forEach(function(track) {
						track.stop();
					});
				});

			})
			.catch((err) => {
				console.error(`${err.name}: ${err.message}`);
			});
		}
		*/
	}
	catch ( error )
	{
		console.error(error);
	}
}

function stopScanning ( event )
{
	if ( event )
	{
		event.preventDefault();
	}

	try
	{
		html5QrCode.stop().then((ignore) => {
			// QR Code scanning is stopped.
		}).catch((err) => {
			// Stop failed, handle it.
		});
	}
	catch ( error )
	{
		console.error(error);
	}
	
	// Hide scanner
	animateCSS('#qrcodeReaderContainer', 'fadeOut').then((message) => {
		jQuery('#qrcodeReaderContainer').removeClass('scanner-overlay');
	});
}

docReady(function () {
});