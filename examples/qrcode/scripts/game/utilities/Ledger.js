/**
 * 
 * Usage:

let MyLedger = new Ledger();


let MyLedger = new Ledger()
.add([
	{type: 'challenge', action: 'add', id: 1},
	{type: 'challenge', action: 'activate', id: 1},
	{type: 'item', action: 'add', id: 2},
	{type: 'item', action: 'activate', id: 2},
	{type: 'item', action: 'use', id: 1},
	{type: 'item', action: 'use', id: 2},
	{type: 'challenge', action: 'complete', id: 1},
]);
MyGame.logger.debug(MyLedger.ledger);
// Find match
MyGame.logger.debug(MyLedger.check('match', {entry: {type: 'item', action: 'add', id: 2}}));
// Find matches
MyGame.logger.debug(MyLedger.check('match', {entries: [
	{type: 'item', action: 'use', id: 2},
	{type: 'item', action: 'use', id: 1},
]}));
// Find order
MyGame.logger.debug(MyLedger.check('order', {entries: [
	{type: 'item', action: 'use', id: 1},
	{type: 'item', action: 'use', id: 2},
]}));

 */
class Ledger
{
	// Properties

	// Public
	
	ledger;

	// Methods

	/**
	 * 
	 * @returns object
	 */
    constructor () {
		this.reset();
		return this; // For method chaining
    }
	
	/**
	 * 
	 * @returns object
	 */
	reset () {
		this.ledger = [];
		return this; // For method chaining
	}

	/**
	 * 
	 * @returns object
	 */
	add ( entries ) {
		if ( ! Array.isArray(entries) )
		{
			entries = [entries];
		}

		for ( let entry of entries )
		{
			// If a time isn't set, add now
			if ( ! Reflect.has(entry, 'time') )
			{
				entry.time = Date.now();
			}

			this.ledger.push(entry);
		}

		return this; // For method chaining
	}

	/**
	 * 
	 * @returns object
	 */
	check ( checkType, params )
	{
		// If checking for a single or multiple matches
		if ( checkType == 'match' || checkType == 'exists' )
		{
			let matchData = this.findMatch(params);
			return ( matchData ) ? ( matchData.indexes.length > 0 ) : false;
		}
		// If checking for the amount of matches
		else if ( checkType == 'count' )
		{
			let matchData = this.findMatch(params);
			if ( matchData && matchData.matches.length > 0 )
			{
				return matchData.matches[0].length;
			}
			
			return 0;
		}
		// If checking for a single or multiple matches and their order
		else if ( checkType == 'order' )
		{
			let matchData = this.findMatch(params);

			// If matches were found
			if ( matchData && matchData.indexes.length > 0 )
			{
				/*
				// Numeric sort by values
				// https://www.w3schools.com/js/js_array_sort.asp
				let sortedIndexes = Array.from(matchData.indexes).sort(function(a, b){
					return a - b;
				});

				// Check if the unsorted and sorted arrays match
				let arraysMatch = matchData.indexes.every((value, index) => {
					return (value === sortedIndexes[index]);
				});
				*/

				// Check if the unsorted and sorted arrays match exactly
				let arraysMatch = matchData.entryExpectedOrder.every((value, index) => {
					return (value === matchData.entryMatchOrder[index]);
				});

				// Check if expected order happened in unsorted in series, not exact (1, 2, 3 found in 1, 1, 2, 3)
				/*
				let arraySeriesMatch = false;
				for ( let matchIndex = 0; matchIndex < matchData.entryMatchOrder.length; matchIndex++ )
				{
					// Look ahead
					for ( let expIndex = 0; expIndex < matchData.entryExpectedOrder.length; expIndex++ )
					{
						let expOrder = matchData.entryExpectedOrder[expIndex];
						// Start from where we are at in the parent loop
						let matchOrder = matchData.entryMatchOrder[matchIndex + expIndex];

						// If not the expected order, skip
						if ( expOrder !== matchOrder )
						{
							break;
						}

						// If made it to the end of expected orders, the order was found in series
						if ( expIndex === matchData.entryExpectedOrder.length - 1 )
						{
							arraySeriesMatch = true;
						}
					}
				}
				*/
				// This may be far simpler by .join(',') and then checking for substring existence
				let matchOrderString = '|' + matchData.entryMatchOrder.join('|') + '|';
				let expectedOrderString = '|' + matchData.entryExpectedOrder.join('|') + '|';
				let arraysSeriesMatch = matchOrderString.includes(expectedOrderString);

				// Not looking at order, were at least all the expected records found
				let arrayContainsAll = matchData.entryExpectedOrder.every(element => {
					return matchData.entryMatchOrder.indexOf(element) !== -1;
				});

				// If arrays match, they are in the expected order
				return {
					match: arraysMatch,
					seriesMatch: arraysSeriesMatch,
					containsAll: arrayContainsAll
				};
			}
		}
	}

	/**
	 * 
	 * @returns object
	 */
	findMatch ( params )
	{
		/* // Example params
		{
			entry: {
				type: 'item',
				action: 'add',
				id: 0,
				time: 0
			},
			// OR
			entries: [{
				type: 'item',
				action: 'add',
				id: 0,
				time: 0
			}],
			minTime: 0,
			maxTime: 0,
			limitCheckToLength: true
		}
		*/

		// Stop if no entries passed
		if ( ! Reflect.has(params, 'entries') && ! Reflect.has(params, 'entry') )
		{
			return;
		}

		// If entry is set but not entries, normalize to entries array
		if ( ! Reflect.has(params, 'entries') && Reflect.has(params, 'entry') )
		{
			params.entries = [params.entry];
		}

		// Keep track of the matched indexes and their order
		let matchIndexes = [];
		let entryMatchOrder = [];
		let entryExpectedOrder = [];
		// Keep track of the amount of matches
		let matches = [];
		// Create initial entries for later population
		for ( let index = 0; index < params.entries.length; index++ )
		{
			matches.push([]);
			entryExpectedOrder.push(index);
		}

		// https://stackoverflow.com/questions/3010840/loop-through-an-array-in-javascript
		// For each entry
		//for ( let index = 0; index < this.ledger.length; index++ )
		for ( let [index, entry] of this.ledger.entries() )
		{
			// If checking minimum time and entry is less than minimum, stop
			if ( params.minTime && entry.time < params.minTime )
			{
				continue;
			}
			
			// If checking maximum time and entry is greater than maximum, stop
			if ( params.maxTime && entry.time > params.maxTime )
			{
				continue;
			}
			
			// For each entry to check
			for ( let [passedEntryIndex, passedEntry] of params.entries.entries() )
			{
				// Assume match
				let match = true;
				
				// For each passed entry param to check
				for ( let passedEntryParam of Object.keys(passedEntry) )
				{
					// If param doesn't match entry, stop
					if ( passedEntry[passedEntryParam] !== entry[passedEntryParam] )
					{
						match = false;
						break;
					}
				}
	
				// If there was a match, add it and continue to next passed entry
				if ( match )
				{
					// Add match association between passed entry index and ledger index
					entryMatchOrder.push(passedEntryIndex);
					matchIndexes.push(index);

					// Keep track of multiple match indexes
					matches[passedEntryIndex].push(index);

					continue;
				}
			}

			// If not including the count and the amount to match has been found, stop
			// This means if you are supposed to find 1, 2, 3 and 1, 1, 2, 3 are in the ledger, it will stop at 1, 1, 2 because there were 3 entries to match and it did
			if ( params.limitCheckToLength && matchIndexes.length === params.entries.length )
			{
				break;
			}
		}

		let matchData = {
			// Array of ledger indexes matched
			indexes: matchIndexes,
			// Array of param entry indexes with array of all matching ledger indexes
			matches: matches,
			// Order of param entries matched in ledger
			entryMatchOrder: entryMatchOrder,
			// Expected order of param entries matched in ledger
			entryExpectedOrder: entryExpectedOrder
		};
		
		return matchData;
	}

}