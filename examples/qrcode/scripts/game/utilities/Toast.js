/*
<!-- Position it: -->
<!-- - `.toast-container` for spacing between toasts -->
<!-- - `top-0` & `end-0` to position the toasts in the upper right corner -->
<!-- - `.p-3` to prevent the toasts from sticking to the edge of the container  -->
<div class="toast-container position-fixed top-0 end-0 p-3">

	<!-- Then put toasts within -->
	
</div>
*/
class Toast
{
	$toastContainer;
	toastContainerSelector = '.toast-container';
	toastHeaderSelector = '.toast-header';
	toastHeaderTitleSelector = '.toast-header__title';
	toastHeaderDateSelector = '.toast-header__date';
	toastBodySelector = '.toast-body';
	toastBodyContentSelector = '.toast-body__content';
	
	// Templates
	toastTemplate = `
<div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
	<div class="toast-header">
		<strong class="me-auto toast-header__title">Bootstrap</strong>
		<small class="text-muted toast-header__date">just now</small>
		<button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
	</div>
	<div class="toast-body">
		<div class="toast-body__content">Hello, world! This is a toast message.</div>
		<div class="mt-2 pt-2 border-top toast-ctas">
			<button type="button" class="btn btn-primary btn-sm" data-bs-dismiss="toast">Take action</button><!-- also close toast -->
			<button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="toast">Close</button>
		</div>
	</div>
</div>
`;
	toastTemplateMinimal = `
<div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">
	<div class="d-flex">
		<div class="toast-body">
			<div class="toast-body__content">Hello, world! This is a toast message.</div>
		</div>
		<button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
	</div>
</div>
`;
	toastTemplateColor = `
<div class="toast align-items-center text-bg-primary border-0" role="alert" aria-live="assertive" aria-atomic="true">
	<div class="d-flex">
		<div class="toast-body">
			<div class="toast-body__content">Hello, world! This is a toast message.</div>
		</div>
		<button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
	</div>
</div>
`;

	constructor ()
	{
		// Reference this instance to set references using jQuery when available
		let toastInstance = this;

		(function() {
			let nTimer = setInterval(function() {
				if (window.jQuery) {
					// Do something with jQuery
					clearInterval(nTimer);
					
					jQuery(document).ready(function($)
					{
						// Set references
						toastInstance.$toastContainer = $(toastInstance.toastContainerSelector);
		
						// Create examples
						//MyToast.create(toastTemplate, 'Hello, world! This is a toast message.');
						//MyToast.create(toastTemplateMinimal, 'Hello, world! This is a toast message.');
						//MyToast.create(toastTemplateColor, 'Hello, world! This is a toast message.');
						
					});
				}
			}, 100);
		})();
	}

	create ( toastTemplate, params )
	{
		let $toast = $(toastTemplate)
		.appendTo(this.$toastContainer);
		// Configure and show the toast
		$toast.toast({
			animation: true,
			autohide: true,
			delay: 3000
		})
		.toast('show')
		// When toast is hidden, remove it from the DOM
		.on('hidden.bs.toast', function(event){
			$(this).remove();
		});

		if ( toastTemplate == this.toastTemplateColor )
		{
			// Update toast contents
			$toast.find(this.toastBodyContentSelector)
			.html(params.body);

			if ( params.color )
			{
				$toast.removeClass('text-bg-primary').addClass(`text-bg-${params.color}`)
			}
		}
		else if ( toastTemplate == this.toastTemplate )
		{
			$toast.find(this.toastHeaderTitleSelector)
			.html(params.title);
			$toast.find(this.toastBodyContentSelector)
			.html(params.body);
			
			if ( params.ctas )
			{
				if ( params.ctas.primary )
				{
					$toast.find('.toast-ctas .btn-primary')
					.addClass(params.ctas.primary.classAddon)
					.html(params.ctas.primary.label)
					.attr(params.ctas.primary.attrObj);
				}
			}
		}

		return $toast;
	}

}
