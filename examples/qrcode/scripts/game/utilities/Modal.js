class Modal
{
	// Templates
	static modalButtonTemplate = `
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
	Launch demo modal
</button>
`;

	static modalTemplate = `
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<!-- Modal -->
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle"></h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<!--
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
				-->
				<button type="button" class="btn btn-sm btn-secondary button--cancel">Cancel</button>
				<button type="button" class="btn btn-sm btn-primary button--confirm">Ok</button>
			</div>
		</div>
	</div>
</div>
`;

	static leftAvatarTemplate = `<div class="modal-avatar--left"><img alt="Avatar" class="modal-avatar__image img-fluid"></div>`;
	static rightAvatarTemplate = `<div class="modal-avatar--right"><img alt="Avatar" class="modal-avatar__image img-fluid"></div>`;

	// Keep track of the number of modals being displayed for race conditions
	// Keep track here (true) or in other global modal events (show/hidden)
	static track = false;
	static numShown = 0;
	static shown = false;

	constructor ()
	{
	}

	static show ( instance, modalOptions = {} )
	{
		if ( instance )
		{
			instance.modal(modalOptions).modal('show');
		}

		if ( Modal.track )
		{
			Modal.shown = true;
			Modal.numShown++;
		}
	}

	static hide ( instance )
	{
		if ( instance )
		{
			instance.modal('hide');
		}

		if ( Modal.track )
		{
			Modal.shown = false;
			Modal.numShown--;
		}
	}

	static createFromTemplate ( template )
	{
		if ( ! template )
		{
			template = Modal.modalTemplate;
		}

		// Create a unique id
		let uniqueId = Date.now();
		let modalId = `modal-${uniqueId}`;
		let modalTitleId = `modal-title-${uniqueId}`;
		let modalIdSelector = `#${modalId}`;

		let $modal = $(template).appendTo('body').attr('id', modalId).attr('aria-labelledby', modalTitleId);
		let $modalHeader = $modal.find('.modal-header');
		let $modalTitle = $modal.find('.modal-title').attr('id', modalTitleId);
		let $modalBody = $modal.find('.modal-body');
		let $modalFooter = $modal.find('.modal-footer');
		
		//let $modalButtonTemplate = $(modalButtonTemplate).appendTo('.modal-container').attr('data-bs-target', modalIdSelector);
		
		// Remove modal when hidden
		const modalElement = document.getElementById(modalId);
		/*
		modalElement.addEventListener('hidden.bs.modal', event => {
			modalElement.remove();
		});
		*/

		let data = {
			uniqueId: uniqueId,
			modalId: modalId,
			modalTitleId: modalTitleId,
			modalIdSelector: modalIdSelector,
			modalElement: modalElement,
			modalInstance: bootstrap.Modal.getInstance(modalElement),
			$modal: $modal,
			$modalHeader: $modalHeader,
			$modalTitle: $modalTitle,
			$modalBody: $modalBody,
			$modalFooter: $modalFooter
		}

		return data;
	}

	static getShownTemplate ()
	{
		// Reverse-engineer modal details
		const $modal = $('.modal.show');
		// If no open modal, stop
		if ( $modal.length == 0 )
		{
			return;
		}

		let $modalHeader = $modal.find('.modal-header');
		let $modalTitle = $modal.find('.modal-title');
		let $modalBody = $modal.find('.modal-body');
		let $modalFooter = $modal.find('.modal-footer');

		let modalId = $modal.attr('id');
		let modalIdSelector = `#${modalId}`;
		let modalTitleId = $modalTitle.attr('id');
		let uniqueId = modalId.slice('modal-'.length);
		
		// Remove modal when hidden
		let modalElement = document.getElementById(modalId);

		// Update Bootstrap
		//const bsModal = bootstrap.Modal.getInstance($modal[0]);//document.querySelector('.modal.show')
		//bsModal.handleUpdate();
		
		let data = {
			uniqueId: uniqueId,
			modalId: modalId,
			modalTitleId: modalTitleId,
			modalIdSelector: modalIdSelector,
			modalElement: modalElement,
			modalInstance: bootstrap.Modal.getInstance(modalElement),
			$modal: $modal,
			$modalHeader: $modalHeader,
			$modalTitle: $modalTitle,
			$modalBody: $modalBody,
			$modalFooter: $modalFooter
		}

		return data;
	}

	// https://getbootstrap.com/docs/5.2/components/modal/
	static create ( params )
	{
		// Assign default params
		params = Object.assign({}, {
			body: '...',
			title: 'Modal title',
			dataOnly: false,
			modalParams: {}
			//cancelLabel
			//okLabel
		}, params);

		let modalData = Modal.createFromTemplate();

		modalData.$modalTitle.html(MyGame.parseShortcodes(params.title));
		modalData.$modalBody.html(MyGame.parseShortcodes(params.body));

		// If changing modal background
		if ( params.background )
		{
			modalData.$modal.find('.modal-content').css({
				backgroundImage: `url("${MyGame.parseShortcodes(params.background)}")`, 
				backgroundPosition: '50% 50%', 
				backgroundSize: 'cover'
			});
		}

		// If hiding buttons
		if ( params.cancelButtonHide )
		{
			modalData.$modal.find('.button--cancel').addClass('d-none');
		}

		if ( params.confirmButtonHide )
		{
			modalData.$modal.find('.button--confirm').addClass('d-none');
		}

		// If changing button labels
		if ( params.cancelButtonLabel )
		{
			modalData.$modal.find('.button--cancel').html(MyGame.parseShortcodes(params.cancelButtonLabel));
		}

		if ( params.confirmButtonLabel )
		{
			modalData.$modal.find('.button--confirm').html(MyGame.parseShortcodes(params.confirmButtonLabel));
		}

		// If showing left avatar
		if ( params.leftAvatar )
		{
			let $leftAvatar = $(Modal.leftAvatarTemplate).prependTo(modalData.$modalBody);
			let $leftAvatarImage = $leftAvatar.find('.modal-avatar__image');
			$leftAvatarImage.attr('src', MyGame.parseShortcodes(params.leftAvatar));

			// If the image should flip so it looks the other way, add class
			if ( params.leftAvatarFlip )
			{
				$leftAvatarImage.addClass('flip--h');
			}
		}

		// If showing right avatar
		if ( params.rightAvatar )
		{
			let $rightAvatar = $(Modal.rightAvatarTemplate).prependTo(modalData.$modalBody);
			let $rightAvatarImage = $rightAvatar.find('.modal-avatar__image');
			$rightAvatarImage.attr('src', MyGame.parseShortcodes(params.rightAvatar));

			// If the image should flip so it looks the other way, add class
			if ( params.rightAvatarFlip )
			{
				$rightAvatarImage.addClass('flip--h');
			}
		}

		// If showcase
		if ( params.showcase )
		{
			modalData.$modal.addClass('modal--showcase');
			modalData.$modal.find('.modal-content')
			.children()
			.addClass('justify-content-center text-center');
			modalData.$modal.find('.btn-close').addClass('d-none');
		}

		//let $modalButtonTemplate = $(modalButtonTemplate).appendTo('.modal-container').attr('data-bs-target', modalIdSelector);
		
		// Remove modal when hidden
		const modalElement = document.getElementById(modalData.modalId);
		/*
		modalElement.addEventListener('hidden.bs.modal', event => {
			modalElement.remove();
		});
		*/

		if ( params.dataOnly )
		{
			return modalData;
		}

		// https://stackoverflow.com/questions/62902960/show-bootstrap-modal-before-proceeding
		// https://stackoverflow.com/questions/41815905/wait-value-in-bootstrap-confirm-modal
		// https://www.freecodecamp.org/news/javascript-es6-promises-for-beginners-resolve-reject-and-chaining-explained/
		const modalPromise = new Promise(function(resolve, reject){
			$(modalIdSelector).modal('show');
			$(`${modalIdSelector} .button--confirm`).click(function(){
				resolve("user clicked");
				// Hide modal
				$(modalIdSelector).modal('hide');
			});
			$(`${modalIdSelector} .button--cancel`).click(function(){
				reject("user clicked cancel");
			});
		}).then(function(val){
			//val is your returned value. argument called with resolve.
			//alert(val);
		}).catch(function(err){
			//user clicked cancel
			console.log("user clicked cancel", err);

			// Hide modal
			$(modalIdSelector).modal('hide');
		});
	}

}
