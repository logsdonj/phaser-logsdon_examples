const qrcodeDatabase = {
	"game": {
		"version": "0.0.0",
		"name": "Quest Game",
		"description": "Adventure into new lands in search of mythical treasure.",
		// Optional: Language used in the game
		"lexicon": {
			"challenges": {
				"activateVerb": "start",
				"deactivateVerb": "stop",
				"useVerb": "go"
			},
			"items": {
				"activateVerb": "equip",
				"deactivateVerb": "unequip",
				"useVerb": "use"
			},
			"characters": {
				"activateVerb": "visit",
				"deactivateVerb": "leave",
				"useVerb": "interact"
			},
			"locations": {
				"activateVerb": "visit",
				"deactivateVerb": "leave",
				"useVerb": "explore"
			},
			"combinations": {
				"combineVerb": "combine",
				"combineVerbPast": "combined"
			}
		},
		// Optional: Parameters used as defaults for the game
		"params": {
			"showcase": true
		},
		// Optional: Popup information for different points in the game
		"intro": "Intros can be any HTML, including a video.",
		"outtro": "Outros can be any HTML, including a video.",
		"overOuttro": "Outros can be any HTML, including a video.",
		// Completion requirements
		"requirements": {
			"challenges": {
				"1": ""/*,
				"2": "",
				"3": "",
				"4": ""*/
			},
			"playerStats": {
				"health": 1
			},
			"timeLimit": 1
		},
		// Optional: An action that must happen to submit the game for completion
		"submitTriggers": { // Optional
			"type": "item",
			"action": "use",
			"id": "1"
		},
		"interactionDefaults": {
			"background": "images\/backgrounds\/nordwood-themes-KcsKWw77Ovw-unsplash.jpg"
		},
		"interactionManifest": {
			// A set of dialog entries grouped in an array with no next value needed
			"welcome": {
				"series": [
					{
						"text": "Welcome to the game!",
						"leftAvatar": "images\/characters\/c59b64a6176f16d87eb2f4bdbe7f.jpeg",
						"payload": {
							"actions": [
								{
									"action": "showScreen",
									"params": {
										"screen": "main",
										"prevScreen": "title"
									}
								}
							]
						}
					},
					{
						"text": "To get back to the title screen, click <i class=\"icon-home icon--fa\"><\/i> at the top left of the screen.",
						"rightAvatar": "images\/characters\/fc1a8132664d204da8cdc164eafb.jpeg",
						"rightAvatarFlip": true
					},
					{
						"text": "Keep track of your <i class=\"icon-health icon--fa\"><\/i> Health, <i class=\"icon-exp icon--fa\"><\/i> Experience, <i class=\"icon-special icon--fa\"><\/i> Special, <i class=\"icon-currency icon--fa\"><\/i> Currency, and <i class=\"icon-points icon--fa\"><\/i> Points at the top of the screen.",
						"leftAvatar": "images\/characters\/ff37f49e0256ee5a95be.jpeg"
					},
					{
						"text": "Below your stats are status updates and helpful information to progress.",
						"rightAvatar": "images\/characters\/d5defb2d1355bf1c2091ac94dc23.jpeg",
						"rightAvatarFlip": true
					},
					{
						"text": "Below your status, capture codes as you find them using <i class=\"icon-scan icon--fa\"><\/i>. If they are stored on your device, upload them using <i class=\"icon-upload icon--fa\"><\/i>.",
						"leftAvatar": "images\/characters\/832d7ddf0fe356d8e71770382e8a.jpeg"
					},
					{
						"text": "At the bottom of the screen, you can access your <i class=\"icon-challenges icon--fa\"><\/i> Challenges, <i class=\"icon-items icon--fa\"><\/i> Items, <i class=\"icon-locations icon--fa\"><\/i> Locations, and <i class=\"icon-characters icon--fa\"><\/i> Characters. In these screens you can manage them and view more detailed information.",
						"rightAvatar": "images\/characters\/c59b64a6176f16d87eb2f4bdbe7f.jpeg",
						"rightAvatarFlip": true
					},
					{
						"text": "That's it. Good luck!"
					}
				]
			}
		}
	},
	"prompts": {
		"prompt1": {
			"questions": [
				{
					"type": "choice",
					"title": "Quiz Example",
					"text": "What do you think?",
					"choices": {
						"choice1": {
							"text": "Choice 1",
							"correct": true
						},
						"choice2": {
							"text": "Choice 2"
						},
						"choice3": {
							"text": "Choice 3"
						}
					}
				}
			],
			"shuffleChoices": true,
			"success": {
				"message": "<h6 class=\"mt-2\">Correct!</h6>",
				"payload": {
					"items": {
						"20": ""
					}
				}
			},
			"failure": {
				"message": "<h6 class=\"mt-2\">Wrong!</h6>"
			}
		},
		"prompt2": {
			"questions": [
				{
					"type": "fill-in",
					"text": "What is 20 + 1?",
					"correctPool": [
						"21",
						"Twenty-one"
					]
				}
			],
			"success": {
				"message": "<h6 class=\"text-success mt-2\">Correct!</h6>",
				"payload": {
					"items": {
						"20": ""
					}
				}
			},
			"failure": {
				"message": "<h6 class=\"text-danger mt-2\">Wrong!</h6>"
			}
		},
		"prompt3": {
			"type": "quiz",
			"title": "Quiz Example",
			"questions": [
				{
					"text": "What is 0 + 0?",
					"choices": {
						"choice1": {
							"text": "0",
							"correct": true
						},
						"choice2": {
							"text": "1"
						},
						"choice3": {
							"text": "2"
						}
					}
				},
				{
					"text": "What is 1 + 1?",
					"choices": {
						"choice1": {
							"text": "1"
						},
						"choice2": {
							"text": "2",
							"correct": true
						},
						"choice3": {
							"text": "3"
						}
					}
				},
				{
					"text": "What is 2 + 2?",
					"choices": {
						"choice1": {
							"text": "2"
						},
						"choice2": {
							"text": "3"
						},
						"choice3": {
							"text": "4",
							"correct": true
						}
					}
				},
				{
					"text": "What is 20 + 1?",
					"correctPool": [
						"21",
						"Twenty-one"
					]
				}
			],
			"shuffleQuestions": true,
			"shuffleChoices": true,
			"success": {
				"message": "<h6 class=\"mt-2\">Correct!</h6>",
				"passingPercent": 75,
				"payload": {
					"items": {
						"20": ""
					}
				}
			},
			"failure": {
				"message": "<h6 class=\"mt-2\">Wrong!</h6>"
			}
		}
	},
	"items": {
		"1": {
			"id": "1",
			"type": "weapon",
			"name": "Inventoriable 1",
			"description": "Has a type (weapon) to be sorted and hints, showcase with image, custom useVerb | <u>OnUse:</u> Does nothing, no stock reduction",
			"notes": "Example item notes.",
			"hints": [
				"Use it this way.",
				"Use it that way."
			],
			"showcase": 1,
			"image": "images/characters/fc1a8132664d204da8cdc164eafb.jpeg",
			// Whether to track in item inventory system
			"isInventory": 1,
			"useVerb": "eat",
			"useVerbPast": "eaten"
		},
		"2": {
			"id": "2",
			"type": "food",
			"name": "Inventoriable 2",
			"description": "Has a type (food) to be sorted and no showcase | <u>Stock:</u> 5 | <u>OnUse:</u> Does nothing, reduce stock",
			"isInventory": 1,
			"showcase": 0,
			"stockUpdateOnUse": -1,
			"stock": 5
		},
		"3": {
			"id": "3",
			"name": "Inventoriable 3",
			"description": "<u>Item Stats:</u> Health 2\/Strength 10\/Damage 2 | <u>OnUse:</u> Does nothing, Item Stats Health -1, reduce stock when 0 vs stockUpdateOnUse",
			"isInventory": 1,
			"stats": {
				"strength": 10,
				"damage": 2,
				"health": 2 // When item health is at 0, 1 stock is removed
			},
			"statsUpdateOnUse": {
				"health": -1 // When item health is at 0, 1 stock is removed
			}
		},
		"4": {
			"id": "4",
			"name": "Inventoriable 4",
			"description": "<u>maxStock:</u> 2 | <u>Throttle:</u> 5s add and use | <u>OnUse:</u> Reduce stock, Player Health +10",
			"isInventory": 1,
			"stockUpdateOnUse": -1,
			// Optional: Maximum item stock
			"maxStock": 2,
			"playerStatsUpdateOnUse": {
				"health": 10
			},
			"addThrottleMs": 5000,
			"useThrottleMs": 5000
		},
		"5": {
			"id": "5",
			"name": "Inventoriable 5",
			"description": "<u>OnActivate:</u> Player Stat Special +5 | <u>OnUse:</u> Does nothing, no stock reduction",
			"isInventory": 1,
			"playerStatsUpdateOnActivate": {
				"special": 5
			}
		},
		"6": {
			"id": "6",
			"name": "Inventoriable 6",
			"description": "<u>OnActivate:</u> Add | <u>OnUse:</u> Reduce stock, Player Stats Exp +5",
			"isInventory": 1,
			"stockUpdateOnUse": -1,
			"playerStatsUpdateOnUse": {
				"exp": 5
			},
			"activateOnAdd": 1
		},
		"7": {
			"id": "7",
			"name": "Inventoriable 7",
			"description": "<u>OnAdd:</u> Use | <u>OnUse:</u> Reduce stock, Player Stats Currency +1",
			"isInventory": 1,
			"stockUpdateOnUse": -1,
			"playerStatsUpdateOnUse": {
				"currency": 1
			},
			"useOnAdd": 1
		},
		"8": {
			"id": "8",
			"name": "Inventoriable 8",
			"description": "<u>OnActivate:</u> Use | <u>OnUse:</u> Reduce stock, Player Stats Points +3",
			"isInventory": 1,
			"stockUpdateOnUse": -1,
			"playerStatsUpdateOnUse": {
				"points": 3
			},
			"useOnActivate": 1
		},
		"9": {
			"id": "9",
			"name": "Inventoriable 9",
			"description": "Inventoriable, not usable so player can't reduce stock and it can be passed around in game as needed",
			"isInventory": 1,
			"isUsable": 0
		},
		"20": {
			"id": "20",
			"name": "Consumable 1",
			"description": "<u>OnUse:</u> Player Stats Health +5",
			"playerStatsUpdateOnUse": {
				"health": 5
			}
		},
		"21": {
			"id": "21",
			"name": "Poison",
			"description": "<u>OnUse:</u> createStatTimer, Player Stats Health -1 every second",
			"payload": {
				"actions": [
					{
						"action": "createStatTimer",
						"params": {
							"id": "createStatTimer_Item_21",
							"type": "player",
							"stat": "health",
							"valueIncrement": -33
						}
					}
				]
			}
		},
		"22": {
			"id": "22",
			"name": "Antidote",
			"description": "<u>OnUse:</u> destroyStatTimer, stop Player Stats Health drain",
			"payload": {
				"actions": [
					{
						"action": "destroyStatTimer",
						"params": {
							"id": "createStatTimer_Item_21"
						}
					}
				]
			}
		},
		"23": {
			"id": "23",
			"name": "Consumable Triggers Quiz Prompt",
			"description": "<u>OnUse:</u> Prompt 1 Quiz",
			"showcase": 0,
			"payload": {
				"actions": {
					"action": "prompt",
					"params": {
						"promptId": "prompt1"
					}
				}
			}
		},
		"24": {
			"id": "24",
			"name": "Consumable Triggers Fill In Prompt",
			"description": "<u>OnUse:</u> Prompt 2 Fill In",
			"showcase": 0,
			"payload": {
				"actions": {
					"action": "prompt",
					"params": {
						"promptId": "prompt2"
					}
				}
			}
		}
	},
	"challenges": {
		"1": {
			"id": "1",
			"name": "Challenge 1",
			"description": "Instructions, intro, outtro, hints | <u>Prereqs:</u> None | <u>Reqs:</u> Item 1 | <u>Success:</u> 20 Health, etc.",
			"notes": "Example notes",
			"intro": "Intros can be any HTML, including a video.",
			"outtro": "Outros can be any HTML, including a video.",
			"hints": [
				"Go down there.",
				"Don't go down there."
			],
			"requirements": {
				"items": {
					"1": ""
				}
			},
			"success": {
				"playerStats": {
					"health": 20,
					"currency": 20,
					"points": 20,
					"exp": 20
				}
			}
		},
		"2": {
			"id": "2",
			"name": "Challenge 2",
			"description": "<u>OnAdd:</u> Activate | <u>Prereqs:</u> Challenge 1 | <u>Reqs:</u> 2 Item 2 and 3 Item 3 | <u>Submit:</u> Use Item 1",
			"activateOnAdd": true,
			"prerequisites": {
				"challenges": {
					"1": ""
				}
			},
			"requirements": {
				"items": {
					"2": {
						"stock": 2
					},
					"3": {
						"stock": 3
					}
				}
			},
			"submitTriggers": { // Optional
				"type": "item",
				"action": "use",
				"id": "1"
			}
		},
		"3": {
			"id": "3",
			"name": "Challenge 3",
			"description": "<u>OnActivate:</u> Payload | <u>Prereqs:</u> Challenge 2, Item 3, Player Stat Health 100 | <u>Reqs:</u> None | <u>Submit:</u> Visit Location 1",
			"payloadOnActivate": {
				"actions": [
					{
						"action": "interaction",
						"params": {
							"type": "",//challenge
							"id": "",
							"entryId": {
								"text": "Monkeys!"
							}
						}
					}
				]
			},
			"prerequisites": {
				"challenges": {
					"2": ""
				},
				"playerStats": {
					"health": 100
				}
			},
			"submitTriggers": { // Optional
				"type": "location",
				"action": "add",
				"id": "1"
			}
		},
		"4": {
			"id": "4",
			"name": "Challenge 4",
			"description": "<u>Prereqs:</u> None | <u>Reqs:</u> Items 1, 2, and 3 | <u>Time:</u> 30s",
			"requirements": {
				"items": {
					"1": "",
					"2": "",
					"3": ""
				},
				"timeLimit": 30
			}
		},
		"5": {
			"id": "5",
			"name": "Challenge 5",
			"description": "<u>Prereqs:</u> Location 1 | <u>Reqs:</u> Item 4 | <u>Submit:</u> Use Item 4 | <u>Locations:</u> 1",
			"prerequisites": {
				"locations": {
					"1": ""
				}
			},
			"requirements": {
				"items": {
					"4": {
						"stockMin": 0 // In case the last stock used to complete the challenge
					}
				}
			},
			"submitTriggers": { // Optional
				"type": "item",
				"action": "use",
				"id": "4"
			},
			"locations": {
				"1": ""
			}
		},
		"6": {
			"id": "6",
			"name": "Challenge 6",
			"description": "<u>Prereqs:</u> None | <u>Reqs:</u> Items 2, 1, and 3 in that order | <u>Time:</u> 30s",
			"requirements": {
				"ledgers":  [
					{"entries": [
						{"type": "item", "action": "add", "id": "2"},
						{"type": "item", "action": "add", "id": "1"},
						{"type": "item", "action": "add", "id": "3"}
					], "type": "order"}
				],
				"timeLimit": 30
			},
			"activateOnAdd": 1
		},
		"7": {
			"id": "7",
			"name": "Challenge 7",
			"description": "<u>Prereqs:</u> None | <u>Reqs:</u> None | <u>Submit:</u> Use Item 4 | <u>Locations:</u> 1",
			"submitTriggers": { // Optional
				"type": "item",
				"action": "use",
				"id": "4"
			},
			"locations": {
				"1": ""
			}
		},
		"8": {
			"id": "8",
			"name": "Challenge 8",
			"description": "<u>Prereqs:</u> None | <u>Reqs:</u> 2 of either Item 1, 2, or 3",
			"requirements": {
				"2MatchOf": {
					"items": [
						"1",
						"2",
						{
							"3": {"stock": 2}
						}
					]
				}
			}
		},
		"9": {
			"id": "9",
			"name": "Challenge 9",
			"description": "<u>Prereqs:</u> None | <u>Reqs:</u> Complete Prompt 1 in 30 seconds",
			"requirements": {
				"ledgers":  [
					{
						"entries": [
							{"type": "prompt", "id": "prompt3"}
						]
					}
				],
				"timeLimit": 30
			},
			"payloadOnActivate": {
				"actions": {
					"action": "prompt",
					"params": {
						"promptId": "prompt3"
					}
				}
			}
		}
	},
	"locations": {
		"1": {
			"id": "1",
			"name": "Location 1",
			"description": "<u>OnAdd:</u> Activate | <u>OnFirstAdd:</u> Payload Item 1",
			"activateOnAdd": true,
			"payloadOnFirstAdd": {
				"items": {
					"20": ""
				}
			}
		},
		"2": {
			"id": "2",
			"name": "Location 2",
			"description": "<u>OnAdd:</u> Activate",
			"activateOnAdd": true
		}
	},
	"characters": {
		"1": {
			"id": "1",
			"name": "Character 1",
			"description": "Random text, no skip | <u>Conditions:</u> None | <u>OnAdd:</u> Activate",
			"activateOnAdd": true,
			"interactionManifest": {
				"entryTemplate": {
					"leftAvatar": "",
					"rightAvatar": "",
					"background": "",
					"audio": "",
					"text": [
						"Hello",
						"Hola",
						"Bonjour",
						"Aloha",
						"Domo Origato"
					],
					//next: 'entry1',
					"allowSkip": false,
					"payload": {
						"items": {
							"1": ""
						}
					}/*,
					availableConditions: {
						challengesActive: [1],
						challengesComplete: [],
						itemsActive: [],
						items : {
							1 : {
								stockMin: 1
							}
						},
						ledgers :  [
							{entries: [
								{type: 'character', action: 'interaction', id: 2},
								{type: 'character', action: 'interaction', id: 1},
							]}
						]
						
					},
					"unavailableConditions": {
						"ledgers":  [
							{"entries": [
								{"type": "character", "action": "interaction", "id": "1", "entryId": "entryTemplate"}
							]}
						]
					}
					*/
				}
			}
		},
		"2": {
			"id": "2",
			"name": "Character 2",
			"description": "Nested entries | <u>Conditions:</u> Generic available, Special available with Item 2 stockMin 1 | <u>OnAdd:</u> Activate",
			"activateOnAdd": true,
			"interactionManifest": {
				// A nested entry instead of several separated
				"nestedEntry1": {
					"text": "One ring to rule them all...",
					"next": {
						"text": "one ring to find them...",
						"next": {
							"text": "one ring to bring them all...",
							"next": {
								"text": "and in the darkness, bind them."
							}
						}
					},
					"availableConditions": {
						"items" : {
							"2" : {
								"stockMin": 1
							}
						}
					}
				},
				"genericEntry1": {
					"text": "Hey"
				}
			}
		},
		"3": {
			"id": "3",
			"name": "Character 3",
			"description": "Entry with options, Choice 1 with Item 1 Payload | <u>Conditions:</u> Unavailable after Choice 1 | <u>OnAdd:</u> Activate",
			"activateOnAdd": true,
			"interactionManifest": {
				// An entry with options nested
				"optionsEntry": {
					"text": "What do you think?",
					"choices": {
						"choice1": {
							"text": "Choice 1",
							"next": {
								"text": "Ok, 1 it is!",
								"payload": {
									"items": {
										"1": ""
									}
								}
							}
						},
						"choice2": {
							"text": "Choice 2",
							"next": {
								"text": "Fine, be that way with 2."
							}
						},
						"choice3": {
							"text": "Choice 3",
							"next": {
								"text": "Hmmm, I'll have to think 3 over..."
							}
						}
					},
					"unavailableConditions": {
						"ledgers":  [
							{
								"entries": [
									{"type": "character", "action": "interaction", "id": "3", "entryId": "optionsEntry", "choice": "choice1"}
								]
							}
						]
					}
				}
			}
		},
		"4": {
			"id": "4",
			"name": "Character 4",
			"description": "Random Entry from array, avatars, background | <u>Conditions:</u> None | <u>OnAdd:</u> Activate",
			"activateOnAdd": true,
			"interactionManifest": {
				// A set of dialog entries grouped in an array with no next value needed
				"welcome": [
					{
						"text": "A",
						"leftAvatar": "images\/characters\/c59b64a6176f16d87eb2f4bdbe7f.jpeg"
					},
					{
						"text": "B",
						"rightAvatar": "images\/characters\/fc1a8132664d204da8cdc164eafb.jpeg",
						"rightAvatarFlip": true
					},
					{
						"text": "C",
						"leftAvatar": "images\/characters\/ff37f49e0256ee5a95be.jpeg"
					},
					{
						"text": "That's it. Good luck!"
					}
				]
			}
		},
		"5": {
			"id": "5",
			"name": "Character 5",
			"description": "Series array of entries | <u>Conditions:</u> None | <u>OnAdd:</u> Activate",
			"activateOnAdd": true,
			"interactionManifest": {
				"welcome": {
					"series": [
						{
							"text": "A"
						},
						{
							"text": "B"
						},
						{
							"text": "C"
						}
					]
				}
			}
		}
	},
	// Item combinations reference
	"combinations": {
		"combination1": {
			"items": {
				"1": {
					"stock": 1,
					"removeIfEmpty": true
				},
				"2": {
					"stock": 1,
					"removeIfEmpty": true
				}
			},
			"payload": {
				"items" : {
					"3": ""
				}
			}
		}
	},
	// TODO - Just thoughts for now
	"shops": {
		"shopId": {
			"id": 0,
			"locations": [],
			"inventories": {
				"inventoryId": {
					"id": 0,
					"conditions": {},
					"sell": {
						"conditions": {},
						"items": {
							"itemId": {
								"id": 0,
								"stock": 10,
								"price": 2
							}
						}
					},
					"buy": {
						"conditions": {},
						"items": {
							"itemId": {
								"id": 0,
								"stock": 10,
								"price": 2
							}
						}
					}
				}
			}
		}
	},
	// TODO - Just thoughts for now
	"enemies": {
		"footSoldiers": {
			"footSoldierId": {
				"id": 0,
				"type": "",
				"stats": {
					"randomHealthBetween": "3,5"
				},
				"locations": [],
				"conditions": {}
			}
		},
		"henchmen": {},
		"miniBosses": {},
		"bosses": {}
	}
};