const qrcodeDatabase = {
	"game": {
		"version": "0.1.3",
		"name": "Christmas Tutorial",
		"description": "Learn the ropes of playing a game.",
		// Optional: Language used in the game
		"lexicon": {
			"game": {
				"continue": "continue",
				"findVerb": "find",
				"game": "game",
				"instructions": "instructions",
				"settings": "settings",
				"start": "start",
				"titleScreenHeading": "Merry Christmas!"
			},
			"challenge": {
				"activateVerb": "start",
				"addVerb": "add",
				"challenge": "challenge",
				"deactivateVerb": "stop",
				"restartVerb": "restart",
				"useVerb": "go"
			},
			"item": {
				"activateVerb": "equip",
				"addVerb": "add",
				"deactivateVerb": "unequip",
				"inventory": "inventory",
				"item": "item",
				"useVerb": "use"
			},
			"character": {
				"activateVerb": "visit",
				"addVerb": "add",
				"character": "character",
				"deactivateVerb": "leave",
				"useVerb": "interact"
			},
			"location": {
				"activateVerb": "visit",
				"addVerb": "visit",
				"deactivateVerb": "leave",
				"location": "location",
				"useVerb": "explore"
			},
			"combination": {
				"combination": "combination",
				"combineVerb": "combine"
			}
		},
		// Optional: Parameters used as defaults for the game
		"params": {
			"autoFullscreen": true,
			//"challengeMode": "freeplay",//story,freeplay
			"autoZoom": true,
			"debug": false,
			"hudMode": "minimal",
			"collectorMode": true,//explorer,collector,scavenger
			"actionMode": true,//explorer,collector
			"activeChallengeLimit": "",// If 0, no limit to # of active challenges
			"showcase": true,
			"gameBg": "{{gameProp:database.game.resources.gameBg}}",
			"showPlayerStats": false,
			"challenge": {
				"showcase": true,
				"notifyAdd": true,
				"notifyActivate": true,
				"notifyDeactivate": true,
				"notifyUse": true,
				"notifyRemove": true
			},
			"character": {
				"showcase": true,
				"notifyAdd": true,
				"notifyAddThrottle": true,
				"notifyActivate": true,
				"notifyDeactivate": true,
				"notifyUse": true,
				"notifyUseThrottle": true,
				"notifyRemove": true,
				"notifyStats": true
			},
			"location": {
				"showcase": true,
				"notifyAdd": true,
				"notifyActivate": true,
				"notifyDeactivate": true,
				"notifyUse": true,
				"notifyUseThrottle": true,
				"notifyRemove": true
			},
			"item": {
				"showcase": true,
				"notifyAdd": true,
				"notifyAddThrottle": true,
				"notifyMaxStock": true,
				"notifyActivate": true,
				"notifyDeactivate": true,
				"notifyUse": true,
				"notifyUseThrottle": true,
				"notifyRemove": true,
				"notifyStats": true
			}
		},
		// Optional: Popup information for different points in the game. String or object
		//"intro": "",
		"outtro": "That's it, thank you for playing!",
		"overOuttro": "Awww, maaaan. Game over, but thank you for playing!",
		// Completion requirements
		"requirements": {
			"challenges": {
				"1": "",
				"2": "",
				"3": "",
				"4": "",
				"6": "",
				"7": "",
				"8": ""
			}
		},
		"interactionDefaults": {
		},
		"interactionManifest": {
			// A set of dialog entries grouped in an array with no next value needed
			"welcome": {
				"allowSkip": false,
				"series": [
					{
						"text": "<p><b>Yay! Welcome to the game!</b></p><p>If this is your first time playing, click <i class=\"fa-solid fa-compass fa-fw\"></i> at the top of the screen to take a tour.</p><p class=\"text-end\">&mdash; <code>{{gameProp:database.characters.bear2.name}}</code></p>",
						"leftAvatar": "{{gameProp:database.game.resources.bear2}}",
						"payload": {
							"characters": {
								"bear2": {
									"notifyActivate": false
								}
							}
						}
					}
				]
			}
		},
		"resources": {
			"bear1": "images\/christmas-tutorial\/bear-1.png",
			"bear2": "images\/christmas-tutorial\/bear-2.png",
			"cat": "images\/christmas-tutorial\/cat-with-knitting-needles.png",
			"fireplaceStockings": "images\/christmas-tutorial\/addy-mae-e7fDXFHCvKA-unsplash.jpg",
			"fish": "images\/christmas-tutorial\/fish.png",
			"gameBg": "images\/christmas-tutorial\/kateryna-hliznitsova-7zd7WPuQ82c-unsplash.jpg",
			"groundBulbs": "images\/christmas-tutorial\/sincerely-media-EuZuJrYJbmg-unsplash.jpg",
			"rabbit": "images\/christmas-tutorial\/rabbit-1.png",
			"snowflakeA": "images\/christmas-tutorial\/snowflake-1.png",
			"snowflakeB": "images\/christmas-tutorial\/snowflake-2.png",
			"snowflakes": "images\/christmas-tutorial\/snowflakes.png",
			"snowmanChallenge": "images\/christmas-tutorial\/felix-berger-2evqv4zZxTY-unsplash.jpg",
			"snowman": "images\/christmas-tutorial\/snowman.png",
			"snowmanTop": "images\/christmas-tutorial\/snowman-top.png",
			"snowmanMiddle": "images\/christmas-tutorial\/snowman-middle.png",
			"snowmanBottom": "images\/christmas-tutorial\/snowman-bottom.png",
			"star": "images\/christmas-tutorial\/star.png",
			"squirrel": "images\/christmas-tutorial\/squirrel-4.png",
			"tree": "images\/christmas-tutorial\/anne-nygard-aHHxZOXWJHc-unsplash.jpg",
			"yarn": "images\/christmas-tutorial\/yarn.png",
			"uglySweater": "images\/christmas-tutorial\/ugly-sweater.png"
		}
	},
	"items": {
		"1": {
			"type": "ornament",
			"name": "Star",
			"description": "The main symbolism of a star at Christmas is to pay tribute to the Star of Bethlehem. In the Bible, the star is said to be a sign that appeared at the birth of Jesus. It was seen by the three Wise Men, who were led by it to the baby Jesus.",
			"image": "{{gameProp:database.game.resources.star}}",
			"showcase": 1,
			// Whether to track in item inventory system
			"isInventory": 1,
			"isUsable": 0
		},
		"2": {
			"type": "food",
			"name": "Fish",
			"description": "The catch of the day.",
			"image": "{{gameProp:database.game.resources.fish}}",
			"isInventory": 1,
			"isUsable": 0
		},
		"3": {
			"type": "pet",
			"name": "Knitting Cat",
			"description": "A pet with a hobby of its own.",
			"image": "{{gameProp:database.game.resources.cat}}",
			"isInventory": 1,
			"isUsable": 0
		},
		"4": {
			"type": "clothing",
			"name": "Yarn",
			"description": "A green, continuous strand of fibers for use.",
			"image": "{{gameProp:database.game.resources.yarn}}",
			"isInventory": 1,
			"isUsable": 0
		},
		"5": {
			"type": "clothing",
			"name": "Ugly Sweater",
			"description": "Haven't you gotten rid of that old thing yet?! Burn it with fire!",
			"image": "{{gameProp:database.game.resources.uglySweater}}",
			"isInventory": 1,
			"isUsable": 0
		},
		"6": {
			"type": "nature",
			"name": "Snowflake A",
			"description": "A pretty snowflake.",
			"image": "{{gameProp:database.game.resources.snowflakeA}}",
			"isInventory": 1,
			"isUsable": 0
		},
		"7": {
			"type": "nature",
			"name": "Snowflake B",
			"image": "{{gameProp:database.game.resources.snowflakeB}}",
			"description": "Another pretty snowflake.",
			"isInventory": 1,
			"isUsable": 0
		},
		"9": {
			"type": "nature",
			"name": "Snowman Top",
			"description": "",
			"image": "{{gameProp:database.game.resources.snowmanTop}}",
			"isInventory": 0
		},
		"10": {
			"type": "nature",
			"name": "Snowman Middle",
			"description": "",
			"image": "{{gameProp:database.game.resources.snowmanMiddle}}",
			"isInventory": 0
		},
		"11": {
			"type": "nature",
			"name": "Snowman Bottom",
			"description": "",
			"image": "{{gameProp:database.game.resources.snowmanBottom}}",
			"isInventory": 0
		}
	},
	"challenges": {
		"1": {
			"type": "seek",
			"name": "Tree Topper",
			"description": "Find something you would put on top of a Christmas tree.",
			"notes": "Look around and explore for items.",
			"order": 1,
			"image": "{{gameProp:database.game.resources.tree}}",
			"intro": "{{modal-avatar:left,database.game.resources.squirrel}}<p><b>You started your first challenge!</b></p><p>On the Main screen you will see the status of active Challenges.</p><p>Double-check info and even restart challenges as you need.</p><p class=\"text-end\">&mdash; <code>{{gameProp:database.characters.squirrel.name}}</code>",
			"outtro": "{{modal-avatar:left,database.game.resources.squirrel}}<p><b>Good job!</b></p><p>You're getting the hang of things.</p></p><p class=\"text-end\">&mdash; <code>{{gameProp:database.characters.squirrel.name}}</code></p>",
			"hints": [
				"It shines bright.",
				"Twinkle, twinkle.",
				"It's sometimes shooting.",
				"When you wish upon a..."
			],
			"requirements": {
				"ledgers":  [
					{"entries": [
						{"type": "item", "action": "add", "id": "1"}
					], "type": "match"}
				]
			},
			"success": {
				"payload": {
					"characters": {
						"squirrel": {
							"notifyActivate": false
						}
					}
				}
			}
		},
		"2": {
			"type": "order",
			"name": "Snowman Minute",
			"image": "{{gameProp:database.game.resources.snowmanChallenge}}",
			"description": "Build a snowman in 60 seconds.",
			"order": 5,
			"requirements": {
				"ledgers":  [
					{"entries": [
						{"type": "item", "action": "add", "id": "11"},
						{"type": "item", "action": "add", "id": "10"},
						{"type": "item", "action": "add", "id": "9"}
					], "type": "order"}
				],
				"timeLimit": 60
			},
			"success": {
				"payload": {
					"characters": {
						"snowman": {
							"notifyActivate": false
						},
						"rabbit": {
							"notifyActivate": false
						}
					}
				}
			},
			"outtro": "{{modal-avatar:left,database.game.resources.rabbit}}<p><b>Wow!</b></p><p>And I thought I was fast!</p><p>Great job!</p><p class=\"text-end\">&mdash; <code>{{gameProp:database.characters.rabbit.name}}</code></p>"
		},
		"3": {
			"type": "build",
			"name": "Ugly Sweater",
			"description": "Find materials and make your own ugly sweater.",
			"order": 6,
			"intro": "{{modal-avatar:left,database.game.resources.bear1}}<p><b><i>Really?</i></b> You woke me up... for this?</p><p>Jeez. Just go select stuff in the right amounts and combine them to make something new.</p><p>You're on your own, pal. I'm going back to sleep.</p><p class=\"text-end\">&mdash; <code>{{gameProp:database.characters.bear1.name}}</code></p>",
			"requirements": {
				"ledgers":  [
					{"entries": [
						{"type": "item", "action": "combination", "id": "sweater"}
					], "ignoreTime": true, "type": "match"}
				]
			},
			"hints": [
				"Nine lives",
				"Spin it",
				"Go"
			],
			"success": {
				"payload": {
					"characters": {
						"bear1": {
							"notifyActivate": false
						}
					}
				}
			}
		},
		"4": {
			"type": "seek",
			"name": "2 of a Kind",
			"description": "Find a pair of snowflakes.",
			"order": 2,
			"image": "{{gameProp:database.game.resources.snowflakes}}",
			"requirements": {
				"ledgers":  [
					{"entries": [
						{"type": "item", "action": "add", "id": "7"}
					], "type": "count", "count": 2}
				]
			}
		},
		"6": {
			"type": "quiz",
			"name": "Study Hard",
			"description": "Recall details from an image.",
			"order": 4,
			"image": "{{gameProp:database.game.resources.groundBulbs}}",
			"requirements": {
				"ledgers":  [
					{
						"entries": [
							{"type": "prompt", "id": "bulbsImageQuiz"}
						]
					}
				]
			},
			"payloadOnActivate": {
				"actions": {
					"action": "prompt",
					"params": {
						"promptId": "bulbsImageQuiz"
					}
				}
			}
		},
		"7": {
			"type": "quiz",
			"name": "Study Harder",
			"description": "Recall details from an image.",
			"order": 7,
			"image": "{{gameProp:database.game.resources.fireplaceStockings}}",
			"requirements": {
				"ledgers":  [
					{
						"entries": [
							{"type": "prompt", "id": "stockingImageQuiz"}
						]
					}
				]
			},
			"payloadOnActivate": {
				"actions": {
					"action": "prompt",
					"params": {
						"promptId": "stockingImageQuiz"
					}
				}
			}
		},
		"8": {
			"type": "quiz",
			"name": "Name That Tune",
			"description": "Test your contemporary Christmas song aptitude.",
			"order": 3,
			"requirements": {
				"ledgers":  [
					{
						"entries": [
							{"type": "prompt", "id": "songQuiz"}
						]
					}
				]
			},
			"payloadOnActivate": {
				"actions": {
					"action": "prompt",
					"params": {
						"promptId": "songQuiz"
					}
				}
			}
		}
	},
	"locations": {
	},
	"characters": {
		"bear2": {
			"name": "Bearington",
			"description": "A jovial bear who greets you.",
			"image": "{{gameProp:database.game.resources.bear2}}",
			"activateOnAdd": true,
			"interactionManifest": {
				"1": {
					"text": [
						"Merry Christmas!",
						"Happy New Year!",
						"It's a wonderful time of year!"
					],
					"leftAvatar": "{{gameProp:database.game.resources.bear2}}"
				}
			}
		},
		"bear1": {
			"name": "Grizzle",
			"description": "A not-so-jovial bear who would rather not deal with you.",
			"image": "{{gameProp:database.game.resources.bear1}}",
			"activateOnAdd": true,
			"interactionManifest": {
				"1": {
					"text": [
						"Bah humbug!",
						"Leave me be!",
						"Go away or I'll decrease the surplus population."
					],
					"leftAvatar": "{{gameProp:database.game.resources.bear1}}"
				}
			}
		},
		"rabbit": {
			"name": "Rabbiddge",
			"description": "A forest friend impressed by your speed.",
			"image": "{{gameProp:database.game.resources.rabbit}}",
			"activateOnAdd": true,
			"interactionManifest": {
				"1": {
					"text": "You are almost as fast as me...",
					"leftAvatar": "{{gameProp:database.game.resources.rabbit}}",
					"next": {
						"text": "<p><i>almost.</i></p>",
						"leftAvatar": "{{gameProp:database.game.resources.rabbit}}"
					}
				}
			}
		},
		"squirrel": {
			"name": "Squirrelbington",
			"description": "A nutty little guy who likes watching you play.",
			"image": "{{gameProp:database.game.resources.squirrel}}",
			"activateOnAdd": true,
			"interactionManifest": {
				// A nested entry instead of several separated
				"nestedEntry1": {
					"text": "Can I climb the tree now?",
					"leftAvatar": "{{gameProp:database.game.resources.squirrel}}",
					"choices": {
						"choice1": {
							"text": "Yes",
							"next": {
								"text": "<p><b>Yay!!!</b> Thank you!<br><i>[... scurries away in a hurry]</i></p>",
								"leftAvatar": "{{gameProp:database.game.resources.squirrel}}"
							}
						},
						"choice2": {
							"text": "No",
							"next": {
								"text": "<p><b>Pleeeeaaaase?!?!</b></p>",
								"leftAvatar": "{{gameProp:database.game.resources.squirrel}}",
								"choices": {
									"choice1": {
										"text": "Give in",
										"next": {
											"text": "<p><b>Yay!!!</b> Thank you!<br><i>[... scurries away in a hurry]</i></p>",
											"leftAvatar": "{{gameProp:database.game.resources.squirrel}}"
										}
									},
									"choice2": {
										"text": "Stay firm",
										"next": {
											"text": "<p>Awwww... that makes me sad.<br><i>[... slumps away slowly with its furry little head hanging down]</i></p>",
											"leftAvatar": "{{gameProp:database.game.resources.squirrel}}"
										}
									}
								}
							}
						}
					}
				}
			}
		},
		"snowman": {
			"name": "Powder",
			"description": "A new friend you made... literally.",
			"image": "{{gameProp:database.game.resources.snowman}}",
			"activateOnAdd": true,
			"interactionManifest": {
				"1": {
					"text": [
						"Ahhh, you did a great job!",
						"Thank you so much!",
						"You're the best!"
					],
					"leftAvatar": "{{gameProp:database.game.resources.snowman}}"
				}
			}
		}
	},
	// Item combinations reference
	"combinations": {
		"sweater": {
			"items": {
				"2": {
					"remove": false
				},
				"3": {
					"remove": false
				},
				"4": {
					"remove": false
				}
			},
			"payload": {
				"items": {
					"5": ""
				}
			}
		}
	},
	// Game questions and quizzes
	"prompts": {
		"bulbsImageQuiz": {
			"title": "Image Recall",
			"type": "recall",
			"instructions": "<p>Study this image...</p>",
			"image": "{{gameProp:database.game.resources.groundBulbs}}",
			"questions": [
				{
					"text": "How many blue bulbs were there?",
					"choices": {
						"choice1": {
							"text": "0",
							"correct": true
						},
						"choice2": {
							"text": "1"
						},
						"choice3": {
							"text": "2"
						}
					}
				},
				{
					"text": "How many total bulbs were there?",
					"correctPool": [
						"8",
						"Eight"
					]
				},
				{
					"text": "Which color had the least amount of bulbs?",
					"choices": {
						"choice1": {
							"text": "Silver",
							"correct": true
						},
						"choice2": {
							"text": "Red"
						},
						"choice3": {
							"text": "Green"
						},
						"choice4": {
							"text": "Gold"
						}
					}
				}
			],
			"shuffleQuestions": true,
			"shuffleChoices": true,
			"success": {
				"message": "<h6 class=\"mt-2\">Correct!</h6>",
				"passingPercent": 80
			},
			"failure": {
				"message": "<h6 class=\"mt-2\">Wrong!</h6>"
			}
		},
		"stockingImageQuiz": {
			"title": "Image Recall",
			"type": "recall",
			"instructions": "<p>Study this image...</p>",
			"image": "{{gameProp:database.game.resources.fireplaceStockings}}",
			"questions": [
				{
					"text": "The fireplace mantle was made of brick.",
					"choices": {
						"choice1": {
							"text": "True"
						},
						"choice2": {
							"text": "False",
							"correct": true
						}
					}
				},
				{
					"text": "How many stockings were hung above the fireplace?",
					"correctPool": [
						"5",
						"Five"
					]
				},
				{
					"text": "Were the stockings fur or leather lined?",
					"choices": {
						"choice1": {
							"text": "Fur",
							"correct": true
						},
						"choice2": {
							"text": "Leather"
						}
					}
				},
				{
					"text": "What colors were on the stockings?",
					"choices": {
						"choice1": {
							"text": "Red"
						},
						"choice2": {
							"text": "Green"
						},
						"choice3": {
							"text": "Black"
						},
						"choice4": {
							"text": "All These Colors",
							"correct": true
						}
					}
				},
				{
					"text": "How many stockings had snowflakes?",
					"correctPool": [
						"3",
						"Three"
					]
				},
				{
					"text": "How many snowflakes were on the stockings?",
					"correctPool": [
						"6",
						"Six"
					]
				}
			],
			"shuffleQuestions": true,
			"shuffleChoices": true,
			"success": {
				"message": "<h6 class=\"mt-2\">Correct!</h6>",
				"passingPercent": 80
			},
			"failure": {
				"message": "<h6 class=\"mt-2\">Wrong!</h6>"
			}
		},
		"songQuiz": {
			"type": "choices",
			"title": "Name That Tune",
			"image": "{{gameProp:database.game.resources.fireplaceStockings}}",
			"questions": [
				{
					"text": "Bing Crosby recorded the original version of this song in 1942.",
					"choices": {
						"choice1": {
							"text": "‘Christmas Snow’"
						},
						"choice2": {
							"text": "‘White Christmas’",
							"correct": true
						},
						"choice3": {
							"text": "‘Bright Christmas’"
						},
						"choice4": {
							"text": "‘Christmas Light’"
						}
					}
				},
				{
					"text": "Which famous Christmas song begins with the lyrics: ‘the mood is right, the spirit’s up, we’re here tonight and that’s enough’?",
					"choices": {
						"choice1": {
							"text": "‘Wonderful Christmastime’ by Paul McCartney",
							"correct": true
						},
						"choice2": {
							"text": "‘Last Christmas’ by Wham"
						},
						"choice3": {
							"text": "‘Stay Another Day’ by East 17"
						}
					}
				},
				{
					"text": "Kylie Minogue re-recorded this Eartha Kitt hit from the 1950s.",
					"choices": {
						"choice1": {
							"text": "‘All I Want for Christmas’"
						},
						"choice2": {
							"text": "‘The Snowman’"
						},
						"choice3": {
							"text": "‘Blue Christmas’"
						},
						"choice4": {
							"text": "‘Santa Baby’",
							"correct": true
						}
					}
				}
			],
			"shuffleQuestions": true,
			"shuffleChoices": true,
			"success": {
				"message": "<h6 class=\"mt-2\">Correct!</h6>",
				"passingPercent": 80
			},
			"failure": {
				"message": "<h6 class=\"mt-2\">Wrong!</h6>"
			}
		}
	}
};