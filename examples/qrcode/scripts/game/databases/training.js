const qrcodeDatabase = {
	"game": {
		"version": "0.0.0",
		"name": "Training Game",
		"description": "Learn the ropes of playing a game.",
		// Optional: Language used in the game
		"lexicon": {
			"game": {
				"continue": "continue",
				"game": "game",
				"instructions": "instructions",
				"settings": "settings",
				"start": "start",
				"titleScreenHeading": "Welcome"
			},
			"challenge": {
				"activateVerb": "start",
				"addVerb": "add",
				"challenge": "challenge",
				"deactivateVerb": "stop",
				"restartVerb": "restart",
				"useVerb": "go"
			},
			"item": {
				"activateVerb": "equip",
				"addVerb": "add",
				"deactivateVerb": "unequip",
				"inventory": "inventory",
				"item": "item",
				"useVerb": "use"
			},
			"character": {
				"activateVerb": "visit",
				"addVerb": "add",
				"character": "character",
				"deactivateVerb": "leave",
				"useVerb": "interact"
			},
			"location": {
				"activateVerb": "visit",
				"addVerb": "visit",
				"deactivateVerb": "leave",
				"location": "location",
				"useVerb": "explore"
			},
			"combination": {
				"combination": "combination",
				"combineVerb": "combine"
			}
		},
		// Optional: Parameters used as defaults for the game
		"params": {
			"autoFullscreen": false,
			//"challengeMode": "freeplay",//story,freeplay
			"autoZoom": true,
			"debug": true,
			"hudMode": "minimal",
			"collectorMode": false,//explorer,collector,scavenger
			"actionMode": false,//explorer,collector
			"activeChallengeLimit": "",// If 0, no limit to # of active challenges
			"showcase": true,
			"challenge": {
				"showcase": true,
				"notifyAdd": true,
				"notifyActivate": true,
				"notifyDeactivate": true,
				"notifyUse": true,
				"notifyRemove": true
			},
			"character": {
				"showcase": true,
				"notifyAdd": true,
				"notifyAddThrottle": true,
				"notifyActivate": true,
				"notifyDeactivate": true,
				"notifyUse": true,
				"notifyUseThrottle": true,
				"notifyRemove": true,
				"notifyStats": true
			},
			"location": {
				"showcase": true,
				"notifyAdd": true,
				"notifyActivate": true,
				"notifyDeactivate": true,
				"notifyUse": true,
				"notifyUseThrottle": true,
				"notifyRemove": true
			},
			"item": {
				"showcase": true,
				"notifyAdd": true,
				"notifyAddThrottle": true,
				"notifyMaxStock": true,
				"notifyActivate": true,
				"notifyDeactivate": true,
				"notifyUse": true,
				"notifyUseThrottle": true,
				"notifyRemove": true,
				"notifyStats": true
			}
		},
		// Optional: Popup information for different points in the game. String or object
		//"intro": "",
		"outtro": "That's it, thank you for playing!",
		"overOuttro": "Awww, maaaan. Game over, but thank you for playing!",
		// Completion requirements
		"requirements": {
			"challenges": {
				"1": "",
				"2": "",
				"3": "",
				"4": "",
				"5": ""/**/
			}
		},
		"interactionDefaults": {
		},
		"interactionManifest": {
			// A set of dialog entries grouped in an array with no next value needed
			"welcome": {
				"allowSkip": false,
				"series": [
					{
						"text": "<p><b>Welcome to the game!</b></p><p>My name is Squijeebazu, but you can call me <code>{{gameProp:database.characters.1.name}}</code>.</p><p>If you need shown around, click <i class=\"fa-solid fa-compass fa-fw\"></i> at the top of the screen for a tour.</p>",
						"leftAvatar": "{{gameProp:database.characters.1.image}}"
					},
					{
						"text": "<p>Hi, I'm...</p><p>I'm {{gameProp:database.characters.1.name}}'s sister, <code>{{gameProp:database.characters.2.name}}</code>.</p><p>I...</p><p>I just want to be alone right now...</p>",
						"rightAvatar": "{{gameProp:database.characters.2.image}}",
						"rightAvatarFlip": true
					},
					{
						"text": "<p>Sorry about that. Ever since she lost her crystals, she just wants to be alone.</p><p>Maybe <code>find one to cheer her up</code>!</p>",
						"leftAvatar": "{{gameProp:database.characters.1.image}}"
					},
					{
						"text": "<p><b><i>WOAH, WOAH, WOAH!</i></b></p><p>You forgot <code>{{gameProp:database.characters.3.name}}</code>. Brother always forgetting {{gameProp:database.characters.3.name}}.</p><p>{{gameProp:database.characters.3.name}} likes new friend. Come talk to {{gameProp:database.characters.3.name}} soon.</p>",
						"leftAvatar": "{{gameProp:database.characters.3.image}}",
						"payload": {
							"characters": {
								"1": {
									"notifyActivate": false
								},
								"2": {
									"notifyActivate": false
								},
								"3": {
									"notifyActivate": false
								}
							}
						}
					}
				]
			}
		},
		"payloadOnStart": {
		}
	},
	"items": {
		"1": {
			"type": "object",
			"name": "Red Crystal",
			"description": "It's so... shiny!",
			"notes": "Don't be afraid to use it when needed.",
			"image": "images\/training\/items\/red-crystal.png",
			"hints": [
				"Use it this way.",
				"Use it that way."
			],
			"showcase": 1,
			// Whether to track in item inventory system
			"isInventory": 1,
			"isUsable": 0
		},
		"2": {
			"type": "object",
			"name": "Blue Crystal",
			"description": "It glistens like a sunken pirate wreck.",
			"image": "images\/training\/items\/blue-crystal.png",
			"isInventory": 1,
			"isUsable": 0
		},
		"3": {
			"type": "object",
			"name": "Green Crystal",
			"description": "The power is strong with this one.",
			"image": "images\/training\/items\/green-crystal.png",
			"isInventory": 1,
			"isUsable": 0
		},
		"4": {
			"type": "food",
			"name": "Salt",
			"description": "It's of the earth. It can be the pink Himalayan kind if it makes you feel special.",
			"image": "images\/training\/items\/salt.jpg",
			"isInventory": 1,
			"stockUpdateOnUse": -1,
			"useVerb": "eat",
			"useVerbPast": "eaten",
			"payloadOnFirstAdd": {
				"challenges": {
					"3": ""
				}
			}
		},
		"5": {
			"type": "food",
			"name": "Chocolate",
			"description": "Do we really need to explain this 8<sup>th</sup> wonder of the world? We don't want you getting sick though, so you can only get or eat one every 5 seconds.",
			"image": "images\/training\/items\/chocolate.jpg",
			"isInventory": 1,
			"stockUpdateOnUse": -1,
			"playerStatsUpdateOnUse": {
				"health": 10
			},
			"addThrottleMs": 5000,
			"useThrottleMs": 5000
		},
		"6": {
			"type": "food",
			"name": "Pill",
			"description": "We feel you should only be able to carry 1 of these at a time. It's for safety.",
			"image": "images\/training\/items\/pill.jpg",
			"isInventory": 1,
			"stockUpdateOnUse": -1,
			// Optional: Maximum item stock
			"maxStock": 1
		},
		"7": {
			"type": "food",
			"name": "Miracle Pill",
			"image": "images\/training\/items\/miracle-pill.jpg",
			"description": "After using it, wait a good hour before swimming. Keto-friendly.",
			"isInventory": 1,
			"isUsable": 0
		},
		"9": {
			"type": "food",
			"name": "Poison",
			"description": "Vizzini tricked you! Find the antidote before your health is depleted and you die!",
			"image": "images\/training\/items\/poison.jpg",
			"payload": {
				"actions": [
					{
						"action": "createStatTimer",
						"params": {
							"id": "createStatTimer_Item_21",
							"type": "player",
							"stat": "health",
							"valueIncrement": -1
						}
					}
				]
			}
		},
		"10": {
			"type": "food",
			"name": "Antidote",
			"description": "Ahhh, that was close!",
			"image": "images\/training\/items\/antidote.png",
			"payload": {
				"actions": [
					{
						"action": "destroyStatTimer",
						"params": {
							"id": "createStatTimer_Item_21"
						}
					}
				]
			}
		}
	},
	"challenges": {
		"1": {
			"name": "Find a Crystal",
			"description": "Yep, any crystal. Find one.",
			"notes": "Look around and explore for items.",
			"intro": "{{modal-avatar:left,database.characters.2.image}}<p><b>You started your first challenge!</b></p><p>On the Main screen you will see the status of active Challenges.</p><p>Double-check info and even restart challenges as you need.</p>",
			"outtro": "{{modal-avatar:left,database.characters.3.image}}<p><b>Good job!</b></p><p>Now that you're getting the hang of things, I think you are ready for another. I'm so excited that I'm going to even start it for you.</p><p><b>Get ready, here it comes!</b></p><p class=\"text-end\">&mdash; <code>Grunnk</code></p>",
			"hints": [
				"Look around for shiny, rock-looking things.",
				"You can't eat it...",
				"well ok, you can eat it, but you wouldn't want to...",
				"or rather you <b><i>shouldn't</i></b> want to."
			],
			"requirements": {
				"1MatchOf": {
					"items": [
						"1",
						"2",
						"3"
					]
				}
			},
			"success": {
				"payload": {
					"challenges": {
						"2": ""
					}
				}
			}
		},
		"2": {
			"name": "Find 3 Crystals",
			"description": "Find 3 crystals in a specific order in 60 seconds. You may need to look around for some clues before starting this one. Once you start, hurry before time runs out!",
			"activateOnAdd": true,
			"prerequisites": {
				"challenges": {
					"1": ""
				}
			},
			"requirements": {
				"ledgers":  [
					{"entries": [
						{"type": "item", "action": "add", "id": "2"},
						{"type": "item", "action": "add", "id": "1"},
						{"type": "item", "action": "add", "id": "3"}
					], "type": "order"}
				],
				"timeLimit": 60
			},
			"outtro": "{{modal-avatar:left,database.characters.1.image}}<p><b>Wow!</b></p><p>I thought you were a goner for sure. I'm going to let you catch your breath and look around for your next challenge.</p><p>All this running around is making me hungry!</p><p class=\"text-end\">&mdash; <code>Frank</code></p>"
		},
		"3": {
			"name": "New Creations",
			"description": "Use whatchoo got.",
			"intro": "{{modal-avatar:left,database.characters.2.image}}<p>You may have seen some checkboxes and a button at the bottom of your Items screen.</p><p>You can select items and combine them to make new ones if you have the right amounts.</p>",
			"payloadOnActivate": {
				"actions": [
					{
						"action": "interaction",
						"params": {
							"type": "",//challenge
							"id": "",
							"entryId": {
								"text": "Monkeys!"
							}
						}
					}
				]
			},
			"prerequisites": {
				"challenges": {
					"2": ""
				}
			},
			"requirements": {
				"ledgers":  [
					{"entries": [
						{"type": "item", "action": "combination", "id": "billy"}
					], "ignoreTime": true, "type": "match"}
				]
			},
			"hints": [
				"Tis the season",
				"Combine them with this to bring out some flavor",
				"Season's greetings",
				"C'mon already, man, it's salt, salt, salt!",
				"Ah, but what might you call those little pieces of salt?"
			],
			"outtro": "{{modal-avatar:left,database.characters.1.image}}<p><b>Niiiiice.</b></p><p>Now that you have someone new to talk to, I suppose I have time to take that vaction.</p><p>Maybe someplace with a tower.</p>"
		},
		"4": {
			"name": "New Horizons",
			"description": "Yeah, I can't really say too much here. Now that you have who you need, you will know where to go and what to do. You just need one more thing.",
			"hints": [
				"It would take a...",
				"Storm it"
			],
			"requirements": {
				"characters": {
					"4": ""
				},
				"items": {
					"7": ""
				}
			},
			"submitTriggers": { // Optional
				"type": "location",
				"action": "use",
				"id": "1"
			},
			"locations": {
				"1": ""
			},
			"outtro": "{{modal-avatar:left,database.characters.1.image}}<p><b>Sweet!</b></p><p>A little miracle never hurts.</p><p>You'll have to let Max know it actually worked.</p>"
		},
		"5": {
			"name": "Quizalicious",
			"description": "Complete the quiz in 60 seconds!",
			"requirements": {
				"ledgers":  [
					{
						"entries": [
							{"type": "prompt", "id": "billyQuiz"}
						]
					}
				],
				"timeLimit": 60
			},
			"activateOnAdd": true,
			"payloadOnActivate": {
				"actions": {
					"action": "prompt",
					"params": {
						"promptId": "billyQuiz"
					}
				}
			}
		}
	},
	"locations": {
		"1": {
			"name": "The Castle",
			"description": "Like in that one scene where the cool stuff happens.",
			"image": "images\/training\/locations\/castle.jpg",
			"hints": [
				"It would take a..."
			],
			"activateOnAdd": true,
			"useVerb": "storm",
			"useVerbPast": "stormed",
			"payloadOnFirstAdd": {
				"challenges": {
					"4": ""
				}
			}
		}
	},
	"characters": {
		"1": {
			"name": "Frank",
			"description": "The oldest of 3 siblings, he appoints himself as your guide.",
			"image": "images\/characters\/c59b64a6176f16d87eb2f4bdbe7f.jpeg",
			"hints": [
				"I'm pretty random. Try me."
			],
			"activateOnAdd": true,
			"interactionManifest": {
				"1": {
					"text": [
						"Do I look French? ... I feel kind of French.",
						"Bonjour!",
						"If you need a refresher, you can check out the Instructions on the Title screen."
					],
					"leftAvatar": "{{gameProp:database.characters.1.image}}"
				}
			}
		},
		"2": {
			"name": "Krenne",
			"description": "The sensitive middle sister, she's fond of collecting shiny things.",
			"image": "images\/characters\/fc1a8132664d204da8cdc164eafb.jpeg",
			"activateOnAdd": true,
			"interactionManifest": {
				// A nested entry instead of several separated
				"nestedEntry1": {
					"text": "One ring to rule them all...",
					"leftAvatar": "{{gameProp:database.characters.2.image}}",
					"next": {
						"text": "one ring to find them...",
						"leftAvatar": "{{gameProp:database.characters.2.image}}",
						"next": {
							"text": "one ring to bring them all...",
							"leftAvatar": "{{gameProp:database.characters.2.image}}",
							"next": {
								"text": "and in the darkness, bind them.",
								"leftAvatar": "{{gameProp:database.characters.2.image}}"
							}
						}
					},
					"availableConditions": {
						"challenges": {
							"1": ""
						}
					}
				},
				"genericEntry1": {
					"text": "Come back after you've completed a challenge.",
					"leftAvatar": "{{gameProp:database.characters.2.image}}"
				}
			}
		},
		"3": {
			"name": "Grunnk",
			"description": "An often-overlooked baby brother, he's eager for a friend.",
			"image": "images\/characters\/832d7ddf0fe356d8e71770382e8a.jpeg",
			"activateOnAdd": true,
			"interactionManifest": {
				// An entry with options nested
				"optionsEntry": {
					"text": "You found me! How about it, do you want me to just give you a challenge or a hint?",
					"leftAvatar": "{{gameProp:database.characters.3.image}}",
					"choices": {
						"choice1": {
							"text": "Give it to me",
							"next": {
								"text": "<p><b>Here you go.</b> I'm going to take you to your Challenges screen.</p><p>You can start it when you are ready.</p>",
								"leftAvatar": "{{gameProp:database.characters.3.image}}",
								"payload": {
									"challenges": {
										"1": ""
									},
									"actions": [
										{
											"action": "showScreen",
											"params": {
												"screen": "challenges",
												"prevScreen": "main"
											}
										}
									]
								}
							}
						},
						"choice2": {
							"text": "I'll take a hint",
							"next": {
								"text": "<p><i>You play this word on an instrument, and the next word has pages.</p>",
								"leftAvatar": "{{gameProp:database.characters.3.image}}"
							}
						}
					},
					"unavailableConditions": {
						"ledgers":  [
							{
								"entries": [
									{"type": "character", "action": "interaction", "id": "3", "entryId": "optionsEntry", "choice": "choice1"}
								]
							}
						]
					}
				}
			}
		},
		"4": {
			"type": "comedian",
			"name": "Billy Crystal",
			"description": "The one and only.",
			"image": "images\/training\/characters\/miracle-max.jpg",
			"activateOnAdd": true,
			"interactionManifest": {
				// A nested entry instead of several separated
				"nestedEntry1": {
					"text": "Are you ready for your last challenge?",
					"leftAvatar": "{{gameProp:database.characters.4.image}}",
					"choices": {
						"choice1": {
							"text": "Yes",
							"payload": {
								"challenges": {
									"5": ""
								},
								"actions": [
									{
										"action": "showScreen",
										"params": {
											"screen": "characters",
											"prevScreen": "main"
										}
									}
								]
							}
						},
						"choice2": {
							"text": "No"
						}
					},
					"availableConditions": {
						"challenges": {
							"4": ""
						}
					}
				},
				// A set of dialog entries grouped in an array with no next value needed
				"welcome": [
					{
						"text": "You rush a miracle man, you get rotten miracles.",
						"leftAvatar": "{{gameProp:database.characters.4.image}}"
					},
					{
						"text": "The King's stinking son fired me, and thank you so much for bringing up such a painful subject.",
						"leftAvatar": "{{gameProp:database.characters.4.image}}"
					},
					{
						"text": "Sonny, true love is the greatest thing in the world - except for a nice MLT - mutton, lettuce, and tomato sandwich, where the mutton is nice and lean and the tomatoes are ripe. <i>[smacks lips]</i> They're so perky, I love that.",
						"leftAvatar": "{{gameProp:database.characters.4.image}}"
					},
					{
						"text": "Have fun storming the castle!",
						"leftAvatar": "{{gameProp:database.characters.4.image}}"
					}
				]
			}
		}
	},
	// Item combinations reference
	"combinations": {
		"billy": {
			"items": {
				"1": {
					"remove": true
				},
				"2": {
					"remove": true
				},
				"3": {
					"remove": true
				},
				"4": {
					"stock": 3
				}
			},
			"payload": {
				"characters": {
					"4": ""
				}
			}
		},
		"miraclePill": {
			"items": {
				"5": {
					"stock": 1
				},
				"6": {
					"stock": 1
				}
			},
			"payload": {
				"items": {
					"7": ""
				}
			}
		}
	},
	// Game questions and quizzes
	"prompts": {
		"billyQuiz": {
			"title": "The Princess Bride Trivia",
			"questions": [
				{
					"text": "What is Miracle Max’s wife’s name?",
					"choices": {
						"choice1": {
							"text": "Veronica"
						},
						"choice2": {
							"text": "Anne"
						},
						"choice3": {
							"text": "Carol"
						},
						"choice4": {
							"text": "Valerie",
							"correct": true
						}
					}
				},
				{
					"text": "How many years of Westley’s life were sucked away on his <b>first</b> encounter with The Machine?",
					"correctPool": [
						"1",
						"One"
					]
				},
				{
					"text": "Who killed Inigo’s father?",
					"choices": {
						"choice1": {
							"text": "Prince Humperdinck"
						},
						"choice2": {
							"text": "Vizzini"
						},
						"choice3": {
							"text": "Count Rugen",
							"correct": true
						}
					}
				},
				{
					"text": "Which character is praised for having “a great gift for rhyme”?",
					"choices": {
						"choice1": {
							"text": "Westley"
						},
						"choice2": {
							"text": "Vizzini"
						},
						"choice3": {
							"text": "Fezzik",
							"correct": true
						},
						"choice4": {
							"text": "Inigo Montoya"
						}
					}
				},
				{
					"text": "Where do Westley and Princess Buttercup hide from Prince Humperdinck?",
					"correctPool": [
						"The Fire Swamp"
					]
				}
			],
			"shuffleQuestions": true,
			"shuffleChoices": true,
			"success": {
				"message": "<h6 class=\"mt-2\">Correct!</h6>",
				"passingPercent": 80
			},
			"failure": {
				"message": "<h6 class=\"mt-2\">Wrong!</h6>"
			}
		}
	}
};