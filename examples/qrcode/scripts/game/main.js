// Start up game
const MyGame = new Game(qrcodeDatabase);
let codeReader;

// Listen for events

// Game
MyGame.events.on(MyGame.events.names.showScreen, MyGame.ui.nav.showScreen.bind(MyGame.ui.nav));
MyGame.events.on(MyGame.events.names.hint, MyGame.ui.cta.handleHint.bind(MyGame.ui.cta));
MyGame.events.on(MyGame.events.names.progress, MyGame.ui.handleProgress.bind(MyGame.ui));
MyGame.events.on(MyGame.events.names.complete, MyGame.ui.handleComplete.bind(MyGame.ui));
MyGame.events.on(MyGame.events.names.over, MyGame.ui.handleOver.bind(MyGame.ui));
MyGame.events.on(MyGame.events.names.error, MyGame.ui.handleError.bind(MyGame.ui));
MyGame.events.on(MyGame.events.names.processPayloadAfter, MyGame.ui.handleProcessPayloadAfter.bind(MyGame.ui));
MyGame.events.on(MyGame.events.names.updateHud, MyGame.ui.resizeStatusContainerForScrollDebounced);
// Challenges
MyGame.challenge.events.on(MyGame.challenge.events.names.add, MyGame.challenge.ui.handleAdd.bind(MyGame.challenge.ui));
MyGame.challenge.events.on(MyGame.challenge.events.names.activate, MyGame.challenge.ui.handleActivate.bind(MyGame.challenge.ui));
MyGame.challenge.events.on(MyGame.challenge.events.names.deactivate, MyGame.challenge.ui.handleDeactivate.bind(MyGame.challenge.ui));
MyGame.challenge.events.on(MyGame.challenge.events.names.progress, MyGame.challenge.ui.handleProgress.bind(MyGame.challenge.ui));
MyGame.challenge.events.on(MyGame.challenge.events.names.hint, MyGame.ui.cta.handleHint.bind(MyGame.ui.cta));
MyGame.challenge.events.on(MyGame.challenge.events.names.complete, MyGame.challenge.ui.handleComplete.bind(MyGame.challenge.ui));
MyGame.challenge.events.on(MyGame.challenge.events.names.updateHud, MyGame.ui.resizeStatusContainerForScrollDebounced);
MyGame.challenge.events.on(MyGame.challenge.events.names.updateHud, MyGame.ui.componentInfo.updateModalDebounced);
// Player
MyGame.player.events.on(MyGame.player.events.names.stats, MyGame.player.ui.updateHudDebounced);
// Items
MyGame.events.on(MyGame.events.names.processItemsPayloadAfter, MyGame.player.ui.updateHudDebounced);
MyGame.events.on(MyGame.events.names.processItemsPayloadAfter, MyGame.item.ui.updateHudDebounced);
MyGame.item.events.on(MyGame.item.events.names.add, MyGame.item.ui.handleAdd.bind(MyGame.item.ui));
MyGame.item.events.on(MyGame.item.events.names.use, MyGame.item.ui.handleUse.bind(MyGame.item.ui));
MyGame.item.events.on(MyGame.item.events.names.activate, MyGame.item.ui.handleActivate.bind(MyGame.item.ui));
MyGame.item.events.on(MyGame.item.events.names.deactivate, MyGame.item.ui.handleDeactivate.bind(MyGame.item.ui));
MyGame.item.events.on(MyGame.item.events.names.remove, MyGame.item.ui.updateHudDebounced);
MyGame.item.events.on(MyGame.item.events.names.addThrottle, MyGame.item.ui.handleAddThrottle.bind(MyGame.item.ui));
MyGame.item.events.on(MyGame.item.events.names.useThrottle, MyGame.item.ui.handleUseThrottle.bind(MyGame.item.ui));
MyGame.item.events.on(MyGame.item.events.names.maxStock, MyGame.item.ui.handleMaxStock.bind(MyGame.item.ui));
MyGame.item.events.on(MyGame.item.events.names.hint, MyGame.ui.cta.handleHint.bind(MyGame.ui.cta));
MyGame.item.events.on(MyGame.item.events.names.updateHud, MyGame.ui.resizeStatusContainerForScrollDebounced);
MyGame.item.events.on(MyGame.item.events.names.updateHud, MyGame.ui.componentInfo.updateModalDebounced);
// Character
MyGame.events.on(MyGame.events.names.processCharactersPayloadAfter, MyGame.character.ui.updateHudDebounced);
MyGame.character.events.on(MyGame.character.events.names.add, MyGame.character.ui.handleAdd.bind(MyGame.character.ui));
MyGame.character.events.on(MyGame.character.events.names.activate, MyGame.character.ui.handleActivate.bind(MyGame.character.ui));
MyGame.character.events.on(MyGame.character.events.names.deactivate, MyGame.character.ui.handleDeactivate.bind(MyGame.character.ui));
MyGame.character.events.on(MyGame.character.events.names.use, MyGame.character.ui.handleUse.bind(MyGame.character.ui));
MyGame.character.events.on(MyGame.character.events.names.hint, MyGame.ui.cta.handleHint.bind(MyGame.character.ui));
MyGame.character.events.on(MyGame.character.events.names.updateHud, MyGame.ui.resizeStatusContainerForScrollDebounced);
MyGame.character.events.on(MyGame.character.events.names.updateHud, MyGame.ui.componentInfo.updateModalDebounced);
// Locations
MyGame.events.on(MyGame.events.names.processLocationsPayloadAfter, MyGame.location.ui.updateHudDebounced);
MyGame.location.events.on(MyGame.location.events.names.add, MyGame.location.ui.handleAdd.bind(MyGame.location.ui));
MyGame.location.events.on(MyGame.location.events.names.activate, MyGame.location.ui.handleActivate.bind(MyGame.location.ui));
MyGame.location.events.on(MyGame.location.events.names.deactivate, MyGame.location.ui.handleDeactivate.bind(MyGame.location.ui));
MyGame.location.events.on(MyGame.location.events.names.use, MyGame.location.ui.handleUse.bind(MyGame.location.ui));
MyGame.location.events.on(MyGame.location.events.names.hint, MyGame.ui.cta.handleHint.bind(MyGame.ui.cta));
MyGame.location.events.on(MyGame.location.events.names.updateHud, MyGame.ui.resizeStatusContainerForScrollDebounced);
MyGame.location.events.on(MyGame.location.events.names.updateHud, MyGame.ui.componentInfo.updateModalDebounced);

// Bootstrap Modal global listeners
/*
modalData.modalElement.addEventListener('hidden.bs.modal', event => {
	// Process the next action
	//this.processActionQueue(params);
	this.pollActionQueue();
});
*/
document.addEventListener('show.bs.modal', event => {
	//console.log('show.bs.modal', event);
	// Use a variable to avoid race conditions on checking for created modals if multiple happen at once
	// Keep track here or in Modal
	Modal.shown = true;
	Modal.numShown++;
});
document.addEventListener('hidden.bs.modal', event => {
	//console.log('hidden.bs.modal', event);
	
	// Remove modal when hidden
	event.target.remove();

	Modal.shown = false;
	Modal.numShown--;

	// Continue with any queued actions
	MyGame.pollActionQueue();
});

document.addEventListener('readystatechange', (event) => {
	if ( event.target.readyState === 'complete' )
	{
		const loadingScreen = document.querySelector('#loading-screen');
		RL.a.animateCSS(loadingScreen, 'fadeOut').then(message => {
			loadingScreen.classList.add('d-none');
		});
	}
});

// Listen for browser history changing
// This helps with the issue of people using the device/browser back button and 
// the showScreen method previously not making any browser url changes
// https://developer.mozilla.org/en-US/docs/Web/API/History_API#The_popstate_event
// https://stackoverflow.com/questions/25806608/how-to-detect-browser-back-button-event-cross-browser
// https://stackoverflow.com/questions/33308121/can-you-bind-this-in-an-arrow-function
window.addEventListener('popstate', function ( event )
{
	this.game.logger.debug(`popstate: location: ${document.location}, state: ${JSON.stringify(event.state)}`);

	var state = event.state;
	if ( state == null )
	{
		return;
	}
	
	if ( event?.state?.showScreen )
	{
		this.showScreen(event.state.showScreen.screen, event.state.showScreen.prevScreen, '', '', true);
	}
}.bind(MyGame.ui.nav));

// Poll with interval until available
(function() {
	let nTimer = setInterval(function() {
		// If available, stop polling
		if (window.jQuery) {
			clearInterval(nTimer);
			//====================

			jQuery(window).on('load', function ()
			{
			});

			jQuery(window).on('resize', function ()
			{
				MyGame.ui.resizeStatusContainerForScrollDebounced();
			});
			MyGame.ui.resizeStatusContainerForScrollDebounced(); // This will happen when updating Huds, so pass for now
			
			// TODO: Work with back button or browser history to avoid accidental back
			// https://stackoverflow.com/questions/57102502/preventing-mouse-fourth-and-fifth-buttons-from-navigating-back-forward-in-browse/62482736#62482736
			// https://stackoverflow.com/questions/25806608/how-to-detect-browser-back-button-event-cross-browser
			// https://medium.com/@liuzhenglaichn/hack-the-browser-history-167f73ac403c

			// DOM ready
			jQuery(document).ready(function($)
			{
				MyGame.logger.debug(`jQuery DOM ready!`);
				
				// Check for background
				const gameBg = MyGame.getSetting('gameBg', '', '', 'game');
				if ( gameBg )
				{
					$('.game-screen:not(.game-screen--nested) .game-screen__wrapper').css({
						'backgroundImage': `url('${MyGame.parseShortcodes(gameBg)}')`,
						'backgroundScroll': 'scroll',
						'backgroundRepeat': 'no-repeat',
						'backgroundPosition': '50% 50%',
						'backgroundSize': 'cover'
					});
				}

				// Check for showing player stats
				const showPlayerStats = MyGame.getSetting('showPlayerStats', '', '', 'game');
				if ( ! showPlayerStats )
				{
					$('.player-stats').addClass('d-none');
				}

				// Initialize HUDs
				MyGame.ui.updateHudsDebounced();
				// Set game version
				$('.stat--game-version').html(`v${MyGame.database.game.version ?? '?'}`);
				// Set game start button
				let $startButton = $('button.cta--game-start');
				let $startIcon = $startButton.find('i').clone();
				$startButton.html(`${RL.u.capitalize(MyGame.getLexicon('start', '', '', 'game'))}`).prepend($startIcon);
				// Set game instructions button
				let $instructionsButton = $('button[data-screen="instructions"]');
				let $instructionsIcon = $instructionsButton.find('i').clone();
				$instructionsButton.html(`${RL.u.capitalize(MyGame.getLexicon('instructions', '', '', 'game'))}`).prepend($instructionsIcon);
				// Set game setings button
				let $settingsButton = $('button[data-screen="settings"]');
				let $settingsIcon = $settingsButton.find('i').clone();
				$settingsButton.html(`${RL.u.capitalize(MyGame.getLexicon('settings', '', '', 'game'))}`).prepend($settingsIcon);
				// Set game title
				$('.game-screen--main .game-screen__title-heading').html(MyGame.database.game.name);
				// Set title screen title
				$('.game-screen--title .game-screen__title-heading').html(MyGame.getLexicon('titleScreenHeading', '', '', 'game'));
				// Set screen titles
				$('.game-screen--challenges .game-screen__title-heading').html(RL.u.capitalize( RL.u.wordPluralizeSuffix(MyGame.getLexicon('challenge', '', '', 'challenge'), 0) ));
				$('.game-screen--items .game-screen__title-heading').html(RL.u.capitalize( RL.u.wordPluralizeSuffix(MyGame.getLexicon('item', '', '', 'item'), 0) ));
				$('.game-screen--locations .game-screen__title-heading').html(RL.u.capitalize( RL.u.wordPluralizeSuffix(MyGame.getLexicon('location', '', '', 'location'), 0) ));
				$('.game-screen--characters .game-screen__title-heading').html(RL.u.capitalize( RL.u.wordPluralizeSuffix(MyGame.getLexicon('character', '', '', 'character'), 0) ));

				// Handle interactivity

				// QRCode
				jQuery(document).on('click', '.cta--start-scan', {}, MyGame.ui.camera.startScanning.bind(MyGame.ui.camera));
				jQuery(document).on('click', '.cta--stop-scan', {}, MyGame.ui.camera.stopScanning.bind(MyGame.ui.camera));
				jQuery(document).on('change', '.cta--file-decode', {}, MyGame.ui.camera.handleFileDecodeChange.bind(MyGame.ui.camera));

				// Tour CTAs
				jQuery(document).on('click', '.cta--start-tour', function(event) {
					event.preventDefault();

					let tour = $(this).data('tour')
					.split('-')
					.map(val => RL.u.capitalize(val))
					.join('');

					// If tour exists, start it
					if ( MyGame.ui.tour[`do${tour}Intro`] )
					{
						MyGame.ui.tour[`do${tour}Intro`]();
					}
				});

				// Game Screen

				jQuery(document).on('click', '[data-screen]', {}, MyGame.ui.nav.handleOpenScreenClick.bind(MyGame.ui.nav));
				jQuery(document).on('click', '.cta--prev-screen, .cta--close-screen', {}, MyGame.ui.nav.handleCloseScreenClick.bind(MyGame.ui.nav));
				
				// If using game screen backdrop and wrapper, close on backdrop click
				jQuery(document).on('click', '.game-screen', function ( event ) {
					const targetElement = event.currentTarget;
					let $target = $(targetElement);
					if ( $target.is('.game-screen') )
					{
						MyGame.ui.nav.handleCloseScreenClick(event);
					}
				});
				// Trap wrapper clicks to prevent bubbling to backdrop
				jQuery(document).on('click', '.game-screen__wrapper', function(event) {
					event.stopImmediatePropagation();
				});

				// Game
				jQuery(document).on('click', '.cta--game-start', {}, MyGame.ui.cta.handleGameStartClick.bind(MyGame.ui.cta));
				jQuery(document).on('click', '.cta--hud-refresh', {}, MyGame.ui.cta.handleHudRefreshClick.bind(MyGame.ui.cta));
				// Component
				jQuery(document).on('click', '.cta--component-info, .cta--component-info-next, .cta--component-info-prev', {}, MyGame.ui.cta.handleComponentInfoClick.bind(MyGame.ui.cta));
				jQuery(document).on('click', '.cta--component-add', {}, MyGame.ui.cta.handleComponentAddClick.bind(MyGame.ui.cta));
				jQuery(document).on('click', '.cta--component-activate', {}, MyGame.ui.cta.handleComponentActivateClick.bind(MyGame.ui.cta));
				jQuery(document).on('click', '.cta--component-deactivate', {}, MyGame.ui.cta.handleComponentDeactivateClick.bind(MyGame.ui.cta));
				jQuery(document).on('click', '.cta--component-use', {}, MyGame.ui.cta.handleComponentUseClick.bind(MyGame.ui.cta));
				jQuery(document).on('click', '.cta--component-restart', {}, MyGame.ui.cta.handleComponentRestartClick.bind(MyGame.ui.cta));
				jQuery(document).on('click', '.cta--component-hint', {}, MyGame.ui.cta.handleComponentHintClick.bind(MyGame.ui.cta));
				jQuery(document).on('click', '.cta--component-combine', {}, MyGame.ui.cta.handleComponentCombineClick.bind(MyGame.ui.cta));
				// Challenge
				// Item
				// Location
				// Character

				// Settings

				// Auto Light
				jQuery(document).on('change', '#setting__autoLight', {}, function ( event ) {
					MyGame.database.game.params.autoLight = this.checked;
				});
				jQuery('#setting__autoLight').prop('checked', qrcodeDatabase.game?.params?.autoLight ?? false).trigger('change');
				// Auto Zoom
				jQuery(document).on('change', '#setting__autoZoom', {}, function ( event ) {
					MyGame.database.game.params.autoZoom = this.checked;
				});
				jQuery('#setting__autoZoom').prop('checked', qrcodeDatabase.game?.params?.autoZoom ?? false).trigger('change');
				// Debug
				jQuery(document).on('change', '#setting__debugMode', {}, function ( event ) {
					// Hide debug areas
					$('.setting--debug-mode').toggleClass('d-none', ! this.checked);
					
					MyGame.ui.resizeStatusContainerForScrollDebounced();
				});
				jQuery('#setting__debugMode').prop('checked', qrcodeDatabase.game?.params?.debug ?? false).trigger('change');
				// Collector Mode
				jQuery(document).on('change', '#setting__collectorMode', {}, function ( event ) {
					MyGame.database.game.params.collectorMode = this.checked;
					MyGame.ui.updateHudsDebounced();
				});
				jQuery('#setting__collectorMode').prop('checked', qrcodeDatabase.game?.params?.collectorMode ?? false).trigger('change');
				// Action Mode
				jQuery(document).on('change', '#setting__actionMode', {}, function ( event ) {
					MyGame.database.game.params.actionMode = this.checked;
					MyGame.ui.updateHudsDebounced();
				});
				jQuery('#setting__actionMode').prop('checked', qrcodeDatabase.game?.params?.actionMode ?? false).trigger('change');
				// Fullscreen
				jQuery(document).on('fullscreenchange', function ( event ) {
					$('#setting__fullScreen').prop('checked', RL.fullscreen.is());
				});
				jQuery(document).on('click', '.toggle__fullScreen', {}, function ( event ) {
					RL.fullscreen.toggle();
				});
				
				// If not fullscreen capable, disable options
				if ( ! RL.fullscreen.isEnabled() )
				{
					// Adjust toggle
					$('.toggle__fullScreen').html(`
						<i class="fa-solid fa-triangle-exclamation fa-fw"></i>
						Fullscreen <i>(Disabled)</i>
					`)
					.addClass('disabled').attr({'aria-disabled': 'true', 'disabled': true});
					// Adjust setting
					$('label[for="setting__fullScreen"]').html(`
						<i class="fa-solid fa-triangle-exclamation fa-fw"></i>
						Fullscreen
						<i>(Disabled)</i>
					`);
				}
				// Camera permissions
				jQuery(document).on('click', '.toggle__checkCameraPermission', {}, function ( event ) {
					MyGame.ui.camera.checkCameraPermission();
				});
				// Left-handed
				jQuery(document).on('change', '#setting__leftHanded', {}, function ( event ) {
					// Flip UI
					$('.setting--gameplay-left-handed-toggle').toggleClass('setting--gameplay-left-handed-on', this.checked);

					MyGame.ui.resizeStatusContainerForScrollDebounced();
				});
				// Theme
				jQuery(document).on('change', 'input[name="setting__theme"]', {}, function ( event ) {
					// Remove any theme classes from body
					const prefix = 'theme-';
					const classes = document.body.className.split(' ').filter(c => !c.startsWith(prefix));
					// Add theme class to body
					classes.push(this.value);
					document.body.className = classes.join(' ').trim();

					MyGame.ui.resizeStatusContainerForScrollDebounced();
				});
				
				// Debugging

				//console.log(MyGame.challenge.checkPrerequisites(3));

				// Automatically process a payload
				//MyGame.processPayload(examplePayload);
				//MyGame.challenge.activate(1);
				//console.log(checkActiveChallenges(MyGame.player));
				//console.log(`------------------------------`);
				//console.log(`MyGame.challenge.checkRequirements 0: ${MyGame.challenge.checkRequirements(0, MyGame.player)}`);

				// Create debug payload buttons

				// For each database key
				let databaseKeys = Object.keys(qrcodeDatabase);
				databaseKeys.forEach(key => {
					// If not a core record, stop
					if ( ['items', 'challenges', 'locations', 'characters'].indexOf(key) === -1 )
					{
						return;
					}

					// For each record id
					let recordIds = Object.keys(qrcodeDatabase[key]);
					recordIds.forEach(id => {
						// Get record, create payload, create option
						let record = qrcodeDatabase[key][id];
						let payload = {[key]:{[id]:''}};
						DebugUi.createPayloadOptions(key, record, payload);
					});
				});

				// Create adhoc payload options
				DebugUi.createPayloadOptions('challenges', 'Challenges 1-4', {challenges:{1:'', 2:'', 3:'', 4:''}});

				/*
				MyGame.queueAction({type: 'modal', params: {title: 'Title 1', body: 'My body'}, test: 'doSomething'});
				MyGame.queueAction([
					{type: 'modal', params: {title: 'Title 2', body: 'My body', dataOnly: true}, test: 'doSomething1'},
					{type: 'modal', params: {title: 'Title 3', body: 'My body'}, test: 'doSomething2'},
					{type: 'modal', params: {title: 'Title 4', body: 'My body'}, test: 'doSomething3'}
				], true);
				*/

				//MyGame.character.showInteraction('character', 1);
				//MyGame.character.showInteraction('character', 1, 'arrayEntry');
				//MyGame.character.showInteraction('character', 1, 'nestedEntry1');
				//MyGame.character.showInteraction('character', 1, 'optionsEntry');
				//MyGame.character.showInteraction('game', '', 'welcome');
				//MyGame.character.showInteraction('character', 1);//, 'entryTemplate'
				//MyGame.character.showInteraction('character', 2);
				//MyGame.character.showInteraction('character', 4);
				//MyGame.startPrompt('billyQuiz');

				// Object sorting example 
				/* let items = {
					"1": {
						"type": "item",
						"name": "E"
					},
					"2": {
						"type": "challenge",
						"name": "D"
					},
					"3": {
						"type": "item",
						"name": "C"
					},
					"4": {
						"type": "prompt",
						"name": "B"
					},
					"5": {
						"type": "item",
						"name": "A"
					}
				};
				let items = {
					"1": {
						"type": "object",
						"name": "R"
					},
					"2": {
						"type": "object",
						"name": "B"
					},
					"3": {
						"type": "object",
						"name": "G"
					}
				};

				// Turn object into an array for sorting
				let itemsArray = [];
				let itemKeys = Object.keys(items);
				itemKeys.forEach(key => {
					let item = structuredClone(items[key]);
					item.id = key;
					itemsArray.push(item);
				});

				// Sort by name and type
				itemsArray.sort((a, b) => {
					let aType = a.type ?? '';
					let bType = b.type ?? '';
					let aName = a.name ?? '';
					let bName = b.name ?? '';
					if ( aName == bName ) return 0;
					return ( aName > bName ) ? 1 : -1;
				});
				// Sort by name and type
				itemsArray.sort((a, b) => {
					let aType = a.type ?? '';
					let bType = b.type ?? '';
					let aName = a.name ?? '';
					let bName = b.name ?? '';
					if ( aType == bType ) return 0;
					return ( aType > bType ) ? 1 : -1;
				});
				console.log(itemsArray);
				/**/
			});


			//====================
			// Continue polling
		}
	}, 100);
})();