class Player
{
	// Properties

	// Public

	events;
	eventNames = {
		reset: 'resetPlayer',
		stats: 'statsPlayer',
		statsHealth: 'statsPlayerHealth',
		statsExp: 'statsPlayerExp',
		statsSpecial: 'statsPlayerSpecial',
		statsCurrency: 'statsPlayerCurrency',
		statsPoints: 'statsPlayerPoints',
		updateHud: 'updatePlayerHud'
	};
	
	id = 0;

	statLabels = {
		health: 'Health',
		exp: 'Experience',
		special: 'Magic',
		currency: 'Coins',
		points: 'Points',

		// Character-based
		hitPoints: 'Hit Points'
	};

	statsDefault = {
		health: 100,
		exp: 0,
		special: 0,
		currency: 0,
		points: 0,

		// Character-based
		hitPoints: 0
	};
	stats;
	preUpdateStats;

	// Methods

    constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.ui = new PlayerUi(this.game);
		this.events = new EventHandler(this.eventNames);
		this.events.on(this.events.names.stats, this.checkPlayerStats);
		this.reset();
    }
	
	reset ()
	{
		this.stats = structuredClone(this.statsDefault);
		// Trigger event
		this.events.trigger(this.events.names.reset);
	}

	doPreUpdateSnapshot () {
		this.preUpdateStats = structuredClone(this.stats);
	}

	checkPlayerStats ( listenerData ) {
		this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);

		// Check health
		if ( this.game.player.health <= 0 )
		{
			// Game over
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: ${this.game.player.health} Health. Game Over!`);
			this.game.over();
		}
	}

	// Getters and Setters

	get health () {
		return this.stats.health;
	}
	set health ( value ) {
		let prevValue = this.stats.health;

		// Normalize value
		if ( value < 0 )
		{
			value = 0;
		}

		this.stats.health = value;

		// Trigger event
		let listenerData = {
			key: 'health',
			value: this.stats.health,
			prevValue: prevValue
		};
		this.events.trigger(this.events.names.statsHealth, listenerData);
		this.events.trigger(this.events.names.stats, listenerData);
	}

	get exp () {
		return this.stats.exp;
	}
	set exp ( value ) {
		let prevValue = this.stats.exp;
		this.stats.exp = value;

		// Trigger event
		let listenerData = {
			key: 'exp',
			value: this.stats.exp,
			prevValue: prevValue
		};
		this.events.trigger(this.events.names.statsExp, listenerData);
		this.events.trigger(this.events.names.stats, listenerData);
	}

	get special () {
		return this.stats.special;
	}
	set special ( value ) {
		let prevValue = this.stats.special;
		this.stats.special = value;

		// Trigger event
		let listenerData = {
			key: 'special',
			value: this.stats.special,
			prevValue: prevValue
		};
		this.events.trigger(this.events.names.statsSpecial, listenerData);
		this.events.trigger(this.events.names.stats, listenerData);
	}

	get currency () {
		return this.stats.currency;
	}
	set currency ( value ) {
		let prevValue = this.stats.currency;
		this.stats.currency = value;

		// Trigger event
		let listenerData = {
			key: 'currency',
			value: this.stats.currency,
			prevValue: prevValue
		};
		this.events.trigger(this.events.names.statsCurrency, listenerData);
		this.events.trigger(this.events.names.stats, listenerData);
	}

	get points () {
		return this.stats.points;
	}
	set points ( value ) {
		let prevValue = this.stats.points;
		this.stats.points = value;

		// Trigger event
		let listenerData = {
			key: 'points',
			value: this.stats.points,
			prevValue: prevValue
		};
		this.events.trigger(this.events.names.statsPoints, listenerData);
		this.events.trigger(this.events.names.stats, listenerData);
	}
	
}
