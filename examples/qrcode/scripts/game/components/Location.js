/**
 * 
 */
class Location
{
	// Properties

	events;
	eventNames = {
		add: 'addLocation',
		activate: 'activateLocation',
		deactivate: 'deactivateLocation',
		use: 'useLocation',
		useThrottle: 'useThrottleLocation',
		remove: 'removeLocation',
		stats: 'statsLocation',
		reset: 'resetLocation',
		hint: 'hintLocation',
		updateHud: 'updateLocationHud'
	};

	locations;
	active;
	
	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.events = new EventHandler(this.eventNames);
		this.ui = new LocationUi(this.game);
		this.reset();
    }
	
	/**
	 * 
	 * @returns void
	 */
	reset ()
	{
		this.locations = {};
		// Trigger event
		this.events.trigger(this.events.names.reset);
	}

	/**
	 * 
	 * @returns void
	 */
	add ( locationId, params )
	{
		// Stop if there is no item reference
		if ( ! Reflect.has(this.game.database.locations, locationId) )
		{
			this.game.logger.debug(`The location does not exist.`);
			return;
		}

		/* Allow re-adding
		// If we already have this location, stop
		if ( Reflect.has(this.locations, locationId) )
		{
			this.game.logger.debug(`The location has already been added.`);
			return;
		}
		*/
		
		// If record is new, add it
		// this.game.ledger.check('count', {entry: {type: 'location', action: 'add', id: locationId}}) === 1
		let newRecord = ( ! Reflect.has(this.locations, locationId) );
		if ( newRecord )
		{
			// Deep copy item reference
			this.locations[locationId] = structuredClone(this.game.database.locations[locationId]);
			this.locations[locationId].addTime = Date.now();
		}

		let location = this.locations[locationId];
		
		// Add to ledger
		this.game.ledger.add({
			type: 'location',
			action: 'add',
			id: locationId
		});

		// Trigger event
		this.events.trigger(this.events.names.add, locationId, location, newRecord, params);

		// If there is a payload on first add or after
		if ( newRecord && location.payloadOnFirstAdd )
		{
			this.game.queueAction({type: 'payload', params: location.payloadOnFirstAdd}, true);
		}
		else if ( location.payload )
		{
			this.game.queueAction({type: 'payload', params: location.payload}, true);
		}

		// Check if a challenge or game submit
		this.game.checkSubmits({
			type: 'location',
			action: 'add',
			id: locationId
		});
		
		// If activate on add
		if ( location.activateOnAdd )
		{
			this.activate(locationId);
		}

		// If use on add
		if ( location.useOnAdd )
		{
			this.use(locationId);
		}
	}

	/**
	 * 
	 * @returns void
	 */
	remove ( locationId, params )
	{
		// If item not in database, stop
		if ( ! Reflect.has(this.game.database.locations, locationId) )
		{
			this.game.logger.debug(`The location does not exist.`);
			return;
		}
		
		// If location wasn't found, stop
		if ( ! Reflect.has(this.locations, locationId) )
		{
			this.game.logger.debug(`The location was never found.`);
			return;
		}
		
		delete this.locations[locationId];

		// Add to ledger
		this.game.ledger.add({
			type: 'location',
			action: 'remove',
			id: locationId
		});

		// Trigger event
		this.events.trigger(this.events.names.remove, locationId, params);
	}

	/**
	 * 
	 * @returns void
	 */
	use ( locationId, params )
	{
		let location = this.locations[locationId];
		
		// If throttling use
		if ( location.useThrottleMs )
		{
			// If using sooner than throttle time, stop
			if ( Date.now() - location.lastUseTime < location.useThrottleMs )
			{
				// Trigger event
				this.events.trigger(this.events.names.useThrottle, locationId, location, params);
				return;
			}
		}
		
		// Update time info
		location.lastUseTime = Date.now();

		// If location stats get updated after use
		if ( location.statsUpdateOnUse )
		{
			// For each key
			let statKeys = Object.keys(location.statsUpdateOnUse);
			statKeys.forEach(key => {
				// Stop if inventory stat nonexistent
				if ( ! Reflect.has(location.stats, key) )
				{
					return;
				}

				// Update location stats
				location.stats[key] += parseFloat(location.statsUpdateOnUse[key]);
			});

			// If the location uses health
			if ( Reflect.has(location.stats, 'health') )
			{
				// If no more health or energy is left, reduce stock
				if ( location.stats.health <= 0 )
				{
				}
			}
			
			// Trigger event
			this.events.trigger(this.events.names.stats, locationId, params);
		}

		// If player stats get updated after use
		if ( location.playerStatsUpdateOnUse )
		{
			// For each key
			let playerStatsKeys = Object.keys(location.playerStatsUpdateOnUse);
			playerStatsKeys.forEach(key => {
				// Stop if player stat nonexistent
				if ( ! Reflect.has(this.game.player.stats, key) )
				{
					return;
				}

				this.game.player.stats[key] += parseFloat(location.playerStatsUpdateOnUse[key]);
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats, locationId, location);
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'location',
			action: 'use',
			id: locationId
		});

		// Trigger event
		this.events.trigger(this.events.names.use, locationId, location, params);

		// Check if a challenge or game submit
		this.game.checkSubmits({
			type: 'location',
			action: 'use',
			id: locationId
		});
	}

	/**
	 * 
	 * @returns void
	 */
	activate ( locationId, params )
	{
		let location = this.locations[locationId];
		location.active = true;
		location.activateTime = Date.now();

		// Set active
		this.active = location;
		
		// Update player stats after activate
		if ( location.playerStatsUpdateOnActivate )
		{
			// For each key
			let playerStatsKeys = Object.keys(location.playerStatsUpdateOnActivate);
			playerStatsKeys.forEach(key => {
				// Update player stats directly
				this.game.player.stats[key] += parseFloat(location.playerStatsUpdateOnActivate[key]);
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats);
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'location',
			action: 'activate',
			id: locationId,
			time: location.activateTime
		});

		// Trigger event
		this.events.trigger(this.events.names.activate, locationId, location, params);

		// If use on activate
		if ( location.useOnActivate )
		{
			this.use(locationId);
		}
	}

	/**
	 * 
	 * @returns void
	 */
	deactivate ( locationId, params )
	{
		let location = this.locations[locationId];
		location.active = false;

		// If the location is active, unset active
		if ( this.active && this.active.id == locationId )
		{
			this.active = undefined;
		}
		
		// Update player stats after deactivate
		if ( location.playerStatsUpdateOnActivate )
		{
			// For each key
			let playerStatsKeys = Object.keys(location.playerStatsUpdateOnActivate);
			playerStatsKeys.forEach(key => {
				// Update player stats directly
				this.game.player.stats[key] -= parseFloat(location.playerStatsUpdateOnActivate[key]);
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats);
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'location',
			action: 'deactivate',
			id: locationId
		});

		// Trigger event
		this.events.trigger(this.events.names.deactivate, locationId, location, params);

		// When leaving a location, deactivate associated active challenges
		// For each active challenge
		let activeChallengeIds = Object.keys(this.game.challenge.activeChallenges);
		activeChallengeIds.forEach(activeChallengeId => {
			// Get reference
			let challenge = this.game.challenge.challenges[activeChallengeId];
			// If challenge has locations
			if ( challenge.locations )
			{
				let challengeLocationIds = Object.keys(challenge.locations);
				// If active location in challenge locations
				if ( challengeLocationIds.indexOf(locationId) !== -1 )
				{
					// Deactivate it
					this.game.challenge.deactivate(activeChallengeId);
				}
			}
		});
	}
	
}