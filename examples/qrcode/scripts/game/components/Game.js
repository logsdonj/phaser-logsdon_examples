class Game
{
	// Properties
	
	// Static - static myStaticProp = 'Test';

	// Use inst getters and setters
	static instance;

	// Private - #myPrivateProp = 'Test';

	#_title = 'Test Game';
	#_description = 'A game for testing.';
	#_version = '0.1';

	// Public - myPublicProp = 'Test';

	events;
	eventNames = {
		reset: 'resetGame',
		processPayloadBefore: 'processPayloadBefore',
		processPayloadAfter: 'processPayloadAfter',
		processItemsPayloadBefore: 'processItemsPayloadBefore',
		processItemsPayloadAfter: 'processItemsPayloadAfter',
		processChallengesPayloadBefore: 'processChallengesPayloadBefore',
		processChallengesPayloadAfter: 'processChallengesPayloadAfter',
		processLocationsPayloadBefore: 'processLocationsPayloadBefore',
		processLocationsPayloadAfter: 'processLocationsPayloadAfter',
		processCharactersPayloadBefore: 'processCharactersPayloadBefore',
		processCharactersPayloadAfter: 'processCharactersPayloadAfter',
		processActionsPayloadBefore: 'processActionsPayloadBefore',
		processActionsPayloadAfter: 'processActionsPayloadAfter',
		showScreen: 'showScreen',
		start: 'startGame',
		progress: 'progressGame',
		complete: 'completeGame',
		over: 'overGame',
		stop: 'stopGame',
		hint: 'hintGame',
		updateHud: 'updateGameHud',
		error: 'errorGame'
	};

	// A queue of actions to process, like multiple modals
	actionQueue;
	actionQueueTimerId;

	player;
	challenge;
	item;
	location;
	character;

	database;
	logger;
	ledger;

	toast;
	modal;

	debug;
	completeCount = 0;
	overCount = 0;
	freeplay;
	
	active;
	isStarted;
	isStopped;
	currentOver;
	currentComplete;

	startTime;
	stopTime;

	// Keep track of stat timers and associated data
	statTimers = {};

	// Methods

	/**
	 * 
	 * @returns void
	 */
    constructor ( database )
	{
		this.database = database;
		this.inst = this;
		DebugUi.game = this;

		this.events = new EventHandler(this.eventNames);

		this.ui = new GameUi(this);
		this.ui.nav = new NavUi(this);
		this.ui.cta = new CtaUi(this);
		this.ui.componentInfo = new ComponentInfoUi(this);
		this.ui.camera = new CameraUi(this);
		this.ui.tour = new TourUi(this);

		this.logger = new Logger();
		this.ledger = new Ledger();
		this.toast = new Toast();

		this.player = new Player(this);
		this.challenge = new Challenge(this);
		this.item = new Item(this);
		this.location = new Location(this);
		this.character = new Character(this);

		this.reset();
    }
	
	/**
	 * 
	 * @returns void
	 */
	reset ()
	{
		this.currentOver = false;
		this.currentComplete = false;
		this.active = false;
		this.isStarted = false;
		this.isStopped = false;

		this.actionQueue = [];
		this.ledger.reset();
		// Trigger event
		this.events.trigger(this.events.names.reset);
	}

	queueAction ( actions, poll = true, bypassQueue = false )
	{
		// If an array not passed, turn into array
		if ( ! Array.isArray(actions) )
		{
			actions = [actions];
		}

		// For each action, add it to queue
		for ( let action of actions )
		{
			if ( bypassQueue || action.bypassQueue )
			{
				action.bypassQueue = true;
				this.processAction(action);
			}
			else
			{
				this.actionQueue.push(action);
			}
		}

		// If automatically starting the poll
		if ( poll )
		{
			this.pollActionQueue();
		}
	}

	// https://brianchildress.co/simple-polling-using-settimeout/
	// https://stackoverflow.com/a/24989958
	pollActionQueue ()
	{
		// Debounce, putting off execution until no more poll requests after the interval
		clearTimeout(this.actionQueueTimerId);
		
		// If actions, set timeout
		if ( this.actionQueue.length > 0 )
		{
			this.actionQueueTimerId = setTimeout(this.processActionQueue.bind(this), 100);
		}
	}

	processActionQueue ( params )
	{
		// If modal open, keep pooling
		if ( Modal.shown && ! params?.bypassQueue )// || document.querySelector('.introjs-overlay')
		{
			console.log('modals open', Modal.numShown);
			console.log(`processActionQueue: ${Modal.numShown} open, delay`);
			this.pollActionQueue();
			return;
		}

		// Assign default params
		params = Object.assign({}, {
			all: false
		}, params);

		// Now that we're processing, make sure polling is stopped
		clearTimeout(this.actionQueueTimerId);
		
		// If no actions, restart poll and stop
		if ( this.actionQueue.length == 0 )
		{
			//this.pollActionQueue();
			return;
		}

		// If processing all actions
		if ( params.all )
		{
			// https://stackoverflow.com/questions/3010840/loop-through-an-array-in-javascript
			// https://stackoverflow.com/questions/34348937/access-to-es6-array-element-index-inside-for-of-loop
			// https://stackoverflow.com/questions/5767325/how-can-i-remove-a-specific-item-from-an-array
			//this.actionQueue.forEach((action) => {
			//for (const action of this.actionQueue)
			for ( let [index, action] of this.actionQueue.entries() )
			{
				// Run action
				// Remove it from queue
				this.actionQueue.splice(index, 1); // 2nd parameter means remove one item only
			}
		}
		// Else process actions one at a time
		else
		{
			// Get next action
			let action = this.actionQueue.shift();
			
			// Check the type of access to process
			this.processAction(action);

			/*
			// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises
			// Process the first item
			const myPromise = new Promise(function(resolve, reject)
			{
				
				if ( action.type === 'modal' )
				{
					action.params.dataOnly = true;
					let modalData = Modal.create(action.params);
	
					// Limit closing modal to only given options
					modalData.$modal.find('.btn-close').remove();
					$(modalData.modalIdSelector).modal({backdrop: 'static', keyboard: false}).modal('show');

					$(`${modalData.modalIdSelector} .button--confirm`).click(function(){
						//resolve("user clicked");
						// Hide modal
						$(modalData.modalIdSelector).modal('hide');
					});
					$(`${modalData.modalIdSelector} .button--cancel`).click(function(){
						//reject("user clicked cancel");
						// Hide modal
						$(modalData.modalIdSelector).modal('hide');
					});

					modalData.modalElement.addEventListener('hidden.bs.modal', event => {
						resolve("user clicked");
					});
				}
				else
				{
					resolve();
				}
			})
			.then(function(val){
				//val is your returned value. argument called with resolve.
				// Process the next action
				this.processActionQueue(params);
			}.bind(this))
			.catch(function(err){
				//user clicked cancel
				console.log(`processActionQueue error: ${err}`);
				// Process the next action
				this.processActionQueue(params);
			}.bind(this));
			*/
		}
	}

	processAction ( action )
	{
		if ( action.type === 'modal' )
		{
			action.params.dataOnly = true;
			let modalData = Modal.create(action.params);

			// Limit closing modal to only given options
			modalData.$modal.find('.btn-close').addClass('d-none');
			Modal.show($(modalData.modalIdSelector), {backdrop: 'static', keyboard: false});

			$(`${modalData.modalIdSelector} .button--confirm`).click(function(){
				// Hide modal
				Modal.hide($(modalData.modalIdSelector));
			});
			$(`${modalData.modalIdSelector} .button--cancel`).click(function(){
				// Hide modal
				Modal.hide($(modalData.modalIdSelector));
			});
		}
		else if ( action.type === 'toast' )
		{
			this.toast.create(this.toast[action.params.type], action.params);

			// If not skipping poll, poll
			if ( ! action.params.skipPoll )
			{
				this.pollActionQueue();
			}
		}
		else if ( action.type === 'interaction' )
		{
			this.character.showInteraction(action.params.type, action.params.id, action.params.entryId, action.params.entryIndex, action.params.data);
		}
		else if ( action.type === 'prompt' )
		{
			this.startPrompt(action.params);
		}
		else if ( action.type === 'payload' )
		{
			this.processPayload(action.params);
			
			// If not skipping poll, poll
			if ( ! action.params.skipPoll )
			{
				this.pollActionQueue();
			}
		}
		else if ( action.type === 'callback' )
		{
			if ( typeof(action.callback) == 'function' )
			{
				action.callback();
			}
		}
		// Else action type not found, next
		else
		{
			// Process the next action
			//this.processActionQueue(params);
			// OR
			// Continue polling
			this.pollActionQueue();
		}
	}

	// Getters and Setters

	get version () {
		return this.#_version.toString();
	}

	get player () {
		return this.player;
	}

	set player ( value ) {
		this.player = value;
		return this.player;
	}

	// Handle payloads

	/**
	 * Validate payload integrity, including format
	 * @param {object} payload 
	 * @returns 
	 */
	validatePayload ( payload )
	{
		if ( ! payload )
		{
			return false;
		}

		return true;
	}

	/**
	 * Process payload object of multiple objects like items and challenges
	 * @param {object} payload 
	 * @returns void
	 */
	processPayload ( payload )
	{
		// Handle string as JSON
		if ( typeof(payload) == 'string' )
		{
			payload = RL.u.safelyParseJSON(payload);
		}

		if ( ! this.validatePayload(payload) )
		{
			return false;
		}

		// Trigger event
		this.events.trigger(this.events.names.processPayloadBefore);

		// Process
		
		let keys = Object.keys(payload);
		keys.forEach(key => {
			if ( ['items', 'challenges', 'locations', 'characters'].indexOf(key) !== -1 )
			{
				this.processComponentPayload(key, payload[key]);
			}
			else if ( ['actions'].indexOf(key) !== -1 )
			{
				this[`process${RL.u.capitalize(key)}Payload`](payload[key]);
			}
			
		});

		// Trigger event
		this.events.trigger(this.events.names.processPayloadAfter);
	}

	/**
	 * Process items payload object
	 * @param {object} payload 
	 * @returns void
	 */
	processComponentPayload ( componentType, payload )
	{
		if ( ! this.validatePayload(payload) )
		{
			return false;
		}

		let componentTypeCapitalized = RL.u.capitalize(componentType);
		let componentTypeSingular = componentType.slice(0, -1);
		
		// Trigger event
		this.events.trigger(this.events.names[`process${componentTypeCapitalized}PayloadBefore`]);

		// Process

		let errorKeys = [];

		let keys = Object.keys(payload);
		keys.forEach(key => {
			// Stop if there is no item reference
			if ( ! Reflect.has(this.database[componentType], key) )
			{
				errorKeys.push(key);
				return;
			}

			// Initialize item
			this[componentTypeSingular].add(key, payload[key]);
		});
		
		// Trigger event
		this.events.trigger(this.events.names[`process${componentTypeCapitalized}PayloadAfter`]);
	}

	/**
	 * Process actions payload object
	 * @param {object} payload 
	 * @returns void
	 */
	processActionsPayload ( payload )
	{
		if ( ! this.validatePayload(payload) )
		{
			return false;
		}
		
		// Trigger event
		this.events.trigger(this.events.names.processActionsPayloadBefore);

		// Process

		// Ensure array
		if ( ! Array.isArray(payload) )
		{
			payload = [payload]
		}
		
		// For each action
		for ( let action of payload )
		{
			// If showing a screen
			if ( action.action == 'showScreen' )
			{
				this.showScreen(action.params.screen, action.params.prevScreen)
			}
			// If starting an interaction
			else if ( action.action == 'interaction' )
			{
				this.queueAction({type: 'interaction', params: action.params}, true);
			}
			// If starting a prompt
			else if ( action.action == 'prompt' )
			{
				this.queueAction({type: 'prompt', params: action.params}, true);
			}
			// If starting a timer or destroying a timer
			else if ( ['createStatTimer', 'destroyStatTimer'].indexOf(action.action) !== -1 )
			{
				this.queueAction({type: 'callback', callback: () => {
					this[action.action](action.params);
				}}, true);
			}
			// If running a function
			else if ( action.action == 'windowCallback' )
			{
				this.queueAction({type: 'callback', callback: () => {
					window[action.params.callback](action.params.params);
				}}, true);
			}
			// Remove item from component
			else if ( action.action.startsWith('remove') )
			{
				// Isolate component
				let target = action.action.slice('remove'.length).replace('remove', '', 0).toLowerCase();
				if ( ['item', 'challenge', 'character', 'location'].indexOf(target) !== -1 )
				{
					this[target].remove(action.params.id);
				}
			}
			// Remove item from component
			else if ( action.action == 'itemStock' )
			{
				let item = this.item.inventory[action.params.itemId];
				if ( item )
				{
					item.stock += action.params.amount ?? 1;
				}
			}
		}
		
		// Trigger event
		this.events.trigger(this.events.names.processActionsPayloadAfter);
	}

	// You can treat stat timers like poison/decay/rot/toxin and antidote/remedy/cure/antitoxin
	createStatTimer ( data )
	{
		let defaultData = {
			id: RL.u.getRandomInt(0, 99999),
			type: 'player',
			typeId: '',
			stat: 'health',
			valueIncrement: -1,
			valueRangeMin: undefined,
			valueRangeMax: undefined,
			timeoutS: 1,
			stop: false,
			timeoutId: 0
		};
		data = Object.assign({}, defaultData, data);

		this.statTimers[data.id] = data;
		let timeoutId = this.runStatTimer(data);
		this.statTimers[data.id].timeoutId = timeoutId;
	}

	runStatTimer ( data )
	{
		// Create new timeout
		let timeoutId = setTimeout(data => {
			// If not stopping and value is still positive, repeat
			if ( ! data.stop )
			{
				if ( data.type == 'player' && this.player[data.stat] )
				{
					// Reduce stat by a specific amount or random from range
					this.player[data.stat] += data.valueIncrement;
				}
				else if ( data.type == 'item' && 
					this.inventory[data.typeId] && 
					this.inventory[data.typeId].stats && 
					this.inventory[data.typeId].stats[data.stat] 
				)
				{
					this.inventory[data.typeId].stats[data.stat] += data.valueIncrement;
				}

				// Restart
				timeoutId = this.runStatTimer(data);
				// Refresh game object stat timer data
				data.timeoutId = timeoutId;
				this.statTimers[data.id] = data;
			}
		}, data.timeoutS * 1000, data);

		return timeoutId;
	}

	destroyStatTimer ( timerId )
	{
		// If an object passed, check for the id in parameters
		if ( typeof(timerId) == 'object' && timerId.id )
		{
			timerId = timerId.id;
		}

		// Clear timer
		if ( this.statTimers[timerId] )
		{
			let timer = this.statTimers[timerId];
			timer.stop = true;
			clearTimeout(timer.timeoutId);
			delete this.statTimers[timerId], timer;
			return true;
		}
	}


	// TODO: Also get # total for completionist... mission accomplished vs complete; for awards functionality
	// TODO: Change requirements depending on mode; scavenger mode find all items, locations, and characters, etc.
	/**
	 * Check game requirements
	 * @returns object
	 */
	checkRequirements ( conditions, submitted )
	{
		let defaultData = {
			met: false,
			num: 0,
			numComplete: 0,
			progress: 0,
			progressPercent: 0,
			timeLimitFailure: false,
			timeLimitFailureText: '',
			itemsText: '',
			itemsStrings: [],
			challengesText: '',
			challengesStrings: [],
			locationsText: '',
			locationsStrings: [],
			playerStatsText: '',
			playerStatsStrings: [],
			ledgersStrings: [],
			text: '',
			strings: [],
			missingText: '',
			missingStrings: [],
			metText: '',
			metStrings: []
		};

		// Get references
		let componentNoun = this.getLexicon('game', '', '', 'game');
		let requirements = conditions ?? this.database.game?.requirements ?? {};

		// If there are no requirements or submit triggers, stop
		if ( ! requirements && ! this.submitTriggers && ! this.locations )
		{
			let data = Object.assign(defaultData, {
				met: true,
				progress: 1,
				progressPercent: 100,
				text: 'There are no conditions to meet.'
			});
			return data;
		}

		// Assume all requirements are met, find where they do not
		let met = true;
		let num = 0;
		let numComplete = 0;

		let metStrings = [];
		let missingStrings = [];
		let itemsStrings = [];
		let challengesStrings = [];
		let locationsStrings = [];
		let associatedLocationsStrings = [];
		let playerStatsStrings = [];
		let ledgersStrings = [];
		let matchOfStrings = [];
		let submitTriggersStrings = [];
		let timeStrings = [];
		let timeLimitFailure = false;

		// Check challenge requirements
		let requirementKeys = Object.keys(requirements);
		requirementKeys.forEach(key => {
			// If checking items, challenges, playerStats, or timeLimit
			if ( ['items', 'challenges', 'locations', 'playerStats', 'timeLimit', 'ledgers'].indexOf(key) !== -1 )
			{
				let requirementsData;

				if ( key == 'items' )
				{
					requirementsData = this.checkItemsRequirements(requirements.items);
					itemsStrings = requirementsData.strings;
				}
				// If checking challenges
				else if ( key == 'challenges' )
				{
					requirementsData = this.checkChallengesRequirements(requirements.challenges);
					challengesStrings = requirementsData.strings;
				}
				// If checking locations
				else if ( key == 'locations' )
				{
					requirementsData = this.checkLocationsRequirements(requirements.locations);
					locationsStrings = requirementsData.strings;
				}
				// If checking player stats
				else if ( key == 'playerStats' )
				{
					requirementsData = this.checkPlayerStatsRequirements(requirements.playerStats);
					playerStatsStrings = requirementsData.strings;
				}
				// If checking challenge time in seconds
				else if ( key == 'timeLimit' )
				{
					requirementsData = this.checkTimeLimitRequirements(requirements.timeLimit, 'game');
					timeStrings = requirementsData.strings;
					timeLimitFailure = requirementsData.timeLimitFailure;
				}
				else if ( key == 'ledgers' )
				{
					requirementsData = this.checkLedgersRequirements(requirements.ledgers, 'game');
					ledgersStrings = requirementsData.strings;
				}

				met = ( met ) ? requirementsData.met : met;
				num += requirementsData.num;
				numComplete += requirementsData.numComplete;
				metStrings = metStrings.concat(requirementsData.metStrings);
				missingStrings = missingStrings.concat(requirementsData.missingStrings);
			}
			else if ( key.endsWith('MatchOf') )
			{
				let requirementsData = this.checkMatchOfRequirements(key, requirements[key]);
				matchOfStrings = requirementsData.strings;

				met = ( met ) ? requirementsData.met : met;
				num += requirementsData.num;
				numComplete += requirementsData.numComplete;
				metStrings = metStrings.concat(requirementsData.metStrings);
				missingStrings = missingStrings.concat(requirementsData.missingStrings);
			}
		});

		// Check associated locations
		let associatedLocationsData = this.checkLocationsAssociation('game');
		met = ( met ) ? associatedLocationsData.met : met;
		num += associatedLocationsData.num;
		numComplete += associatedLocationsData.numComplete;
		metStrings = metStrings.concat(associatedLocationsData.metStrings);
		missingStrings = missingStrings.concat(associatedLocationsData.missingStrings);
		associatedLocationsStrings = associatedLocationsData.strings;
		let associatedLocationsText = RL.u.createConjunctionSentence(associatedLocationsStrings, 'You need to ', '.');

		// Check submit triggers
		let triggerData = this.checkSubmitTriggers(this.database.game, submitted);
		submitTriggersStrings = triggerData.strings;
		num += triggerData.num;
		numComplete += triggerData.numComplete;
		metStrings = metStrings.concat(triggerData.metStrings);
		missingStrings = missingStrings.concat(triggerData.missingStrings);

		// Create submit triggers string
		let submitTriggersText = RL.u.createConjunctionSentence(submitTriggersStrings, `You must ', ' to complete the ${componentNoun}.`);
		// Create required player stats string
		let playerStatsText = RL.u.createConjunctionSentence(playerStatsStrings, 'You need ', '.');
		// Create required items string
		let itemsText = RL.u.createConjunctionSentence(itemsStrings, 'You need ', '.');
		// Create required challenges string
		let challengesText = RL.u.createConjunctionSentence(challengesStrings, 'You need to complete ', '.');
		let locationsText = RL.u.createConjunctionSentence(locationsStrings, 'You need to visit ', '.');
		let timeText = timeStrings.join(' ');
		let ledgersText = RL.u.createConjunctionSentence(ledgersStrings, 'You need ', '.');
		let matchOfText = RL.u.createConjunctionSentence(matchOfStrings, 'You need ', '.');
		let missingText = RL.u.createConjunctionSentence(missingStrings, '', '.');
		let metText = RL.u.createConjunctionSentence(metStrings, 'You completed ', '.');
		// If there was a time limit failure, prompt to restart the challenge
		let timeLimitFailureText = '';
		if ( timeLimitFailure )
		{
			timeLimitFailureText = `Because of the time limit, you will need to restart the ${componentNoun}.`;
			missingText += ' ' + timeLimitFailureText;
		}

		let data = Object.assign(defaultData, {
			met: met,
			num: num,
			numComplete: numComplete,
			progress: ( num > 0 ) ? (numComplete / num) : 0,
			progressPercent: ( num > 0 ) ? ((numComplete / num) * 100).toFixed(2) : 0,
			challengesText: challengesText,
			challengesStrings: challengesStrings,
			timeLimitFailure: timeLimitFailure,
			timeLimitFailureText: timeLimitFailureText,
			timeText: timeText,
			timeStrings: timeStrings,
			text: [itemsText, challengesText, locationsText, associatedLocationsText, playerStatsText, ledgersText, matchOfText, timeText, submitTriggersText].join(' ').trim(),
			strings: [itemsStrings, challengesStrings, locationsStrings, associatedLocationsStrings, playerStatsStrings, timeStrings, ledgersStrings, matchOfStrings, submitTriggersStrings].concat(),
			missingText: missingText,
			missingStrings: missingStrings,
			metText: metText,
			metStrings: metStrings
		});

		// Set default text if no requirements
		if ( data.text == '' )
		{
			data.text = 'There are no requirements to complete.';
		}

		return data;
	}
	
	/**
	 * Check game progress
	 * @returns void
	 */
	checkProgress ()
	{
		let gameRequirementsData = this.checkRequirements();

		// Trigger event
		this.events.trigger(this.events.names.progress, gameRequirementsData);

		
		// If time limit failure, game over
		if ( gameRequirementsData.timeLimitFailure )
		{
			return this.over(gameRequirementsData);
		}
		// If no submit trigger, check complete
		else if ( ! this.database.game?.submitTriggers )
		{
			this.checkComplete();
		}
	}

	/**
	 * Check game complete
	 * @returns void
	 */
	checkComplete ()
	{
		this.logger.debug(`Game checkComplete`);

		// Check if the game has been completed
		let gameRequirementsData = this.checkRequirements();
		// If complete, complete it
		if ( gameRequirementsData.met )
		{
			return this.complete( gameRequirementsData );
		}
	}

	/**
	 * Start the game
	 * @returns boolean
	 */
	start ()
	{
		this.logger.debug(`Game start`);

		this.startTime = Date.now();
		this.active = true;
		this.isStarted = true;

		// If there is an payload on start, queue it
		if ( Reflect.has(this.database.game, 'payloadOnStart') )
		{
			this.queueAction({type: 'payload', params: this.database.game.payloadOnStart}, true);
		}

		// If there is an intro, show it
		if ( Reflect.has(this.database.game, 'intro') )
		{
			let componentNoun = this.getLexicon('game', '', '', 'game');
			let intro = this.database.game.intro;
			let title = ( typeof(intro) == 'object' && Reflect.has(intro, 'title') ) ? intro.title : `${RL.u.capitalize(componentNoun)} Intro`;
			let body = this.parseShortcodes( ( typeof(intro) == 'object' && Reflect.has(intro, 'body') ) ? intro.body : intro );
			
			this.queueAction({type: 'modal', params: {
				title: title, 
				body: body,
				cancelButtonHide: true,
				confirmButtonLabel: 'Continue'
			}}, true);
		}
	
		// If there is a welcome entry in the game interaction manifest
		if ( this.database.game.interactionManifest?.welcome )
		{
			this.queueAction({type: 'interaction', params: {
				type: 'game',
				id: '',
				entryId: 'welcome'
			}}, true);
			//this.character.showInteraction('game', '', 'welcome');
		}

		// Trigger event
		this.events.trigger(this.events.names.start);

		return true;
	}

	/**
	 * Complete the game
	 * @returns void
	 */
	complete ()
	{
		this.logger.debug(`Game complete`);

		// If current already complete, stop
		// Safety stop because complete could be called multiple times from progress checks
		if ( this.currentComplete )
		{
			return;
		}

		this.currentComplete = true;
		this.completeCount++;
		this.ledger.gameCompleteCount++;
		this.stopTime = Date.now();
		
		// Give awards like completionist

		// Notify and allow to replay any challenge from listing. Maybe hint or notify of remaining, side, or hidden challenges.

		// Trigger event
		this.events.trigger(this.events.names.complete);

		this.stop();

		return true;
	}

	// Game over
	over ()
	{
		this.logger.debug(`Game over`);

		this.currentOver = true;
		this.overCount++;

		// Trigger event
		this.events.trigger(this.events.names.over);

		this.stop();
	}

	// Stop game
	stop ()
	{
		this.logger.debug(`Game stop`);

		this.active = false;
		this.isStopped = true;

		// Destroy stat timers or other timers
		let statTimerKeys = Object.keys(this.statTimers);
		statTimerKeys.forEach(keyId => {
			this.destroyStatTimer(keyId);
		});

		// Destroy challenge timers
		// For each active challenge
		let activeChallengeIds = Object.keys(this.challenge.activeChallenges);
		for ( let activeChallengeId of activeChallengeIds )
		{
			let challenge = this.challenge.challenges[activeChallengeId];
			// If timer exists, stop
			if ( challenge.timer )
			{
				challenge.timer.stop();
			}
		}

		// Trigger event
		this.events.trigger(this.events.names.stop);
	}

	showScreen ( screen, prevScreen)
	{
		// Trigger event
		this.events.trigger(this.events.names.showScreen, screen, prevScreen);
	}

	/**
	 * Show a hint
	 * 
	 * @param {int} id 
	 * @returns array
	 */
	showHint ( type, id, bypassQueue )
	{
		let pluralType = ( type == 'item') ? 'inventory' : `${type}s`;

		this.logger.debug(`showHint game[${type}][${pluralType}][${id}]`);

		// If no record, stop
		if ( 
			! Reflect.has(this, type) || 
			! Reflect.has(this[type], pluralType) || 
			! Reflect.has(this[type][pluralType], id)
		)
		{
			this.logger.debug(`showHint: No record type found.`);
			return;
		}

		let record = ( type == 'game' ) ? this : this[type][pluralType][id];

		// If no hints, stop
		if ( ! Reflect.has(record, 'hints') )
		{
			this.logger.debug(`Record has no hints.`);
			return;
		}

		// Ensure array
		if ( ! Array.isArray(record.hints) )
		{
			record.hints = [record.hints];
		}

		// Get next index, default 0
		let nextHintIndex = ( Reflect.has(record, 'lastHintIndex') ) ? record.lastHintIndex + 1 : 0;
		// If next hint index greater than amount of hints, wrap to 0
		if ( nextHintIndex >= record.hints.length )
		{
			nextHintIndex = 0;
		}

		// Set the last index used
		record.lastHintIndex = nextHintIndex;

		let hint = record.hints[nextHintIndex];

		// Trigger event
		if ( type == 'game' )
		{
			this.events.trigger(this.events.names.hint, '', '', hint, nextHintIndex, record.hints, bypassQueue);
		}
		else
		{
			this[type].events.trigger(this[type].events.names.hint, id, record, hint, nextHintIndex, record.hints, bypassQueue);
		}
	}

	getPlayerOrDefault( player )
	{
		// If no player passed
		if ( ! player )
		{
			// If no default player, stop
			if ( ! this.player )
			{
				this.logger.debug(`The player does not exist.`);
				return;
			}

			// Use default player
			player = this.player;
		}

		return player;
	}

	/**
	 * Check game for a matching trigger
	 * 
	 * @param {object} passedTrigger 
	 * @returns void
	 */
	checkSubmits ( passedTrigger )
	{
		// Check if a challenge submit
		this.challenge.checkActiveSubmit(passedTrigger);
		
		// If game submitted, check complete
		if ( this.isSubmitTrigger('game', '', passedTrigger) )
		{
			return this.checkComplete();
		}
	}

	/**
	 * Check game for a matching submit trigger
	 * 
	 * @param {string} recordType
	 * @param {int} recordId
	 * @param {object} passedTrigger 
	 * @returns boolean
	 */
	isSubmitTrigger ( recordType, recordId, passedTrigger )
	{
		let record;
		
		if ( recordType == 'challenge' )
		{
			record = this.challenge.challenges[recordId];
		}
		else if ( recordType == 'game' )
		{
			record = this.database.game;
		}

		// If record has no submit trigger, stop
		if ( ! record.submitTriggers )
		{
			return;
		}

		let triggerMatch;

		// Compare record trigger keys with passed submit trigger
		if ( record.submitTriggers )
		{
			// Make triggers an array if it isn't already
			if ( ! Array.isArray(record.submitTriggers) )
			{
				record.submitTriggers = [record.submitTriggers];
			}

			// For each trigger
			for ( let submitTrigger of record.submitTriggers )
			{
				// Start each trigger out successful until a failure is found
				triggerMatch = true;

				let recordTriggerKeys = Object.keys(submitTrigger);
				for ( let recordTriggerKey of recordTriggerKeys )
				{
					// Preprocess any special parameters, like counts
		
					// If the record key doesn't exist in passed or does not match, break
					if ( ! passedTrigger[recordTriggerKey] || submitTrigger[recordTriggerKey] != passedTrigger[recordTriggerKey] )
					{
						triggerMatch = false;
						break;
					}
				}

				// If a matching trigger was found, stop
				if ( triggerMatch )
				{
					break;
				}
			}
		}

		return triggerMatch;
	}

	checkSubmitTriggers ( obj, submitted )
	{
		// Assume all requirements are met, find where they do not
		let num = 0;
		let numComplete = 0;
		let strings = [];
		let metStrings = [];
		let missingStrings = [];

		// If submit triggers necessary, include them as a requirement
		if ( obj.submitTriggers )
		{
			// Ensure array
			let submitTriggers = Array.isArray(obj.submitTriggers) ? obj.submitTriggers : [obj.submitTriggers];
			// For each trigger
			for ( let trigger of submitTriggers )
			{
				num++;

				// If submitted, complete
				if ( submitted )
				{
					numComplete++;
				}

				let record;
				let name;
				let action = trigger.action;
				
				// Get records and names based on type
				if ( trigger.type == 'challenge' )
				{
					if ( Reflect.has(this.database.challenges, trigger.id) )
					{
						record = this.database.challenges[trigger.id];
						name = record.name;
					}
				}
				else if ( trigger.type == 'item' )
				{
					if ( Reflect.has(this.database.items, trigger.id) )
					{
						record = this.database.items[trigger.id];
						name = record.name;
					}
				}
				else if ( trigger.type == 'location' )
				{
					if ( Reflect.has(this.database.locations, trigger.id) )
					{
						record = this.database.locations[trigger.id];
						name = record.name;
					}
				}
				else if ( trigger.type == 'character' )
				{
					if ( Reflect.has(this.character.characters, trigger.id) )
					{
						record = this.character.characters[trigger.id];
						name = record.name;
					}
				}

				// Add strings
				strings.push(`${action} ${name}`);

				// If submitted
				if ( submitted )
				{
					metStrings.push(`${action} ${name}`);
				}
				else
				{
					missingStrings.push(`you must ${action} ${name}`);
				}
			}
		}

		return {
			num: num,
			numComplete: numComplete,
			strings: strings,
			metStrings: metStrings,
			missingStrings: missingStrings
		};
	}

	checkItemsRequirements ( requirements )
	{
		// Assume all requirements are met, find where they do not
		let met = true;
		let num = 0;
		let numComplete = 0;
		let strings = [];
		let metStrings = [];
		let missingStrings = [];
		
		// If the player has no inventory, stop
		if ( ! this.item.inventory )
		{
			met = false;
		}

		// Get player inventory item ids
		let inventoryItemIds = Object.keys(this.item.inventory);

		// Get the item ids required
		let requiredItemIds = Object.keys(requirements);
		requiredItemIds.forEach(id => {
			// Track current requirment met
			let currentMet = true;

			// If the player doesn't have the required item, stop
			if ( inventoryItemIds.indexOf(id) === -1 )
			{
				met = false;
				currentMet = false;
			}
			
			// Get the player item stock, default 0
			let playerItemStock = 0;
			// If item exists in the inventory
			if ( Reflect.has(this.item.inventory, id) )
			{
				// Use its stock or default to 1
				playerItemStock = this.item.inventory[id]?.stock ?? 1;
			}

			/* In case checking 0 stock and redundant with following checks of stock and defaulting to check 1 if not 0
			// If out of stock, stop
			if ( playerItemStock < 1 )
			{
				met = false;
				currentMet = false;
			}
			*/

			// Check the required amount, default 1
			let requiredItemStock = requirements[id].stock ?? 1;
			
			// If checking for minimum stock
			if ( 
				Reflect.has(requirements, id) && 
				requirements[id] && 
				Reflect.has(requirements[id], 'stockMin') 
			)
			{
				requiredItemStock = requirements[id].stockMin;
			}

			// If insufficient stock, stop
			if ( playerItemStock < requiredItemStock )
			{
				met = false;
				currentMet = false;
			}

			// If requirement met
			if ( currentMet )
			{
				numComplete += requiredItemStock || 1; // || in case checking 0 stock
			}

			// Increase the amount of requirements
			num += requiredItemStock || 1; // || in case checking 0 stock
			
			// Calculate the number left to complete
			let numLeft = (requiredItemStock || 1) - playerItemStock;
			numLeft = (numLeft < 0) ? 0 : numLeft;
			
			// Get item for reference
			let dbItem = this.database.items[id];

			// Format text articles and pluralization
			let requiredItemName = dbItem.name;
			let requiredItemArticle = RL.u.wordArticlePrefix(requiredItemName);
			// Create requirement text, default singular
			let requiredString = `${requiredItemName}`;
			// If not singular, adjust text
			if ( (requiredItemStock || 1) !== 1 )
			{
				requiredString = `${requiredItemStock || 1} of ${requiredItemName}`;
			}

			strings.push(requiredString);
			
			// If current requirement not met, create description
			if ( ! currentMet )
			{
				let missingString = `${numLeft} ${RL.u.wordPluralizeSuffix(requiredItemName, numLeft)} ${RL.u.numberPluralizeVerb(numLeft, 'is')} missing`;
				missingStrings.push(missingString);
			}
			else
			{
				let metString = `${requiredItemStock || 1} ${RL.u.wordPluralizeSuffix(requiredItemName, requiredItemStock || 1)}`;
				metStrings.push(metString);
			}
		});

		return {
			met: met,
			num: num,
			numComplete: numComplete,
			strings: strings,
			metStrings: metStrings,
			missingStrings: missingStrings
		};
	}

	checkChallengesRequirements ( requirements )
	{
		// Assume all requirements are met, find where they do not
		let met = true;
		let num = 0;
		let numComplete = 0;
		let strings = [];
		let metStrings = [];
		let missingStrings = [];

		// Get completed challenge ids
		let completedChallengeIds = Object.keys(this.challenge.completedChallenges);

		// Get the challenge ids required
		let requiredChallengeIds = Object.keys(requirements);
		// Check if the player has completed each challenge
		requiredChallengeIds.forEach(id => {
			// Track current requirment met
			let currentMet = true;

			// If the player hasn't completed the required challenge, stop
			if ( completedChallengeIds.indexOf(id) === -1 )
			{
				met = false;
				currentMet = false;
			}

			// If requirement met
			if ( currentMet )
			{
				numComplete++;
			}

			// Increase the amount of requirements
			num++;

			// Get challenge for reference
			let dbRequiredChallenge = this.database.challenges[id];
			let name = ( dbRequiredChallenge ) ? dbRequiredChallenge.name : 'Non-existent challenge';

			// Create requirement text description
			let requiredString = `${name}`;
			strings.push(requiredString);
			
			// If current requirement not met, create description
			if ( ! currentMet )
			{
				let missingString = `${name} is incomplete`;
				missingStrings.push(missingString);
			}
			else
			{
				let metString = `${name} is complete`;
				metStrings.push(metString);
			}
		});

		return {
			met: met,
			num: num,
			numComplete: numComplete,
			strings: strings,
			metStrings: metStrings,
			missingStrings: missingStrings
		};
	}

	checkLocationsRequirements ( requirements, challengeId )
	{
		// Assume all requirements are met, find where they do not
		let met = true;
		let num = 0;
		let numComplete = 0;
		let strings = [];
		let metStrings = [];
		let missingStrings = [];

		// Get game location ids
		let locationIds = Object.keys(this.location.locations);

		// Get the location ids required
		let requiredLocationIds = Object.keys(requirements);
		// For each required location
		requiredLocationIds.forEach(id => {
			// Track current requirment met
			let currentMet = true;

			// If the player does not have the required location, stop
			if ( locationIds.indexOf(id) === -1 )
			{
				met = false;
				currentMet = false;
			}

			// If requirement met
			if ( currentMet )
			{
				numComplete++;
			}

			// Increase the amount of requirements
			num++;

			// Get record reference
			let dbRequiredLocation = this.database.locations[id];

			// Create requirement text description
			let requiredString = `${dbRequiredLocation.name}`;
			strings.push(requiredString);
			
			// If current requirement not met, create description
			if ( ! currentMet )
			{
				let missingString = `${dbRequiredLocation.name} is incomplete`;
				missingStrings.push(missingString);
			}
			else
			{
				let metString = `${dbRequiredLocation.name} is complete`;
				metStrings.push(metString);
			}
		});

		return {
			met: met,
			num: num,
			numComplete: numComplete,
			strings: strings,
			metStrings: metStrings,
			missingStrings: missingStrings
		};
	}

	checkLocationsAssociation ( type, typeId )
	{
		// Assume all requirements are met, find where they do not
		let met = true;
		let num = 0;
		let numComplete = 0;
		let strings = [];
		let metStrings = [];
		let missingStrings = [];

		// Check for associated locations
		let record;
		if ( type == 'challenge' )
		{
			record = ( Reflect.has(this.database.challenges, typeId) ) ? this.database.challenges[typeId] : undefined;
		}
		else if ( type == 'game' )
		{
			record = this.database.game;
		}

		// If challenge associated with locations
		if ( record.locations )
		{
			// Track current requirment met
			let currentMet = true;

			let associatedLocationIds = Object.keys(record.locations);

			// If not in a correct location
			/*
			if (
				! this.location.active || 
				(
					this.location.active && 
					associatedLocationIds.indexOf(this.location.active.id) === -1
				)
			)
			*/
			if ( associatedLocationIds.indexOf(this.location.active?.id) === -1 )
			{
				met = false;
				currentMet = false;
			}

			// If requirement met
			if ( currentMet )
			{
				numComplete++;
			}

			// Increase the amount of requirements
			num++;

			// Get all associated location names
			let locationNames = [];
			associatedLocationIds.forEach(id => {
				// Ensure record exists
				let dbLocation = this.database.locations[id];
				if ( dbLocation )
				{
					locationNames.push(dbLocation.name);
				}
			});

			// Create requirement text description
			let locationNamesText = RL.u.createConjunctionSentence(locationNames, '', '', 'or');
			let requiredString = `be in ${locationNamesText}`;
			strings.push(requiredString);
			
			// If current requirement not met, create description
			if ( ! currentMet )
			{
				let missingString = `not in ${locationNamesText}`;
				missingStrings.push(missingString);
			}
			else
			{
				let metString = `in ${locationNamesText}`;
				metStrings.push(metString);
			}
		}

		return {
			met: met,
			num: num,
			numComplete: numComplete,
			strings: strings,
			metStrings: metStrings,
			missingStrings: missingStrings
		};
	}

	checkPlayerStatsRequirements ( requirements, player )
	{
		// Get a player
		player = this.getPlayerOrDefault(player);

		// Assume all requirements are met, find where they do not
		let met = true;
		let num = 0;
		let numComplete = 0;
		let strings = [];
		let metStrings = [];
		let missingStrings = [];

		// Get the player stats required
		let requiredStatKeys = Object.keys(requirements);
		// Check if the player has completed each challenge
		requiredStatKeys.forEach(key => {
			// Track current requirment met
			let currentMet = true;

			// If player stat doesn't exist, stop
			if ( ! Reflect.has(player.stats, key) )
			{
				met = false;
				currentMet = false;
			}

			let requiredPlayerStatValue = requirements[key];
			let playerStatValue = player.stats[key];

			// If an object, check parameters for conditions
			if ( typeof(requiredPlayerStatValue) == 'object' )
			{
				if ( Reflect.has(requiredPlayerStatValue, 'min') )
				{
					if ( playerStatValue < requiredPlayerStatValue.min )
					{
						met = false;
						currentMet = false;
					}
				}
			}
			// If player stat fails condition, stop
			else if ( playerStatValue < requiredPlayerStatValue )
			{
				met = false;
				currentMet = false;
			}

			// If requirement met
			if ( currentMet )
			{
				numComplete++;
			}

			// Increase the amount of requirements
			num++;

			// Gather information to describe the condition not met
			let statLabel = player.statLabels[key];
			let statDiff = requiredPlayerStatValue - playerStatValue;
			statDiff = (statDiff < 0) ? 0 : statDiff;
			
			// Create requirement text description
			let requiredString = `${requiredPlayerStatValue} ${statLabel}`;
			strings.push(requiredString);
			
			// If current requirement not met, create description
			if ( ! currentMet )
			{
				let missingString = `${statDiff} ${statLabel} ${RL.u.numberPluralizeVerb(statDiff, 'is')} needed`;
				missingStrings.push(missingString);
			}
			else
			{
				let metString = `${requiredPlayerStatValue} ${statLabel}`;
				metStrings.push(metString);
			}
		});

		return {
			met: met,
			num: num,
			numComplete: numComplete,
			strings: strings,
			metStrings: metStrings,
			missingStrings: missingStrings
		};
	}

	checkTimeLimitRequirements ( requirements, type, typeId )
	{
		// Assume all requirements are met, find where they do not
		let met = true;
		let num = 0;
		let numComplete = 0;
		let strings = [];
		let metStrings = [];
		let missingStrings = [];
		let timeLimitFailure = false;
		
		// Get challenge reference
		let timeLimit = requirements;

		let now = Date.now();
		let timeTakenMs = 0;
		// If challenge
		if ( type == 'challenge' )
		{
			let challenge = ( Reflect.has(this.challenge.challenges, typeId) ) ? this.challenge.challenges[typeId] : undefined;
			timeTakenMs = now - (challenge ? challenge.activateTime : 0);
		}
		// If game
		else if ( type == 'game' )
		{
			timeTakenMs = ( this.stopTime ?? now ) - this.startTime;
		}

		// Track current requirment met
		let currentMet = true;

		let timeLimitS = timeLimit;
		let timeLimitMs = timeLimitS * 1000;
		let timeTakenS = timeTakenMs / 1000;
		let timeDiffMs = timeTakenMs - timeLimitMs;
		let timeDiffS = timeDiffMs / 1000;
		// If time is past the limit
		if ( timeTakenMs > timeLimitMs )
		{
			met = false;
			currentMet = false;
			timeLimitFailure = true;
		}

		// If requirement met
		if ( currentMet )
		{
			numComplete++;
		}

		// Increase the amount of requirements
		num++;
		
		// Create requirement text description
		let requiredString = `The ${type} must be completed in <i class="icon-timer icon--fa icon--fw"></i>${RL.u.timeDurationToText(timeLimit)}.`;
		strings.push(requiredString);
		
		// If current requirement not met, create description
		if ( ! currentMet )
		{
			let missingString = `time <i class="icon-timer icon--fa icon--fw"></i>${timeDiffS.toFixed(2)} ${RL.u.wordPluralizeSuffix('second', timeDiffS)} over the ${timeLimitS} ${RL.u.wordPluralizeSuffix('second', timeLimitS)} time limit`;
			missingStrings.push(missingString);
		}
		else
		{
			let metString = `time <i class="icon-timer icon--fa icon--fw"></i>${timeTakenS.toFixed(2)} of ${timeLimitS} ${RL.u.wordPluralizeSuffix('second', timeLimitS)}`;
			metStrings.push(metString);
		}

		return {
			met: met,
			num: num,
			numComplete: numComplete,
			strings: strings,
			metStrings: metStrings,
			missingStrings: missingStrings,
			timeLimitFailure: timeLimitFailure
		};
	}

	checkLedgersRequirements ( requirements, type, typeId )
	{
		// Assume all requirements are met, find where they do not
		let met = true;
		let num = 0;
		let numComplete = 0;
		let strings = [];
		let metStrings = [];
		let missingStrings = [];

		// If an array not passed, make it one
		if ( ! Array.isArray(requirements) )
		{
			requirements = [requirements];
		}

		let allMet = true;

		// For each ledger
		for ( let ledger of requirements )
		{
			// Track current requirment met
			let currentMet = true;

			// Use ledger type for responses
			let checkType = ledger.type ?? 'match';

			// Minimum ledger time
			let minTime = 0;
			// If challenge
			if ( type == 'challenge' )
			{
				// Get record reference
				let challenge = ( Reflect.has(this.challenge.challenges, typeId) ) ? this.challenge.challenges[typeId] : undefined;
				// Set min time to challenge start time
				minTime = ( challenge ) ? challenge.activateTime : 0;
			}

			// Limit ledger times to the challenge
			if ( ! ledger.ignoreTime )
			{
				ledger.minTime = minTime;
				ledger.maxTime = Date.now();
			}
			
			// Create requirement text description
			let requiredString = 'certain actions';

			if ( checkType == 'count' )
			{
				requiredString = 'certain actions in a specific amount';
			}
			else if ( checkType == 'order' )
			{
				requiredString = 'certain actions in order';
			}

			strings.push(requiredString);

			// If ledger not matched, stop
			let ledgerResult = this.ledger.check(checkType, ledger);
			if ( ledgerResult && checkType == 'order' )
			{
				ledgerResult = ledgerResult.seriesMatch;
			}
			else if ( checkType == 'count' )
			{
				ledgerResult = ( Reflect.has(ledger, 'count') && ledger.count == ledgerResult ) ? true : false;
			}
			if ( ! ledgerResult )
			{
				met = false;
				allMet = false;
				currentMet = false;
				//break;
			}

			// Increase the amount of requirements
			num++;

			// If current requirement not met, create description
			if ( ! currentMet )
			{
				let missingString = 'actions did not match';

				if ( checkType == 'count' )
				{
					missingString = 'actions not in a specific amount';
				}
				else if ( checkType == 'order' )
				{
					missingString = 'actions were not in order';
				}

				missingStrings.push(missingString);
			}
			else
			{
				// If requirement met
				numComplete++;

				let metString = 'actions matched';

				if ( checkType == 'count' )
				{
					metString = 'actions in a specific amount';
				}
				else if ( checkType == 'order' )
				{
					metString = 'actions in order';
				}

				metStrings.push(metString);
			}

			// If requirement met
			if ( allMet )
			{
			}
		}

		return {
			met: met,
			num: num,
			numComplete: numComplete,
			strings: strings,
			metStrings: metStrings,
			missingStrings: missingStrings
		};
	}

	checkMatchOfRequirements ( key, requirements )
	{
		// Assume all requirements are met, find where they do not
		let met = false;
		let num = 0;
		let numComplete = 0;
		let strings = [];
		let metStrings = [];
		let missingStrings = [];

		// Get all names
		let requiredNames = [];
		let matchNames = [];
		let keySuffix = 'MatchOf';
		let numMatchesTarget = key.slice(0, - keySuffix.length) * 1; // Convert to number
		let matchPoolCount = 0;

		// Increase the amount of requirements
		num += numMatchesTarget;

		// For each match key
		let requirementKeys = Object.keys(requirements);
		let requirementKey;
		for ( requirementKey of requirementKeys )
		{
			// Track current requirment met
			let currentMet = false;
			let numMatches = 0;

			// If matching items
			if ( requirementKey == 'items' )
			{
				// For each item
				for ( let index in requirements.items )
				{
					matchPoolCount++;

					// Assume string passed
					let itemId = requirements.items[index];
					let requiredItem;

					// If object passed
					if ( typeof(itemId) == 'object' )
					{
						requiredItem = itemId;
						itemId = Object.keys(requiredItem)[0];
						requiredItem = requiredItem[itemId];
					}

					// Get item reference and required name
					let dbItem = this.database.items[itemId];
					if ( dbItem )
					{
						let name = dbItem.name;
						// Add stock requirements into the name if necessary
						if ( requiredItem && Reflect.has(requiredItem, 'stock') )
						{
							name = `${requiredItem.stock} ${name}`;
						}

						requiredNames.push(name);
					}

					// If item is in inventory, match
					if ( Reflect.has(this.item.inventory, itemId) )
					{
						let inventoryItem = this.item.inventory[itemId];

						if ( 
							// If no more item details
							! requiredItem || 
							// Or stock check passes
							(
								Reflect.has(requiredItem, 'stock') && inventoryItem.stock >= requiredItem.stock
							)
						)
						{
							numMatches++;
							matchNames.push(dbItem.name);
						}
					}

					// If requirement met, stop
					if ( numMatches == numMatchesTarget )
					{
						met = true;
						currentMet = true;
					}
				}
			}

			// If requirement met
			if ( currentMet )
			{
				met = true;
				currentMet = true;
				numComplete += numMatchesTarget;
			}
			else
			{
				numComplete += numMatches;
			}

			break;
		}

		// Create requirement text description
		let requiredNamesText = RL.u.createConjunctionSentence(requiredNames, '', '', 'or');
		//let requiredString = `${numMatchesTarget} of ${matchPoolCount} ${requirementKey}`;
		let requiredString = `${numMatchesTarget} of either ${requiredNamesText}`;
		strings.push(requiredString);
		
		// If current requirement not met, create description
		if ( ! met )
		{
			let missingString = `${numMatchesTarget - numComplete} of ${matchPoolCount} possible ${requirementKey} ${RL.u.numberPluralizeVerb(numMatchesTarget - numComplete, 'is')} missing`;
			missingStrings.push(missingString);
		}
		else
		{
			let metString = `have ${numComplete} of ${matchPoolCount} possible ${requirementKey}`;
			metStrings.push(metString);
		}

		return {
			met: met,
			num: num,
			numComplete: numComplete,
			strings: strings,
			metStrings: metStrings,
			missingStrings: missingStrings
		};
	}

	// TODO: Better organize and document all code. In particular, look at what makes sense in game vs a game utilities class.

	// Go through options for setting until one is found or default
	getSetting ( setting, params, componentObj, component )
	{
		let paramsSetting = ( params ) ? params[setting] : undefined;
		let componentObjSetting = ( componentObj ) ? componentObj[setting] : undefined;
		let dbComponentSetting = ( this.database.game.params && this.database.game.params[component] ) ? this.database.game.params[component][setting] : undefined;
		let dbSetting = ( this.database.game.params ) ? this.database.game.params[setting] : undefined;

		//console.log(`setting ${setting}`, paramsSetting, componentObjSetting, dbComponentSetting, dbSetting);
		return paramsSetting ?? componentObjSetting ?? dbComponentSetting ?? dbSetting;
	}

	// Go through options for setting until one is found or default
	getLexicon ( setting, params, componentObj, component )
	{
		let paramsSetting = ( params ) ? params[setting] : undefined;
		let componentObjSetting = ( componentObj ) ? componentObj[setting] : undefined;
		let dbSetting = ( this.database.game.lexicon && this.database.game.lexicon[component] ) ? this.database.game.lexicon[component][setting] : undefined;

		return paramsSetting ?? componentObjSetting ?? dbSetting ?? setting;
	}

	// Go through options for setting until one is found or default
	getVerb ( setting, tense, params, componentObj, component )
	{
		let settingVerb = `${setting}Verb`;
		let verb = this.getLexicon(settingVerb, params, componentObj, component);

		// If verb matches the setting + verb combination still, revert to original setting
		if ( verb === settingVerb )
		{
			verb = setting;
		}
		
		// If needing a tense, use compromise for verb tense handling
		if ( tense )
		{
			//let doc = nlp(verb).verbs();
			//doc[`to${RL.u.capitalize(tense)}Tense`]();
			//console.log(verb, tense, doc);
			//return doc.text();
			
			// https://stackoverflow.com/questions/56623676/how-to-match-a-verb-in-any-tense-in-compromise-js
			// https://observablehq.com/@spencermountain/compromise-match-syntax
			return nlp(verb).verbs().conjugate()[0][`${RL.u.capitalize(tense)}Tense`];
		}

		return verb;
	}

	// this.parseShortcodes('Here is my {{test}} of something {{image:items.1}}.');
	//console.log( this.parseShortcodes('{{modal-avatar:left,database.characters.4.image}}') );
	parseShortcodes ( str )
	{
		const regex = /{{([a-zA-Z0-9\-\.\,\:\;\|]+)}}/gi;
		let matches = str.matchAll(regex);
		// https://www.webtips.dev/webtips/javascript/how-to-convert-a-regexpstringiterator-to-an-array-in-javascript
		// https://medium.com/nerd-for-tech/basics-of-javascript-string-matchall-method-1dd7fdc90e21
		let result = [...matches];

		// If no matches, stop
		if ( ! result.length )
		{
			return str;
		}
		
		// Go through matches and replacements
		for ( let match of result )
		{
			let matchText = match[0];
			let matchTextLength = match[0].length;
			let matchGroup = match[1];
			let startIndex = match.index;
			let stopIndex = startIndex + matchTextLength;

			let matchGroupSplit = matchGroup.split(':');
			let method = matchGroupSplit[0];

			let methodDetails = ( matchGroupSplit.length ) ? matchGroupSplit[1] : '';
			
			// Process methods
			
			// {{gameProp:database.characters.4.image}}
			if ( method == 'gameProp' )
			{
				let prop = this.getGamePropFromStringDotPath(methodDetails);
				
				// If property found, replace
				if ( prop )
				{
					str = str.replace(matchText, prop);
				}
			}
			// {{modal-avatar:left,database.characters.4.image}}
			else if ( method == 'modal-avatar' )
			{
				let methodDetailsSplit = methodDetails.split(',');
				let direction = methodDetailsSplit[0];
				let image = this.getGamePropFromStringDotPath(methodDetailsSplit[1]);

				let avatar = `<div class="modal-avatar--${direction}"><img alt="Avatar" class="modal-avatar__image img-fluid" src="${image}"></div>`;
				str = str.replace(matchText, avatar);
			}

			// https://stackoverflow.com/questions/1431094/how-do-i-replace-a-character-at-a-particular-index-in-javascript
			// https://bobbyhadz.com/blog/javascript-string-replace-character-at-index
			//console.log(matchText, matchGroup, startIndex, stopIndex);
		}
		
		return str;
	}

	getGamePropFromStringDotPath ( path )
	{
		// If no path, stop
		if ( ! path )
		{
			return;
		}

		let pathSplit = path.split('.');

		// Default to property found
		let propFound = true;
		// Start at the database level
		let prop = this;
		// For each level
		for ( let index = 0; index < pathSplit.length; index++ )
		{
			// If property not found, stop
			if ( ! Reflect.has(prop, pathSplit[index]) )
			{
				propFound = false
				break;
			}

			prop = prop[ pathSplit[index] ];
		}

		// If property found, return
		if ( propFound )
		{
			return prop;
		}
	}

	startPrompt ( params )
	{
		// Set default params
		let defaultParams = {
			promptId: ''
			//passedQuestionKeyIndex
			//passedQuestionKeys
			//summary
		}

		// If a string past, adjust parameters
		if ( typeof(params) == 'string' )
		{
			params = {promptId: params};
		}

		// Merge params
		params = Object.assign({}, defaultParams, params);

		// Check if this is the first question
		let isFirstQuestion = ( ! Reflect.has(params, 'passedQuestionKeyIndex') && ! Reflect.has(params, 'passedQuestionKeys') );

		// Get record
		let promptId = params.promptId;
		let prompt = this.database.prompts[promptId];
		if ( ! prompt )
		{
			this.logger.debug(`startPrompt ${promptId} not found`);
			return;
		}

		let questionKeyIndex = params.passedQuestionKeyIndex ?? 0;
		let questionKeys = params.passedQuestionKeys ?? Object.keys(prompt.questions);

		// If this is the first question
		if ( isFirstQuestion )
		{
			// If shuffling questions
			if ( prompt.shuffleQuestions )
			{
				RL.u.shuffleArray(questionKeys);
			}
		}

		let questionKey = questionKeys[questionKeyIndex];
		let question = prompt.questions[questionKey];

		let type;
		if ( params.summary )
		{
			type = 'summary';
		}
		else if ( question.choices )
		{
			type = 'choice';
		}
		else if ( question.correctPool )
		{
			type = 'fill-in';
		}

		// Create or reuse modal
		let modalData;
		let buttonConfirmClass = 'button--confirm';
		let buttonCancelClass = 'button--cancel';
		let buttonChoiceClass = 'button--choice';
		let modalOptionsClass = 'modal-prompt__options';
		let modalFeedbackClass = 'modal-prompt__feedback';
		let modalContinueClass = 'modal-prompt__continue';
		let modalNumProgressClass = 'modal-prompt__numProgress';
		// If this is the first question
		if ( isFirstQuestion )
		{
			modalData = Modal.createFromTemplate();

			// Add at the end
			modalData.$modalFooter.addClass(`justify-content-center`)
			.append(`<div class="${modalOptionsClass} w-100 text-center"></div>`)
			.append(`<div class="${modalFeedbackClass} w-100 text-center"></div>`)
			.append(`<div class="${modalContinueClass} w-100 text-center"></div>`);
		}
		else
		{
			modalData = Modal.getShownTemplate();
		}

		let $modal = modalData.$modal;
		
		$modal.data('promptId', promptId);
		$modal.data('questionKeyIndex', questionKeyIndex);
		$modal.data('questionKeys', questionKeys);
		let answers = params.passedAnswers ?? [];
		$modal.data('answers', answers);
		$modal.data('summary', params.summary ?? false);

		// Set title
		modalData.$modalTitle.html(`${prompt.title ?? RL.u.capitalize(type)}`);

		// If more than 1 question and not a summary, add # of indicator
		$modal.find(`.${modalNumProgressClass}`).remove();
		if ( prompt.type == 'recall' && ! params.recallShown )
		{

		}
		else if ( questionKeys.length > 1 && ! params.summary )
		{
			modalData.$modalHeader
			.append(`<small class="${modalNumProgressClass}">${questionKeyIndex + 1} of ${questionKeys.length}</small>`);
		}

		// Get references
		let $confirmButton = $modal.find(`.${buttonConfirmClass}`).removeClass('d-none');
		let $cancelButton = $modal.find(`.${buttonCancelClass}`).removeClass('d-none');
		let $modalOptions = $modal.find(`.${modalOptionsClass}`).empty().addClass('d-none');
		let $modalFeedback = $modal.find(`.${modalFeedbackClass}`).empty().addClass('d-none');
		let $modalContinue = $modal.find(`.${modalContinueClass}`).empty().addClass('d-none');
		// Reset previous elements
		$modal.find(`.${buttonChoiceClass}`).remove();

		// If recall
		if ( prompt.type == 'recall' )
		{
			// If object to recall not shown, show it first before asking questions
			if ( ! params.recallShown )
			{
				modalData.$modalBody.html(`<div>${this.parseShortcodes(prompt.instructions)}</div>`);

				// If image
				if ( prompt.image )
				{
					modalData.$modalBody.html( modalData.$modalBody.html() + `<img class="img-fluid" src="${this.parseShortcodes(prompt.image)}">`);
				}

				$modalContinue.removeClass('d-none');
				type = '';
			}
		}

		// If choice question
		if ( type == 'choice' )
		{
			modalData.$modalBody.html(question.text);

			let choiceKeys = Object.keys(question.choices);

			// If shuffling choices
			if ( prompt.shuffleChoices )
			{
				RL.u.shuffleArray(choiceKeys);
			}

			// For each choice, create the option
			for ( let choiceKey of choiceKeys )
			{
				// Get record
				let choice = question.choices[choiceKey];

				$confirmButton
				.clone()
				.appendTo($modalOptions)
				.removeClass(buttonConfirmClass)
				.addClass(`${buttonChoiceClass} me-2 mb-2`)
				.html(choice.text)
				.data('choiceKey', choiceKey)
				.data('correct', choice.correct ?? false)
				.click(function ( event ) {
					const targetElement = event.currentTarget;
					// Get data from element
					let $this = $(targetElement);
					// Set correct or incorrect
					let correct = $this.data('correct');
					
					// Set modal data for use after closed
					answers.push(correct);
					$modal.data('answers', answers);
					$modal.data('choiceKey', $this.data('choiceKey'));

					$this.removeClass('.btn-primary')
					.addClass(correct ? 'btn-success' : 'btn-danger');

					// If not correct, reveal answer
					if ( ! correct )
					{
						$this.siblings(`.${buttonChoiceClass}`)
						.each((index, el) => {
							let $this = $(el);
							let correct = $this.data('correct');
							$this.removeClass('.btn-primary')
							.addClass(correct ? 'btn-success' : 'btn-danger');
						});
					}

					// Disable all buttons
					$this.add( $this.siblings() ).attr('disabled', true);

					// Default to failure
					let message = prompt.failure.message;
					// If correct
					if ( correct )
					{
						message = prompt.success.message;
					}

					// Show message
					$modalFeedback.html(message).removeClass('d-none');
					$modalContinue.removeClass('d-none');
				});
			}

			$modalOptions.removeClass('d-none');
		}
		// Else if fill-in question
		else if ( type == 'fill-in' )
		{
			let fillinInputId = 'fill-in-input';
			let fillinButtonId = 'fill-in-button';

			modalData.$modalBody.html(question.text);
			$modalOptions.append(`
				<div class="input-group">
					<input type="text" class="form-control" id="${fillinInputId}" placeholder="Enter your answer" aria-label="Fill in answer" aria-describedby="${fillinButtonId}">
					<button class="btn btn-primary" type="button" id="${fillinButtonId}">Submit</button>
				</div>
			`).removeClass('d-none');

			let $fillInInput = $modalOptions.find(`#${fillinInputId}`);
			let $fillInButton = $modalOptions.find(`#${fillinButtonId}`);
			// On enter key, trigger click
			$fillInInput.on('keydown', event => {
				if ( event.keyCode == 13 )
				{
					event.preventDefault();
					$fillInButton.trigger('click');
				}
			});

			// On click, check answer and give feedback
			$fillInButton.on('click', event => {
				// Get input value
				let $input = $(event.currentTarget).prev('input');
				let value = $input.val();
				// Remove all non-alphanumeric values
				let sanitizedValue = RL.u.removeArticles(value.toLowerCase()).replace(/[^a-z0-9]/gi, '');
				let sanitizedPool = question.correctPool.map(val => RL.u.removeArticles(val.toLowerCase()).replace(/[^a-z0-9]/gi, ''));
				
				// When submitted, check text against pool, show correct/incorrect
				let correct = ( sanitizedPool.indexOf(sanitizedValue) !== -1 );

				// Disable input
				$input.parent().children().attr('disabled', true);
				
				// Set modal data for use after closed
				answers.push(correct);
				$modal.data('answers', answers);
				$modal.data('choiceKey', sanitizedValue);

				
				$fillInButton.removeClass('.btn-primary').addClass(correct ? 'btn-success' : 'btn-danger');
				
				// Default to failure
				let message = `${prompt.failure.message}
				<div class="mb-3">Expected answers like ${RL.u.createConjunctionSentence(question.correctPool, '', '', 'or')}</div>`;
				// If correct
				if ( correct )
				{
					message = prompt.success.message;
				}

				// Show message
				$modalFeedback.html(message).removeClass('d-none');
				$modalContinue.removeClass('d-none');
			});
		}
		// Else if summary
		else if ( type == 'summary' )
		{
			let questionKeyIndex = $modal.data('questionKeyIndex');
			let questionKeys = $modal.data('questionKeys');
			let summary = $modal.data('summary');
			
			// Check if all questions answered
			let complete = ( questionKeyIndex == questionKeys.length - 1 );
			let answers = $modal.data('answers');
			let numCorrect = 0;
			// For each answer, gather info
			for ( let answer of answers )
			{
				if ( answer )
				{
					numCorrect++;
				}
			}

			let correctPercent = (numCorrect / questionKeys.length) * 100;
			let successPercent = prompt.success.passingPercent ?? 100;

			// If all correct
			let correct = ( correctPercent >= successPercent );
			
			modalData.$modalBody.addClass('text-center')
			.html(`<h6>${correct ? 'Congratulations!' : 'Try again!'}</h6>
			${correctPercent.toFixed(2)}% correct and you needed ${successPercent.toFixed(2)}%`);

			$modalContinue.removeClass('d-none');
		}

		// Create continue button to close modal
		$confirmButton
		.clone()
		//.appendTo( modalData.$modalFooter )
		.appendTo($modalContinue)
		.removeClass(`${buttonConfirmClass} btn-primary d-none`)
		.addClass(`${buttonChoiceClass} btn-outline-secondary btn-sm`)
		.html(`Continue`)
		.click(event => {
			let questionKeyIndex = $modal.data('questionKeyIndex');
			let questionKeys = $modal.data('questionKeys');
			let summary = $modal.data('summary');
			
			// Check if all questions answered
			let complete = ( questionKeyIndex == questionKeys.length - 1 );
			let answers = $modal.data('answers');
			let numCorrect = 0;
			// For each answer, gather info
			for ( let answer of answers )
			{
				if ( answer )
				{
					numCorrect++;
				}
			}

			let correctPercent = (numCorrect / questionKeys.length) * 100;
			let successPercent = prompt.success.passingPercent ?? 100;

			// If all correct
			let correct = ( correctPercent >= successPercent );

			// If not complete, show next question
			if ( ! complete )
			{
				let nextQuestionKeyIndex = questionKeyIndex + 1;
				if ( prompt.type == 'recall' && ! params.recallShown )
				{
					nextQuestionKeyIndex = 0;
				}

				this.startPrompt({
					promptId: promptId, 
					passedQuestionKeyIndex: nextQuestionKeyIndex, 
					passedQuestionKeys: questionKeys,
					passedAnswers: answers,
					recallShown: true
				});
				/*
				this.queueAction({type: 'prompt', params: {
					promptId: promptId, 
					passedQuestionKeyIndex: questionKeyIndex + 1, 
					passedQuestionKeys: questionKeys,
					passedAnswers: answers
				}}, true);
				*/
			}
			// If complete, not a summary, and if more than one question, then show summary
			else if ( complete && ! summary && questionKeys.length > 1 )
			{
				this.startPrompt({
					promptId: promptId, 
					passedQuestionKeyIndex: questionKeyIndex, 
					passedQuestionKeys: questionKeys,
					passedAnswers: answers,
					recallShown: true,
					summary: true
				});
				/*
				this.queueAction({type: 'prompt', params: {
					promptId: promptId, 
					passedQuestionKeyIndex: questionKeyIndex, 
					passedQuestionKeys: questionKeys,
					passedAnswers: answers,
					summary: true
				}}, true);
				*/

				// If correct
				if ( correct )
				{
					// Add to ledger
					this.ledger.add({
						type: 'prompt',
						action: type,
						id: promptId,
						choice: $modal.data('choiceKey')
					});

					this.challenge.updateActiveProgress();
				}
			}
			// Else end the prompt and process any final payloads
			else
			{
				// If there is a success/failure payload, process at the end
				//let prompt = this.database.prompts[promptId];
				let payload = ( correct ) ? prompt.success.payload : prompt.failure.payload;
				if ( payload )
				{
					this.queueAction({type: 'payload', params: payload});
				}
	
				// If there is an overall payload after the prompt, process at the end
				if ( prompt.nextPayload )
				{
					this.queueAction({type: 'payload', params: prompt.nextPayload});
				}
	
				// Hide modal
				Modal.hide($modal);

				// Continue with any queued actions
				//this.pollActionQueue();
			}
		});
		
		// Hide templates
		$confirmButton.addClass('d-none');
		$cancelButton.addClass('d-none');

		// Show modal or update it
		if ( isFirstQuestion )
		{
			// Limit closing modal to only given options
			// Remove close button
			$modal.find('.btn-close').addClass('d-none');
			// Don't allow backdrop or keyboard to close modal
			Modal.show($modal, {backdrop: 'static', keyboard: false});
		}
		else
		{
			// Update Bootstrap
			const bsModal = bootstrap.Modal.getInstance(document.querySelector('.modal.show'));
			bsModal.handleUpdate();
		}
	}

	showcaseComponent ( params, poll = true, bypassQueue = false )
	{
		this.queueAction({type: 'callback', callback: () => {
				// Create modal
				let modalData = Modal.createFromTemplate(component__modalShowcaseTemplate);
				// Reset any styles in the template
				modalData.$modal.addClass('fade').attr('style', '');

				modalData.$modalTitle.html(`<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>${this.parseShortcodes(params.title)}`);
				let $componentTitle = modalData.$modal.find('.modal-showcase__component-title');
				$componentTitle.html(params.name);

				let $imageContainer = modalData.$modal.find('.modal-showcase__image-container');
				if ( params.image )
				{
					$imageContainer.find('img').attr('src', this.parseShortcodes(params.image));
				}
				else
				{
					$imageContainer.addClass('d-none');
				}
				
				let $description = modalData.$modal.find('.modal-showcase__description');
				$description.html(this.parseShortcodes(params.description ?? ''));
				
				// Update CTAs
				const $ctaContainer = modalData.$modalFooter.find('.modal-footer__cta-container');
				$ctaContainer
				//.removeClass('d-none')
				.find('a, [type="button"]').attr({
					'data-component-key': params.componentKey,
					'data-component-type': params.componentType
				});
				
				// Update CTA language and hide
				let ctas = ['add', 'activate', 'deactivate', 'use', 'restart', 'hint', 'info'];
				let ctaClassRef = {};
				for ( let cta of ctas )
				{
					let ctaClass = `.cta--component-${cta}`;
					ctaClassRef[cta] = ctaClass;
					const $cta = $ctaContainer.find(ctaClass);

					if ( ! $cta.length )
					{
						continue;
					}

					let verb = RL.u.capitalize(this.getVerb(cta, '', '', params.component, params.componentType));

					$cta.html(verb).removeClass('d-none');

					if ( params.componentType == 'challenge' )
					{
						$ctaContainer.removeClass('d-none');

						if ( cta == 'activate' )
						{
							$cta.one('click', function(event) {
								event.preventDefault();
								this.ui.nav.showScreen('main', `${params.componentType}s`);
								// Close modal
								$('.modal--showcase').modal('hide');
							}.bind(this));
						}
					}
				}
				
				// Limit closing modal to only given options
				// Remove close button
				//$modal.find('.btn-close').addClass('d-none');
				// Don't allow backdrop or keyboard to close modal
				Modal.show(modalData.$modal);//, {backdrop: 'static', keyboard: false}
			}
		});
	}

	get inst ()
	{
		return Game.instance;
	}

	set inst ( value )
	{
		Game.instance = value;
		return Game.instance;
	}
	
	static get inst ()
	{
		return Game.instance;
	}

	static set inst ( value )
	{
		Game.instance = value;
		return Game.instance;
	}
}