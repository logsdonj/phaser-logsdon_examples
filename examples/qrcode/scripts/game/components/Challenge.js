/**
 * 
 */
class Challenge
{
	// Properties

	game;
	events;
	eventNames = {
		add: 'addChallenge',
		update: 'updateChallenge',
		progress: 'progressChallenge',
		complete: 'completeChallenge',
		activate: 'activateChallenge',
		deactivate: 'deactivateChallenge',
		reset: 'resetChallenge',
		hint: 'hintChallenge',
		updateHud: 'updateChallengeHud'
	};

	challenges;

	completedChallenges;
	completedChallengesExample = {
		1: {
			count: 1,
			status: 'statusObj',
			completedTimes: ['timeMs'],
			elapsedTimes: ['timeMs']
		}
	};
	activeChallenges;
	activeChallengesExample = {
		1: {
			order: 1,
			challenge: 'challengeObj',
			status: 'statusObj',
			startTime: 'timeMs',
			stopTime: 'timeMs'
		}
	};
	availableChallenges;

	ui;
	
	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.events = new EventHandler(this.eventNames);
		this.ui = new ChallengeUi(this.game);
		this.reset();
    }
	
	/**
	 * 
	 * @returns void
	 */
	reset ()
	{
		this.challenges = {};
		this.completedChallenges = {};
		this.activeChallenges = {};
		this.availableChallenges = {};
		// Trigger event
		this.events.trigger(this.events.names.reset);
	}

	/**
	 * 
	 * @returns void
	 */
	add ( challengeId, params )
	{
		// Stop if there is no challenge reference
		if ( ! Reflect.has(this.game.database.challenges, challengeId) )
		{
			this.game.logger.debug(`The challenge does not exist.`);
			return;
		}

		let dbChallenge = this.game.database.challenges[challengeId];

		// If we already have this challenge, stop
		if ( Reflect.has(this.challenges, challengeId) )
		{
			this.game.logger.debug(`The challenge has already been added.`);
			let code = `CHALLENGE_ALREADY_ADDED`;
			let message = `The challenge has already been added.`;
			
			// Trigger event
			this.game.events.trigger(this.game.events.names.error, code, message, {
				params: params,
				challenge: dbChallenge
			});
			
			return;
		}

		// If record is new
		// this.game.ledger.check('count', {entry: {type: 'challenge', action: 'add', id: challengeId}}) === 1
		let newRecord = ! ( this.game.ledger.check('match', {
			entry:{
				type: 'challenge', 
				id: challengeId, 
				action: 'add'
			}, 
			limitCheckToLength: true
		}) );

		// Copy the challenge for reference
		let challenge = structuredClone(dbChallenge);
		challenge.addTime = Date.now();
		challenge.attempts = 0;
		// Add the challenge
		this.challenges[challengeId] = challenge;

		// Add to ledger
		this.game.ledger.add({
			type: 'challenge',
			action: 'add',
			id: challengeId,
			time: challenge.addTime
		});

		// Trigger event
		this.events.trigger(this.events.names.add, challengeId, challenge, newRecord, params);
		
		// If there is a payload on first add or after
		if ( newRecord && challenge.payloadOnFirstAdd )
		{
			this.game.queueAction({type: 'payload', params: challenge.payloadOnFirstAdd}, true);
		}
		else if ( challenge.payload )
		{
			this.game.queueAction({type: 'payload', params: challenge.payload}, true);
		}

		// If activate on add
		if ( challenge.activateOnAdd )
		{
			this.activate(challengeId);
		}
	}

	/**
	 * 
	 * @returns void
	 */
	activate ( challengeId, params )
	{
		// Check if challenge already active, stop if yes
		if ( Reflect.has(this.activeChallenges, challengeId) )
		{
			this.game.logger.debug(`Challenge already active.`);
			return;
		}
	
		// Check if the challenge reference exists, stop if no
		if ( ! Reflect.has(this.game.database.challenges, challengeId) )
		{
			this.game.logger.debug(`The challenge does not exist.`);
			return;
		}
		
		// If challenge prerequisites not met, stop
		let prerequisiteData = this.checkPrerequisites(challengeId, this.game.player);
		if ( ! prerequisiteData.met )
		{
			this.game.logger.debug(`The challenge prerequisites are not met.`);
			return;
		}

		// Check for active challenge limit
		const activeChallengeLimit = this.game.getSetting('activeChallengeLimit', '', '', 'game') || 0;
		// If there is a limit to the number of active challenges
		if ( activeChallengeLimit > 0 )
		{
			// Iterate over active challenges
			const activeChallengeIds = Object.keys(this.activeChallenges);
			for ( let challengeId of activeChallengeIds )
			{
				// If active challenges within range, stop
				if ( activeChallengeIds.length <= activeChallengeLimit )
				{
					break;
				}

				// Deactivate the challenge
				this.deactivate(challengeId, params);
				delete activeChallengeIds[challengeId];
			}
		}
	
		let challenge = this.challenges[challengeId];
		challenge.active = true;
		challenge.activateTime = Date.now();
		challenge.attempts++;
		
		// Add to active challenges
		this.activeChallenges[challengeId] = '';

		// Add to ledger
		this.game.ledger.add({
			type: 'challenge',
			action: 'activate',
			id: challengeId,
			time: challenge.activateTime
		});

		// If a timed challenge
		if ( challenge.requirements?.timeLimit )
		{
			// If no timer exists, create one
			if ( ! challenge.timer )
			{
				let goalTime = challenge.requirements.timeLimit * 1000;
				challenge.timer = new Timer({
					goalTime: goalTime,
					intervalMs: 500,
					delay: false
				});
				challenge.timer.events.on(challenge.timer.events.names.update, function ( timer ) {
					//console.log('tick', challengeId);
					this.updateActiveProgress();
				}.bind(this));
				challenge.timer.events.on(challenge.timer.events.names.complete, function ( timer ) {
					//console.log('complete', challengeId);
				}.bind(this));
				challenge.timer.start();
			}
			// Else reuse timer
			else
			{
				challenge.timer.stop().reset().start();
			}
		}
		
		// Trigger event
		this.events.trigger(this.events.names.activate, challengeId, challenge, params);

		// If there is a payload on activate
		if ( challenge.payloadOnActivate )
		{
			this.game.queueAction({type: 'payload', params: challenge.payloadOnActivate});
		}
	}

	/**
	 * 
	 * @returns void
	 */
	deactivate ( challengeId, params )
	{
		// Check if the challenge reference exists, stop if no
		if ( ! Reflect.has(this.game.database.challenges, challengeId) )
		{
			this.game.logger.debug(`The challenge does not exist.`);
			return;
		}
	
		let challenge = this.challenges[challengeId];
		challenge.active = false;
		challenge.deactivateTime = Date.now();
		challenge.activeMs = challenge.deactivateTime - challenge.activateTime;
		// Stop any timers
		if ( challenge.timer )
		{
			challenge.timer.stop();
		}
		// Remove from active challenges
		delete this.activeChallenges[challengeId];
		
		// Add to ledger
		this.game.ledger.add({
			type: 'challenge',
			action: 'deactivate',
			id: challengeId,
			time: challenge.deactivateTime
		});

		// Trigger event
		this.events.trigger(this.events.names.deactivate, challengeId, challenge, params);
	}
	
	/**
	 * Complete a challenge
	 * 
	 * @param {int} challengeId 
	 * @returns boolean
	 */
	complete ( challengeId, params )
	{
		// Get challenge reference
		let challenge = this.challenges[challengeId];
		challenge.completed = true;
		challenge.completedTime = Date.now();
		// Stop any timers
		if ( challenge.timer )
		{
			challenge.timer.stop();
		}
		
		// Move challenge to completed
		this.completedChallenges[challengeId] = '';
		// Remove from active and available
		delete this.activeChallenges[challengeId];
		delete this.availableChallenges[challengeId];
		
		// Distribute challenge awards
		
		// Check for player stats
		if ( challenge.success?.playerStats )
		{
			let successPlayerStatKeys = Object.keys(challenge.success.playerStats);
			successPlayerStatKeys.forEach(key => {
				// Check if player stat exists
				if ( Reflect.has(this.game.player.stats, key) )
				{
					// Set status object to bypass events for each update
					this.game.player.stats[key] += parseFloat(challenge.success.playerStats[key]);
				}
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats);
		}

		// Trigger event
		this.events.trigger(this.events.names.complete, challengeId, challenge);

		// Check for payload
		if ( challenge.success?.payload )
		{
			this.game.queueAction({type: 'payload', params: challenge.success.payload});
		}

		return true;
	}

	/**
	 * Check if a challenge's prerequisites are met by a player
	 * @param {object | int} challenge
	 * @returns bool
	 */
	restart ( challengeId, params )
	{
		this.deactivate(challengeId, params);
		this.activate(challengeId, params);
	}
	
	// TODO: Better language results
	/**
	 * Check if a challenge's requirements are met by a player
	 * @param {int} challengeId 
	 * @param {string} conditionsType requirements or prerequisites 
	 * @param {object} player 
	 * @param {bool} submitted 
	 * @returns object
	 */
	checkConditions ( challengeId, conditionsType, player, submitted )
	{
		// Check if the challenge reference exists, stop if no
		if ( ! this.game.database.challenges[challengeId] )
		{
			this.game.logger.debug(`The challenge does not exist.`);
			return;
		}

		let defaultData = {
			met: false,
			num: 0,
			numComplete: 0,
			progress: 0,
			progressPercent: 0,
			timeLimitFailure: false,
			timeLimitFailureText: '',
			itemsText: '',
			itemsStrings: [],
			challengesText: '',
			challengesStrings: [],
			locationsText: '',
			locationsStrings: [],
			playerStatsText: '',
			playerStatsStrings: [],
			ledgersStrings: [],
			text: '',
			strings: [],
			missingText: '',
			missingStrings: [],
			metText: '',
			metStrings: []
		};

		// Get references
		let dbChallenge = this.game.database.challenges[challengeId];
		let componentNoun = this.game.getLexicon('challenge', '', dbChallenge, 'challenge');
		let conditions = dbChallenge[conditionsType];

		// If there are no requirements or submit triggers, stop
		if ( ! conditions && ! dbChallenge.submitTriggers && ! dbChallenge.locations )
		{
			let data = Object.assign(defaultData, {
				met: true,
				progress: 1,
				progressPercent: 100,
				text: 'There are no conditions to meet.'
			});
			return data;
		}

		// Assume all requirements are met, find where they do not
		let met = true;
		let num = 0;
		let numComplete = 0;

		let metStrings = [];
		let missingStrings = [];
		let itemsStrings = [];
		let challengesStrings = [];
		let locationsStrings = [];
		let playerStatsStrings = [];
		let ledgersStrings = [];
		let matchOfStrings = [];
		let timeStrings = [];
		let timeLimitFailure = false;

		let requirements = conditions ?? {};

		// Check challenge requirements
		let requirementKeys = Object.keys(requirements);
		requirementKeys.forEach(key => {
			// If checking items, challenges, playerStats, or timeLimit
			if ( ['items', 'challenges', 'locations', 'playerStats', 'timeLimit', 'ledgers'].indexOf(key) !== -1 )
			{
				let requirementsData;

				if ( key == 'items' )
				{
					requirementsData = this.game.checkItemsRequirements(requirements.items);
					itemsStrings = requirementsData.strings;
				}
				// If checking challenges
				else if ( key == 'challenges' )
				{
					requirementsData = this.game.checkChallengesRequirements(requirements.challenges);
					challengesStrings = requirementsData.strings;
				}
				// If checking locations
				else if ( key == 'locations' )
				{
					requirementsData = this.game.checkLocationsRequirements(requirements.locations);
					locationsStrings = requirementsData.strings;
				}
				// If checking player stats
				else if ( key == 'playerStats' )
				{
					requirementsData = this.game.checkPlayerStatsRequirements(requirements.playerStats);
					playerStatsStrings = requirementsData.strings;
				}
				// If checking challenge time in seconds
				else if ( key == 'timeLimit' )
				{
					requirementsData = this.game.checkTimeLimitRequirements(requirements.timeLimit, 'challenge', challengeId);
					timeStrings = requirementsData.strings;
					timeLimitFailure = requirementsData.timeLimitFailure;
				}
				else if ( key == 'ledgers' )
				{
					requirementsData = this.game.checkLedgersRequirements(requirements.ledgers, 'challenge', challengeId);
					ledgersStrings = requirementsData.strings;
				}

				met = ( met ) ? requirementsData.met : met;
				num += requirementsData.num;
				numComplete += requirementsData.numComplete;
				metStrings = metStrings.concat(requirementsData.metStrings);
				missingStrings = missingStrings.concat(requirementsData.missingStrings);
			}
			else if ( key.endsWith('MatchOf') )
			{
				let requirementsData = this.game.checkMatchOfRequirements(key, requirements[key]);
				matchOfStrings = requirementsData.strings;

				met = ( met ) ? requirementsData.met : met;
				num += requirementsData.num;
				numComplete += requirementsData.numComplete;
				metStrings = metStrings.concat(requirementsData.metStrings);
				missingStrings = missingStrings.concat(requirementsData.missingStrings);
			}
		});

		// Check associated locations
		let associatedLocationsData = this.game.checkLocationsAssociation('challenge', challengeId);
		met = ( met ) ? associatedLocationsData.met : met;
		num += associatedLocationsData.num;
		numComplete += associatedLocationsData.numComplete;
		metStrings = metStrings.concat(associatedLocationsData.metStrings);
		missingStrings = missingStrings.concat(associatedLocationsData.missingStrings);
		let associatedLocationsStrings = associatedLocationsData.strings;
		let associatedLocationsText = RL.u.createConjunctionSentence(associatedLocationsStrings, 'You need to ', '.');
		
		// Check submit triggers
		let triggerData = this.game.checkSubmitTriggers(dbChallenge, submitted);
		let submitTriggersStrings = triggerData.strings;
		num += triggerData.num;
		numComplete += triggerData.numComplete;
		metStrings = metStrings.concat(triggerData.metStrings);
		missingStrings = missingStrings.concat(triggerData.missingStrings);

		// Create submit triggers string
		let submitTriggersText = RL.u.createConjunctionSentence(submitTriggersStrings, `You must ', ' to complete the ${componentNoun}.`);
		// Create required player stats string
		let playerStatsText = RL.u.createConjunctionSentence(playerStatsStrings, 'You need ', '.');
		// Create required items string
		let itemsText = RL.u.createConjunctionSentence(itemsStrings, 'You need ', '.');
		// Create required challenges string
		let challengesText = RL.u.createConjunctionSentence(challengesStrings, 'You need to complete ', '.');
		let locationsText = RL.u.createConjunctionSentence(locationsStrings, 'You need to visit ', '.');
		let timeText = timeStrings.join(' ');
		let ledgersText = RL.u.createConjunctionSentence(ledgersStrings, 'You need ', '.');
		let matchOfText = RL.u.createConjunctionSentence(matchOfStrings, 'You need ', '.');
		let missingText = RL.u.createConjunctionSentence(missingStrings, '', '.');
		let metText = RL.u.createConjunctionSentence(metStrings, 'You completed ', '.');
		// If there was a time limit failure, prompt to restart the challenge
		let timeLimitFailureText = '';
		if ( timeLimitFailure )
		{
			timeLimitFailureText = `Because of the time limit, you will need to <a class="cta--component-restart" data-component-type="challenge" data-component-key="${challengeId}" href="#challenges-screen"><i class="icon-reset-time icon--fa icon--fw"></i> restart the ${componentNoun}</a>.`;
			missingText += ' ' + timeLimitFailureText;
		}

		let data = Object.assign(defaultData, {
			met: met,
			num: num,
			numComplete: numComplete,
			progress: ( num > 0 ) ? (numComplete / num) : 0,
			progressPercent: ( num > 0 ) ? ((numComplete / num) * 100).toFixed(2) : 0,
			timeLimitFailure: timeLimitFailure,
			timeLimitFailureText: timeLimitFailureText,
			submitTriggersStrings: submitTriggersStrings,
			submitTriggersText: submitTriggersText,
			itemsText: itemsText,
			itemsStrings: itemsStrings,
			challengesText: challengesText,
			challengesStrings: challengesStrings,
			locationsText: locationsText,
			locationsStrings: locationsStrings,
			locationsText: locationsText,
			associatedLocationsText: associatedLocationsText,
			associatedLocationsStrings: associatedLocationsStrings,
			playerStatsText: playerStatsText,
			playerStatsStrings: playerStatsStrings,
			ledgersText: ledgersText,
			ledgersStrings: ledgersStrings,
			matchOfText: matchOfText,
			matchOfStrings: matchOfStrings,
			timeText: timeText,
			timeStrings: timeStrings,
			text: [itemsText, challengesText, locationsText, associatedLocationsText, playerStatsText, ledgersText, matchOfText, timeText, submitTriggersText].join(' ').trim(),
			strings: [itemsStrings, challengesStrings, locationsStrings, associatedLocationsStrings, playerStatsStrings, timeStrings, ledgersStrings, matchOfStrings, submitTriggersStrings].concat(),
			missingText: missingText,
			missingStrings: missingStrings,
			metText: metText,
			metStrings: metStrings
		});

		// Set default text if no requirements
		if ( data.text == '' )
		{
			data.text = 'There are no requirements to complete.';
		}

		return data;
	}
	
	/**
	 * Check if a challenge's prerequisites are met by a player
	 * @param {int} challengeId 
	 * @param {object} player 
	 * @param {bool} submitted 
	 * @returns object
	 */
	checkPrerequisites ( challengeId, player, submitted )
	{
		return this.checkConditions(challengeId, 'prerequisites', player, submitted);
	}
	
	/**
	 * Check if a challenge's requirements are met by a player
	 * @param {int} challengeId 
	 * @param {object} player 
	 * @param {bool} submitted 
	 * @returns object
	 */
	checkRequirements ( challengeId, player, submitted )
	{
		return this.checkConditions(challengeId, 'requirements', player, submitted);
	}

	/**
	 * Check if an item is in an active challenge's requirements
	 * 
	 * @param {int} itemId 
	 * @returns array
	 */
	isItemForActiveChallenge ( itemId )
	{
		let forChallengeIds = [];

		// For each active challenge
		let activeChallengeIds = Object.keys(this.activeChallenges);
		for ( let activeChallengeId of activeChallengeIds )
		{
			let challenge = this.challenges[activeChallengeId];

			// If challenge has no required items, skip
			if ( ! challenge.requirements || ! Reflect.has(challenge.requirements, 'items') )
			{
				continue;
			}
			
			// Get the required item ids
			let requiredItemIds = Object.keys(challenge.requirements.items);
			// If the item is required, add challenge id
			if ( requiredItemIds.indexOf(itemId) !== -1 )
			{
				forChallengeIds.push(activeChallengeId);
			}
		}

		return forChallengeIds;
	}

	/**
	 * 
	 * @returns void
	 */
	updateActiveProgress ()
	{
		// Trigger event
		this.events.trigger(this.events.names.progress);

		// For each active challenge
		let activeChallengeIds = Object.keys(this.activeChallenges);
		activeChallengeIds.forEach(id => {
			let challenge = this.challenges[id];

			// If no submit trigger, check complete
			if ( ! challenge.submitTriggers )
			{
				this.checkComplete(id);
			}
		});

		// Check game progress
		this.game.checkProgress();
	}

	updateAvailable ()
	{
		// Get challenges for reference
		let availableChallengeIds = Object.keys(this.game.challenge.availableChallenges);
		
		// For each challenge currently in the game
		let challengeIds = Object.keys(this.game.challenge.challenges);
		challengeIds.forEach(id => {
			// If the challenge is available to activate
			if ( this.isAvailableToActivate(id) )
			{
				// If a newly available challenge
				if ( availableChallengeIds.indexOf(id) === -1 )
				{
					this.game.logger.debug(`You have a new challenge.`);
				}

				// Add to available challenges
				this.game.challenge.availableChallenges[id] = '';
			}
			// Else make sure to remove it
			else
			{
				delete this.game.challenge.availableChallenges[id];
			}
		});
	}

	isAvailableToActivate ( challengeId )
	{
		// Get references
		let activeChallengeIds = Object.keys(this.game.challenge.activeChallenges);
		let completedChallengeIds = Object.keys(this.game.challenge.completedChallenges);
		
		// Get challenge reference
		let challenge = this.game.challenge.challenges[challengeId];
		// If not found, stop
		if ( ! challenge )
		{
			return false;
		}
		
		// If challenge has already been completed or if replaying the game
		let isComplete = ( completedChallengeIds.indexOf(challengeId) !== -1 );
		let allowReplay = ( isComplete && this.game.completeCount );

		// Get requirements data for feedback
		let prerequisitesData = this.game.challenge.checkPrerequisites(challengeId, this.game.player);

		// If challenge is active
		if ( activeChallengeIds.indexOf(challengeId) !== -1 )
		{
			return false;
		}
		// If challenge is not available because prerequisites are not met and not replaying
		else if ( ! prerequisitesData.met )
		{
			return false;
		}
		// If complete and no replay
		else if ( isComplete && ! allowReplay )
		{
			return false;
		}

		// Else the challenge is available
		return true;
	}

	/**
	 * Check active challenges for a matching trigger
	 * 
	 * @param {object} passedTrigger 
	 * @returns void
	 */
	checkActiveSubmit ( passedTrigger )
	{
		// Gather challenge ids matching the submit trigger
		let submitChallengeIds = [];

		// For each active challenge
		let activeChallengeIds = Object.keys(this.activeChallenges);
		for ( let activeChallengeId of activeChallengeIds )
		{
			// If triggers match, add to submit
			if ( this.game.isSubmitTrigger('challenge', activeChallengeId, passedTrigger) )
			{
				// Add challenge id to submit
				submitChallengeIds.push(activeChallengeId);
			}
		}

		// If no challenges submitted, stop
		if ( submitChallengeIds.length <= 0 )
		{
			return;
		}

		// For each submitted challenge
		for ( let submitChallengeId of submitChallengeIds )
		{
			this.checkComplete(submitChallengeId);
		}

		// Update progress for other active challenges
		this.updateActiveProgress();
	}

	/**
	 * Check if a challenge is complete
	 * 
	 * @param {int} challengeId 
	 * @returns boolean
	 */
	checkComplete ( challengeId )
	{
		// Check if the challenge has been completed
		let requirementsData = this.checkRequirements(challengeId, this.game.player);
		// If complete, complete it
		if ( requirementsData.met )
		{
			return this.complete(challengeId, this.game.player);
		}
	}

}