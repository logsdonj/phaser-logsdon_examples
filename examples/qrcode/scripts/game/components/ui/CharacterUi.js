/**
 * 
 */
class CharacterUi
{
	// Properties

	className = 'CharacterUi';
	updateHudDebounced;

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.updateHudDebounced = RL.u.debounce(this.updateHud.bind(this), this.game.ui.debounceDelay);
    }

	updateHud ()
	{
		this.game.logger.debug(`${this.className} ${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);
		
		let collectorMode = this.game.getSetting('collectorMode', '', '', 'game');
		let actionMode = this.game.getSetting('actionMode', '', '', 'game');
		let hudMode = this.game.getSetting('hudMode', '', '', 'character');
		let componentType = 'character';
		let activeCharacterNames = [];
		
		// Get character status element reference
		let $charactersStatusContainer = $('.status-container--characters');
		$charactersStatusContainer.addClass('d-none');
		let $charactersStatus = jQuery('.characters-status');
		$charactersStatus.empty();

		// Reset the list of characters
		let $charactersList = jQuery('.characters-list');
		$charactersList.empty();

		// Sort into array of ids
		// If collector, list out all records
		let characterIds = RL.u.sortObjectBy(this.game[collectorMode ? 'database' : 'character'].characters, ['type', 'name'], true);

		// For each character of the game
		//let characterIds = Object.keys(this.game.character.characters);
		characterIds.forEach(characterId => {
			// List out character information
			let isAdded = Reflect.has(this.game.character.characters, characterId);

			let isCombination = false;
			if ( this.game.database?.combinations )
			{
				const combinationKeys = Object.keys(this.game.database.combinations);
				for ( const combinationKey of combinationKeys )
				{
					const combination = this.game.database.combinations[combinationKey];
					if ( combination?.payload?.characters && Reflect.has(combination.payload.characters, characterId) )
					{
						isCombination = true;
						break;
					}
				}
			}

			// Start template
			let $template = $(component__listGroupItemTemplate)
			.appendTo($charactersList)
			.attr('data-character-id', characterId);

			// Hiding all areas
			$template.find('[class*="list-group-item__"]').addClass('d-none');

			// If collector mode and not in inventory, show missing
			if ( collectorMode && ! isAdded )
			{
				// Get record
				let dbCharacter = this.game.database.characters[characterId];

				// Update content
				$template.find('.list-group-item__heading')
				.html('???')//dbCharacter.name
				.addClass('text-muted')
				.removeClass('d-none');

				$template.find('.list-group-item__type')
				.html(`<i class="fa-solid fa-triangle-exclamation fa-fw me-1"></i>Missing`)
				.removeClass('d-none');

				/*
				let firstCol = RL.u.getRandomInt(2, 9);
				let secondCol = 11 - firstCol;
				$template.find('.list-group-item__notes')
				.html(`<span class="placeholder col-${firstCol}"></span>&nbsp;<span class="placeholder col-${secondCol}"></span>`)
				.addClass('placeholder-glow')
				.removeClass('d-none');
				*/
			}
			else
			{
				// Get record
				let character = this.game.character.characters[characterId];
				
				// Update content
				$template.find('.list-group-item__heading')
				.html(character.name)
				.removeClass('d-none');

				$template.find('.list-group-item__type')
				.html(`${RL.u.capitalize(character.type ?? '')}`)
				.removeClass('d-none');

				if ( hudMode == 'minimal' )
				{
					$template.find('.list-group-item__notes')
					.html(character.description)
					.toggleClass('d-none', ! character.description);
				}
				else
				{
					$template.find('.list-group-item__description')
					.html(character.description)
					.toggleClass('d-none', ! character.description);
					
					$template.find('.list-group-item__notes')
					.html(character.notes)
					.toggleClass('d-none', ! character.notes);
				}
				
				// Update CTAs
				$template.find('.list-group-item__cta-container')
				.removeClass('d-none')
				.find('a, [type="button"]').attr({
					'data-character-id': characterId,
					'data-component-key': characterId,
					'data-component-type': 'character'
				});
				
				// Update CTA language and hide
				let ctas = ['add', 'activate', 'deactivate', 'use', 'restart', 'hint', 'info'];
				let ctaClassRef = {};
				for ( let cta of ctas )
				{
					let ctaClass = `.cta--component-${cta}`;
					ctaClassRef[cta] = ctaClass;
					let verb = RL.u.capitalize(this.game.getVerb(cta, '', '', character, componentType));
					$template.find(ctaClass).html(verb).addClass('d-none');
				}

				// If is added, show info
				if ( isAdded )
				{
					// Update CTAs
					$template.find(ctaClassRef['info'])
					.removeClass('d-none');
				}

				// If hints, set them up
				if ( Reflect.has(character, 'hints') )
				{
					let $component__badge_hintAmount = $(component__badge_hintAmount)
					.html(Array.isArray(character.hints) ? character.hints.length : 1);
					
					// Update CTAs
					$template.find(ctaClassRef['hint'])
					.append($component__badge_hintAmount)
					.attr('data-bypass-queue', 'true')
					.removeClass('d-none');
				}
				
				// If is added, show info
				if ( isAdded )
				{
					// Update CTAs
					$template.find(ctaClassRef['info'])
					.removeClass('d-none');
				}

				// If character is active
				if ( character.active )
				{
					// Show active
					$template.addClass('active').attr('aria-current', true);
					// Update CTAs
					$template.find([ctaClassRef['use'], ctaClassRef['deactivate']].join(',')).removeClass('d-none');
					// Track active character names
					activeCharacterNames.push(character.name);
				}
				else
				{
					// Update CTAs
					$template.find(ctaClassRef['activate']).removeClass('d-none');
				}
			}
			
			// Add combinable icon to type
			if ( isCombination )
			{
				$template.find('.list-group-item__type')
				.prepend(`<i class="fa-solid fa-screwdriver-wrench fa-fw me-1"></i>`);
			}
		});

		// If no characters, note it
		if ( ! characterIds.length )
		{
			let componentNoun = this.game.getLexicon('character', '', '', 'character');
			let $template = $(component__listGroupItemTemplate).appendTo($charactersList)
			.html(`No ${RL.u.wordPluralizeSuffix(componentNoun, 0)} are available.`);
		}

		// If active characters, note it
		if ( activeCharacterNames.length && $charactersStatus.length )
		{
			let isAre = RL.u.numberPluralizeVerb(activeCharacterNames, 'is');
			let sentence = RL.u.createConjunctionSentence(activeCharacterNames, '', ` ${isAre} available.`);
			$charactersStatus.html(sentence).removeClass('d-none');
		}
		
		// If there is a status, show the container
		if ( $charactersStatus.length && $charactersStatus.html().length )
		{
			$charactersStatusContainer.removeClass('d-none');
		}

		// Update game icon stat
		$('.stat--num-available-characters').html(characterIds.length);
		
		// Trigger event
		this.game.character.events.trigger(this.game.character.events.names.updateHud);
	}

	// Event handlers
	
	handleAdd ( characterId, character, newRecord, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${character.name} added!`);
		let notifyAdd = this.game.getSetting('notifyAdd', params, character, 'character');
		this.game.character.ui.updateHudDebounced();

		// If the first time this item is found, show info
		if ( newRecord )
		{
			let showcase = this.game.getSetting('showcase', params, character, 'character');
			
			let componentNoun = this.game.getLexicon('character', params, character, 'character');
			let addVerb = this.game.getVerb('add', 'past', params, character, 'character');

			// If showcasing
			if ( showcase )
			{
				this.game.showcaseComponent({
					title: `New ${RL.u.capitalize(componentNoun)} ${RL.u.capitalize(addVerb)}`, 
					name: character.name,
					image: character.image,
					description: character.description,
					component: character,
					componentKey: characterId,
					componentType: 'character'
				}, true, params?.bypassQueue);

				/*
				this.game.queueAction({type: 'modal', params: {
					title: `<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>New ${RL.u.capitalize(componentNoun)}!`, 
					body: `${character.name} ${addVerb}!`,
					cancelButtonHide: true,
					confirmButtonLabel: 'Continue'
				}}, true);
				*/
			}
			else if ( notifyAdd )
			{
				let activateVerb = RL.u.capitalize(this.game.getVerb('activate', '', params, character, 'character'));
				let useVerb = RL.u.capitalize(this.game.getVerb('use', '', params, character, 'character'));
		
				this.game.queueAction({type: 'toast', params: {
					title: `<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>New ${componentNoun}`,
					body: `${character.name} ${addVerb}!`,
					ctas: {
						primary: {
							attrObj: {
								'data-component-type': 'character',
								'data-component-key': characterId
							},
							classAddon: ( character.activateOnAdd ) ? 'cta--component-use' : 'cta--component-activate',
							label: ( character.activateOnAdd ) ? useVerb : activateVerb
						}
					},
					type: 'toastTemplate'
				}}, true, params?.bypassQueue);
			}
		}
		// If notify add, do it
		else if ( notifyAdd )
		{
			let findVerb = this.game.getVerb('find', 'past', params, character, 'character');

			this.game.queueAction({type: 'toast', params: {
				body: `${character.name} ${findVerb}.`,
				color: 'success',
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}
	}

	handleActivate ( characterId, character, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${character.name} activated!`);

		let notifyActivate = this.game.getSetting('notifyActivate', params, character, 'character');
		if ( notifyActivate )
		{
			let activateVerb = this.game.getVerb('activate', 'past', params, character, 'character');
		
			this.game.queueAction({type: 'toast', params: {
				body: `${character.name} ${activateVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		this.game.ui.updateHudsDebounced();
	}

	handleDeactivate ( characterId, character, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${character.name} deactivated!`);

		let notifyDeactivate = this.game.getSetting('notifyDeactivate', params, character, 'character');
		if ( notifyDeactivate )
		{
			let deactivateVerb = this.game.getVerb('deactivate', 'past', params, character, 'character');
		
			this.game.queueAction({type: 'toast', params: {
				body: `${location.name} ${deactivateVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		this.game.ui.updateHudsDebounced();
	}

	handleUse ( characterId, character, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${character.name} used!`);

		let notifyUse = this.game.getSetting('notifyUse', params, character, 'character');
		if ( notifyUse )
		{
			let useVerb = this.game.getVerb('use', 'past', params, character, 'character');
		
			this.game.queueAction({type: 'toast', params: {
				body: `${character.name} ${useVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		this.game.queueAction({type: 'interaction', params: {
			type: 'character',
			id: characterId
		}}, true, params?.bypassQueue);
		//this.game.character.showInteraction('character', characterId);

		this.game.character.ui.updateHudDebounced();
	}

}