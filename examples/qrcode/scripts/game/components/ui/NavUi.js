/**
 * 
 */
class NavUi
{
	// Properties

	className = 'NavUi';

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
    }

	showScreen ( screen, prevScreen, targetCallback, prevCallback, bypassBrowserHistory )
	{
		this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: ${screen}${prevScreen ? `, ${prevScreen}` : ''}`);

		// If not bypassing the browser history entry to prevent completely backing out of app
		if ( ! bypassBrowserHistory )
		{
			// Listen for popstate and check state
			history.pushState({showScreen: {screen: screen, prevScreen: prevScreen}}, '', `#showScreen:${screen}${prevScreen ? `,${prevScreen}` : ''}`);
		}
	
		let gameScreenClass = 'game-screen';
		let gameScreenActiveClass = 'active';
		let gameScreenHiddenClass = 'd-none';
		let animateShowUp = 'fadeInUp';
		let animateHideDown = 'fadeOutDown';
		let animateShowFade = 'fadeIn';
		let animateHideFade = 'fadeOut';
	
		let targetAnimateShow = animateShowFade;
		let targetAnimateHide = animateHideFade;
		let prevAnimateShow = animateShowFade;
		let prevAnimateHide = animateHideFade;
	
		let $targetScreen = $(`#${screen}-screen`);
		
		let curScreen;
		let $curScreen = $(`.${gameScreenClass}.${gameScreenActiveClass}`);//( $(event.target).is('.game-screen') ) ? $(event.target) : $(this).parentsUntil('.game-screen').parent().eq(0);
		if ( $curScreen.length )
		{
			curScreen = $curScreen.attr('id').replace('-screen', '');
		}
	
		// If the previous screen wasn't passed, attempt setting it
		if ( ! prevScreen )
		{
			// If current active screen exists
			let $prevScreen = $(`.${gameScreenClass}.${gameScreenActiveClass}`);//$(`.game-screen:not(.${gameScreenHiddenClass})`);
			if ( $prevScreen.length )
			{
				prevScreen = $prevScreen.attr('id').replace('-screen', '');
			}
		}
	
		let $prevScreen = $(`#${prevScreen}-screen`);
	
		let mainScreens = ['instructions', 'settings'];
		let nestedMainScreen = ( mainScreens.indexOf(screen) !== -1 );
		let nestedMainPrevScreen = ( mainScreens.indexOf(prevScreen) !== -1 );
		let hudScreens = ['challenges', 'items', 'locations', 'characters'];
		let hudScreen = ( hudScreens.indexOf(screen) !== -1 );
		let nestedScreens = mainScreens.concat(hudScreens);
		let nestedScreen = ( nestedScreens.indexOf(screen) !== -1 );
		let nestedPrevScreen = ( nestedScreens.indexOf(prevScreen) !== -1 );
	
		// Check if screens should be refreshed
		if ( nestedScreen )
		{
			targetAnimateShow = animateShowUp;
			targetAnimateHide = animateHideDown;
	
			// If showing a HUD screen, update it first
			if ( hudScreen )
			{
				// https://www.educative.io/answers/eval-vs-function-in-javascript
				//let updateHudResult = Function(`return update${RL.u.capitalize(screen)}HudDebounced()`)();
			}
		}
		
		if ( nestedPrevScreen )
		{
			prevAnimateShow = animateShowUp;
			prevAnimateHide = animateHideDown;
		}
	
		let showTarget = true;
		let hidePrev = true;
		let hideCurrent = false;
		// If the same screen, toggle
		if ( screen == prevScreen )
		{
			// If same is open, close previous and don't open new
			// If same is closed, don't close previous and open new
			hidePrev = $targetScreen.is(`.${gameScreenActiveClass}`);
			showTarget = ! hidePrev;
		}
		
		// If the target screen doesn't exist
		if ( ! $targetScreen.length )
		{
			showTarget = false;
		}
		
		// If the previous and current screen don't match, hide it
		if ( $curScreen.length && curScreen != prevScreen )
		{
			hideCurrent = true;
		}
	
		// If the previous screen doesn't exist
		if ( ! $prevScreen.length )
		{
			hidePrev = false;
		}
	
		// Hide all game screens, but title and main
		//$(`.game-screen:not(.game-screen--title):not(.game-screen--main):not(.${gameScreenHiddenClass})`)
		//.removeClass(gameScreenHiddenClass).removeClass(animateShowUp).addClass(animateHideDown);//.addClass(gameScreenHiddenClass);//:not(.game-screen--main)
		
		// Reset classes
		if ( hidePrev )
		{
			$prevScreen.removeClass(`${gameScreenHiddenClass} ${gameScreenActiveClass}`);//.addClass(gameScreenHiddenClass);
			// Work around title and main screens not always needing to hide, hide main if previous
			if ( nestedPrevScreen || (screen == 'main' && prevScreen == 'title') )
			{
				// Animate out and hide after
				RL.a.animateCSS($prevScreen[0], prevAnimateHide).then((message) => {
					// Do something after the animation
					$prevScreen.addClass(gameScreenHiddenClass);
	
					if ( typeof(prevCallback) == 'function' )
					{
						prevCallback();
					}
				});
			}
		}
	
		if ( hideCurrent )
		{
			$curScreen.removeClass(`${gameScreenHiddenClass} ${gameScreenActiveClass}`);//.addClass(gameScreenHiddenClass);
			// Animate out and hide after
			RL.a.animateCSS($curScreen[0], prevAnimateHide).then((message) => {
				// Do something after the animation
				$curScreen.addClass(gameScreenHiddenClass);
			});
		}
	
		if ( showTarget )
		{
			// Show the target one and set the previous screen
			$targetScreen.attr('data-prev-screen', prevScreen).removeClass(gameScreenHiddenClass).addClass(gameScreenActiveClass);
	
			// If not the main screen, animate
			if ( nestedScreen || (screen == 'title' && prevScreen == 'main' || screen == 'title' && ! prevScreen) )
			{
				// Animate in
				RL.a.animateCSS($targetScreen[0], targetAnimateShow).then((message) => {
					// Do something after the animation
					if ( typeof(targetCallback) == 'function' )
					{
						targetCallback();
					}
				});
			}
		}
	
		$(`.game-screen__nav-icon[data-screen="${screen}"]`).addClass('active');
		$(`.game-screen__nav-icon[data-screen="${curScreen}"], .game-screen__nav-icon[data-screen="${prevScreen}"]`).removeClass('active');
	
		return (hidePrev || showTarget);
	}
	
	closeClosestGameScreen ( element )
	{
		// Get the game screen reference
		let gameScreenClass = 'game-screen';
		let $gameScreen = $(element).closest(`.${gameScreenClass}`);
		// Close the game screen
		$gameScreen.find(`.${gameScreenClass}__close`).trigger('click');
	}
	
	// Event handlers

	handleOpenScreenClick ( event )
	{
		this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);
	
		// If not debugging, prevent default behavior
		if ( ! this.game.debug )
		{
			event.preventDefault();
		}
	
		// Set current screen as the previous screen
		let gameScreenClass = 'game-screen';
		let gameScreenActiveClass = 'active';
		let prevScreen;
		let $curScreen = $(`.${gameScreenClass}.${gameScreenActiveClass}`);//( $(event.target).is('.game-screen') ) ? $(event.target) : $(targetElement).parentsUntil('.game-screen').parent().eq(0);
		if ( $curScreen.length )
		{
			prevScreen = $curScreen.attr('id').replace('-screen', '');
		}
	
		const targetElement = event.currentTarget;
		let screen = $(targetElement).data('screen');
		this.game.ui.nav.showScreen(screen, prevScreen);
	}
	
	handleCloseScreenClick ( event )
	{
		this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);
	
		// If not debugging, prevent default behavior
		if ( ! this.game.debug )
		{
			event.preventDefault();
		}
	
		let gameScreenClass = 'game-screen';
		let gameScreenActiveClass = 'active';
		// If the current screen is found, show the previous screen
		let $curScreen = $(`.${gameScreenClass}.${gameScreenActiveClass}`);//( $(event.target).is('.game-screen') ) ? $(event.target) : $(this).parentsUntil('.game-screen').parent().eq(0);
	
		if ( $curScreen.length )
		{
			let prevScreen = $curScreen.attr('id').replace('-screen', '');
			let nextScreen = $curScreen.data('prev-screen');
			this.game.ui.nav.showScreen('', prevScreen);//showScreen(nextScreen, prevScreen);
		}
	}
	
}