/**
 * 
 */
class ComponentInfoUi
{
	// Properties

	className = 'ComponentInfoUi';
	updateModalDebounced;

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.updateModalDebounced = RL.u.debounce(this.updateModal.bind(this), this.game.ui.debounceDelay);
    }

	showModal ( componentType, componentKey )
	{
		this.game.logger.debug(`showModalComponentInfo(${componentType}, ${componentKey})`);
	
		let componentTypePlural = `${componentType}s`;
		// Track keys to help with previous and next navigation
		let recordKeys;
	
		// Get record references
		let dbRecord;
		if ( 
			Reflect.has(this.game.database, componentTypePlural) && 
			Reflect.has(this.game.database[componentTypePlural], componentKey)
		)
		{
			dbRecord = this.game.database[componentTypePlural][componentKey];
			// Sort into array of ids used on screens for previous and next
			recordKeys = RL.u.sortObjectBy(this.game.database[componentTypePlural], ['type', 'name'], true);//Object.keys(this.game.database[componentTypePlural]);
		}
	
		let recordComponentTypePlural = ( componentType == 'item' ) ? 'inventory' : componentTypePlural;
		let record;
		if ( 
			Reflect.has(this.game, componentType) && 
			Reflect.has(this.game[componentType], recordComponentTypePlural) && 
			Reflect.has(this.game[componentType][recordComponentTypePlural], componentKey)
		)
		{
			record = this.game[componentType][recordComponentTypePlural][componentKey];
			// Sort into array of ids used on screens for previous and next
			recordKeys = RL.u.sortObjectBy(this.game[componentType][recordComponentTypePlural], ['type', 'name'], true);//Object.keys(this.game[componentType][recordComponentTypePlural]);
		}
	
		// Stop if no record
		if ( ! dbRecord || ! record )
		{
			return;
		}
	
		// Check if a similar modal is shown
		let reuseModal = false;
		let existingElement = document.querySelector('.modal.show.modal--component-info');
	
		// Create modal
		let modalData;
		// If one already open, reuse?
		if ( existingElement )
		{
			reuseModal = true;
			modalData = Modal.getShownTemplate();
		}
		else
		{
			modalData = Modal.createFromTemplate(component__modalInfoTemplate);
			// Reset any styles in the template
			modalData.$modal.addClass('fade modal-fullscreen').attr('style', '');
			//modalData.$modal.find('.modal-dialog').addClass('modal-fullscreen');
		}
	
		// Set component data on modal and CTAs
		modalData.$modal
		.add( modalData.$modal.find('a, [type="button"]') )
		.attr({
			'data-component-key': componentKey,
			'data-component-type': componentType,
			'data-bypass-queue': 'true'
		});
		
		// Find indexes and keys for previous and next navigation
		let numRecords = recordKeys.length;
		let recordIndex = recordKeys.indexOf(componentKey);
		let minRecordIndex = 0;
		let maxRecordIndex = numRecords - 1;
		// Wrap indexes if necessary
		let nextRecordIndex = ( recordIndex + 1 > maxRecordIndex ) ? 0 : recordIndex + 1;
		let prevRecordIndex = ( recordIndex - 1 < minRecordIndex ) ? maxRecordIndex : recordIndex - 1;
		//console.log(recordKeys, prevRecordIndex, recordIndex, nextRecordIndex);
		// Update previous and next CTAs
		modalData.$modal.find('.button--component-info-next')
		.attr({
			'data-component-key': recordKeys[nextRecordIndex]
		})
		.toggleClass('d-none', ( numRecords <= 1 ));
		modalData.$modal.find('.button--component-info-prev')
		.attr({
			'data-component-key': recordKeys[prevRecordIndex]
		})
		.toggleClass('d-none', ( numRecords <= 1 ));
		
		// Update CTA language and hide
		let ctas = ['add', 'activate', 'deactivate', 'use', 'restart', 'hint', 'info'];
		let ctaClassRef = {};
		for ( let cta of ctas )
		{
			let ctaClass = `.cta--component-${cta}`;
			ctaClassRef[cta] = ctaClass;
			let verb = RL.u.capitalize(this.game.getVerb(cta, '', '', record, componentType));
			modalData.$modal.find(ctaClass).html(verb).addClass('d-none');
		}
		
		// If hints, set them up
		if ( Reflect.has(record, 'hints') )
		{
			let $component__badge_hintAmount = $(component__badge_hintAmount)
			.html(Array.isArray(record.hints) ? record.hints.length : 1);
			
			// Update CTAs
			modalData.$modal.find(ctaClassRef['hint'])
			.append($component__badge_hintAmount)
			.attr('data-bypass-queue', 'true')
			.removeClass('d-none');
		}
	
		// Work with similar content
	
		// Icons reference
		let $iconConatiner = modalData.$modal.find('.modal-heading-icon').empty().removeClass('d-none');
	
		// Update title
		let name = record?.name ?? dbRecord?.name ?? RL.u.capitalize(componentType);
		modalData.$modalTitle.html(name);
	
		// Update image
		let $imageCol = modalData.$modal.find('.modal-image-col').empty();
		if ( record.image )
		{
			let $component__modalInfoImageContainer = $(component__modalInfoImageContainer).find('img').attr('src', this.game.parseShortcodes(record.image));
			$imageCol.append($component__modalInfoImageContainer);
		}
	
		// Update type, description, time ago, etc.
		let $textCol = modalData.$modal.find('.modal-text-col').empty();
	
		if ( record.type )
		{
			$textCol.append(`
			<div class="mb-2">
				<b>Type:</b> ${RL.u.capitalize(record.type)}
			</div>
			`);
		}
	
		if ( record.description )
		{
			$textCol.append(`
			<div class="mb-2">
				<b>Description:</b> ${this.game.parseShortcodes(record.description)}
			</div>
			`);
		}
	
		if ( record.notes )
		{
			$textCol.append(`
			<div class="mb-2">
				<b>Notes:</b> ${this.game.parseShortcodes(record.notes)}
			</div>
			`);
		}
	
		// Check for associated locations
		let locationRestricted = false;
		let locationNames = [];
		let locationsText = '';
		// If challenge associated with locations
		if ( record.locations )
		{
			let associatedLocationIds = Object.keys(record.locations);
			associatedLocationIds.forEach(id => {
				// Get location reference
				let dbLocation = this.game.database.locations[id];
				if ( dbLocation )
				{
					locationNames.push(dbLocation.name);
				}
			});
	
			// Check active location in restrictions
			locationRestricted = ( associatedLocationIds.indexOf(this.game.location.active?.id) === -1 );
			
			// If there are locations, make a sentence with them
			if ( locationNames.length > 0 )
			{
				locationsText = RL.u.createConjunctionSentence(locationNames, '', '');
	
				$textCol.append(`
				<div class="mb-2">
					<b>Locations:</b> <i class="icon-map-pin icon--fa icon--fw"></i> ${locationsText}
				</div>
				`);
			}
		}
	
		if ( Reflect.has(record, 'addTime') || Reflect.has(record, 'lastAddTime') )
		{
			$textCol.append(`
			<div class="mb-2">
				<b>Added:</b> ${RL.u.timeAgo(Date.now() - (record.lastAddTime ?? record.addTime))}
			</div>
			`);
		}
	
		if ( componentType == 'item' )
		{
			// Update button options, CTAs
	
			// If not tracking as inventory, hide CTAs
			if ( record.isInventory )
			{
				// If item uses stock, set up display
				if ( Reflect.has(record, 'stock') )
				{
					// Check if item uses stock
					let depletesStock = this.game.item.depletesStock(record);
					let stock = ( depletesStock ) ? record.stock : '<i class="icon-infinity icon--fa icon--fw"></i>';
					let maxStock = ( Reflect.has(record, 'maxStock') ) ? `, max stock ${record.maxStock}` : '';
		
					// Update content
					$textCol.append(`
					<div class="mb-2">
						<b>Stock:</b> ${stock}${maxStock}
					</div>
					`);
		
					// Update CTA
					let $component__badge_stockAmount = $(component__badge_stockAmount).html(stock);
		
					modalData.$modal.find(ctaClassRef['use'])
					.append($component__badge_stockAmount);
					
					modalData.$modal.find('.badge--stock-amount').html(stock);
					
					// If item is used up, disable CTAs
					if ( record.stock <= 0 )
					{
						modalData.$modal.find(ctaClassRef['use']).addClass('disabled').attr({'aria-disabled': 'true', 'disabled': true});
					}
				}
	
				// If activated
				if ( record.active )
				{
					// Update CTAs
					modalData.$modal.find([ctaClassRef['use'], ctaClassRef['deactivate']].join(',')).removeClass('d-none');
				}
				// Else available to activate
				else
				{
					// Update CTAs
					modalData.$modal.find(ctaClassRef['activate']).removeClass('d-none');
				}
			}
		}
		else if ( componentType == 'challenge' )
		{
			// Get requirements data for feedback
			let prerequisitesData = this.game.challenge.checkPrerequisites(componentKey, this.game.player);
			let requirementsData = this.game.challenge.checkRequirements(componentKey, this.game.player);
	
			// Get challenges for reference
			let activeChallengeIds = Object.keys(this.game.challenge.activeChallenges);
			let availableChallengeIds = Object.keys(this.game.challenge.availableChallenges);
			let completedChallengeIds = Object.keys(this.game.challenge.completedChallenges);
	
			// If challenge has already been completed or if replaying the game
			let isComplete = ( completedChallengeIds.indexOf(componentKey) !== -1 );
			let isActive = ( activeChallengeIds.indexOf(componentKey) !== -1 );
			let allowReplay = ( isComplete && this.game.completeCount );
	
			let status = 'available';
			if ( isComplete )
			{
				status = 'complete';
			}
			else if ( isActive )
			{
				status = 'active';
			}
			else if ( ! prerequisitesData.met )
			{
				status = 'unavailable';
			}
			
			$textCol.append(`
			<div class="mb-2">
				<b>Status:</b> ${RL.u.capitalize(status)}
			</div>
			`);
	
			if ( isComplete )
			{
				$iconConatiner.html('<i class="icon-complete icon--fa icon--fw"></i>' + $iconConatiner.html()).removeClass('d-none');
			}
	
			// Update button options, CTAs
	
			if ( isActive )
			{
				$iconConatiner.html('<i class="icon-warning icon--fa icon--fw"></i>' + $iconConatiner.html()).removeClass('d-none');
	
				// Update CTAs
				modalData.$modal.find([ctaClassRef['restart'], ctaClassRef['deactivate']].join(',')).removeClass('d-none');
				
				// Update content
	
				// Update timer progress
				if ( record.timer )
				{
					let progressPercent = record.timer.progressPercent;
	
					$textCol.append(`
					<div class="mb-2">
						<b>Timer:</b> ${progressPercent}%
					</div>
					`);
				}
				
				let progressPercent = requirementsData.progressPercent;
				$textCol.append(`
				<div class="mb-2">
					<b>Progress:</b> ${progressPercent}%
				</div>
				`);
			}
			// If challenge is not available because prerequisites are not met
			else if ( ! prerequisitesData.met )
			{
				$iconConatiner.html('<i class="icon-incomplete icon--fa icon--fw"></i>' + $iconConatiner.html()).removeClass('d-none');
				
			}
			// If complete and no replay
			else if ( isComplete && ! allowReplay )
			{
			}
			// Else the challenge is available
			else 
			{
				$iconConatiner.html('<i class="icon-available icon--fa icon--fw"></i>' + $iconConatiner.html()).removeClass('d-none');
	
				// Update CTAs
				modalData.$modal.find(ctaClassRef['activate']).removeClass('d-none');
			}
			
			$textCol.append(`
			<div class="mb-2">
				<b>Prerequisites:</b> <span class="text-primary"><i class="icon-info icon--fa icon--fw"></i></span> ${prerequisitesData.text}
			</div>
			`);
	
			if ( prerequisitesData.metText )
			{
				$textCol.append(`
				<div class="mb-2">
					<b>Prerequisites Met:</b> <span class="text-success"><i class="icon-complete icon--fa icon--fw"></i></span> ${prerequisitesData.metText}
				</div>
				`);
			}
	
			if ( prerequisitesData.missingText )
			{
				$textCol.append(`
				<div class="mb-2">
					<b>Prerequisites Missing:</b> <span class="text-danger"><i class="icon-incomplete icon--fa icon--fw"></i></span> ${prerequisitesData.missingText}
				</div>
				`);
			}
			
			$textCol.append(`
			<div class="mb-2">
				<b>Requirements:</b> <span class="text-primary"><i class="icon-info icon--fa icon--fw"></i></span> ${requirementsData.text}
			</div>
			`);
	
			if ( requirementsData.metText )
			{
				$textCol.append(`
				<div class="mb-2">
					<b>Requirements Met:</b> <span class="text-success"><i class="icon-complete icon--fa icon--fw"></i></span> ${requirementsData.metText}
				</div>
				`);
			}
	
			if ( requirementsData.missingText )
			{
				$textCol.append(`
				<div class="mb-2">
					<b>Requirements Missing:</b> <span class="text-danger"><i class="icon-incomplete icon--fa icon--fw"></i></span> ${requirementsData.missingText}
				</div>
				`);
			}
		}
		else if ( componentType == 'character' )
		{
			// If active
			if ( record.active )
			{
				// Update CTAs
				modalData.$modal.find([ctaClassRef['use'], ctaClassRef['deactivate']].join(',')).removeClass('d-none');
			}
			else
			{
				// Update CTAs
				modalData.$modal.find(ctaClassRef['activate']).removeClass('d-none');
			}
		}
		else if ( componentType == 'location' )
		{
			// If active
			if ( this.game.location.active && this.game.location.active.id == componentKey )
			{
				// Update CTAs
				modalData.$modal.find([ctaClassRef['use'], ctaClassRef['deactivate']].join(',')).removeClass('d-none');
			}
			else
			{
				// Update CTAs
				modalData.$modal.find(ctaClassRef['activate']).removeClass('d-none');
			}
		}
	
		// Show modal or update it
		if ( reuseModal )
		{
			// Update Bootstrap
			const bsModal = bootstrap.Modal.getInstance(document.querySelector('.modal.show'));
			bsModal.handleUpdate();//$modal.handleUpdate();
		}
		else
		{
			// Limit closing modal to only given options
			// Remove close button
			//$modal.find('.btn-close').addClass('d-none');
			// Don't allow backdrop or keyboard to close modal
			Modal.show(modalData.$modal);//, {backdrop: 'static', keyboard: false}
		}
	}
	
	updateModal ()
	{
		this.game.logger.debug(`${this.className} ${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);
	
		const modal = document.querySelector('.modal.show.modal--component-info');
		if ( ! modal )
		{
			return;
		}
	
		const componentType = modal.getAttribute('data-component-type');
		const componentKey = modal.getAttribute('data-component-key');
		this.showModal(componentType, componentKey);
	}
	
}