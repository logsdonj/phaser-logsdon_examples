/**
 * 
 */
class TourUi
{
	// Properties

	className = 'TourUi';

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
    }

	doTitleScreenIntro ()
	{
		introJs().setOptions({
			steps: [{
				title: 'Welcome 👋',
				intro: 'This is the <span class="text-nowrap"><i class="icon-home icon--fa icon--fw"></i> Title</span> screen.'
			},
			{
				title: 'The Highlights',
				element: document.querySelector('#title-screen .cta--start-tour'),
				intro: 'Click the <span class="text-nowrap"><i class="fa-solid fa-compass fa-fw" style="font-family: \'Font Awesome 6 Free\' !important;"></i> Compass</span> icon whenever you see it for a tour.'
			},
			{
				title: 'Better Gameplay',
				element: document.querySelector('#title-screen .tour__enhanced-experience'),
				intro: 'You can have a better experience by allowing fullscreen and camera access ahead of time.'
			},
			{
				title: 'Instructions',
				element: document.querySelector('#title-screen #instructions-button'),
				intro: 'If you are new, you can quickly learn how to play with these Instructions.'
			},
			{
				title: 'Farewell!',
				element: document.querySelector('#title-screen .cta--game-start'),
				intro: 'When you are ready to start the game, click Start.'
			}]
		}).start();
	}
	
	doMainScreenIntro ()
	{
		introJs().setOptions({
			steps: [{
				title: 'Main Screen',
				intro: 'This is the Main screen where you play most of your game.'
			},{
				element: document.querySelector('#main-screen  [data-screen="title"]'),
				intro: 'Click here to return to the <span class="text-nowrap"><i class="icon-home icon--fa icon--fw"></i> Title</span> screen.'
			},
			{
				title: 'Stats',
				element: document.querySelector('#main-screen  .game-screen__player-stats-row'),
				intro: 'Player Stats like <span class="text-nowrap"><i class="icon-health icon--fa icon--fw"></i> Health</span> can be seen here.'
			},
			{
				title: 'Statuses',
				element: document.querySelector('#main-screen  .game-screen__status-row'),
				intro: '<p>Your statuses will show here.</p><p>An <span class="text-nowrap"><i class="icon-challenge icon--fa icon--fw"></i> Active Challenge</span> will show helpful information to progress.</p>'
			},
			{
				title: 'Scans',
				element: document.querySelector('#main-screen  .cta--start-scan'),
				intro: '<p>Click here to <span class="text-nowrap"><i class="icon-scan icon--fa icon--fw"></i> Scan</span> objects.</p><p>Below it you can click to <span class="text-nowrap"><i class="icon-upload icon--fa icon--fw"></i> Upload</span> scans if needed.</p>'
			},
			{
				title: 'Screens',
				element: document.querySelector('#main-screen  .game-screen__nav-icon-row'),
				intro: '<p>Click these icons to access <span class="text-nowrap"><i class="icon-challenges icon--fa icon--fw"></i> Challenge</span>, <span class="text-nowrap"><i class="icon-items icon--fa icon--fw"></i> Item</span>, <span class="text-nowrap"><i class="icon-locations icon--fa icon--fw"></i> Location</span>, and <span class="text-nowrap"><i class="icon-characters icon--fa icon--fw"></i> Character</span> screens.</p><p>You can start new challenges, equip items, visit locations, and interact with characters from these as they become available.</p>'
			}]
		}).start();
	}
	
}