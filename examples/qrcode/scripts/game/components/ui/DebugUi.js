/**
 * 
 */
class DebugUi
{
	// Properties

	static game;

	// Process an example payload
	// {"items":{"1":""}}
	static examplePayload = {
		/*
		items: {
			1: '',
			2: {
				stock: 2
			}
		},
		challenges: {
			1: ''
		}
		*/
		locations: {
			1: ''
		}
	};

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ()
	{
    }

	static createPayloadOptions ( type, record, payload )
	{
		//createPayloadButton(type, text, payload);
		DebugUi.createPayloadDropdownOption(type, record, payload);
	}
	
	static createPayloadButton ( type, record, payload )
	{
		// beforebegin, afterbegin, beforeend, afterend
		let buttons = document.getElementById(type + 'PayloadButtons');
		
		if ( buttons )
		{
			buttons
			.insertAdjacentHTML('beforeend', `<button type="button" class="btn btn-primary btn-sm mb-1 me-1"><i class="fa-solid fa-plus fa-2xs"></i><i class="fa-solid fa-qrcode fa-fw"></i> ${record.name}</button>`);
	
			let newButton = buttons.lastChild;
			newButton.addEventListener('click', function (event) {
				DebugUi.game.processPayload(payload);
				return false;
			}, false);
		}
	}
	
	static createPayloadDropdownOption ( type, record, payload )
	{
		let optionTemplate = `
		<li class="payload-dropdown-option payload-dropdown-option--items">
			<a class="dropdown-item" href="#" onclick="document.getElementById('injectPayloadValue').value = JSON.stringify({items:{1:''}});document.getElementById('injectPayloadValue').focus();return false;"><b>${record.name ?? record}</b>${record.description ? `<br><small class="">&ndash; ${record.description}</small>` : ''}</a>
		</li>
		`;
	
		let $newOption = $(optionTemplate).insertBefore($(`.payload-dropdown-option--${type}`).last());
		$newOption.on('click', function ( event ) {
			event.preventDefault();
			document.getElementById('injectPayloadValue').value = JSON.stringify(payload);
			document.getElementById('injectPayloadValue').focus();
			DebugUi.game.processPayload(payload);
		});
	}
	
}