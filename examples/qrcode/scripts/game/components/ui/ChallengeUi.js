// TODO: More cohesive structure
/**
 * 
 */
class ChallengeUi
{
	// Properties

	className = 'ChallengeUi';
	updateHudDebounced;

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.updateHudDebounced = RL.u.debounce(this.updateHud.bind(this), this.game.ui.debounceDelay);
    }

	// Update challenge screen listings
	updateHud ()
	{
		this.game.logger.debug(`${this.className} ${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);

		let collectorMode = this.game.getSetting('collectorMode', '', '', 'game');
		let actionMode = this.game.getSetting('actionMode', '', '', 'game');
		let hudMode = this.game.getSetting('hudMode', '', '', 'challenge');
		let componentType = 'challenge';
		
		// Update available challenges
		this.game.challenge.updateAvailable();

		// Get challenges for reference
		let activeChallengeIds = Object.keys(this.game.challenge.activeChallenges);
		let availableChallengeIds = Object.keys(this.game.challenge.availableChallenges);
		let completedChallengeIds = Object.keys(this.game.challenge.completedChallenges);
		
		// Reset active challenge
		let $activeChallengeStatusContainer = $('.status-container--active-challenges');
		$activeChallengeStatusContainer.addClass('d-none');
		let $activeChallengeStatus = jQuery('.active-challenges-status');
		$activeChallengeStatus.empty();

		// Get challenge status element reference
		let $challengesStatusContainer = $('.status-container--challenges');
		$challengesStatusContainer.addClass('d-none');
		let $challengesStatus = jQuery('.challenges-status');
		$challengesStatus.empty();

		// Reset the list of challenges on the game screen
		let $challengesList = jQuery('.challenges-list');
		$challengesList.empty();

		// Sort into array of ids
		let challengeIds = RL.u.sortObjectBy(this.game[ actionMode ? 'database' : 'challenge' ].challenges, ['order', 'difficulty', 'type', 'name'], true);

		// For each challenge currently in the game
		//let challengeIds = Object.keys(this.game.challenge.challenges);
		challengeIds.forEach(challengeId => {
			// Get challenge reference
			let dbChallenge = this.game.database.challenges[challengeId];
			let challenge = this.game.challenge.challenges[challengeId];
			
			// If challenge has already been completed or if replaying the game
			let isAdded = Reflect.has(this.game.challenge.challenges, challengeId);
			let isComplete = ( completedChallengeIds.indexOf(challengeId) !== -1 );
			let isActive = ( activeChallengeIds.indexOf(challengeId) !== -1 );
			let allowReplay = ( isComplete && this.game.completeCount );

			let isCombination = false;
			if ( this.game.database?.combinations )
			{
				const combinationKeys = Object.keys(this.game.database.combinations);
				for ( const combinationKey of combinationKeys )
				{
					const combination = this.game.database.combinations[combinationKey];
					if ( combination?.payload?.challenges && Reflect.has(combination.payload.challenges, challengeId) )
					{
						isCombination = true;
						break;
					}
				}
			}

			// Get requirements data for feedback
			let prerequisitesData = this.game.challenge.checkPrerequisites(challengeId, this.game.player);
			let requirementsData = this.game.challenge.checkRequirements(challengeId, this.game.player);

			// Check for associated locations
			let locationRestricted = false;
			let locationNames = [];
			let locationsText = '';
			// If challenge associated with locations
			if ( dbChallenge.locations )
			{
				let associatedLocationIds = Object.keys(dbChallenge.locations);
				associatedLocationIds.forEach(id => {
					// Get location reference
					let dbLocation = this.game.database.locations[id];
					if ( dbLocation )
					{
						locationNames.push(dbLocation.name);
					}
				});

				// Check active location in restrictions
				locationRestricted = ( associatedLocationIds.indexOf(this.game.location.active?.id) === -1 );
				
				// If there are locations, make a sentence with them
				if ( locationNames.length > 0 )
				{
					locationsText = RL.u.createConjunctionSentence(locationNames, 'Available at ', '.');
				}
			}
			
			// Start template
			let $template = $(component__listGroupItemTemplate)
			.appendTo($challengesList)
			.attr('data-challenge-id', challengeId);

			// Hiding all areas
			$template.find('[class*="list-group-item__"]').addClass('d-none');

			// Update content
			$template.find('.list-group-item__heading')
			.html(dbChallenge.name)
			.removeClass('d-none');

			$template.find('.list-group-item__type')
			.html(`${RL.u.capitalize(dbChallenge.type ?? '')}`)
			.removeClass('d-none');

			let descriptionText = '';
			let notesText = '';
			let $templateNotes = $template.find('.list-group-item__notes');

			if ( hudMode == 'minimal' )
			{
				if ( ! isComplete )
				{
					notesText = RL.u.filterArrayEmpty([
						dbChallenge.description
					]).join('<br>');
				}
			}
			else
			{
				descriptionText = RL.u.filterArrayEmpty([
					locationsText,
					dbChallenge.description
				]).join(' ');
				
				notesText = RL.u.filterArrayEmpty([
					dbChallenge.notes ?? '', 
					`${prerequisitesData.text}`
					`${requirementsData.text}`
				]).join('<br>');
			}

			$template.find('.list-group-item__description')
			.html(descriptionText)
			.toggleClass('d-none', ! descriptionText);
			
			$templateNotes
			.html(notesText)
			.toggleClass('d-none', ! notesText);
			
			let $templateIcon = $template.find('.list-group-item__heading-icon');

			// If there are locations, show locations
			if ( locationNames.length > 0 )
			{
				$template.find('.list-group-item__locations')
				.html(`<i class="icon-map-pin icon--fa icon--fw"></i> ${RL.u.createConjunctionSentence(locationNames, '', '', 'or')}`);
			}

			// Update CTAs
			$template.find('.list-group-item__cta-container')
			.removeClass('d-none')
			.find('a, [type="button"]').attr({
				'data-challenge-id': challengeId,
				'data-component-key': challengeId,
				'data-component-type': 'challenge'
			});
			
			// Update CTA language and hide
			let ctas = ['add', 'activate', 'deactivate', 'use', 'restart', 'hint', 'info'];
			let ctaClassRef = {};
			for ( let cta of ctas )
			{
				let ctaClass = `.cta--component-${cta}`;
				ctaClassRef[cta] = ctaClass;
				let verb = RL.u.capitalize(this.game.getVerb(cta, '', '', location, componentType));
				$template.find(ctaClass).html(verb).addClass('d-none');
			}

			// If is added, show info
			if ( isAdded )
			{
				// Update CTAs
				$template.find(ctaClassRef['info'])
				.removeClass('d-none');
			}

			// If hints, set them up
			if ( isAdded && Reflect.has(dbChallenge, 'hints') )
			{
				let $component__badge_hintAmount = $(component__badge_hintAmount)
				.html(Array.isArray(dbChallenge.hints) ? dbChallenge.hints.length : 1);
				
				// Update CTAs
				$template.find(ctaClassRef['hint'])
				.append($component__badge_hintAmount)
				.attr('data-bypass-queue', 'true')
				.removeClass('d-none');
			}

			let activeChallengeClass = 'active challenge--active';
			let completeClass = 'challenge--complete';
			let restrictedClass = 'challenge--restricted';
			let availableClass = 'challenge--available';

			// If challenge is complete, add indicators
			if ( isComplete )
			{
				// Update listing
				$template.addClass(completeClass);
				// Update icon
				$templateIcon.html('<i class="icon-complete icon--fa icon--fw text-success"></i>' + $templateIcon.html()).removeClass('d-none');
				// Update CTAs
				$template.find([ctaClassRef['hint']].join(',')).addClass('d-none');
			}

			// If challenge is active
			if ( isActive )
			{
				// Update listing
				$template.addClass(activeChallengeClass).attr('aria-current', true);
				// Update icon
				$templateIcon.html('<i class="icon-warning icon--fa icon--fw"></i>' + $templateIcon.html()).removeClass('d-none');
				// Update CTAs
				$template.find([ctaClassRef['restart'], ctaClassRef['deactivate']].join(',')).removeClass('d-none');
				
				// Update notes
				let notesText = '';
				if ( hudMode == 'minimal' )
				{
					notesText = RL.u.filterArrayEmpty([
						(requirementsData.metText ? `<span class="text-success"><i class="icon-complete icon--fa icon--fw"></i> ${requirementsData.metText}</span>` : ''),
						(requirementsData.missingText ? `<span class="text-danger"><i class="icon-incomplete icon--fa icon--fw"></i> ${requirementsData.missingText}</span>` : ''),
					]).join('<br>');
				}
				else
				{
					notesText = RL.u.filterArrayEmpty([
						dbChallenge.notes, 
						(requirementsData.text ? `<span class="text-primary"><i class="icon-info icon--fa icon--fw"></i> ${requirementsData.text}</span>` : ''),
						(requirementsData.metText ? `<span class="text-success"><i class="icon-complete icon--fa icon--fw"></i> ${requirementsData.metText}</span>` : ''),
						(requirementsData.missingText ? `<span class="text-danger"><i class="icon-incomplete icon--fa icon--fw"></i> ${requirementsData.missingText}</span>` : ''),
					]).join('<br>');
				}
				$templateNotes.html(notesText);

				// Update timer progress
				if ( challenge.timer )
				{
					let progressPercent = challenge.timer.progressPercent;
					let $progress = $template.find('.list-group-item__timer-progress');
					let $progressBar = $progress.find('.progress-bar');

					$progress.removeClass('d-none');
					$progressBar
					.css('width', `${progressPercent}%`)
					.html(`${progressPercent}%`)
					.attr({'aria-valuenow': progressPercent});

					// Change the progress color
					if ( progressPercent > 80 )
					{
						$progressBar.addClass('bg-danger');
					}
					else if ( progressPercent > 60 )
					{
						$progressBar.addClass('bg-warning');
					}
					else
					{
						$progressBar.addClass('bg-success');
					}
				}

				// Update progress
				let progressPercent = requirementsData.progressPercent;
				let $progress = $template.find('.list-group-item__progress');
				let $progressBar = $progress.find('.progress-bar');
				
				$progress.removeClass('d-none');
				$progressBar
				.css('width', `${progressPercent}%`)
				.html(`${progressPercent}%`)
				.attr({'aria-valuenow': progressPercent});

				// Clone for active challenge display
				let $activeTemplate = $template.clone()
				.appendTo($activeChallengeStatus)
				.removeClass(activeChallengeClass);
				// Hide elements
				$activeTemplate.find('.list-group-item__icon').addClass('d-none');
			}
			// If challenge is not available because prerequisites are not met
			else if ( ! prerequisitesData.met )
			{
				// Update listing
				$template.addClass(restrictedClass);//.addClass('disabled').attr({'aria-disabled': 'true', 'disabled': true});
				// Update icon
				//$templateIcon.html('<i class="icon-incomplete icon--fa icon--fw"></i>' + $templateIcon.html()).removeClass('d-none');
				// Update CTAs
				//$templateCtaContainer.addClass('d-none');
				
				// Update content
				$template.find('.list-group-item__heading')
				.addClass('text-muted');
				
				let componentNoun = this.game.getLexicon('challenge', '', dbChallenge, 'challenge');
				
				// Update notes
				if ( hudMode == 'minimal' )
				{
					let notesText = RL.u.filterArrayEmpty([
						dbChallenge.description ?? '', 
						(! prerequisitesData.met ? `<i class="fa-solid fa-triangle-exclamation fa-fw"></i> Prerequisites: ${prerequisitesData.text}` : '')
					]).join('<br>');
					$templateNotes.html(notesText);
				}
				else
				{
					let notesText = RL.u.filterArrayEmpty([
						dbChallenge.notes ?? '', 
						(! prerequisitesData.met ? `<i class="fa-solid fa-triangle-exclamation fa-fw"></i> The ${componentNoun} is unavailable until prerequisites are met: ${prerequisitesData.text}` : ''),
						(! requirementsData.met && prerequisitesData.text != requirementsData.text ? requirementsData.text : '')
					]).join('<br>');
					$templateNotes.html(notesText);
				}
			}
			// If complete and no replay
			else if ( isComplete && ! allowReplay )
			{
				//$templateCtaContainer.addClass('d-none');
			}
			// If the challenge is added
			else if ( isAdded )
			{
				// Update listing
				$template.addClass(availableClass);
				// Update icon
				$templateIcon.html('<i class="icon-available icon--fa icon--fw"></i>' + $templateIcon.html()).removeClass('d-none');
				// Update CTAs
				$template.find(ctaClassRef['activate']).removeClass('d-none');
			}
			// If in action mode
			else if ( actionMode )
			{
				// Update CTAs
				$template.find(ctaClassRef['add']).removeClass('d-none');
			}
			
			// Add combinable icon to type
			if ( isCombination )
			{
				$template.find('.list-group-item__type')
				.prepend(`<i class="fa-solid fa-screwdriver-wrench fa-fw me-1"></i>`);
			}
		});

		let componentNoun = this.game.getLexicon('challenge', '', '', 'challenge');
		
		// If no challenges, note it
		if ( ! challengeIds.length )
		{
			let $template = $(component__listGroupItemTemplate).appendTo($challengesList)
			.html(`No ${RL.u.wordPluralizeSuffix(componentNoun, 0)} are available.`);
		}

		// If there are no active challenges, prompt player in challenge status
		if ( ! activeChallengeIds.length )
		{
			let availableChallengeText = '';
			// Refresh available challenges
			availableChallengeIds = Object.keys(this.game.challenge.availableChallenges);
			// If there are available challenges
			if ( availableChallengeIds.length )
			{
				availableChallengeText = `You have ${RL.u.wordPluralizeSuffix(componentNoun, 0)} waiting. <a href="#challenges-screen" data-screen="challenges">Activate one!</a>`;
			}
			else
			{
				availableChallengeText = `You have no ${RL.u.wordPluralizeSuffix(componentNoun, 0)} available. Explore and find some!`;
			}

			$challengesStatus.html(availableChallengeText);
		}

		// If there is a status, show the container
		if ( $challengesStatus.length && $challengesStatus.html().length )
		{
			$challengesStatusContainer.removeClass('d-none');
		}

		// If there is a status, show the container
		if ( $activeChallengeStatus.length && $activeChallengeStatus.html().length )
		{
			$activeChallengeStatusContainer.removeClass('d-none');
		}

		// Update game icon stat
		$('.stat--num-available-challenges').html(challengeIds.length);
		
		// Trigger event
		this.game.challenge.events.trigger(this.game.challenge.events.names.updateHud);
	}

	// Event handlers

	handleAdd ( challengeId, challenge, newRecord, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${challenge.name} added!`);
		let notifyAdd = this.game.getSetting('notifyAdd', params, challenge, 'challenge');

		// If the first time this item is found, show info
		if ( newRecord )
		{
			let showcase = this.game.getSetting('showcase', params, challenge, 'challenge');

			let componentNoun = this.game.getLexicon('challenge', params, challenge, 'challenge');
			let addVerb = this.game.getVerb('add', 'past', params, challenge, 'challenge');

			// If showcasing
			if ( showcase )
			{
				this.game.showcaseComponent({
					title: `New ${RL.u.capitalize(componentNoun)} ${RL.u.capitalize(addVerb)}`, 
					name: challenge.name,
					image: challenge.image,
					description: challenge.description,
					component: challenge,
					componentKey: challengeId,
					componentType: 'challenge'
				}, true, params?.bypassQueue);
			}
			else if ( notifyAdd )
			{
				/*
				this.game.queueAction({type: 'modal', params: {
					title: `<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>New ${componentNoun}!`, 
					body: `${challenge.name} ${addVerb}!`,
					cancelButtonHide: true,
					confirmButtonLabel: 'Continue'
				}}, true, params?.bypassQueue);
				*/
				/*
				this.game.queueAction({type: 'toast', params: {
					body: `${componentNoun} '${challenge.name}' ${addVerb}!`,
					cancelButtonHide: true,
					confirmButtonLabel: 'Continue',
					type: 'toastTemplateColor'
				}}, true, params?.bypassQueue);
				*/
				
				let addVerb = this.game.getVerb('add', 'past', params, challenge, 'challenge');
				let activateVerb = RL.u.capitalize(this.game.getVerb('activate', '', params, challenge, 'challenge'));
				
				let prerequisitesData = this.game.challenge.checkPrerequisites(challengeId, this.game.player)
				this.game.queueAction({type: 'toast', params: {
					title: `<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>New ${componentNoun}`,
					body: `${challenge.name} ${addVerb}!`,
					ctas: {
						primary: {
							attrObj: {
								'data-component-type': 'challenge',
								'data-component-key': challengeId
							},
							classAddon: ( challenge.activateOnAdd || ! prerequisitesData.met ) ? 'd-none' : 'cta--component-activate',
							label: activateVerb
						}
					},
					type: 'toastTemplate'
				}}, true, params?.bypassQueue);
			}
		}
		
		this.game.challenge.updateActiveProgress();
	}

	handleActivate ( challengeId, challenge, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${challenge.name} activated!`);
		
		let notifyActivate = this.game.getSetting('notifyActivate', params, challenge, 'challenge');
		if ( notifyActivate )
		{
			let activateVerb = this.game.getVerb('activate', 'past', params, challenge, 'challenge');

			this.game.queueAction({type: 'toast', params: {
				body: `${challenge.name} ${activateVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		// If there is an intro, show it
		if ( Reflect.has(challenge, 'intro') )
		{
			let componentNoun = this.game.getLexicon('challenge', params, challenge, 'challenge');
			let intro = challenge.intro;
			let title = ( typeof(intro) == 'object' && Reflect.has(intro, 'title') ) ? intro.title : `${RL.u.capitalize(componentNoun)} Intro`;
			let body = this.game.parseShortcodes( ( typeof(intro) == 'object' && Reflect.has(intro, 'body') ) ? intro.body : intro );

			this.game.queueAction({type: 'modal', params: {
				title: title, 
				body: body,
				cancelButtonHide: true,
				confirmButtonLabel: 'Continue'
			}}, true, params?.bypassQueue);
		}

		this.game.challenge.updateActiveProgress();
	}

	handleDeactivate ( challengeId, challenge, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${challenge.name} deactivated!`);
		
		let notifyDeactivate = this.game.getSetting('notifyDeactivate', params, challenge, 'challenge');
		if ( notifyDeactivate )
		{
			let deactivateVerb = this.game.getVerb('deactivate', 'past', params, challenge, 'challenge');

			this.game.queueAction({type: 'toast', params: {
				body: `${challenge.name} ${deactivateVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		this.game.challenge.updateActiveProgress();
	}

	handleProgress ( challengeId, challenge )
	{
		this.game.challenge.ui.updateHudDebounced();
	}

	handleComplete ( challengeId, challenge, params )
	{
		this.game.logger.debug(`You completed ${challenge.name}!`);
		
		let requirementsData = this.game.challenge.checkRequirements(challengeId, this.game.player, true);
		// Get challenge status element reference
		let $challengesStatus = jQuery('.challenges-status');
		// Update challenge status
		let feedbackText = `You completed ${challenge.name}! ${requirementsData.metText}`;
		$challengesStatus.html(feedbackText);

		let componentNoun = this.game.getLexicon('challenge', '', challenge, 'challenge');

		this.game.queueAction({type: 'modal', params: {
			title: `${RL.u.capitalize(componentNoun)} Complete!`, 
			body: feedbackText,
			cancelButtonHide: true,
			confirmButtonLabel: 'Continue'
		}}, true, params?.bypassQueue);

		// If there is an outtro, show it
		if ( Reflect.has(challenge, 'outtro') )
		{
			let outtro = challenge.outtro;
			let title = ( typeof(outtro) == 'object' && Reflect.has(outtro, 'title') ) ? outtro.title : `${RL.u.capitalize(componentNoun)} Outtro`;
			let body = this.game.parseShortcodes( ( typeof(outtro) == 'object' && Reflect.has(outtro, 'body') ) ? outtro.body : outtro );

			this.game.queueAction({type: 'modal', params: {
				title: title, 
				body: body,
				cancelButtonHide: true,
				confirmButtonLabel: 'Continue'
			}}, true, params?.bypassQueue);
		}

		this.game.ui.updateHudsDebounced();
	}

}