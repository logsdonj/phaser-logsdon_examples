/**
 * 
 */
class GameUi
{
	// Properties

	className = 'GameUi';
	debounceDelay = 10;
	updateHudsDebounced;
	resizeStatusContainerForScrollDebounced;

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.updateHudsDebounced = RL.u.debounce(this.updateHuds.bind(this), this.debounceDelay);
		this.resizeStatusContainerForScrollDebounced = RL.u.debounce(this.resizeStatusContainerForScroll.bind(this), this.debounceDelay * 10);
    }

	updateHuds ()
	{
		this.game.logger.debug(`${this.className} ${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);
	
		this.game.player.ui.updateHudDebounced();//updatePlayerStatsHudDebounced();
		this.game.challenge.ui.updateHudDebounced();//updateChallengesHudDebounced();
		this.game.item.ui.updateHudDebounced();//updateItemsHudDebounced();
		this.game.location.ui.updateHudDebounced();//updateLocationsHudDebounced();
		this.game.character.ui.updateHudDebounced();//updateCharactersHudDebounced();
		this.game.ui.componentInfo.updateModalDebounced();
		
		// Trigger event
		this.game.events.trigger(this.game.events.names.updateHud);
	}
	
	// If needing to scroll the status area, get all other vertical areas
	resizeStatusContainerForScroll ()
	{
		let titleRowHeight = jQuery('.game-screen__title-row').outerHeight(true);
		let playerStatsRowHeight = jQuery('.game-screen__player-stats-row').outerHeight(true);
		let inventoryRowHeight = jQuery('.game-screen__inventory-row').outerHeight(true);
		let bottomRowHeight = jQuery('.game-screen__bottom-row').outerHeight();

		let areasHeight = titleRowHeight + playerStatsRowHeight + inventoryRowHeight + bottomRowHeight;

		let $gsWrapper = jQuery('.game-screen__wrapper');
		let extraSpacing = $gsWrapper.outerHeight(true) - $gsWrapper.height();

		let windowHeight = $(window).height();
		let targetHeight = windowHeight - areasHeight - extraSpacing;

		jQuery('.game-screen__status-row').css({height: targetHeight, overflowY: 'auto'});

		/*
		console.log(windowHeight, areasHeight, extraSpacing);
		console.log(targetHeight);

		var bordT = $('img').outerWidth() - $('img').innerWidth();
		var paddT = $('img').innerWidth() - $('img').width();
		var margT = $('img').outerWidth(true) - $('img').outerWidth();

		console.log(jQuery('.game-screen').outerHeight(true));
		console.log(jQuery('.game-screen__wrapper').outerHeight(true));
		console.log(jQuery('.game-screen__wrapper').outerHeight());
		console.log(jQuery('.game-screen__wrapper').height());
		console.log(jQuery('.game-screen__wrapper').innerHeight());

		var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
		console.log($(window).height());
		console.log($(document).height());
		*/
	}

	// Event handlers

	handleProcessPayloadAfter ()
	{
		this.game.logger.debug('Payload processed.');
		// Update challenge progress after payload
		this.game.challenge.updateActiveProgress();
	}
	
	handleProgress ( gameRequirementsData )
	{
		this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);
	
		// If requirements met but not submitted, prompt for completion
		if ( gameRequirementsData.met && this.game.database.game?.submitTriggers && ! this.game.currentComplete )
		{
			let triggerData = this.game.checkSubmitTriggers(this.game.database.game);
			// Create submit triggers string
			let submitTriggersText = RL.u.createConjunctionSentence(triggerData.strings, 'You must ', ' to complete the game.');
	
			// Get game status element reference
			let $gameStatus = jQuery('.status-container--game');
			// Update game status
			let feedbackText = `<i class="icon-info icon--fa icon--fw"></i> You are ready to complete the game! ${submitTriggersText}`;
			$gameStatus.html(feedbackText).removeClass('d-none');
		}
	}
	
	handleComplete ( gameRequirementsData )
	{
		this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);
	
		let requirementsData = this.game.checkRequirements();
		// Get game status element reference
		let $gameStatus = jQuery('.status-container--game');
		// Update game status
		let componentNoun = this.game.getLexicon('game', '', '', 'game');
		let feedbackText = `<i class="icon-trophy icon--fa icon--fw"></i> You completed the ${componentNoun}! ${requirementsData.metText}<br>
		<i class="icon-timer icon--fa icon--fw me-1"></i>You played for ${RL.u.timeDurationToText(this.game.stopTime - this.game.startTime)}.`;
		$gameStatus.html(feedbackText).removeClass('d-none');
	
	
		if ( this.game.completeCount === 1 )
		{
			this.game.logger.debug(`You completed the game!`);
			
			this.game.queueAction({type: 'modal', params: {
				title: `${RL.u.capitalize(componentNoun)} Complete!`, 
				body: feedbackText,
				cancelButtonHide: true,
				confirmButtonLabel: 'Continue'
			}}, true);
		}
	
		// If there is an outtro, show it
		if ( Reflect.has(this.game.database.game, 'outtro') )
		{
			let outtro = this.game.database.game.outtro;
			let title = ( typeof(outtro) == 'object' && Reflect.has(outtro, 'title') ) ? outtro.title : `${RL.u.capitalize(componentNoun)} Outtro`;
			let body = this.game.parseShortcodes( ( typeof(outtro) == 'object' && Reflect.has(outtro, 'body') ) ? outtro.body : outtro );
	
			this.game.queueAction({type: 'modal', params: {
				title: title, 
				body: body,
				cancelButtonHide: true,
				confirmButtonLabel: 'Continue'
			}}, true);
		}
	
		// Show main screen with statuses
		this.game.ui.nav.showScreen('main');
	}
	
	handleOver ( gameRequirementsData )
	{
		this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);
	
		let requirementsData = this.game.checkRequirements();
		// Get game status element reference
		let $gameStatus = jQuery('.status-container--game');
		// Update game status
		let componentNoun = this.game.getLexicon('game', '', '', 'game');
		let feedbackText = `<i class="icon-death icon--fa icon--fw"></i> ${RL.u.capitalize(componentNoun)} Over! ${requirementsData.missingText}`;
		$gameStatus.html(feedbackText).removeClass('d-none');
	
		this.game.queueAction({type: 'modal', params: {
			title: `${RL.u.capitalize(componentNoun)} Over!`, 
			body: feedbackText,
			cancelButtonHide: true,
			confirmButtonLabel: 'Continue'
		}}, true);
	
		// If there is an outtro, show it
		if ( Reflect.has(this.game.database.game, 'overOuttro') )
		{
			this.game.queueAction({type: 'modal', params: {
				title: `${RL.u.capitalize(componentNoun)} Over Outtro`, 
				body: `${this.game.database.game.overOuttro}`,
				cancelButtonHide: true,
				confirmButtonLabel: 'Continue'
			}}, true);
		}
	
		// Show main screen with statuses
		this.game.ui.nav.showScreen('main');
	}
	
	handleError ( code, message, params )
	{
		// https://rollbar.com/guides/javascript/how-to-throw-exceptions-in-javascript/
		// https://xjamundx.medium.com/custom-javascript-errors-in-es6-aa891b173f87

		// If notify add, do it
		if ( code == 'CHALLENGE_ALREADY_ADDED' )
		{
			let challenge = params.challenge;
			params = params.params;
	
			let findVerb = this.game.getVerb('find', 'past', params, challenge, 'challenge');
	
			this.game.queueAction({type: 'toast', params: {
				body: `${challenge.name} already ${findVerb}.`,
				color: 'danger',
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}
	}

}