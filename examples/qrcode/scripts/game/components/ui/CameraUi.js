/**
 * 
 */
class CameraUi
{
	// Properties

	className = 'CameraUi';
	cameraPermission;

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;

		this.hasCameraPermission()
		.then((result) => {
			this.cameraPermission = result;
		});
    }

	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises
	// https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Promises
	// https://stackoverflow.com/questions/54901478/js-how-to-call-an-async-function-within-a-promise-then
	// https://www.javascripttutorial.net/es-next/javascript-async-await/
	async hasCameraPermission ( permission = 'granted' )
	{
		// https://developer.mozilla.org/en-US/docs/Web/API/Navigator/permissions
		return navigator.permissions.query({name: 'camera'})
		.then((permissionObj) => {
			//console.log(permissionObj.state);//prompt, granted, denied
			return ( permissionObj.state == permission );
		})
		.catch((error) => {
			console.log(`${error.name}: ${error.message}`, error);
		});
	}


	async checkCameraPermission ()
	{
		let cameraPermission = await this.hasCameraPermission();

		// If no permission, request it
		if ( ! cameraPermission )
		{
			cameraPermission = await this.requestCameraPermission({
				video: {
					facingMode: 'environment',
					torch: true,
					zoom: true
				}
			});
		}
		
		return cameraPermission;
	}

	async requestCameraPermission ( constraints, stopStream = true )
	{
		// If browser not capable of media devices
		// https://www.digitalocean.com/community/tutorials/front-and-rear-camera-access-with-javascripts-getusermedia
		if ( ! 'mediaDevices' in navigator || ! 'getUserMedia' in navigator.mediaDevices )
		{
			console.log('browser not capable of media devices');
			return 
		}

		if ( typeof(constraints) == 'undefined' )
		{
			constraints = {
				audio: true,
				video: true// {deviceId: cameraId, facingMode: 'environment', torch: true, zoom: true }
			};
		}
		
		// https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
		return navigator.mediaDevices.getUserMedia(constraints)
		.then((stream) => {
			/* Use the stream */
			// This was just a preemptive use to get permissions, so stop now

			// https://stackoverflow.com/questions/11642926/stop-close-webcam-stream-which-is-opened-by-navigator-mediadevices-getusermedia
			if ( stopStream )
			{
				stream.getTracks().forEach((track) =>
				{
					if ( track.readyState == 'live' )
					{
						track.stop();
					}
				});
			}

			return {
				stream: stream,
				deviceId: this.getStreamDeviceId(stream)
			};
		})
		.catch((error) => {
			return false;
		});
	}

	getStreamDeviceId ( stream )
	{
		if ( stream )
		{
			return stream.getTracks()[0].getSettings().deviceId;
		}
	}

	async handleFileDecodeChange ( event )
	{
		if ( event.target.files.length == 0 )
		{
			// No file selected, ignore 
			return;
		}

		const fileInput = event.target;
		const label = fileInput.parentElement.querySelector('span');

		// https://github.com/zxing-js/library/issues/545
		// https://developer.mozilla.org/en-US/docs/Web/API/Barcode_Detection_API
		// https://medium.com/js-now/creating-a-real-time-qr-code-scanner-with-vanilla-javascript-part-1-2-creating-the-scanner-a8934ee8f614
		const imageFile = event.target.files[0];
		//const img = document.createElement('img');img.src = imageUrl;
		const imageUrl = URL.createObjectURL(imageFile);
		//document.querySelector('#qrcodeReaderInput').append(img);

		// Scan QR Code
		// https://github.com/zxing-js/library/issues/64
		/*
		const hints = new Map();
		const formats = [
			ZXing.BarcodeFormat.AZTEC,
			ZXing.BarcodeFormat.CODABAR,
			ZXing.BarcodeFormat.CODE_39,
			ZXing.BarcodeFormat.CODE_93,
			ZXing.BarcodeFormat.CODE_128,
			ZXing.BarcodeFormat.DATA_MATRIX,
			ZXing.BarcodeFormat.EAN_8,
			ZXing.BarcodeFormat.EAN_13,
			ZXing.BarcodeFormat.ITF,
			ZXing.BarcodeFormat.MAXICODE,
			ZXing.BarcodeFormat.PDF_417,
			ZXing.BarcodeFormat.QR_CODE,
			ZXing.BarcodeFormat.RSS_14,
			ZXing.BarcodeFormat.RSS_EXPANDED,
			ZXing.BarcodeFormat.UPC_A,
			ZXing.BarcodeFormat.UPC_E,
			ZXing.BarcodeFormat.UPC_EAN_EXTENSION
		];
		hints.set(ZXing.DecodeHintType.POSSIBLE_FORMATS, formats);
		*/
		codeReader = codeReader ?? new ZXing.BrowserQRCodeReader();//new ZXing.BrowserMultiFormatReader(hints);
		const result = await codeReader.decodeFromImageUrl(imageUrl)
		.then((result) => {
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Success ${result}`);
			fileInput.value = null;
			
			// Show scan success
			label.classList.add('bg-success');
			RL.a.animateCSS(label, 'pulse');
			
			// Process the value
			this.game.processPayload(result.text);
		}).catch((error) => {
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Error scanning file. Reason: ${error}`);

			// Show scan error
			label.classList.add('bg-danger');
			RL.a.animateCSS(label, 'fadeIn');
		});

		// Wait, then remove class
		setTimeout(() => {
			RL.a.animateCSS(label, 'fadeOut')
			.then((message) => {
				label.classList.remove('bg-success', 'bg-danger');
			});
		}, 1 * 750);
	}

	async startScanning ( event )
	{
		if ( event )
		{
			event.preventDefault();
			RL.a.animateCSS(event.currentTarget, 'heartBeat');
		}

		const cameraAreaSelector = `.qrcode-reader__container`;
		const cameraArea = document.querySelector(`${cameraAreaSelector}`);
		const $cameraArea = $(`${cameraAreaSelector}`);
		const deviceId = $cameraArea.data('deviceId');

		const video = document.querySelector(`${cameraAreaSelector} video`);
		const videoArea = document.querySelector(`${cameraAreaSelector} .video-area`);
		const videoAreaOverlay = document.querySelector(`${cameraAreaSelector} .video-area-overlay`);
		const videoSpinner = document.querySelector(`${cameraAreaSelector} .video-spinner`);
		
		// Show scanner
		cameraArea.classList.remove('scanner-overlay', 'scanner-overlay--bg-danger', 'scanner-overlay--bg-success');
		cameraArea.classList.add('scanner-overlay');
		RL.a.animateCSS(cameraArea, 'fadeIn').then((message) => {});

		// Show loading
		videoSpinner.classList.remove('d-none');
		RL.a.animateCSS(videoSpinner, 'fadeIn');

		// Show controls
		$('.video-area, .video-controls, .cta--take-screenshot', $cameraArea).removeClass('d-none');

		// Setup code reader
		codeReader = codeReader ?? new ZXing.BrowserQRCodeReader();

		// Setup video
		video.classList.add('d-none');
		// https://www.w3schools.com/tags/ref_av_dom.asp
		video.oncanplay = () => {
			// Fade in video elements and hide loading
			video.classList.remove('d-none');
			RL.a.animateCSS(video, 'fadeIn')
			.then((message) => {
				videoSpinner.classList.add('d-none');
				videoAreaOverlay.classList.remove('d-none');
				RL.a.animateCSS(videoAreaOverlay, 'fadeIn');
			});
			
			// Use video stream and set up controls
			const stream = video.srcObject;
			const track = stream.getVideoTracks()[0];
			const capabilities = track.getCapabilities();
			const settings = track.getSettings();
			
			const autoLight = this.game.getSetting('autoLight', '', '', 'game');
			const autoZoom = this.game.getSetting('autoZoom', '', '', 'game');
			
			let settingKey;
			
			// If camera light, use it
			settingKey = 'torch';
			$(`.cta--toggle-${settingKey}`, $cameraArea)
			.toggleClass('d-none', ! Boolean(capabilities[settingKey]));
			if ( capabilities[settingKey] )
			{
				$(`.cta--toggle-${settingKey}`, $cameraArea).off()
				.on('click', function ( event ) {
					track.applyConstraints({
						advanced: [{[settingKey]: ! settings[settingKey]}]
					})
					.catch(e => console.log(e));
				});
			}

			const ranges = ['zoom', 'brightness'];
			for ( const settingKey of ranges )
			{
				let $videoControl = $(`.video-control-${settingKey}`, $cameraArea);
				$videoControl.toggleClass('d-none', ! Boolean(capabilities[settingKey]));
				if ( capabilities[settingKey] )
				{
					const optionCapabilities = capabilities[settingKey];
					const optionSetting = settings[settingKey];
					const $range = $(`input[type="range"]`, $videoControl);
					$videoControl.find('.setting-value').html(`(${optionSetting})`);
					$range.attr({
						max: optionCapabilities.max,
						min: optionCapabilities.min,
						step: optionCapabilities.step
					}).val(optionSetting);
					$range.off().on('change', function ( event ) {//change,input
						$videoControl.find('.setting-value').html(`(${this.value})`);
						track.applyConstraints({
							advanced: [{[settingKey]: this.value}]
						})
						.catch(e => console.log(e));
					});

					// If zoom, default start to 4
					if ( settingKey == 'zoom' && autoZoom )
					{
						if ( optionCapabilities.min <= 4 && optionCapabilities.max >= 4 )
						{
							$range.val(4).trigger('change');
						}
					}
				}
			}
		};

		const constraints = {
			video: {
				facingMode: 'environment',
				torch: true,
				zoom: true
			}
		};
		const result = await codeReader.decodeOnceFromConstraints(constraints, video)
		.catch((error) => {
			//${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}
			this.game.logger.debug(`decodeOnceFromConstraints: Error: ${error}`);
		});
		// TODO: Possibly check and use preferred or previous device
		//result = await decodeOnce(codeReader, document.querySelector(`${cameraAreaSelector} video`), deviceId);
		$cameraArea.data('deviceId', this.game.ui.camera.getStreamDeviceId(video.srcObject));
		
		// If no result, error
		if ( ! result )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Error with permissions or scan. ${result}`);

			// Show scan error
			cameraArea.classList.add('scanner-overlay--bg-danger');
		}
		else
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Code found: ${result}`);

			// Show scan success
			cameraArea.classList.add('scanner-overlay--bg-success');
			RL.a.animateCSS(videoArea, 'rubberBand')
			.then((message) => {
				// Stop decoding
				this.game.ui.camera.stopScanning();
				
				this.game.processPayload(result.text);
			});
		}
	}

	async stopScanning ( event )
	{
		if ( event )
		{
			event.preventDefault();
		}
		
		const cameraAreaSelector = `.qrcode-reader__container`;
		const $cameraArea = $(`${cameraAreaSelector}`);
		const cameraArea = document.querySelector(`${cameraAreaSelector}`);
		const video = document.querySelector(`${cameraAreaSelector} video`);
		const videoArea = document.querySelector(`${cameraAreaSelector} .video-area`);
		const videoAreaOverlay = document.querySelector(`${cameraAreaSelector} .video-area-overlay`);
		const videoSpinner = document.querySelector(`${cameraAreaSelector} .video-spinner`);

		// If clicked on, show error state
		if ( event )
		{
			cameraArea.classList.add('scanner-overlay--bg-danger');
			let result = await RL.a.animateCSS(videoArea, 'headShake');
		}

		// Hide scanner
		RL.a.animateCSS(cameraArea, 'fadeOut')
		.then((message) => {
			// Reset video elements
			cameraArea.classList.remove('scanner-overlay', 'scanner-overlay--bg-danger', 'scanner-overlay--bg-success');

			videoAreaOverlay.classList.add('d-none');
			videoSpinner.classList.add('d-none');
			video.classList.add('d-none');

			// Hide video controls
			$('.video-controls, .video-controls btn, .video-controls [class*="video-control-"]', cameraAreaSelector).addClass('d-none');

			// If code reader available, stop it and clean video
			if ( codeReader )
			{
				codeReader.reset();
				// Remove all event listeners
				// https://bobbyhadz.com/blog/javascript-remove-all-event-listeners-from-element
				video.replaceWith(video.cloneNode(true));
			}
		});

	}

}