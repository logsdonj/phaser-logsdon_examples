/**
 * 
 */
class PlayerUi
{
	// Properties

	className = 'PlayerUi';
	updateHudDebounced;

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.updateHudDebounced = RL.u.debounce(this.updateHud.bind(this), this.game.ui.debounceDelay);
    }

	updateHud ()
	{
		this.game.logger.debug(`${this.className} ${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);

		this.game.player.ui.updateStatsHud();
	}

	// Update player HUD
	updateStatsHud ()
	{
		this.game.logger.debug(`${this.className} ${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);

		jQuery('.player-stat-health').html(this.game.player.stats.health);
		jQuery('.player-stat-exp').html(this.game.player.stats.exp);
		jQuery('.player-stat-special').html(this.game.player.stats.special);
		jQuery('.player-stat-currency').html(this.game.player.stats.currency);
		jQuery('.player-stat-points').html(this.game.player.stats.points);
		
		// Trigger event
		this.game.player.events.trigger(this.game.player.events.names.updateHud);
	}

}