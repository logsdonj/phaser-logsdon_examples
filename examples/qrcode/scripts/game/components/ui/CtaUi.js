/**
 * 
 */
class CtaUi
{
	// Properties

	className = 'CtaUi';

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
    }

	// Event handlers

	handleHint ( id, record, hint, nextHintIndex, hints, bypassQueue )
	{
		// Show the hint
		this.game.queueAction({type: 'toast', params: {
			body: `${hint} <i>&mdash; ${nextHintIndex + 1} of ${hints.length}</i>`, 
			type: 'toastTemplateColor'
		}}, true, bypassQueue);
	}
	
	handleHudRefreshClick ( event )
	{
		event.preventDefault();
		this.game.ui.updateHudsDebounced();
	}

	handleGameStartClick ( event )
	{
		// If not debugging, prevent default behavior
		if ( ! this.game.debug )
		{
			event.preventDefault();
		}

		// Check for automatic fullscreen handling
		const autoFullscreen = this.game.getSetting('autoFullscreen', '', '', 'game');
		if ( autoFullscreen )
		{
			RL.fullscreen.toggle(true);
		}

		// Prevent double-click
		const targetElement = event.currentTarget;
		targetElement.classList.add('pe-none');

		RL.a.animateCSS(targetElement, 'pulse')
		.then(message => {
			// Unprevent double-click
			targetElement.classList.remove('pe-none');

			// If game isn't started, start it
			if ( ! this.game.isStarted )
			{
				this.game.start();
			}
		
			this.game.ui.resizeStatusContainerForScrollDebounced();
			
			// Continue
			this.game.ui.nav.showScreen('main', 'title', '', () => {
				// If game already started, update CTAs
				if ( this.game.isStarted )
				{
					// Change start button text to continue
					let $startButton = $('button.cta--game-start');
					let $startIcon = $startButton.find('i').clone();
					$startButton.html(`${RL.u.capitalize(this.game.getLexicon('continue', '', '', 'game'))}`).prepend($startIcon);
				}
			});
		});
	}

	// Handle component add
	handleComponentAddClick ( event )
	{
		event.preventDefault();

		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		const targetElement = event.currentTarget;

		// Animate
		RL.a.animateCSS(targetElement, 'heartBeat');
		// Activate
		let componentType = targetElement.getAttribute('data-component-type');
		let componentKey = targetElement.getAttribute('data-component-key');
		let bypassQueue = targetElement.getAttribute('data-bypass-queue');
		this.game[componentType].add(componentKey, {bypassQueue: bypassQueue});
	}

	// Handle component activation
	handleComponentActivateClick ( event )
	{
		event.preventDefault();

		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		// Close the game screen
		//closeClosestGameScreen(this);

		const targetElement = event.currentTarget;

		// Animate
		RL.a.animateCSS(targetElement, 'heartBeat');
		// Activate
		let componentType = targetElement.getAttribute('data-component-type');
		let componentKey = targetElement.getAttribute('data-component-key');
		let bypassQueue = targetElement.getAttribute('data-bypass-queue');
		this.game[componentType].activate(componentKey, {bypassQueue: bypassQueue});
	}

	// Handle component deactivation
	handleComponentDeactivateClick ( event ) {
		event.preventDefault();
		const targetElement = event.currentTarget;
		// Animate
		RL.a.animateCSS(targetElement, 'headShake');
		// Deactivate
		let componentType = targetElement.getAttribute('data-component-type');
		let componentKey = targetElement.getAttribute('data-component-key');
		let bypassQueue = targetElement.getAttribute('data-bypass-queue');
		this.game[componentType].deactivate(componentKey, {bypassQueue: bypassQueue});
	}

	// Handle component use
	handleComponentUseClick ( event )
	{
		event.preventDefault();
		
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		const targetElement = event.currentTarget;

		// Animate
		RL.a.animateCSS(targetElement, 'rubberBand');
		// Use
		let componentType = targetElement.getAttribute('data-component-type');
		let componentKey = targetElement.getAttribute('data-component-key');
		let bypassQueue = targetElement.getAttribute('data-bypass-queue');
		this.game[componentType].use(componentKey, {bypassQueue: bypassQueue});
	}

	// Handle component restart
	handleComponentRestartClick ( event )
	{
		event.preventDefault();
		
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		const targetElement = event.currentTarget;

		// Animate
		RL.a.animateCSS(targetElement, 'wobble');
		// Restart
		let componentType = targetElement.getAttribute('data-component-type');
		let componentKey = targetElement.getAttribute('data-component-key');
		let bypassQueue = targetElement.getAttribute('data-bypass-queue');
		this.game[componentType].restart(componentKey, {bypassQueue: bypassQueue});
	}

	// Handle component info
	handleComponentInfoClick ( event )
	{
		event.preventDefault();
		const targetElement = event.currentTarget;
		// Animate
		RL.a.animateCSS(targetElement, 'bounce');
		// Info
		let componentType = targetElement.getAttribute('data-component-type');
		let componentKey = targetElement.getAttribute('data-component-key');
		let bypassQueue = targetElement.getAttribute('data-bypass-queue');
		this.game.ui.componentInfo.showModal(componentType, componentKey, {bypassQueue: bypassQueue});
	}

	// Handle component hint
	handleComponentHintClick ( event )
	{
		event.preventDefault();
		const targetElement = event.currentTarget;
		// Animate
		RL.a.animateCSS(targetElement, 'tada');
		// Hint
		let componentType = targetElement.getAttribute('data-component-type');
		let componentKey = targetElement.getAttribute('data-component-key');
		let bypassQueue = targetElement.getAttribute('data-bypass-queue');
		this.game.showHint(componentType, componentKey, bypassQueue);
	}

	// Handle component combine
	handleComponentCombineClick ( event )
	{
		event.preventDefault();
		
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		const targetElement = event.currentTarget;
		let bypassQueue = targetElement.getAttribute('data-bypass-queue');

		// Animate
		RL.a.animateCSS(targetElement, 'rubberBand');
		
		// If no combinations to check, stop
		if ( ! this.game.database.combinations )
		{
			this.game.logger.debug(`No combinations are available in the game.`);

			let componentNoun = this.game.getLexicon('combination', '', '', 'combination');

			this.game.queueAction({type: 'toast', params: {
				body: `No ${RL.u.wordPluralizeSuffix(componentNoun, 0)} are available.`,
				type: 'toastTemplateColor',
				color: 'danger'
			}}, true, bypassQueue);

			return;
		}

		// Get the game screen reference
		let $gameScreen = $(targetElement).closest('.game-screen');

		// If nothing checked, stop
		let componentType = $gameScreen.find('input[name="combineComponentType\[\]"]')[0].value;
		let $checked = $gameScreen.find('input[name="combineKey\[\]"]:checked');
		if ( ! $checked.length )
		{
			this.game.logger.debug(`No items are selected to combine.`);

			let componentNoun = this.game.getLexicon(componentType, '', '', componentType);
			let combineVerb = this.game.getVerb('combine', '', '', '', 'combination');

			this.game.queueAction({type: 'toast', params: {
				body: `No ${RL.u.wordPluralizeSuffix(componentNoun, 0)} are selected to ${combineVerb}.`,
				type: 'toastTemplateColor',
				color: 'danger'
			}}, true, bypassQueue);

			return;
		}

		// Gather the selected combine information
		let combineKeys = [];
		let combineOrders = [];
		let combineKeyAmounts = [];
		$gameScreen.find('input[name="combineKey\[\]"]:checked')
		.each((index, el) => {
			combineKeys.push(el.value);
			const $ctaContainer = $(el).closest('.list-group-item__cta-container');
			const $order = $ctaContainer.find('input[name="combineOrder\[\]"]');
			combineOrders.push($order.val());
			const $stock = $ctaContainer.find('select[name="combineKeyAmount\[\]"]');
			combineKeyAmounts.push($stock.val());
		});

		let combinationMatchKey;
		// Go through combinations for matches
		let combinationKeys = Object.keys(this.game.database.combinations);
		for ( let combinationKey of combinationKeys )
		{
			let combination = this.game.database.combinations[combinationKey];

			// If no items to check against, next
			if ( ! combination.items )
			{
				continue;
			}
			
			let combinationRecordKeys = Object.keys(combination.items);
			
			// If not the same number of items to compare, next
			if ( combineKeys.length !== combinationRecordKeys.length )
			{
				continue;
			}

			// Default to combinable
			let combinable = true;
			// For each item needed
			for ( let combinationRecordKey of combinationRecordKeys )
			{
				// If it wasn't selected, stop
				let combinationRecordIndex = combineKeys.indexOf(combinationRecordKey);
				if ( combinationRecordIndex === -1 )
				{
					combinable = false;
					break;
				}

				let combinationRecord = combination.items[combinationRecordKey];

				// Check inventory stock
				if ( Reflect.has(combinationRecord, 'stock') )
				{
					// If the selected and combinable stocks don't match, stop
					if ( combineKeyAmounts[combinationRecordIndex] != combinationRecord.stock )
					{
						combinable = false;
						break;
					}

					// Double-check if the inventory doesn't have enough stock, stop
					let inventoryItem = this.game.item.inventory[combinationRecordKey];
					if ( inventoryItem.stock < combinationRecord.stock )
					{
						combinable = false;
						break;
					}
				}

				// Check order
				if ( Reflect.has(combinationRecord, 'order') )
				{
					// If the selected and combinable orders don't match, stop
					if ( combineOrders[combinationRecordIndex] != combinationRecord.order )
					{
						combinable = false;
						break;
					}
				}
			}

			// If combinable after checks, stop
			if ( combinable )
			{
				combinationMatchKey = combinationKey;
				break;
			}
		}

		// If combinable
		if ( combinationMatchKey )
		{
			let combination = this.game.database.combinations[combinationMatchKey];
			// Reduce inventory or remove item
			let combinationRecordKeys = Object.keys(combination.items);
			// For each item needed
			for ( let combinationRecordKey of combinationRecordKeys )
			{
				// Reduce stock
				let combinationRecord = combination.items[combinationRecordKey];
				let inventoryItem = this.game.item.inventory[combinationRecordKey];
				let combinationRecordIndex = combineKeys.indexOf(combinationRecordKey);
				let stockReduction = combineKeyAmounts[combinationRecordIndex];

				// If item stock is updatable, update it
				if ( this.game.item.depletesStock(inventoryItem) )
				{
					inventoryItem.stock -= stockReduction;
				}

				// If removing or no more stock, stock is updatable, and flagged to remove if empty, remove it
				if ( 
					combinationRecord.remove ||
					(
						inventoryItem.stock <= 0 && 
						combinationRecord.removeIfEmpty 
					)
				)
				{
					this.game.item.remove(combinationRecordKey);
				}
			}

			// Add to ledger
			this.game.ledger.add({
				type: 'item',
				action: 'combination',
				id: combinationMatchKey
			});

			let componentNoun = this.game.getLexicon('item', '', '', 'item');
			let combineVerb = this.game.getVerb('combine', 'past', '', '', 'combination');
			
			this.game.queueAction({type: 'toast', params: {
				body: `${RL.u.capitalize(RL.u.wordPluralizeSuffix(componentNoun, 0))} ${combineVerb}.`,
				type: 'toastTemplateColor',
				color: 'success'
			}}, true, bypassQueue);
			
			// Process payload
			if ( combination.payload )
			{
				this.game.queueAction({type: 'payload', params: combination.payload}, true, bypassQueue);
			}
		}
		else
		{
			this.game.logger.debug(`Nothing could be created from that combination`);

			let componentNoun = this.game.getLexicon('combination', '', '', 'combination');

			this.game.queueAction({type: 'toast', params: {
				body: `Nothing could be created from that ${componentNoun}.`,
				type: 'toastTemplateColor',
				color: 'danger'
			}}, true, bypassQueue);
		}
	}

}