/**
 * 
 */
class ItemUi
{
	// Properties

	className = 'ItemUi';
	updateHudDebounced;

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.updateHudDebounced = RL.u.debounce(this.updateHud.bind(this), this.game.ui.debounceDelay);
    }

	// Update inventory screen listings
	updateHud ()
	{
		this.game.logger.debug(`${this.className} ${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);

		let collectorMode = this.game.getSetting('collectorMode', '', '', 'game');
		let actionMode = this.game.getSetting('actionMode', '', '', 'game');
		let hudMode = this.game.getSetting('hudMode', '', '', 'item');
		let componentType = 'item';
		
		// Get item status element reference
		let $itemsStatusContainer = $('.status-container--items');
		$itemsStatusContainer.addClass('d-none');
		let $itemsStatus = jQuery('.items-status');
		$itemsStatus.empty();

		let $activatedItemsStatusContainer = $('.status-container--activated-items');
		$activatedItemsStatusContainer.addClass('d-none');
		let $activatedItemsStatus = $('.activated-items-status');
		$activatedItemsStatus.empty();

		// Reset the list of items on the items screen
		let $inventoryList = jQuery('.inventory-list');
		$inventoryList.empty();

		// Sort into array of ids
		let itemIds;
		// If collector, list out all records
		if ( collectorMode )
		{
			itemIds = RL.u.sortObjectBy(this.game.database.items, ['type', 'name'], true);
			// Filter out non-inventory
			itemIds = itemIds.filter(itemId => this.game.database.items[itemId].isInventory);
		}
		else
		{
			itemIds = RL.u.sortObjectBy(this.game.item.inventory, ['type', 'name'], true);
		}

		// For each item in inventory
		//let itemIds = Object.keys(this.game.item.inventory); itemIds.forEach(itemId => { // This was used before pre-sorting
		itemIds.forEach(itemId => {
			// List out item information
			let isAdded = Reflect.has(this.game.item.inventory, itemId);

			let isCombination = false;
			if ( this.game.database?.combinations )
			{
				const combinationKeys = Object.keys(this.game.database.combinations);
				for ( const combinationKey of combinationKeys )
				{
					const combination = this.game.database.combinations[combinationKey];
					if ( combination?.payload?.items && Reflect.has(combination.payload.items, itemId) )
					{
						isCombination = true;
						break;
					}
				}
			}

			// Start template
			let $template = $(component__listGroupItemTemplate)
			.appendTo($inventoryList)
			.attr({
				'data-component-type': 'item',
				'data-component-key': itemId,
				'data-item-id': itemId
			});

			// Hiding all areas
			$template.find('[class*="list-group-item__"]').addClass('d-none');

			// If collector mode and not in inventory, show missing
			if ( collectorMode && ! isAdded )
			{
				// Get record
				let dbItem = this.game.database.items[itemId];

				// Update content
				$template.find('.list-group-item__heading')
				.html('???')//dbItem.name
				.addClass('text-muted')
				.removeClass('d-none');
				
				$template.find('.list-group-item__type')
				.html(`<i class="fa-solid fa-triangle-exclamation fa-fw me-1"></i>Missing`)
				.removeClass('d-none');

				/*
				let firstCol = RL.u.getRandomInt(2, 9);
				let secondCol = 11 - firstCol;
				$template.find('.list-group-item__notes')
				.html(`<span class="placeholder col-${firstCol}"></span>&nbsp;<span class="placeholder col-${secondCol}"></span>`)
				.addClass('placeholder-glow')
				.removeClass('d-none');
				*/
			}
			else
			{
				// Get record
				let inventoryItem = this.game.item.inventory[itemId];

				// Update content
				$template.find('.list-group-item__heading')
				.html(inventoryItem.name)
				.removeClass('d-none');
				
				$template.find('.list-group-item__type')
				.html(`${RL.u.capitalize(inventoryItem.type ?? '')}`)
				.removeClass('d-none');

				if ( hudMode == 'minimal' )
				{
					$template.find('.list-group-item__notes')
					.html(inventoryItem.description)
					.toggleClass('d-none', ! inventoryItem.description);
				}
				else
				{
					$template.find('.list-group-item__description')
					.html(inventoryItem.description)
					.toggleClass('d-none', ! inventoryItem.description);
					
					$template.find('.list-group-item__notes')
					.html(inventoryItem.notes)
					.toggleClass('d-none', ! inventoryItem.notes);
				}

				// Update CTAs
				$template.find('.list-group-item__cta-container')
				.removeClass('d-none')
				.find('a, [type="button"]').attr({
					'data-item-id': itemId,
					'data-component-key': itemId,
					'data-component-type': 'item'
				});
				
				// Update CTA language and hide
				let ctas = ['add', 'activate', 'deactivate', 'use', 'restart', 'hint', 'info'];
				let ctaClassRef = {};
				for ( let cta of ctas )
				{
					let ctaClass = `.cta--component-${cta}`;
					ctaClassRef[cta] = ctaClass;
					let verb = RL.u.capitalize(this.game.getVerb(cta, '', '', inventoryItem, componentType));
					$template.find(ctaClass).html(verb).addClass('d-none');
				}

				// If is added, show info
				if ( isAdded )
				{
					// Update CTAs
					$template.find(ctaClassRef['info'])
					.removeClass('d-none');
				}

				// If hints, set them up
				if ( Reflect.has(inventoryItem, 'hints') )
				{
					let $component__badge_hintAmount = $(component__badge_hintAmount)
					.html(Array.isArray(inventoryItem.hints) ? inventoryItem.hints.length : 1);
					
					// Update CTAs
					$template.find(ctaClassRef['hint'])
					.append($component__badge_hintAmount)
					.attr('data-bypass-queue', 'true')
					.removeClass('d-none');
				}

				// If tracking as inventory
				if ( inventoryItem.isInventory )
				{
					let depletesStock;
					let stock;

					// If item uses stock, set up display
					if ( Reflect.has(inventoryItem, 'stock') )
					{
						depletesStock = this.game.item.depletesStock(inventoryItem);
						stock = ( depletesStock ) ? inventoryItem.stock : '<i class="icon-infinity icon--fa icon--fw"></i>';
						
						// Update content
						$template.find('.list-group-item__stock')
						.html(stock)
						.removeClass('d-none');

						// Update CTA
						let $component__badge_stockAmount = $(component__badge_stockAmount).html(stock);

						$template.find(ctaClassRef['use'])
						.append($component__badge_stockAmount);
						
						$template.find('.badge--stock-amount').html(stock);
						
						// If item is used up, disable CTAs
						if ( inventoryItem.stock <= 0 )
						{
							$template.find(ctaClassRef['use']).addClass('disabled').attr({'aria-disabled': 'true', 'disabled': true});
						}
					}

					// If activated
					if ( inventoryItem.active )
					{
						// Show active
						$template.addClass('active').attr('aria-current', true);
						// Update CTAs
						$template.find([ctaClassRef['use'], ctaClassRef['deactivate']].join(',')).removeClass('d-none');
						
						// Update inventory status
						//$itemsStatus.html(`${$itemsStatus.html()}, ${inventoryItem.name}`);
						
						// Create active item for use on Main screen
						let $activatedItemTemplate = $(activatedInventoryItemButtonTemplate)
						.appendTo($activatedItemsStatus).attr({
							'data-component-type': 'item', 
							'data-component-key': itemId
						});
						$activatedItemTemplate.find('.activated-item__cta-title')
						.html(`${inventoryItem.name} `);
						$activatedItemTemplate.find('.badge--stock-amount').html(stock ?? '');
						// If item is used up, disable CTAs
						if ( inventoryItem.stock <= 0 )
						{
							$activatedItemTemplate.addClass('disabled').attr({'aria-disabled': 'true', 'disabled': true});
						}
					}
					// Else available to activate
					else
					{
						// Update CTAs
						$template.find(ctaClassRef['activate']).removeClass('d-none');
					}
					
					// If combinations available, set up data
					if ( this.game.database.combinations )
					{
						// Show input
						$template.find('.list-group-item__combinable-input-group').removeClass('d-none');

						// Set item value
						$template.find('input[name="combineComponentType\[\]"]').val('item');
						$template.find('input[name="combineKey\[\]"]').val(itemId).on('change', ( event ) => {
							const $this = $(event.currentTarget);
							const checked = event.currentTarget.checked;
							const numChecked = $template.closest('.list-group').find('input[name="combineKey\[\]"]:checked').length;
							const order = checked ? numChecked : '';
							// Update display
							$template.find('.list-group-item__combinable-input-group-order-container').toggleClass('d-none');
							$template.find('.list-group-item__combinable-input-group-order').removeClass('d-none').html(order);
							// Update value
							$template.find('input[name="combineOrder\[\]"]').val(order);
						});
			
						// Set up stock dropdown
						const $amountSelect = $template.find('select[name="combineKeyAmount\[\]"]');
						$amountSelect.empty();
						let maxAmount = inventoryItem.stock;
						for ( let amountIndex = 0; amountIndex <= maxAmount; amountIndex++ )
						{
							$amountSelect.append(`<option${amountIndex === 1 ? ' selected' : ''}>${amountIndex}</option>`);
						}
					}
				}

				// If not usable, hide CTAs
				if ( Reflect.has(inventoryItem, 'isUsable') && ! inventoryItem.isUsable )
				{
					$template.find([ctaClassRef['use'], ctaClassRef['activate'], ctaClassRef['deactivate']].join(',')).addClass('d-none');
				}
			}
			
			// Add combinable icon to type
			if ( isCombination )
			{
				$template.find('.list-group-item__type')
				.prepend(`<i class="fa-solid fa-screwdriver-wrench fa-fw me-1"></i>`);
			}

			// If there is a status, show the container
			if ( $activatedItemsStatus.length && $activatedItemsStatus.html().length )
			{
				$activatedItemsStatusContainer.removeClass('d-none');
			}
		});

		// If no items, note it
		if ( ! itemIds.length )
		{
			let componentNoun = this.game.getLexicon('item', '', '', 'item');
			let $template = $(component__listGroupItemTemplate).appendTo($inventoryList)
			.html(`No ${RL.u.wordPluralizeSuffix(componentNoun, 0)} are available.`);
		}
		else
		{
			// If combinations available, add combinable CTA
			if ( this.game.database.combinations )
			{
				let $withSelected = $(component__withSelectedContainer);
				let combineVerb = RL.u.capitalize(this.game.getVerb('combine', '', '', '', 'combination'));
				$withSelected.find('.cta--component-combine').html(`${combineVerb}`);
				let $withSelectedContainer = $('.game-screen__with-selected-container');
				$withSelectedContainer.empty().append(component__withSelectedContainer);
			}
		}

		// Update game icon stat
		$('.stat--num-inventory-items').html(itemIds.length);
		
		// Trigger event
		this.game.item.events.trigger(this.game.item.events.names.updateHud);
	}

	// Event handlers

	handleAdd ( itemId, item, newRecord, requiredForChallenges, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		// If error
		if ( params.error )
		{
			this.game.queueAction({type: 'toast', params: {
				title: `Error`,
				body: `${params.error}`,
				color: 'danger',
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
			return;
		}

		this.game.logger.debug(`${item.name} added!`);
		
		// If tracking item inventory
		if ( item.isInventory )
		{
			this.game.item.ui.updateHudDebounced();
		}

		let notifyAdd = this.game.getSetting('notifyAdd', params, item, 'item');

		// If the first time this item is found, show info
		if ( newRecord )
		{
			let showcase = this.game.getSetting('showcase', params, item, 'item');

			let componentNoun = this.game.getLexicon('item', params, item, 'item');
			let inventoryNoun = this.game.getLexicon('item', params, item, 'inventory');
			let addVerb = this.game.getVerb('add', 'past', params, item, 'item');

			// If showcasing
			if ( showcase )
			{
				this.game.showcaseComponent({
					title: `New ${RL.u.capitalize(componentNoun)}${item.isInventory ? ` ${RL.u.capitalize(addVerb)}` : ''}`, 
					name: item.name,
					image: item.image,
					description: item.description,
					component: item,
					componentKey: itemId,
					componentType: 'item'
				}, true, params?.bypassQueue);

				/*
				this.game.queueAction({type: 'modal', params: {
					title: `<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>New Item!`, 
					body: `${item.name}!${item.isInventory ? ` You can find this item in your ${inventoryNoun}.` : ` You just used it.`}`,
					cancelButtonHide: true,
					confirmButtonLabel: 'Continue'
				}}, true, params?.bypassQueue);
				*/
			}
			// If item is inventoriable, notify
			else if ( notifyAdd && item.isInventory )
			{
				let activateVerb = RL.u.capitalize(this.game.getVerb('activate', '', params, item, 'item'));

				this.game.queueAction({type: 'toast', params: {
					title: `<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>New ${RL.u.capitalize(componentNoun)}`,
					body: `${item.name} is in your ${inventoryNoun}.`,
					ctas: {
						primary: {
							attrObj: {
								'data-component-type': 'item',
								'data-component-key': itemId
							},
							classAddon: ( item.activateOnAdd || (Reflect.has(item, 'isUsable') && ! item.isUsable) ) ? 'd-none' : 'cta--component-activate',
							label: activateVerb
						}
					},
					type: 'toastTemplate'
				}}, true, params?.bypassQueue);
			}
		}
		// If notify add, do it
		else if ( notifyAdd )
		{
			let findVerb = this.game.getVerb('find', 'past', params, item, 'item');

			this.game.queueAction({type: 'toast', params: {
				body: `${item.name} ${findVerb}.`,
				color: 'success',
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		/*
		// If not needed, note it
		if ( ! requiredForChallenges )
		{
			this.game.queueAction({type: 'toast', params: {
				body: `That's great, but it's not required for any active challenges.`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}
		*/
	}

	handleActivate ( itemId, item, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${item.name} activated!`);
		
		let notifyActivate = this.game.getSetting('notifyActivate', params, item, 'item');
		if ( notifyActivate )
		{
			let activateVerb = this.game.getVerb('activate', 'past', params, item, 'item');
			
			this.game.queueAction({type: 'toast', params: {
				body: `${item.name} ${activateVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		this.game.ui.updateHudsDebounced();
	}

	handleDeactivate ( itemId, item, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${item.name} deactivated!`);
		
		let notifyDeactivate = this.game.getSetting('notifyDeactivate', params, item, 'item');
		if ( notifyDeactivate )
		{
			let deactivateVerb = this.game.getVerb('deactivate', 'past', params, item, 'item');
		
			this.game.queueAction({type: 'toast', params: {
				body: `${item.name} ${deactivateVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		this.game.ui.updateHudsDebounced();
	}

	handleUse ( itemId, item, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${item.name} used!`);
		
		let notifyUse = this.game.getSetting('notifyUse', params, item, 'item');
		if ( notifyUse )
		{
			let useVerb = this.game.getVerb('use', 'past', params, item, 'item');
		
			this.game.queueAction({type: 'toast', params: {
				body: `${item.name} ${useVerb}!`, 
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}
		
		// If tracking item inventory
		if ( item.isInventory )
		{
			this.game.ui.updateHudsDebounced();
		}
	}

	handleAddThrottle ( itemId, item, params )
	{
		let throttleTimeLeft = item.addThrottleMs - (Date.now() - item.lastAddTime);
		let throttleTimeLeftSeconds = (throttleTimeLeft / 1000).toFixed(2);
		this.game.logger.debug(`${item.name} add throttled for ${throttleTimeLeftSeconds} more seconds!`);
		
		let notifyAddThrottle = this.game.getSetting('notifyAddThrottle', params, item, 'item');
		if ( notifyAddThrottle )
		{
			this.game.queueAction({type: 'toast', params: {
				body: `You must wait ${throttleTimeLeftSeconds} more seconds before adding another ${item.name}.`, 
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}
	}

	handleUseThrottle ( itemId, item, params )
	{
		let throttleTimeLeft = item.useThrottleMs - (Date.now() - item.lastUseTime);
		let throttleTimeLeftSeconds = (throttleTimeLeft / 1000).toFixed(2);
		this.game.logger.debug(`${item.name} use throttled for ${throttleTimeLeftSeconds} more seconds!`);
		
		let notifyUseThrottle = this.game.getSetting('notifyUseThrottle', params, item, 'item');
		if ( notifyUseThrottle )
		{
			this.game.queueAction({type: 'toast', params: {
				body: `You must wait ${throttleTimeLeftSeconds} more seconds before using another ${item.name}.`, 
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}
	}

	handleMaxStock ( itemId, item, params )
	{
		this.game.logger.debug(`${item.name} at ${item.maxStock} max stock!`);
		
		let notifyMaxStock = this.game.getSetting('notifyMaxStock', params, item, 'item');
		if ( notifyMaxStock )
		{
			this.game.queueAction({type: 'toast', params: {
				body: `You already have the max stock of ${item.maxStock} for ${item.name}.`, 
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}
	}

}