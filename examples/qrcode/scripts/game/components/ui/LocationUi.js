/**
 * 
 */
class LocationUi
{
	// Properties

	className = 'LocationUi';
	updateHudDebounced;

	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.updateHudDebounced = RL.u.debounce(this.updateHud.bind(this), this.game.ui.debounceDelay);
    }

	// Update locations HUD
	updateHud ()
	{
		this.game.logger.debug(`${this.className} ${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}`);

		let collectorMode = this.game.getSetting('collectorMode', '', '', 'game');
		let actionMode = this.game.getSetting('actionMode', '', '', 'game');
		let hudMode = this.game.getSetting('hudMode', '', '', 'location');
		let componentType = 'location';
		
		// Get location status element reference
		let $locationsStatusContainer = $('.status-container--locations');
		$locationsStatusContainer.addClass('d-none');
		let $locationsStatus = jQuery('.locations-status');
		$locationsStatus.empty();
		
		// If a location is active, note it
		if ( $locationsStatus.length && this.game.location.active )
		{
			$locationsStatus.html(`You are in ${this.game.location.active.name}.`).removeClass('d-none');
		}

		// Reset the list of locations
		let $locationsList = jQuery('.locations-list');
		$locationsList.empty();
		
		// Sort into array of ids
		// If collector, list out all records
		let locationIds = RL.u.sortObjectBy(this.game[collectorMode ? 'database' : 'location'].locations, ['type', 'name'], true);

		// For each location of the game
		//let locationIds = Object.keys(this.game.location.locations);
		locationIds.forEach(locationId => {
			// List out location information
			let isAdded = Reflect.has(this.game.location.locations, locationId);

			let isCombination = false;
			if ( this.game.database?.combinations )
			{
				const combinationKeys = Object.keys(this.game.database.combinations);
				for ( const combinationKey of combinationKeys )
				{
					const combination = this.game.database.combinations[combinationKey];
					if ( combination?.payload?.locations && Reflect.has(combination.payload.locations, locationId) )
					{
						isCombination = true;
						break;
					}
				}
			}

			// Start template
			let $template = $(component__listGroupItemTemplate)
			.appendTo($locationsList)
			.attr('data-location-id', locationId);

			// Hiding all areas
			$template.find('[class*="list-group-item__"]').addClass('d-none');

			// If collector mode and not in inventory, show missing
			if ( collectorMode && ! isAdded )
			{
				// Get record
				let dbLocation = this.game.database.locations[locationId];

				// Update content
				$template.find('.list-group-item__heading')
				.html('???')//dbLocation.name
				.addClass('text-muted')
				.removeClass('d-none');

				$template.find('.list-group-item__type')
				.html(`<i class="fa-solid fa-triangle-exclamation fa-fw me-1"></i>Missing`)
				.removeClass('d-none');

				/*
				let firstCol = RL.u.getRandomInt(2, 9);
				let secondCol = 11 - firstCol;
				$template.find('.list-group-item__notes')
				.html(`<span class="placeholder col-${firstCol}"></span>&nbsp;<span class="placeholder col-${secondCol}"></span>`)
				.addClass('placeholder-glow')
				.removeClass('d-none');
				*/
			}
			else
			{
				// Get record
				let location = this.game.location.locations[locationId];

				// Update content
				$template.find('.list-group-item__heading')
				.html(location.name)
				.removeClass('d-none');

				$template.find('.list-group-item__type')
				.html(`${RL.u.capitalize(location.type ?? '')}`)
				.removeClass('d-none');

				if ( hudMode == 'minimal' )
				{
					$template.find('.list-group-item__notes')
					.html(location.description)
					.toggleClass('d-none', ! location.description);
				}
				else
				{
					$template.find('.list-group-item__description')
					.html(location.description)
					.toggleClass('d-none', ! location.description);
					
					$template.find('.list-group-item__notes')
					.html(location.notes)
					.toggleClass('d-none', ! location.notes);
				}
				
				// Update CTAs
				$template.find('.list-group-item__cta-container')
				.removeClass('d-none')
				.find('a, [type="button"]').attr({
					'data-location-id': locationId,
					'data-component-key': locationId,
					'data-component-type': 'location'
				});
				
				// Update CTA language and hide
				let ctas = ['add', 'activate', 'deactivate', 'use', 'restart', 'hint', 'info'];
				let ctaClassRef = {};
				for ( let cta of ctas )
				{
					let ctaClass = `.cta--component-${cta}`;
					ctaClassRef[cta] = ctaClass;
					let verb = RL.u.capitalize(this.game.getVerb(cta, '', '', location, componentType));
					$template.find(ctaClass).html(verb).addClass('d-none');
				}
		
				// If is added, show info
				if ( isAdded )
				{
					// Update CTAs
					$template.find(ctaClassRef['info'])
					.removeClass('d-none');
				}
		
				// If hints, set them up
				if ( Reflect.has(location, 'hints') )
				{
					let $component__badge_hintAmount = $(component__badge_hintAmount)
					.html(Array.isArray(location.hints) ? location.hints.length : 1);
					
					// Update CTAs
					$template.find(ctaClassRef['hint'])
					.append($component__badge_hintAmount)
					.attr('data-bypass-queue', 'true')
					.removeClass('d-none');
				}
				
				// If location is active
				if ( this.game.location.active && this.game.location.active.id == locationId )
				{
					// Show active
					$template.addClass('active').attr('aria-current', true);
					// Update CTAs
					$template.find([ctaClassRef['use'], ctaClassRef['deactivate']].join(',')).removeClass('d-none');
				}
				else
				{
					// Update CTAs
					$template.find(ctaClassRef['activate']).removeClass('d-none');
				}
			}
			
			// Add combinable icon to type
			if ( isCombination )
			{
				$template.find('.list-group-item__type')
				.prepend(`<i class="fa-solid fa-screwdriver-wrench fa-fw me-1"></i>`);
			}
		});

		// If no locations, note it
		if ( ! locationIds.length )
		{
			let componentNoun = this.game.getLexicon('location', '', '', 'location');
			let $template = $(component__listGroupItemTemplate).appendTo($locationsList)
			.html(`No ${RL.u.wordPluralizeSuffix(componentNoun, 0)} are available.`);
		}

		// If there is a status, show the container
		if ( $locationsStatus.length && $locationsStatus.html().length && $locationsStatusContainer.length )
		{
			$locationsStatusContainer.removeClass('d-none');
		}

		// Update game icon stat
		$('.stat--num-available-locations').html(locationIds.length);
		
		// Trigger event
		this.game.location.events.trigger(this.game.location.events.names.updateHud);
	}

	// Event handlers

	handleAdd ( locationId, location, newRecord, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${location.name} added!`);
		let notifyAdd = this.game.getSetting('notifyAdd', params, location, 'location');
		this.game.location.ui.updateHudDebounced();

		// If the first time this item is found, show info
		if ( newRecord )
		{
			let showcase = this.game.getSetting('showcase', params, location, 'location');

			let componentNoun = this.game.getLexicon('location', params, location, 'location');
			let addVerb = this.game.getVerb('add', 'past', params, location, 'location');

			// If showcasing
			if ( showcase )
			{
				this.game.showcaseComponent({
					title: `New ${RL.u.capitalize(componentNoun)} ${RL.u.capitalize(addVerb)}`, 
					name: location.name,
					image: location.image,
					description: location.description,
					component: location,
					componentKey: locationId,
					componentType: 'location'
				}, true, params?.bypassQueue);

				/*
				this.game.queueAction({type: 'modal', params: {
					title: `<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>New ${RL.u.capitalize(componentNoun)}!`, 
					body: `${location.name} ${addVerb}!`,
					cancelButtonHide: true,
					confirmButtonLabel: 'Continue'
				}}, true);
				*/
			}
			else if ( notifyAdd )
			{
				let activateVerb = RL.u.capitalize(this.game.getVerb('activate', '', params, location, 'location'));
				let useVerb = RL.u.capitalize(this.game.getVerb('use', '', params, location, 'location'));
		
				this.game.queueAction({type: 'toast', params: {
					title: `<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>New ${RL.u.capitalize(componentNoun)}`,
					body: `${location.name} ${addVerb}!`,
					ctas: {
						primary: {
							attrObj: {
								'data-component-type': 'location',
								'data-component-key': locationId
							},
							classAddon: ( location.activateOnAdd ) ? 'd-none' : 'cta--component-activate',
							label: ( location.activateOnAdd ) ? useVerb : activateVerb
						}
					},
					type: 'toastTemplate'
				}}, true, params?.bypassQueue);
			}
		}
		// If notify add, do it
		else if ( notifyAdd )
		{
			let findVerb = this.game.getVerb('find', 'past', params, location, 'location');

			this.game.queueAction({type: 'toast', params: {
				body: `${location.name} ${findVerb}.`,
				color: 'success',
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}
	}

	handleActivate ( locationId, location, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${location.name} activated!`);

		let notifyActivate = this.game.getSetting('notifyActivate', params, location, 'location');
		if ( notifyActivate )
		{
			let activateVerb = this.game.getVerb('activate', 'past', params, location, 'location');

			this.game.queueAction({type: 'toast', params: {
				body: `${location.name} ${activateVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		this.game.ui.updateHudsDebounced();
	}

	handleDeactivate ( locationId, location, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${location.name} deactivated!`);

		let notifyDeactivate = this.game.getSetting('notifyDeactivate', params, location, 'location');
		if ( notifyDeactivate )
		{
			let deactivateVerb = this.game.getVerb('deactivate', 'past', params, location, 'location');

			this.game.queueAction({type: 'toast', params: {
				body: `${location.name} ${deactivateVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		this.game.ui.updateHudsDebounced();
	}

	// Handle location use
	handleUse ( locationId, location, params )
	{
		// Ensure active game
		if ( ! this.game.active )
		{
			this.game.logger.debug(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Game not active`);
			return;
		}

		this.game.logger.debug(`${location.name} used!`);

		let notifyUse = this.game.getSetting('notifyUse', params, location, 'location');
		if ( notifyUse )
		{
			let useVerb = this.game.getVerb('use', 'past', params, location, 'location');
		
			this.game.queueAction({type: 'toast', params: {
				body: `${location.name} ${useVerb}!`,
				type: 'toastTemplateColor'
			}}, true, params?.bypassQueue);
		}

		this.game.location.ui.updateHudDebounced();
	}

}