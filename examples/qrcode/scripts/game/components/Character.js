/**
 * 
 */
class Character
{
	// Properties

	events;
	eventNames = {
		add: 'addCharacter',
		activate: 'activateCharacter',
		deactivate: 'deactivateCharacter',
		use: 'useCharacter',
		useThrottle: 'useThrottleCharacter',
		remove: 'removeCharacter',
		stats: 'statsCharacter',
		reset: 'resetCharacter',
		hint: 'hintCharacter',
		updateHud: 'updateCharacterHud'
	};

	characters;
	
	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.events = new EventHandler(this.eventNames);
		this.ui = new CharacterUi(this.game);
		this.reset();
    }
	
	/**
	 * 
	 * @returns void
	 */
	reset ()
	{
		this.characters = {};
		// Trigger event
		this.events.trigger(this.events.names.reset);
	}

	/**
	 * 
	 * @returns void
	 */
	add ( characterId, params )
	{
		// Stop if there is no item reference
		if ( ! Reflect.has(this.game.database.characters, characterId) )
		{
			this.game.logger.debug(`The character does not exist.`);
			return;
		}

		/* Allow re-adding
		// If we already have this character, stop
		if ( Reflect.has(this.characters, characterId) )
		{
			this.game.logger.debug(`The character has already been added.`);
			return;
		}
		*/
		
		// If record is new, add it
		// this.game.ledger.check('count', {entry: {type: 'character', action: 'add', id: characterId}}) === 1
		let newRecord = ( ! Reflect.has(this.characters, characterId) );
		if ( newRecord )
		{
			// Deep copy item reference
			this.characters[characterId] = structuredClone(this.game.database.characters[characterId]);
			this.characters[characterId].addTime = Date.now();
		}

		let character = this.characters[characterId];
		
		// Add to ledger
		this.game.ledger.add({
			type: 'character',
			action: 'add',
			id: characterId
		});

		// Trigger event
		this.events.trigger(this.events.names.add, characterId, character, newRecord, params);

		// If there is a payload on first add or after
		if ( newRecord && character.payloadOnFirstAdd )
		{
			this.game.queueAction({type: 'payload', params: character.payloadOnFirstAdd}, true);
		}
		else if ( character.payload )
		{
			this.game.queueAction({type: 'payload', params: character.payload}, true);
		}

		// Check if a challenge or game submit
		this.game.checkSubmits({
			type: 'character',
			action: 'add',
			id: characterId
		});
		
		// If activate on add
		if ( character.activateOnAdd )
		{
			this.activate(characterId, params);
		}

		// If use on add
		if ( character.useOnAdd )
		{
			this.use(characterId, params);
		}
	}

	/**
	 * 
	 * @returns void
	 */
	remove ( characterId, params )
	{
		// If item not in database, stop
		if ( ! Reflect.has(this.game.database.characters, characterId) )
		{
			this.game.logger.debug(`The character does not exist.`);
			return;
		}
		
		// If character wasn't found, stop
		if ( ! Reflect.has(this.characters, characterId) )
		{
			this.game.logger.debug(`The character was never found.`);
			return;
		}
		
		delete this.characters[characterId];

		// Add to ledger
		this.game.ledger.add({
			type: 'character',
			action: 'remove',
			id: characterId
		});

		// Trigger event
		this.events.trigger(this.events.names.remove, characterId, params);
	}

	/**
	 * 
	 * @returns void
	 */
	use ( characterId, params )
	{
		let character = this.characters[characterId];
		
		// If throttling use
		if ( character.useThrottleMs )
		{
			// If using sooner than throttle time, stop
			if ( Date.now() - character.lastUseTime < character.useThrottleMs )
			{
				// Trigger event
				this.events.trigger(this.events.names.useThrottle, characterId, character, params);
				return;
			}
		}
		
		// Update time info
		character.lastUseTime = Date.now();

		// If character stats get updated after use
		if ( character.statsUpdateOnUse )
		{
			// For each key
			let statKeys = Object.keys(character.statsUpdateOnUse);
			statKeys.forEach(key => {
				// Stop if inventory stat nonexistent
				if ( ! Reflect.has(character.stats, key) )
				{
					return;
				}

				// Update character stats
				character.stats[key] += parseFloat(character.statsUpdateOnUse[key]);
			});

			// If the character uses health
			if ( Reflect.has(character.stats, 'health') )
			{
				// If no more health or energy is left, reduce stock
				if ( character.stats.health <= 0 )
				{
				}
			}
			
			// Trigger event
			this.events.trigger(this.events.names.stats, characterId, params);
		}

		// If player stats get updated after use
		if ( character.playerStatsUpdateOnUse )
		{
			// For each key
			let playerStatsKeys = Object.keys(character.playerStatsUpdateOnUse);
			playerStatsKeys.forEach(key => {
				// Stop if player stat nonexistent
				if ( ! Reflect.has(this.game.player.stats, key) )
				{
					return;
				}

				this.game.player.stats[key] += parseFloat(character.playerStatsUpdateOnUse[key]);
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats, characterId, character, params);
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'character',
			action: 'use',
			id: characterId
		});

		// Trigger event
		this.events.trigger(this.events.names.use, characterId, character, params);

		// Check if a challenge or game submit
		this.game.checkSubmits({
			type: 'character',
			action: 'use',
			id: characterId
		});
	}

	/**
	 * 
	 * @returns void
	 */
	activate ( characterId, params )
	{
		let character = this.characters[characterId];
		character.active = true;
		character.activateTime = Date.now();
		
		// Update player stats after activate
		if ( character.playerStatsUpdateOnActivate )
		{
			// For each key
			let playerStatsKeys = Object.keys(character.playerStatsUpdateOnActivate);
			playerStatsKeys.forEach(key => {
				// Update player stats directly
				this.game.player.stats[key] += parseFloat(character.playerStatsUpdateOnActivate[key]);
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats, params);
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'character',
			action: 'activate',
			id: characterId,
			time: character.activateTime
		});

		// Trigger event
		this.events.trigger(this.events.names.activate, characterId, character, params);

		// If use on activate
		if ( character.useOnActivate )
		{
			this.use(characterId);
		}
	}

	/**
	 * 
	 * @returns void
	 */
	deactivate ( characterId, params )
	{
		let character = this.characters[characterId];
		character.active = false;
		
		// Update player stats after deactivate
		if ( character.playerStatsUpdateOnActivate )
		{
			// For each key
			let playerStatsKeys = Object.keys(character.playerStatsUpdateOnActivate);
			playerStatsKeys.forEach(key => {
				// Update player stats directly
				this.game.player.stats[key] -= parseFloat(character.playerStatsUpdateOnActivate[key]);
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats, params);
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'character',
			action: 'deactivate',
			id: characterId
		});

		// Trigger event
		this.events.trigger(this.events.names.deactivate, characterId, character, params);
	}

	showInteraction ( type, id, entryId, entryIndex, data )
	{
		// Default entry to show if none found to display 
		let defaultEntry = {
			text: '.....'
		};

		// Get record reference
		let record;
		if ( type == 'game' )
		{
			record = this.game.database.game;
		}
		else if ( this.game.database[`${type}s`] && Reflect.has(this.game.database[`${type}s`], id) )
		{
			record = this.game.database[`${type}s`][id];
		}

		// If a record found, continue
		if ( record && ! entryId )
		{
			// If no entry id but only one, default to it
			let entryKeys = Object.keys(record.interactionManifest);
			if ( entryKeys.length == 1 )
			{
				// Get first manifest key
				let firstEntryKey = entryKeys[0];
				// Get entry reference
				let entry = record.interactionManifest[firstEntryKey];
				// If the entry is available, stop
				if ( this.isInteractionEntryAvailable(entry) )
				{
					entryId = firstEntryKey;
				}
			}
			// Else check through conditions
			else
			{
				// The manifest is treated like a conditional block, so a default interaction with no conditions will need to be last or else it will always pass conditions first
				// For each entry key
				for ( let entryKey of entryKeys )
				{
					// Get entry reference
					let entry = record.interactionManifest[entryKey];
					// If the entry is available, stop
					if ( this.isInteractionEntryAvailable(entry) )
					{
						entryId = entryKey;
						break;
					}
				}
			}
		}

		// Default to assuming object passed even though it could be string or empty
		// Ex.: {"text":"A"}
		let entry = entryId;
		// If entry is not an object and there is a record, look it up
		if ( typeof(entryId) !== 'object' && record )
		{
			entry = record.interactionManifest[entryId];
		}
		// Else now reset entryId after having handed off to entry prior
		else
		{
			entryId = '';
		}
		
		// If array entry, pick random
		// Ex.: "welcome": [{"text":"A"},{"text":"B"}]
		if ( Array.isArray(entry) )
		{
			entry = RL.u.randomFromArray(entry);
		}

		let entryArrayLength = 0;
		let seriesAllowSkip;
		// If array entry, select index
		// Ex.: "welcome": {"series":[{"text":"A"},{"text":"B"}]}
		// A set of dialog entries grouped in an array with no next value needed
		if ( entry && entry.series && Array.isArray(entry.series) )
		{
			seriesAllowSkip = entry.allowSkip;
			entryArrayLength = entry.series.length;
			entryIndex = entryIndex ?? 0;
			entry = entry.series[entryIndex];
		}

		// If no entry, use default
		if ( ! entry )
		{
			this.game.logger.debug(`No available interaction entry.`);
			// Give a default entry instead
			entry = structuredClone(defaultEntry);
		}

		// If text is an array, choose a random entry
		// Ex.: {"text":["A","B"]}
		let text = entry.text ?? defaultEntry.text;
		if ( Array.isArray(text) )
		{
			text = RL.u.randomFromArray(text);
		}
		
		// Check if a similar modal is shown
		let reuseModal = false;
		let existingElement = document.querySelector('.modal.show.modal-interaction');
		
		// Create modal
		let modalData;
		// If one already open, reuse?
		if ( existingElement )
		{
			reuseModal = true;
			modalData = Modal.getShownTemplate();
		}
		else
		{
			modalData = Modal.createFromTemplate(Modal.modalTemplate);
		}
		
		let title = this.game.parseShortcodes(`Interaction`);
		let body = this.game.parseShortcodes(text);
		let background = entry.background ?? record?.interactionDefaults?.background ?? this.game?.database.interactionDefaults?.background ?? '';
		let leftAvatar = entry.leftAvatar ?? record?.interactionDefaults?.leftAvatar ?? this.game?.database.interactionDefaults?.leftAvatar ?? '';
		let leftAvatarFlip = entry.leftAvatarFlip ?? record?.interactionDefaults?.leftAvatarFlip ?? this.game?.database.interactionDefaults?.leftAvatarFlip ?? '';
		let rightAvatar = entry.rightAvatar ?? record?.interactionDefaults?.rightAvatar ?? this.game?.database.interactionDefaults?.rightAvatar ?? '';
		let rightAvatarFlip = entry.rightAvatarFlip ?? record?.interactionDefaults?.rightAvatarFlip ?? this.game?.database.interactionDefaults?.rightAvatarFlip ?? '';
		
		// Update content
		modalData.$modal.addClass('modal-interaction');
		modalData.$modalTitle.html(title).addClass('d-none');
		modalData.$modalBody.html(body);

		// If changing modal background
		let backgroundCSS = {backgroundImage: ''};
		if ( background )
		{
			background = this.game.parseShortcodes(background);
			backgroundCSS = {
				backgroundImage: `url("${this.game.parseShortcodes(background)}")`, 
				backgroundPosition: '50% 50%', 
				backgroundSize: 'cover'
			};
		}
		modalData.$modal.find('.modal-content').css(backgroundCSS);

		// If showing left avatar
		modalData.$modal.find('.modal-avatar--left').remove();
		if ( leftAvatar )
		{
			leftAvatar = this.game.parseShortcodes(leftAvatar);
			let $leftAvatar = $(Modal.leftAvatarTemplate).prependTo(modalData.$modalBody);
			let $leftAvatarImage = $leftAvatar.find('.modal-avatar__image');
			$leftAvatarImage.attr('src', this.game.parseShortcodes(leftAvatar));

			// If the image should flip so it looks the other way, add class
			if ( leftAvatarFlip )
			{
				$leftAvatarImage.addClass('flip--h');
			}
		}

		// If showing right avatar
		modalData.$modal.find('.modal-avatar--right').remove();
		if ( rightAvatar )
		{
			rightAvatar = this.game.parseShortcodes(rightAvatar);

			let $rightAvatar = $(Modal.rightAvatarTemplate).prependTo(modalData.$modalBody);
			let $rightAvatarImage = $rightAvatar.find('.modal-avatar__image');
			$rightAvatarImage.attr('src', this.game.parseShortcodes(rightAvatar));

			// If the image should flip so it looks the other way, add class
			if ( rightAvatarFlip )
			{
				$rightAvatarImage.addClass('flip--h');
			}
		}



		// Get references
		let $confirmButton = modalData.$modal.find(`.button--confirm`).removeClass('d-none');
		let $skipButton = modalData.$modal.find(`.button--cancel`).removeClass('d-none');
		
		// If reusing modal, clone buttons without events
		if ( reuseModal )
		{
			let $confirmButtonClone = $confirmButton.clone().insertAfter($confirmButton);
			$confirmButton.remove();
			$confirmButton = $confirmButtonClone;

			let $skipButtonClone = $skipButton.clone().insertAfter($skipButton);
			$skipButton.remove();
			$skipButton = $skipButtonClone;

			// Remove previous choices
			$(`${modalData.modalIdSelector} .button--choice`).remove();
		}

		/*
		// If hiding buttons
		modalData.$modal.find('.button--cancel').toggleClass('d-none', ( cancelButtonHide ));
		modalData.$modal.find('.button--confirm').toggleClass('d-none', ( confirmButtonHide ));

		// If changing button labels
		if ( cancelButtonLabel )
		{
			modalData.$modal.find('.button--cancel').html(this.game.parseShortcodes(cancelButtonLabel));
		}

		if ( confirmButtonLabel )
		{
			modalData.$modal.find('.button--confirm').html(this.game.parseShortcodes(confirmButtonLabel));
		}
		*/

		// If there are choices
		if ( entry.choices )
		{
			let choiceIteration = 0;
			// For each choice, create the option
			for ( let [key, value] of Object.entries(entry.choices) )
			{
				choiceIteration++;

				$confirmButton
				.clone()
				.appendTo( $(`${modalData.modalIdSelector} .modal-footer`) )
				.removeClass('button--confirm')
				.addClass('button--choice')
				.html(this.game.parseShortcodes(value.text))
				.data('choice', key)
				.data('choiceEntryId', entryId)
				.data('entry', value)
				.click(function ( event ) {
					const targetElement = event.currentTarget;
					// Get entry from element
					let choice = $(targetElement).data('choice');
					let entry = $(targetElement).data('entry');
					let choiceEntryId = $(targetElement).data('choiceEntryId');

					// If there is a payload, process at the end
					if ( entry.payload )
					{
						this.game.queueAction({type: 'payload', params: entry.payload, bypassQueue: entry.payload.bypassQueue});
					}
		
					// If there is a next param to use
					if ( entry.next )
					{
						this.game.character.showInteraction(type, id, entry.next, entryIndex, {choice: choice, choiceEntryId: choiceEntryId});
						/*
						this.queueAction({type: 'interaction', params: {
							type: type, 
							id: id, 
							entryId: entry.next, 
							entryIndex: entryIndex, 
							data: {choice: choice, choiceEntryId: choiceEntryId}
						}}, true);
						*/
					}
					else
					{
						// Hide modal
						Modal.hide($(modalData.modalIdSelector));
					}
				}.bind(this));
			}

			// Hide templates
			$confirmButton.addClass('d-none');
			$skipButton.addClass('d-none');
		}
		// If a generic modal
		else
		{
			// Set up confirm
			$confirmButton
			.html('Continue')
			.click(function ( event ) {
				const targetElement = event.currentTarget;

				// If there is a next param to use
				if ( entry.next )
				{
					this.game.character.showInteraction(type, id, entry.next, '', '');
					/*
					this.game.queueAction({type: 'interaction', params: {
						type: type, 
						id: id, 
						entryId: entry.next
					}}, true);
					*/
				}
				// If using an array of entries and not on the last entry
				else if ( entryArrayLength > 0 && entryIndex + 1 < entryArrayLength )
				{
					this.game.character.showInteraction(type, id, entryId, entryIndex + 1, '');
					/*
					this.game.queueAction({type: 'interaction', params: {
						type: type, 
						id: id, 
						entryId: entryId,
						entryIndex: entryIndex + 1
					}}, true);
					*/
				}
				else
				{
					// Hide modal
					Modal.hide($(modalData.modalIdSelector));
				}
			}.bind(this));

			// Go through all possibilities for allowing a skip
			// Check parameter, if there is a next entry, if not on the last array item
			let allowSkip = Boolean(entry.allowSkip ?? seriesAllowSkip ?? entry.next ?? (entryArrayLength > 0 && entryIndex + 1 < entryArrayLength));
	
			// If allowing skip, set up
			if ( allowSkip )
			{
				$skipButton
				.html('Skip')
				.click(function(){
					// Hide modal
					Modal.hide($(modalData.modalIdSelector));
				})
				.removeClass('d-none');
			}
			// Else remove button
			else
			{
				$skipButton.addClass('d-none');
			}
		}

		// If there is a payload, process at the end
		if ( entry.payload )
		{
			this.game.queueAction({type: 'payload', params: entry.payload, bypassQueue: entry.payload.bypassQueue});
		}

		// Don't allow backdrop or keyboard to close modal
		if ( reuseModal )
		{
			// Update Bootstrap
			const bsModal = bootstrap.Modal.getInstance(document.querySelector('.modal.show'));
			bsModal.handleUpdate();//$modal.handleUpdate();
		}
		else
		{
			/*
			// Perform actions when modal is about to hide
			modalData.modalElement.addEventListener('hide.bs.modal', event => {
				//bootstrap.Modal.getInstance(document.querySelector('.modal.show'))
				event.preventDefault();
			});
			*/
			// Perform actions when modal is hidden
			modalData.modalElement.addEventListener('hidden.bs.modal', event => {
				// Add to ledger
				this.game.ledger.add({
					type: type,
					action: 'interaction',
					id: id,
					entryId: data?.choiceEntryId ?? entryId,
					entryIndex: entryIndex ?? '',
					choice: data?.choice ?? ''
				});
				
				/* // Automatically continue
				if ( entry.next )
				{
					this.showInteraction(type, id, entry.next);-
				}
				*/
			});

			// Limit closing modal to only given options
			// Remove close button
			modalData.$modal.find('.btn-close').addClass('d-none');
			Modal.show($(modalData.modalIdSelector), {backdrop: 'static', keyboard: false});
		}
	}

	isInteractionEntryAvailable ( entry )
	{
		// If available conditions
		let available = ( Reflect.has(entry, 'availableConditions') ) ? this.checkInteractionConditions(entry.availableConditions) : true;

		// If unavailable conditions
		let unavailable = ( Reflect.has(entry, 'unavailableConditions') ) ? this.checkInteractionConditions(entry.unavailableConditions) : false;

		return ( available && ! unavailable );
	}

	checkInteractionConditions ( conditions )
	{
		let match = true;

		let conditionKeys = Object.keys(conditions);
		// For each condition key
		for ( let conditionKey of conditionKeys )
		{
			if ( conditionKey == 'challengesComplete' )
			{
				// If an array not passed, make it one
				if ( ! Array.isArray(conditions.challengesComplete) )
				{
					conditions.challengesComplete = [conditions.challengesComplete];
				}

				// For each challengeId
				for ( let challengeId of conditions.challengesComplete )
				{
					// If challenge not complete, stop
					if ( ! Reflect.has(this.game.challenge.completedChallenges, challengeId) )
					{
						match = false;
						break;
					}
				}
			}
			else if ( conditionKey == 'challengesActive' )
			{
				// If an array not passed, make it one
				if ( ! Array.isArray(conditions.challengesActive) )
				{
					conditions.challengesActive = [conditions.challengesActive];
				}

				// For each challengeId
				for ( let challengeId of conditions.challengesActive )
				{
					// If challenge not active, stop
					if ( ! Reflect.has(this.game.challenge.activeChallenges, challengeId) )
					{
						match = false;
						break;
					}
				}
			}
			else if ( conditionKey == 'itemsActive' )
			{
				// If an array not passed, make it one
				if ( ! Array.isArray(conditions.itemsActive) )
				{
					conditions.itemsActive = [conditions.itemsActive];
				}

				// For each itemId
				for ( let itemId of conditions.itemsActive )
				{
					// If item not in inventory, stop
					if ( ! Reflect.has(this.game.item.inventory, itemId) )
					{
						match = false;
						break;
					}

					// If item not active, stop
					if ( ! this.game.item.inventory[itemId].active )
					{
						match = false;
						break;
					}
				}
			}
			else if ( conditionKey == 'challenges' )
			{
				let challengeIds = Object.keys(conditions.challenges);
				// For each challengeId
				for ( let challengeId of challengeIds )
				{
					// If id not in object, stop
					if ( ! Reflect.has(this.game.challenge.challenges, challengeId) )
					{
						match = false;
						break;
					}
				}
			}
			else if ( conditionKey == 'locations' )
			{
				let locationIds = Object.keys(conditions.locations);
				// For each locationId
				for ( let locationId of locationIds )
				{
					// If id not in object, stop
					if ( ! Reflect.has(this.game.location.locations, locationId) )
					{
						match = false;
						break;
					}
				}
			}
			else if ( conditionKey == 'characters' )
			{
				let characterIds = Object.keys(conditions.characters);
				// For each characterId
				for ( let characterId of characterIds )
				{
					// If id not in object, stop
					if ( ! Reflect.has(this.game.character.characters, characterId) )
					{
						match = false;
						break;
					}
				}
			}
			else if ( conditionKey == 'items' )
			{
				let itemIds = Object.keys(conditions.items);
				// For each itemId
				for ( let itemId of itemIds )
				{
					// If id not in object, stop
					if ( ! Reflect.has(this.game.item.inventory, itemId) )
					{
						match = false;
						break;
					}

					// Get item reference
					let item = this.game.item.inventory[itemId];

					// Get additional criteria
					let itemConditions = conditions.items[itemId];
					let itemConditionKeys = Object.keys(itemConditions);
					// For each addition condition
					for ( let itemConditionKey of itemConditionKeys )
					{
						// If checking minimum stock
						if ( itemConditionKey == 'stockMin' )
						{
							// If less that minimum stock, stop
							if ( item.stock < itemConditions.stockMin )
							{
								match = false;
								break;
							}
						}
					}
				}
			}
			else if ( conditionKey == 'ledgers' )
			{
				// If an array not passed, make it one
				if ( ! Array.isArray(conditions.ledgers) )
				{
					conditions.ledger = [conditions.ledgers];
				}

				// For each ledger
				for ( let ledger of conditions.ledgers )
				{
					// If ledger not matched, stop
					if ( ! this.game.ledger.check('match', ledger) )
					{
						match = false;
						break;
					}
					
				}
			} // end conditionKey
		}

		return match;
	}

}