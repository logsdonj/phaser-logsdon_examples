/**
 * 
 */
class Item
{
	// Properties

	events;
	eventNames = {
		add: 'addItem',
		addThrottle: 'addThrottleItem',
		activate: 'activateItem',
		deactivate: 'deactivateItem',
		use: 'useItem',
		useThrottle: 'useThrottleItem',
		remove: 'removeItem',
		maxStock: 'maxStockItem',
		stats: 'statsItem',
		reset: 'resetItem',
		hint: 'hintItem',
		updateHud: 'updateItemHud'
	};

	inventory;
	
	// Methods

	/**
	 * 
	 * @returns void
	 */
	constructor ( gameInstance )
	{
		this.game = gameInstance;
		this.events = new EventHandler(this.eventNames);
		this.ui = new ItemUi(this.game);
		this.reset();
    }
	
	/**
	 * 
	 * @returns void
	 */
	reset ()
	{
		this.inventory = {};
		// Trigger event
		this.events.trigger(this.events.names.reset);
	}

	/**
	 * 
	 * @returns void
	 */
	add ( itemId, params )
	{
		// Stop if there is no item reference
		if ( ! Reflect.has(this.game.database.items, itemId) )
		{
			this.game.logger.debug(`The item does not exist.`);
			return;
		}
		
		// If record is new
		// this.game.ledger.check('count', {entry: {type: 'item', action: 'add', id: itemId}}) === 1
		let newRecord = ! ( this.game.ledger.check('match', {
			entry:{
				type: 'item', 
				id: itemId, 
				action: 'add'
			}, 
			limitCheckToLength: true
		}) );

		let dbItem = this.game.database.items[itemId];
		// Deep copy item reference
		let item = structuredClone(dbItem);

		// TODO: Better item condition handling. This is just to start.
		// If prerequisites, check
		if ( dbItem.prerequisites )
		{
			// Assume all requirements are met, find where they do not
			let met = true;
			let conditions = dbItem.prerequisites;
			let requirements = conditions ?? {};
	
			// Check challenge requirements
			let requirementKeys = Object.keys(requirements);
			requirementKeys.forEach(key => {
				// If checking items, challenges, playerStats, or timeLimit
				if ( ['items', 'challenges', 'locations', 'playerStats', 'timeLimit', 'ledgers'].indexOf(key) !== -1 )
				{
					let requirementsData;
	
					if ( key == 'items' )
					{
						requirementsData = this.game.checkItemsRequirements(requirements.items);
						//itemsStrings = requirementsData.strings;
					}

					met = ( met ) ? requirementsData.met : met;
					//num += requirementsData.num;
					//numComplete += requirementsData.numComplete;
					//metStrings = metStrings.concat(requirementsData.metStrings);
					//missingStrings = missingStrings.concat(requirementsData.missingStrings);
				}
			});

			// If not met, stop
			if ( ! met )
			{
				this.game.logger.debug(`Item prerequisites not met.`);

				// Trigger event
				this.events.trigger(this.events.names.add, itemId, item, newRecord, '', Object.assign(params, {error: 'Prerequisites not met. Try others first.'}));

				return;
			}
		}

		// Check if item is for any active challenge
		let requiredForChallenges = ( this.game.challenge.isItemForActiveChallenge(itemId).length > 0 );
		if ( ! requiredForChallenges )
		{
			this.game.logger.debug(`The item is not required for any active challenge.`);
		}
		else
		{
			this.game.logger.debug(`The item is required for an active challenge.`);
		}

		// If item uses inventory
		if ( item.isInventory )
		{
			// If item not in inventory, add it
			let firstAdd = ( ! Reflect.has(this.inventory, itemId) );
			if ( firstAdd )
			{
				// Add to inventory
				this.inventory[itemId] = item;
				item.addTime = Date.now();
				// Initialize stock
				item.stock = item.stock ?? 1;
			}

			// Update item reference to new or old inventory
			item = this.inventory[itemId];

			// If added sooner than throttle time, stop
			if ( Date.now() - item.lastAddTime < item.addThrottleMs )
			{
				// Trigger event
				this.events.trigger(this.events.names.addThrottle, itemId, item, params);
				return;
			}

			// If no stock and item health is 0
			if ( 
				item.stock == 0 && 
				item.stats && 
				Reflect.has(item.stats, 'health') && 
				item.stats.health == 0 
			)
			{
				// Reset item health for new stock
				item.stats.health = dbItem.stats.health;
			}

			// Safely increase stock in case not yet set
			if ( ! firstAdd )
			{
				item.stock += dbItem.stock ?? 1;
			}

			// If greater than max item stock, adjust and stop
			if ( item.maxStock > 0 && item.stock > item.maxStock )
			{
				item.stock = item.maxStock;

				// Trigger event
				this.events.trigger(this.events.names.maxStock, itemId, item, params);
				return;
			}
			
			// Update time info
			item.lastAddTime = Date.now();
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'item',
			action: 'add',
			id: itemId
		});

		// Trigger event
		this.events.trigger(this.events.names.add, itemId, item, newRecord, requiredForChallenges, params);

		// If there is a payload on first add or after
		if ( newRecord && item.payloadOnFirstAdd )
		{
			this.game.queueAction({type: 'payload', params: item.payloadOnFirstAdd}, true);
		}
		else if ( item.payload )
		{
			this.game.queueAction({type: 'payload', params: item.payload}, true);
		}

		// Check if a challenge or game submit
		this.game.checkSubmits({
			type: 'item',
			action: 'add',
			id: itemId
		});

		// If activate on add
		if ( item.activateOnAdd )
		{
			this.activate(itemId);
		}

		// If use on add
		if ( item.useOnAdd )
		{
			this.use(itemId);
		}

		// If the item is consumable, use immediately
		if ( ! item.isInventory )
		{
			this.use(itemId);
		}
	}

	/**
	 * 
	 * @returns void
	 */
	remove ( itemId, params )
	{
		// If item not in database, stop
		if ( ! Reflect.has(this.game.database.items, itemId) )
		{
			this.game.logger.debug(`The item does not exist.`);
			return;
		}
		
		// If item not in inventory, stop
		if ( ! Reflect.has(this.inventory, itemId) )
		{
			this.game.logger.debug(`The item is not inventoried.`);
			return;
		}
		
		delete this.inventory[itemId];

		// Add to ledger
		this.game.ledger.add({
			type: 'item',
			action: 'remove',
			id: itemId
		});

		// Trigger event
		this.events.trigger(this.events.names.remove, itemId, params);
	}

	/**
	 * 
	 * @returns void
	 */
	use ( itemId, params )
	{
		let dbItem = this.game.database.items[itemId];
		let item = structuredClone(dbItem);
		
		// If item uses inventory
		if ( item.isInventory )
		{
			item = this.inventory[itemId];

			// If item is used up, stop
			if ( item.stock <= 0 )
			{
				return;
			}

			// If throttling use
			if ( item.useThrottleMs )
			{
				// If using sooner than throttle time, stop
				if ( Date.now() - item.lastUseTime < item.useThrottleMs )
				{
					// Trigger event
					this.events.trigger(this.events.names.useThrottle, itemId, item, params);
					return;
				}
			}
			
			// Update time info
			item.lastUseTime = Date.now();

			// If item stats get updated after use
			if ( item.statsUpdateOnUse )
			{
				// For each key
				let statKeys = Object.keys(item.statsUpdateOnUse);
				statKeys.forEach(key => {
					// Stop if inventory stat nonexistent
					if ( ! item.stats || ! Reflect.has(item.stats, key) )
					{
						return;
					}

					// Update inventory item
					item.stats[key] += parseFloat(item.statsUpdateOnUse[key]);
				});

				// If the item uses health
				if ( item.stats && Reflect.has(item.stats, 'health') )
				{
					// If no more health or energy is left, reduce stock
					if ( item.stats.health <= 0 )
					{
						item.stock--;

						// If stock left, reset item health
						if ( item.stock > 0 )
						{
							item.stats.health = dbItem.stats.health;
						}
					}
				}
				
				// Trigger event
				this.events.trigger(this.events.names.stats, itemId, params);
			}

			// If the item reduce stock after use
			if ( Reflect.has(item, 'stockUpdateOnUse') )
			{
				item.stock += parseFloat(item.stockUpdateOnUse);
			}

			// If item is used up, normalize stock value
			if ( item.stock <= 0 )
			{
				item.stock = 0;
				// Deactivate item
				//this.deactivate(itemId);
			}
		}

		// If player stats get updated after use
		if ( item.playerStatsUpdateOnUse )
		{
			// For each key
			let playerStatsKeys = Object.keys(item.playerStatsUpdateOnUse);
			playerStatsKeys.forEach(key => {
				// Stop if player stat nonexistent
				if ( ! Reflect.has(this.game.player.stats, key) )
				{
					return;
				}

				this.game.player.stats[key] += parseFloat(item.playerStatsUpdateOnUse[key]);
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats, itemId, item);
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'item',
			action: 'use',
			id: itemId
		});

		// Trigger event
		this.events.trigger(this.events.names.use, itemId, item, params);

		// Check if a challenge or game submit
		this.game.checkSubmits({
			type: 'item',
			action: 'use',
			id: itemId
		});
	}

	/**
	 * 
	 * @returns void
	 */
	activate ( itemId, params )
	{
		let item = this.inventory[itemId];
		item.active = true;
		item.activateTime = Date.now();
		
		// Update player stats after activate
		if ( item.playerStatsUpdateOnActivate )
		{
			// For each key
			let playerStatsKeys = Object.keys(item.playerStatsUpdateOnActivate);
			playerStatsKeys.forEach(key => {
				// Update player stats directly
				this.game.player.stats[key] += parseFloat(item.playerStatsUpdateOnActivate[key]);
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats, params);
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'item',
			action: 'activate',
			id: itemId,
			time: item.activateTime
		});

		// Trigger event
		this.events.trigger(this.events.names.activate, itemId, item, params);

		// If use on activate
		if ( item.useOnActivate )
		{
			this.use(itemId);
		}
	}

	/**
	 * 
	 * @returns void
	 */
	deactivate ( itemId, params )
	{
		let item = this.inventory[itemId];
		item.active = false;
		
		// Update player stats after deactivate
		if ( item.playerStatsUpdateOnActivate )
		{
			// For each key
			let playerStatsKeys = Object.keys(item.playerStatsUpdateOnActivate);
			playerStatsKeys.forEach(key => {
				// Update player stats directly
				this.game.player.stats[key] -= parseFloat(item.playerStatsUpdateOnActivate[key]);
			});

			// Trigger event
			this.game.player.events.trigger(this.game.player.events.names.stats, params);
		}

		// Add to ledger
		this.game.ledger.add({
			type: 'item',
			action: 'deactivate',
			id: itemId
		});

		// Trigger event
		this.events.trigger(this.events.names.deactivate, itemId, item, params);
	}
	
	depletesStock ( itemId )
	{
		// List out item information
		let item = ( typeof(itemId) == 'string' ) ? this.game.database.items[itemId] : itemId;

		// Check if item uses stock
		let depletesStock = (
			(
				Reflect.has(item, 'isUsable') &&
				item.isUsable
			) || 
			( 
				Reflect.has(item, 'stockUpdateOnUse') &&
				item.stockUpdateOnUse !== 0 
			) || 
			(
				Reflect.has(item, 'stats') &&
				Reflect.has(item.stats, 'health') &&
				Reflect.has(item, 'statsUpdateOnUse') &&
				Reflect.has(item.statsUpdateOnUse, 'health') 
			)
		);

		return depletesStock;
	}
}