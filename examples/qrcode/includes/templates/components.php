<?php

// Component Showcase Modal

$component__modalShowcaseTemplate = <<<EOD
<div class="modal modal--showcase" tabindex="-1" role="dialog" aria-labelledby="modal-showcase-title" id="modal-showcase" aria-modal="true" style="display: block; position: relative; height: auto;">
<!-- Modal -->
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header justify-content-center text-center">
				<h5 class="modal-title" id="modal-showcase-title">
					<i class="fa-solid fa-star fa-xs me-1 text-warning"></i>
					New Component Added
				</h5>
				<button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body justify-content-center text-center">
				<h3 class="fw-bold modal-showcase__component-title">Component Title</h3>
				<div class="modal-showcase__image-container mt-4 mb-4">
					<img alt="Showcase Image" class="modal-showcase__image img-fluid" src="">
				</div>
				<!--
				<svg class="rounded" width="100" height="100" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
				-->
				<div class="modal-showcase__description">Component description.</div>
			</div>
			<div class="modal-footer justify-content-center text-center">
				<!--
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
				
				<button type="button" class="btn btn-secondary button--cancel d-none">Cancel</button>
				<button type="button" class="btn btn-primary button--confirm">Ok</button>

				<button type="button" class="btn btn-sm btn-outline-primary">Activate</button>
				-->

				<div class="display-flex w-100 mb-3 justify-content-start modal-footer__cta-container d-none">
					<button type="button" class="btn btn-outline-primary btn-sm cta--component-activate">Activate</button>
				</div>
				

				<div class="display-flex w-100 justify-content-start modal-footer__nav">
					<button type="button" class="btn btn-sm btn-secondary button--cancel" data-bs-dismiss="modal">Ok</button>
				</div>

			</div>
		</div>
	</div>
</div>
EOD;

// Component Info Modal

$component__modalInfoTemplate = <<<EOD
<div class="modal modal--component-info" tabindex="-1" role="dialog" aria-labelledby="component-info-title" aria-modal="true" style="display: block; position: relative; height: auto;">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="d-flex align-items-center">
					<span class="modal-heading-icon me-1"></span>
					<h5 class="modal-title" id="component-info-title">Item Name</h5>
				</div>
				<div class="d-flex align-items-center text-end">
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
			</div>
			<div class="modal-body">

				<div class="d-flex flex-column">
					<div class="flex-shrink-0 d-flex flex-column mb-3 modal-image-col">
						<div class="modal-image-container">
							<svg class="modal-image img-fluid rounded" width="100" height="100" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
						</div>
						<!--
						<div class="modal-image-container">
							<img alt="..." class="modal-image img-fluid rounded" src="...">
						</div>
						-->
					</div>
					<div class="flex-grow-1 ms-3 modal-text-col">
						<div class="mb-2">
							<b>Type:</b> Food
						</div>
						<div class="mb-2">
							<b>Description:</b> This is some content from a media component. You can replace this with any content and adjust it as needed. This is some content from a media component.
						</div>
						<div class="mb-2">
							<b>Added:</b> about 15 minutes ago
						</div>
						<div class="mb-2">
							<b>Stock:</b> 3, max stock 5
						</div>
						<div class="mb-2">
							<b>Stats:</b> 3
						</div>
						<div class="mb-2">
							<b>Usage:</b> 3
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				
				<div class="display-flex w-100 mb-3 justify-content-start modal-footer__cta-container">
					<button type="button" class="btn btn-outline-primary btn-sm cta--component-use">Use <span class="badge bg-secondary badge--stock-amount">0</span></button>
					
					<button type="button" class="btn btn-outline-primary btn-sm cta--component-activate">Activate</button>
					
					<button type="button" class="btn btn-outline-secondary btn-sm cta--component-deactivate">Deactivate</button>
					
					<button type="button" class="btn btn-outline-secondary btn-sm cta--component-restart">Restart</button>
					
					<button type="button" class="btn btn-outline-secondary btn-sm cta--component-hint">Hint<span class="badge rounded-pill bg-dark ms-1 badge--hint-amount">0</span></button>
				</div>

				<div class="display-flex w-100 justify-content-start modal-footer__nav">
					<button type="button" class="btn btn-outline-secondary btn-sm me-2 button--component-info-prev cta--component-info-prev"><i class="fa-solid fa-chevron-left fa-fw"></i></button>
					
					<button type="button" class="btn btn-outline-secondary btn-sm button--cancel" data-bs-dismiss="modal">Close</button>
					
					<button type="button" class="btn btn-outline-secondary btn-sm ms-2 button--component-info-next cta--component-info-next"><i class="fa-solid fa-chevron-right fa-fw"></i></button>
				</div>

			</div>
		</div>
	</div>
</div>
EOD;

$component__modalInfoImageContainer = <<<EOD
<div class="modal-info-image-container"><img alt="Info Image" class="modal-image img-fluid rounded" src=""></div>
EOD;

$component__badge_hintAmount = <<<EOD
<span class="badge rounded-pill bg-dark ms-1 badge--hint-amount"></span>
EOD;

$component__badge_stockAmount = <<<EOD
<span class="badge bg-secondary ms-1 badge--stock-amount"></span>
EOD;


// Component List Item

$component__listGroupItemInner = <<<EOD
	<div class="d-flex w-100 justify-content-between">
		<div class="d-flex">
			<span class="list-group-item__heading-icon me-1"></span>
			<h5 class="mb-1 list-group-item__heading">List group item heading</h5>
		</div>
		<div class="d-flex align-items-center text-end">
			<small class="text-muted fst-italic list-group-item__type">Type</small>
			<span class="badge border border-secondary text-secondary rounded-pill align-self-center ms-2 list-group-item__stock">1</span>
		</div>
	</div>
	<div class="list-group-item__description">Some placeholder content in a paragraph.</div>
	<div class="small text-muted list-group-item__notes">Some placeholder content in a paragraph.</div>
	
	<div class="d-flex mt-2 list-group-item__timer-progress">
		<i class="icon-timer icon--fa icon--fw me-1"></i>
		<div class="progress w-100">
			<div class="progress-bar" role="progressbar" aria-label="Timer Progress" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
		</div>
	</div>

	<div class="d-flex mt-2 list-group-item__progress">
		<i class="icon-finish icon--fa icon--fw me-1"></i>
		<div class="progress w-100">
			<div class="progress-bar" role="progressbar" aria-label="Progress" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
		</div>
	</div>
	
	<div class="d-flex flex-wrap mt-2 list-group-item--button-container list-group-item__cta-container">
	
		<button type="button" class="btn btn-outline-primary btn-sm cta--component-add">Add</button>

		<button type="button" class="btn btn-outline-primary btn-sm cta--component-use">Use <span class="badge bg-secondary badge--stock-amount">0</span></button>
		
		<button type="button" class="btn btn-outline-primary btn-sm cta--component-activate">Activate</button>
		
		<button type="button" class="btn btn-outline-secondary btn-sm cta--component-deactivate">Deactivate</button>
		
		<button type="button" class="btn btn-outline-secondary btn-sm cta--component-restart">Restart</button>
		
		<button type="button" class="btn btn-outline-secondary btn-sm cta--component-hint">Hint<span class="badge rounded-pill bg-dark ms-1 badge--hint-amount">0</span></button>

		<button type="button" class="btn btn-outline-primary btn-sm cta--component-info"><i class="icon-info icon--fa me-1"></i>Info</button>
		
		<div class="input-group w-auto ms-auto list-group-item__combinable-input-group">
			<!-- Even if the first item is display: none; the styles for the input-group will not do rounded borders on the first visual element
			<span class="input-group-text"><i class="fa-solid fa-box-open"></i></span>-->
			<span class="input-group-text text-muted list-group-item__combinable-input-group-order-container d-none">
				<small>#</small><small class="list-group-item__combinable-input-group-order">1</small>
			</span>
			<select class="form-select form-select-sm" name="combineKeyAmount[]" aria-label="Default select example">
				<option value="1" selected>1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
			<label class="form-check-label input-group-text">
				<input type="hidden" name="combineComponentType[]" value="">
				<input type="hidden" name="combineOrder[]" value="">
				<input class="form-check-input m-0" type="checkbox" name="combineKey[]" value="" aria-label="Checkbox for following text input">
			</label>
		</div>

	</div>
EOD;

$component__listGroupItemTemplate = <<<EOD
<li class="list-group-item">
	$component__listGroupItemInner
</li>
EOD;

$component__listGroupItemActiveTemplate = <<<EOD
<li class="list-group-item active">
	$component__listGroupItemInner
</li>
EOD;

$component__withSelectedContainer = <<<EOD
<div class="list-group-item__with-selected-container">
	<hr>
	<div class="d-flex align-items-center justify-content-end ps-2 pe-2 list-group-item--button-container">

		<span class="small text-muted">With selected:</span>

		<button type="button" class="btn btn-primary btn-sm cta--component-combine">Combine</button>

	</div>
</div>
EOD;
