<?php

$character__activatedComponentButtonTemplate = <<<EOD
<button class="btn btn-outline-primary btn-sm mb-1 me-1 cta--component-use" type="button">
	<span class="activated-item__cta-title">Title</span><span class="badge rounded-pill bg-secondary"></span>
</button>
EOD;
