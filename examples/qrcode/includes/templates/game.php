<?php

$main__componentNav = <<<EOD
<div class="row m-0 game-screen__nav-icon-row pe-auto">
	<div class="col">
		<a class="icon-challenges icon--fa game-screen__nav-icon game-screen__nav-icon--challenges" data-screen="challenges" href="#challenges-screen">
			<span class="visually-hidden">Challenges</span>
			<span class="badge rounded-pill position-absolute bg-secondary game-screen__nav-icon-stat stat--num-available-challenges"></span>
		</a>
	</div>
	<div class="col">
		<a class="icon-items icon--fa game-screen__nav-icon game-screen__nav-icon--items" data-screen="items" href="#items-screen">
			<span class="visually-hidden">Inventory</span>
			<span class="badge rounded-pill position-absolute bg-secondary game-screen__nav-icon-stat stat--num-inventory-items"></span>
		</a>
	</div>
	<div class="col">
		<a class="icon-locations icon--fa game-screen__nav-icon game-screen__nav-icon--settings" data-screen="locations" href="#locations-screen">
			<span class="visually-hidden">Locations</span>
			<span class="badge rounded-pill position-absolute bg-secondary game-screen__nav-icon-stat stat--num-available-locations"></span>
		</a>
	</div>
	<div class="col">
		<a class="icon-characters icon--fa game-screen__nav-icon game-screen__nav-icon--settings" data-screen="characters" href="#characters-screen">
			<span class="visually-hidden">Characters</span>
			<span class="badge rounded-pill position-absolute bg-secondary game-screen__nav-icon-stat stat--num-available-characters"></span>
		</a>
	</div>
</div>
EOD;
