<?php

$item__activatedInventoryItemButtonTemplate = <<<EOD
<button class="btn btn-outline-primary btn-sm mb-1 me-1 cta--component-use" type="button">
	<span class="activated-item__cta-title">Item</span><span class="badge rounded-pill bg-secondary badge--stock-amount">0</span>
</button>
EOD;
