<?php
// Serve as a JavaScript file
header('Content-Type: application/javascript');

// Include PHP templates for example and JS use
foreach ( ['components', 'challenges', 'items', 'locations', 'characters'] as $filename )
{
	require_once('templates/' . $filename . '.php');
}
?>

// Component templates
const component__badge_stockAmount = `<?php echo $component__badge_stockAmount; ?>`;
const component__badge_hintAmount = `<?php echo $component__badge_hintAmount; ?>`;
const component__modalShowcaseTemplate = `<?php echo $component__modalShowcaseTemplate; ?>`;
const component__modalInfoTemplate = `<?php echo $component__modalInfoTemplate; ?>`;
const component__modalInfoImageContainer = `<?php echo $component__modalInfoImageContainer; ?>`;
const component__listGroupItemTemplate = `<?php echo $component__listGroupItemTemplate; ?>`;
const component__withSelectedContainer = `<?php echo $component__withSelectedContainer; ?>`;

// Item templates
const activatedInventoryItemButtonTemplate = `<?php echo $item__activatedInventoryItemButtonTemplate; ?>`;
