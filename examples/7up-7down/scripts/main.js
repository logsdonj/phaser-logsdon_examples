// Poll with interval until available
(function() {
	let nTimer = setInterval(function() {
		// If available, stop polling
		if (window.jQuery) {
			clearInterval(nTimer);
			//====================

			jQuery(window).on('load', function ()
			{
			});

			// DOM ready
			jQuery(document).ready(function($)
			{
				
				// contenteditable
				// https://stackoverflow.com/a/6263537
				// https://blog.frankmtaylor.com/2012/01/23/html5-contenteditable-and-css/
				$('body').on('focus', '[contenteditable]', function ( event )
				{
					const $this = $(this);
					$this.data('before', $this.html());
				})
				.on('blur keyup paste input', '[contenteditable]', function ( event )
				{
					const $this = $(this);
					if ($this.data('before') !== $this.html()) {
						console.log('change');
						$this.data('before', $this.html());
						$this.trigger('change');
					}
				});

				$(document).on('focus', '[contenteditable]', function ( event )
				{
					console.log('here');
					// https://stackoverflow.com/questions/4067469/selecting-all-text-in-html-text-input-when-clicked
					//this.select();
					// https://stackoverflow.com/questions/3805852/select-all-text-in-contenteditable-div-when-it-focus-click
					window.setTimeout(function () {
						var sel, range;
						if (window.getSelection && document.createRange) {
							range = document.createRange();
							range.selectNodeContents(this);
							sel = window.getSelection();
							sel.removeAllRanges();
							sel.addRange(range);
						} else if (document.body.createTextRange) {
							range = document.body.createTextRange();
							range.moveToElementText(this);
							range.select();
						}
					}.bind(this), 1);
				});

				$(document).on('click', '.cta--player-add', function ( event )
				{
					createPlayer();
					calculate();
				});

				$(document).on('click', '.cta--round-add', function ( event )
				{
					createRound();
				});
				
				$(document).on('click', 'tbody tr', function ( event )
				{
					const $gameTable = getGameTable();
					const activeClass = 'active';
					$gameTable.find('tbody tr').removeClass(activeClass);
					const $roundRow = $(this);
					$roundRow.addClass(activeClass);
					const numRounds = getNumRounds($gameTable);
					const roundIndex = numRounds - $roundRow.index();
					updateDealer(roundIndex);
				});

				$(document).on('change', '.round-estimate[contenteditable], .round-actual[contenteditable]', function ( event )
				{
					calculate();
				});
				

				$(document).on('focus', 'input[type="number"]', function ( event )
				{
					this.select();
				});

				$(document).on('change, keyup', 'input[type="number"]', function ( event )
				{
					calculate();
				});

				/*
				for ( let i = 0; i < 3; i++ )
				{
					createPlayer();
				}

				for ( let i = 0; i < 1; i++ )
				{
					createRound();
				}

				calculate();
				*/

				function getGameTable ()
				{
					return $('.game-table');
				}

				function getNumPlayers ( $gameTable )
				{
					if ( ! $gameTable )
					{
						$gameTable = getGameTable();
					}

					return $gameTable.find('thead th').length - 1;
				}
				
				function getNumRounds ( $gameTable )
				{
					if ( ! $gameTable )
					{
						$gameTable = getGameTable();
					}

					return $gameTable.find('tbody tr').length;
				}
				
				function updateDealer ( round )
				{
					const $gameTable = getGameTable();
					const numPlayers = getNumPlayers($gameTable);
					const numRounds = ( typeof(round) !== 'undefined' ) ? round : getNumRounds($gameTable);

					let dealerIndex = ( numRounds <= numPlayers ) ? numRounds : (numRounds % numPlayers);

					if ( numRounds > 0 )
					{
						const dealerClass = 'dealer';
						$playerInfoCols = $gameTable.find('.player-info-column');
						$playerInfoCols.removeClass(dealerClass);

						dealerIndex--;
						$playerInfoCols.eq(dealerIndex).addClass(dealerClass);
					}
				}

				function createPlayer ()
				{
					const $templateTable = $('.game-table-template');
					const $gameTable = getGameTable();
					const numPlayers = getNumPlayers($gameTable);
					const nextPlayerNum = numPlayers + 1;

					// Create additional player

					// Info col
					const $playerInfoColTemplate = $templateTable.find('.player-info-column').clone();
					$playerInfoColTemplate.find('.player-name').html(`P${nextPlayerNum}`);
					const $playerInfoContainer = $gameTable.find('thead tr');
					$playerInfoContainer.append($playerInfoColTemplate);

					console.log(`Create player ${nextPlayerNum}`);
					$playerInfoColTemplate.addClass('animate__animated animate__bounceIn');

					// Round col
					const $playerRoundColTemplate = $templateTable.find('.player-round-column').clone();
					const $playerRoundContainer = $gameTable.find('tbody tr');
					$playerRoundContainer.each(function ( index, el )
					{
						const uniqueId = Math.floor(Math.random() * Date.now());
						const $playerRoundColTemplateClone = $playerRoundColTemplate.clone();

						$playerRoundColTemplateClone.find('.round-estimate-label').attr('for', `est-${uniqueId}`);
						$playerRoundColTemplateClone.find('.round-estimate').attr('id', `est-${uniqueId}`);
						
						$playerRoundColTemplateClone.find('.round-actual-label').attr('for', `act-${uniqueId}`);
						$playerRoundColTemplateClone.find('.round-actual').attr('id', `act-${uniqueId}`);

						$(el).append($playerRoundColTemplateClone);
						$playerRoundColTemplateClone.addClass('animate__animated animate__bounceInRight');
					});

					// Score col
					const $playerScoreColTemplate = $templateTable.find('.player-score-column').clone();
					const $playerScoreContainer = $gameTable.find('tfoot tr');
					$playerScoreContainer.append($playerScoreColTemplate);

					updateDealer();
				}

				function createRound ()
				{
					const $templateTable = $('.game-table-template');
					const $gameTable = getGameTable();
					const numPlayers = getNumPlayers($gameTable);
					const numRounds = getNumRounds($gameTable);
					const newRoundNum = numRounds + 1;
					const maxRounds = 14;

					if ( newRoundNum > maxRounds )
					{
						alert('Max rounds reached!');
						return;
					}
					
					console.log(`createRound #${newRoundNum} for ${numPlayers} player(s)`)

					// Create additional round row
					const $roundRowTemplate = $templateTable.find('tbody tr').clone();
					const $playerRoundColTemplate = $roundRowTemplate.find('.player-round-column:first').clone();
					$roundRowTemplate.find('.player-round-column').remove();

					// Create a round column for every additional player
					for ( let i = 1; i <= numPlayers; i++ )
					{
						console.log(`createRound playerRoundColTemplate ${i}`);

						const uniqueId = Math.floor(Math.random() * Date.now());
						const $playerRoundColTemplateClone = $playerRoundColTemplate.clone();

						$playerRoundColTemplateClone.find('.round-estimate-label').attr('for', `est-${uniqueId}`);
						$playerRoundColTemplateClone.find('.round-estimate').attr('id', `est-${uniqueId}`);
						
						$playerRoundColTemplateClone.find('.round-actual-label').attr('for', `act-${uniqueId}`);
						$playerRoundColTemplateClone.find('.round-actual').attr('id', `act-${uniqueId}`);

						$roundRowTemplate.append($playerRoundColTemplateClone);
					}

					// Got from 7 to 1 then 1 to 7
					let rowDisplayNum = newRoundNum;
					if ( newRoundNum < 8 )
					{
						rowDisplayNum = 8 - newRoundNum;
					}
					else
					{
						rowDisplayNum = newRoundNum - 7;
					}

					$roundRowTemplate.find('.round-number').html(`${rowDisplayNum}`);

					const $roundContainer = $gameTable.find('tbody');
					$roundContainer.prepend($roundRowTemplate);

					const activeClass = 'active';
					$gameTable.find('tbody tr').removeClass(activeClass);
					$roundRowTemplate.addClass(activeClass);

					updateDealer();
					$roundRowTemplate.addClass('animate__animated animate__flipInX');
				}

				function calculate ()
				{
					console.log('calculate');
					
					const $gameTable = getGameTable();
					const numPlayers = getNumPlayers($gameTable);
					const numRounds = getNumRounds($gameTable);

					// For each player
					for ( let playerIndex = 0; playerIndex < numPlayers; playerIndex++ )
					{
						const $playerInfoCol = $gameTable.find('thead tr .player-info-column').eq(playerIndex);
						const $playerScoreCol = $gameTable.find('tfoot tr .player-score-column').eq(playerIndex);

						// Start score total
						let playerScoreTotal = 0;

						// For each row
						for ( let roundIndex = 0; roundIndex < numRounds; roundIndex++ )
						{
							const $roundRow = $gameTable.find('tbody tr').eq(roundIndex);
							const $roundRowPlayerCol = $roundRow.find('.player-round-column').eq(playerIndex);

							// Convert to number
							// https://stackoverflow.com/questions/175739/how-can-i-check-if-a-string-is-a-valid-number
							const estimate = parseInt($roundRowPlayerCol.find('.round-estimate').val());
							const actual = parseInt($roundRowPlayerCol.find('.round-actual').val());

							// If not a number, skip
							if ( isNaN(estimate) || isNaN(actual) )
							{
								continue;
							}

							// Calculate score subtotal
							let playerScoreSubtotal = actual;
							if ( estimate == actual )
							{
								playerScoreSubtotal += 10;
							}

							// Calculate score total
							playerScoreTotal += playerScoreSubtotal;
						}

						// Update player score
						$playerInfoCol.find('.player-score').html(playerScoreTotal.toFixed());
						$playerScoreCol.find('.player-score').html(playerScoreTotal.toFixed());
					}
					
					// Check row is last
				}

			});


			//====================
			// Continue polling
		}
	}, 100);
})();