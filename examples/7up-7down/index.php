<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	
	<meta http-equiv="X-UA-Compatible" content="chrome=1, IE=9">
	<meta http-equiv="cleartype" content="on">
	<meta name="format-detection" content="telephone=no">
	<meta name="HandheldFriendly" content="true">
	<meta name="robots" content="noindex,nofollow">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Game">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 minimum-scale=1.0 user-scalable=0 minimal-ui shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<!--
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
	-->
	<link href="../../vendor/bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">

	<!-- Use common styles. -->
	<link href="../../vendor/fontawesome/fontawesome-free-6.2.0-web/css/all.min.css" rel="stylesheet">

	<!-- Animate.css -->
	<link href="../../vendor/animate.css/4.1.1/animate.min.css" rel="stylesheet">
	
	<?php
	// Gather files to load in order
	$stylesToLoad = [
		'styles/style.css',
	];
	// For each file
	foreach ( $stylesToLoad as $style )
	{
		// Add modified time onto file to break cache
		$modifiedTime = filemtime($style);
	?>
	<link href="<?php echo $style; ?>?<?php echo $modifiedTime; ?>" rel="stylesheet">
	<?php
	}
	?>

	<style type="text/css">
	</style>

	<!-- Fonts -->
	<!--
	<link rel="stylesheet" href="./assets/fonts/stylesheet.css">
	-->

	<!-- Custom config loading -->
	<script type="text/javascript">
    </script>
</head>
<body>

<div class="container-fluid">

<table border="0" cellpadding="0" cellspacing="0" class="game-table-template d-none">
	<thead>
		<tr>
			<th></th>
			<th class="player-info-column">
				<div class="ratio ratio-1x1 d-none">
					<svg class="bd-placeholder-img card-img-top rounded-circle border border-3 border-primary" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Image cap" preserveAspectRatio="xMidYMid slice" focusable="false">
						<title>Placeholder</title>
						<rect width="100%" height="100%" fill="#868e96"></rect>
					</svg>
				</div>
				<span class="player-name" contentEditable="true">P1</span>
				<div><span class="player-score fw-normal">0</span></div>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th>
				<span class="round-number">#</span>
			</th>
			<td class="player-round-column text-center">
				<div class="input-group">
					<label class="input-group-text round-estimate-label">
						<i class="fa-solid fa-hourglass-start"></i>
					</label>
					<input aria-label="Estimate" class="form-control round-estimate" min="0" max="7" placeholder="-" step="1" type="number">
				</div>
				<div class="input-group">
					<label class="input-group-text round-actual-label">
						<i class="fa-solid fa-hourglass-end"></i>
					</label>
					<input aria-label="Actual" class="form-control round-actual" min="0" max="7" placeholder="-" step="1" type="number">
				</div>
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td></td>
			<td class="player-score-column">
				<span class="player-score">0</span>
			</td>
		</tr>
	</tfoot>
</table>


<table border="0" cellpadding="0" cellspacing="0" class="game-table">
	<thead>
		<tr>
			<th>
				<button class="btn btn-sm btn-primary d-block mt-auto cta--player-add mb-2 position-relative" type="button">
					<i class="fa-solid fa-user"></i>
					<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-white text-primary">
						<i class="fa-solid fa-plus"></i>
						<span class="visually-hidden">Add</span>
					</span>
				</button>
				<button class="btn btn-sm btn-primary d-block mt-auto cta--round-add position-relative" type="button">
					<i class="fa-solid fa-diagram-next"></i>
					<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-white text-primary">
						<i class="fa-solid fa-plus"></i>
						<span class="visually-hidden">Add</span>
					</span>
				</button>
			</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>

</div>



	<!-- Optional JavaScript; choose one of the two! -->

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<!--
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
	-->
	<script type="text/javascript" src="../../vendor/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>

	<!-- Option 2: Separate Popper and Bootstrap JS -->
	<!--
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
	-->

	<!--
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	-->
	<script type="text/javascript" src="../../vendor/jquery/jquery-3.6.1.min.js"></script>

	<?php
	// Gather files to load in order
	$scriptsToLoad = [
		// Game
		//'scripts/utilities/Storage.js',
		//'scripts/utilities/Utilities.js',
		//'scripts/game/utilities/Clock.js',
		//'scripts/game/utilities/EventHandler.js',
		//'scripts/game/utilities/Logger.js',
		//'scripts/game/utilities/Timer.js',
		'scripts/main.js'
	];

	// For each file
	foreach ( $scriptsToLoad as $script )
	{
		// Add modified time onto file to break cache
		$modifiedTime = filemtime($script);
	?>
	<script src="<?php echo $script; ?>?<?php echo $modifiedTime; ?>" type="text/javascript"></script>
	<?php
	}
	?>

</body>
</html>
