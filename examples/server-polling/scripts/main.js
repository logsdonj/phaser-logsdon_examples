// Hide loading screen
document.addEventListener('readystatechange', (event) => {
	// If complete
	if ( event.target.readyState === 'complete' )
	{
		const loadingScreen = document.querySelector('#loading-screen');
		// Fade and then hide
		RL.a.animateCSS(loadingScreen, 'fadeOut').then((message) => {
			loadingScreen.classList.add('d-none');
		});
	}
});

// Poll with interval until available
(function() {
	let nTimer = setInterval(function() {
		// If available, stop polling
		if (window.jQuery) {
			clearInterval(nTimer);
			//====================


			
			// DOM ready
			jQuery(document).ready(function($)
			{
				console.log('test');

				pollServerTimer = setupPollServerTimeout();

			});

			// Window load
			jQuery(window).on('load', function ()
			{
			});

			

			//====================
			// Continue polling
		}
	}, 100);
})();

let pollServerTimer;

function setupPollServerTimeout ()
{
	return setTimeout( async function ()
	{
		await pollServer();
		pollServerTimer = setupPollServerTimeout();
	}, 1000);
}

function stopPollServerTimeout ()
{
	if ( pollServerTimer )
	{
		clearTimeout(pollServerTimer);
	}
}

async function pollServer ()
{
	// https://codeburst.io/polling-vs-sse-vs-websocket-how-to-choose-the-right-one-1859e4e13bd9
	
	// http://127.0.0.1/42connect/moen/moen-designer-pro-council/site/ajax-response.php
	const endpointUrl = 'ajax-response.php';
	let data;

	// https://dmitripavlutin.com/javascript-fetch-async-await/
	// https://medium.com/beginners-guide-to-mobile-web-development/the-fetch-api-2c962591f5c
	// https://www.atatus.com/blog/fetch-api-replacement-for-xmlhttprequest-xhr/
	await fetch(endpointUrl)
	.then((response) => response.json())
	.then((json) => {
		data = json;
	});
	
	console.log(data);

	/*
	// https://kinsta.com/knowledgebase/javascript-http-request/
	const xhr = new XMLHttpRequest();
	xhr.open('GET', endpointUrl);
	xhr.send();
	xhr.responseType = 'json';

	xhr.onload = () =>
	{
		if ( xhr.readyState == 4 && xhr.status == 200 )
		{
			const data = xhr.response;
			console.log(data);
		}
		else
		{
			console.log(`Error: ${xhr.status}`);
		}
	};
	*/

	/*
	const xhr = new XMLHttpRequest();
	xhr.open('POST', 'https://jsonplaceholder.typicode.com/posts');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	const body = JSON.stringify({
		title: 'Hello World',
		body: 'My POST request',
		userId: 900,
	});
	xhr.onload = () =>
	{
		var data = JSON.parse(xhr.responseText);

		if ( xhr.readyState == 4 && xhr.status == 201 )
		{
			console.log(data);
		}
		else
		{
			console.log(`Error: ${xhr.status}`);
		}
	};
	xhr.send(body);
	*/
}