<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	
	<meta http-equiv="X-UA-Compatible" content="chrome=1, IE=9">
	<meta http-equiv="cleartype" content="on">
	<meta name="format-detection" content="telephone=no">
	<meta name="HandheldFriendly" content="true">
	<meta name="robots" content="noindex,nofollow">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Game">
	<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 minimum-scale=1.0 user-scalable=0 minimal-ui shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<!--
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
	-->
	<link href="../../vendor/bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">

	<!-- Use common styles. -->
	<link href="../../vendor/fontawesome/fontawesome-free-6.2.0-web/css/all.min.css" rel="stylesheet">

	<!-- Animate.css -->
	<link href="../../vendor/animate.css/4.1.1/animate.min.css" rel="stylesheet">
	
	<?php
	// Gather files to load in order
	$stylesToLoad = [
		'styles/style.css',
	];
	// For each file
	foreach ( $stylesToLoad as $style )
	{
		// Add modified time onto file to break cache
		$modifiedTime = filemtime($style);
	?>
	<link href="<?php echo $style; ?>?<?php echo $modifiedTime; ?>" rel="stylesheet">
	<?php
	}
	?>

	<style type="text/css">
	</style>

	<!-- Fonts -->
	<!--
	<link rel="stylesheet" href="./assets/fonts/stylesheet.css">
	-->

	<!-- Custom config loading -->
	<script type="text/javascript">
    </script>
</head>
<body>

<div class="container-fluid d-none">

	<img alt="Slider Image" src="images/felix-berger-2evqv4zZxTY-unsplash.jpg">

	<div class="game-board-template">
		<div class="tile position-relative">
			<div class="tile-wrapper" style="width: 200px; height: 200px; background-image: url('images/felix-berger-2evqv4zZxTY-unsplash.jpg'); background-size: 800px 800px; background-position: -200px -200px;">
				<span class="tile-number-badge position-absolute top-0 end-0 mt-1 me-1 badge rounded-pill bg-secondary border border-2 border-white d-none">#</span>
				<span class="tile-number position-absolute top-50 start-50 translate-middle badge rounded-pill bg-secondary border border-2 border-white d-none">#</span>
			</div>
		</div>
	</div>

</div>

<!-- window width, game container width would change after tiles were loaded in container-fluid or no container -->
<div class="container">
	<div class="game-board position-relative justify-content-center m-auto"></div>
</div>

<div class="container-fluid pt-4 pb-4 d-flex align-items-center justify-content-between -position-fixed top-0 start-0">
	
	<div class="form-group">
		<label class="btn btn-outline-secondary btn-lg text-muted">
			<span class="fa-solid fa-image fa-fw">
				<span class="visually-hidden">Select Image</span>
			</span>
			<input accept="image/*" class="form-control cta--file-upload d-none" type="file"><!-- capture -->
		</label>
	</div>

	<button type="button" class="btn btn-outline-secondary btn-sm text-secondary text-muted cta--tile-show-badge" data-bs-toggle="button">
		<span class="fa-solid fa-hashtag fa-fw">
			<span class="visually-hidden">Show Numbers</span>
		</span>
	</button>

	<div class="form-group d-flex align-items-center">
		<button type="button" class="btn btn-outline-secondary btn-sm text-secondary text-muted cta--tile-num-decrease">
			<span class="fa-solid fa-minus fa-fw">
				<span class="visually-hidden">Decrease Number of Tiles</span>
			</span>
		</button>
		<i class="fa-solid fa-puzzle-piece fa-xl text-secondary text-muted d-inline-block ms-1 me-1"></i>
		<button type="button" class="btn btn-outline-secondary btn-sm text-secondary text-muted cta--tile-num-increase">
			<span class="fa-solid fa-plus fa-fw">
				<span class="visually-hidden">Increase Number of Tiles</span>
			</span>
		</button>
	</div>

	<button class="btn btn-outline-secondary btn-lg text-muted cta--tile-shuffle" type="button">
		<span class="fa-solid fa-shuffle fa-fw">
			<span class="visually-hidden">Reshuffle</span>
		</span>
	</button>

</div>

<!-- Loading screen -->
<div class="container-fluid d-flex vw-100 justify-content-center align-items-center flex-column min-vh-100 fixed-top" id="loading-screen" style="background-color: #ffffff;">
	<div class="d-flex justify-content-center">
		<div class="spinner-border" role="status">
			<span class="visually-hidden">Loading...</span>
		</div>
	</div>
</div>



	<!-- Optional JavaScript; choose one of the two! -->

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<!--
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
	-->
	<script type="text/javascript" src="../../vendor/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>

	<!-- Option 2: Separate Popper and Bootstrap JS -->
	<!--
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
	-->

	<!--
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	-->
	<script type="text/javascript" src="../../vendor/jquery/jquery-3.6.1.min.js"></script>

	<?php
	// Gather files to load in order
	$scriptsToLoad = [
		// Game
		//'scripts/utilities/Storage.js',
		//'scripts/utilities/Utilities.js',
		//'scripts/game/utilities/Clock.js',
		//'scripts/game/utilities/EventHandler.js',
		//'scripts/game/utilities/Logger.js',
		//'scripts/game/utilities/Timer.js',
		'../../vendor/redlove/scripts/RedLove.js',
		'../../vendor/redlove/scripts/library/animation.js',
		'../../vendor/redlove/scripts/library/fullscreen.js',
		'../../vendor/redlove/scripts/library/phaser.js',
		'../../vendor/redlove/scripts/library/utilities.js',
		'../../vendor/redlove/scripts/library/utilities/datetime.js',
		'../../vendor/redlove/scripts/library/utilities/dom.js',
		'../../vendor/redlove/scripts/library/utilities/function.js',
		'../../vendor/redlove/scripts/library/utilities/image.js',
		'../../vendor/redlove/scripts/library/utilities/number.js',
		'../../vendor/redlove/scripts/library/utilities/object.js',
		'../../vendor/redlove/scripts/library/utilities/string.js',
		'scripts/SlidingPuzzle.js',
		'scripts/main.js'
	];

	// For each file
	foreach ( $scriptsToLoad as $script )
	{
		// Add modified time onto file to break cache
		$modifiedTime = filemtime($script);
	?>
	<script src="<?php echo $script; ?>?<?php echo $modifiedTime; ?>" type="text/javascript"></script>
	<?php
	}
	?>

</body>
</html>
