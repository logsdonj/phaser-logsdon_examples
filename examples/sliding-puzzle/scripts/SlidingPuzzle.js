/**
 * 
 */
class SlidingPuzzle
{
	debug;

	defaultBoardParams = {
		//imageHeight: 0,
		//imageWidth: 0
	};
	
	defaultParams = {
		image: '',
		boardHeight: 900,
		boardWidth: 900,
		numCols: 3,
		numRows: 3,
		moveType: 'swap',
		blankTileClass: 'tile--blank',
		gameBoardClass: 'game-board',
		gameBoardTemplateClass: 'game-board-template',
		tileClass: 'tile',
		tileWrapperClass: 'tile-wrapper',
		tileNumberClass: 'tile-number',
		tileNumberBadgeClass: 'tile-number-badge',
		tileNumberAttributeName: 'data-tile-num',
		tileOriginalColumnAttributeName: 'data-tile-original-col',
		tileOriginalRowAttributeName: 'data-tile-original-row',
		tileColumnAttributeName: 'data-tile-col',
		tileRowAttributeName: 'data-tile-row'
	};
	params;

	board;
	boardDefault = {
		pieces: {},
		num: {}
	};
	boardSolution;
	boardPositionReferenceDefault = {
		num: {},
		rc: {}
	};
	boardPositionReference;

	numMoves = 0;
	startTime = 0;
	stopTime = 0;
	showTileNum;
	showTileNumBadge;

	constructor ( params )
	{
		this.params = Object.assign({}, this.defaultParams, params);

		// If an image was passed, process it
		if ( this.params.image )
		{
			this.processImage(this.params.image);
		}
	}
	
	async processImage ( file )
	{
		try
		{
			const dimensions = await RL.u.imageDimensions(file);

			this.createGameBoard({
				image: file,
				imageHeight: dimensions.height,
				imageWidth: dimensions.width
			});
		}
		catch ( error )
		{
			console.error(error);
		}
	}

	createGameBoard ( params )
	{
		if ( this.debug )
		{
			console.log(`Create board`);
		}

		// Set default params
		const defaultParams = this.defaultBoardParams;

		// If a string past, adjust parameters
		if ( typeof(params) == 'string' )
		{
			params = {image: params};
		}

		// Merge params
		//params = Object.assign({}, defaultParams, params);
		this.params = Object.assign(this.params, defaultParams, params);

		if ( this.debug )
		{
			console.log(this.params);
		}

		// Restart board
		this.board = structuredClone(this.boardDefault);
		this.boardPositionReference = structuredClone(this.boardPositionReferenceDefault);

		// Create set of images for grid
		const numCols = ( this.params.numCols >= 2 ) ? this.params.numCols : 2;
		const numRows = ( this.params.numRows >= 2 ) ? this.params.numRows : 2;
		const numTiles = numCols * numRows;
		this.params.numTiles = numTiles;
		const boardWidth = this.params.boardWidth;
		const boardHeight = this.params.boardHeight;
		const boardRatio = boardWidth / boardHeight;
		this.params.boardRatio = boardRatio;
		const tileWidth = boardWidth / numCols;
		this.params.tileWidth = tileWidth;
		const tileHeight = boardHeight / numRows;
		this.params.tileHeight = tileHeight;
		const tileRatio = tileWidth / tileHeight;
		this.params.tileRatio = tileRatio;

		const $gameBoard = $(`.${this.params.gameBoardClass}`);
		$gameBoard.empty();
		$gameBoard.css({
			width: this.params.boardWidth,
			height: this.params.boardHeight
		});

		const $tileTemplate = $(`.${this.params.gameBoardTemplateClass} .${this.params.tileClass}`);

		// Scale image so smallest size fills board
		const scaleDimensions = RL.p.scaleToWorld(this.params.imageWidth, this.params.imageHeight, this.params.boardWidth, this.params.boardHeight);

		const offsetX = (scaleDimensions.width - this.params.boardWidth) / 2;
		const offsetY = (scaleDimensions.height - this.params.boardHeight) / 2;

		// For each row
		for ( let rowI = 0; rowI < numRows; rowI++ )
		{
			// For each column
			for ( let colI = 0; colI < numCols; colI++ )
			{
				const tileNum = (rowI * numRows) + colI;

				if ( this.debug )
				{
					console.log(`Create tile row: ${rowI} col: ${colI} num: ${tileNum}`);
				}

				// Add to board
				this.board.pieces[`r${rowI}c${colI}`] = tileNum;
				this.board.num[tileNum] = {r: rowI, c: colI};

				// Add to board position reference
				this.boardPositionReference.num[tileNum] = 
				this.boardPositionReference.rc[`r${rowI}c${colI}`] = 
				{x: colI * tileWidth, y: rowI * tileHeight};

				const $tileClone = $tileTemplate.clone();
				$tileClone.attr({
					[this.params.tileNumberAttributeName]: tileNum,
					[this.params.tileColumnAttributeName]: colI,
					[this.params.tileRowAttributeName]: rowI,
					[this.params.tileOriginalColumnAttributeName]: colI,
					[this.params.tileOriginalRowAttributeName]: rowI
				});

				// If sliding, adjust styles
				if ( this.params.moveType == 'slide' )
				{
					$tileClone.addClass('position-absolute animate__animated');
					$tileClone.css({
						left: colI * tileWidth,
						top: rowI * tileHeight
					});
				}

				$tileClone.find(`.${this.params.tileNumberClass}`).html(tileNum + 1).toggleClass('d-none', ! this.showTileNum);
				$tileClone.find(`.${this.params.tileNumberBadgeClass}`).html(tileNum + 1).toggleClass('d-none', ! this.showTileNumBadge);
				const $tileWrapper = $tileClone.find(`.${this.params.tileWrapperClass}`);
				$tileWrapper.css({
					backgroundImage: `url('${this.params.image}')`,
					backgroundPosition: `-${(colI * tileWidth) + offsetX}px -${(rowI * tileHeight) + offsetY}px`,
					backgroundSize: `${scaleDimensions.width}px ${scaleDimensions.height}px`,
					height: `${tileHeight}px`,
					width: `${tileWidth}px`
				});

				// If on this last tile, it should be blank
				if ( colI == numCols - 1 && rowI == numRows - 1 )
				{
					$tileClone.addClass(`${this.params.blankTileClass}`);
					$tileWrapper.css({
						backgroundColor: `transparent`,
						backgroundImage: ``
					});
				}

				$gameBoard.append($tileClone);
			}

			// Force flex wrapping with styled element
			$gameBoard.append(`<hr>`);
		}

		/*
		// If not using another method to control column flex wrapper
		$gameBoard.find(`.${this.params.tileWrapperClass}`).css({
			flexBasis: `${100 / numCols}%`,
			flexGrow: 1,
			flexShrink: 0
		});
		*/
		
		this.setupTileClickEventListener();

		// Save board as solution
		this.boardSolution = structuredClone(this.board);
		// Shuffle tiles
		this.shuffleTiles();
	}

	isSolvable ( blankTileNum )
	{
		// Default that the blank tile is the bottom right piece
		blankTileNum = ( typeof(blankTileNum) === 'undefined' ) ? (this.params.numRows * this.params.numCols) - 1 : blankTileNum;

		const $gameBoard = $(`.${this.params.gameBoardClass}`);
		
		// Go through tiles in board major order and note the tile order
		let blankTileRow = 0;
		let blankTileCol = 0;
		let tileOrder = [];
		const $tiles = $gameBoard.find(`.${this.params.tileClass}`);
		$tiles.each(function ( index, el )
		{
			const $tile = $(el);

			// If blank tile, note its row
			if ( $tile.hasClass(this.params.blankTileClass) )
			{
				blankTileRow = +$tile.attr(this.params.tileRowAttributeName);
				blankTileCol = +$tile.attr(this.params.tileColumnAttributeName);
			}

			tileOrder.push($tile.attr(this.params.tileNumberAttributeName));
		}.bind(this));

		// Check for inversions
		// https://www.cs.princeton.edu/courses/archive/spring21/cos226/assignments/8puzzle/specification.php
		// https://datawookie.dev/blog/2019/04/sliding-puzzle-solvable/
		// https://www.geeksforgeeks.org/check-instance-15-puzzle-solvable/
		// https://math.stackexchange.com/a/2842654
		const numInversions = this.getNumInversions(tileOrder, blankTileNum);

		// Odd size board
		if ( this.params.numCols % 2 )
		{
			// Not solvable if odd
			return ( numInversions % 2 ) ? false : true;
		}
		else
		{
			const sum = numInversions + blankTileRow;
			// Not solvable if even
			return ( sum % 2 ) ? true : false;
		}
	}

	shuffleTiles ()
	{
		this.shuffleBoard();
		this.placeBoardTiles();
	}
	
	swapTiles ( $tileA, $tileB )
	{
		// Update element data
		const tileACol = $tileA.attr(this.params.tileColumnAttributeName);
		const tileARow = $tileA.attr(this.params.tileRowAttributeName);
		const tileBCol = $tileB.attr(this.params.tileColumnAttributeName);
		const tileBRow = $tileB.attr(this.params.tileRowAttributeName);

		// Swap elements
		if ( this.params.moveType == 'slide' )
		{
			const tileAPosition = this.boardPositionReference.rc[`r${tileARow}c${tileACol}`];
			const tileBPosition = this.boardPositionReference.rc[`r${tileBRow}c${tileBCol}`];
			$tileA.css({left: tileBPosition.x, top: tileBPosition.y});
			$tileB.css({left: tileAPosition.x, top: tileAPosition.y});
		}
		else
		{
			RL.u.swapElements($tileA[0], $tileB[0]);
		}

		$tileA.attr({
			[this.params.tileColumnAttributeName]: tileBCol,
			[this.params.tileRowAttributeName]: tileBRow
		});

		$tileB.attr({
			[this.params.tileColumnAttributeName]: tileACol,
			[this.params.tileRowAttributeName]: tileARow
		});
	}

	shuffleBoard ()
	{
		// For each row
		for ( let rowI = 0; rowI < this.params.numRows; rowI++ )
		{
			// For each column
			for ( let colI = 0; colI < this.params.numCols; colI++ )
			{
				const randomCol = RL.u.getRandomInt(0, this.params.numCols - 1);
				const randomRow = RL.u.getRandomInt(0, this.params.numRows - 1);

				// If the same element, skip
				if ( rowI === randomRow && colI === randomCol )
				{
					continue;
				}
				
				// Swap places
				let pieceNum = this.board.pieces[`r${rowI}c${colI}`];
				let randomPieceNum = this.board.pieces[`r${randomRow}c${randomCol}`];

				this.board.pieces[`r${rowI}c${colI}`] = randomPieceNum;
				this.board.num[pieceNum] = {r: randomRow, c: randomCol};

				this.board.pieces[`r${randomRow}c${randomCol}`] = pieceNum;
				this.board.num[randomPieceNum] = {r: rowI, c: colI};
			}
		}

		// Shuffle until solvable with inversions
		let tryI;
		for ( tryI = 0; tryI <= 100; tryI++ )
		{
			const alreadySolved = ( JSON.stringify(this.board) === JSON.stringify(this.boardSolution) );
			// If already solved or not solvable, try again
			if ( alreadySolved || ! this.isBoardSolvable() )
			{
				return this.shuffleBoard();
			}

			// Stop
			break;
		}

		if ( tryI > 100 )
		{
			alert(`Could not create a solvable board to play after ${tryI} tries.`);
		}

		// Reset tracking
		this.numMoves = 0;
		this.startTime = Date.now();
	}
	
	isBoardSolvable ( blankTileNum )
	{
		// Default that the blank tile is the bottom right piece
		blankTileNum = ( typeof(blankTileNum) === 'undefined' ) ? (this.params.numRows * this.params.numCols) - 1 : blankTileNum;

		// Go through tiles in board major order and note the tile order
		let blankTileRow = 0;
		let blankTileCol = 0;
		let tileOrder = [];

		// For each row
		for ( let rowI = 0; rowI < this.params.numRows; rowI++ )
		{
			// For each column
			for ( let colI = 0; colI < this.params.numCols; colI++ )
			{
				const tileNum = +this.board.pieces[`r${rowI}c${colI}`];

				// If blank tile, note its position
				if ( tileNum === blankTileNum )
				{
					blankTileRow = rowI;
					blankTileCol = colI;
				}

				tileOrder.push(tileNum);
			}
		}

		// Check for inversions
		// https://www.cs.princeton.edu/courses/archive/spring21/cos226/assignments/8puzzle/specification.php
		// https://datawookie.dev/blog/2019/04/sliding-puzzle-solvable/
		// https://www.geeksforgeeks.org/check-instance-15-puzzle-solvable/
		// https://math.stackexchange.com/a/2842654
		const numInversions = this.getNumInversions(tileOrder, blankTileNum);

		// Odd size board
		if ( this.params.numCols % 2 )
		{
			// Not solvable if odd
			return ( numInversions % 2 ) ? false : true;
		}
		else
		{
			const sum = numInversions + blankTileRow;
			// Not solvable if even
			return ( sum % 2 ) ? true : false;
		}
	}
	
	placeBoardTiles ()
	{
		const $gameBoard = $(`.${this.params.gameBoardClass}`);
		
		// For each row
		for ( let rowI = 0; rowI < this.params.numRows; rowI++ )
		{
			// For each column
			for ( let colI = 0; colI < this.params.numCols; colI++ )
			{
				let pieceNum = this.board.pieces[`r${rowI}c${colI}`];
				const $pieceTile = $gameBoard.find(`.${this.params.tileClass}[${this.params.tileNumberAttributeName}="${pieceNum}"]`);
				const $locationTile = $gameBoard.find(`.${this.params.tileClass}[${this.params.tileColumnAttributeName}="${colI}"][${this.params.tileRowAttributeName}="${rowI}"]`);

				// If the same element, skip
				if ( $pieceTile.is($locationTile) )
				{
					continue;
				}

				// Swap elements
				this.swapTiles($pieceTile, $locationTile);
			}
		}
	}

	checkProgress ()
	{
		const $gameBoard = $(`.${this.params.gameBoardClass}`);
		
		// Gather current tile orders
		const $tiles = $gameBoard.find(`.${this.params.tileClass}`);
		const numTiles = this.params.numTiles;
		let numCorrectTiles = 0;
		let tileOrders = [];

		$tiles.each(function( index, el ) {
			const $tile = $(el);
			tileOrders.push(`${$tile.attr(this.params.tileOriginalColumnAttributeName)},${$tile.attr(this.params.tileOriginalRowAttributeName)}`);

			// If current position matches the original position, correct
			if ( 
				$tile.attr(this.params.tileColumnAttributeName) === $tile.attr(this.params.tileOriginalColumnAttributeName) && 
				$tile.attr(this.params.tileRowAttributeName) === $tile.attr(this.params.tileOriginalRowAttributeName) 
			)
			{
				numCorrectTiles++;
			}
		}.bind(this));

		let percentComplete = numCorrectTiles / numTiles;
		return percentComplete;
	}

	getNumInversions ( orders, ignoreNum )
	{
		// Check for inversions
		// https://www.cs.princeton.edu/courses/archive/spring21/cos226/assignments/8puzzle/specification.php
		// https://datawookie.dev/blog/2019/04/sliding-puzzle-solvable/
		let numInversions = 0;

		for ( let i = 0; i < orders.length; i++ )
		{
			const order = orders[i];

			// If blank tile, skip
			if ( order == ignoreNum )
			{
				continue;
			}
			
			for ( let j = 0; j < i; j++ )
			{
				const prevOrder = orders[j];

				// If blank tile, skip
				if ( prevOrder == ignoreNum )
				{
					continue;
				}

				if ( prevOrder > order )
				{
					if ( this.debug )
					{
						console.log(`Inversion: ${order} - ${prevOrder}`);
					}

					numInversions++;
				}
			}
		}

		return numInversions;
	}

	// Event listeners

	setupTileClickEventListener ()
	{
		// https://developer.mozilla.org/en-US/docs/Web/API/Touch_events
		const handler = this.handleTileClick.bind(this);
		const eventName = ( 'ontouchend' in window ) ? 'touchend' : 'click';
		$(document)
		.off(eventName, `.${this.params.tileClass}`, handler)
		.on(eventName, `.${this.params.tileClass}`, handler);
	}

	// Event handlers

	handleTileClick ( event )
	{
		const $tile = $(event.currentTarget);
		const tileCol = $tile.attr(this.params.tileColumnAttributeName);
		const tileRow = $tile.attr(this.params.tileRowAttributeName);
		
		if ( this.debug )
		{
		}
			console.log(`Clicked tile ${tileCol} ${tileRow}`);

		event.stopImmediatePropagation();

		const $blankTile = $(`.${this.params.blankTileClass}`)

		// If the last tile that's blank clicked, stop
		if ( $tile[0] == $blankTile[0] )
		{
			return;
		}

		// Check if the clicked tile is next to the blank
		const blankTileCol = $blankTile.attr(this.params.tileColumnAttributeName);
		const blankTileRow = $blankTile.attr(this.params.tileRowAttributeName);
		// Valid values to be beside blank tile
		const blankLeftPosition = `${+blankTileCol - 1},${blankTileRow}`;
		const blankRightPosition = `${+blankTileCol + 1},${blankTileRow}`;
		const blankTopPosition = `${blankTileCol},${+blankTileRow - 1}`;
		const blankBottomPosition = `${blankTileCol},${+blankTileRow + 1}`;
		const clickedTilePosition = `${tileCol},${tileRow}`;

		const validMoves = [blankLeftPosition, blankRightPosition, blankTopPosition, blankBottomPosition];
		const validMove = ( validMoves.indexOf(clickedTilePosition) !== -1 );

		// If clicked tile position near blank tile is not valid, stop
		if ( ! validMove )
		{
			if ( this.debug )
			{
				console.error('Invalid move.', clickedTilePosition, validMoves);
			}
			
			return;
		}

		this.numMoves++;

		// Swap elements
		this.swapTiles($blankTile, $tile);

		const percentComplete = this.checkProgress();
		// If complete, show feedback
		if ( percentComplete === 1 )
		{
			this.stopTime = Date.now();
			const timeDiff = this.stopTime - this.startTime;
			const timeDurationText = RL.u.timeDurationToText(timeDiff);

			// Dispatch custom event
			const event = new CustomEvent('SlidingPuzzle_complete', {bubbles: true, cancelable: true, detail: {
				numMoves: this.numMoves,
				timeDurationText: timeDurationText
			}});
			document.dispatchEvent(event);
		}
	}

	handleReshuffle ( event )
	{
		this.shuffleTiles();
	}

	handleTileNumDecrease ( event )
	{
		this.params.numCols--;
		this.params.numRows--;
		this.params.numCols = ( this.params.numCols >= 2 ) ? this.params.numCols : 2;
		this.params.numRows = ( this.params.numRows >= 2 ) ? this.params.numRows : 2;
		
		this.createGameBoard({
			numCols: this.params.numCols,
			numRows: this.params.numRows
		});
	}

	handleTileNumIncrease ( event )
	{
		this.params.numCols++;
		this.params.numRows++;

		this.createGameBoard({
			numCols: this.params.numCols,
			numRows: this.params.numRows
		});
	}

	handleShowBadge ( event )
	{
		$(`.${this.params.gameBoardClass}`)
		.find(`.${this.params.tileNumberBadgeClass}`)
		.toggleClass('d-none');

		this.showTileNumBadge = ! this.showTileNumBadge;
	}

	async handleFileChange ( event )
	{
		if ( event.target.files.length == 0 )
		{
			// No file selected, ignore 
			return;
		}

		const fileInput = event.target;
		const imageFile = event.target.files[0];
		//const img = document.createElement('img');img.src = imageUrl;
		const imageUrl = URL.createObjectURL(imageFile);
		//document.querySelector('#qrcodeReaderInput').append(img);

		const result = await this.processImage(imageUrl)
		.then((result) => {
			fileInput.value = null;
		}).catch((error) => {
			console.error(`${(new Error()).stack.split("\n")[1].trim().split(" ")[1]}: Error with file. Reason: ${error}`);
		});
	}
}