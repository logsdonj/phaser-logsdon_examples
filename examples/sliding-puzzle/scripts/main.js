// Hide loading screen
document.addEventListener('readystatechange', (event) => {
	// If complete
	if ( event.target.readyState === 'complete' )
	{
		const loadingScreen = document.querySelector('#loading-screen');
		// Fade and then hide
		RL.a.animateCSS(loadingScreen, 'fadeOut').then((message) => {
			loadingScreen.classList.add('d-none');
		});
	}
});

// Poll with interval until available
(function() {
	let nTimer = setInterval(function() {
		// If available, stop polling
		if (window.jQuery) {
			clearInterval(nTimer);
			//====================


			
			// DOM ready
			jQuery(document).ready(function($)
			{
				// Make sure puzzle fits inside of browser
				const windowHeight = $(window).height();
				const gameContainerWidth = $('.game-board').width();
				const targetWidth = ( windowHeight < gameContainerWidth ) ? windowHeight : gameContainerWidth;
				// Start puzzle
				const Game = new SlidingPuzzle({
					boardHeight: targetWidth,
					boardWidth: targetWidth,
					numCols: 3,
					numRows: 3,
					moveType: 'slide'
				});

				// Load puzzle image
				const image = 'images/felix-berger-2evqv4zZxTY-unsplash.jpg';
				Game.processImage(image);

				// Event listeners
				jQuery(document).on('change', '.cta--file-upload', {}, Game.handleFileChange.bind(Game));
				jQuery(document).on('click', '.cta--tile-shuffle', {}, Game.handleReshuffle.bind(Game));
				jQuery(document).on('click', '.cta--tile-show-badge', {}, Game.handleShowBadge.bind(Game));
				jQuery(document).on('click', '.cta--tile-num-decrease', {}, Game.handleTileNumDecrease.bind(Game));
				jQuery(document).on('click', '.cta--tile-num-increase', {}, Game.handleTileNumIncrease.bind(Game));
				jQuery(document).on('SlidingPuzzle_complete', function ( event ) {
					// Avoid element swap race condition
					setTimeout(() => {
						alert(`Great job! ${event.detail.numMoves} moves in ${event.detail.timeDurationText}`);
					}, 500);
				}.bind(Game));
			});

			// Window load
			jQuery(window).on('load', function ()
			{
			});

			

			//====================
			// Continue polling
		}
	}, 100);
})();